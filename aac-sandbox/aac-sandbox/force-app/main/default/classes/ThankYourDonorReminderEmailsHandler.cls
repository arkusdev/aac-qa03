/**
 * @description Handler of the batch process with the same name as the class
 */
public without sharing class ThankYourDonorReminderEmailsHandler {
    
    static String schoolDonationRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('School Donation').getRecordTypeId();
    static String classroomAdoptionRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Classroom Adoption').getRecordTypeId();
    static String disbursementRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Disbursement').getRecordTypeId();

    static final Date THIRTY_DAYS_AGO = Date.today().addDays(-30);
    static final Date SIXTY_DAYS_AGO = Date.today().addDays(-60);
    static final Date NINETY_DAYS_AGO = Date.today().addDays(-90);

    /**
     * @description Get opportunities pending thanks
     * @return Opportunities that didnt thank donor in the last 30, 60 or 90 days and are from a certain record type
     */
    public static List<Opportunity> getOpportunities(){
        Set<String> recordTypes = new Set<String>{ schoolDonationRT, classroomAdoptionRT, disbursementRT };
        return [
            SELECT Id, CloseDate, ThanksNotified1__c,
            ThanksNotified2__c, ThanksNotified3__c,
            Teacher__r.Email, Teacher__c
            FROM Opportunity
            WHERE IsThanksReceived__c = FALSE
            AND RecordTypeId IN: recordTypes
            AND 
            (
                (CloseDate =: THIRTY_DAYS_AGO AND ThanksNotified1__c = FALSE) OR
                (CloseDate =: SIXTY_DAYS_AGO AND ThanksNotified2__c = FALSE) OR
                (CloseDate =: NINETY_DAYS_AGO AND ThanksNotified3__c = FALSE)
            )
            AND Teacher__r.Email != NULL
        ];
    }

    /**
     * @description Get opportunities pending thanks
     * @param opps List of opps from getOpportunities method
     * @return Number of emails sent
     */
    public static Integer sendEmail(List<Opportunity> opps) {
        OrgWideEmailAddress owa = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName='AdoptAClassroom.org' LIMIT 1];
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();

        EmailTemplate templateThankYourDonor1 = [SELECT Id FROM EmailTemplate WHERE name = 'Thank Your Donor Reminder 1' LIMIT 1];
        EmailTemplate templateThankYourDonor2 = [SELECT Id FROM EmailTemplate WHERE name = 'Thank Your Donor Reminder 2' LIMIT 1];
        EmailTemplate templateThankYourDonor3 = [SELECT Id FROM EmailTemplate WHERE name = 'Thank Your Donor Reminder 3' LIMIT 1];
        
        for (Opportunity opp : opps) {
            DonorReminderInfo newDonorReminder = new DonorReminderInfo();
            newDonorReminder.contactEmail = opp.Teacher__r.Email;
            newDonorReminder.contactId = opp.Teacher__c;
            if (opp.CloseDate == NINETY_DAYS_AGO){
                opp.ThanksNotified1__c = TRUE;
                opp.ThanksNotified2__c = TRUE;
                opp.ThanksNotified3__c = TRUE;
                newDonorReminder.emailTemplateId = templateThankYourDonor3.Id;
            } else {
                if (opp.CloseDate == SIXTY_DAYS_AGO){
                    opp.ThanksNotified1__c = TRUE;
                    opp.ThanksNotified2__c = TRUE;
                    newDonorReminder.emailTemplateId = templateThankYourDonor2.Id;
                } else {
                    if (opp.CloseDate == THIRTY_DAYS_AGO){
                        opp.ThanksNotified1__c = TRUE;
                        newDonorReminder.emailTemplateId = templateThankYourDonor1.Id;
                    }
                }
            }
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateID(newDonorReminder.emailTemplateId);
            mail.setWhatId(opp.Id);
            mail.setTargetObjectId(newDonorReminder.contactId);
            mail.setSaveAsActivity(false);
            mail.setOrgWideEmailAddressId(owa.Id);

            allmsg.add(mail);
        }

        update opps;

        if(!Test.isRunningTest()) {
            Messaging.sendEmail(allmsg, false);
        }
        return allmsg.size();
    }

    /**
     * @description Inner class that has information of the donor reminder
     */
    public class DonorReminderInfo {
        /**
         * @description Contact's id to send email
         */
        public String contactId { set; get; }
        /**
         * @description Contact's email to send
         */
        public String contactEmail { set; get; }
        /**
         * @description Id from the template
         */
        public String emailTemplateId { set; get; }
    }
}