@isTest
public inherited sharing class ThankYourDonorReminderEmailsTest {
    @TestSetup
    static void makeData(){
        String schoolDonationRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('School Donation').getRecordTypeId();

        AcctSeed__GL_Account__c newAcctSeed = new AcctSeed__GL_Account__c(
            Name = '4599 Teacher Restricted Released',
            AcctSeed__Type__c = 'Revenue'
        );
        insert newAcctSeed;

        Contact c = new Contact(
            LastName = 'Only Test',
            Email = 'test@test.com'
        );
        insert c;

        Opportunity oppNinetyDaysAgo = new Opportunity(
            CloseDate = Date.today().addDays(-90),
            StageName = 'Pledged',
            Name = 'Test Donation 90 days ago',
            RecordtypeId = schoolDonationRT,
            Teacher__c = c.Id
        );
        insert oppNinetyDaysAgo;

        Opportunity oppSixtyDaysAgo = new Opportunity(
            CloseDate = Date.today().addDays(-60),
            StageName = 'Pledged',
            Name = 'Test Donation 60 days ago',
            RecordtypeId = schoolDonationRT,
            Teacher__c = c.Id
        );
        insert oppSixtyDaysAgo;

        Opportunity oppThirtyDaysAgo = new Opportunity(
            CloseDate = Date.today().addDays(-30),
            StageName = 'Pledged',
            Name = 'Test Donation 30 days ago',
            RecordtypeId = schoolDonationRT,
            Teacher__c = c.Id
        );
        insert oppThirtyDaysAgo;
    }

    @isTest
    static void runBatchTest() {
        Test.startTest();
            Database.executeBatch(new ThankYourDonorReminderEmails());
        Test.stopTest();
        
        Opportunity oppNinetyDaysAgo = [SELECT ThanksNotified1__c, ThanksNotified2__c, ThanksNotified3__c FROM Opportunity WHERE Name = 'Test Donation 90 days ago'];
        Opportunity oppSixtyDaysAgo = [SELECT ThanksNotified1__c, ThanksNotified2__c, ThanksNotified3__c FROM Opportunity WHERE Name = 'Test Donation 60 days ago'];
        Opportunity oppThirtyDaysAgo = [SELECT ThanksNotified1__c, ThanksNotified2__c, ThanksNotified3__c FROM Opportunity WHERE Name = 'Test Donation 30 days ago'];
        System.assert(oppNinetyDaysAgo.ThanksNotified1__c && oppNinetyDaysAgo.ThanksNotified2__c && oppNinetyDaysAgo.ThanksNotified3__c, 'The donation with close date 90 days ago didnt change the thanks notified checkbox correctly');
        System.assert(oppSixtyDaysAgo.ThanksNotified1__c && oppSixtyDaysAgo.ThanksNotified2__c, 'The donation with close date 60 days ago didnt change the thanks notified checkbox correctly');
        System.assert(oppThirtyDaysAgo.ThanksNotified1__c, 'The donation with close date 30 days ago didnt change the thanks notified checkbox correctly');
    }

    @isTest
    static void runScheduleTest(){
        Test.startTest();
            ThankYourDonorReminderEmails scheduleThankYourDonor = new ThankYourDonorReminderEmails();
                        
            String seconds = Datetime.now().second() == 60 ? '0' : String.valueOf(Datetime.now().second() + 1);
            String hour = String.valueOf(Datetime.now().hour());
            String min = seconds == '0' ? String.valueOf(Datetime.now().minute() + 1) : String.valueOf(Datetime.now().minute()); 
            String ss = seconds;
            
            String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
            System.schedule('Schedule WavelinQ cases process at ' + String.valueOf(Datetime.now()), nextFireTime, scheduleThankYourDonor);
        Test.stopTest();
        System.assert(scheduleThankYourDonor != null, 'The Schedule process wasnt executed succesfully');
    }
}