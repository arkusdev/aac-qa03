// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Contact {
    global Id Id;
    global Boolean IsDeleted;
    global Contact MasterRecord;
    global Id MasterRecordId;
    global Account Account;
    global Id AccountId;
    global String LastName;
    global String FirstName;
    global String Salutation;
    global String Name;
    global RecordType RecordType;
    global Id RecordTypeId;
    global String OtherStreet;
    global String OtherCity;
    global String OtherState;
    global String OtherPostalCode;
    global String OtherCountry;
    global String OtherStateCode;
    global String OtherCountryCode;
    global Double OtherLatitude;
    global Double OtherLongitude;
    global String OtherGeocodeAccuracy;
    global Address OtherAddress;
    global String MailingStreet;
    global String MailingCity;
    global String MailingState;
    global String MailingPostalCode;
    global String MailingCountry;
    global String MailingStateCode;
    global String MailingCountryCode;
    global Double MailingLatitude;
    global Double MailingLongitude;
    global String MailingGeocodeAccuracy;
    global Address MailingAddress;
    global String Phone;
    global String Fax;
    global String MobilePhone;
    global String HomePhone;
    global String OtherPhone;
    global String AssistantPhone;
    global Contact ReportsTo;
    global Id ReportsToId;
    global String Email;
    global String Title;
    global String Department;
    global String AssistantName;
    global String LeadSource;
    global Date Birthdate;
    global String Description;
    global User Owner;
    global Id OwnerId;
    global Boolean HasOptedOutOfEmail;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global Datetime LastCURequestDate;
    global Datetime LastCUUpdateDate;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global String EmailBouncedReason;
    global Datetime EmailBouncedDate;
    global Boolean IsEmailBounced;
    global String PhotoUrl;
    global String Jigsaw;
    global String JigsawContactId;
    global Individual Individual;
    global Id IndividualId;
    global String npe01__AlternateEmail__c;
    global String npe01__HomeEmail__c;
    global String npe01__Home_Address__c;
    global Date npe01__Last_Donation_Date__c;
    global Decimal npe01__Lifetime_Giving_History_Amount__c;
    global String npe01__Organization_Type__c;
    global String npe01__Other_Address__c;
    global String npe01__PreferredPhone__c;
    global String npe01__Preferred_Email__c;
    global String npe01__Primary_Address_Type__c;
    global Boolean npe01__Private__c;
    global String npe01__Secondary_Address_Type__c;
    global String npe01__SystemAccountProcessor__c;
    global Boolean npe01__SystemIsIndividual__c;
    global String npe01__WorkEmail__c;
    global String npe01__WorkPhone__c;
    global String npe01__Work_Address__c;
    global String npo02__Formula_HouseholdMailingAddress__c;
    global String npo02__Formula_HouseholdPhone__c;
    global npo02__Household__c npo02__Household__r;
    global Id npo02__Household__c;
    global String npo02__Languages__c;
    global String npo02__Level__c;
    global String npo02__SystemHouseholdProcessor__c;
    global String active_on_db__c;
    /* Shows the type of Account this Contact is connected to.
    */
    global String npe01__Type_of_Account__c;
    global String Giving_preferences__c;
    global String Development_Partner_Type__c;
    global String District_Website_Mgmt_Company__c;
    global String Declined_Campaign__c;
    global Date AAC_Data_Upload_Date__c;
    global Decimal Dollars_Raised__c;
    global Double No_of_AAC_Teachers__c;
    global String Assigned_Manager__c;
    global Decimal npo02__AverageAmount__c;
    global Decimal npo02__Best_Gift_Year_Total__c;
    global String npo02__Best_Gift_Year__c;
    global Date npo02__FirstCloseDate__c;
    global Double npo02__Household_Naming_Order__c;
    global Decimal npo02__LargestAmount__c;
    global Date npo02__LastCloseDateHH__c;
    global Date npo02__LastCloseDate__c;
    global Decimal npo02__LastMembershipAmount__c;
    global Date npo02__LastMembershipDate__c;
    global String npo02__LastMembershipLevel__c;
    global String npo02__LastMembershipOrigin__c;
    global Decimal npo02__LastOppAmount__c;
    global Date npo02__MembershipEndDate__c;
    global Date npo02__MembershipJoinDate__c;
    /* Determines which household names this contact is NOT included as part of.
    */
    global String npo02__Naming_Exclusions__c;
    global Double npo02__NumberOfClosedOpps__c;
    global Double npo02__NumberOfMembershipOpps__c;
    global Decimal npo02__OppAmount2YearsAgo__c;
    global Decimal npo02__OppAmountLastNDays__c;
    global Decimal npo02__OppAmountLastYearHH__c;
    global Decimal npo02__OppAmountLastYear__c;
    global Decimal npo02__OppAmountThisYearHH__c;
    global Decimal npo02__OppAmountThisYear__c;
    global Double npo02__OppsClosed2YearsAgo__c;
    global Double npo02__OppsClosedLastNDays__c;
    global Double npo02__OppsClosedLastYear__c;
    global Double npo02__OppsClosedThisYear__c;
    global Decimal npo02__SmallestAmount__c;
    /* Total soft credit amount last year.
    */
    global Decimal npo02__Soft_Credit_Last_Year__c;
    /* Total soft credit amount this year
    */
    global Decimal npo02__Soft_Credit_This_Year__c;
    /* Total amount in soft credits for this Contact's lifetime. Defined in the Household Settings tab.
    */
    global Decimal npo02__Soft_Credit_Total__c;
    /* Total soft credit amount two years ago.
    */
    global Decimal npo02__Soft_Credit_Two_Years_Ago__c;
    global Decimal npo02__TotalMembershipOppAmount__c;
    global Decimal npo02__TotalOppAmount__c;
    global Decimal npo02__Total_Household_Gifts__c;
    /* If this address requires verification, click the Verify Address button at the top of the Address page. Consult the Help documentation in the Power of Us Hub for more information.
    */
    global String npsp__Address_Verification_Status__c;
    /* lookup to the Batch that this contact was created in.
    */
    global npsp__Batch__c npsp__Batch__r;
    /* lookup to the Batch that this contact was created in.
    */
    global Id npsp__Batch__c;
    /* The current Address that the Contact's mailing fields are filled with.
    */
    global npsp__Address__c npsp__Current_Address__r;
    /* The current Address that the Contact's mailing fields are filled with.
    */
    global Id npsp__Current_Address__c;
    /* When selected, this option marks the contact as deceased, and excludes them from household naming, emails, and phone calls.
    */
    global Boolean npsp__Deceased__c;
    /* When selected, this option excludes the contact from emails and phone calls.
    */
    global Boolean npsp__Do_Not_Contact__c;
    /* Checking this box will exclude this contact from the Household Formal Greeting.
    */
    global Boolean npsp__Exclude_from_Household_Formal_Greeting__c;
    /* Checking this box will exclude this contact from the Household Informal Greeting.
    */
    global Boolean npsp__Exclude_from_Household_Informal_Greeting__c;
    /* Checking this box will exclude this contact from the Household Name.
    */
    global Boolean npsp__Exclude_from_Household_Name__c;
    /* The amount of the earliest soft credit.
    */
    global Decimal npsp__First_Soft_Credit_Amount__c;
    /* The date of the earliest soft credit.
    */
    global Date npsp__First_Soft_Credit_Date__c;
    /* holds the ID of the current household, whether it is a Household Account, or a Household Object.
    */
    global String npsp__HHId__c;
    /* The amount of the largest soft credit.
    */
    global Decimal npsp__Largest_Soft_Credit_Amount__c;
    /* The date of the largest soft credit.
    */
    global Date npsp__Largest_Soft_Credit_Date__c;
    /* The amount of the most recent soft credit.
    */
    global Decimal npsp__Last_Soft_Credit_Amount__c;
    /* The date of the most recent soft credit.
    */
    global Date npsp__Last_Soft_Credit_Date__c;
    /* The number of soft credits, closed in the last number of days defined in NPSP Settings.
    */
    global Double npsp__Number_of_Soft_Credits_Last_N_Days__c;
    /* The number of soft credits last year.
    */
    global Double npsp__Number_of_Soft_Credits_Last_Year__c;
    /* The number of soft credits this year.
    */
    global Double npsp__Number_of_Soft_Credits_This_Year__c;
    /* The number of soft credits 2 years ago.
    */
    global Double npsp__Number_of_Soft_Credits_Two_Years_Ago__c;
    /* The number of soft credits.
    */
    global Double npsp__Number_of_Soft_Credits__c;
    /* Used to track the account of the contact's Organization Affiliation record that is marked primary.
    */
    global Account npsp__Primary_Affiliation__r;
    /* Used to track the account of the contact's Organization Affiliation record that is marked primary.
    */
    global Id npsp__Primary_Affiliation__c;
    /* This is is checked if the user is the Primary Contact on their Account.  The Primary Contact can be edited on the Account.
    */
    global Boolean npsp__Primary_Contact__c;
    /* The total amount of Soft Credit, closed in the last number of days defined in NPSP Settings.
    */
    global Decimal npsp__Soft_Credit_Last_N_Days__c;
    /* If checked, the Contact's Current Address lookup overrides the Household's default Address or seasonal Address.
    */
    global Boolean npsp__is_Address_Override__c;
    global String Gender__c;
    global String Alternate_Email__c;
    global Boolean Ambassador__c;
    global String Classroom_Number__c;
    global String Spotlight_Fund_interest__c;
    global String External_ID__c;
    global String Password__c;
    global Boolean Verified__c;
    /* Marks default 1099 box for reporting cash disbursements to a 1099 vendor.
    */
    global String AcctSeed__Default_1099_Box__c;
    /* Marks vendor to report cash disbursements on 1099-Misc form.
    */
    global Boolean AcctSeed__X1099_Vendor__c;
    global Boolean Active__c;
    /* Shows status based on Teacher Portal registration and donations granted
    */
    global String Contact_Status__c;
    /* Please select the first year you started teaching.
    */
    global String First_Year_of_Teaching__c;
    global String Recovery_Email__c;
    global String stayclassy__Address_Type__c;
    /* To change the number of days used to calculate this field, go to Develop > Custom Settings > Classy Settings > Manage > Edit Default
    */
    global Decimal stayclassy__Amount_Donated_in_Last_X_Days__c;
    global Decimal stayclassy__Amount_Recurring_Donations__c;
    global String stayclassy__Best_Time_to_Contact__c;
    global String stayclassy__Blog__c;
    global Double stayclassy__Classy_Fundraising_Page_Total__c;
    global String stayclassy__Company__c;
    global Datetime stayclassy__Data_Refreshed_At__c;
    global String stayclassy__Gender__c;
    global String stayclassy__Middle_Name__c;
    /* To change the number of days used to calculate this field, go to Develop > Custom Settings > Classy Settings > Manage > Edit Default
    */
    global Double stayclassy__Number_of_Donations_in_Last_X_Days__c;
    global Double stayclassy__Number_of_Recurring_Donations__c;
    global String stayclassy__Shirt_Size__c;
    global String stayclassy__Suffix__c;
    global String stayclassy__TEST_Email__c;
    global Boolean stayclassy__Text_Opt_In__c;
    global String stayclassy__Website__c;
    global Decimal stayclassy__amount_of_last_donation__c;
    global Double stayclassy__average_donation__c;
    global Date stayclassy__date_of_last_donation__c;
    global Boolean stayclassy__opt_in__c;
    global Double stayclassy__sc_member_id__c;
    global Decimal stayclassy__sc_total_donated__c;
    global Double stayclassy__sc_total_donations__c;
    global Decimal stayclassy__sc_total_fundraising__c;
    global String Billing_Address_Id__c;
    /* READ ONLY FIELD. May vary from Email address.
    */
    global String CommunityUserName__c;
    global Double First_Year_Teaching_Formula__c;
    global String Mailing_Address_Id__c;
    global Boolean RegistrationComplete__c;
    global Account School__r;
    global Id School__c;
    global Decimal Total_Funds_Raised__c;
    global Decimal Funds_Available__c;
    global String Order_Cloud_Spending_Account_Id__c;
    /* Id assigned to the contact by Google.
    */
    global Double pymt__Google_Buyer_Id__c;
    /* JSON string containing IDs for all remotely hosted customer profiles linked to this contact (includes profiles stored at multiple payment processors)
    */
    global String pymt__Hosted_Profiles__c;
    /* Check this box to allow PaymentConnect add-ons to present member-only pricing to this customer.
    */
    global Boolean pymt__Member_Pricing_Enabled__c;
    /* Id assigned by PayPal to this contact.
    */
    global String pymt__PayPal_Payer_Id__c;
    global String DonorDisplayName__c;
    /* Alternate payee name which can be used to populate the payee field on a cash disbursement.
    */
    global String AcctSeed__Alternate_Payee_Name__c;
    global String AcctSeed__Taxpayer_Identification_Number__c;
    /* How did you hear about AAC?
    */
    global String Referral_Source__c;
    global Date Total_Donation_Modified_Date__c;
    global Decimal Total_Donation__c;
    global String UserID__c;
    global Boolean Update_contact__c;
    global Boolean update_sa__c;
    global Boolean dupcheck__dc3DisableDuplicateCheck__c;
    global String dupcheck__dc3Index__c;
    global String stayclassy__Classy_Related_Member_Id__c;
    global Opportunity Donor_email__r;
    global Id Donor_email__c;
    global String Donor_email_address__c;
    global String stayclassy__Classy_Username__c;
    global String State_Postal_Code__c;
    global String Postal_Code_State__c;
    global Decimal Previous_Balance__c;
    global Boolean Marketing_Email_Opt_In__c;
    global Boolean Potential_Press_Contact__c;
    global String School_SalesforceID__c;
    global Boolean Press_Contact__c;
    global Double AAC_giving_priority__c;
    global String School_State__c;
    global String Donor_comments_improvements__c;
    global String Other_crowdfunding_sites_used__c;
    global String Donor_communication_preferences__c;
    global Double Interest_in_Cont_Ed_fund_for_teachers__c;
    global Double Interest_in_school_accounts__c;
    global String Why_I_give__c;
    global String Donor_opinion_on_AAC__c;
    global String Crowdfunding_sites_other__c;
    global String Why_support_school_accounts__c;
    global String Opposed_to_school_accounts__c;
    global String Support_cont_ed_fund_for_teachers__c;
    global String Opposed_to_cont_ed_fund_for_teachers__c;
    global String Personal_motivation_for_giving__c;
    global String Giving_preferences_other__c;
    global String Donor_communication_preferences_other__c;
    global Double Annual_Budget__c;
    global Double Estimated_Annual_Spending__c;
    global String Estimate_of_Change_in_Budget__c;
    global Double Students_Can_t_Afford__c;
    global String Basic_Supplies__c;
    global String Supplies_bought_on_Own__c;
    global String Supplies_for_Fundraising__c;
    global String Other_Crowdfunding__c;
    global String Addl_Comments__c;
    global String Welcome_Ref__c;
    global String Welcome_Why_Signed_Up__c;
    global String Welcome_Vendors__c;
    global String Welcome_Other_Sites__c;
    /* Did you hear about us from another teacher? Enter their name here.
    */
    global String Teacher_Referral__c;
    /* Checked if the contact record is that of a school principal
    */
    global Boolean School_Principal__c;
    global String Title__c;
    global Boolean Primary_School_Wide_Title_1__c;
    global Boolean Do_Not_Match_per_School_Record__c;
    global String No_of_Employees__c;
    global String Company_Revenue__c;
    global String Company_Type__c;
    global String Company_Website__c;
    global String Company_Ticker__c;
    /* The scoring system assigns a possible estimated range of investible assets to an executive. A plethora of variables is taken into consideration when calculating this score including insider transactions, compensation, title, company, and more.
    */
    global String Wealth_Score_360__c;
    global Decimal Annual_Compensation__c;
    global Decimal Stock_Sales_in_Last_Year__c;
    global Decimal Estimated_Holdings__c;
    global Account Company_HEPData__r;
    global Id Company_HEPData__c;
    global String Company_Revenue_HEPData__c;
    global String Industry__c;
    global String Sector__c;
    global Date First_Gift_Date__c;
    global String First_Gift_by_Fiscal_Month__c;
    /* We intend to publish the names of our donors in our Annual Report. If you check this box, your name may be published in this report depending on your gift level. If you have further questions, please contact us: info@adoptaclassroom.org or 1-877-384-0764.
    */
    global Boolean Annual_Report_Opt_In__c;
    global Boolean Primary_School_Title_1__c;
    global String Primary_School_NCESID__c;
    global Datetime Last_Expiring_Fund_Notification_Date__c;
    global String Account_Managed_By__c;
    /* Email address for account manager in cases where the account is not managed by the principal
    */
    global String Account_Manager_Email__c;
    /* Stage 1 - Registered
Stage 2 - Created page; no letter submitted
Stage 3 - Created page; not public-facing
Stage 4 - Public-facing page needs improvement
Stage 5 - Public-facing page with no donations
Stage 6 - Some funding
Stage 7 - Reached goal
    */
    global String School_Stage__c;
    global Boolean District_Level__c;
    global Decimal Funds_Raised_Last_18_Months__c;
    global Boolean Reviewed__c;
    global Boolean ToSyncOrderCloud__c;
    global Double Donors_Last_18_Months__c;
    global Boolean DS360oi__Background_Batch_Flag__c;
    global String DS360oi__DS_update__c;
    global Datetime DS360oi__DS_update_date__c;
    global String DS360oi__Middle_Initial_Name__c;
    global Double DS360oi__X5_Postal_code__c;
    global List<AcceptedEventRelation> AcceptedEventRelations;
    global List<Account> AcctSeed__Accounts__r;
    global List<Account> AcctSeed__Accounts1__r;
    global List<Account> npe01__Organizations__r;
    global List<AccountContactRelation> AccountContactRelations;
    global List<AccountContactRole> AccountContactRoles;
    global List<AcctSeedERP__Purchase_Order__c> AcctSeedERP__Purchase_Orders__r;
    global List<AcctSeedERP__Purchase_Order__c> AcctSeedERP__Purchase_Orders1__r;
    global List<AcctSeed__Account_Payable__c> AcctSeed__Accounts_Payable__r;
    global List<AcctSeed__Billing__c> AcctSeed__Billings__r;
    global List<AcctSeed__Billing__c> AcctSeed__Billings1__r;
    global List<AcctSeed__Cash_Disbursement__c> AcctSeed__Cash_Disbursements__r;
    global List<AcctSeed__Cash_Receipt__c> Cash_Receipts__r;
    global List<AcctSeed__Project_Task__c> AcctSeed__Project_Tasks__r;
    global List<AcctSeed__Recurring_Account_Payable__c> AcctSeed__Recurring_Payables__r;
    global List<ActivityHistory> ActivityHistories;
    global List<Announcement__c> Announcements__r;
    global List<Asset> Assets;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<Campaign> stayclassy__Campaigns__r;
    global List<CampaignMember> CampaignMembers;
    global List<Case> Cases;
    global List<CaseContactRole> CaseContactRoles;
    global List<ChargentOrders__ChargentOrder__c> Chargent_Orders__r;
    global List<ChargentOrders__ChargentOrder__c> Chargent_Order__r;
    global List<ChargentOrders__ChargentOrder__c> Chargent_Orders1__r;
    global List<ChargentOrders__Payment_Request__c> ChargentOrders__Payment_Requests__r;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactFeed> Feeds;
    global List<ContactHistory> Histories;
    global List<ContactRequest> ContactRequests;
    global List<ContactShare> Shares;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<Contract> ContractsSigned;
    global List<ContractContactRole> ContractContactRoles;
    global List<Control_Account_Balance__c> Control_Account_Balances__r;
    global List<DS360oi__DonorSearch_del_del__c> DS360oi__DonorSearch__r;
    global List<DeclinedEventRelation> DeclinedEventRelations;
    global List<Donor_Cart_Item__c> Donor_Cart_Items__r;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessageRelation> EmailMessageRelations;
    global List<EmailStatus> EmailStatuses;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<EventRelation> EventRelations;
    global List<Expired_Fund_Contact_Update__c> Expired_Fund_Contact_Updates__r;
    global List<GoogleDoc> GoogleDocs;
    global List<Lead> Leads__r;
    global List<ListEmailIndividualRecipient> ListEmailIndividualRecipients;
    global List<NetworkActivityAudit> ParentEntities;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<Opportunity> Opportunities;
    global List<Opportunity> Account_Transactions__r;
    global List<Opportunity> Opportunities__r;
    global List<Opportunity> npsp__Honoree_Opportunities__r;
    global List<Opportunity> npsp__Notification_Opportunities__r;
    global List<Opportunity> npsp__Opportunities__r;
    global List<Opportunity> stayclassy__Opportunities__r;
    global List<OpportunityContactRole> OpportunityContactRoles;
    global List<OutgoingEmailRelation> OutgoingEmailRelations;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<SocialPersona> Personas;
    global List<SurveyInvitation> SurveyInvitations;
    global List<SurveySubject> SurveySubjectEntities;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global List<UndecidedEventRelation> UndecidedEventRelations;
    global List<User> Users;
    global List<UserEmailPreferredPerson> PersonRecord;
    global List<donation_split__Designation__c> Designations__r;
    global List<mch_ed__MCH_Ed_Contact__c> mch_ed__MCH_Ed_Contact__r;
    global List<npe03__Recurring_Donation__c> npe03__R00N80000002bOmREAU__r;
    global List<npe4__Relationship__c> npe4__Relationships__r;
    global List<npe4__Relationship__c> npe4__Relationships1__r;
    global List<npe5__Affiliation__c> npe5__Affiliations__r;
    global List<npsp__DataImport__c> npsp__Data_Imports__r;
    global List<npsp__DataImport__c> npsp__Data_Imports1__r;
    global List<npsp__Partial_Soft_Credit__c> npsp__Partial_Soft_Credits__r;
    global List<pymt__PaymentX__c> Payments__r;
    global List<pymt__PaymentX__c> pymt__Payments__r;
    global List<pymt__Payment_Method__c> pymt__Payment_Methods__r;
    global List<pymt__Payment_Profile__c> pymt__Payment_Profiles__r;
    global List<pymt__Shopping_Cart_Item__c> pymt__R00N40000001tGO4EAM__r;
    global List<stayclassy__Classy_API_Process_Link__c> stayclassy__Classy_API_Process_Links_Contact__r;
    global List<stayclassy__Classy_Contact_Match__c> stayclassy__Classy_Contact_Match__r;
    global List<stayclassy__Classy_Custom_Answer__c> stayclassy__Classy_Custom_Answers__r;
    global List<stayclassy__Classy_Related_Entity__c> stayclassy__Classy_Related_Entities__r;
    global List<stayclassy__Classy_Supporter__c> stayclassy__Classy_Supporters__r;
    global List<stayclassy__Classy_Team__c> stayclassy__Classy_Teams__r;
    global List<stayclassy__sc_recurring_profile__c> stayclassy__SCRecurringContact__r;
    global List<AccountContactRoleChangeEvent> Contact;
    global List<CampaignMember> LeadOrContact;
    global List<CampaignMemberChangeEvent> Contact;
    global List<CaseTeamMember> Member;
    global List<Contact> ReportsTo;
    global List<ContentDistribution> RelatedRecord;
    global List<ContentVersion> FirstPublishLocation;
    global List<ContractChangeEvent> CustomerSigned;
    global List<EventChangeEvent> Who;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<Lead> ConvertedContact;
    global List<MatchingInformation> SFDCId;
    global List<OpportunityChangeEvent> Contact;
    global List<OutgoingEmail> Who;
    global List<SurveyResponse> Submitter;
    global List<TaskChangeEvent> Who;
    global List<UserChangeEvent> Contact;

    global Contact () 
    {
    }
}