// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Product2 {
    global Id Id;
    global String Name;
    global String ProductCode;
    global String Description;
    global Boolean IsActive;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global String Family;
    global ExternalDataSource ExternalDataSource;
    global Id ExternalDataSourceId;
    global String ExternalId;
    global String DisplayUrl;
    global String QuantityUnitOfMeasure;
    global Boolean IsDeleted;
    global Boolean IsArchived;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global String StockKeepingUnit;
    global String AcctSeed__Accounting_Type__c;
    global AcctSeed__GL_Account__c AcctSeed__Expense_GL_Account__r;
    global Id AcctSeed__Expense_GL_Account__c;
    global AcctSeed__Accounting_Variable__c AcctSeed__GL_Account_Variable_1__r;
    global Id AcctSeed__GL_Account_Variable_1__c;
    global AcctSeed__Accounting_Variable__c AcctSeed__GL_Account_Variable_2__r;
    global Id AcctSeed__GL_Account_Variable_2__c;
    global AcctSeed__Accounting_Variable__c AcctSeed__GL_Account_Variable_3__r;
    global Id AcctSeed__GL_Account_Variable_3__c;
    global AcctSeed__Accounting_Variable__c AcctSeed__GL_Account_Variable_4__r;
    global Id AcctSeed__GL_Account_Variable_4__c;
    global AcctSeed__GL_Account__c AcctSeed__Inventory_GL_Account__r;
    global Id AcctSeed__Inventory_GL_Account__c;
    /* Denotes whether the product is a managed inventory product
    */
    global Boolean AcctSeed__Inventory_Product__c;
    global String AcctSeed__Inventory_Type__c;
    /* This field is no longer used
    */
    global Boolean AcctSeed__Non_Inventoried_Item__c;
    global AcctSeed__GL_Account__c AcctSeed__Revenue_GL_Account__r;
    global Id AcctSeed__Revenue_GL_Account__c;
    global Double AcctSeed__Tax_Rate__c;
    global Double AcctSeed__Unit_Cost__c;
    global Account AcctSeedERP__Default_Vendor__r;
    global Id AcctSeedERP__Default_Vendor__c;
    /* This checkbox enables asset management for this product. With asset management an asset record is created when an inventory product is allocated to a sales order. The asset record is associated with the customer (account) and is used to track the product.
    */
    global Boolean AcctSeedERP__Inventory_Asset__c;
    global Double AcctSeedERP__Lead_Time__c;
    global AcctSeed__Project__c AcctSeedERP__Manufacturing_Order_Template__r;
    global Id AcctSeedERP__Manufacturing_Order_Template__c;
    global Double AcctSeedERP__Minimum_Order_Quantity__c;
    global Double AcctSeedERP__Safety_Stock_Quantity__c;
    /* Checking this field will exclude creating a Sales Order Line for any Opportunity Product associated with this Product when using the "Create Sales Order" button on an Opportunity.
    */
    global Boolean AcctSeedERP__Sales_Order_Exclude__c;
    global Boolean AcctSeedERP__Serialized__c;
    global Double stayclassy__Charity_Donation_Percentage__c;
    global Double stayclassy__Classy_Campaign_ID__c;
    global Campaign stayclassy__Classy_Campaign__r;
    global Id stayclassy__Classy_Campaign__c;
    global Double stayclassy__Classy_Product_ID__c;
    global Double stayclassy__Quantity_Available__c;
    global String Order_Cloud_Product_Id__c;
    /* PaymentConnect Category this product is associated with.
    */
    global pymt__Category__c pymt__Category__r;
    /* PaymentConnect Category this product is associated with.
    */
    global Id pymt__Category__c;
    /* Attach an image file to this record using the image upload widget to populate this field with an attachment Id. If a value has been set for the Image URL field, this value will be ignored.
    */
    global String pymt__Image_Id__c;
    /* Add or edit an image attachment to this record to make an image available for display on the website, or override any image attachments by entering the URL of an image on another server here. Clear this field to use image attachments.
    */
    global String pymt__Image_URL__c;
    global String pymt__Image__c;
    /* Number of items in stock
    */
    global Double pymt__Inventory__c;
    /* PaymentConnect Action to perform after this product has been paid for.
    */
    global String pymt__On_Payment_Completed__c;
    /* Check if the product is a physical product (ie requires shipping).
    */
    global Boolean pymt__Tangible__c;
    /* Check if the item is taxable.
    */
    global Boolean pymt__Taxable__c;
    global String pymt__Weight_Class__c;
    global Double pymt__Weight__c;
    global Boolean PunchoutProduct__c;
    global String Vendor_Name__c;
    global stayclassy__Classy_API_Request__c stayclassy__Classy_API_Request__r;
    global Id stayclassy__Classy_API_Request__c;
    global Decimal stayclassy__Deductible_Amount__c;
    global Datetime stayclassy__Ended_At__c;
    global Double stayclassy__Entries_Per_Ticket__c;
    global Boolean stayclassy__Is_Classy_Mode__c;
    global Double stayclassy__Max_Per_Transaction__c;
    global Double stayclassy__Min_Per_Transaction__c;
    global Datetime stayclassy__Started_At__c;
    global String Case_Safe_ID__c;
    global AcctSeed__Tax_Group__c AcctSeed__Tax_Group__r;
    global Id AcctSeed__Tax_Group__c;
    global List<AcctSeedERP__Inventory_Balance__c> AcctSeedERP__Inventory_Balance__r;
    global List<AcctSeedERP__Material__c> AcctSeedERP__Project_Materials__r;
    global List<AcctSeedERP__Purchase_Order_Line__c> AcctSeedERP__Purchase_Order_Lines__r;
    global List<AcctSeedERP__Sales_Order_Line__c> AcctSeedERP__Sales_Order_Line__r;
    global List<AcctSeed__Account_Payable_Line__c> AcctSeed__Account_Payable_Lines__r;
    global List<AcctSeed__Account_Tax__c> AcctSeed__Account_Taxes__r;
    global List<AcctSeed__Billing_Line__c> AcctSeed__Billing_Lines__r;
    global List<AcctSeed__Journal_Entry_Line__c> AcctSeed__Journal_Entry_Lines__r;
    global List<AcctSeed__Product_Part__c> AcctSeed__Product_Parts1__r;
    global List<AcctSeed__Product_Part__c> AcctSeed__Product_Parts__r;
    global List<AcctSeed__Project__c> AcctSeedERP__Projects__r;
    global List<AcctSeed__Recurring_Account_Payable_Line__c> AcctSeed__Recurring_Accounts_Payable_Line__r;
    global List<AcctSeed__Recurring_Billing_Line__c> AcctSeed__Recurring_Billing_Lines__r;
    global List<AcctSeed__Scheduled_Revenue_Expense__c> AcctSeed__Scheduled_Revenue_Expense__r;
    global List<AcctSeed__Transaction__c> AcctSeed__Transactions__r;
    global List<ActivityHistory> ActivityHistories;
    global List<Asset> Assets;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<GoogleDoc> GoogleDocs;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<PricebookEntry> PricebookEntries;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<Product2Feed> Feeds;
    global List<Product2History> Histories;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<SurveySubject> SurveySubjectEntities;
    global List<Task> Tasks;
    global List<donation_split__Designation_Budget__c> Designation_Budgets__r;
    global List<pymt__Shopping_Cart_Item__c> pymt__R00N40000001tGNZEA2__r;
    global List<stayclassy__Classy_API_Process_Link__c> stayclassy__Classy_API_Process_Links__r;
    global List<stayclassy__Classy_Custom_Answer__c> stayclassy__Classy_Custom_Answers__r;
    global List<stayclassy__Classy_Related_Entity__c> stayclassy__Classy_Related_Entities__r;
    global List<AssetChangeEvent> Product2;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<OpportunityLineItem> Product2;
    global List<OutgoingEmail> RelatedTo;
    global List<TaskChangeEvent> What;

    global Product2 () 
    {
    }
}