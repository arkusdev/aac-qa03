// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Lead {
    global Id Id;
    global Boolean IsDeleted;
    global Lead MasterRecord;
    global Id MasterRecordId;
    global String LastName;
    global String FirstName;
    global String Salutation;
    global String Name;
    global RecordType RecordType;
    global Id RecordTypeId;
    global String Title;
    global String Company;
    global String Street;
    global String City;
    global String State;
    global String PostalCode;
    global String Country;
    global String StateCode;
    global String CountryCode;
    global Double Latitude;
    global Double Longitude;
    global String GeocodeAccuracy;
    global Address Address;
    global String Phone;
    global String MobilePhone;
    global String Fax;
    global String Email;
    global String Website;
    global String PhotoUrl;
    global String Description;
    global String LeadSource;
    global String Status;
    global String Industry;
    global String Rating;
    global Decimal AnnualRevenue;
    global Integer NumberOfEmployees;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsConverted;
    global Date ConvertedDate;
    global Account ConvertedAccount;
    global Id ConvertedAccountId;
    global Contact ConvertedContact;
    global Id ConvertedContactId;
    global Opportunity ConvertedOpportunity;
    global Id ConvertedOpportunityId;
    global Boolean IsUnreadByOwner;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global String Jigsaw;
    global String JigsawContactId;
    global String EmailBouncedReason;
    global Datetime EmailBouncedDate;
    global Individual Individual;
    global Id IndividualId;
    global Boolean DS360oi__Background_Batch_Flag__c;
    global String DS360oi__DS_update__c;
    global Datetime DS360oi__DS_update_date__c;
    global String DS360oi__Middle_Initial_Name__c;
    global Double DS360oi__X5_Postal_code__c;
    global String npe01__Preferred_Email__c;
    global String npe01__Preferred_Phone__c;
    /* The batch this lead was created from.
    */
    global npsp__Batch__c npsp__Batch__r;
    /* The batch this lead was created from.
    */
    global Id npsp__Batch__c;
    global String npsp__CompanyCity__c;
    global String npsp__CompanyCountry__c;
    global String npsp__CompanyPostalCode__c;
    global String npsp__CompanyState__c;
    global String npsp__CompanyStreet__c;
    global Boolean dupcheck__dc3DisableDuplicateCheck__c;
    global String dupcheck__dc3Index__c;
    global Boolean dupcheck__dc3Web2Lead__c;
    /* Used to track which (if any) AdoptAClassroom.org Board Member is responsible for this lead.
    */
    global Contact Assigned_to_AAC_Board_Member__r;
    /* Used to track which (if any) AdoptAClassroom.org Board Member is responsible for this lead.
    */
    global Id Assigned_to_AAC_Board_Member__c;
    /* Used to track which (if any) AdoptAClassroom.org staff member is responsible for this lead.
    */
    global User Assigned_to_AAC_Staff__r;
    /* Used to track which (if any) AdoptAClassroom.org staff member is responsible for this lead.
    */
    global Id Assigned_to_AAC_Staff__c;
    global List<AcceptedEventRelation> AcceptedEventRelations;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CampaignMember> CampaignMembers;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DS360oi__DonorSearch_del_del__c> DS360oi__DonorSearch__r;
    global List<DeclinedEventRelation> DeclinedEventRelations;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessageRelation> EmailMessageRelations;
    global List<EmailStatus> EmailStatuses;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<EventRelation> EventRelations;
    global List<GoogleDoc> GoogleDocs;
    global List<LeadFeed> Feeds;
    global List<LeadHistory> Histories;
    global List<LeadShare> Shares;
    global List<ListEmailIndividualRecipient> ListEmailIndividualRecipients;
    global List<NetworkActivityAudit> ParentEntities;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<OutgoingEmailRelation> OutgoingEmailRelations;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<SocialPersona> Personas;
    global List<SurveyInvitation> SurveyInvitations;
    global List<SurveySubject> SurveySubjectEntities;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global List<UndecidedEventRelation> UndecidedEventRelations;
    global List<UserEmailPreferredPerson> PersonRecord;
    global List<mch_ed__MCH_Ed_Account_Lead__c> mch_ed__MCH_Ed_Account_Lead__r;
    global List<mch_ed__MCH_Ed_Contact_Lead__c> mch_ed__MCH_Ed_Contact_Lead__r;
    global List<pymt__Shopping_Cart_Item__c> pymt__R00N40000001tGO3EAM__r;
    global List<CampaignMember> LeadOrContact;
    global List<CampaignMemberChangeEvent> Lead;
    global List<ContentDistribution> RelatedRecord;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> Who;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<OutgoingEmail> Who;
    global List<SurveyResponse> Submitter;
    global List<TaskChangeEvent> Who;

    global Lead () 
    {
    }
}