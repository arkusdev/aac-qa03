// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Opportunity {
    global Id Id;
    global Boolean IsDeleted;
    global Account Account;
    global Id AccountId;
    global RecordType RecordType;
    global Id RecordTypeId;
    global Boolean IsPrivate;
    global String Name;
    global String Description;
    global String StageName;
    global Decimal Amount;
    global Double Probability;
    global Decimal ExpectedRevenue;
    global Double TotalOpportunityQuantity;
    global Date CloseDate;
    global String Type;
    global String NextStep;
    global String LeadSource;
    global Boolean IsClosed;
    global Boolean IsWon;
    global String ForecastCategory;
    global String ForecastCategoryName;
    global Campaign Campaign;
    global Id CampaignId;
    global Boolean HasOpportunityLineItem;
    global Pricebook2 Pricebook2;
    global Id Pricebook2Id;
    global User Owner;
    global Id OwnerId;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global Integer FiscalQuarter;
    global Integer FiscalYear;
    global String Fiscal;
    global Contact Contact;
    global Id ContactId;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global Contract Contract;
    global Id ContractId;
    global Boolean HasOpenActivity;
    global Boolean HasOverdueTask;
    global npe03__Recurring_Donation__c npe03__Recurring_Donation__r;
    global Id npe03__Recurring_Donation__c;
    global String npo02__CurrentGenerators__c;
    global String npo02__DeliveryInstallationStatus__c;
    global String npo02__MainCompetitors__c;
    global String npo02__OrderNumber__c;
    global String npo02__TrackingNumber__c;
    global String Target_Area_s__c;
    global Double No_of_Teachers__c;
    global String Development_Partner_Type__c;
    global Double No_of_Prospective_Donors__c;
    /* Formula: The Amount of this Opportunity minus the Payment Writeoff Amount and Payment Amount Received.
    */
    global Decimal npe01__Amount_Outstanding__c;
    /* Hidden field for passing a Contact Id for Contact Role creation.
    */
    global String npe01__Contact_Id_for_Role__c;
    /* If payments are active, and if checked, a new payment will not be automatically created for this Opportunity.
    */
    global Boolean npe01__Do_Not_Automatically_Create_Payment__c;
    /* Formula: Whether or not the related Account is defined as an individual (SYSTEMIsIndividual__c)
    */
    global String npe01__Is_Opp_From_Individual__c;
    /* Determines the value on the Contact or Account of the Last Membership Level field.
    */
    global String npe01__Member_Level__c;
    /* Date membership period ends.
    */
    global Date npe01__Membership_End_Date__c;
    /* Is this a renewal, a new membership, or a membership with someone who lapsed and has returned.
    */
    global String npe01__Membership_Origin__c;
    /* Date when membership period starts
    */
    global Date npe01__Membership_Start_Date__c;
    /* Total value of the Payments marked as written-off or noncollectable for this Opportunity.
    */
    global Decimal npe01__Amount_Written_Off__c;
    /* Total number of payments for this Opportunity (Donation)
    */
    global Double npe01__Number_of_Payments__c;
    /* The total value of Payments marked as paid for this Opportunity.
    */
    global Decimal npe01__Payments_Made__c;
    /* Concatenated fields used during rollups to locate the most recent opportunity.
    */
    global String npo02__CombinedRollupFieldset__c;
    /* Controls if Household contact roles are created for Opportunities.  DEPRECATED.  Replaced by custom settings.
    */
    global String npo02__systemHouseholdContactRoleProcessor__c;
    /* Date of acknowledgment
    */
    global Date npsp__Acknowledgment_Date__c;
    /* Status of Acknowledgment of this gift.
    */
    global String npsp__Acknowledgment_Status__c;
    global npsp__Batch__c npsp__Batch__r;
    global Id npsp__Batch__c;
    /* The Fair Market Value of an In-Kind gift.
    */
    global Decimal npsp__Fair_Market_Value__c;
    /* The date of the grant's inception, according to the document or contract sent by the grant-making organization.
    */
    global Date npsp__Grant_Contract_Date__c;
    /* Tracks unique numbers sometimes assigned by grant-making organizations.
    */
    global String npsp__Grant_Contract_Number__c;
    global Date npsp__Grant_Period_End_Date__c;
    global Date npsp__Grant_Period_Start_Date__c;
    /* Describes the funding areas for the grant. You can enter anything you want here. For example, you could enter the name of the program the grant applies to.
    */
    global String npsp__Grant_Program_Area_s__c;
    /* A URL that points to the grant requirements on the grant-maker's website.
    */
    global String npsp__Grant_Requirements_Website__c;
    /* The existing Contact record of the honoree for this donation.
    */
    global Contact npsp__Honoree_Contact__r;
    /* The existing Contact record of the honoree for this donation.
    */
    global Id npsp__Honoree_Contact__c;
    /* The honoree name as provided by the donor. If this field is left blank, and the Honoree Contact field contains a value, Salesforce automatically populates this field with that value.
    */
    global String npsp__Honoree_Name__c;
    /* Holds any rich text description of the In-Kind Gift, including pictures.
    */
    global String npsp__In_Kind_Description__c;
    /* Checked if the donor provided the Fair Market Value for this In-Kind gift.
    */
    global Boolean npsp__In_Kind_Donor_Declared_Value__c;
    /* The type of In-Kind gift.
    */
    global String npsp__In_Kind_Type__c;
    /* If selected, indicates that the grant is a renewal of a previous grant.
    */
    global Boolean npsp__Is_Grant_Renewal__c;
    /* The account of the company who will match this gift.
    */
    global Account npsp__Matching_Gift_Account__r;
    /* The account of the company who will match this gift.
    */
    global Id npsp__Matching_Gift_Account__c;
    /* The name of the company who will match this gift.
    */
    global String npsp__Matching_Gift_Employer__c;
    /* The status of this gift being matched.
    */
    global String npsp__Matching_Gift_Status__c;
    /* The employer's matching gift for this employee's gift.
    */
    global Opportunity npsp__Matching_Gift__r;
    /* The employer's matching gift for this employee's gift.
    */
    global Id npsp__Matching_Gift__c;
    /* A personalized message for the notification recipient.
    */
    global String npsp__Notification_Message__c;
    /* How the notification recipient should be notified.
    */
    global String npsp__Notification_Preference__c;
    /* The existing Contact record of the person to be notified for this donation.
    */
    global Contact npsp__Notification_Recipient_Contact__r;
    /* The existing Contact record of the person to be notified for this donation.
    */
    global Id npsp__Notification_Recipient_Contact__c;
    /* The contact information (as provided by the donor) for the notification recipient.
    */
    global String npsp__Notification_Recipient_Information__c;
    /* The name (as provided by the donor) of the person who should be notified about this gift. If this field is left blank and the Notification Recipient Contact field contains a value, Salesforce automatically populates this field with that value.
    */
    global String npsp__Notification_Recipient_Name__c;
    /* If this grant is a renewal of a previous grant, use this field to relate this record to the previous one.
    */
    global Opportunity npsp__Previous_Grant_Opportunity__r;
    /* If this grant is a renewal of a previous grant, use this field to relate this record to the previous one.
    */
    global Id npsp__Previous_Grant_Opportunity__c;
    /* The status to use when creating a campaign member for this opportunity's primary contact and primary campaign source. Leave blank to default to an responded status for closed/won opportunities, and a non-responded status for other opportunities.
    */
    global String npsp__Primary_Contact_Campaign_Member_Status__c;
    /* The primary contact for this opportunity. The primary contact role is kept in sync with this field. For individual donations, this denotes attribution for this opportunity.
    */
    global Contact npsp__Primary_Contact__r;
    /* The primary contact for this opportunity. The primary contact role is kept in sync with this field. For individual donations, this denotes attribution for this opportunity.
    */
    global Id npsp__Primary_Contact__c;
    /* Installment name for inclusion in Opportunity Naming. Shows the installment number in parenthesis for open ended recurring donations, or the installment number out of the total for recurring donations with a set number of installments.
    */
    global String npsp__Recurring_Donation_Installment_Name__c;
    /* The installment number of this opportunity's recurring donation schedule.
    */
    global Double npsp__Recurring_Donation_Installment_Number__c;
    /* The amount you are requesting. This number might be different from the final award amount.
    */
    global Decimal npsp__Requested_Amount__c;
    /* Indicates whether the gift is in honor or memory of an individual.
    */
    global String npsp__Tribute_Type__c;
    /* The NPSP updates this field automatically based on the next Grant Deadline record.
    */
    global Date npsp__Next_Grant_Deadline_Due_Date__c;
    global String AcctSeed__TrackingNumber__c;
    global Date AdoptionDate__c;
    global Boolean Anonymous__c;
    global String ConfirmationCode__c;
    /* Date donation expires.
    */
    global Date Expiration_Date__c;
    global String External_ID__c;
    /* Date donation pledged
    */
    global Date PledgeDate__c;
    global AcctSeed__Project_Task__c ProjectTask__r;
    global Id ProjectTask__c;
    global AcctSeed__Project__c Project__r;
    global Id Project__c;
    global Boolean Sponsorship__c;
    /* Notes on why the donation ask was not successful.
    */
    global String npsp__Closed_Lost_Reason__c;
    global Boolean stayclassy__Anonymous_Donor__c;
    global String stayclassy__Billing_First_Name__c;
    global String stayclassy__Billing_Last_Name__c;
    global String stayclassy__Browser_Info__c;
    global Campaign stayclassy__Campaign__r;
    global Id stayclassy__Campaign__c;
    global stayclassy__Classy_Custom_Answer__c stayclassy__Classy_Custom_Answer__r;
    global Id stayclassy__Classy_Custom_Answer__c;
    global Decimal stayclassy__Classy_Order_Total__c;
    global String stayclassy__Credit_Card_Expiration_Date__c;
    global String stayclassy__Credit_Card_Last_Four_Digits__c;
    global String stayclassy__Credit_Card_Type__c;
    global Boolean stayclassy__Dedication_Ecard_Request__c;
    global String stayclassy__Dedication_Email_Body__c;
    global String stayclassy__Device_Type__c;
    global String stayclassy__Discount_Code__c;
    global Double stayclassy__Discount_Total__c;
    global Campaign stayclassy__Fundraising_Page__r;
    global Id stayclassy__Fundraising_Page__c;
    global Campaign stayclassy__Fundraising_Team__r;
    global Id stayclassy__Fundraising_Team__c;
    global Boolean stayclassy__Hide_Donation_Amount__c;
    global Boolean stayclassy__Hide_Donation_Comment__c;
    global Boolean stayclassy__Offline_Donation__c;
    global Double stayclassy__Organization_ID__c;
    global String stayclassy__PaymentID__c;
    global String stayclassy__Payment_Method__c;
    global String stayclassy__Transaction_Type__c;
    global String stayclassy__cc_address2__c;
    global String stayclassy__cc_address__c;
    global String stayclassy__cc_city__c;
    global String stayclassy__cc_country__c;
    global String stayclassy__cc_state__c;
    global String stayclassy__cc_zip__c;
    global String stayclassy__check_number__c;
    global String stayclassy__dedication_contact_address__c;
    global String stayclassy__dedication_contact_city__c;
    global String stayclassy__dedication_contact_country__c;
    global String stayclassy__dedication_contact_email__c;
    global String stayclassy__dedication_contact_name__c;
    global String stayclassy__dedication_contact_state__c;
    global String stayclassy__dedication_contact_zip__c;
    global Boolean stayclassy__dedication_include_gift_amount__c;
    global String stayclassy__dedication_name__c;
    global String stayclassy__dedication_type__c;
    global Decimal stayclassy__donation_total__c;
    global String stayclassy__gateway_name__c;
    global String stayclassy__gateway_transaction_id__c;
    global Decimal stayclassy__host_total__c;
    global Datetime stayclassy__order_date__c;
    global Datetime stayclassy__refund_date__c;
    global String stayclassy__sc_order_id__c;
    global Contact stayclassy__sf_contact_id__r;
    global Id stayclassy__sf_contact_id__c;
    global stayclassy__sc_designation__c stayclassy__sf_designation_id__r;
    global Id stayclassy__sf_designation_id__c;
    global stayclassy__sc_recurring_profile__c stayclassy__sf_recurring_id__r;
    global Id stayclassy__sf_recurring_id__c;
    global Decimal stayclassy__transaction_total__c;
    global Datetime DateOrderCreated__c;
    global Datetime DateOrderSubmitted__c;
    global Boolean IsSubmitted__c;
    global String Order_Cloud_Order_Id__c;
    global Decimal PromotionDiscount__c;
    global Account School__r;
    global Id School__c;
    global Decimal ShippingCost__c;
    global Decimal Subtotal__c;
    global Decimal TaxCost__c;
    global Contact Teacher__r;
    global Id Teacher__c;
    /* Total Amount - Payments Made
    */
    global Decimal pymt__Balance__c;
    /* PaymentConnect currency code.
    */
    global String pymt__Currency_ISO_Code__c;
    /* Recurring payments frequency
    */
    global Double pymt__Frequency__c;
    global String pymt__Invoice_Number__c;
    /* Total number of settled or completed payments related to this opportunity. Updated automatically when a payment record is added, updated or removed.
    */
    global Double pymt__Number_of_Payments_Made__c;
    /* Number of recurring payment occurrences.
    */
    global Double pymt__Occurrences__c;
    /* Purchase order number.  Used when generating PaymentConnect invoices.
    */
    global String pymt__PO_Number__c;
    /* Checked by PaymentConnect when payments equal or exceed the opportunity amount (if >0).
    */
    global Boolean pymt__Paid_Off__c;
    /* Total of settled payments related to this opportunity. This field is updated automatically when a payment record is added, updated or removed.
    */
    global Decimal pymt__Payments_Made__c;
    /* Recurring payments period.
    */
    global String pymt__Period__c;
    global Decimal pymt__Recurring_Amount__c;
    /* PaymentConnect Shipping Amount
    */
    global Decimal pymt__Shipping__c;
    /* Date that the web-accessible quote on this opportunity expires.  Leave blank to disable the site quote entirely.
    */
    global Date pymt__SiteQuote_Expiration__c;
    /* Defines the way recurring payment terms should presented up on the web-accessible quote or invoice page for this opportunity. Note that the payment processor selected for the site may also influence the way subscriptions can be setup.
    */
    global String pymt__SiteQuote_Recurring_Setup__c;
    /* Database name of Sites website or community portal to publish quote or invoice to (optional).  If listing multiple site names separate with a comma.
    */
    global String pymt__Site_Name__c;
    /* Date to start recurring or subscription payments
    */
    global Date pymt__Subscription_Start_Date__c;
    /* PaymentConnect tax amount
    */
    global Decimal pymt__Tax__c;
    /* PaymentConnect total calculated amount (Opportunity amount + shipping + tax)
    */
    global Decimal pymt__Total_Amount__c;
    global String Donor_Address__c;
    global String Donor_Name__c;
    global String Honoree_Email__c;
    global String Public_Classroom_Landing_Page_URL__c;
    global Double OrderCloudLineItemCount__c;
    global Boolean Processing_Fee__c;
    global Decimal Total_Amount__c;
    global Double OpportunityProductCount__c;
    global String Honoree_message__c;
    global String Acknowledge_status__c;
    global Boolean Teacher_Thank_you_note_mailed__c;
    global String State_designation__c;
    global String Postal_Code_state__c;
    global Double stayclassy__Cash_Ledger_ID__c;
    global String Donor_email__c;
    global String Donor_comment__c;
    global Boolean IsThanksReceived__c;
    global String Donation_Contact_Name__c;
    global stayclassy__Cash_Ledger__c stayclassy__Cash_Ledger__r;
    global Id stayclassy__Cash_Ledger__c;
    global String Classroom_page_URL__c;
    global Double stayclassy__Charged_Classy_Fees_Amount__c;
    global String test_link__c;
    global String stayclassy__Charged_Currency_Code__c;
    global String Teacher_s_School__c;
    global pymt__Category_Split__c Campaign_designations__r;
    global Id Campaign_designations__c;
    global Campaign Secondary_Campaign_Source__r;
    global Id Secondary_Campaign_Source__c;
    global Decimal Primary_Campaign_split_amount__c;
    global Decimal Secondary_Campaign_split_amount__c;
    global String Teacher_ID__c;
    global String Teacher_Email__c;
    global String Teacher_s_School_Street__c;
    global Double Teacher_Funds_Available__c;
    global Double stayclassy__Charged_Fees_Amount__c;
    global Double stayclassy__Charged_Processor_Fees_Amount__c;
    global Double stayclassy__Charged_Total_Gross_Amount__c;
    global String stayclassy__Classy_API_Request_Status2__c;
    global stayclassy__Classy_API_Request__c stayclassy__Classy_API_Request__r;
    global Id stayclassy__Classy_API_Request__c;
    global Double stayclassy__Classy_Business_ID__c;
    global Double stayclassy__Classy_Campaign_ID__c;
    global Double stayclassy__Classy_Designation_ID__c;
    global Double stayclassy__Classy_Fundraising_Page_ID__c;
    global Double stayclassy__Classy_Fundraising_Team_ID__c;
    global String stayclassy__Classy_Member_Email__c;
    global Double stayclassy__Classy_Member_ID__c;
    global String stayclassy__Classy_Member_Phone__c;
    global Double stayclassy__Classy_Pay_Payment_Token_ID__c;
    global Double stayclassy__Classy_Pay_Recurring_ID__c;
    global Double stayclassy__Classy_Pay_Transaction_ID__c;
    global String stayclassy__Classy_Processor_Object_ID__c;
    global Double stayclassy__Classy_Recurring_Donor_ID__c;
    global String stayclassy__Company_Name__c;
    global String stayclassy__Currency_Code__c;
    /* Classy ID of the dedication Ecard record
    */
    global Double stayclassy__Dedication_Ecard_ID__c;
    /* The Classy ID for the Dedication object.
    */
    global Double stayclassy__Dedication_ID__c;
    /* For donations to UK charitable entities, a donor can select to mark the donation as "Gift Aide" for the purposes of reducing the tax burden on the NPO.
    */
    global Boolean stayclassy__Is_Gift_Aid_Eligible__c;
    global Boolean stayclassy__Is_a_Matched_Gift__c;
    global Double stayclassy__Parent_Transaction_ID__c;
    global Decimal stayclassy__Partial_Refund_Amount__c;
    /* The account number of the payment method used. In case of ACH, it will be the last four digits of the bank account number.
    */
    global String stayclassy__Payment_Account_Number__c;
    /* The account type of the payment method, such as checking or saving if ACH is used.
    */
    global String stayclassy__Payment_Account_Type__c;
    global Date stayclassy__Payment_End_Date__c;
    /* If checked, this field indicates that the donor has elected to pay the portion of Classy fees associated with the donation.
    */
    global Boolean stayclassy__Payment_Fee_On_Top__c;
    /* The name of the company that handles payment. In the case of ACH, it will be the bank name.
    */
    global String stayclassy__Payment_Institution__c;
    global String stayclassy__Raw_Currency_Code__c;
    global Double stayclassy__Raw_Donation_Gross_Amount__c;
    global Double stayclassy__Raw_Donation_Net_Amount__c;
    global Double stayclassy__Raw_Overhead_Net_Amount__c;
    global Double stayclassy__Raw_Total_Gross_Amount__c;
    global String stayclassy__Transaction_Metadata__c;
    global String stayclassy__Transaction_Status__c;
    global String Classy_Campaign_ID_to_text__c;
    global String Teacher_s_School_City__c;
    global String Teacher_s_School_State__c;
    global String Teacher_s_School_Zip__c;
    global String Teacher_External_ID__c;
    /* Choose donation type based on source of funding.
    */
    global String Revenue_Type__c;
    /* Date of check
    */
    global Date Check_Date__c;
    global String Disbursement_ID__c;
    /* The total amount of the check from which this donation funding comes
    */
    global Decimal Total_Check_Amount__c;
    global Boolean Teacher_Mailed_Classroom_Cards__c;
    global Boolean Teacher_Sent__c;
    global String donor_street__c;
    global String Donor_mailing_city__c;
    global String Donor_mailing_state__c;
    global String Donor_mailing_zip__c;
    /* Denotes whether this Classroom Adoption is part of a disbursement of funds from an earlier donation
    */
    global Boolean Funds_Disbursement__c;
    global String Primary_Contact_ID__c;
    /* The date the donation was received by the organization
    */
    global Date Donation_Date__c;
    global Date Contract_start__c;
    global Date Contract_End__c;
    global String Fund__c;
    global Date Write_Off_Date__c;
    global String Write_Off_Notes__c;
    global String Frequency_of_Gift__c;
    /* Day of week, month, quarter, or year the recurring gift is given
    */
    global String Day_of_Recurring_Gift__c;
    global String School_Legacy__c;
    global String Teacher_Email_Legacy__c;
    global String Teacher_Legacy__c;
    global String School_City_Legacy__c;
    global String School_State_Legacy__c;
    global String School_Zip_Legacy__c;
    global Boolean Private_School_Legacy__c;
    global String Reason_for_Donation_Legacy__c;
    global Decimal Estimated_Total__c;
    global String Contact_Record_Type__c;
    global String Items__c;
    global ChargentOrders__ChargentOrder__c Chargent_Order__r;
    global Id Chargent_Order__c;
    global donation_split__Designation__c Classroom__r;
    global Id Classroom__c;
    /* The Fee On Top percentage applied to each Classy Transaction
    */
    global Double stayclassy__Applied_FOT_Percent__c;
    global String stayclassy__Dedication_First_Name__c;
    global String stayclassy__Dedication_Last_Name__c;
    /* Dedication Honoree First Name
    */
    global String stayclassy__Honoree_First_Name__c;
    /* Dedication Honoree Last Name
    */
    global String stayclassy__Honoree_Last_Name__c;
    global String Donor_s_Affiliation__c;
    global String Promo_Code__c;
    global String Resubmitted__c;
    global Boolean Teacher_s_School_SW_Title_1__c;
    global Date Phone_Call_1__c;
    global Date Phone_Call_2__c;
    global Date Close_Date_for_Reports__c;
    global String Tribute__c;
    global String Fiscal_Month__c;
    global Boolean PersonalThankYou__c;
    global Boolean Teacher_s_School_Title_1__c;
    global Boolean Use_for_marketing__c;
    global Boolean ExpDateToday__c;
    global Double Expiration_Notice_Due__c;
    global Boolean FundsExpNot_1__c;
    global Boolean FundsExpNot_2__c;
    global Boolean FundsExpNot_3__c;
    global Boolean FundsExpNot__c;
    global Decimal Funds_Expiration_Amount__c;
    global String Teacher_or_School_Admin__c;
    global Boolean Do_not_process__c;
    global Opportunity Expired_Opportunity__r;
    global Id Expired_Opportunity__c;
    global String Expired_Opportunity_Record_ID__c;
    global String Donor_Record_Type__c;
    global String Teacher_s_School_Phone__c;
    global Datetime Last_Expiring_Fund_Notification_Date__c;
    global Date Expired_Opportunity_Expiration_Date__c;
    global Boolean Expired_Opportunity_Dates_Match__c;
    global Boolean Teacher_Reviewed__c;
    global Boolean Reallocated_Funds__c;
    global Date Monthly_Giving_Start_Date__c;
    global Date Monthly_Giving_End_Date__c;
    global String One_Time_or_Monthly__c;
    global String stayclassy__Classy_Member_Name2__c;
    global String Order_School_City__c;
    global String Order_School_Country__c;
    global Boolean Order_School_SW_Title_1__c;
    global String Order_School_State__c;
    global String Order_School_Street__c;
    global Boolean Order_School_Title_1__c;
    global String Order_School_Zip__c;
    global String Order_School__c;
    /* Fill in the name of the teacher or school admin that funds are being transferred to or from.
    */
    global Contact Funds_Transferred_To_From__r;
    /* Fill in the name of the teacher or school admin that funds are being transferred to or from.
    */
    global Id Funds_Transferred_To_From__c;
    global Decimal Amount_W_Feed__c;
    global String Disbursement_Name__c;
    /* Check this is you do not want this transaction to show up on the teacher or school's fundraising page.
    */
    global Boolean Hide_Transaction_on_Profile__c;
    global Boolean Expired_Opportunity_Reallocated__c;
    global Boolean Auto_Posted__c;
    /* Summary of the Opportunity Product Line’s' Tax Amount fields
    */
    global Decimal AcctSeed__Tax_Amount__c;
    /* Grand Total for Opportunity.
    */
    global Decimal AcctSeed__Total_with_Tax__c;
    global List<AccountPartner> AccountPartners;
    global List<AcctSeedERP__Purchase_Order__c> Purchase_Order__r;
    global List<AcctSeedERP__Sales_Order__c> AcctSeedERP__Sales_Order__r;
    global List<AcctSeed__Billing__c> AcctSeed__Billings__r;
    global List<AcctSeed__Cash_Receipt__c> Cash_Receipts__r;
    global List<AcctSeed__Journal_Entry_Line__c> Journal_Entry_Lines__r;
    global List<AcctSeed__Journal_Entry__c> Journal_Entries__r;
    global List<AcctSeed__Project_Task__c> Project_Tasks__r;
    global List<AcctSeed__Project__c> AcctSeed__Projects__r;
    global List<AcctSeed__Recurring_Billing__c> AcctSeed__Recurring_Billings__r;
    global List<AcctSeed__Transaction__c> AS_Transactions__r;
    global List<ActivityHistory> ActivityHistories;
    global List<Announcement__c> Announcements__r;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<Community_Opportunity__c> Community_Opportunities__r;
    global List<Contact> Contacts__r;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<Donor_Cart_Item__c> Donor_Cart_Items__r;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<Expired_Fund_Contact_Update__c> Expired_Fund_Contact_Updates__r;
    global List<GoogleDoc> GoogleDocs;
    global List<NetworkActivityAudit> ParentEntities;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<Opportunity> Opportunities__r;
    global List<Opportunity> npsp__MatchedGifts__r;
    global List<Opportunity> npsp__Previous_Grant_Opportunities__r;
    global List<OpportunityCompetitor> OpportunityCompetitors;
    global List<OpportunityContactRole> OpportunityContactRoles;
    global List<OpportunityFeed> Feeds;
    global List<OpportunityFieldHistory> Histories;
    global List<OpportunityHistory> OpportunityHistories;
    global List<OpportunityLineItem> OpportunityLineItems;
    global List<OpportunityPartner> OpportunityPartnersFrom;
    global List<OpportunityShare> Shares;
    global List<Partner> Partners;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<SurveySubject> SurveySubjectEntities;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global List<donation_split__Designation_Budget__c> donation_split__Designation_Budgets__r;
    global List<npe01__OppPayment__c> npe01__OppPayment__r;
    global List<npsp__Account_Soft_Credit__c> npsp__Account_Soft_Credits__r;
    global List<npsp__Allocation__c> npsp__Allocations__r;
    global List<npsp__DataImport__c> npsp__Data_Imports__r;
    global List<npsp__Grant_Deadline__c> npsp__Grant_Deadlines__r;
    global List<npsp__Partial_Soft_Credit__c> npsp__Partial_Soft_Credits__r;
    global List<pymt__PaymentX__c> pymt__Payments__r;
    global List<pymt__Payment_Profile__c> pymt__Payment_Profiles__r;
    global List<pymt__Shopping_Cart_Item__c> Shopping_Cart_Items__r;
    global List<stayclassy__Cash_Ledger__c> stayclassy__Cash_Ledger__r;
    global List<stayclassy__Classy_API_Process_Link__c> stayclassy__Classy_API_Process_Links_Opportunity__r;
    global List<stayclassy__Classy_Custom_Answer__c> stayclassy__Classy_Custom_Answers__r;
    global List<stayclassy__Classy_Related_Entity__c> stayclassy__Classy_Related_Entities__r;
    global List<stayclassy__Classy_Source_Code__c> stayclassy__Classy_Source_Codes__r;
    global List<stayclassy__Deferred_Transaction__c> stayclassy__Deferred_Transactions__r;
    global List<ContentDistribution> RelatedRecord;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<Lead> ConvertedOpportunity;
    global List<OpportunityContactRoleChangeEvent> Opportunity;
    global List<OutgoingEmail> RelatedTo;
    global List<TaskChangeEvent> What;

    global Opportunity () 
    {
    }
}