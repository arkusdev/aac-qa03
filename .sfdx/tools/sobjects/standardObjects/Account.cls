// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Account {
    global Id Id;
    global Boolean IsDeleted;
    global Account MasterRecord;
    global Id MasterRecordId;
    global String Name;
    global String Type;
    global RecordType RecordType;
    global Id RecordTypeId;
    global Account Parent;
    global Id ParentId;
    global String BillingStreet;
    global String BillingCity;
    global String BillingState;
    global String BillingPostalCode;
    global String BillingCountry;
    global String BillingStateCode;
    global String BillingCountryCode;
    global Double BillingLatitude;
    global Double BillingLongitude;
    global String BillingGeocodeAccuracy;
    global Address BillingAddress;
    global String ShippingStreet;
    global String ShippingCity;
    global String ShippingState;
    global String ShippingPostalCode;
    global String ShippingCountry;
    global String ShippingStateCode;
    global String ShippingCountryCode;
    global Double ShippingLatitude;
    global Double ShippingLongitude;
    global String ShippingGeocodeAccuracy;
    global Address ShippingAddress;
    global String Phone;
    global String Fax;
    global String AccountNumber;
    global String Website;
    global String PhotoUrl;
    global String Sic;
    global String Industry;
    global Decimal AnnualRevenue;
    global Integer NumberOfEmployees;
    global String Ownership;
    global String TickerSymbol;
    global String Description;
    global String Rating;
    global String Site;
    global User Owner;
    global Id OwnerId;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global Boolean IsCustomerPortal;
    global String Jigsaw;
    global String JigsawCompanyId;
    global String AccountSource;
    global String SicDesc;
    global Contact npe01__One2OneContact__r;
    global Id npe01__One2OneContact__c;
    global Boolean npe01__SYSTEMIsIndividual__c;
    global Date npe01__FirstDonationDate__c;
    global Date npe01__LastDonationDate__c;
    global Decimal npe01__LifetimeDonationHistory_Amount__c;
    global Double npe01__LifetimeDonationHistory_Number__c;
    global String Email__c;
    /* Indicates which Account Model this account is for: Household Account, One-to-One, or Individual (bucket).
    */
    global String npe01__SYSTEM_AccountType__c;
    global Decimal npo02__AverageAmount__c;
    global Decimal npo02__Best_Gift_Year_Total__c;
    global String npo02__Best_Gift_Year__c;
    global Date npo02__FirstCloseDate__c;
    /* For Household Accounts, uses fields of non-excluded, ordered related Contacts. Generated automatically.
    */
    global String npo02__Formal_Greeting__c;
    global String npo02__HouseholdPhone__c;
    /* For Household Accounts, uses fields of non-excluded, ordered related Contacts. Generated automatically.
    */
    global String npo02__Informal_Greeting__c;
    global Decimal npo02__LargestAmount__c;
    global Date npo02__LastCloseDate__c;
    global Decimal npo02__LastMembershipAmount__c;
    global Date npo02__LastMembershipDate__c;
    global String npo02__LastMembershipLevel__c;
    global String npo02__LastMembershipOrigin__c;
    global Decimal npo02__LastOppAmount__c;
    global Date npo02__MembershipEndDate__c;
    global Date npo02__MembershipJoinDate__c;
    global Double npo02__NumberOfClosedOpps__c;
    global Double npo02__NumberOfMembershipOpps__c;
    global Decimal npo02__OppAmount2YearsAgo__c;
    global Decimal npo02__OppAmountLastNDays__c;
    global Decimal npo02__OppAmountLastYear__c;
    global Decimal npo02__OppAmountThisYear__c;
    global Double npo02__OppsClosed2YearsAgo__c;
    global Double npo02__OppsClosedLastNDays__c;
    global Double npo02__OppsClosedLastYear__c;
    global Double npo02__OppsClosedThisYear__c;
    /* For Household Accounts, specifies which name related fields should not be automatically populated by the Householding code.
    */
    global String npo02__SYSTEM_CUSTOM_NAMING__c;
    global Decimal npo02__SmallestAmount__c;
    global Decimal npo02__TotalMembershipOppAmount__c;
    global Decimal npo02__TotalOppAmount__c;
    /* The batch this Account was created in.
    */
    global npsp__Batch__c npsp__Batch__r;
    /* The batch this Account was created in.
    */
    global Id npsp__Batch__c;
    /* The general program area(s) that this organization funds.
    */
    global String npsp__Funding_Focus__c;
    /* Indicates that this organization is a grant making organization. Can apply to foundations, government agencies, corporations, etc.
    */
    global Boolean npsp__Grantmaker__c;
    /* The name of the company's matching gift administrator.
    */
    global String npsp__Matching_Gift_Administrator_Name__c;
    /* The maximum amount the company will match on any single gift.
    */
    global Decimal npsp__Matching_Gift_Amount_Max__c;
    /* The smallest gift the company will match.
    */
    global Decimal npsp__Matching_Gift_Amount_Min__c;
    /* The maximum amount the company will match each year for an employee.
    */
    global Decimal npsp__Matching_Gift_Annual_Employee_Max__c;
    /* Additional notes about the company's matching gift program.
    */
    global String npsp__Matching_Gift_Comments__c;
    /* Is this a company that offers matching gifts?
    */
    global Boolean npsp__Matching_Gift_Company__c;
    /* Email of the company's matching gift administrator.
    */
    global String npsp__Matching_Gift_Email__c;
    /* The date when the matching gift information was last updated.
    */
    global Date npsp__Matching_Gift_Info_Updated__c;
    /* The portion of a gift the company will match.
    */
    global Double npsp__Matching_Gift_Percent__c;
    /* Phone for the matching gift administration office.
    */
    global String npsp__Matching_Gift_Phone__c;
    /* Any restrictions on how old gifts can be to be eligible for matching.
    */
    global String npsp__Matching_Gift_Request_Deadline__c;
    /* Formula: The number of years for which this Household has had a Member.
    */
    global Double npsp__Membership_Span__c;
    /* Formula: Current, expired, or grace period.  Default grace period set in Household Settings.
    */
    global String npsp__Membership_Status__c;
    /* for Household Accounts, the number of Contacts in the Household.
    */
    global Double npsp__Number_of_Household_Members__c;
    global Boolean Charter__c;
    global Double Discount__c;
    global String External_ID__c;
    global Decimal Flat_Fee__c;
    global String High_Grade__c;
    global String Level__c;
    global String Low_Grade__c;
    global Decimal Minimum_Purchase__c;
    global String NCESSCHID__c;
    global Boolean SW_Title_1__c;
    global Decimal Shipping_Handling_Fee__c;
    global Boolean Title_1__c;
    /* Grace period customers give until payment is due. If you are Net 30, put 30 in this field. If you are a Due Upon Receipt customer, put zero to make the issue and due date be the same.
    */
    global Double AcctSeed__Account_Payable_Terms__c;
    /* Indicates account is eligible for use in accounting application.
    */
    global Boolean AcctSeed__Accounting_Active__c;
    /* Used in filtered lookups for Accounts Payable and Billing screens to limit selection of Accounts to those marked as Customers or Vendors for use by Accounting.
    */
    global String AcctSeed__Accounting_Type__c;
    /* Used to configure formatting and display of fields in Activity Statement PDF. Overrides the default Activity Statement defined in Accounting Settings.
    */
    global AcctSeed__Billing_Format__c AcctSeed__Activity_Statement_Format__r;
    /* Used to configure formatting and display of fields in Activity Statement PDF. Overrides the default Activity Statement defined in Accounting Settings.
    */
    global Id AcctSeed__Activity_Statement_Format__c;
    /* Used as billing contact in Accounting Seed Billings.
    */
    global Contact AcctSeed__Billing_Contact__r;
    /* Used as billing contact in Accounting Seed Billings.
    */
    global Id AcctSeed__Billing_Contact__c;
    /* Used to calculate billing due date. Billing date minus Billing days offset equals due date.
    */
    global Double AcctSeed__Billing_Days_Due__c;
    /* Used to calculate payment due date for a prompt payment discount on a billing.
    */
    global Double AcctSeed__Billing_Discount_Days_Due__c;
    /* Used to determine the discount off the total price of a billing if payment is received within billing discount days due.
    */
    global Double AcctSeed__Billing_Discount_Percent__c;
    /* This field has been deprecated.
    */
    global String AcctSeed__Billing_Email__c;
    /* The name of the billing terms as you would like them to appear on the invoice.
    */
    global String AcctSeed__Billing_Terms_Name__c;
    /* This field denotes a credit card vendor. If checked this account will display in the credit card field filtered lookup on the expense object.
    */
    global Boolean AcctSeed__Credit_Card_Vendor__c;
    /* Marks default 1099 box for reporting cash disbursements to a 1099 vendor.
    */
    global String AcctSeed__Default_1099_Box__c;
    /* If this field is populated and this account is selected as the vendor on an account payable, the account payable line Expense GL Account field will be populated with this value dynamically.
    */
    global AcctSeed__GL_Account__c AcctSeed__Default_Expense_GL_Account__r;
    /* If this field is populated and this account is selected as the vendor on an account payable, the account payable line Expense GL Account field will be populated with this value dynamically.
    */
    global Id AcctSeed__Default_Expense_GL_Account__c;
    /* Used in the calculation of payment discounts in the accounts payable process. A discount will be automatically taken in the cash disbursements process if payment is made within the discount days due of the issue date.
    */
    global Double AcctSeed__Discount_Days_Due__c;
    /* The discount percentage of an account payable that a vendor provides as part of prompt payment terms.
    */
    global Double AcctSeed__Discount_Percent__c;
    /* Used to match the vendor account to a specific file import related transaction. This field is only used when importing financial files such as a bank statement via the Import Financial File tab.
    */
    global String AcctSeed__File_Import_Match_Name__c;
    /* Used to configure formatting and display of fields in Outstanding Statement PDF. Overrides the default Outstanding Statement defined in Accounting Settings.
    */
    global AcctSeed__Billing_Format__c AcctSeed__Outstanding_Statement_Format__r;
    /* Used to configure formatting and display of fields in Outstanding Statement PDF. Overrides the default Outstanding Statement defined in Accounting Settings.
    */
    global Id AcctSeed__Outstanding_Statement_Format__c;
    global Contact AcctSeed__Shipping_Contact__r;
    global Id AcctSeed__Shipping_Contact__c;
    /* Marks vendor to report cash disbursements on 1099-Misc form.
    */
    global Boolean AcctSeed__X1099_Vendor__c;
    global Double Donations_Received__c;
    global String NCES_District_ID__c;
    global String NCES_School_ID__c;
    global String Tax_ID__c;
    global Decimal Total_Funds_Raised__c;
    global String Usual_Account_Name__c;
    /* Cannot Edit. This is a formula that pulls from "District". If empty, that means no district is linked, or missing MCH data point.
    */
    global String ELL_District__c;
    global String stayclassy__Active__c;
    global Double stayclassy__NumberofLocations__c;
    global String Account_Status__c;
    global String School_Type__c;
    global Boolean Verified__c;
    /* PaymentConnect checks this box when auto-creating a Contact and an 'Individual Account' that should be converted to a PersonAccount using a batch converter.
    */
    global Boolean pymt__Convert_To_Person_Account__c;
    /* Alternate payee name which can be used to populate the payee field on a cash disbursement.
    */
    global String AcctSeed__Alternate_Payee_Name__c;
    /* Used to configure formatting and display of fields in Billing PDF. Overrides the default Billing Format defined in Accounting Settings.
    */
    global AcctSeed__Billing_Format__c AcctSeed__Billing_Format__r;
    /* Used to configure formatting and display of fields in Billing PDF. Overrides the default Billing Format defined in Accounting Settings.
    */
    global Id AcctSeed__Billing_Format__c;
    /* Traditional Sub-General Ledger account segment used to report on divisions, departments, geographies, cost centers, profit centers or business units.
    */
    global AcctSeed__Accounting_Variable__c AcctSeed__GL_Account_Variable_1__r;
    /* Traditional Sub-General Ledger account segment used to report on divisions, departments, geographies, cost centers, profit centers or business units.
    */
    global Id AcctSeed__GL_Account_Variable_1__c;
    /* Traditional Sub-General Ledger account segment used to report on divisions, departments, geographies, cost centers, profit centers or business units.
    */
    global AcctSeed__Accounting_Variable__c AcctSeed__GL_Account_Variable_2__r;
    /* Traditional Sub-General Ledger account segment used to report on divisions, departments, geographies, cost centers, profit centers or business units.
    */
    global Id AcctSeed__GL_Account_Variable_2__c;
    /* Traditional Sub-General Ledger account segment used to report on divisions, departments, geographies, cost centers, profit centers or business units.
    */
    global AcctSeed__Accounting_Variable__c AcctSeed__GL_Account_Variable_3__r;
    /* Traditional Sub-General Ledger account segment used to report on divisions, departments, geographies, cost centers, profit centers or business units.
    */
    global Id AcctSeed__GL_Account_Variable_3__c;
    /* Traditional Sub-General Ledger account segment used to report on divisions, departments, geographies, cost centers, profit centers or business units.
    */
    global AcctSeed__Accounting_Variable__c AcctSeed__GL_Account_Variable_4__r;
    /* Traditional Sub-General Ledger account segment used to report on divisions, departments, geographies, cost centers, profit centers or business units.
    */
    global Id AcctSeed__GL_Account_Variable_4__c;
    global String AcctSeed__Stripe_Customer_Id__c;
    global String AcctSeed__Taxpayer_Identification_Number__c;
    global Boolean dupcheck__dc3DisableDuplicateCheck__c;
    global String dupcheck__dc3Index__c;
    global Double zaapit__Hierarchy_Level__c;
    global String Order_Cloud_Vendor_Name__c;
    global String Parent_Account__c;
    global String State_Postal_Code__c;
    global String Donor_Display_name__c;
    global String Affil_state_postal_code__c;
    global String Invoice_vendor_name__c;
    global String Assigned_Manager__c;
    global String No_of_Employees_HEPData__c;
    /* Used to indicate schools that should not be selected for corporate match programs
    */
    global Boolean Do_Not_Match_for_Corporate__c;
    global String Company_Revenue_HEPData__c;
    /* Used to indicate schools that should be chosen for partnership opportunities based on past participation
    */
    global Boolean Preferred_Partner_School__c;
    global String Industry__c;
    global String Sector_HEPData__c;
    /* If specified, in conjunction with the Routing Number, will be used by Checkbook.io plugin to send direct deposit checks.
    */
    global String DigitalCheck__Account_Number__c;
    /* If specified, in conjunction with the Account Number, will be used by Checkbook.io plugin to send direct deposit checks.
    */
    global String DigitalCheck__Routing_Number__c;
    /* Ignore - For reporting use only
    */
    global Double Power_of_1__c;
    global String Metro_Status__c;
    global String Latitude__c;
    global String Longitude__c;
    global String Parent_Institution_ID__c;
    global npe5__Affiliation__c Parent_Institution_Name__r;
    global Id Parent_Institution_Name__c;
    global String MCH_Institution_ID__c;
    global Double Enrollment_African_American__c;
    global Double Enrollment_Asian__c;
    global Double Enrollment_Caucasian__c;
    global Double Enrollment_Hispanic__c;
    global Double Enrollment_Native_American__c;
    global Double Student_Enrollment__c;
    global Double Student_to_Teacher_Ratio__c;
    global Double Number_of_Teachers__c;
    global String Grade_Span__c;
    /* Cannot Edit. This is a formula that pulls from "District". If empty, that means no district is linked, or missing MCH data point.
    */
    global String IEP_District__c;
    global Double AAC_High_Needs_Rating__c;
    global Boolean School_wide_Title_I_MCH__c;
    global Double Free_Reduced_Lunch_Percent__c;
    global String School_Types_MCH__c;
    global String MCH_Ed_Account__c;
    global Decimal Instructional_Expenditures_per_Pupil__c;
    global Double AAC_Demographics_Rating__c;
    global String ELL_Students__c;
    global String IEP_Students__c;
    global Double Number_of_Schools_in_District__c;
    global Double AAC_IEP_and_ELL_Rating__c;
    global Account District__r;
    global Id District__c;
    global String DS360oi__Active__c;
    global Boolean DS360oi__Background_Batch_Flag__c;
    global String DS360oi__CustomerPriority__c;
    global String DS360oi__DS_update__c;
    global Datetime DS360oi__DS_update_date__c;
    global String DS360oi__Middle_Initial_Name__c;
    global Double DS360oi__NumberofLocations__c;
    global Date DS360oi__SLAExpirationDate__c;
    global String DS360oi__SLASerialNumber__c;
    global String DS360oi__SLA__c;
    global String DS360oi__UpsellOpportunity__c;
    global List<Account> School__r;
    global List<Account> ChildAccounts;
    global List<AccountBrand> AccountBrands;
    global List<AccountContactRelation> AccountContactRelations;
    global List<AccountContactRole> AccountContactRoles;
    global List<AccountFeed> Feeds;
    global List<AccountHistory> Histories;
    global List<AccountPartner> AccountPartnersFrom;
    global List<AccountPartner> AccountPartnersTo;
    global List<AccountShare> Shares;
    global List<AcctSeedERP__Purchase_Order__c> AcctSeedERP__Purchase_Orders1__r;
    global List<AcctSeedERP__Purchase_Order__c> AcctSeedERP__Purchase_Orders__r;
    global List<AcctSeedERP__Sales_Order__c> AcctSeedERP__Sales_Order__r;
    global List<AcctSeedERP__Shipment__c> AcctSeedERP__Shipments__r;
    global List<AcctSeed__Account_Payable__c> AcctSeed__Project_Payable_Invoices__r;
    global List<AcctSeed__Account_Tax__c> AcctSeed__Account_Taxes__r;
    global List<AcctSeed__Billing__c> AcctSeed__Billings__r;
    global List<AcctSeed__Cash_Disbursement__c> AcctSeed__Cash_Disbursements__r;
    global List<AcctSeed__Cash_Receipt__c> AcctSeed__Cash_Receipts__r;
    global List<AcctSeed__Cash_Receipt__c> Cash_Receipts__r;
    global List<AcctSeed__Expense_Line__c> AcctSeed__Expenses__r;
    global List<AcctSeed__Journal_Entry_Line__c> AcctSeed__Journal_Entry_Lines__r;
    global List<AcctSeed__Payment_Method__c> AcctSeed__Payment_Methods__r;
    global List<AcctSeed__Project__c> AcctSeed__Projects__r;
    global List<AcctSeed__Recurring_Account_Payable__c> AcctSeed__Recurring_Accounts_Payable__r;
    global List<AcctSeed__Recurring_Billing__c> AcctSeed__Recurring_Billings__r;
    global List<AcctSeed__Scheduled_Revenue_Expense__c> AcctSeed__Scheduled_Revenue_Expenses__r;
    global List<AcctSeed__Transaction__c> AcctSeed__Transactions__r;
    global List<ActivityHistory> ActivityHistories;
    global List<Asset> Assets;
    global List<AssociatedLocation> AssociatedLocations;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<Case> Cases;
    global List<ChargentOrders__ChargentOrder__c> ChargentOrders__Orders__r;
    global List<ChargentOrders__ChargentOrder__c> Chargent_Orders__r;
    global List<ChargentOrders__Chargent_Automated_Collections__c> ChargentOrders__Chargent_Automated_Collections__r;
    global List<ChargentOrders__Transaction__c> ChargentOrders__Transactions__r;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<Contact> Contacts;
    global List<Contact> Contacts1__r;
    global List<Contact> Contacts__r;
    global List<Contact> npsp__PrimaryAffiliatedContacts__r;
    global List<ContactPointEmail> ContactPointEmails;
    global List<ContactPointPhone> ContactPointPhones;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<Contract> Contracts;
    global List<Control_Account_Balance__c> Control_Account_Balances__r;
    global List<DS360oi__DonorSearch_del_del__c> DS360oi__DonorSearch__r;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<GoogleDoc> GoogleDocs;
    global List<NetworkActivityAudit> ParentEntities;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<Opportunity> Opportunities;
    global List<Opportunity> Opportunities__r;
    global List<Opportunity> npsp__Opportunities__r;
    global List<OpportunityPartner> OpportunityPartnersTo;
    global List<Partner> PartnersFrom;
    global List<Partner> PartnersTo;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<Product2> AcctSeedERP__Products__r;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<SocialPersona> Personas;
    global List<SurveySubject> SurveySubjectEntities;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global List<User> Users;
    global List<donation_split__Designation__c> Designations__r;
    global List<donation_split__Designation__c> Designations1__r;
    global List<mch_ed__MCH_Ed_Account__c> mch_ed__MCH_Ed_Account__r;
    global List<npe03__Recurring_Donation__c> npe03__RecurringDonations__r;
    global List<npe5__Affiliation__c> npe5__Affiliations__r;
    global List<npsp__Account_Soft_Credit__c> npsp__Account_Soft_Credits__r;
    global List<npsp__Address__c> npsp__Addresses__r;
    global List<npsp__DataImport__c> npsp__Data_Imports__r;
    global List<npsp__DataImport__c> npsp__Data_Imports1__r;
    global List<npsp__DataImport__c> npsp__NPSP_Data_Imports__r;
    global List<pymt__PaymentX__c> pymt__Payments__r;
    global List<pymt__Payment_Profile__c> pymt__Payment_Profiles__r;
    global List<pymt__Processor_Connection__c> pymt__Processor_Connections__r;
    global List<stayclassy__Deferred_Transaction__c> stayclassy__Deferred_Transactions__r;
    global List<AccountChangeEvent> Parent;
    global List<AccountContactRoleChangeEvent> Account;
    global List<ContentDistribution> RelatedRecord;
    global List<ContentVersion> FirstPublishLocation;
    global List<ContractChangeEvent> Account;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<Lead> ConvertedAccount;
    global List<NetworkSelfRegistration> Account;
    global List<OutgoingEmail> RelatedTo;
    global List<Task> Account;
    global List<TaskChangeEvent> What;
    global List<UserChangeEvent> Account;
    global List<UserRole> PortalAccount;

    global Account () 
    {
    }
}