declare module "@salesforce/label/c.Cart_Subtotal" {
    var Cart_Subtotal: string;
    export default Cart_Subtotal;
}
declare module "@salesforce/label/c.ClassroomEmail1" {
    var ClassroomEmail1: string;
    export default ClassroomEmail1;
}
declare module "@salesforce/label/c.ClassroomEmail2" {
    var ClassroomEmail2: string;
    export default ClassroomEmail2;
}
declare module "@salesforce/label/c.Classroom_Created" {
    var Classroom_Created: string;
    export default Classroom_Created;
}
declare module "@salesforce/label/c.Cover_Fee_Text" {
    var Cover_Fee_Text: string;
    export default Cover_Fee_Text;
}
declare module "@salesforce/label/c.DuplicateUsernameMessage" {
    var DuplicateUsernameMessage: string;
    export default DuplicateUsernameMessage;
}
declare module "@salesforce/label/c.FAQ_A01" {
    var FAQ_A01: string;
    export default FAQ_A01;
}
declare module "@salesforce/label/c.FAQ_A02_S" {
    var FAQ_A02_S: string;
    export default FAQ_A02_S;
}
declare module "@salesforce/label/c.FAQ_A02_T" {
    var FAQ_A02_T: string;
    export default FAQ_A02_T;
}
declare module "@salesforce/label/c.FAQ_A03_S" {
    var FAQ_A03_S: string;
    export default FAQ_A03_S;
}
declare module "@salesforce/label/c.FAQ_A03_T" {
    var FAQ_A03_T: string;
    export default FAQ_A03_T;
}
declare module "@salesforce/label/c.FAQ_Q01" {
    var FAQ_Q01: string;
    export default FAQ_Q01;
}
declare module "@salesforce/label/c.FAQ_Q02" {
    var FAQ_Q02: string;
    export default FAQ_Q02;
}
declare module "@salesforce/label/c.FAQ_Q03" {
    var FAQ_Q03: string;
    export default FAQ_Q03;
}
declare module "@salesforce/label/c.Fee_Label" {
    var Fee_Label: string;
    export default Fee_Label;
}
declare module "@salesforce/label/c.Generate_Describe_Label" {
    var Generate_Describe_Label: string;
    export default Generate_Describe_Label;
}
declare module "@salesforce/label/c.Higher_Ed_Tools_Title" {
    var Higher_Ed_Tools_Title: string;
    export default Higher_Ed_Tools_Title;
}
declare module "@salesforce/label/c.Minimum_Donation_Error" {
    var Minimum_Donation_Error: string;
    export default Minimum_Donation_Error;
}
declare module "@salesforce/label/c.Missing_Fields" {
    var Missing_Fields: string;
    export default Missing_Fields;
}
declare module "@salesforce/label/c.Next_Button_Label" {
    var Next_Button_Label: string;
    export default Next_Button_Label;
}
declare module "@salesforce/label/c.Notify_Alert" {
    var Notify_Alert: string;
    export default Notify_Alert;
}
declare module "@salesforce/label/c.Previous_Button_Label" {
    var Previous_Button_Label: string;
    export default Previous_Button_Label;
}
declare module "@salesforce/label/c.Profile_Successfully_Saved" {
    var Profile_Successfully_Saved: string;
    export default Profile_Successfully_Saved;
}
declare module "@salesforce/label/c.Save_and_Exit_Button_Label" {
    var Save_and_Exit_Button_Label: string;
    export default Save_and_Exit_Button_Label;
}
declare module "@salesforce/label/c.Saving_Status_Text" {
    var Saving_Status_Text: string;
    export default Saving_Status_Text;
}
declare module "@salesforce/label/c.SchoolEmail1" {
    var SchoolEmail1: string;
    export default SchoolEmail1;
}
declare module "@salesforce/label/c.SchoolEmail2" {
    var SchoolEmail2: string;
    export default SchoolEmail2;
}
declare module "@salesforce/label/c.School_Created" {
    var School_Created: string;
    export default School_Created;
}
declare module "@salesforce/label/c.Total_Label" {
    var Total_Label: string;
    export default Total_Label;
}
declare module "@salesforce/label/c.closed_lost_Opportunities" {
    var closed_lost_Opportunities: string;
    export default closed_lost_Opportunities;
}