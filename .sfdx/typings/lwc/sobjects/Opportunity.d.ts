declare module "@salesforce/schema/Opportunity.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Opportunity.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Opportunity.Account" {
  const Account:any;
  export default Account;
}
declare module "@salesforce/schema/Opportunity.AccountId" {
  const AccountId:any;
  export default AccountId;
}
declare module "@salesforce/schema/Opportunity.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/Opportunity.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/Opportunity.IsPrivate" {
  const IsPrivate:boolean;
  export default IsPrivate;
}
declare module "@salesforce/schema/Opportunity.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Opportunity.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Opportunity.StageName" {
  const StageName:string;
  export default StageName;
}
declare module "@salesforce/schema/Opportunity.Amount" {
  const Amount:number;
  export default Amount;
}
declare module "@salesforce/schema/Opportunity.Probability" {
  const Probability:number;
  export default Probability;
}
declare module "@salesforce/schema/Opportunity.ExpectedRevenue" {
  const ExpectedRevenue:number;
  export default ExpectedRevenue;
}
declare module "@salesforce/schema/Opportunity.TotalOpportunityQuantity" {
  const TotalOpportunityQuantity:number;
  export default TotalOpportunityQuantity;
}
declare module "@salesforce/schema/Opportunity.CloseDate" {
  const CloseDate:any;
  export default CloseDate;
}
declare module "@salesforce/schema/Opportunity.Type" {
  const Type:string;
  export default Type;
}
declare module "@salesforce/schema/Opportunity.NextStep" {
  const NextStep:string;
  export default NextStep;
}
declare module "@salesforce/schema/Opportunity.LeadSource" {
  const LeadSource:string;
  export default LeadSource;
}
declare module "@salesforce/schema/Opportunity.IsClosed" {
  const IsClosed:boolean;
  export default IsClosed;
}
declare module "@salesforce/schema/Opportunity.IsWon" {
  const IsWon:boolean;
  export default IsWon;
}
declare module "@salesforce/schema/Opportunity.ForecastCategory" {
  const ForecastCategory:string;
  export default ForecastCategory;
}
declare module "@salesforce/schema/Opportunity.ForecastCategoryName" {
  const ForecastCategoryName:string;
  export default ForecastCategoryName;
}
declare module "@salesforce/schema/Opportunity.Campaign" {
  const Campaign:any;
  export default Campaign;
}
declare module "@salesforce/schema/Opportunity.CampaignId" {
  const CampaignId:any;
  export default CampaignId;
}
declare module "@salesforce/schema/Opportunity.HasOpportunityLineItem" {
  const HasOpportunityLineItem:boolean;
  export default HasOpportunityLineItem;
}
declare module "@salesforce/schema/Opportunity.Pricebook2" {
  const Pricebook2:any;
  export default Pricebook2;
}
declare module "@salesforce/schema/Opportunity.Pricebook2Id" {
  const Pricebook2Id:any;
  export default Pricebook2Id;
}
declare module "@salesforce/schema/Opportunity.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Opportunity.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Opportunity.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Opportunity.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Opportunity.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Opportunity.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Opportunity.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Opportunity.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Opportunity.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Opportunity.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Opportunity.FiscalQuarter" {
  const FiscalQuarter:number;
  export default FiscalQuarter;
}
declare module "@salesforce/schema/Opportunity.FiscalYear" {
  const FiscalYear:number;
  export default FiscalYear;
}
declare module "@salesforce/schema/Opportunity.Fiscal" {
  const Fiscal:string;
  export default Fiscal;
}
declare module "@salesforce/schema/Opportunity.Contact" {
  const Contact:any;
  export default Contact;
}
declare module "@salesforce/schema/Opportunity.ContactId" {
  const ContactId:any;
  export default ContactId;
}
declare module "@salesforce/schema/Opportunity.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Opportunity.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Opportunity.Contract" {
  const Contract:any;
  export default Contract;
}
declare module "@salesforce/schema/Opportunity.ContractId" {
  const ContractId:any;
  export default ContractId;
}
declare module "@salesforce/schema/Opportunity.HasOpenActivity" {
  const HasOpenActivity:boolean;
  export default HasOpenActivity;
}
declare module "@salesforce/schema/Opportunity.HasOverdueTask" {
  const HasOverdueTask:boolean;
  export default HasOverdueTask;
}
declare module "@salesforce/schema/Opportunity.npe03__Recurring_Donation__r" {
  const npe03__Recurring_Donation__r:any;
  export default npe03__Recurring_Donation__r;
}
declare module "@salesforce/schema/Opportunity.npe03__Recurring_Donation__c" {
  const npe03__Recurring_Donation__c:any;
  export default npe03__Recurring_Donation__c;
}
declare module "@salesforce/schema/Opportunity.npo02__CurrentGenerators__c" {
  const npo02__CurrentGenerators__c:string;
  export default npo02__CurrentGenerators__c;
}
declare module "@salesforce/schema/Opportunity.npo02__DeliveryInstallationStatus__c" {
  const npo02__DeliveryInstallationStatus__c:string;
  export default npo02__DeliveryInstallationStatus__c;
}
declare module "@salesforce/schema/Opportunity.npo02__MainCompetitors__c" {
  const npo02__MainCompetitors__c:string;
  export default npo02__MainCompetitors__c;
}
declare module "@salesforce/schema/Opportunity.npo02__OrderNumber__c" {
  const npo02__OrderNumber__c:string;
  export default npo02__OrderNumber__c;
}
declare module "@salesforce/schema/Opportunity.npo02__TrackingNumber__c" {
  const npo02__TrackingNumber__c:string;
  export default npo02__TrackingNumber__c;
}
declare module "@salesforce/schema/Opportunity.Target_Area_s__c" {
  const Target_Area_s__c:string;
  export default Target_Area_s__c;
}
declare module "@salesforce/schema/Opportunity.No_of_Teachers__c" {
  const No_of_Teachers__c:number;
  export default No_of_Teachers__c;
}
declare module "@salesforce/schema/Opportunity.Development_Partner_Type__c" {
  const Development_Partner_Type__c:string;
  export default Development_Partner_Type__c;
}
declare module "@salesforce/schema/Opportunity.No_of_Prospective_Donors__c" {
  const No_of_Prospective_Donors__c:number;
  export default No_of_Prospective_Donors__c;
}
declare module "@salesforce/schema/Opportunity.npe01__Amount_Outstanding__c" {
  const npe01__Amount_Outstanding__c:number;
  export default npe01__Amount_Outstanding__c;
}
declare module "@salesforce/schema/Opportunity.npe01__Contact_Id_for_Role__c" {
  const npe01__Contact_Id_for_Role__c:string;
  export default npe01__Contact_Id_for_Role__c;
}
declare module "@salesforce/schema/Opportunity.npe01__Do_Not_Automatically_Create_Payment__c" {
  const npe01__Do_Not_Automatically_Create_Payment__c:boolean;
  export default npe01__Do_Not_Automatically_Create_Payment__c;
}
declare module "@salesforce/schema/Opportunity.npe01__Is_Opp_From_Individual__c" {
  const npe01__Is_Opp_From_Individual__c:string;
  export default npe01__Is_Opp_From_Individual__c;
}
declare module "@salesforce/schema/Opportunity.npe01__Member_Level__c" {
  const npe01__Member_Level__c:string;
  export default npe01__Member_Level__c;
}
declare module "@salesforce/schema/Opportunity.npe01__Membership_End_Date__c" {
  const npe01__Membership_End_Date__c:any;
  export default npe01__Membership_End_Date__c;
}
declare module "@salesforce/schema/Opportunity.npe01__Membership_Origin__c" {
  const npe01__Membership_Origin__c:string;
  export default npe01__Membership_Origin__c;
}
declare module "@salesforce/schema/Opportunity.npe01__Membership_Start_Date__c" {
  const npe01__Membership_Start_Date__c:any;
  export default npe01__Membership_Start_Date__c;
}
declare module "@salesforce/schema/Opportunity.npe01__Amount_Written_Off__c" {
  const npe01__Amount_Written_Off__c:number;
  export default npe01__Amount_Written_Off__c;
}
declare module "@salesforce/schema/Opportunity.npe01__Number_of_Payments__c" {
  const npe01__Number_of_Payments__c:number;
  export default npe01__Number_of_Payments__c;
}
declare module "@salesforce/schema/Opportunity.npe01__Payments_Made__c" {
  const npe01__Payments_Made__c:number;
  export default npe01__Payments_Made__c;
}
declare module "@salesforce/schema/Opportunity.npo02__CombinedRollupFieldset__c" {
  const npo02__CombinedRollupFieldset__c:string;
  export default npo02__CombinedRollupFieldset__c;
}
declare module "@salesforce/schema/Opportunity.npo02__systemHouseholdContactRoleProcessor__c" {
  const npo02__systemHouseholdContactRoleProcessor__c:string;
  export default npo02__systemHouseholdContactRoleProcessor__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Acknowledgment_Date__c" {
  const npsp__Acknowledgment_Date__c:any;
  export default npsp__Acknowledgment_Date__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Acknowledgment_Status__c" {
  const npsp__Acknowledgment_Status__c:string;
  export default npsp__Acknowledgment_Status__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Batch__r" {
  const npsp__Batch__r:any;
  export default npsp__Batch__r;
}
declare module "@salesforce/schema/Opportunity.npsp__Batch__c" {
  const npsp__Batch__c:any;
  export default npsp__Batch__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Fair_Market_Value__c" {
  const npsp__Fair_Market_Value__c:number;
  export default npsp__Fair_Market_Value__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Grant_Contract_Date__c" {
  const npsp__Grant_Contract_Date__c:any;
  export default npsp__Grant_Contract_Date__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Grant_Contract_Number__c" {
  const npsp__Grant_Contract_Number__c:string;
  export default npsp__Grant_Contract_Number__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Grant_Period_End_Date__c" {
  const npsp__Grant_Period_End_Date__c:any;
  export default npsp__Grant_Period_End_Date__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Grant_Period_Start_Date__c" {
  const npsp__Grant_Period_Start_Date__c:any;
  export default npsp__Grant_Period_Start_Date__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Grant_Program_Area_s__c" {
  const npsp__Grant_Program_Area_s__c:string;
  export default npsp__Grant_Program_Area_s__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Grant_Requirements_Website__c" {
  const npsp__Grant_Requirements_Website__c:string;
  export default npsp__Grant_Requirements_Website__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Honoree_Contact__r" {
  const npsp__Honoree_Contact__r:any;
  export default npsp__Honoree_Contact__r;
}
declare module "@salesforce/schema/Opportunity.npsp__Honoree_Contact__c" {
  const npsp__Honoree_Contact__c:any;
  export default npsp__Honoree_Contact__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Honoree_Name__c" {
  const npsp__Honoree_Name__c:string;
  export default npsp__Honoree_Name__c;
}
declare module "@salesforce/schema/Opportunity.npsp__In_Kind_Description__c" {
  const npsp__In_Kind_Description__c:string;
  export default npsp__In_Kind_Description__c;
}
declare module "@salesforce/schema/Opportunity.npsp__In_Kind_Donor_Declared_Value__c" {
  const npsp__In_Kind_Donor_Declared_Value__c:boolean;
  export default npsp__In_Kind_Donor_Declared_Value__c;
}
declare module "@salesforce/schema/Opportunity.npsp__In_Kind_Type__c" {
  const npsp__In_Kind_Type__c:string;
  export default npsp__In_Kind_Type__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Is_Grant_Renewal__c" {
  const npsp__Is_Grant_Renewal__c:boolean;
  export default npsp__Is_Grant_Renewal__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Matching_Gift_Account__r" {
  const npsp__Matching_Gift_Account__r:any;
  export default npsp__Matching_Gift_Account__r;
}
declare module "@salesforce/schema/Opportunity.npsp__Matching_Gift_Account__c" {
  const npsp__Matching_Gift_Account__c:any;
  export default npsp__Matching_Gift_Account__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Matching_Gift_Employer__c" {
  const npsp__Matching_Gift_Employer__c:string;
  export default npsp__Matching_Gift_Employer__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Matching_Gift_Status__c" {
  const npsp__Matching_Gift_Status__c:string;
  export default npsp__Matching_Gift_Status__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Matching_Gift__r" {
  const npsp__Matching_Gift__r:any;
  export default npsp__Matching_Gift__r;
}
declare module "@salesforce/schema/Opportunity.npsp__Matching_Gift__c" {
  const npsp__Matching_Gift__c:any;
  export default npsp__Matching_Gift__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Notification_Message__c" {
  const npsp__Notification_Message__c:string;
  export default npsp__Notification_Message__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Notification_Preference__c" {
  const npsp__Notification_Preference__c:string;
  export default npsp__Notification_Preference__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Notification_Recipient_Contact__r" {
  const npsp__Notification_Recipient_Contact__r:any;
  export default npsp__Notification_Recipient_Contact__r;
}
declare module "@salesforce/schema/Opportunity.npsp__Notification_Recipient_Contact__c" {
  const npsp__Notification_Recipient_Contact__c:any;
  export default npsp__Notification_Recipient_Contact__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Notification_Recipient_Information__c" {
  const npsp__Notification_Recipient_Information__c:string;
  export default npsp__Notification_Recipient_Information__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Notification_Recipient_Name__c" {
  const npsp__Notification_Recipient_Name__c:string;
  export default npsp__Notification_Recipient_Name__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Previous_Grant_Opportunity__r" {
  const npsp__Previous_Grant_Opportunity__r:any;
  export default npsp__Previous_Grant_Opportunity__r;
}
declare module "@salesforce/schema/Opportunity.npsp__Previous_Grant_Opportunity__c" {
  const npsp__Previous_Grant_Opportunity__c:any;
  export default npsp__Previous_Grant_Opportunity__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Primary_Contact_Campaign_Member_Status__c" {
  const npsp__Primary_Contact_Campaign_Member_Status__c:string;
  export default npsp__Primary_Contact_Campaign_Member_Status__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Primary_Contact__r" {
  const npsp__Primary_Contact__r:any;
  export default npsp__Primary_Contact__r;
}
declare module "@salesforce/schema/Opportunity.npsp__Primary_Contact__c" {
  const npsp__Primary_Contact__c:any;
  export default npsp__Primary_Contact__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Recurring_Donation_Installment_Name__c" {
  const npsp__Recurring_Donation_Installment_Name__c:string;
  export default npsp__Recurring_Donation_Installment_Name__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Recurring_Donation_Installment_Number__c" {
  const npsp__Recurring_Donation_Installment_Number__c:number;
  export default npsp__Recurring_Donation_Installment_Number__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Requested_Amount__c" {
  const npsp__Requested_Amount__c:number;
  export default npsp__Requested_Amount__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Tribute_Type__c" {
  const npsp__Tribute_Type__c:string;
  export default npsp__Tribute_Type__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Next_Grant_Deadline_Due_Date__c" {
  const npsp__Next_Grant_Deadline_Due_Date__c:any;
  export default npsp__Next_Grant_Deadline_Due_Date__c;
}
declare module "@salesforce/schema/Opportunity.AcctSeed__TrackingNumber__c" {
  const AcctSeed__TrackingNumber__c:string;
  export default AcctSeed__TrackingNumber__c;
}
declare module "@salesforce/schema/Opportunity.AdoptionDate__c" {
  const AdoptionDate__c:any;
  export default AdoptionDate__c;
}
declare module "@salesforce/schema/Opportunity.Anonymous__c" {
  const Anonymous__c:boolean;
  export default Anonymous__c;
}
declare module "@salesforce/schema/Opportunity.ConfirmationCode__c" {
  const ConfirmationCode__c:string;
  export default ConfirmationCode__c;
}
declare module "@salesforce/schema/Opportunity.Expiration_Date__c" {
  const Expiration_Date__c:any;
  export default Expiration_Date__c;
}
declare module "@salesforce/schema/Opportunity.External_ID__c" {
  const External_ID__c:string;
  export default External_ID__c;
}
declare module "@salesforce/schema/Opportunity.PledgeDate__c" {
  const PledgeDate__c:any;
  export default PledgeDate__c;
}
declare module "@salesforce/schema/Opportunity.ProjectTask__r" {
  const ProjectTask__r:any;
  export default ProjectTask__r;
}
declare module "@salesforce/schema/Opportunity.ProjectTask__c" {
  const ProjectTask__c:any;
  export default ProjectTask__c;
}
declare module "@salesforce/schema/Opportunity.Project__r" {
  const Project__r:any;
  export default Project__r;
}
declare module "@salesforce/schema/Opportunity.Project__c" {
  const Project__c:any;
  export default Project__c;
}
declare module "@salesforce/schema/Opportunity.Sponsorship__c" {
  const Sponsorship__c:boolean;
  export default Sponsorship__c;
}
declare module "@salesforce/schema/Opportunity.npsp__Closed_Lost_Reason__c" {
  const npsp__Closed_Lost_Reason__c:string;
  export default npsp__Closed_Lost_Reason__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Anonymous_Donor__c" {
  const stayclassy__Anonymous_Donor__c:boolean;
  export default stayclassy__Anonymous_Donor__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Billing_First_Name__c" {
  const stayclassy__Billing_First_Name__c:string;
  export default stayclassy__Billing_First_Name__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Billing_Last_Name__c" {
  const stayclassy__Billing_Last_Name__c:string;
  export default stayclassy__Billing_Last_Name__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Browser_Info__c" {
  const stayclassy__Browser_Info__c:string;
  export default stayclassy__Browser_Info__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Campaign__r" {
  const stayclassy__Campaign__r:any;
  export default stayclassy__Campaign__r;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Campaign__c" {
  const stayclassy__Campaign__c:any;
  export default stayclassy__Campaign__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_Custom_Answer__r" {
  const stayclassy__Classy_Custom_Answer__r:any;
  export default stayclassy__Classy_Custom_Answer__r;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_Custom_Answer__c" {
  const stayclassy__Classy_Custom_Answer__c:any;
  export default stayclassy__Classy_Custom_Answer__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_Order_Total__c" {
  const stayclassy__Classy_Order_Total__c:number;
  export default stayclassy__Classy_Order_Total__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Credit_Card_Expiration_Date__c" {
  const stayclassy__Credit_Card_Expiration_Date__c:string;
  export default stayclassy__Credit_Card_Expiration_Date__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Credit_Card_Last_Four_Digits__c" {
  const stayclassy__Credit_Card_Last_Four_Digits__c:string;
  export default stayclassy__Credit_Card_Last_Four_Digits__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Credit_Card_Type__c" {
  const stayclassy__Credit_Card_Type__c:string;
  export default stayclassy__Credit_Card_Type__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Dedication_Ecard_Request__c" {
  const stayclassy__Dedication_Ecard_Request__c:boolean;
  export default stayclassy__Dedication_Ecard_Request__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Dedication_Email_Body__c" {
  const stayclassy__Dedication_Email_Body__c:string;
  export default stayclassy__Dedication_Email_Body__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Device_Type__c" {
  const stayclassy__Device_Type__c:string;
  export default stayclassy__Device_Type__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Discount_Code__c" {
  const stayclassy__Discount_Code__c:string;
  export default stayclassy__Discount_Code__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Discount_Total__c" {
  const stayclassy__Discount_Total__c:number;
  export default stayclassy__Discount_Total__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Fundraising_Page__r" {
  const stayclassy__Fundraising_Page__r:any;
  export default stayclassy__Fundraising_Page__r;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Fundraising_Page__c" {
  const stayclassy__Fundraising_Page__c:any;
  export default stayclassy__Fundraising_Page__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Fundraising_Team__r" {
  const stayclassy__Fundraising_Team__r:any;
  export default stayclassy__Fundraising_Team__r;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Fundraising_Team__c" {
  const stayclassy__Fundraising_Team__c:any;
  export default stayclassy__Fundraising_Team__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Hide_Donation_Amount__c" {
  const stayclassy__Hide_Donation_Amount__c:boolean;
  export default stayclassy__Hide_Donation_Amount__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Hide_Donation_Comment__c" {
  const stayclassy__Hide_Donation_Comment__c:boolean;
  export default stayclassy__Hide_Donation_Comment__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Offline_Donation__c" {
  const stayclassy__Offline_Donation__c:boolean;
  export default stayclassy__Offline_Donation__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Organization_ID__c" {
  const stayclassy__Organization_ID__c:number;
  export default stayclassy__Organization_ID__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__PaymentID__c" {
  const stayclassy__PaymentID__c:string;
  export default stayclassy__PaymentID__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Payment_Method__c" {
  const stayclassy__Payment_Method__c:string;
  export default stayclassy__Payment_Method__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Transaction_Type__c" {
  const stayclassy__Transaction_Type__c:string;
  export default stayclassy__Transaction_Type__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__cc_address2__c" {
  const stayclassy__cc_address2__c:string;
  export default stayclassy__cc_address2__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__cc_address__c" {
  const stayclassy__cc_address__c:string;
  export default stayclassy__cc_address__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__cc_city__c" {
  const stayclassy__cc_city__c:string;
  export default stayclassy__cc_city__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__cc_country__c" {
  const stayclassy__cc_country__c:string;
  export default stayclassy__cc_country__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__cc_state__c" {
  const stayclassy__cc_state__c:string;
  export default stayclassy__cc_state__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__cc_zip__c" {
  const stayclassy__cc_zip__c:string;
  export default stayclassy__cc_zip__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__check_number__c" {
  const stayclassy__check_number__c:string;
  export default stayclassy__check_number__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__dedication_contact_address__c" {
  const stayclassy__dedication_contact_address__c:string;
  export default stayclassy__dedication_contact_address__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__dedication_contact_city__c" {
  const stayclassy__dedication_contact_city__c:string;
  export default stayclassy__dedication_contact_city__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__dedication_contact_country__c" {
  const stayclassy__dedication_contact_country__c:string;
  export default stayclassy__dedication_contact_country__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__dedication_contact_email__c" {
  const stayclassy__dedication_contact_email__c:string;
  export default stayclassy__dedication_contact_email__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__dedication_contact_name__c" {
  const stayclassy__dedication_contact_name__c:string;
  export default stayclassy__dedication_contact_name__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__dedication_contact_state__c" {
  const stayclassy__dedication_contact_state__c:string;
  export default stayclassy__dedication_contact_state__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__dedication_contact_zip__c" {
  const stayclassy__dedication_contact_zip__c:string;
  export default stayclassy__dedication_contact_zip__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__dedication_include_gift_amount__c" {
  const stayclassy__dedication_include_gift_amount__c:boolean;
  export default stayclassy__dedication_include_gift_amount__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__dedication_name__c" {
  const stayclassy__dedication_name__c:string;
  export default stayclassy__dedication_name__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__dedication_type__c" {
  const stayclassy__dedication_type__c:string;
  export default stayclassy__dedication_type__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__donation_total__c" {
  const stayclassy__donation_total__c:number;
  export default stayclassy__donation_total__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__gateway_name__c" {
  const stayclassy__gateway_name__c:string;
  export default stayclassy__gateway_name__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__gateway_transaction_id__c" {
  const stayclassy__gateway_transaction_id__c:string;
  export default stayclassy__gateway_transaction_id__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__host_total__c" {
  const stayclassy__host_total__c:number;
  export default stayclassy__host_total__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__order_date__c" {
  const stayclassy__order_date__c:any;
  export default stayclassy__order_date__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__refund_date__c" {
  const stayclassy__refund_date__c:any;
  export default stayclassy__refund_date__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__sc_order_id__c" {
  const stayclassy__sc_order_id__c:string;
  export default stayclassy__sc_order_id__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__sf_contact_id__r" {
  const stayclassy__sf_contact_id__r:any;
  export default stayclassy__sf_contact_id__r;
}
declare module "@salesforce/schema/Opportunity.stayclassy__sf_contact_id__c" {
  const stayclassy__sf_contact_id__c:any;
  export default stayclassy__sf_contact_id__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__sf_designation_id__r" {
  const stayclassy__sf_designation_id__r:any;
  export default stayclassy__sf_designation_id__r;
}
declare module "@salesforce/schema/Opportunity.stayclassy__sf_designation_id__c" {
  const stayclassy__sf_designation_id__c:any;
  export default stayclassy__sf_designation_id__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__sf_recurring_id__r" {
  const stayclassy__sf_recurring_id__r:any;
  export default stayclassy__sf_recurring_id__r;
}
declare module "@salesforce/schema/Opportunity.stayclassy__sf_recurring_id__c" {
  const stayclassy__sf_recurring_id__c:any;
  export default stayclassy__sf_recurring_id__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__transaction_total__c" {
  const stayclassy__transaction_total__c:number;
  export default stayclassy__transaction_total__c;
}
declare module "@salesforce/schema/Opportunity.DateOrderCreated__c" {
  const DateOrderCreated__c:any;
  export default DateOrderCreated__c;
}
declare module "@salesforce/schema/Opportunity.DateOrderSubmitted__c" {
  const DateOrderSubmitted__c:any;
  export default DateOrderSubmitted__c;
}
declare module "@salesforce/schema/Opportunity.IsSubmitted__c" {
  const IsSubmitted__c:boolean;
  export default IsSubmitted__c;
}
declare module "@salesforce/schema/Opportunity.Order_Cloud_Order_Id__c" {
  const Order_Cloud_Order_Id__c:string;
  export default Order_Cloud_Order_Id__c;
}
declare module "@salesforce/schema/Opportunity.PromotionDiscount__c" {
  const PromotionDiscount__c:number;
  export default PromotionDiscount__c;
}
declare module "@salesforce/schema/Opportunity.School__r" {
  const School__r:any;
  export default School__r;
}
declare module "@salesforce/schema/Opportunity.School__c" {
  const School__c:any;
  export default School__c;
}
declare module "@salesforce/schema/Opportunity.ShippingCost__c" {
  const ShippingCost__c:number;
  export default ShippingCost__c;
}
declare module "@salesforce/schema/Opportunity.Subtotal__c" {
  const Subtotal__c:number;
  export default Subtotal__c;
}
declare module "@salesforce/schema/Opportunity.TaxCost__c" {
  const TaxCost__c:number;
  export default TaxCost__c;
}
declare module "@salesforce/schema/Opportunity.Teacher__r" {
  const Teacher__r:any;
  export default Teacher__r;
}
declare module "@salesforce/schema/Opportunity.Teacher__c" {
  const Teacher__c:any;
  export default Teacher__c;
}
declare module "@salesforce/schema/Opportunity.pymt__Balance__c" {
  const pymt__Balance__c:number;
  export default pymt__Balance__c;
}
declare module "@salesforce/schema/Opportunity.pymt__Currency_ISO_Code__c" {
  const pymt__Currency_ISO_Code__c:string;
  export default pymt__Currency_ISO_Code__c;
}
declare module "@salesforce/schema/Opportunity.pymt__Frequency__c" {
  const pymt__Frequency__c:number;
  export default pymt__Frequency__c;
}
declare module "@salesforce/schema/Opportunity.pymt__Invoice_Number__c" {
  const pymt__Invoice_Number__c:string;
  export default pymt__Invoice_Number__c;
}
declare module "@salesforce/schema/Opportunity.pymt__Number_of_Payments_Made__c" {
  const pymt__Number_of_Payments_Made__c:number;
  export default pymt__Number_of_Payments_Made__c;
}
declare module "@salesforce/schema/Opportunity.pymt__Occurrences__c" {
  const pymt__Occurrences__c:number;
  export default pymt__Occurrences__c;
}
declare module "@salesforce/schema/Opportunity.pymt__PO_Number__c" {
  const pymt__PO_Number__c:string;
  export default pymt__PO_Number__c;
}
declare module "@salesforce/schema/Opportunity.pymt__Paid_Off__c" {
  const pymt__Paid_Off__c:boolean;
  export default pymt__Paid_Off__c;
}
declare module "@salesforce/schema/Opportunity.pymt__Payments_Made__c" {
  const pymt__Payments_Made__c:number;
  export default pymt__Payments_Made__c;
}
declare module "@salesforce/schema/Opportunity.pymt__Period__c" {
  const pymt__Period__c:string;
  export default pymt__Period__c;
}
declare module "@salesforce/schema/Opportunity.pymt__Recurring_Amount__c" {
  const pymt__Recurring_Amount__c:number;
  export default pymt__Recurring_Amount__c;
}
declare module "@salesforce/schema/Opportunity.pymt__Shipping__c" {
  const pymt__Shipping__c:number;
  export default pymt__Shipping__c;
}
declare module "@salesforce/schema/Opportunity.pymt__SiteQuote_Expiration__c" {
  const pymt__SiteQuote_Expiration__c:any;
  export default pymt__SiteQuote_Expiration__c;
}
declare module "@salesforce/schema/Opportunity.pymt__SiteQuote_Recurring_Setup__c" {
  const pymt__SiteQuote_Recurring_Setup__c:string;
  export default pymt__SiteQuote_Recurring_Setup__c;
}
declare module "@salesforce/schema/Opportunity.pymt__Site_Name__c" {
  const pymt__Site_Name__c:string;
  export default pymt__Site_Name__c;
}
declare module "@salesforce/schema/Opportunity.pymt__Subscription_Start_Date__c" {
  const pymt__Subscription_Start_Date__c:any;
  export default pymt__Subscription_Start_Date__c;
}
declare module "@salesforce/schema/Opportunity.pymt__Tax__c" {
  const pymt__Tax__c:number;
  export default pymt__Tax__c;
}
declare module "@salesforce/schema/Opportunity.pymt__Total_Amount__c" {
  const pymt__Total_Amount__c:number;
  export default pymt__Total_Amount__c;
}
declare module "@salesforce/schema/Opportunity.Donor_Address__c" {
  const Donor_Address__c:string;
  export default Donor_Address__c;
}
declare module "@salesforce/schema/Opportunity.Donor_Name__c" {
  const Donor_Name__c:string;
  export default Donor_Name__c;
}
declare module "@salesforce/schema/Opportunity.Honoree_Email__c" {
  const Honoree_Email__c:string;
  export default Honoree_Email__c;
}
declare module "@salesforce/schema/Opportunity.Public_Classroom_Landing_Page_URL__c" {
  const Public_Classroom_Landing_Page_URL__c:string;
  export default Public_Classroom_Landing_Page_URL__c;
}
declare module "@salesforce/schema/Opportunity.OrderCloudLineItemCount__c" {
  const OrderCloudLineItemCount__c:number;
  export default OrderCloudLineItemCount__c;
}
declare module "@salesforce/schema/Opportunity.Processing_Fee__c" {
  const Processing_Fee__c:boolean;
  export default Processing_Fee__c;
}
declare module "@salesforce/schema/Opportunity.Total_Amount__c" {
  const Total_Amount__c:number;
  export default Total_Amount__c;
}
declare module "@salesforce/schema/Opportunity.OpportunityProductCount__c" {
  const OpportunityProductCount__c:number;
  export default OpportunityProductCount__c;
}
declare module "@salesforce/schema/Opportunity.Honoree_message__c" {
  const Honoree_message__c:string;
  export default Honoree_message__c;
}
declare module "@salesforce/schema/Opportunity.Acknowledge_status__c" {
  const Acknowledge_status__c:string;
  export default Acknowledge_status__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_Thank_you_note_mailed__c" {
  const Teacher_Thank_you_note_mailed__c:boolean;
  export default Teacher_Thank_you_note_mailed__c;
}
declare module "@salesforce/schema/Opportunity.State_designation__c" {
  const State_designation__c:string;
  export default State_designation__c;
}
declare module "@salesforce/schema/Opportunity.Postal_Code_state__c" {
  const Postal_Code_state__c:string;
  export default Postal_Code_state__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Cash_Ledger_ID__c" {
  const stayclassy__Cash_Ledger_ID__c:number;
  export default stayclassy__Cash_Ledger_ID__c;
}
declare module "@salesforce/schema/Opportunity.Donor_email__c" {
  const Donor_email__c:string;
  export default Donor_email__c;
}
declare module "@salesforce/schema/Opportunity.Donor_comment__c" {
  const Donor_comment__c:string;
  export default Donor_comment__c;
}
declare module "@salesforce/schema/Opportunity.IsThanksReceived__c" {
  const IsThanksReceived__c:boolean;
  export default IsThanksReceived__c;
}
declare module "@salesforce/schema/Opportunity.Donation_Contact_Name__c" {
  const Donation_Contact_Name__c:string;
  export default Donation_Contact_Name__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Cash_Ledger__r" {
  const stayclassy__Cash_Ledger__r:any;
  export default stayclassy__Cash_Ledger__r;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Cash_Ledger__c" {
  const stayclassy__Cash_Ledger__c:any;
  export default stayclassy__Cash_Ledger__c;
}
declare module "@salesforce/schema/Opportunity.Classroom_page_URL__c" {
  const Classroom_page_URL__c:string;
  export default Classroom_page_URL__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Charged_Classy_Fees_Amount__c" {
  const stayclassy__Charged_Classy_Fees_Amount__c:number;
  export default stayclassy__Charged_Classy_Fees_Amount__c;
}
declare module "@salesforce/schema/Opportunity.test_link__c" {
  const test_link__c:string;
  export default test_link__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Charged_Currency_Code__c" {
  const stayclassy__Charged_Currency_Code__c:string;
  export default stayclassy__Charged_Currency_Code__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_s_School__c" {
  const Teacher_s_School__c:string;
  export default Teacher_s_School__c;
}
declare module "@salesforce/schema/Opportunity.Campaign_designations__r" {
  const Campaign_designations__r:any;
  export default Campaign_designations__r;
}
declare module "@salesforce/schema/Opportunity.Campaign_designations__c" {
  const Campaign_designations__c:any;
  export default Campaign_designations__c;
}
declare module "@salesforce/schema/Opportunity.Secondary_Campaign_Source__r" {
  const Secondary_Campaign_Source__r:any;
  export default Secondary_Campaign_Source__r;
}
declare module "@salesforce/schema/Opportunity.Secondary_Campaign_Source__c" {
  const Secondary_Campaign_Source__c:any;
  export default Secondary_Campaign_Source__c;
}
declare module "@salesforce/schema/Opportunity.Primary_Campaign_split_amount__c" {
  const Primary_Campaign_split_amount__c:number;
  export default Primary_Campaign_split_amount__c;
}
declare module "@salesforce/schema/Opportunity.Secondary_Campaign_split_amount__c" {
  const Secondary_Campaign_split_amount__c:number;
  export default Secondary_Campaign_split_amount__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_ID__c" {
  const Teacher_ID__c:string;
  export default Teacher_ID__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_Email__c" {
  const Teacher_Email__c:string;
  export default Teacher_Email__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_s_School_Street__c" {
  const Teacher_s_School_Street__c:string;
  export default Teacher_s_School_Street__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_Funds_Available__c" {
  const Teacher_Funds_Available__c:number;
  export default Teacher_Funds_Available__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Charged_Fees_Amount__c" {
  const stayclassy__Charged_Fees_Amount__c:number;
  export default stayclassy__Charged_Fees_Amount__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Charged_Processor_Fees_Amount__c" {
  const stayclassy__Charged_Processor_Fees_Amount__c:number;
  export default stayclassy__Charged_Processor_Fees_Amount__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Charged_Total_Gross_Amount__c" {
  const stayclassy__Charged_Total_Gross_Amount__c:number;
  export default stayclassy__Charged_Total_Gross_Amount__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_API_Request_Status2__c" {
  const stayclassy__Classy_API_Request_Status2__c:string;
  export default stayclassy__Classy_API_Request_Status2__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_API_Request__r" {
  const stayclassy__Classy_API_Request__r:any;
  export default stayclassy__Classy_API_Request__r;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_API_Request__c" {
  const stayclassy__Classy_API_Request__c:any;
  export default stayclassy__Classy_API_Request__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_Business_ID__c" {
  const stayclassy__Classy_Business_ID__c:number;
  export default stayclassy__Classy_Business_ID__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_Campaign_ID__c" {
  const stayclassy__Classy_Campaign_ID__c:number;
  export default stayclassy__Classy_Campaign_ID__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_Designation_ID__c" {
  const stayclassy__Classy_Designation_ID__c:number;
  export default stayclassy__Classy_Designation_ID__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_Fundraising_Page_ID__c" {
  const stayclassy__Classy_Fundraising_Page_ID__c:number;
  export default stayclassy__Classy_Fundraising_Page_ID__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_Fundraising_Team_ID__c" {
  const stayclassy__Classy_Fundraising_Team_ID__c:number;
  export default stayclassy__Classy_Fundraising_Team_ID__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_Member_Email__c" {
  const stayclassy__Classy_Member_Email__c:string;
  export default stayclassy__Classy_Member_Email__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_Member_ID__c" {
  const stayclassy__Classy_Member_ID__c:number;
  export default stayclassy__Classy_Member_ID__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_Member_Phone__c" {
  const stayclassy__Classy_Member_Phone__c:string;
  export default stayclassy__Classy_Member_Phone__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_Pay_Payment_Token_ID__c" {
  const stayclassy__Classy_Pay_Payment_Token_ID__c:number;
  export default stayclassy__Classy_Pay_Payment_Token_ID__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_Pay_Recurring_ID__c" {
  const stayclassy__Classy_Pay_Recurring_ID__c:number;
  export default stayclassy__Classy_Pay_Recurring_ID__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_Pay_Transaction_ID__c" {
  const stayclassy__Classy_Pay_Transaction_ID__c:number;
  export default stayclassy__Classy_Pay_Transaction_ID__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_Processor_Object_ID__c" {
  const stayclassy__Classy_Processor_Object_ID__c:string;
  export default stayclassy__Classy_Processor_Object_ID__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_Recurring_Donor_ID__c" {
  const stayclassy__Classy_Recurring_Donor_ID__c:number;
  export default stayclassy__Classy_Recurring_Donor_ID__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Company_Name__c" {
  const stayclassy__Company_Name__c:string;
  export default stayclassy__Company_Name__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Currency_Code__c" {
  const stayclassy__Currency_Code__c:string;
  export default stayclassy__Currency_Code__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Dedication_Ecard_ID__c" {
  const stayclassy__Dedication_Ecard_ID__c:number;
  export default stayclassy__Dedication_Ecard_ID__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Dedication_ID__c" {
  const stayclassy__Dedication_ID__c:number;
  export default stayclassy__Dedication_ID__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Is_Gift_Aid_Eligible__c" {
  const stayclassy__Is_Gift_Aid_Eligible__c:boolean;
  export default stayclassy__Is_Gift_Aid_Eligible__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Is_a_Matched_Gift__c" {
  const stayclassy__Is_a_Matched_Gift__c:boolean;
  export default stayclassy__Is_a_Matched_Gift__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Parent_Transaction_ID__c" {
  const stayclassy__Parent_Transaction_ID__c:number;
  export default stayclassy__Parent_Transaction_ID__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Partial_Refund_Amount__c" {
  const stayclassy__Partial_Refund_Amount__c:number;
  export default stayclassy__Partial_Refund_Amount__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Payment_Account_Number__c" {
  const stayclassy__Payment_Account_Number__c:string;
  export default stayclassy__Payment_Account_Number__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Payment_Account_Type__c" {
  const stayclassy__Payment_Account_Type__c:string;
  export default stayclassy__Payment_Account_Type__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Payment_End_Date__c" {
  const stayclassy__Payment_End_Date__c:any;
  export default stayclassy__Payment_End_Date__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Payment_Fee_On_Top__c" {
  const stayclassy__Payment_Fee_On_Top__c:boolean;
  export default stayclassy__Payment_Fee_On_Top__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Payment_Institution__c" {
  const stayclassy__Payment_Institution__c:string;
  export default stayclassy__Payment_Institution__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Raw_Currency_Code__c" {
  const stayclassy__Raw_Currency_Code__c:string;
  export default stayclassy__Raw_Currency_Code__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Raw_Donation_Gross_Amount__c" {
  const stayclassy__Raw_Donation_Gross_Amount__c:number;
  export default stayclassy__Raw_Donation_Gross_Amount__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Raw_Donation_Net_Amount__c" {
  const stayclassy__Raw_Donation_Net_Amount__c:number;
  export default stayclassy__Raw_Donation_Net_Amount__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Raw_Overhead_Net_Amount__c" {
  const stayclassy__Raw_Overhead_Net_Amount__c:number;
  export default stayclassy__Raw_Overhead_Net_Amount__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Raw_Total_Gross_Amount__c" {
  const stayclassy__Raw_Total_Gross_Amount__c:number;
  export default stayclassy__Raw_Total_Gross_Amount__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Transaction_Metadata__c" {
  const stayclassy__Transaction_Metadata__c:string;
  export default stayclassy__Transaction_Metadata__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Transaction_Status__c" {
  const stayclassy__Transaction_Status__c:string;
  export default stayclassy__Transaction_Status__c;
}
declare module "@salesforce/schema/Opportunity.Classy_Campaign_ID_to_text__c" {
  const Classy_Campaign_ID_to_text__c:string;
  export default Classy_Campaign_ID_to_text__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_s_School_City__c" {
  const Teacher_s_School_City__c:string;
  export default Teacher_s_School_City__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_s_School_State__c" {
  const Teacher_s_School_State__c:string;
  export default Teacher_s_School_State__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_s_School_Zip__c" {
  const Teacher_s_School_Zip__c:string;
  export default Teacher_s_School_Zip__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_External_ID__c" {
  const Teacher_External_ID__c:string;
  export default Teacher_External_ID__c;
}
declare module "@salesforce/schema/Opportunity.Revenue_Type__c" {
  const Revenue_Type__c:string;
  export default Revenue_Type__c;
}
declare module "@salesforce/schema/Opportunity.Check_Date__c" {
  const Check_Date__c:any;
  export default Check_Date__c;
}
declare module "@salesforce/schema/Opportunity.Disbursement_ID__c" {
  const Disbursement_ID__c:string;
  export default Disbursement_ID__c;
}
declare module "@salesforce/schema/Opportunity.Total_Check_Amount__c" {
  const Total_Check_Amount__c:number;
  export default Total_Check_Amount__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_Mailed_Classroom_Cards__c" {
  const Teacher_Mailed_Classroom_Cards__c:boolean;
  export default Teacher_Mailed_Classroom_Cards__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_Sent__c" {
  const Teacher_Sent__c:boolean;
  export default Teacher_Sent__c;
}
declare module "@salesforce/schema/Opportunity.donor_street__c" {
  const donor_street__c:string;
  export default donor_street__c;
}
declare module "@salesforce/schema/Opportunity.Donor_mailing_city__c" {
  const Donor_mailing_city__c:string;
  export default Donor_mailing_city__c;
}
declare module "@salesforce/schema/Opportunity.Donor_mailing_state__c" {
  const Donor_mailing_state__c:string;
  export default Donor_mailing_state__c;
}
declare module "@salesforce/schema/Opportunity.Donor_mailing_zip__c" {
  const Donor_mailing_zip__c:string;
  export default Donor_mailing_zip__c;
}
declare module "@salesforce/schema/Opportunity.Funds_Disbursement__c" {
  const Funds_Disbursement__c:boolean;
  export default Funds_Disbursement__c;
}
declare module "@salesforce/schema/Opportunity.Primary_Contact_ID__c" {
  const Primary_Contact_ID__c:string;
  export default Primary_Contact_ID__c;
}
declare module "@salesforce/schema/Opportunity.Donation_Date__c" {
  const Donation_Date__c:any;
  export default Donation_Date__c;
}
declare module "@salesforce/schema/Opportunity.Contract_start__c" {
  const Contract_start__c:any;
  export default Contract_start__c;
}
declare module "@salesforce/schema/Opportunity.Contract_End__c" {
  const Contract_End__c:any;
  export default Contract_End__c;
}
declare module "@salesforce/schema/Opportunity.Fund__c" {
  const Fund__c:string;
  export default Fund__c;
}
declare module "@salesforce/schema/Opportunity.Write_Off_Date__c" {
  const Write_Off_Date__c:any;
  export default Write_Off_Date__c;
}
declare module "@salesforce/schema/Opportunity.Write_Off_Notes__c" {
  const Write_Off_Notes__c:string;
  export default Write_Off_Notes__c;
}
declare module "@salesforce/schema/Opportunity.Frequency_of_Gift__c" {
  const Frequency_of_Gift__c:string;
  export default Frequency_of_Gift__c;
}
declare module "@salesforce/schema/Opportunity.Day_of_Recurring_Gift__c" {
  const Day_of_Recurring_Gift__c:string;
  export default Day_of_Recurring_Gift__c;
}
declare module "@salesforce/schema/Opportunity.School_Legacy__c" {
  const School_Legacy__c:string;
  export default School_Legacy__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_Email_Legacy__c" {
  const Teacher_Email_Legacy__c:string;
  export default Teacher_Email_Legacy__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_Legacy__c" {
  const Teacher_Legacy__c:string;
  export default Teacher_Legacy__c;
}
declare module "@salesforce/schema/Opportunity.School_City_Legacy__c" {
  const School_City_Legacy__c:string;
  export default School_City_Legacy__c;
}
declare module "@salesforce/schema/Opportunity.School_State_Legacy__c" {
  const School_State_Legacy__c:string;
  export default School_State_Legacy__c;
}
declare module "@salesforce/schema/Opportunity.School_Zip_Legacy__c" {
  const School_Zip_Legacy__c:string;
  export default School_Zip_Legacy__c;
}
declare module "@salesforce/schema/Opportunity.Private_School_Legacy__c" {
  const Private_School_Legacy__c:boolean;
  export default Private_School_Legacy__c;
}
declare module "@salesforce/schema/Opportunity.Reason_for_Donation_Legacy__c" {
  const Reason_for_Donation_Legacy__c:string;
  export default Reason_for_Donation_Legacy__c;
}
declare module "@salesforce/schema/Opportunity.Estimated_Total__c" {
  const Estimated_Total__c:number;
  export default Estimated_Total__c;
}
declare module "@salesforce/schema/Opportunity.Contact_Record_Type__c" {
  const Contact_Record_Type__c:string;
  export default Contact_Record_Type__c;
}
declare module "@salesforce/schema/Opportunity.Items__c" {
  const Items__c:string;
  export default Items__c;
}
declare module "@salesforce/schema/Opportunity.Chargent_Order__r" {
  const Chargent_Order__r:any;
  export default Chargent_Order__r;
}
declare module "@salesforce/schema/Opportunity.Chargent_Order__c" {
  const Chargent_Order__c:any;
  export default Chargent_Order__c;
}
declare module "@salesforce/schema/Opportunity.Classroom__r" {
  const Classroom__r:any;
  export default Classroom__r;
}
declare module "@salesforce/schema/Opportunity.Classroom__c" {
  const Classroom__c:any;
  export default Classroom__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Applied_FOT_Percent__c" {
  const stayclassy__Applied_FOT_Percent__c:number;
  export default stayclassy__Applied_FOT_Percent__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Dedication_First_Name__c" {
  const stayclassy__Dedication_First_Name__c:string;
  export default stayclassy__Dedication_First_Name__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Dedication_Last_Name__c" {
  const stayclassy__Dedication_Last_Name__c:string;
  export default stayclassy__Dedication_Last_Name__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Honoree_First_Name__c" {
  const stayclassy__Honoree_First_Name__c:string;
  export default stayclassy__Honoree_First_Name__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Honoree_Last_Name__c" {
  const stayclassy__Honoree_Last_Name__c:string;
  export default stayclassy__Honoree_Last_Name__c;
}
declare module "@salesforce/schema/Opportunity.Donor_s_Affiliation__c" {
  const Donor_s_Affiliation__c:string;
  export default Donor_s_Affiliation__c;
}
declare module "@salesforce/schema/Opportunity.Promo_Code__c" {
  const Promo_Code__c:string;
  export default Promo_Code__c;
}
declare module "@salesforce/schema/Opportunity.Resubmitted__c" {
  const Resubmitted__c:string;
  export default Resubmitted__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_s_School_SW_Title_1__c" {
  const Teacher_s_School_SW_Title_1__c:boolean;
  export default Teacher_s_School_SW_Title_1__c;
}
declare module "@salesforce/schema/Opportunity.Phone_Call_1__c" {
  const Phone_Call_1__c:any;
  export default Phone_Call_1__c;
}
declare module "@salesforce/schema/Opportunity.Phone_Call_2__c" {
  const Phone_Call_2__c:any;
  export default Phone_Call_2__c;
}
declare module "@salesforce/schema/Opportunity.Close_Date_for_Reports__c" {
  const Close_Date_for_Reports__c:any;
  export default Close_Date_for_Reports__c;
}
declare module "@salesforce/schema/Opportunity.Tribute__c" {
  const Tribute__c:string;
  export default Tribute__c;
}
declare module "@salesforce/schema/Opportunity.Fiscal_Month__c" {
  const Fiscal_Month__c:string;
  export default Fiscal_Month__c;
}
declare module "@salesforce/schema/Opportunity.PersonalThankYou__c" {
  const PersonalThankYou__c:boolean;
  export default PersonalThankYou__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_s_School_Title_1__c" {
  const Teacher_s_School_Title_1__c:boolean;
  export default Teacher_s_School_Title_1__c;
}
declare module "@salesforce/schema/Opportunity.Use_for_marketing__c" {
  const Use_for_marketing__c:boolean;
  export default Use_for_marketing__c;
}
declare module "@salesforce/schema/Opportunity.ExpDateToday__c" {
  const ExpDateToday__c:boolean;
  export default ExpDateToday__c;
}
declare module "@salesforce/schema/Opportunity.Expiration_Notice_Due__c" {
  const Expiration_Notice_Due__c:number;
  export default Expiration_Notice_Due__c;
}
declare module "@salesforce/schema/Opportunity.FundsExpNot_1__c" {
  const FundsExpNot_1__c:boolean;
  export default FundsExpNot_1__c;
}
declare module "@salesforce/schema/Opportunity.FundsExpNot_2__c" {
  const FundsExpNot_2__c:boolean;
  export default FundsExpNot_2__c;
}
declare module "@salesforce/schema/Opportunity.FundsExpNot_3__c" {
  const FundsExpNot_3__c:boolean;
  export default FundsExpNot_3__c;
}
declare module "@salesforce/schema/Opportunity.FundsExpNot__c" {
  const FundsExpNot__c:boolean;
  export default FundsExpNot__c;
}
declare module "@salesforce/schema/Opportunity.Funds_Expiration_Amount__c" {
  const Funds_Expiration_Amount__c:number;
  export default Funds_Expiration_Amount__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_or_School_Admin__c" {
  const Teacher_or_School_Admin__c:string;
  export default Teacher_or_School_Admin__c;
}
declare module "@salesforce/schema/Opportunity.Do_not_process__c" {
  const Do_not_process__c:boolean;
  export default Do_not_process__c;
}
declare module "@salesforce/schema/Opportunity.Expired_Opportunity__r" {
  const Expired_Opportunity__r:any;
  export default Expired_Opportunity__r;
}
declare module "@salesforce/schema/Opportunity.Expired_Opportunity__c" {
  const Expired_Opportunity__c:any;
  export default Expired_Opportunity__c;
}
declare module "@salesforce/schema/Opportunity.Expired_Opportunity_Record_ID__c" {
  const Expired_Opportunity_Record_ID__c:string;
  export default Expired_Opportunity_Record_ID__c;
}
declare module "@salesforce/schema/Opportunity.Donor_Record_Type__c" {
  const Donor_Record_Type__c:string;
  export default Donor_Record_Type__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_s_School_Phone__c" {
  const Teacher_s_School_Phone__c:string;
  export default Teacher_s_School_Phone__c;
}
declare module "@salesforce/schema/Opportunity.Last_Expiring_Fund_Notification_Date__c" {
  const Last_Expiring_Fund_Notification_Date__c:any;
  export default Last_Expiring_Fund_Notification_Date__c;
}
declare module "@salesforce/schema/Opportunity.Expired_Opportunity_Expiration_Date__c" {
  const Expired_Opportunity_Expiration_Date__c:any;
  export default Expired_Opportunity_Expiration_Date__c;
}
declare module "@salesforce/schema/Opportunity.Expired_Opportunity_Dates_Match__c" {
  const Expired_Opportunity_Dates_Match__c:boolean;
  export default Expired_Opportunity_Dates_Match__c;
}
declare module "@salesforce/schema/Opportunity.Teacher_Reviewed__c" {
  const Teacher_Reviewed__c:boolean;
  export default Teacher_Reviewed__c;
}
declare module "@salesforce/schema/Opportunity.Reallocated_Funds__c" {
  const Reallocated_Funds__c:boolean;
  export default Reallocated_Funds__c;
}
declare module "@salesforce/schema/Opportunity.Monthly_Giving_Start_Date__c" {
  const Monthly_Giving_Start_Date__c:any;
  export default Monthly_Giving_Start_Date__c;
}
declare module "@salesforce/schema/Opportunity.Monthly_Giving_End_Date__c" {
  const Monthly_Giving_End_Date__c:any;
  export default Monthly_Giving_End_Date__c;
}
declare module "@salesforce/schema/Opportunity.One_Time_or_Monthly__c" {
  const One_Time_or_Monthly__c:string;
  export default One_Time_or_Monthly__c;
}
declare module "@salesforce/schema/Opportunity.stayclassy__Classy_Member_Name2__c" {
  const stayclassy__Classy_Member_Name2__c:string;
  export default stayclassy__Classy_Member_Name2__c;
}
declare module "@salesforce/schema/Opportunity.Order_School_City__c" {
  const Order_School_City__c:string;
  export default Order_School_City__c;
}
declare module "@salesforce/schema/Opportunity.Order_School_Country__c" {
  const Order_School_Country__c:string;
  export default Order_School_Country__c;
}
declare module "@salesforce/schema/Opportunity.Order_School_SW_Title_1__c" {
  const Order_School_SW_Title_1__c:boolean;
  export default Order_School_SW_Title_1__c;
}
declare module "@salesforce/schema/Opportunity.Order_School_State__c" {
  const Order_School_State__c:string;
  export default Order_School_State__c;
}
declare module "@salesforce/schema/Opportunity.Order_School_Street__c" {
  const Order_School_Street__c:string;
  export default Order_School_Street__c;
}
declare module "@salesforce/schema/Opportunity.Order_School_Title_1__c" {
  const Order_School_Title_1__c:boolean;
  export default Order_School_Title_1__c;
}
declare module "@salesforce/schema/Opportunity.Order_School_Zip__c" {
  const Order_School_Zip__c:string;
  export default Order_School_Zip__c;
}
declare module "@salesforce/schema/Opportunity.Order_School__c" {
  const Order_School__c:string;
  export default Order_School__c;
}
declare module "@salesforce/schema/Opportunity.Funds_Transferred_To_From__r" {
  const Funds_Transferred_To_From__r:any;
  export default Funds_Transferred_To_From__r;
}
declare module "@salesforce/schema/Opportunity.Funds_Transferred_To_From__c" {
  const Funds_Transferred_To_From__c:any;
  export default Funds_Transferred_To_From__c;
}
declare module "@salesforce/schema/Opportunity.Amount_W_Feed__c" {
  const Amount_W_Feed__c:number;
  export default Amount_W_Feed__c;
}
declare module "@salesforce/schema/Opportunity.Disbursement_Name__c" {
  const Disbursement_Name__c:string;
  export default Disbursement_Name__c;
}
declare module "@salesforce/schema/Opportunity.Hide_Transaction_on_Profile__c" {
  const Hide_Transaction_on_Profile__c:boolean;
  export default Hide_Transaction_on_Profile__c;
}
declare module "@salesforce/schema/Opportunity.Expired_Opportunity_Reallocated__c" {
  const Expired_Opportunity_Reallocated__c:boolean;
  export default Expired_Opportunity_Reallocated__c;
}
declare module "@salesforce/schema/Opportunity.Auto_Posted__c" {
  const Auto_Posted__c:boolean;
  export default Auto_Posted__c;
}
declare module "@salesforce/schema/Opportunity.AcctSeed__Tax_Amount__c" {
  const AcctSeed__Tax_Amount__c:number;
  export default AcctSeed__Tax_Amount__c;
}
declare module "@salesforce/schema/Opportunity.AcctSeed__Total_with_Tax__c" {
  const AcctSeed__Total_with_Tax__c:number;
  export default AcctSeed__Total_with_Tax__c;
}
