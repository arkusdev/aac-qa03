declare module "@salesforce/schema/Product2.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Product2.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Product2.ProductCode" {
  const ProductCode:string;
  export default ProductCode;
}
declare module "@salesforce/schema/Product2.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Product2.IsActive" {
  const IsActive:boolean;
  export default IsActive;
}
declare module "@salesforce/schema/Product2.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Product2.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Product2.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Product2.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Product2.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Product2.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Product2.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Product2.Family" {
  const Family:string;
  export default Family;
}
declare module "@salesforce/schema/Product2.ExternalDataSource" {
  const ExternalDataSource:any;
  export default ExternalDataSource;
}
declare module "@salesforce/schema/Product2.ExternalDataSourceId" {
  const ExternalDataSourceId:any;
  export default ExternalDataSourceId;
}
declare module "@salesforce/schema/Product2.ExternalId" {
  const ExternalId:string;
  export default ExternalId;
}
declare module "@salesforce/schema/Product2.DisplayUrl" {
  const DisplayUrl:string;
  export default DisplayUrl;
}
declare module "@salesforce/schema/Product2.QuantityUnitOfMeasure" {
  const QuantityUnitOfMeasure:string;
  export default QuantityUnitOfMeasure;
}
declare module "@salesforce/schema/Product2.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Product2.IsArchived" {
  const IsArchived:boolean;
  export default IsArchived;
}
declare module "@salesforce/schema/Product2.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Product2.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Product2.StockKeepingUnit" {
  const StockKeepingUnit:string;
  export default StockKeepingUnit;
}
declare module "@salesforce/schema/Product2.AcctSeed__Accounting_Type__c" {
  const AcctSeed__Accounting_Type__c:string;
  export default AcctSeed__Accounting_Type__c;
}
declare module "@salesforce/schema/Product2.AcctSeed__Expense_GL_Account__r" {
  const AcctSeed__Expense_GL_Account__r:any;
  export default AcctSeed__Expense_GL_Account__r;
}
declare module "@salesforce/schema/Product2.AcctSeed__Expense_GL_Account__c" {
  const AcctSeed__Expense_GL_Account__c:any;
  export default AcctSeed__Expense_GL_Account__c;
}
declare module "@salesforce/schema/Product2.AcctSeed__GL_Account_Variable_1__r" {
  const AcctSeed__GL_Account_Variable_1__r:any;
  export default AcctSeed__GL_Account_Variable_1__r;
}
declare module "@salesforce/schema/Product2.AcctSeed__GL_Account_Variable_1__c" {
  const AcctSeed__GL_Account_Variable_1__c:any;
  export default AcctSeed__GL_Account_Variable_1__c;
}
declare module "@salesforce/schema/Product2.AcctSeed__GL_Account_Variable_2__r" {
  const AcctSeed__GL_Account_Variable_2__r:any;
  export default AcctSeed__GL_Account_Variable_2__r;
}
declare module "@salesforce/schema/Product2.AcctSeed__GL_Account_Variable_2__c" {
  const AcctSeed__GL_Account_Variable_2__c:any;
  export default AcctSeed__GL_Account_Variable_2__c;
}
declare module "@salesforce/schema/Product2.AcctSeed__GL_Account_Variable_3__r" {
  const AcctSeed__GL_Account_Variable_3__r:any;
  export default AcctSeed__GL_Account_Variable_3__r;
}
declare module "@salesforce/schema/Product2.AcctSeed__GL_Account_Variable_3__c" {
  const AcctSeed__GL_Account_Variable_3__c:any;
  export default AcctSeed__GL_Account_Variable_3__c;
}
declare module "@salesforce/schema/Product2.AcctSeed__GL_Account_Variable_4__r" {
  const AcctSeed__GL_Account_Variable_4__r:any;
  export default AcctSeed__GL_Account_Variable_4__r;
}
declare module "@salesforce/schema/Product2.AcctSeed__GL_Account_Variable_4__c" {
  const AcctSeed__GL_Account_Variable_4__c:any;
  export default AcctSeed__GL_Account_Variable_4__c;
}
declare module "@salesforce/schema/Product2.AcctSeed__Inventory_GL_Account__r" {
  const AcctSeed__Inventory_GL_Account__r:any;
  export default AcctSeed__Inventory_GL_Account__r;
}
declare module "@salesforce/schema/Product2.AcctSeed__Inventory_GL_Account__c" {
  const AcctSeed__Inventory_GL_Account__c:any;
  export default AcctSeed__Inventory_GL_Account__c;
}
declare module "@salesforce/schema/Product2.AcctSeed__Inventory_Product__c" {
  const AcctSeed__Inventory_Product__c:boolean;
  export default AcctSeed__Inventory_Product__c;
}
declare module "@salesforce/schema/Product2.AcctSeed__Inventory_Type__c" {
  const AcctSeed__Inventory_Type__c:string;
  export default AcctSeed__Inventory_Type__c;
}
declare module "@salesforce/schema/Product2.AcctSeed__Non_Inventoried_Item__c" {
  const AcctSeed__Non_Inventoried_Item__c:boolean;
  export default AcctSeed__Non_Inventoried_Item__c;
}
declare module "@salesforce/schema/Product2.AcctSeed__Revenue_GL_Account__r" {
  const AcctSeed__Revenue_GL_Account__r:any;
  export default AcctSeed__Revenue_GL_Account__r;
}
declare module "@salesforce/schema/Product2.AcctSeed__Revenue_GL_Account__c" {
  const AcctSeed__Revenue_GL_Account__c:any;
  export default AcctSeed__Revenue_GL_Account__c;
}
declare module "@salesforce/schema/Product2.AcctSeed__Tax_Rate__c" {
  const AcctSeed__Tax_Rate__c:number;
  export default AcctSeed__Tax_Rate__c;
}
declare module "@salesforce/schema/Product2.AcctSeed__Unit_Cost__c" {
  const AcctSeed__Unit_Cost__c:number;
  export default AcctSeed__Unit_Cost__c;
}
declare module "@salesforce/schema/Product2.AcctSeedERP__Default_Vendor__r" {
  const AcctSeedERP__Default_Vendor__r:any;
  export default AcctSeedERP__Default_Vendor__r;
}
declare module "@salesforce/schema/Product2.AcctSeedERP__Default_Vendor__c" {
  const AcctSeedERP__Default_Vendor__c:any;
  export default AcctSeedERP__Default_Vendor__c;
}
declare module "@salesforce/schema/Product2.AcctSeedERP__Inventory_Asset__c" {
  const AcctSeedERP__Inventory_Asset__c:boolean;
  export default AcctSeedERP__Inventory_Asset__c;
}
declare module "@salesforce/schema/Product2.AcctSeedERP__Lead_Time__c" {
  const AcctSeedERP__Lead_Time__c:number;
  export default AcctSeedERP__Lead_Time__c;
}
declare module "@salesforce/schema/Product2.AcctSeedERP__Manufacturing_Order_Template__r" {
  const AcctSeedERP__Manufacturing_Order_Template__r:any;
  export default AcctSeedERP__Manufacturing_Order_Template__r;
}
declare module "@salesforce/schema/Product2.AcctSeedERP__Manufacturing_Order_Template__c" {
  const AcctSeedERP__Manufacturing_Order_Template__c:any;
  export default AcctSeedERP__Manufacturing_Order_Template__c;
}
declare module "@salesforce/schema/Product2.AcctSeedERP__Minimum_Order_Quantity__c" {
  const AcctSeedERP__Minimum_Order_Quantity__c:number;
  export default AcctSeedERP__Minimum_Order_Quantity__c;
}
declare module "@salesforce/schema/Product2.AcctSeedERP__Safety_Stock_Quantity__c" {
  const AcctSeedERP__Safety_Stock_Quantity__c:number;
  export default AcctSeedERP__Safety_Stock_Quantity__c;
}
declare module "@salesforce/schema/Product2.AcctSeedERP__Sales_Order_Exclude__c" {
  const AcctSeedERP__Sales_Order_Exclude__c:boolean;
  export default AcctSeedERP__Sales_Order_Exclude__c;
}
declare module "@salesforce/schema/Product2.AcctSeedERP__Serialized__c" {
  const AcctSeedERP__Serialized__c:boolean;
  export default AcctSeedERP__Serialized__c;
}
declare module "@salesforce/schema/Product2.stayclassy__Charity_Donation_Percentage__c" {
  const stayclassy__Charity_Donation_Percentage__c:number;
  export default stayclassy__Charity_Donation_Percentage__c;
}
declare module "@salesforce/schema/Product2.stayclassy__Classy_Campaign_ID__c" {
  const stayclassy__Classy_Campaign_ID__c:number;
  export default stayclassy__Classy_Campaign_ID__c;
}
declare module "@salesforce/schema/Product2.stayclassy__Classy_Campaign__r" {
  const stayclassy__Classy_Campaign__r:any;
  export default stayclassy__Classy_Campaign__r;
}
declare module "@salesforce/schema/Product2.stayclassy__Classy_Campaign__c" {
  const stayclassy__Classy_Campaign__c:any;
  export default stayclassy__Classy_Campaign__c;
}
declare module "@salesforce/schema/Product2.stayclassy__Classy_Product_ID__c" {
  const stayclassy__Classy_Product_ID__c:number;
  export default stayclassy__Classy_Product_ID__c;
}
declare module "@salesforce/schema/Product2.stayclassy__Quantity_Available__c" {
  const stayclassy__Quantity_Available__c:number;
  export default stayclassy__Quantity_Available__c;
}
declare module "@salesforce/schema/Product2.Order_Cloud_Product_Id__c" {
  const Order_Cloud_Product_Id__c:string;
  export default Order_Cloud_Product_Id__c;
}
declare module "@salesforce/schema/Product2.pymt__Category__r" {
  const pymt__Category__r:any;
  export default pymt__Category__r;
}
declare module "@salesforce/schema/Product2.pymt__Category__c" {
  const pymt__Category__c:any;
  export default pymt__Category__c;
}
declare module "@salesforce/schema/Product2.pymt__Image_Id__c" {
  const pymt__Image_Id__c:string;
  export default pymt__Image_Id__c;
}
declare module "@salesforce/schema/Product2.pymt__Image_URL__c" {
  const pymt__Image_URL__c:string;
  export default pymt__Image_URL__c;
}
declare module "@salesforce/schema/Product2.pymt__Image__c" {
  const pymt__Image__c:string;
  export default pymt__Image__c;
}
declare module "@salesforce/schema/Product2.pymt__Inventory__c" {
  const pymt__Inventory__c:number;
  export default pymt__Inventory__c;
}
declare module "@salesforce/schema/Product2.pymt__On_Payment_Completed__c" {
  const pymt__On_Payment_Completed__c:string;
  export default pymt__On_Payment_Completed__c;
}
declare module "@salesforce/schema/Product2.pymt__Tangible__c" {
  const pymt__Tangible__c:boolean;
  export default pymt__Tangible__c;
}
declare module "@salesforce/schema/Product2.pymt__Taxable__c" {
  const pymt__Taxable__c:boolean;
  export default pymt__Taxable__c;
}
declare module "@salesforce/schema/Product2.pymt__Weight_Class__c" {
  const pymt__Weight_Class__c:string;
  export default pymt__Weight_Class__c;
}
declare module "@salesforce/schema/Product2.pymt__Weight__c" {
  const pymt__Weight__c:number;
  export default pymt__Weight__c;
}
declare module "@salesforce/schema/Product2.PunchoutProduct__c" {
  const PunchoutProduct__c:boolean;
  export default PunchoutProduct__c;
}
declare module "@salesforce/schema/Product2.Vendor_Name__c" {
  const Vendor_Name__c:string;
  export default Vendor_Name__c;
}
declare module "@salesforce/schema/Product2.stayclassy__Classy_API_Request__r" {
  const stayclassy__Classy_API_Request__r:any;
  export default stayclassy__Classy_API_Request__r;
}
declare module "@salesforce/schema/Product2.stayclassy__Classy_API_Request__c" {
  const stayclassy__Classy_API_Request__c:any;
  export default stayclassy__Classy_API_Request__c;
}
declare module "@salesforce/schema/Product2.stayclassy__Deductible_Amount__c" {
  const stayclassy__Deductible_Amount__c:number;
  export default stayclassy__Deductible_Amount__c;
}
declare module "@salesforce/schema/Product2.stayclassy__Ended_At__c" {
  const stayclassy__Ended_At__c:any;
  export default stayclassy__Ended_At__c;
}
declare module "@salesforce/schema/Product2.stayclassy__Entries_Per_Ticket__c" {
  const stayclassy__Entries_Per_Ticket__c:number;
  export default stayclassy__Entries_Per_Ticket__c;
}
declare module "@salesforce/schema/Product2.stayclassy__Is_Classy_Mode__c" {
  const stayclassy__Is_Classy_Mode__c:boolean;
  export default stayclassy__Is_Classy_Mode__c;
}
declare module "@salesforce/schema/Product2.stayclassy__Max_Per_Transaction__c" {
  const stayclassy__Max_Per_Transaction__c:number;
  export default stayclassy__Max_Per_Transaction__c;
}
declare module "@salesforce/schema/Product2.stayclassy__Min_Per_Transaction__c" {
  const stayclassy__Min_Per_Transaction__c:number;
  export default stayclassy__Min_Per_Transaction__c;
}
declare module "@salesforce/schema/Product2.stayclassy__Started_At__c" {
  const stayclassy__Started_At__c:any;
  export default stayclassy__Started_At__c;
}
declare module "@salesforce/schema/Product2.Case_Safe_ID__c" {
  const Case_Safe_ID__c:string;
  export default Case_Safe_ID__c;
}
declare module "@salesforce/schema/Product2.AcctSeed__Tax_Group__r" {
  const AcctSeed__Tax_Group__r:any;
  export default AcctSeed__Tax_Group__r;
}
declare module "@salesforce/schema/Product2.AcctSeed__Tax_Group__c" {
  const AcctSeed__Tax_Group__c:any;
  export default AcctSeed__Tax_Group__c;
}
