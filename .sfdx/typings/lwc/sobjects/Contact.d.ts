declare module "@salesforce/schema/Contact.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Contact.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Contact.MasterRecord" {
  const MasterRecord:any;
  export default MasterRecord;
}
declare module "@salesforce/schema/Contact.MasterRecordId" {
  const MasterRecordId:any;
  export default MasterRecordId;
}
declare module "@salesforce/schema/Contact.Account" {
  const Account:any;
  export default Account;
}
declare module "@salesforce/schema/Contact.AccountId" {
  const AccountId:any;
  export default AccountId;
}
declare module "@salesforce/schema/Contact.LastName" {
  const LastName:string;
  export default LastName;
}
declare module "@salesforce/schema/Contact.FirstName" {
  const FirstName:string;
  export default FirstName;
}
declare module "@salesforce/schema/Contact.Salutation" {
  const Salutation:string;
  export default Salutation;
}
declare module "@salesforce/schema/Contact.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Contact.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/Contact.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/Contact.OtherStreet" {
  const OtherStreet:string;
  export default OtherStreet;
}
declare module "@salesforce/schema/Contact.OtherCity" {
  const OtherCity:string;
  export default OtherCity;
}
declare module "@salesforce/schema/Contact.OtherState" {
  const OtherState:string;
  export default OtherState;
}
declare module "@salesforce/schema/Contact.OtherPostalCode" {
  const OtherPostalCode:string;
  export default OtherPostalCode;
}
declare module "@salesforce/schema/Contact.OtherCountry" {
  const OtherCountry:string;
  export default OtherCountry;
}
declare module "@salesforce/schema/Contact.OtherStateCode" {
  const OtherStateCode:string;
  export default OtherStateCode;
}
declare module "@salesforce/schema/Contact.OtherCountryCode" {
  const OtherCountryCode:string;
  export default OtherCountryCode;
}
declare module "@salesforce/schema/Contact.OtherLatitude" {
  const OtherLatitude:number;
  export default OtherLatitude;
}
declare module "@salesforce/schema/Contact.OtherLongitude" {
  const OtherLongitude:number;
  export default OtherLongitude;
}
declare module "@salesforce/schema/Contact.OtherGeocodeAccuracy" {
  const OtherGeocodeAccuracy:string;
  export default OtherGeocodeAccuracy;
}
declare module "@salesforce/schema/Contact.OtherAddress" {
  const OtherAddress:any;
  export default OtherAddress;
}
declare module "@salesforce/schema/Contact.MailingStreet" {
  const MailingStreet:string;
  export default MailingStreet;
}
declare module "@salesforce/schema/Contact.MailingCity" {
  const MailingCity:string;
  export default MailingCity;
}
declare module "@salesforce/schema/Contact.MailingState" {
  const MailingState:string;
  export default MailingState;
}
declare module "@salesforce/schema/Contact.MailingPostalCode" {
  const MailingPostalCode:string;
  export default MailingPostalCode;
}
declare module "@salesforce/schema/Contact.MailingCountry" {
  const MailingCountry:string;
  export default MailingCountry;
}
declare module "@salesforce/schema/Contact.MailingStateCode" {
  const MailingStateCode:string;
  export default MailingStateCode;
}
declare module "@salesforce/schema/Contact.MailingCountryCode" {
  const MailingCountryCode:string;
  export default MailingCountryCode;
}
declare module "@salesforce/schema/Contact.MailingLatitude" {
  const MailingLatitude:number;
  export default MailingLatitude;
}
declare module "@salesforce/schema/Contact.MailingLongitude" {
  const MailingLongitude:number;
  export default MailingLongitude;
}
declare module "@salesforce/schema/Contact.MailingGeocodeAccuracy" {
  const MailingGeocodeAccuracy:string;
  export default MailingGeocodeAccuracy;
}
declare module "@salesforce/schema/Contact.MailingAddress" {
  const MailingAddress:any;
  export default MailingAddress;
}
declare module "@salesforce/schema/Contact.Phone" {
  const Phone:string;
  export default Phone;
}
declare module "@salesforce/schema/Contact.Fax" {
  const Fax:string;
  export default Fax;
}
declare module "@salesforce/schema/Contact.MobilePhone" {
  const MobilePhone:string;
  export default MobilePhone;
}
declare module "@salesforce/schema/Contact.HomePhone" {
  const HomePhone:string;
  export default HomePhone;
}
declare module "@salesforce/schema/Contact.OtherPhone" {
  const OtherPhone:string;
  export default OtherPhone;
}
declare module "@salesforce/schema/Contact.AssistantPhone" {
  const AssistantPhone:string;
  export default AssistantPhone;
}
declare module "@salesforce/schema/Contact.ReportsTo" {
  const ReportsTo:any;
  export default ReportsTo;
}
declare module "@salesforce/schema/Contact.ReportsToId" {
  const ReportsToId:any;
  export default ReportsToId;
}
declare module "@salesforce/schema/Contact.Email" {
  const Email:string;
  export default Email;
}
declare module "@salesforce/schema/Contact.Title" {
  const Title:string;
  export default Title;
}
declare module "@salesforce/schema/Contact.Department" {
  const Department:string;
  export default Department;
}
declare module "@salesforce/schema/Contact.AssistantName" {
  const AssistantName:string;
  export default AssistantName;
}
declare module "@salesforce/schema/Contact.LeadSource" {
  const LeadSource:string;
  export default LeadSource;
}
declare module "@salesforce/schema/Contact.Birthdate" {
  const Birthdate:any;
  export default Birthdate;
}
declare module "@salesforce/schema/Contact.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Contact.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Contact.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Contact.HasOptedOutOfEmail" {
  const HasOptedOutOfEmail:boolean;
  export default HasOptedOutOfEmail;
}
declare module "@salesforce/schema/Contact.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Contact.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Contact.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Contact.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Contact.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Contact.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Contact.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Contact.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Contact.LastCURequestDate" {
  const LastCURequestDate:any;
  export default LastCURequestDate;
}
declare module "@salesforce/schema/Contact.LastCUUpdateDate" {
  const LastCUUpdateDate:any;
  export default LastCUUpdateDate;
}
declare module "@salesforce/schema/Contact.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Contact.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Contact.EmailBouncedReason" {
  const EmailBouncedReason:string;
  export default EmailBouncedReason;
}
declare module "@salesforce/schema/Contact.EmailBouncedDate" {
  const EmailBouncedDate:any;
  export default EmailBouncedDate;
}
declare module "@salesforce/schema/Contact.IsEmailBounced" {
  const IsEmailBounced:boolean;
  export default IsEmailBounced;
}
declare module "@salesforce/schema/Contact.PhotoUrl" {
  const PhotoUrl:string;
  export default PhotoUrl;
}
declare module "@salesforce/schema/Contact.Jigsaw" {
  const Jigsaw:string;
  export default Jigsaw;
}
declare module "@salesforce/schema/Contact.JigsawContactId" {
  const JigsawContactId:string;
  export default JigsawContactId;
}
declare module "@salesforce/schema/Contact.Individual" {
  const Individual:any;
  export default Individual;
}
declare module "@salesforce/schema/Contact.IndividualId" {
  const IndividualId:any;
  export default IndividualId;
}
declare module "@salesforce/schema/Contact.npe01__AlternateEmail__c" {
  const npe01__AlternateEmail__c:string;
  export default npe01__AlternateEmail__c;
}
declare module "@salesforce/schema/Contact.npe01__HomeEmail__c" {
  const npe01__HomeEmail__c:string;
  export default npe01__HomeEmail__c;
}
declare module "@salesforce/schema/Contact.npe01__Home_Address__c" {
  const npe01__Home_Address__c:string;
  export default npe01__Home_Address__c;
}
declare module "@salesforce/schema/Contact.npe01__Last_Donation_Date__c" {
  const npe01__Last_Donation_Date__c:any;
  export default npe01__Last_Donation_Date__c;
}
declare module "@salesforce/schema/Contact.npe01__Lifetime_Giving_History_Amount__c" {
  const npe01__Lifetime_Giving_History_Amount__c:number;
  export default npe01__Lifetime_Giving_History_Amount__c;
}
declare module "@salesforce/schema/Contact.npe01__Organization_Type__c" {
  const npe01__Organization_Type__c:string;
  export default npe01__Organization_Type__c;
}
declare module "@salesforce/schema/Contact.npe01__Other_Address__c" {
  const npe01__Other_Address__c:string;
  export default npe01__Other_Address__c;
}
declare module "@salesforce/schema/Contact.npe01__PreferredPhone__c" {
  const npe01__PreferredPhone__c:string;
  export default npe01__PreferredPhone__c;
}
declare module "@salesforce/schema/Contact.npe01__Preferred_Email__c" {
  const npe01__Preferred_Email__c:string;
  export default npe01__Preferred_Email__c;
}
declare module "@salesforce/schema/Contact.npe01__Primary_Address_Type__c" {
  const npe01__Primary_Address_Type__c:string;
  export default npe01__Primary_Address_Type__c;
}
declare module "@salesforce/schema/Contact.npe01__Private__c" {
  const npe01__Private__c:boolean;
  export default npe01__Private__c;
}
declare module "@salesforce/schema/Contact.npe01__Secondary_Address_Type__c" {
  const npe01__Secondary_Address_Type__c:string;
  export default npe01__Secondary_Address_Type__c;
}
declare module "@salesforce/schema/Contact.npe01__SystemAccountProcessor__c" {
  const npe01__SystemAccountProcessor__c:string;
  export default npe01__SystemAccountProcessor__c;
}
declare module "@salesforce/schema/Contact.npe01__SystemIsIndividual__c" {
  const npe01__SystemIsIndividual__c:boolean;
  export default npe01__SystemIsIndividual__c;
}
declare module "@salesforce/schema/Contact.npe01__WorkEmail__c" {
  const npe01__WorkEmail__c:string;
  export default npe01__WorkEmail__c;
}
declare module "@salesforce/schema/Contact.npe01__WorkPhone__c" {
  const npe01__WorkPhone__c:string;
  export default npe01__WorkPhone__c;
}
declare module "@salesforce/schema/Contact.npe01__Work_Address__c" {
  const npe01__Work_Address__c:string;
  export default npe01__Work_Address__c;
}
declare module "@salesforce/schema/Contact.npo02__Formula_HouseholdMailingAddress__c" {
  const npo02__Formula_HouseholdMailingAddress__c:string;
  export default npo02__Formula_HouseholdMailingAddress__c;
}
declare module "@salesforce/schema/Contact.npo02__Formula_HouseholdPhone__c" {
  const npo02__Formula_HouseholdPhone__c:string;
  export default npo02__Formula_HouseholdPhone__c;
}
declare module "@salesforce/schema/Contact.npo02__Household__r" {
  const npo02__Household__r:any;
  export default npo02__Household__r;
}
declare module "@salesforce/schema/Contact.npo02__Household__c" {
  const npo02__Household__c:any;
  export default npo02__Household__c;
}
declare module "@salesforce/schema/Contact.npo02__Languages__c" {
  const npo02__Languages__c:string;
  export default npo02__Languages__c;
}
declare module "@salesforce/schema/Contact.npo02__Level__c" {
  const npo02__Level__c:string;
  export default npo02__Level__c;
}
declare module "@salesforce/schema/Contact.npo02__SystemHouseholdProcessor__c" {
  const npo02__SystemHouseholdProcessor__c:string;
  export default npo02__SystemHouseholdProcessor__c;
}
declare module "@salesforce/schema/Contact.active_on_db__c" {
  const active_on_db__c:string;
  export default active_on_db__c;
}
declare module "@salesforce/schema/Contact.npe01__Type_of_Account__c" {
  const npe01__Type_of_Account__c:string;
  export default npe01__Type_of_Account__c;
}
declare module "@salesforce/schema/Contact.Giving_preferences__c" {
  const Giving_preferences__c:string;
  export default Giving_preferences__c;
}
declare module "@salesforce/schema/Contact.Development_Partner_Type__c" {
  const Development_Partner_Type__c:string;
  export default Development_Partner_Type__c;
}
declare module "@salesforce/schema/Contact.District_Website_Mgmt_Company__c" {
  const District_Website_Mgmt_Company__c:string;
  export default District_Website_Mgmt_Company__c;
}
declare module "@salesforce/schema/Contact.Declined_Campaign__c" {
  const Declined_Campaign__c:string;
  export default Declined_Campaign__c;
}
declare module "@salesforce/schema/Contact.AAC_Data_Upload_Date__c" {
  const AAC_Data_Upload_Date__c:any;
  export default AAC_Data_Upload_Date__c;
}
declare module "@salesforce/schema/Contact.Dollars_Raised__c" {
  const Dollars_Raised__c:number;
  export default Dollars_Raised__c;
}
declare module "@salesforce/schema/Contact.No_of_AAC_Teachers__c" {
  const No_of_AAC_Teachers__c:number;
  export default No_of_AAC_Teachers__c;
}
declare module "@salesforce/schema/Contact.Assigned_Manager__c" {
  const Assigned_Manager__c:string;
  export default Assigned_Manager__c;
}
declare module "@salesforce/schema/Contact.npo02__AverageAmount__c" {
  const npo02__AverageAmount__c:number;
  export default npo02__AverageAmount__c;
}
declare module "@salesforce/schema/Contact.npo02__Best_Gift_Year_Total__c" {
  const npo02__Best_Gift_Year_Total__c:number;
  export default npo02__Best_Gift_Year_Total__c;
}
declare module "@salesforce/schema/Contact.npo02__Best_Gift_Year__c" {
  const npo02__Best_Gift_Year__c:string;
  export default npo02__Best_Gift_Year__c;
}
declare module "@salesforce/schema/Contact.npo02__FirstCloseDate__c" {
  const npo02__FirstCloseDate__c:any;
  export default npo02__FirstCloseDate__c;
}
declare module "@salesforce/schema/Contact.npo02__Household_Naming_Order__c" {
  const npo02__Household_Naming_Order__c:number;
  export default npo02__Household_Naming_Order__c;
}
declare module "@salesforce/schema/Contact.npo02__LargestAmount__c" {
  const npo02__LargestAmount__c:number;
  export default npo02__LargestAmount__c;
}
declare module "@salesforce/schema/Contact.npo02__LastCloseDateHH__c" {
  const npo02__LastCloseDateHH__c:any;
  export default npo02__LastCloseDateHH__c;
}
declare module "@salesforce/schema/Contact.npo02__LastCloseDate__c" {
  const npo02__LastCloseDate__c:any;
  export default npo02__LastCloseDate__c;
}
declare module "@salesforce/schema/Contact.npo02__LastMembershipAmount__c" {
  const npo02__LastMembershipAmount__c:number;
  export default npo02__LastMembershipAmount__c;
}
declare module "@salesforce/schema/Contact.npo02__LastMembershipDate__c" {
  const npo02__LastMembershipDate__c:any;
  export default npo02__LastMembershipDate__c;
}
declare module "@salesforce/schema/Contact.npo02__LastMembershipLevel__c" {
  const npo02__LastMembershipLevel__c:string;
  export default npo02__LastMembershipLevel__c;
}
declare module "@salesforce/schema/Contact.npo02__LastMembershipOrigin__c" {
  const npo02__LastMembershipOrigin__c:string;
  export default npo02__LastMembershipOrigin__c;
}
declare module "@salesforce/schema/Contact.npo02__LastOppAmount__c" {
  const npo02__LastOppAmount__c:number;
  export default npo02__LastOppAmount__c;
}
declare module "@salesforce/schema/Contact.npo02__MembershipEndDate__c" {
  const npo02__MembershipEndDate__c:any;
  export default npo02__MembershipEndDate__c;
}
declare module "@salesforce/schema/Contact.npo02__MembershipJoinDate__c" {
  const npo02__MembershipJoinDate__c:any;
  export default npo02__MembershipJoinDate__c;
}
declare module "@salesforce/schema/Contact.npo02__Naming_Exclusions__c" {
  const npo02__Naming_Exclusions__c:string;
  export default npo02__Naming_Exclusions__c;
}
declare module "@salesforce/schema/Contact.npo02__NumberOfClosedOpps__c" {
  const npo02__NumberOfClosedOpps__c:number;
  export default npo02__NumberOfClosedOpps__c;
}
declare module "@salesforce/schema/Contact.npo02__NumberOfMembershipOpps__c" {
  const npo02__NumberOfMembershipOpps__c:number;
  export default npo02__NumberOfMembershipOpps__c;
}
declare module "@salesforce/schema/Contact.npo02__OppAmount2YearsAgo__c" {
  const npo02__OppAmount2YearsAgo__c:number;
  export default npo02__OppAmount2YearsAgo__c;
}
declare module "@salesforce/schema/Contact.npo02__OppAmountLastNDays__c" {
  const npo02__OppAmountLastNDays__c:number;
  export default npo02__OppAmountLastNDays__c;
}
declare module "@salesforce/schema/Contact.npo02__OppAmountLastYearHH__c" {
  const npo02__OppAmountLastYearHH__c:number;
  export default npo02__OppAmountLastYearHH__c;
}
declare module "@salesforce/schema/Contact.npo02__OppAmountLastYear__c" {
  const npo02__OppAmountLastYear__c:number;
  export default npo02__OppAmountLastYear__c;
}
declare module "@salesforce/schema/Contact.npo02__OppAmountThisYearHH__c" {
  const npo02__OppAmountThisYearHH__c:number;
  export default npo02__OppAmountThisYearHH__c;
}
declare module "@salesforce/schema/Contact.npo02__OppAmountThisYear__c" {
  const npo02__OppAmountThisYear__c:number;
  export default npo02__OppAmountThisYear__c;
}
declare module "@salesforce/schema/Contact.npo02__OppsClosed2YearsAgo__c" {
  const npo02__OppsClosed2YearsAgo__c:number;
  export default npo02__OppsClosed2YearsAgo__c;
}
declare module "@salesforce/schema/Contact.npo02__OppsClosedLastNDays__c" {
  const npo02__OppsClosedLastNDays__c:number;
  export default npo02__OppsClosedLastNDays__c;
}
declare module "@salesforce/schema/Contact.npo02__OppsClosedLastYear__c" {
  const npo02__OppsClosedLastYear__c:number;
  export default npo02__OppsClosedLastYear__c;
}
declare module "@salesforce/schema/Contact.npo02__OppsClosedThisYear__c" {
  const npo02__OppsClosedThisYear__c:number;
  export default npo02__OppsClosedThisYear__c;
}
declare module "@salesforce/schema/Contact.npo02__SmallestAmount__c" {
  const npo02__SmallestAmount__c:number;
  export default npo02__SmallestAmount__c;
}
declare module "@salesforce/schema/Contact.npo02__Soft_Credit_Last_Year__c" {
  const npo02__Soft_Credit_Last_Year__c:number;
  export default npo02__Soft_Credit_Last_Year__c;
}
declare module "@salesforce/schema/Contact.npo02__Soft_Credit_This_Year__c" {
  const npo02__Soft_Credit_This_Year__c:number;
  export default npo02__Soft_Credit_This_Year__c;
}
declare module "@salesforce/schema/Contact.npo02__Soft_Credit_Total__c" {
  const npo02__Soft_Credit_Total__c:number;
  export default npo02__Soft_Credit_Total__c;
}
declare module "@salesforce/schema/Contact.npo02__Soft_Credit_Two_Years_Ago__c" {
  const npo02__Soft_Credit_Two_Years_Ago__c:number;
  export default npo02__Soft_Credit_Two_Years_Ago__c;
}
declare module "@salesforce/schema/Contact.npo02__TotalMembershipOppAmount__c" {
  const npo02__TotalMembershipOppAmount__c:number;
  export default npo02__TotalMembershipOppAmount__c;
}
declare module "@salesforce/schema/Contact.npo02__TotalOppAmount__c" {
  const npo02__TotalOppAmount__c:number;
  export default npo02__TotalOppAmount__c;
}
declare module "@salesforce/schema/Contact.npo02__Total_Household_Gifts__c" {
  const npo02__Total_Household_Gifts__c:number;
  export default npo02__Total_Household_Gifts__c;
}
declare module "@salesforce/schema/Contact.npsp__Address_Verification_Status__c" {
  const npsp__Address_Verification_Status__c:string;
  export default npsp__Address_Verification_Status__c;
}
declare module "@salesforce/schema/Contact.npsp__Batch__r" {
  const npsp__Batch__r:any;
  export default npsp__Batch__r;
}
declare module "@salesforce/schema/Contact.npsp__Batch__c" {
  const npsp__Batch__c:any;
  export default npsp__Batch__c;
}
declare module "@salesforce/schema/Contact.npsp__Current_Address__r" {
  const npsp__Current_Address__r:any;
  export default npsp__Current_Address__r;
}
declare module "@salesforce/schema/Contact.npsp__Current_Address__c" {
  const npsp__Current_Address__c:any;
  export default npsp__Current_Address__c;
}
declare module "@salesforce/schema/Contact.npsp__Deceased__c" {
  const npsp__Deceased__c:boolean;
  export default npsp__Deceased__c;
}
declare module "@salesforce/schema/Contact.npsp__Do_Not_Contact__c" {
  const npsp__Do_Not_Contact__c:boolean;
  export default npsp__Do_Not_Contact__c;
}
declare module "@salesforce/schema/Contact.npsp__Exclude_from_Household_Formal_Greeting__c" {
  const npsp__Exclude_from_Household_Formal_Greeting__c:boolean;
  export default npsp__Exclude_from_Household_Formal_Greeting__c;
}
declare module "@salesforce/schema/Contact.npsp__Exclude_from_Household_Informal_Greeting__c" {
  const npsp__Exclude_from_Household_Informal_Greeting__c:boolean;
  export default npsp__Exclude_from_Household_Informal_Greeting__c;
}
declare module "@salesforce/schema/Contact.npsp__Exclude_from_Household_Name__c" {
  const npsp__Exclude_from_Household_Name__c:boolean;
  export default npsp__Exclude_from_Household_Name__c;
}
declare module "@salesforce/schema/Contact.npsp__First_Soft_Credit_Amount__c" {
  const npsp__First_Soft_Credit_Amount__c:number;
  export default npsp__First_Soft_Credit_Amount__c;
}
declare module "@salesforce/schema/Contact.npsp__First_Soft_Credit_Date__c" {
  const npsp__First_Soft_Credit_Date__c:any;
  export default npsp__First_Soft_Credit_Date__c;
}
declare module "@salesforce/schema/Contact.npsp__HHId__c" {
  const npsp__HHId__c:string;
  export default npsp__HHId__c;
}
declare module "@salesforce/schema/Contact.npsp__Largest_Soft_Credit_Amount__c" {
  const npsp__Largest_Soft_Credit_Amount__c:number;
  export default npsp__Largest_Soft_Credit_Amount__c;
}
declare module "@salesforce/schema/Contact.npsp__Largest_Soft_Credit_Date__c" {
  const npsp__Largest_Soft_Credit_Date__c:any;
  export default npsp__Largest_Soft_Credit_Date__c;
}
declare module "@salesforce/schema/Contact.npsp__Last_Soft_Credit_Amount__c" {
  const npsp__Last_Soft_Credit_Amount__c:number;
  export default npsp__Last_Soft_Credit_Amount__c;
}
declare module "@salesforce/schema/Contact.npsp__Last_Soft_Credit_Date__c" {
  const npsp__Last_Soft_Credit_Date__c:any;
  export default npsp__Last_Soft_Credit_Date__c;
}
declare module "@salesforce/schema/Contact.npsp__Number_of_Soft_Credits_Last_N_Days__c" {
  const npsp__Number_of_Soft_Credits_Last_N_Days__c:number;
  export default npsp__Number_of_Soft_Credits_Last_N_Days__c;
}
declare module "@salesforce/schema/Contact.npsp__Number_of_Soft_Credits_Last_Year__c" {
  const npsp__Number_of_Soft_Credits_Last_Year__c:number;
  export default npsp__Number_of_Soft_Credits_Last_Year__c;
}
declare module "@salesforce/schema/Contact.npsp__Number_of_Soft_Credits_This_Year__c" {
  const npsp__Number_of_Soft_Credits_This_Year__c:number;
  export default npsp__Number_of_Soft_Credits_This_Year__c;
}
declare module "@salesforce/schema/Contact.npsp__Number_of_Soft_Credits_Two_Years_Ago__c" {
  const npsp__Number_of_Soft_Credits_Two_Years_Ago__c:number;
  export default npsp__Number_of_Soft_Credits_Two_Years_Ago__c;
}
declare module "@salesforce/schema/Contact.npsp__Number_of_Soft_Credits__c" {
  const npsp__Number_of_Soft_Credits__c:number;
  export default npsp__Number_of_Soft_Credits__c;
}
declare module "@salesforce/schema/Contact.npsp__Primary_Affiliation__r" {
  const npsp__Primary_Affiliation__r:any;
  export default npsp__Primary_Affiliation__r;
}
declare module "@salesforce/schema/Contact.npsp__Primary_Affiliation__c" {
  const npsp__Primary_Affiliation__c:any;
  export default npsp__Primary_Affiliation__c;
}
declare module "@salesforce/schema/Contact.npsp__Primary_Contact__c" {
  const npsp__Primary_Contact__c:boolean;
  export default npsp__Primary_Contact__c;
}
declare module "@salesforce/schema/Contact.npsp__Soft_Credit_Last_N_Days__c" {
  const npsp__Soft_Credit_Last_N_Days__c:number;
  export default npsp__Soft_Credit_Last_N_Days__c;
}
declare module "@salesforce/schema/Contact.npsp__is_Address_Override__c" {
  const npsp__is_Address_Override__c:boolean;
  export default npsp__is_Address_Override__c;
}
declare module "@salesforce/schema/Contact.Gender__c" {
  const Gender__c:string;
  export default Gender__c;
}
declare module "@salesforce/schema/Contact.Alternate_Email__c" {
  const Alternate_Email__c:string;
  export default Alternate_Email__c;
}
declare module "@salesforce/schema/Contact.Ambassador__c" {
  const Ambassador__c:boolean;
  export default Ambassador__c;
}
declare module "@salesforce/schema/Contact.Classroom_Number__c" {
  const Classroom_Number__c:string;
  export default Classroom_Number__c;
}
declare module "@salesforce/schema/Contact.Spotlight_Fund_interest__c" {
  const Spotlight_Fund_interest__c:string;
  export default Spotlight_Fund_interest__c;
}
declare module "@salesforce/schema/Contact.External_ID__c" {
  const External_ID__c:string;
  export default External_ID__c;
}
declare module "@salesforce/schema/Contact.Password__c" {
  const Password__c:string;
  export default Password__c;
}
declare module "@salesforce/schema/Contact.Verified__c" {
  const Verified__c:boolean;
  export default Verified__c;
}
declare module "@salesforce/schema/Contact.AcctSeed__Default_1099_Box__c" {
  const AcctSeed__Default_1099_Box__c:string;
  export default AcctSeed__Default_1099_Box__c;
}
declare module "@salesforce/schema/Contact.AcctSeed__X1099_Vendor__c" {
  const AcctSeed__X1099_Vendor__c:boolean;
  export default AcctSeed__X1099_Vendor__c;
}
declare module "@salesforce/schema/Contact.Active__c" {
  const Active__c:boolean;
  export default Active__c;
}
declare module "@salesforce/schema/Contact.Contact_Status__c" {
  const Contact_Status__c:string;
  export default Contact_Status__c;
}
declare module "@salesforce/schema/Contact.First_Year_of_Teaching__c" {
  const First_Year_of_Teaching__c:string;
  export default First_Year_of_Teaching__c;
}
declare module "@salesforce/schema/Contact.Recovery_Email__c" {
  const Recovery_Email__c:string;
  export default Recovery_Email__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Address_Type__c" {
  const stayclassy__Address_Type__c:string;
  export default stayclassy__Address_Type__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Amount_Donated_in_Last_X_Days__c" {
  const stayclassy__Amount_Donated_in_Last_X_Days__c:number;
  export default stayclassy__Amount_Donated_in_Last_X_Days__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Amount_Recurring_Donations__c" {
  const stayclassy__Amount_Recurring_Donations__c:number;
  export default stayclassy__Amount_Recurring_Donations__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Best_Time_to_Contact__c" {
  const stayclassy__Best_Time_to_Contact__c:string;
  export default stayclassy__Best_Time_to_Contact__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Blog__c" {
  const stayclassy__Blog__c:string;
  export default stayclassy__Blog__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Classy_Fundraising_Page_Total__c" {
  const stayclassy__Classy_Fundraising_Page_Total__c:number;
  export default stayclassy__Classy_Fundraising_Page_Total__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Company__c" {
  const stayclassy__Company__c:string;
  export default stayclassy__Company__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Data_Refreshed_At__c" {
  const stayclassy__Data_Refreshed_At__c:any;
  export default stayclassy__Data_Refreshed_At__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Gender__c" {
  const stayclassy__Gender__c:string;
  export default stayclassy__Gender__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Middle_Name__c" {
  const stayclassy__Middle_Name__c:string;
  export default stayclassy__Middle_Name__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Number_of_Donations_in_Last_X_Days__c" {
  const stayclassy__Number_of_Donations_in_Last_X_Days__c:number;
  export default stayclassy__Number_of_Donations_in_Last_X_Days__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Number_of_Recurring_Donations__c" {
  const stayclassy__Number_of_Recurring_Donations__c:number;
  export default stayclassy__Number_of_Recurring_Donations__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Shirt_Size__c" {
  const stayclassy__Shirt_Size__c:string;
  export default stayclassy__Shirt_Size__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Suffix__c" {
  const stayclassy__Suffix__c:string;
  export default stayclassy__Suffix__c;
}
declare module "@salesforce/schema/Contact.stayclassy__TEST_Email__c" {
  const stayclassy__TEST_Email__c:string;
  export default stayclassy__TEST_Email__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Text_Opt_In__c" {
  const stayclassy__Text_Opt_In__c:boolean;
  export default stayclassy__Text_Opt_In__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Website__c" {
  const stayclassy__Website__c:string;
  export default stayclassy__Website__c;
}
declare module "@salesforce/schema/Contact.stayclassy__amount_of_last_donation__c" {
  const stayclassy__amount_of_last_donation__c:number;
  export default stayclassy__amount_of_last_donation__c;
}
declare module "@salesforce/schema/Contact.stayclassy__average_donation__c" {
  const stayclassy__average_donation__c:number;
  export default stayclassy__average_donation__c;
}
declare module "@salesforce/schema/Contact.stayclassy__date_of_last_donation__c" {
  const stayclassy__date_of_last_donation__c:any;
  export default stayclassy__date_of_last_donation__c;
}
declare module "@salesforce/schema/Contact.stayclassy__opt_in__c" {
  const stayclassy__opt_in__c:boolean;
  export default stayclassy__opt_in__c;
}
declare module "@salesforce/schema/Contact.stayclassy__sc_member_id__c" {
  const stayclassy__sc_member_id__c:number;
  export default stayclassy__sc_member_id__c;
}
declare module "@salesforce/schema/Contact.stayclassy__sc_total_donated__c" {
  const stayclassy__sc_total_donated__c:number;
  export default stayclassy__sc_total_donated__c;
}
declare module "@salesforce/schema/Contact.stayclassy__sc_total_donations__c" {
  const stayclassy__sc_total_donations__c:number;
  export default stayclassy__sc_total_donations__c;
}
declare module "@salesforce/schema/Contact.stayclassy__sc_total_fundraising__c" {
  const stayclassy__sc_total_fundraising__c:number;
  export default stayclassy__sc_total_fundraising__c;
}
declare module "@salesforce/schema/Contact.Billing_Address_Id__c" {
  const Billing_Address_Id__c:string;
  export default Billing_Address_Id__c;
}
declare module "@salesforce/schema/Contact.CommunityUserName__c" {
  const CommunityUserName__c:string;
  export default CommunityUserName__c;
}
declare module "@salesforce/schema/Contact.First_Year_Teaching_Formula__c" {
  const First_Year_Teaching_Formula__c:number;
  export default First_Year_Teaching_Formula__c;
}
declare module "@salesforce/schema/Contact.Mailing_Address_Id__c" {
  const Mailing_Address_Id__c:string;
  export default Mailing_Address_Id__c;
}
declare module "@salesforce/schema/Contact.RegistrationComplete__c" {
  const RegistrationComplete__c:boolean;
  export default RegistrationComplete__c;
}
declare module "@salesforce/schema/Contact.School__r" {
  const School__r:any;
  export default School__r;
}
declare module "@salesforce/schema/Contact.School__c" {
  const School__c:any;
  export default School__c;
}
declare module "@salesforce/schema/Contact.Total_Funds_Raised__c" {
  const Total_Funds_Raised__c:number;
  export default Total_Funds_Raised__c;
}
declare module "@salesforce/schema/Contact.Funds_Available__c" {
  const Funds_Available__c:number;
  export default Funds_Available__c;
}
declare module "@salesforce/schema/Contact.Order_Cloud_Spending_Account_Id__c" {
  const Order_Cloud_Spending_Account_Id__c:string;
  export default Order_Cloud_Spending_Account_Id__c;
}
declare module "@salesforce/schema/Contact.pymt__Google_Buyer_Id__c" {
  const pymt__Google_Buyer_Id__c:number;
  export default pymt__Google_Buyer_Id__c;
}
declare module "@salesforce/schema/Contact.pymt__Hosted_Profiles__c" {
  const pymt__Hosted_Profiles__c:string;
  export default pymt__Hosted_Profiles__c;
}
declare module "@salesforce/schema/Contact.pymt__Member_Pricing_Enabled__c" {
  const pymt__Member_Pricing_Enabled__c:boolean;
  export default pymt__Member_Pricing_Enabled__c;
}
declare module "@salesforce/schema/Contact.pymt__PayPal_Payer_Id__c" {
  const pymt__PayPal_Payer_Id__c:string;
  export default pymt__PayPal_Payer_Id__c;
}
declare module "@salesforce/schema/Contact.DonorDisplayName__c" {
  const DonorDisplayName__c:string;
  export default DonorDisplayName__c;
}
declare module "@salesforce/schema/Contact.AcctSeed__Alternate_Payee_Name__c" {
  const AcctSeed__Alternate_Payee_Name__c:string;
  export default AcctSeed__Alternate_Payee_Name__c;
}
declare module "@salesforce/schema/Contact.AcctSeed__Taxpayer_Identification_Number__c" {
  const AcctSeed__Taxpayer_Identification_Number__c:string;
  export default AcctSeed__Taxpayer_Identification_Number__c;
}
declare module "@salesforce/schema/Contact.Referral_Source__c" {
  const Referral_Source__c:string;
  export default Referral_Source__c;
}
declare module "@salesforce/schema/Contact.Total_Donation_Modified_Date__c" {
  const Total_Donation_Modified_Date__c:any;
  export default Total_Donation_Modified_Date__c;
}
declare module "@salesforce/schema/Contact.Total_Donation__c" {
  const Total_Donation__c:number;
  export default Total_Donation__c;
}
declare module "@salesforce/schema/Contact.UserID__c" {
  const UserID__c:string;
  export default UserID__c;
}
declare module "@salesforce/schema/Contact.Update_contact__c" {
  const Update_contact__c:boolean;
  export default Update_contact__c;
}
declare module "@salesforce/schema/Contact.update_sa__c" {
  const update_sa__c:boolean;
  export default update_sa__c;
}
declare module "@salesforce/schema/Contact.dupcheck__dc3DisableDuplicateCheck__c" {
  const dupcheck__dc3DisableDuplicateCheck__c:boolean;
  export default dupcheck__dc3DisableDuplicateCheck__c;
}
declare module "@salesforce/schema/Contact.dupcheck__dc3Index__c" {
  const dupcheck__dc3Index__c:string;
  export default dupcheck__dc3Index__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Classy_Related_Member_Id__c" {
  const stayclassy__Classy_Related_Member_Id__c:string;
  export default stayclassy__Classy_Related_Member_Id__c;
}
declare module "@salesforce/schema/Contact.Donor_email__r" {
  const Donor_email__r:any;
  export default Donor_email__r;
}
declare module "@salesforce/schema/Contact.Donor_email__c" {
  const Donor_email__c:any;
  export default Donor_email__c;
}
declare module "@salesforce/schema/Contact.Donor_email_address__c" {
  const Donor_email_address__c:string;
  export default Donor_email_address__c;
}
declare module "@salesforce/schema/Contact.stayclassy__Classy_Username__c" {
  const stayclassy__Classy_Username__c:string;
  export default stayclassy__Classy_Username__c;
}
declare module "@salesforce/schema/Contact.State_Postal_Code__c" {
  const State_Postal_Code__c:string;
  export default State_Postal_Code__c;
}
declare module "@salesforce/schema/Contact.Postal_Code_State__c" {
  const Postal_Code_State__c:string;
  export default Postal_Code_State__c;
}
declare module "@salesforce/schema/Contact.Previous_Balance__c" {
  const Previous_Balance__c:number;
  export default Previous_Balance__c;
}
declare module "@salesforce/schema/Contact.Marketing_Email_Opt_In__c" {
  const Marketing_Email_Opt_In__c:boolean;
  export default Marketing_Email_Opt_In__c;
}
declare module "@salesforce/schema/Contact.Potential_Press_Contact__c" {
  const Potential_Press_Contact__c:boolean;
  export default Potential_Press_Contact__c;
}
declare module "@salesforce/schema/Contact.School_SalesforceID__c" {
  const School_SalesforceID__c:string;
  export default School_SalesforceID__c;
}
declare module "@salesforce/schema/Contact.Press_Contact__c" {
  const Press_Contact__c:boolean;
  export default Press_Contact__c;
}
declare module "@salesforce/schema/Contact.AAC_giving_priority__c" {
  const AAC_giving_priority__c:number;
  export default AAC_giving_priority__c;
}
declare module "@salesforce/schema/Contact.School_State__c" {
  const School_State__c:string;
  export default School_State__c;
}
declare module "@salesforce/schema/Contact.Donor_comments_improvements__c" {
  const Donor_comments_improvements__c:string;
  export default Donor_comments_improvements__c;
}
declare module "@salesforce/schema/Contact.Other_crowdfunding_sites_used__c" {
  const Other_crowdfunding_sites_used__c:string;
  export default Other_crowdfunding_sites_used__c;
}
declare module "@salesforce/schema/Contact.Donor_communication_preferences__c" {
  const Donor_communication_preferences__c:string;
  export default Donor_communication_preferences__c;
}
declare module "@salesforce/schema/Contact.Interest_in_Cont_Ed_fund_for_teachers__c" {
  const Interest_in_Cont_Ed_fund_for_teachers__c:number;
  export default Interest_in_Cont_Ed_fund_for_teachers__c;
}
declare module "@salesforce/schema/Contact.Interest_in_school_accounts__c" {
  const Interest_in_school_accounts__c:number;
  export default Interest_in_school_accounts__c;
}
declare module "@salesforce/schema/Contact.Why_I_give__c" {
  const Why_I_give__c:string;
  export default Why_I_give__c;
}
declare module "@salesforce/schema/Contact.Donor_opinion_on_AAC__c" {
  const Donor_opinion_on_AAC__c:string;
  export default Donor_opinion_on_AAC__c;
}
declare module "@salesforce/schema/Contact.Crowdfunding_sites_other__c" {
  const Crowdfunding_sites_other__c:string;
  export default Crowdfunding_sites_other__c;
}
declare module "@salesforce/schema/Contact.Why_support_school_accounts__c" {
  const Why_support_school_accounts__c:string;
  export default Why_support_school_accounts__c;
}
declare module "@salesforce/schema/Contact.Opposed_to_school_accounts__c" {
  const Opposed_to_school_accounts__c:string;
  export default Opposed_to_school_accounts__c;
}
declare module "@salesforce/schema/Contact.Support_cont_ed_fund_for_teachers__c" {
  const Support_cont_ed_fund_for_teachers__c:string;
  export default Support_cont_ed_fund_for_teachers__c;
}
declare module "@salesforce/schema/Contact.Opposed_to_cont_ed_fund_for_teachers__c" {
  const Opposed_to_cont_ed_fund_for_teachers__c:string;
  export default Opposed_to_cont_ed_fund_for_teachers__c;
}
declare module "@salesforce/schema/Contact.Personal_motivation_for_giving__c" {
  const Personal_motivation_for_giving__c:string;
  export default Personal_motivation_for_giving__c;
}
declare module "@salesforce/schema/Contact.Giving_preferences_other__c" {
  const Giving_preferences_other__c:string;
  export default Giving_preferences_other__c;
}
declare module "@salesforce/schema/Contact.Donor_communication_preferences_other__c" {
  const Donor_communication_preferences_other__c:string;
  export default Donor_communication_preferences_other__c;
}
declare module "@salesforce/schema/Contact.Annual_Budget__c" {
  const Annual_Budget__c:number;
  export default Annual_Budget__c;
}
declare module "@salesforce/schema/Contact.Estimated_Annual_Spending__c" {
  const Estimated_Annual_Spending__c:number;
  export default Estimated_Annual_Spending__c;
}
declare module "@salesforce/schema/Contact.Estimate_of_Change_in_Budget__c" {
  const Estimate_of_Change_in_Budget__c:string;
  export default Estimate_of_Change_in_Budget__c;
}
declare module "@salesforce/schema/Contact.Students_Can_t_Afford__c" {
  const Students_Can_t_Afford__c:number;
  export default Students_Can_t_Afford__c;
}
declare module "@salesforce/schema/Contact.Basic_Supplies__c" {
  const Basic_Supplies__c:string;
  export default Basic_Supplies__c;
}
declare module "@salesforce/schema/Contact.Supplies_bought_on_Own__c" {
  const Supplies_bought_on_Own__c:string;
  export default Supplies_bought_on_Own__c;
}
declare module "@salesforce/schema/Contact.Supplies_for_Fundraising__c" {
  const Supplies_for_Fundraising__c:string;
  export default Supplies_for_Fundraising__c;
}
declare module "@salesforce/schema/Contact.Other_Crowdfunding__c" {
  const Other_Crowdfunding__c:string;
  export default Other_Crowdfunding__c;
}
declare module "@salesforce/schema/Contact.Addl_Comments__c" {
  const Addl_Comments__c:string;
  export default Addl_Comments__c;
}
declare module "@salesforce/schema/Contact.Welcome_Ref__c" {
  const Welcome_Ref__c:string;
  export default Welcome_Ref__c;
}
declare module "@salesforce/schema/Contact.Welcome_Why_Signed_Up__c" {
  const Welcome_Why_Signed_Up__c:string;
  export default Welcome_Why_Signed_Up__c;
}
declare module "@salesforce/schema/Contact.Welcome_Vendors__c" {
  const Welcome_Vendors__c:string;
  export default Welcome_Vendors__c;
}
declare module "@salesforce/schema/Contact.Welcome_Other_Sites__c" {
  const Welcome_Other_Sites__c:string;
  export default Welcome_Other_Sites__c;
}
declare module "@salesforce/schema/Contact.Teacher_Referral__c" {
  const Teacher_Referral__c:string;
  export default Teacher_Referral__c;
}
declare module "@salesforce/schema/Contact.School_Principal__c" {
  const School_Principal__c:boolean;
  export default School_Principal__c;
}
declare module "@salesforce/schema/Contact.Title__c" {
  const Title__c:string;
  export default Title__c;
}
declare module "@salesforce/schema/Contact.Primary_School_Wide_Title_1__c" {
  const Primary_School_Wide_Title_1__c:boolean;
  export default Primary_School_Wide_Title_1__c;
}
declare module "@salesforce/schema/Contact.Do_Not_Match_per_School_Record__c" {
  const Do_Not_Match_per_School_Record__c:boolean;
  export default Do_Not_Match_per_School_Record__c;
}
declare module "@salesforce/schema/Contact.No_of_Employees__c" {
  const No_of_Employees__c:string;
  export default No_of_Employees__c;
}
declare module "@salesforce/schema/Contact.Company_Revenue__c" {
  const Company_Revenue__c:string;
  export default Company_Revenue__c;
}
declare module "@salesforce/schema/Contact.Company_Type__c" {
  const Company_Type__c:string;
  export default Company_Type__c;
}
declare module "@salesforce/schema/Contact.Company_Website__c" {
  const Company_Website__c:string;
  export default Company_Website__c;
}
declare module "@salesforce/schema/Contact.Company_Ticker__c" {
  const Company_Ticker__c:string;
  export default Company_Ticker__c;
}
declare module "@salesforce/schema/Contact.Wealth_Score_360__c" {
  const Wealth_Score_360__c:string;
  export default Wealth_Score_360__c;
}
declare module "@salesforce/schema/Contact.Annual_Compensation__c" {
  const Annual_Compensation__c:number;
  export default Annual_Compensation__c;
}
declare module "@salesforce/schema/Contact.Stock_Sales_in_Last_Year__c" {
  const Stock_Sales_in_Last_Year__c:number;
  export default Stock_Sales_in_Last_Year__c;
}
declare module "@salesforce/schema/Contact.Estimated_Holdings__c" {
  const Estimated_Holdings__c:number;
  export default Estimated_Holdings__c;
}
declare module "@salesforce/schema/Contact.Company_HEPData__r" {
  const Company_HEPData__r:any;
  export default Company_HEPData__r;
}
declare module "@salesforce/schema/Contact.Company_HEPData__c" {
  const Company_HEPData__c:any;
  export default Company_HEPData__c;
}
declare module "@salesforce/schema/Contact.Company_Revenue_HEPData__c" {
  const Company_Revenue_HEPData__c:string;
  export default Company_Revenue_HEPData__c;
}
declare module "@salesforce/schema/Contact.Industry__c" {
  const Industry__c:string;
  export default Industry__c;
}
declare module "@salesforce/schema/Contact.Sector__c" {
  const Sector__c:string;
  export default Sector__c;
}
declare module "@salesforce/schema/Contact.First_Gift_Date__c" {
  const First_Gift_Date__c:any;
  export default First_Gift_Date__c;
}
declare module "@salesforce/schema/Contact.First_Gift_by_Fiscal_Month__c" {
  const First_Gift_by_Fiscal_Month__c:string;
  export default First_Gift_by_Fiscal_Month__c;
}
declare module "@salesforce/schema/Contact.Annual_Report_Opt_In__c" {
  const Annual_Report_Opt_In__c:boolean;
  export default Annual_Report_Opt_In__c;
}
declare module "@salesforce/schema/Contact.Primary_School_Title_1__c" {
  const Primary_School_Title_1__c:boolean;
  export default Primary_School_Title_1__c;
}
declare module "@salesforce/schema/Contact.Primary_School_NCESID__c" {
  const Primary_School_NCESID__c:string;
  export default Primary_School_NCESID__c;
}
declare module "@salesforce/schema/Contact.Last_Expiring_Fund_Notification_Date__c" {
  const Last_Expiring_Fund_Notification_Date__c:any;
  export default Last_Expiring_Fund_Notification_Date__c;
}
declare module "@salesforce/schema/Contact.Account_Managed_By__c" {
  const Account_Managed_By__c:string;
  export default Account_Managed_By__c;
}
declare module "@salesforce/schema/Contact.Account_Manager_Email__c" {
  const Account_Manager_Email__c:string;
  export default Account_Manager_Email__c;
}
declare module "@salesforce/schema/Contact.School_Stage__c" {
  const School_Stage__c:string;
  export default School_Stage__c;
}
declare module "@salesforce/schema/Contact.District_Level__c" {
  const District_Level__c:boolean;
  export default District_Level__c;
}
declare module "@salesforce/schema/Contact.Funds_Raised_Last_18_Months__c" {
  const Funds_Raised_Last_18_Months__c:number;
  export default Funds_Raised_Last_18_Months__c;
}
declare module "@salesforce/schema/Contact.Reviewed__c" {
  const Reviewed__c:boolean;
  export default Reviewed__c;
}
declare module "@salesforce/schema/Contact.ToSyncOrderCloud__c" {
  const ToSyncOrderCloud__c:boolean;
  export default ToSyncOrderCloud__c;
}
declare module "@salesforce/schema/Contact.Donors_Last_18_Months__c" {
  const Donors_Last_18_Months__c:number;
  export default Donors_Last_18_Months__c;
}
declare module "@salesforce/schema/Contact.DS360oi__Background_Batch_Flag__c" {
  const DS360oi__Background_Batch_Flag__c:boolean;
  export default DS360oi__Background_Batch_Flag__c;
}
declare module "@salesforce/schema/Contact.DS360oi__DS_update__c" {
  const DS360oi__DS_update__c:string;
  export default DS360oi__DS_update__c;
}
declare module "@salesforce/schema/Contact.DS360oi__DS_update_date__c" {
  const DS360oi__DS_update_date__c:any;
  export default DS360oi__DS_update_date__c;
}
declare module "@salesforce/schema/Contact.DS360oi__Middle_Initial_Name__c" {
  const DS360oi__Middle_Initial_Name__c:string;
  export default DS360oi__Middle_Initial_Name__c;
}
declare module "@salesforce/schema/Contact.DS360oi__X5_Postal_code__c" {
  const DS360oi__X5_Postal_code__c:number;
  export default DS360oi__X5_Postal_code__c;
}
