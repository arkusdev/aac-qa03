declare module "@salesforce/schema/Lead.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Lead.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Lead.MasterRecord" {
  const MasterRecord:any;
  export default MasterRecord;
}
declare module "@salesforce/schema/Lead.MasterRecordId" {
  const MasterRecordId:any;
  export default MasterRecordId;
}
declare module "@salesforce/schema/Lead.LastName" {
  const LastName:string;
  export default LastName;
}
declare module "@salesforce/schema/Lead.FirstName" {
  const FirstName:string;
  export default FirstName;
}
declare module "@salesforce/schema/Lead.Salutation" {
  const Salutation:string;
  export default Salutation;
}
declare module "@salesforce/schema/Lead.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Lead.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/Lead.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/Lead.Title" {
  const Title:string;
  export default Title;
}
declare module "@salesforce/schema/Lead.Company" {
  const Company:string;
  export default Company;
}
declare module "@salesforce/schema/Lead.Street" {
  const Street:string;
  export default Street;
}
declare module "@salesforce/schema/Lead.City" {
  const City:string;
  export default City;
}
declare module "@salesforce/schema/Lead.State" {
  const State:string;
  export default State;
}
declare module "@salesforce/schema/Lead.PostalCode" {
  const PostalCode:string;
  export default PostalCode;
}
declare module "@salesforce/schema/Lead.Country" {
  const Country:string;
  export default Country;
}
declare module "@salesforce/schema/Lead.StateCode" {
  const StateCode:string;
  export default StateCode;
}
declare module "@salesforce/schema/Lead.CountryCode" {
  const CountryCode:string;
  export default CountryCode;
}
declare module "@salesforce/schema/Lead.Latitude" {
  const Latitude:number;
  export default Latitude;
}
declare module "@salesforce/schema/Lead.Longitude" {
  const Longitude:number;
  export default Longitude;
}
declare module "@salesforce/schema/Lead.GeocodeAccuracy" {
  const GeocodeAccuracy:string;
  export default GeocodeAccuracy;
}
declare module "@salesforce/schema/Lead.Address" {
  const Address:any;
  export default Address;
}
declare module "@salesforce/schema/Lead.Phone" {
  const Phone:string;
  export default Phone;
}
declare module "@salesforce/schema/Lead.MobilePhone" {
  const MobilePhone:string;
  export default MobilePhone;
}
declare module "@salesforce/schema/Lead.Fax" {
  const Fax:string;
  export default Fax;
}
declare module "@salesforce/schema/Lead.Email" {
  const Email:string;
  export default Email;
}
declare module "@salesforce/schema/Lead.Website" {
  const Website:string;
  export default Website;
}
declare module "@salesforce/schema/Lead.PhotoUrl" {
  const PhotoUrl:string;
  export default PhotoUrl;
}
declare module "@salesforce/schema/Lead.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Lead.LeadSource" {
  const LeadSource:string;
  export default LeadSource;
}
declare module "@salesforce/schema/Lead.Status" {
  const Status:string;
  export default Status;
}
declare module "@salesforce/schema/Lead.Industry" {
  const Industry:string;
  export default Industry;
}
declare module "@salesforce/schema/Lead.Rating" {
  const Rating:string;
  export default Rating;
}
declare module "@salesforce/schema/Lead.AnnualRevenue" {
  const AnnualRevenue:number;
  export default AnnualRevenue;
}
declare module "@salesforce/schema/Lead.NumberOfEmployees" {
  const NumberOfEmployees:number;
  export default NumberOfEmployees;
}
declare module "@salesforce/schema/Lead.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Lead.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Lead.IsConverted" {
  const IsConverted:boolean;
  export default IsConverted;
}
declare module "@salesforce/schema/Lead.ConvertedDate" {
  const ConvertedDate:any;
  export default ConvertedDate;
}
declare module "@salesforce/schema/Lead.ConvertedAccount" {
  const ConvertedAccount:any;
  export default ConvertedAccount;
}
declare module "@salesforce/schema/Lead.ConvertedAccountId" {
  const ConvertedAccountId:any;
  export default ConvertedAccountId;
}
declare module "@salesforce/schema/Lead.ConvertedContact" {
  const ConvertedContact:any;
  export default ConvertedContact;
}
declare module "@salesforce/schema/Lead.ConvertedContactId" {
  const ConvertedContactId:any;
  export default ConvertedContactId;
}
declare module "@salesforce/schema/Lead.ConvertedOpportunity" {
  const ConvertedOpportunity:any;
  export default ConvertedOpportunity;
}
declare module "@salesforce/schema/Lead.ConvertedOpportunityId" {
  const ConvertedOpportunityId:any;
  export default ConvertedOpportunityId;
}
declare module "@salesforce/schema/Lead.IsUnreadByOwner" {
  const IsUnreadByOwner:boolean;
  export default IsUnreadByOwner;
}
declare module "@salesforce/schema/Lead.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Lead.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Lead.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Lead.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Lead.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Lead.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Lead.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Lead.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Lead.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Lead.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Lead.Jigsaw" {
  const Jigsaw:string;
  export default Jigsaw;
}
declare module "@salesforce/schema/Lead.JigsawContactId" {
  const JigsawContactId:string;
  export default JigsawContactId;
}
declare module "@salesforce/schema/Lead.EmailBouncedReason" {
  const EmailBouncedReason:string;
  export default EmailBouncedReason;
}
declare module "@salesforce/schema/Lead.EmailBouncedDate" {
  const EmailBouncedDate:any;
  export default EmailBouncedDate;
}
declare module "@salesforce/schema/Lead.Individual" {
  const Individual:any;
  export default Individual;
}
declare module "@salesforce/schema/Lead.IndividualId" {
  const IndividualId:any;
  export default IndividualId;
}
declare module "@salesforce/schema/Lead.DS360oi__Background_Batch_Flag__c" {
  const DS360oi__Background_Batch_Flag__c:boolean;
  export default DS360oi__Background_Batch_Flag__c;
}
declare module "@salesforce/schema/Lead.DS360oi__DS_update__c" {
  const DS360oi__DS_update__c:string;
  export default DS360oi__DS_update__c;
}
declare module "@salesforce/schema/Lead.DS360oi__DS_update_date__c" {
  const DS360oi__DS_update_date__c:any;
  export default DS360oi__DS_update_date__c;
}
declare module "@salesforce/schema/Lead.DS360oi__Middle_Initial_Name__c" {
  const DS360oi__Middle_Initial_Name__c:string;
  export default DS360oi__Middle_Initial_Name__c;
}
declare module "@salesforce/schema/Lead.DS360oi__X5_Postal_code__c" {
  const DS360oi__X5_Postal_code__c:number;
  export default DS360oi__X5_Postal_code__c;
}
declare module "@salesforce/schema/Lead.npe01__Preferred_Email__c" {
  const npe01__Preferred_Email__c:string;
  export default npe01__Preferred_Email__c;
}
declare module "@salesforce/schema/Lead.npe01__Preferred_Phone__c" {
  const npe01__Preferred_Phone__c:string;
  export default npe01__Preferred_Phone__c;
}
declare module "@salesforce/schema/Lead.npsp__Batch__r" {
  const npsp__Batch__r:any;
  export default npsp__Batch__r;
}
declare module "@salesforce/schema/Lead.npsp__Batch__c" {
  const npsp__Batch__c:any;
  export default npsp__Batch__c;
}
declare module "@salesforce/schema/Lead.npsp__CompanyCity__c" {
  const npsp__CompanyCity__c:string;
  export default npsp__CompanyCity__c;
}
declare module "@salesforce/schema/Lead.npsp__CompanyCountry__c" {
  const npsp__CompanyCountry__c:string;
  export default npsp__CompanyCountry__c;
}
declare module "@salesforce/schema/Lead.npsp__CompanyPostalCode__c" {
  const npsp__CompanyPostalCode__c:string;
  export default npsp__CompanyPostalCode__c;
}
declare module "@salesforce/schema/Lead.npsp__CompanyState__c" {
  const npsp__CompanyState__c:string;
  export default npsp__CompanyState__c;
}
declare module "@salesforce/schema/Lead.npsp__CompanyStreet__c" {
  const npsp__CompanyStreet__c:string;
  export default npsp__CompanyStreet__c;
}
declare module "@salesforce/schema/Lead.dupcheck__dc3DisableDuplicateCheck__c" {
  const dupcheck__dc3DisableDuplicateCheck__c:boolean;
  export default dupcheck__dc3DisableDuplicateCheck__c;
}
declare module "@salesforce/schema/Lead.dupcheck__dc3Index__c" {
  const dupcheck__dc3Index__c:string;
  export default dupcheck__dc3Index__c;
}
declare module "@salesforce/schema/Lead.dupcheck__dc3Web2Lead__c" {
  const dupcheck__dc3Web2Lead__c:boolean;
  export default dupcheck__dc3Web2Lead__c;
}
declare module "@salesforce/schema/Lead.Assigned_to_AAC_Board_Member__r" {
  const Assigned_to_AAC_Board_Member__r:any;
  export default Assigned_to_AAC_Board_Member__r;
}
declare module "@salesforce/schema/Lead.Assigned_to_AAC_Board_Member__c" {
  const Assigned_to_AAC_Board_Member__c:any;
  export default Assigned_to_AAC_Board_Member__c;
}
declare module "@salesforce/schema/Lead.Assigned_to_AAC_Staff__r" {
  const Assigned_to_AAC_Staff__r:any;
  export default Assigned_to_AAC_Staff__r;
}
declare module "@salesforce/schema/Lead.Assigned_to_AAC_Staff__c" {
  const Assigned_to_AAC_Staff__c:any;
  export default Assigned_to_AAC_Staff__c;
}
