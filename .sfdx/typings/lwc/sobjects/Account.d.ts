declare module "@salesforce/schema/Account.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Account.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Account.MasterRecord" {
  const MasterRecord:any;
  export default MasterRecord;
}
declare module "@salesforce/schema/Account.MasterRecordId" {
  const MasterRecordId:any;
  export default MasterRecordId;
}
declare module "@salesforce/schema/Account.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Account.Type" {
  const Type:string;
  export default Type;
}
declare module "@salesforce/schema/Account.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/Account.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/Account.Parent" {
  const Parent:any;
  export default Parent;
}
declare module "@salesforce/schema/Account.ParentId" {
  const ParentId:any;
  export default ParentId;
}
declare module "@salesforce/schema/Account.BillingStreet" {
  const BillingStreet:string;
  export default BillingStreet;
}
declare module "@salesforce/schema/Account.BillingCity" {
  const BillingCity:string;
  export default BillingCity;
}
declare module "@salesforce/schema/Account.BillingState" {
  const BillingState:string;
  export default BillingState;
}
declare module "@salesforce/schema/Account.BillingPostalCode" {
  const BillingPostalCode:string;
  export default BillingPostalCode;
}
declare module "@salesforce/schema/Account.BillingCountry" {
  const BillingCountry:string;
  export default BillingCountry;
}
declare module "@salesforce/schema/Account.BillingStateCode" {
  const BillingStateCode:string;
  export default BillingStateCode;
}
declare module "@salesforce/schema/Account.BillingCountryCode" {
  const BillingCountryCode:string;
  export default BillingCountryCode;
}
declare module "@salesforce/schema/Account.BillingLatitude" {
  const BillingLatitude:number;
  export default BillingLatitude;
}
declare module "@salesforce/schema/Account.BillingLongitude" {
  const BillingLongitude:number;
  export default BillingLongitude;
}
declare module "@salesforce/schema/Account.BillingGeocodeAccuracy" {
  const BillingGeocodeAccuracy:string;
  export default BillingGeocodeAccuracy;
}
declare module "@salesforce/schema/Account.BillingAddress" {
  const BillingAddress:any;
  export default BillingAddress;
}
declare module "@salesforce/schema/Account.ShippingStreet" {
  const ShippingStreet:string;
  export default ShippingStreet;
}
declare module "@salesforce/schema/Account.ShippingCity" {
  const ShippingCity:string;
  export default ShippingCity;
}
declare module "@salesforce/schema/Account.ShippingState" {
  const ShippingState:string;
  export default ShippingState;
}
declare module "@salesforce/schema/Account.ShippingPostalCode" {
  const ShippingPostalCode:string;
  export default ShippingPostalCode;
}
declare module "@salesforce/schema/Account.ShippingCountry" {
  const ShippingCountry:string;
  export default ShippingCountry;
}
declare module "@salesforce/schema/Account.ShippingStateCode" {
  const ShippingStateCode:string;
  export default ShippingStateCode;
}
declare module "@salesforce/schema/Account.ShippingCountryCode" {
  const ShippingCountryCode:string;
  export default ShippingCountryCode;
}
declare module "@salesforce/schema/Account.ShippingLatitude" {
  const ShippingLatitude:number;
  export default ShippingLatitude;
}
declare module "@salesforce/schema/Account.ShippingLongitude" {
  const ShippingLongitude:number;
  export default ShippingLongitude;
}
declare module "@salesforce/schema/Account.ShippingGeocodeAccuracy" {
  const ShippingGeocodeAccuracy:string;
  export default ShippingGeocodeAccuracy;
}
declare module "@salesforce/schema/Account.ShippingAddress" {
  const ShippingAddress:any;
  export default ShippingAddress;
}
declare module "@salesforce/schema/Account.Phone" {
  const Phone:string;
  export default Phone;
}
declare module "@salesforce/schema/Account.Fax" {
  const Fax:string;
  export default Fax;
}
declare module "@salesforce/schema/Account.AccountNumber" {
  const AccountNumber:string;
  export default AccountNumber;
}
declare module "@salesforce/schema/Account.Website" {
  const Website:string;
  export default Website;
}
declare module "@salesforce/schema/Account.PhotoUrl" {
  const PhotoUrl:string;
  export default PhotoUrl;
}
declare module "@salesforce/schema/Account.Sic" {
  const Sic:string;
  export default Sic;
}
declare module "@salesforce/schema/Account.Industry" {
  const Industry:string;
  export default Industry;
}
declare module "@salesforce/schema/Account.AnnualRevenue" {
  const AnnualRevenue:number;
  export default AnnualRevenue;
}
declare module "@salesforce/schema/Account.NumberOfEmployees" {
  const NumberOfEmployees:number;
  export default NumberOfEmployees;
}
declare module "@salesforce/schema/Account.Ownership" {
  const Ownership:string;
  export default Ownership;
}
declare module "@salesforce/schema/Account.TickerSymbol" {
  const TickerSymbol:string;
  export default TickerSymbol;
}
declare module "@salesforce/schema/Account.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Account.Rating" {
  const Rating:string;
  export default Rating;
}
declare module "@salesforce/schema/Account.Site" {
  const Site:string;
  export default Site;
}
declare module "@salesforce/schema/Account.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Account.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Account.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Account.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Account.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Account.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Account.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Account.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Account.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Account.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Account.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Account.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Account.IsCustomerPortal" {
  const IsCustomerPortal:boolean;
  export default IsCustomerPortal;
}
declare module "@salesforce/schema/Account.Jigsaw" {
  const Jigsaw:string;
  export default Jigsaw;
}
declare module "@salesforce/schema/Account.JigsawCompanyId" {
  const JigsawCompanyId:string;
  export default JigsawCompanyId;
}
declare module "@salesforce/schema/Account.AccountSource" {
  const AccountSource:string;
  export default AccountSource;
}
declare module "@salesforce/schema/Account.SicDesc" {
  const SicDesc:string;
  export default SicDesc;
}
declare module "@salesforce/schema/Account.npe01__One2OneContact__r" {
  const npe01__One2OneContact__r:any;
  export default npe01__One2OneContact__r;
}
declare module "@salesforce/schema/Account.npe01__One2OneContact__c" {
  const npe01__One2OneContact__c:any;
  export default npe01__One2OneContact__c;
}
declare module "@salesforce/schema/Account.npe01__SYSTEMIsIndividual__c" {
  const npe01__SYSTEMIsIndividual__c:boolean;
  export default npe01__SYSTEMIsIndividual__c;
}
declare module "@salesforce/schema/Account.npe01__FirstDonationDate__c" {
  const npe01__FirstDonationDate__c:any;
  export default npe01__FirstDonationDate__c;
}
declare module "@salesforce/schema/Account.npe01__LastDonationDate__c" {
  const npe01__LastDonationDate__c:any;
  export default npe01__LastDonationDate__c;
}
declare module "@salesforce/schema/Account.npe01__LifetimeDonationHistory_Amount__c" {
  const npe01__LifetimeDonationHistory_Amount__c:number;
  export default npe01__LifetimeDonationHistory_Amount__c;
}
declare module "@salesforce/schema/Account.npe01__LifetimeDonationHistory_Number__c" {
  const npe01__LifetimeDonationHistory_Number__c:number;
  export default npe01__LifetimeDonationHistory_Number__c;
}
declare module "@salesforce/schema/Account.Email__c" {
  const Email__c:string;
  export default Email__c;
}
declare module "@salesforce/schema/Account.npe01__SYSTEM_AccountType__c" {
  const npe01__SYSTEM_AccountType__c:string;
  export default npe01__SYSTEM_AccountType__c;
}
declare module "@salesforce/schema/Account.npo02__AverageAmount__c" {
  const npo02__AverageAmount__c:number;
  export default npo02__AverageAmount__c;
}
declare module "@salesforce/schema/Account.npo02__Best_Gift_Year_Total__c" {
  const npo02__Best_Gift_Year_Total__c:number;
  export default npo02__Best_Gift_Year_Total__c;
}
declare module "@salesforce/schema/Account.npo02__Best_Gift_Year__c" {
  const npo02__Best_Gift_Year__c:string;
  export default npo02__Best_Gift_Year__c;
}
declare module "@salesforce/schema/Account.npo02__FirstCloseDate__c" {
  const npo02__FirstCloseDate__c:any;
  export default npo02__FirstCloseDate__c;
}
declare module "@salesforce/schema/Account.npo02__Formal_Greeting__c" {
  const npo02__Formal_Greeting__c:string;
  export default npo02__Formal_Greeting__c;
}
declare module "@salesforce/schema/Account.npo02__HouseholdPhone__c" {
  const npo02__HouseholdPhone__c:string;
  export default npo02__HouseholdPhone__c;
}
declare module "@salesforce/schema/Account.npo02__Informal_Greeting__c" {
  const npo02__Informal_Greeting__c:string;
  export default npo02__Informal_Greeting__c;
}
declare module "@salesforce/schema/Account.npo02__LargestAmount__c" {
  const npo02__LargestAmount__c:number;
  export default npo02__LargestAmount__c;
}
declare module "@salesforce/schema/Account.npo02__LastCloseDate__c" {
  const npo02__LastCloseDate__c:any;
  export default npo02__LastCloseDate__c;
}
declare module "@salesforce/schema/Account.npo02__LastMembershipAmount__c" {
  const npo02__LastMembershipAmount__c:number;
  export default npo02__LastMembershipAmount__c;
}
declare module "@salesforce/schema/Account.npo02__LastMembershipDate__c" {
  const npo02__LastMembershipDate__c:any;
  export default npo02__LastMembershipDate__c;
}
declare module "@salesforce/schema/Account.npo02__LastMembershipLevel__c" {
  const npo02__LastMembershipLevel__c:string;
  export default npo02__LastMembershipLevel__c;
}
declare module "@salesforce/schema/Account.npo02__LastMembershipOrigin__c" {
  const npo02__LastMembershipOrigin__c:string;
  export default npo02__LastMembershipOrigin__c;
}
declare module "@salesforce/schema/Account.npo02__LastOppAmount__c" {
  const npo02__LastOppAmount__c:number;
  export default npo02__LastOppAmount__c;
}
declare module "@salesforce/schema/Account.npo02__MembershipEndDate__c" {
  const npo02__MembershipEndDate__c:any;
  export default npo02__MembershipEndDate__c;
}
declare module "@salesforce/schema/Account.npo02__MembershipJoinDate__c" {
  const npo02__MembershipJoinDate__c:any;
  export default npo02__MembershipJoinDate__c;
}
declare module "@salesforce/schema/Account.npo02__NumberOfClosedOpps__c" {
  const npo02__NumberOfClosedOpps__c:number;
  export default npo02__NumberOfClosedOpps__c;
}
declare module "@salesforce/schema/Account.npo02__NumberOfMembershipOpps__c" {
  const npo02__NumberOfMembershipOpps__c:number;
  export default npo02__NumberOfMembershipOpps__c;
}
declare module "@salesforce/schema/Account.npo02__OppAmount2YearsAgo__c" {
  const npo02__OppAmount2YearsAgo__c:number;
  export default npo02__OppAmount2YearsAgo__c;
}
declare module "@salesforce/schema/Account.npo02__OppAmountLastNDays__c" {
  const npo02__OppAmountLastNDays__c:number;
  export default npo02__OppAmountLastNDays__c;
}
declare module "@salesforce/schema/Account.npo02__OppAmountLastYear__c" {
  const npo02__OppAmountLastYear__c:number;
  export default npo02__OppAmountLastYear__c;
}
declare module "@salesforce/schema/Account.npo02__OppAmountThisYear__c" {
  const npo02__OppAmountThisYear__c:number;
  export default npo02__OppAmountThisYear__c;
}
declare module "@salesforce/schema/Account.npo02__OppsClosed2YearsAgo__c" {
  const npo02__OppsClosed2YearsAgo__c:number;
  export default npo02__OppsClosed2YearsAgo__c;
}
declare module "@salesforce/schema/Account.npo02__OppsClosedLastNDays__c" {
  const npo02__OppsClosedLastNDays__c:number;
  export default npo02__OppsClosedLastNDays__c;
}
declare module "@salesforce/schema/Account.npo02__OppsClosedLastYear__c" {
  const npo02__OppsClosedLastYear__c:number;
  export default npo02__OppsClosedLastYear__c;
}
declare module "@salesforce/schema/Account.npo02__OppsClosedThisYear__c" {
  const npo02__OppsClosedThisYear__c:number;
  export default npo02__OppsClosedThisYear__c;
}
declare module "@salesforce/schema/Account.npo02__SYSTEM_CUSTOM_NAMING__c" {
  const npo02__SYSTEM_CUSTOM_NAMING__c:string;
  export default npo02__SYSTEM_CUSTOM_NAMING__c;
}
declare module "@salesforce/schema/Account.npo02__SmallestAmount__c" {
  const npo02__SmallestAmount__c:number;
  export default npo02__SmallestAmount__c;
}
declare module "@salesforce/schema/Account.npo02__TotalMembershipOppAmount__c" {
  const npo02__TotalMembershipOppAmount__c:number;
  export default npo02__TotalMembershipOppAmount__c;
}
declare module "@salesforce/schema/Account.npo02__TotalOppAmount__c" {
  const npo02__TotalOppAmount__c:number;
  export default npo02__TotalOppAmount__c;
}
declare module "@salesforce/schema/Account.npsp__Batch__r" {
  const npsp__Batch__r:any;
  export default npsp__Batch__r;
}
declare module "@salesforce/schema/Account.npsp__Batch__c" {
  const npsp__Batch__c:any;
  export default npsp__Batch__c;
}
declare module "@salesforce/schema/Account.npsp__Funding_Focus__c" {
  const npsp__Funding_Focus__c:string;
  export default npsp__Funding_Focus__c;
}
declare module "@salesforce/schema/Account.npsp__Grantmaker__c" {
  const npsp__Grantmaker__c:boolean;
  export default npsp__Grantmaker__c;
}
declare module "@salesforce/schema/Account.npsp__Matching_Gift_Administrator_Name__c" {
  const npsp__Matching_Gift_Administrator_Name__c:string;
  export default npsp__Matching_Gift_Administrator_Name__c;
}
declare module "@salesforce/schema/Account.npsp__Matching_Gift_Amount_Max__c" {
  const npsp__Matching_Gift_Amount_Max__c:number;
  export default npsp__Matching_Gift_Amount_Max__c;
}
declare module "@salesforce/schema/Account.npsp__Matching_Gift_Amount_Min__c" {
  const npsp__Matching_Gift_Amount_Min__c:number;
  export default npsp__Matching_Gift_Amount_Min__c;
}
declare module "@salesforce/schema/Account.npsp__Matching_Gift_Annual_Employee_Max__c" {
  const npsp__Matching_Gift_Annual_Employee_Max__c:number;
  export default npsp__Matching_Gift_Annual_Employee_Max__c;
}
declare module "@salesforce/schema/Account.npsp__Matching_Gift_Comments__c" {
  const npsp__Matching_Gift_Comments__c:string;
  export default npsp__Matching_Gift_Comments__c;
}
declare module "@salesforce/schema/Account.npsp__Matching_Gift_Company__c" {
  const npsp__Matching_Gift_Company__c:boolean;
  export default npsp__Matching_Gift_Company__c;
}
declare module "@salesforce/schema/Account.npsp__Matching_Gift_Email__c" {
  const npsp__Matching_Gift_Email__c:string;
  export default npsp__Matching_Gift_Email__c;
}
declare module "@salesforce/schema/Account.npsp__Matching_Gift_Info_Updated__c" {
  const npsp__Matching_Gift_Info_Updated__c:any;
  export default npsp__Matching_Gift_Info_Updated__c;
}
declare module "@salesforce/schema/Account.npsp__Matching_Gift_Percent__c" {
  const npsp__Matching_Gift_Percent__c:number;
  export default npsp__Matching_Gift_Percent__c;
}
declare module "@salesforce/schema/Account.npsp__Matching_Gift_Phone__c" {
  const npsp__Matching_Gift_Phone__c:string;
  export default npsp__Matching_Gift_Phone__c;
}
declare module "@salesforce/schema/Account.npsp__Matching_Gift_Request_Deadline__c" {
  const npsp__Matching_Gift_Request_Deadline__c:string;
  export default npsp__Matching_Gift_Request_Deadline__c;
}
declare module "@salesforce/schema/Account.npsp__Membership_Span__c" {
  const npsp__Membership_Span__c:number;
  export default npsp__Membership_Span__c;
}
declare module "@salesforce/schema/Account.npsp__Membership_Status__c" {
  const npsp__Membership_Status__c:string;
  export default npsp__Membership_Status__c;
}
declare module "@salesforce/schema/Account.npsp__Number_of_Household_Members__c" {
  const npsp__Number_of_Household_Members__c:number;
  export default npsp__Number_of_Household_Members__c;
}
declare module "@salesforce/schema/Account.Charter__c" {
  const Charter__c:boolean;
  export default Charter__c;
}
declare module "@salesforce/schema/Account.Discount__c" {
  const Discount__c:number;
  export default Discount__c;
}
declare module "@salesforce/schema/Account.External_ID__c" {
  const External_ID__c:string;
  export default External_ID__c;
}
declare module "@salesforce/schema/Account.Flat_Fee__c" {
  const Flat_Fee__c:number;
  export default Flat_Fee__c;
}
declare module "@salesforce/schema/Account.High_Grade__c" {
  const High_Grade__c:string;
  export default High_Grade__c;
}
declare module "@salesforce/schema/Account.Level__c" {
  const Level__c:string;
  export default Level__c;
}
declare module "@salesforce/schema/Account.Low_Grade__c" {
  const Low_Grade__c:string;
  export default Low_Grade__c;
}
declare module "@salesforce/schema/Account.Minimum_Purchase__c" {
  const Minimum_Purchase__c:number;
  export default Minimum_Purchase__c;
}
declare module "@salesforce/schema/Account.NCESSCHID__c" {
  const NCESSCHID__c:string;
  export default NCESSCHID__c;
}
declare module "@salesforce/schema/Account.SW_Title_1__c" {
  const SW_Title_1__c:boolean;
  export default SW_Title_1__c;
}
declare module "@salesforce/schema/Account.Shipping_Handling_Fee__c" {
  const Shipping_Handling_Fee__c:number;
  export default Shipping_Handling_Fee__c;
}
declare module "@salesforce/schema/Account.Title_1__c" {
  const Title_1__c:boolean;
  export default Title_1__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Account_Payable_Terms__c" {
  const AcctSeed__Account_Payable_Terms__c:number;
  export default AcctSeed__Account_Payable_Terms__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Accounting_Active__c" {
  const AcctSeed__Accounting_Active__c:boolean;
  export default AcctSeed__Accounting_Active__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Accounting_Type__c" {
  const AcctSeed__Accounting_Type__c:string;
  export default AcctSeed__Accounting_Type__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Activity_Statement_Format__r" {
  const AcctSeed__Activity_Statement_Format__r:any;
  export default AcctSeed__Activity_Statement_Format__r;
}
declare module "@salesforce/schema/Account.AcctSeed__Activity_Statement_Format__c" {
  const AcctSeed__Activity_Statement_Format__c:any;
  export default AcctSeed__Activity_Statement_Format__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Billing_Contact__r" {
  const AcctSeed__Billing_Contact__r:any;
  export default AcctSeed__Billing_Contact__r;
}
declare module "@salesforce/schema/Account.AcctSeed__Billing_Contact__c" {
  const AcctSeed__Billing_Contact__c:any;
  export default AcctSeed__Billing_Contact__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Billing_Days_Due__c" {
  const AcctSeed__Billing_Days_Due__c:number;
  export default AcctSeed__Billing_Days_Due__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Billing_Discount_Days_Due__c" {
  const AcctSeed__Billing_Discount_Days_Due__c:number;
  export default AcctSeed__Billing_Discount_Days_Due__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Billing_Discount_Percent__c" {
  const AcctSeed__Billing_Discount_Percent__c:number;
  export default AcctSeed__Billing_Discount_Percent__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Billing_Email__c" {
  const AcctSeed__Billing_Email__c:string;
  export default AcctSeed__Billing_Email__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Billing_Terms_Name__c" {
  const AcctSeed__Billing_Terms_Name__c:string;
  export default AcctSeed__Billing_Terms_Name__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Credit_Card_Vendor__c" {
  const AcctSeed__Credit_Card_Vendor__c:boolean;
  export default AcctSeed__Credit_Card_Vendor__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Default_1099_Box__c" {
  const AcctSeed__Default_1099_Box__c:string;
  export default AcctSeed__Default_1099_Box__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Default_Expense_GL_Account__r" {
  const AcctSeed__Default_Expense_GL_Account__r:any;
  export default AcctSeed__Default_Expense_GL_Account__r;
}
declare module "@salesforce/schema/Account.AcctSeed__Default_Expense_GL_Account__c" {
  const AcctSeed__Default_Expense_GL_Account__c:any;
  export default AcctSeed__Default_Expense_GL_Account__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Discount_Days_Due__c" {
  const AcctSeed__Discount_Days_Due__c:number;
  export default AcctSeed__Discount_Days_Due__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Discount_Percent__c" {
  const AcctSeed__Discount_Percent__c:number;
  export default AcctSeed__Discount_Percent__c;
}
declare module "@salesforce/schema/Account.AcctSeed__File_Import_Match_Name__c" {
  const AcctSeed__File_Import_Match_Name__c:string;
  export default AcctSeed__File_Import_Match_Name__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Outstanding_Statement_Format__r" {
  const AcctSeed__Outstanding_Statement_Format__r:any;
  export default AcctSeed__Outstanding_Statement_Format__r;
}
declare module "@salesforce/schema/Account.AcctSeed__Outstanding_Statement_Format__c" {
  const AcctSeed__Outstanding_Statement_Format__c:any;
  export default AcctSeed__Outstanding_Statement_Format__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Shipping_Contact__r" {
  const AcctSeed__Shipping_Contact__r:any;
  export default AcctSeed__Shipping_Contact__r;
}
declare module "@salesforce/schema/Account.AcctSeed__Shipping_Contact__c" {
  const AcctSeed__Shipping_Contact__c:any;
  export default AcctSeed__Shipping_Contact__c;
}
declare module "@salesforce/schema/Account.AcctSeed__X1099_Vendor__c" {
  const AcctSeed__X1099_Vendor__c:boolean;
  export default AcctSeed__X1099_Vendor__c;
}
declare module "@salesforce/schema/Account.Donations_Received__c" {
  const Donations_Received__c:number;
  export default Donations_Received__c;
}
declare module "@salesforce/schema/Account.NCES_District_ID__c" {
  const NCES_District_ID__c:string;
  export default NCES_District_ID__c;
}
declare module "@salesforce/schema/Account.NCES_School_ID__c" {
  const NCES_School_ID__c:string;
  export default NCES_School_ID__c;
}
declare module "@salesforce/schema/Account.Tax_ID__c" {
  const Tax_ID__c:string;
  export default Tax_ID__c;
}
declare module "@salesforce/schema/Account.Total_Funds_Raised__c" {
  const Total_Funds_Raised__c:number;
  export default Total_Funds_Raised__c;
}
declare module "@salesforce/schema/Account.Usual_Account_Name__c" {
  const Usual_Account_Name__c:string;
  export default Usual_Account_Name__c;
}
declare module "@salesforce/schema/Account.ELL_District__c" {
  const ELL_District__c:string;
  export default ELL_District__c;
}
declare module "@salesforce/schema/Account.stayclassy__Active__c" {
  const stayclassy__Active__c:string;
  export default stayclassy__Active__c;
}
declare module "@salesforce/schema/Account.stayclassy__NumberofLocations__c" {
  const stayclassy__NumberofLocations__c:number;
  export default stayclassy__NumberofLocations__c;
}
declare module "@salesforce/schema/Account.Account_Status__c" {
  const Account_Status__c:string;
  export default Account_Status__c;
}
declare module "@salesforce/schema/Account.School_Type__c" {
  const School_Type__c:string;
  export default School_Type__c;
}
declare module "@salesforce/schema/Account.Verified__c" {
  const Verified__c:boolean;
  export default Verified__c;
}
declare module "@salesforce/schema/Account.pymt__Convert_To_Person_Account__c" {
  const pymt__Convert_To_Person_Account__c:boolean;
  export default pymt__Convert_To_Person_Account__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Alternate_Payee_Name__c" {
  const AcctSeed__Alternate_Payee_Name__c:string;
  export default AcctSeed__Alternate_Payee_Name__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Billing_Format__r" {
  const AcctSeed__Billing_Format__r:any;
  export default AcctSeed__Billing_Format__r;
}
declare module "@salesforce/schema/Account.AcctSeed__Billing_Format__c" {
  const AcctSeed__Billing_Format__c:any;
  export default AcctSeed__Billing_Format__c;
}
declare module "@salesforce/schema/Account.AcctSeed__GL_Account_Variable_1__r" {
  const AcctSeed__GL_Account_Variable_1__r:any;
  export default AcctSeed__GL_Account_Variable_1__r;
}
declare module "@salesforce/schema/Account.AcctSeed__GL_Account_Variable_1__c" {
  const AcctSeed__GL_Account_Variable_1__c:any;
  export default AcctSeed__GL_Account_Variable_1__c;
}
declare module "@salesforce/schema/Account.AcctSeed__GL_Account_Variable_2__r" {
  const AcctSeed__GL_Account_Variable_2__r:any;
  export default AcctSeed__GL_Account_Variable_2__r;
}
declare module "@salesforce/schema/Account.AcctSeed__GL_Account_Variable_2__c" {
  const AcctSeed__GL_Account_Variable_2__c:any;
  export default AcctSeed__GL_Account_Variable_2__c;
}
declare module "@salesforce/schema/Account.AcctSeed__GL_Account_Variable_3__r" {
  const AcctSeed__GL_Account_Variable_3__r:any;
  export default AcctSeed__GL_Account_Variable_3__r;
}
declare module "@salesforce/schema/Account.AcctSeed__GL_Account_Variable_3__c" {
  const AcctSeed__GL_Account_Variable_3__c:any;
  export default AcctSeed__GL_Account_Variable_3__c;
}
declare module "@salesforce/schema/Account.AcctSeed__GL_Account_Variable_4__r" {
  const AcctSeed__GL_Account_Variable_4__r:any;
  export default AcctSeed__GL_Account_Variable_4__r;
}
declare module "@salesforce/schema/Account.AcctSeed__GL_Account_Variable_4__c" {
  const AcctSeed__GL_Account_Variable_4__c:any;
  export default AcctSeed__GL_Account_Variable_4__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Stripe_Customer_Id__c" {
  const AcctSeed__Stripe_Customer_Id__c:string;
  export default AcctSeed__Stripe_Customer_Id__c;
}
declare module "@salesforce/schema/Account.AcctSeed__Taxpayer_Identification_Number__c" {
  const AcctSeed__Taxpayer_Identification_Number__c:string;
  export default AcctSeed__Taxpayer_Identification_Number__c;
}
declare module "@salesforce/schema/Account.dupcheck__dc3DisableDuplicateCheck__c" {
  const dupcheck__dc3DisableDuplicateCheck__c:boolean;
  export default dupcheck__dc3DisableDuplicateCheck__c;
}
declare module "@salesforce/schema/Account.dupcheck__dc3Index__c" {
  const dupcheck__dc3Index__c:string;
  export default dupcheck__dc3Index__c;
}
declare module "@salesforce/schema/Account.zaapit__Hierarchy_Level__c" {
  const zaapit__Hierarchy_Level__c:number;
  export default zaapit__Hierarchy_Level__c;
}
declare module "@salesforce/schema/Account.Order_Cloud_Vendor_Name__c" {
  const Order_Cloud_Vendor_Name__c:string;
  export default Order_Cloud_Vendor_Name__c;
}
declare module "@salesforce/schema/Account.Parent_Account__c" {
  const Parent_Account__c:string;
  export default Parent_Account__c;
}
declare module "@salesforce/schema/Account.State_Postal_Code__c" {
  const State_Postal_Code__c:string;
  export default State_Postal_Code__c;
}
declare module "@salesforce/schema/Account.Donor_Display_name__c" {
  const Donor_Display_name__c:string;
  export default Donor_Display_name__c;
}
declare module "@salesforce/schema/Account.Affil_state_postal_code__c" {
  const Affil_state_postal_code__c:string;
  export default Affil_state_postal_code__c;
}
declare module "@salesforce/schema/Account.Invoice_vendor_name__c" {
  const Invoice_vendor_name__c:string;
  export default Invoice_vendor_name__c;
}
declare module "@salesforce/schema/Account.Assigned_Manager__c" {
  const Assigned_Manager__c:string;
  export default Assigned_Manager__c;
}
declare module "@salesforce/schema/Account.No_of_Employees_HEPData__c" {
  const No_of_Employees_HEPData__c:string;
  export default No_of_Employees_HEPData__c;
}
declare module "@salesforce/schema/Account.Do_Not_Match_for_Corporate__c" {
  const Do_Not_Match_for_Corporate__c:boolean;
  export default Do_Not_Match_for_Corporate__c;
}
declare module "@salesforce/schema/Account.Company_Revenue_HEPData__c" {
  const Company_Revenue_HEPData__c:string;
  export default Company_Revenue_HEPData__c;
}
declare module "@salesforce/schema/Account.Preferred_Partner_School__c" {
  const Preferred_Partner_School__c:boolean;
  export default Preferred_Partner_School__c;
}
declare module "@salesforce/schema/Account.Industry__c" {
  const Industry__c:string;
  export default Industry__c;
}
declare module "@salesforce/schema/Account.Sector_HEPData__c" {
  const Sector_HEPData__c:string;
  export default Sector_HEPData__c;
}
declare module "@salesforce/schema/Account.DigitalCheck__Account_Number__c" {
  const DigitalCheck__Account_Number__c:string;
  export default DigitalCheck__Account_Number__c;
}
declare module "@salesforce/schema/Account.DigitalCheck__Routing_Number__c" {
  const DigitalCheck__Routing_Number__c:string;
  export default DigitalCheck__Routing_Number__c;
}
declare module "@salesforce/schema/Account.Power_of_1__c" {
  const Power_of_1__c:number;
  export default Power_of_1__c;
}
declare module "@salesforce/schema/Account.Metro_Status__c" {
  const Metro_Status__c:string;
  export default Metro_Status__c;
}
declare module "@salesforce/schema/Account.Latitude__c" {
  const Latitude__c:string;
  export default Latitude__c;
}
declare module "@salesforce/schema/Account.Longitude__c" {
  const Longitude__c:string;
  export default Longitude__c;
}
declare module "@salesforce/schema/Account.Parent_Institution_ID__c" {
  const Parent_Institution_ID__c:string;
  export default Parent_Institution_ID__c;
}
declare module "@salesforce/schema/Account.Parent_Institution_Name__r" {
  const Parent_Institution_Name__r:any;
  export default Parent_Institution_Name__r;
}
declare module "@salesforce/schema/Account.Parent_Institution_Name__c" {
  const Parent_Institution_Name__c:any;
  export default Parent_Institution_Name__c;
}
declare module "@salesforce/schema/Account.MCH_Institution_ID__c" {
  const MCH_Institution_ID__c:string;
  export default MCH_Institution_ID__c;
}
declare module "@salesforce/schema/Account.Enrollment_African_American__c" {
  const Enrollment_African_American__c:number;
  export default Enrollment_African_American__c;
}
declare module "@salesforce/schema/Account.Enrollment_Asian__c" {
  const Enrollment_Asian__c:number;
  export default Enrollment_Asian__c;
}
declare module "@salesforce/schema/Account.Enrollment_Caucasian__c" {
  const Enrollment_Caucasian__c:number;
  export default Enrollment_Caucasian__c;
}
declare module "@salesforce/schema/Account.Enrollment_Hispanic__c" {
  const Enrollment_Hispanic__c:number;
  export default Enrollment_Hispanic__c;
}
declare module "@salesforce/schema/Account.Enrollment_Native_American__c" {
  const Enrollment_Native_American__c:number;
  export default Enrollment_Native_American__c;
}
declare module "@salesforce/schema/Account.Student_Enrollment__c" {
  const Student_Enrollment__c:number;
  export default Student_Enrollment__c;
}
declare module "@salesforce/schema/Account.Student_to_Teacher_Ratio__c" {
  const Student_to_Teacher_Ratio__c:number;
  export default Student_to_Teacher_Ratio__c;
}
declare module "@salesforce/schema/Account.Number_of_Teachers__c" {
  const Number_of_Teachers__c:number;
  export default Number_of_Teachers__c;
}
declare module "@salesforce/schema/Account.Grade_Span__c" {
  const Grade_Span__c:string;
  export default Grade_Span__c;
}
declare module "@salesforce/schema/Account.IEP_District__c" {
  const IEP_District__c:string;
  export default IEP_District__c;
}
declare module "@salesforce/schema/Account.AAC_High_Needs_Rating__c" {
  const AAC_High_Needs_Rating__c:number;
  export default AAC_High_Needs_Rating__c;
}
declare module "@salesforce/schema/Account.School_wide_Title_I_MCH__c" {
  const School_wide_Title_I_MCH__c:boolean;
  export default School_wide_Title_I_MCH__c;
}
declare module "@salesforce/schema/Account.Free_Reduced_Lunch_Percent__c" {
  const Free_Reduced_Lunch_Percent__c:number;
  export default Free_Reduced_Lunch_Percent__c;
}
declare module "@salesforce/schema/Account.School_Types_MCH__c" {
  const School_Types_MCH__c:string;
  export default School_Types_MCH__c;
}
declare module "@salesforce/schema/Account.MCH_Ed_Account__c" {
  const MCH_Ed_Account__c:string;
  export default MCH_Ed_Account__c;
}
declare module "@salesforce/schema/Account.Instructional_Expenditures_per_Pupil__c" {
  const Instructional_Expenditures_per_Pupil__c:number;
  export default Instructional_Expenditures_per_Pupil__c;
}
declare module "@salesforce/schema/Account.AAC_Demographics_Rating__c" {
  const AAC_Demographics_Rating__c:number;
  export default AAC_Demographics_Rating__c;
}
declare module "@salesforce/schema/Account.ELL_Students__c" {
  const ELL_Students__c:string;
  export default ELL_Students__c;
}
declare module "@salesforce/schema/Account.IEP_Students__c" {
  const IEP_Students__c:string;
  export default IEP_Students__c;
}
declare module "@salesforce/schema/Account.Number_of_Schools_in_District__c" {
  const Number_of_Schools_in_District__c:number;
  export default Number_of_Schools_in_District__c;
}
declare module "@salesforce/schema/Account.AAC_IEP_and_ELL_Rating__c" {
  const AAC_IEP_and_ELL_Rating__c:number;
  export default AAC_IEP_and_ELL_Rating__c;
}
declare module "@salesforce/schema/Account.District__r" {
  const District__r:any;
  export default District__r;
}
declare module "@salesforce/schema/Account.District__c" {
  const District__c:any;
  export default District__c;
}
declare module "@salesforce/schema/Account.DS360oi__Active__c" {
  const DS360oi__Active__c:string;
  export default DS360oi__Active__c;
}
declare module "@salesforce/schema/Account.DS360oi__Background_Batch_Flag__c" {
  const DS360oi__Background_Batch_Flag__c:boolean;
  export default DS360oi__Background_Batch_Flag__c;
}
declare module "@salesforce/schema/Account.DS360oi__CustomerPriority__c" {
  const DS360oi__CustomerPriority__c:string;
  export default DS360oi__CustomerPriority__c;
}
declare module "@salesforce/schema/Account.DS360oi__DS_update__c" {
  const DS360oi__DS_update__c:string;
  export default DS360oi__DS_update__c;
}
declare module "@salesforce/schema/Account.DS360oi__DS_update_date__c" {
  const DS360oi__DS_update_date__c:any;
  export default DS360oi__DS_update_date__c;
}
declare module "@salesforce/schema/Account.DS360oi__Middle_Initial_Name__c" {
  const DS360oi__Middle_Initial_Name__c:string;
  export default DS360oi__Middle_Initial_Name__c;
}
declare module "@salesforce/schema/Account.DS360oi__NumberofLocations__c" {
  const DS360oi__NumberofLocations__c:number;
  export default DS360oi__NumberofLocations__c;
}
declare module "@salesforce/schema/Account.DS360oi__SLAExpirationDate__c" {
  const DS360oi__SLAExpirationDate__c:any;
  export default DS360oi__SLAExpirationDate__c;
}
declare module "@salesforce/schema/Account.DS360oi__SLASerialNumber__c" {
  const DS360oi__SLASerialNumber__c:string;
  export default DS360oi__SLASerialNumber__c;
}
declare module "@salesforce/schema/Account.DS360oi__SLA__c" {
  const DS360oi__SLA__c:string;
  export default DS360oi__SLA__c;
}
declare module "@salesforce/schema/Account.DS360oi__UpsellOpportunity__c" {
  const DS360oi__UpsellOpportunity__c:string;
  export default DS360oi__UpsellOpportunity__c;
}
