declare module "@salesforce/apex/ProfilePictureController.getProfilePictureNew" {
  export default function getProfilePictureNew(param: {recordId: any, pictureType: any}): Promise<any>;
}
declare module "@salesforce/apex/ProfilePictureController.getProfilePicture" {
  export default function getProfilePicture(param: {recordId: any, pictureType: any}): Promise<any>;
}
declare module "@salesforce/apex/ProfilePictureController.saveAttachment" {
  export default function saveAttachment(param: {recordId: any, pictureType: any, base64Data: any, contentType: any}): Promise<any>;
}
declare module "@salesforce/apex/ProfilePictureController.getClassroom" {
  export default function getClassroom(param: {userId: any}): Promise<any>;
}
declare module "@salesforce/apex/ProfilePictureController.getContactDataForUser" {
  export default function getContactDataForUser(param: {userId: any}): Promise<any>;
}
