declare module "@salesforce/apex/ShoppingCartController.getClassroomById" {
  export default function getClassroomById(param: {classroomId: any}): Promise<any>;
}
declare module "@salesforce/apex/ShoppingCartController.getNextMonthDate" {
  export default function getNextMonthDate(): Promise<any>;
}
