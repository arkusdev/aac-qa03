declare module "@salesforce/apex/ThanksYourDonorsController.findDonations" {
  export default function findDonations(): Promise<any>;
}
declare module "@salesforce/apex/ThanksYourDonorsController.getBaseUrlSettings" {
  export default function getBaseUrlSettings(): Promise<any>;
}
declare module "@salesforce/apex/ThanksYourDonorsController.fetchUser" {
  export default function fetchUser(): Promise<any>;
}
