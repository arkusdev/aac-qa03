declare module "@salesforce/apex/ShopController.hasActiveClassroom" {
  export default function hasActiveClassroom(param: {userId: any}): Promise<any>;
}
declare module "@salesforce/apex/ShopController.generateShopUrl" {
  export default function generateShopUrl(param: {userId: any}): Promise<any>;
}
