declare module "@salesforce/apex/DesignationController.getDesignation" {
  export default function getDesignation(): Promise<any>;
}
