declare module "@salesforce/apex/FieldSetController.getCurrentUser" {
  export default function getCurrentUser(): Promise<any>;
}
declare module "@salesforce/apex/FieldSetController.getCurrentContact" {
  export default function getCurrentContact(): Promise<any>;
}
declare module "@salesforce/apex/FieldSetController.getTypeNames" {
  export default function getTypeNames(): Promise<any>;
}
declare module "@salesforce/apex/FieldSetController.hasFieldSets" {
  export default function hasFieldSets(param: {typeName: any}): Promise<any>;
}
declare module "@salesforce/apex/FieldSetController.getFieldSetNames" {
  export default function getFieldSetNames(param: {typeName: any}): Promise<any>;
}
declare module "@salesforce/apex/FieldSetController.getFields" {
  export default function getFields(param: {typeName: any, fsName: any}): Promise<any>;
}
declare module "@salesforce/apex/FieldSetController.updateFields" {
  export default function updateFields(param: {currentContact: any}): Promise<any>;
}
