declare module "@salesforce/apex/Teacher_Dashboard.getClassroomData" {
  export default function getClassroomData(): Promise<any>;
}
