declare module "@salesforce/apex/DonorDetailsController.savePaymentData" {
  export default function savePaymentData(param: {c: any, a: any, payment: any, cartItemString: any, inHonorOfName: any, subTotal: any, total: any, monthlySubTotal: any, monthlyTotal: any, fee: any, monthlyFee: any}): Promise<any>;
}
declare module "@salesforce/apex/DonorDetailsController.upsertContact" {
  export default function upsertContact(param: {c: any}): Promise<any>;
}
declare module "@salesforce/apex/DonorDetailsController.upsertAccount" {
  export default function upsertAccount(param: {a: any, donorType: any}): Promise<any>;
}
declare module "@salesforce/apex/DonorDetailsController.getFAQs" {
  export default function getFAQs(): Promise<any>;
}
declare module "@salesforce/apex/DonorDetailsController.cookiesSet" {
  export default function cookiesSet(): Promise<any>;
}
