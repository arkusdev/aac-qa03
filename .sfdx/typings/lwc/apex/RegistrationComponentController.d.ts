declare module "@salesforce/apex/RegistrationComponentController.createUser" {
  export default function createUser(param: {contact: any}): Promise<any>;
}
declare module "@salesforce/apex/RegistrationComponentController.getIsUsernameTaken" {
  export default function getIsUsernameTaken(param: {un: any}): Promise<any>;
}
