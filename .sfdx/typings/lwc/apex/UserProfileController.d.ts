declare module "@salesforce/apex/UserProfileController.getContactForUser" {
  export default function getContactForUser(param: {userId: any, fieldSetNames: any}): Promise<any>;
}
declare module "@salesforce/apex/UserProfileController.updateContact" {
  export default function updateContact(param: {c: any}): Promise<any>;
}
