declare module "@salesforce/apex/BannerUpload.getBanner" {
  export default function getBanner(): Promise<any>;
}
declare module "@salesforce/apex/BannerUpload.setBanner" {
  export default function setBanner(param: {base64Data: any, fileName: any}): Promise<any>;
}
