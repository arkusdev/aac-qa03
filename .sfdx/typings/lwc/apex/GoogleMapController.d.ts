declare module "@salesforce/apex/GoogleMapController.queryRecord" {
  export default function queryRecord(param: {recordId: any}): Promise<any>;
}
