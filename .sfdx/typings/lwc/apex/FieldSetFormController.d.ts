declare module "@salesforce/apex/FieldSetFormController.getFieldsForFieldSet" {
  export default function getFieldsForFieldSet(param: {currentObject: any, fieldSetName: any}): Promise<any>;
}
declare module "@salesforce/apex/FieldSetFormController.getDescribeInfoForObject" {
  export default function getDescribeInfoForObject(param: {currentObject: any}): Promise<any>;
}
