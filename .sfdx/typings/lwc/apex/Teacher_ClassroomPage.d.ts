declare module "@salesforce/apex/Teacher_ClassroomPage.getData" {
  export default function getData(): Promise<any>;
}
declare module "@salesforce/apex/Teacher_ClassroomPage.saveRecord" {
  export default function saveRecord(param: {classToSave: any}): Promise<any>;
}
