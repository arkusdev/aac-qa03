declare module "@salesforce/apex/PaymentTypeSelectionController.updatePaymentAndReturnLink" {
  export default function updatePaymentAndReturnLink(param: {payment: any}): Promise<any>;
}
