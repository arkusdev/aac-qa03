declare module "@salesforce/apex/ChargentDonorDetailsController.savePaymentData" {
  export default function savePaymentData(param: {donorComment: any, c: any, a: any, payment: any, cartItemString: any, inHonorOfName: any, inHonorOfEmail: any, paymentPromoCode: any, subTotal: any, total: any, monthlySubTotal: any, grandTotal: any, expFees: any, monthlyTotal: any, fee: any, helpAmount: any, monthlyFee: any, isProcessingFeeIncluded: any, myPicklist: any, endDate: any}): Promise<any>;
}
declare module "@salesforce/apex/ChargentDonorDetailsController.upsertContact" {
  export default function upsertContact(param: {c: any}): Promise<any>;
}
declare module "@salesforce/apex/ChargentDonorDetailsController.upsertAccount" {
  export default function upsertAccount(param: {a: any, donorType: any}): Promise<any>;
}
declare module "@salesforce/apex/ChargentDonorDetailsController.getFAQs" {
  export default function getFAQs(): Promise<any>;
}
declare module "@salesforce/apex/ChargentDonorDetailsController.getTributeType" {
  export default function getTributeType(): Promise<any>;
}
