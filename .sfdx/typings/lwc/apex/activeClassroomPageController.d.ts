declare module "@salesforce/apex/activeClassroomPageController.active" {
  export default function active(): Promise<any>;
}
