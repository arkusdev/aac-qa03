declare module "@salesforce/apex/Teacher_Search.getRecordType" {
  export default function getRecordType(): Promise<any>;
}
declare module "@salesforce/apex/Teacher_Search.searchSchool" {
  export default function searchSchool(param: {parametersToSearch: any}): Promise<any>;
}
declare module "@salesforce/apex/Teacher_Search.saveSchool" {
  export default function saveSchool(param: {accId: any, classId: any}): Promise<any>;
}
declare module "@salesforce/apex/Teacher_Search.getClassroom" {
  export default function getClassroom(): Promise<any>;
}
