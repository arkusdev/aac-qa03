declare module "@salesforce/apex/ClassroomLandingPageController.getData" {
  export default function getData(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/ClassroomLandingPageController.saveNotify" {
  export default function saveNotify(param: {notifyStr: any}): Promise<any>;
}
declare module "@salesforce/apex/ClassroomLandingPageController.fetchUser" {
  export default function fetchUser(): Promise<any>;
}
