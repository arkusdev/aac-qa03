declare module "@salesforce/apex/RegistrationRedirectController.getCurrentUser" {
  export default function getCurrentUser(): Promise<any>;
}
declare module "@salesforce/apex/RegistrationRedirectController.getCurrentContact" {
  export default function getCurrentContact(): Promise<any>;
}
