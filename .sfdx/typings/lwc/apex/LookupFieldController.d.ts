declare module "@salesforce/apex/LookupFieldController.getLookupSearchResults" {
  export default function getLookupSearchResults(param: {searchTerm: any, objectType: any}): Promise<any>;
}
declare module "@salesforce/apex/LookupFieldController.getFieldList" {
  export default function getFieldList(param: {objectName: any}): Promise<any>;
}
declare module "@salesforce/apex/LookupFieldController.getLookupObjectById" {
  export default function getLookupObjectById(param: {objectId: any}): Promise<any>;
}
