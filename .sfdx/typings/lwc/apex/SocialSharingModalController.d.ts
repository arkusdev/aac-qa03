declare module "@salesforce/apex/SocialSharingModalController.getClassroom" {
  export default function getClassroom(param: {objectId: any}): Promise<any>;
}
