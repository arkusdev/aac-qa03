declare module "@salesforce/apex/FundThisClassroomController.getClassroom" {
  export default function getClassroom(param: {classroomId: any}): Promise<any>;
}
