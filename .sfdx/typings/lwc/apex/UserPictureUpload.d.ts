declare module "@salesforce/apex/UserPictureUpload.getProfilePicture" {
  export default function getProfilePicture(): Promise<any>;
}
declare module "@salesforce/apex/UserPictureUpload.getUser" {
  export default function getUser(param: {userId: any}): Promise<any>;
}
declare module "@salesforce/apex/UserPictureUpload.saveAttachment" {
  export default function saveAttachment(param: {base64Data: any, contentType: any}): Promise<any>;
}
