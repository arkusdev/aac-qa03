declare module "@salesforce/apex/Teacher_Transaction_History.getTransactions" {
  export default function getTransactions(param: {teacherId: any}): Promise<any>;
}
