declare module "@salesforce/apex/ActivityController.getData" {
  export default function getData(param: {classroomId: any}): Promise<any>;
}
declare module "@salesforce/apex/ActivityController.saveRecord" {
  export default function saveRecord(param: {record: any}): Promise<any>;
}
declare module "@salesforce/apex/ActivityController.getRecord" {
  export default function getRecord(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/ActivityController.setComment" {
  export default function setComment(param: {recordId: any, comment: any}): Promise<any>;
}
declare module "@salesforce/apex/ActivityController.toggleAComment" {
  export default function toggleAComment(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/ActivityController.delPhoto" {
  export default function delPhoto(param: {fileId: any}): Promise<any>;
}
