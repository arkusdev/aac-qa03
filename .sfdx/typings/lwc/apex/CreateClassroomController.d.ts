declare module "@salesforce/apex/CreateClassroomController.updateClassroom" {
  export default function updateClassroom(param: {c: any}): Promise<any>;
}
declare module "@salesforce/apex/CreateClassroomController.updateSchool" {
  export default function updateSchool(param: {s: any}): Promise<any>;
}
declare module "@salesforce/apex/CreateClassroomController.getContactFromUser" {
  export default function getContactFromUser(param: {userId: any, classRoom: any}): Promise<any>;
}
declare module "@salesforce/apex/CreateClassroomController.getRecordType" {
  export default function getRecordType(param: {devName: any}): Promise<any>;
}
