declare module "@salesforce/apex/CartController.cartInit" {
  export default function cartInit(): Promise<any>;
}
declare module "@salesforce/apex/CartController.cartSubmit" {
  export default function cartSubmit(param: {cart: any}): Promise<any>;
}
declare module "@salesforce/apex/CartController.paymentRequest" {
  export default function paymentRequest(param: {corderId: any, cart: any}): Promise<any>;
}
declare module "@salesforce/apex/CartController.getGTMData" {
  export default function getGTMData(param: {recordId: any}): Promise<any>;
}
