/**
 * @description Content Document Link Trigger process
 */
trigger ContentDocumentLinkTriggerSchool on ContentDocumentLink ( after insert) {

    /**
     * @description Get updates related classroom with the PDF file is uploaded
     */
    ContentDocumentLinkTriggerSchoolHandler.checkPDFFile();
}