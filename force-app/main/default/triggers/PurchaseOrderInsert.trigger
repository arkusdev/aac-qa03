trigger PurchaseOrderInsert on OpportunityLineItem (after insert) {
	
	List<account> vendorList = new List<account>();
	set<string> vendorsname = new set<string>();
	set<Id> opptyId = new set<id>();
	Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();

	for(OpportunityLineItem oli : Trigger.New) {
			opptyId.add(oli.OpportunityId); 
	}

	List<OpportunityLineItem> opptylineitemList = new List<OpportunityLineItem>();
	if(opptyId.size() > 0) {

		opptylineitemList = [SELECT Vendor_name__c,
									Opportunity.Teacher__r.Name,
									VendorOrderId__c,ProductCode,
									Product2.Name,
									Quantity,
									product2.punchoutproduct__c,
									UnitPrice,Opportunity.OrderCloudLineItemCount__c,
									Opportunity.OpportunityProductCount__c,
									Opportunity.Order_Cloud_Order_Id__c,
									Opportunity.DateOrderSubmitted__c,
									Opportunity.Subtotal__c,
									Opportunity.ShippingCost__c,
									Opportunity.TaxCost__c,
									Opportunity.Total_Amount__c,
									Opportunity.Teacher__r.mailingCity,
									Opportunity.Teacher__r.mailingPostalCode,
									Opportunity.Teacher__r.mailingCountryCode,
									Opportunity.Teacher__r.mailingStateCode,
									Opportunity.Teacher__r.mailingStreet,
									Opportunity.Teacher__r.email,
									Opportunity.Teacher__r.School__r.Name
							FROM OpportunityLineItem
							WHERE OpportunityId IN: opptyId FOR UPDATE ];
		List<string> vendorsList = new List<string>();
		for(Opportunitylineitem o : opptylineitemList) {      
			if (o.vendor_name__c != Null) {
				if(o.vendor_name__c.contains('\'')) {           
					vendorsname.add(((o.vendor_name__c.remove(o.vendor_name__c.substring(o.vendor_name__c.indexof('\'')+1, o.vendor_name__c.length()))).remove('\'')));
				} else {
					vendorsname.add(o.vendor_name__c);
				} 
			}
		}    
				
		if(vendorsname.size() > 0) {
			vendorsList.addall(vendorsname);
			string vname;
			vname = '\'%'+String.join(vendorsList,'%\' OR Order_Cloud_Vendor_Name__c like\'%')+'%\'';
			
			string query = 'select Email__c,name,Order_Cloud_Vendor_Name__c from Account where RecordTypeId =\''+ rtId+ '\' and Email__c != Null and (Order_Cloud_Vendor_Name__c like '+vname+')';
			
			vendorList = database.query(query);    
			
			List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
			OrgWideEmailAddress oea = [select id, Address, DisplayName from OrgWideEmailAddress where Address='info@adoptaclassroom.org' limit 1];
			
			Map<Id,string> venmap = new Map<Id,string>();
			Map<Id,AcctSeedERP__Purchase_Order__c> purchaseList = new Map<Id,AcctSeedERP__Purchase_Order__c>();
			map<Id,List<AcctSeedERP__Purchase_Order_Line__c>> purchaseline = new Map<Id,List<AcctSeedERP__Purchase_Order_Line__c>>();  
			
			List<AcctSeed__Billing_Format__c> abformate = [select name,Id from AcctSeed__Billing_Format__c where Name = 'Default Purchase Order' limit 1];
			
			for(Account ven: vendorList){
	
				set<Id> venset = new set<Id>();
					
				for (OpportunityLineItem oline :opptylineitemList) {         
							
					if((oline.vendor_name__c.contains('\'') ? (oline.vendor_name__c.remove(oline.vendor_name__c.substring(oline.vendor_name__c.indexof('\'')+1, oline.vendor_name__c.length()))).remove('\'') : oline.vendor_name__c).containsIgnoreCase(ven.Order_Cloud_Vendor_Name__c.contains('\'') ? (ven.Order_Cloud_Vendor_Name__c.remove(ven.Order_Cloud_Vendor_Name__c.substring(ven.Order_Cloud_Vendor_Name__c.indexof('\'')+1, ven.Order_Cloud_Vendor_Name__c.length()))).remove('\'') : ven.Order_Cloud_Vendor_Name__c)) {
								
						if(!purchaseList.containskey(ven.Id) && !abformate.isEmpty()) {
							AcctSeedERP__Purchase_Order__c pos = new AcctSeedERP__Purchase_Order__c( AcctSeedERP__Type__c = 'Standard', AcctSeedERP__Purchase_Order_Format__c = abformate.get(0).Id, AcctSeedERP__Order_Date__c = System.Today(), AcctSeedERP__Vendor__c = ven.Id, Opportunity_Name__c = oLine.OpportunityId, AcctSeedERP__Status__c = 'Open', VendorId__c = oline.VendorOrderId__c);
							purchaseList.put(ven.Id,pos);
						}
									
						if(purchaseline.containsKey(ven.Id)){
							List<AcctSeedERP__Purchase_Order_Line__c> polineList = purchaseline.get(ven.Id);
							AcctSeedERP__Purchase_Order_Line__c poLines = new AcctSeedERP__Purchase_Order_Line__c(AcctSeedERP__Product__c = oLine.Product2Id, AcctSeedERP__Quantity__c = oLine.Quantity, AcctSeedERP__Unit_Price__c = oLine.UnitPrice);
							polineList.add(poLines);
							purchaseline.put(ven.Id, polineList);
						} else {
							AcctSeedERP__Purchase_Order_Line__c poLines = new AcctSeedERP__Purchase_Order_Line__c(AcctSeedERP__Product__c = oLine.Product2Id, AcctSeedERP__Quantity__c = oLine.Quantity, AcctSeedERP__Unit_Price__c = oLine.UnitPrice);             
							purchaseline.put(ven.Id, new List<AcctSeedERP__Purchase_Order_Line__c> { poLines });
						}
					}    
				} 	
			}
				
			if(purchaseList.size() > 0) {
				insert PurchaseList.values();
			}
			
			List<AcctSeedERP__Purchase_Order_Line__c> polineinsert = new List<AcctSeedERP__Purchase_Order_Line__c>();
			for(AcctSeedERP__Purchase_Order__c aporder : purchaseList.values()) {
				if(purchaseline.containskey(aporder.AcctSeedERP__Vendor__c)) {
					for(AcctSeedERP__Purchase_Order_Line__c apline : purchaseline.get(aporder.AcctSeedERP__Vendor__c)) {
						if (apLine.AcctSeedERP__Purchase_Order__c == NULL) {
							apLine.AcctSeedERP__Purchase_Order__c = aporder.Id;
							polineinsert.add(apline);
						}
					}
				}
			}
				
			if(polineinsert.size() > 0) {
				insert polineinsert;
			}
		}
	}	
}