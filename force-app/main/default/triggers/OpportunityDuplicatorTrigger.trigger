trigger OpportunityDuplicatorTrigger on Opportunity (after insert) {
    if (trigger.isAfter && trigger.isInsert)
        OpportunityUtils.CreateCommunityOpportunity();
}