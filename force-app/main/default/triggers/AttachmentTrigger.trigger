trigger AttachmentTrigger on Attachment (before insert,after insert) {
    String urlPath=URL.getCurrentRequestUrl().getPath();
    system.debug('******'+urlPath);
    if((urlPath.containsIgnoreCase('/asyncMetadataDeploy') || urlPath.containsIgnoreCase('/services/data/') || urlPath.endsWithIgnoreCase('/tooling/executeAnonymous/') || urlPath.containsIgnoreCase('/mockRequest/AsyncApexTest'))&& Trigger.isInsert){
        system.debug('******'+urlPath);
        if(Trigger.IsBefore){
            AttachmentHelper.processInvoiceAttachmentBefore(Trigger.New);
        }
        if(Trigger.IsAfter){
            AttachmentHelper.processInvoiceAttachmentAfter(Trigger.New);
        }
    }
}