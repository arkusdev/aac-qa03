/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
trigger dlrs_donation_split_DesignationTrigger on donation_split__Designation__c
    (before delete, before insert, before update, after delete, after insert, after undelete, after update)
{
    dlrs.RollupService.triggerHandler();
    if(Trigger.IsUpdate)
    {
        if(Trigger.IsAfter)
        {
            Map<Id,Id> contactId = new map<Id,Id>();
            for(donation_split__Designation__c dsd : Trigger.new)
            {
                if(Trigger.oldmap.get(dsd.Id).School__c != dsd.School__c)
                {
                    contactId.put(dsd.Teacher__c,dsd.School__c);
                }
            }
            List<Contact> conlist = new list<Contact>();
            if(contactId.size() > 0)
            {
                
                for(Id i : contactId.keyset())
                {
                    contact c = new contact();
                    c.Id = i;
                    c.School__c = contactId.get(i);
                    conlist.add(c);
                }
            }
            if(conlist.size() > 0)
            {
                system.debug('RRRRRRRRRRRRRRRRRRRRRRR'+conlist);
                update conlist;
            }
        }
    }
}