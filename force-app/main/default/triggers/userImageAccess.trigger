trigger userImageAccess on User (before insert, before update) {
    Set<Id> ids = new Set<Id>();
 
    list<user> usrlist = [SELECT Id FROM user Where Id IN : ids];
    
    for(user users : trigger.new){
        users.UserPreferencesShowProfilePicToGuestUsers = true;
    }
    insert usrlist;
}