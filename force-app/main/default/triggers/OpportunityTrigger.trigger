trigger OpportunityTrigger on Opportunity (after insert, before insert, after update, before update) {
  if (Trigger.isAfter) {
    if (Trigger.isInsert) {
      OpportunityManagement.afterInsert(Trigger.new);
    } else if (Trigger.isUpdate) {
      OpportunityManagement.afterUpdate(Trigger.new, Trigger.oldMap);
    }
  } else {
    OpportunityManagement.UpdateSchoolDetails();
  }
  
}