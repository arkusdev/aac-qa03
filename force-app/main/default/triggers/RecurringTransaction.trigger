trigger RecurringTransaction on ChargentOrders__Transaction__c (before insert) {
    
    if(Trigger.isBefore && Trigger.isInsert) {
        for(ChargentOrders__Transaction__c tran : Trigger.new) {
            tran.ToBeProcessed__c = true;
        }
    }
    
}