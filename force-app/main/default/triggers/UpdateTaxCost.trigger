trigger UpdateTaxCost on Teacher_Sales_Tax__c (after Insert,after update,before delete) {

set<string> Statecode = new set<String>();
set<string> country = new set<string>();
List<Teacher_sales_Tax__c> tstlist = new list<Teacher_Sales_tax__c>();

If(trigger.isAfter)
{
    SalesTaxManagement.afterTrigger(Trigger.new, Trigger.oldMap);
}
if(trigger.isBefore)
{
    if(trigger.isDelete)
    {
       SalesTaxManagement.beforeDelete(Trigger.new,Trigger.oldMap);
    }
}
}