trigger ChargentOrdersTrigger on ChargentOrders__ChargentOrder__c (before update) {

    List<ChargentOrders__ChargentOrder__c> orders = new List<ChargentOrders__ChargentOrder__c>();
    for(ChargentOrders__ChargentOrder__c order : Trigger.new) {
        if(order.Transaction_Completed__c && order.ChargentOrders__Status__c != Trigger.oldMap.get(order.ID).ChargentOrders__Status__c && order.ChargentOrders__Status__c == 'Complete'){
            order.ToSendThankYouEmail__c = true;
        }
    }
}