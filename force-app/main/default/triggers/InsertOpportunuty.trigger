trigger InsertOpportunuty on pymt__PaymentX__c (after insert) {
    
    RecordType RType = new RecordType();
    RType =  [select Id, developerName,Name from RecordType where developername='donation' and sobjectType='Opportunity'];
    Map<Id,Opportunity> oplist = new map<Id,opportunity>();
    for(pymt__PaymentX__c pp : Trigger.New)
    {
        double pymtamnt = 0;
        double subtotal = 0;
        if(pp.pymt__Amount__c != Null)
        {
            pymtamnt = pp.pymt__Amount__c;
        }
        if(pp.Subtotal__c != Null)
        {
            subtotal = pp.Subtotal__c;
        }
        double amnt = pymtamnt - subtotal;
        if(amnt > 0) //&& pp.pymt__Transaction_Id__c != Null)
        {
            opportunity op = new Opportunity();
            //op.Name = RType.Name +' - '+ pp.pymt__Contact__r.Name+' '+ date.Today() + ' ' + 'Processing Fee';
            op.Name = RType.Name +' - '+ 'Processing Fee';
            //op.stageName = 'Sale Closed / Paid';
            op.stageName = 'Pledged';
            op.npsp__Primary_Contact__c = pp.pymt__Contact__c;
            op.AccountId = pp.pymt__Contact__r.AccountId;
            op.Amount = pp.pymt__Transaction_Fee__c;           
            op.Expiration_Date__c = date.newInstance(date.Today().Year()+1,date.Today().Month(),date.today().day());
           //  = date.newInstance(system.today().day(),system.today().month(),y);
            op.RecordTypeId = RType.Id;
            op.closedate= date.Today();
            op.Processing_Fee__c = true;
            oplist.put(pp.Id,op);
         }
    }
    if(oplist.size() > 0)
    {
        insert oplist.values();
    }
    list<pymt__Shopping_Cart_Item__c> pshoplist = new list<pymt__Shopping_Cart_Item__c>();
    for(Id Payid : oplist.keyset())
    {
        pymt__Shopping_Cart_Item__c psc = new pymt__Shopping_Cart_Item__c();
        psc.Name = 'Processing Fee - '+oplist.get(payid).npsp__Primary_Contact__r.Name;
        psc.pymt__Quantity__c =1;
        psc.pymt__Unit_Price__c = oplist.get(payid).Amount;
        psc.Donation__c = oplist.get(payid).Id;
        psc.pymt__Payment__c = payid;
        psc.pymt__Contact__c = oplist.get(payid).npsp__Primary_Contact__c;
        pshoplist.add(psc);
    }
    if(pshoplist.size() > 0)
    {
        insert pshoplist;
        set<Id> pymtid = new set<Id>();
        for(pymt__Shopping_Cart_Item__c ps : pshoplist)
        {
           pymtid.add(ps.Id);
        }
        list<pymt__Shopping_Cart_Item__c> pymtlist = new list<pymt__Shopping_Cart_Item__c>();
        pymtlist = [select Id,pymt__Contact__r.name from pymt__Shopping_Cart_Item__c where Id IN: pymtid];
        list<pymt__Shopping_Cart_Item__c> updateshop = new list<pymt__Shopping_Cart_Item__c>();
        for(pymt__Shopping_Cart_Item__c jpshop : pymtlist)
        {
            jpshop.Name = 'Processing Fee - '+jpshop.pymt__Contact__r.Name;
            updateshop.add(jpshop);
        }
        if(updateshop.size() > 0)
        {
            update updateshop;
        }
    }
    
}