/******************************************************************************
        @ Author Name    : Brighthat Team
        @ Description    : Create Journal Entries for OrderCloud Purchase Orders
        @ Date of Create : 05.09.2017
        @ Version        : 39.0   
******************************************************************************/
trigger InsertJETrigger on Opportunity (after insert) {
    AcctSeed__GL_Account__c glAccount = [SELECT Id, Name FROM AcctSeed__GL_Account__c WHERE Name ='4599 Teacher Restricted Released'];
    Map<String, AcctSeed__Accounting_Variable__c> accountingvariableMap = new Map<String, AcctSeed__Accounting_Variable__c>();
    List<String> accountingVariableNameList = new List<String>{'Restricted', 'Unrestricted', 'Programs', 'Classroom Adoption'};
    String period = '';
    if (Date.Today().month() < 7) {
        period = String.valueOf(Date.Today().year());
    } else {
        period = String.valueOf(Date.Today().year() + 1);
    }
    //String period = String.valueOf(Date.Today().year());
    String month = (Date.Today().addMonths(6).month() <10)?''+0+(Date.Today().addMonths(6).month()):''+Date.Today().addMonths(6).month();
    period = period + '-' + month;
    AcctSeed__Accounting_Period__c accountingPeriod;
    
    try {
        accountingPeriod = [SELECT Id, Name FROM AcctSeed__Accounting_Period__c WHERE Name = :period];
    }catch(Exception e){}
    
    SavePoint sp = Database.setSavepoint();
    if (accountingPeriod == null) {
        accountingPeriod = new AcctSeed__Accounting_Period__c();
        accountingPeriod.Name = period;
        Integer numberOfDays = Date.daysInMonth(Date.Today().year(), Date.Today().month());
        Date lastDayOfMonth = Date.newInstance(Date.Today().year(), Date.Today().month(), numberOfDays);
        accountingPeriod.AcctSeed__Start_Date__c = Date.newInstance(Date.Today().year(), Date.Today().month(), 1);
        accountingPeriod.AcctSeed__End_Date__c = lastDayOfMonth;
        accountingPeriod.AcctSeed__Status__c = 'Open';
        system.debug('SSSSSSSSSSSSSSSSSSS'+accountingPeriod);
        insert accountingPeriod;
    }
    
    for (AcctSeed__Accounting_Variable__c accountingVariable : [SELECT Id, Name FROM AcctSeed__Accounting_Variable__c WHERE Name IN :accountingVariableNameList]) {
        accountingvariableMap.put(accountingVariable.Name, accountingVariable);
    }
    Map<Id, AcctSeed__Journal_Entry__c> jeInsertMap = new Map<Id, AcctSeed__Journal_Entry__c>();
    Map<Id, List<AcctSeed__Journal_Entry_Line__c>> jeBeforeInsertMap = new Map<Id, List<AcctSeed__Journal_Entry_Line__c>>();

    RecordType orderRecordType = new RecordType();
    orderRecordType =  [select Id, developerName from RecordType where developername='Orders' and sobjectType='Opportunity'];
    List<Id> oppIdList = new List<Id>();
    for(Opportunity opp: Trigger.new){ 
        oppIdList.add(opp.Id);
        if (opp.recordTypeId == orderRecordType.Id) {
            AcctSeed__Journal_Entry__c je = new AcctSeed__Journal_Entry__c();
            je.Name = 'Funds Released'+'  '+opp.Name;
            je.AcctSeed__Status__c = 'In Process';
            je.AcctSeed__Journal_Date__c = System.Today();
            je.Opportunity__c = opp.Id;
            if(Test.isRunningTest()){
                try{
                    AcctSeed__GL_Account__c accGL = new AcctSeed__GL_Account__c(AcctSeed__Sub_Type_1__c = 'Assets', AcctSeed__Bank__c = true);
                    insert accGL;
                    
                    AcctSeed__Billing_Format__c pdfFormat = new AcctSeed__Billing_Format__c(
                                                                         AcctSeed__Default_Email_Template__c = 'Adoption_Notification',
                                                                         AcctSeed__Visualforce_PDF_Page__c = 'AnswersHome');
                    insert pdfFormat;
                    
                    AcctSeed__Ledger__c ledger = new AcctSeed__Ledger__c(AcctSeed__Type__c = 'Transactional', 
                                                                         AcctSeed__Billing_Activity_Statement_Format__c = pdfFormat.Id,
                                                                         AcctSeed__Billing_Outstanding_Statement_Format__c = pdfFormat.Id,
                                                                         AcctSeed__Default_Bank_Account__c = accGL.Id,
                                                                         AcctSeed__Default_Billing_Format__c = pdfFormat.Id,
                                                                         AcctSeed__Default_Purchase_Order_Format__c = pdfFormat.Id,
                                                                         AcctSeed__Default_Packing_Slip_Format__c = pdfFormat.Id);
                    insert ledger;
                    
                    je.AcctSeed__Ledger__c = ledger.Id;
                }catch(Exception e){
                    System.debug('*** Error InsertJETrigger: ' + e.getMessage());
                }
            }
            
            if (accountingPeriod != null) {
                je.AcctSeed__Accounting_Period__c = accountingPeriod.Id;
            }
            jeInsertMap.put(opp.Id, je);            
            List<AcctSeed__Journal_Entry_Line__c> jeLineList = new List<AcctSeed__Journal_Entry_Line__c>();         
            AcctSeed__Journal_Entry_Line__c creditJeLine = new AcctSeed__Journal_Entry_Line__c();
            creditJeLine.AcctSeed__GL_Account__c = glAccount.Id;
            creditJeLine.AcctSeed__Credit__c = opp.Total_Amount__c;
            creditJeLine.AcctSeed__GL_Account_Variable_1__c = accountingvariableMap.get('Unrestricted').Id;
            creditJeLine.AcctSeed__GL_Account_Variable_2__c = accountingvariableMap.get('Programs').Id;
            creditJeLine.AcctSeed__GL_Account_Variable_3__c = accountingvariableMap.get('Classroom Adoption').Id;
            jeLineList.add(creditJeLine);
            AcctSeed__Journal_Entry_Line__c debitJeLine = new AcctSeed__Journal_Entry_Line__c();
            debitJeLine.AcctSeed__GL_Account__c = glAccount.Id;
            debitJeLine.AcctSeed__Debit__c = opp.Total_Amount__c;
            debitJeLine.AcctSeed__GL_Account_Variable_1__c = accountingvariableMap.get('Restricted').Id;
            debitJeLine.AcctSeed__GL_Account_Variable_2__c = accountingvariableMap.get('Programs').Id;
            debitJeLine.AcctSeed__GL_Account_Variable_3__c = accountingvariableMap.get('Classroom Adoption').Id;
            jeLineList.add(debitJeLine);            
            jeBeforeInsertMap.put(opp.Id, jeLineList);          
        }        
    }
    
    if(!jeInsertMap.isEmpty()){
        Insert jeInsertMap.values();        
        List<AcctSeed__Journal_Entry_Line__c> jelInsertList = new List<AcctSeed__Journal_Entry_Line__c>();  
        for (Id oppId : jeInsertMap.keySet()) {
            Id jeId = jeInsertMap.get(oppId).Id;
            for (AcctSeed__Journal_Entry_Line__c jeLine : jeBeforeInsertMap.get(oppId)) {
                jeLine.AcctSeed__Journal_Entry__c = jeId;
                jelInsertList.add(jeLine);
            }
        }
        insert jelInsertList;
        for (AcctSeed__Journal_Entry__c je : jeInsertMap.values()) {
            je.AcctSeed__Status__c = 'Approved';
        }
        update jeInsertMap.values();
    } else {
        Database.rollback(sp);
    }
    /*List<AcctSeed__Journal_Entry__c> jeListToBePosted = new List<AcctSeed__Journal_Entry__c>();
    // posting a JE
    for (AcctSeed__Journal_Entry__c je : [SELECT Id, Name, AcctSeed__Status__c, AcctSeed__Journal_Date__c, Opportunity__c, (SELECT Id, Name, AcctSeed__GL_Account__c, AcctSeed__Credit__c, AcctSeed__GL_Account_Variable_1__c, AcctSeed__GL_Account_Variable_2__c, AcctSeed__GL_Account_Variable_3__c FROM AcctSeed__Journal_Entry_Lines__r) FROM AcctSeed__Journal_Entry__c WHERE Opportunity__c In :oppIdList]) {
        jeListToBePosted.add(je);
    }
    JournalEntryPostService.postJournalEntries(jeListToBePosted);*/
    AcctSeed.JournalEntryPostService.postJournalEntries(jeInsertMap.values());
    
    Map<Id,String> opportunityVsOrderMap=new Map<Id,String>();
    for(Opportunity o:Trigger.new){
        if(o.Order_Cloud_Order_Id__c != null)
            opportunityVsOrderMap.put(o.Id,o.Order_Cloud_Order_Id__c);
    }
    if(opportunityVsOrderMap.values() != null && opportunityVsOrderMap.values().size() >0)
        OrderManagementClass.processOrderPayments(opportunityVsOrderMap); //New
}