/***********************************************************************************************
        @ Author Name    : Brighthat Team
        @ Description    : Send Email Notification to Vendor of Teacher's Purchase Order
        @ Date of Create : 04.07.2017
        @ Version        : 39.0   
***********************************************************************************************/

trigger SendingEmailtoVendor on OpportunityLineItem (after insert) {
List<account> vendorlist = new list<account>();
set<string> vendorsname = new set<string>();
set<Id> opptyId = new set<id>();
system.debug('FFFFFFInside TriggerFFFFFFFF');
Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
Organization og = [select og.Name,og.PostalCode,og.Street,og.city,og.StateCode,og.state,og.countrycode from Organization og limit 1];
string billing = og.Name+',<br></br>'+og.street+',<br></br>'+og.statecode+',<br></br>'+og.countrycode+',<br></br>'+og.postalcode;
for(OpportunityLineItem oli : Trigger.New){
        opptyId.add(oli.OpportunityId);          
}
List<OpportunityLineItem> opptylineitemlist = new list<OpportunityLineItem>();
system.debug('OOOOOOOOOOOOOOOOOO'+opptyId.size());
if(opptyId.size() > 0){
    list<string> vendorslist = new list<string>();
    opptylineitemlist = [Select Id, Vendor_name__c,Opportunity.Teacher__r.Name,VendorOrderId__c,ProductCode,Product2.Name,Quantity,product2.punchoutproduct__c,UnitPrice,Opportunity.OrderCloudLineItemCount__c,Opportunity.OpportunityProductCount__c,Opportunity.Order_Cloud_Order_Id__c,Opportunity.DateOrderSubmitted__c,Opportunity.Subtotal__c,Opportunity.ShippingCost__c,Opportunity.TaxCost__c,Opportunity.Total_Amount__c,Opportunity.Teacher__r.mailingCity,Opportunity.Teacher__r.mailingPostalCode,Opportunity.Teacher__r.mailingCountryCode,Opportunity.Teacher__r.mailingStateCode,Opportunity.Teacher__r.mailingStreet,Opportunity.Teacher__r.email,Opportunity.Teacher__r.School__r.Name from OpportunityLineItem where OpportunityId IN: opptyId];
    for(Opportunitylineitem o : opptylineitemlist){
      /*  if(o.Opportunity.OrderCloudLineItemCount__c != Null && o.Opportunity.OpportunityProductCount__c != Null){
         system.debug('LLLLLLLLLLLLLLLLL'+(o.Opportunity.OrderCloudLineItemCount__c-1));
        system.debug('LLLLLLLLLLLLLLLLL'+o.Opportunity.OpportunityProductCount__c);
        if((o.Opportunity.OrderCloudLineItemCount__c-1) == o.Opportunity.OpportunityProductCount__c){      */        
            string str;
            if(o.vendor_name__c != Null){
            if(o.vendor_name__c.contains('\'')){
                system.debug('GGGGGGGGGGGG'+o.vendor_name__c);
                system.debug('HHHHHHHHHHHHHH'+o.vendor_name__c.indexof('\''));
                string subs = o.vendor_name__c.substring(o.vendor_name__c.indexof('\'')+1, o.vendor_name__c.length());
                system.debug('JJJJJJJJJJJJJJJJ'+subs);
                string str1 = o.vendor_name__c.remove(subs);     
                str1 = str1.remove('\'');                    
                vendorsname.add(str1);
            }
            else{
                vendorsname.add(o.vendor_name__c);
            }
            }
       //   }
      //  }
      }    
    system.debug('RRRRRRRRRRRRRRR'+vendorsname);
    if(vendorsname.size() > 0){
        vendorslist.addall(vendorsname);
        string vname;
    vname = '\'%'+String.join(vendorslist,'%\' OR  Order_Cloud_Vendor_Name__c like\'%')+'%\'';
    system.debug('HHHHHHHHHHHHHHHHH'+vname);
    string query = 'select Email__c,name,Order_Cloud_Vendor_Name__c from Account where RecordTypeId =\''+ rtId+ '\' and Email__c != Null and ( Order_Cloud_Vendor_Name__c like '+vname+')';
    system.debug('TTTTTTTTTTTTTTTTTTTT'+query);
    vendorlist = database.query(query);    
    system.debug('GGGGGGGGGGGGGGGGGGGG'+vendorlist.size());
    system.debug('FFFFFFFFFFFFFFFFFFFF'+opptylineitemlist.size());
    List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
    OrgWideEmailAddress oea = [select id, Address, DisplayName from OrgWideEmailAddress where Address='info@adoptaclassroom.org' limit 1];
    system.debug('TTTTTTTTTTTTTTTTTTTTTTT'+oea);
  for(Account ven: vendorlist){
      string str;
      if(ven.Order_Cloud_Vendor_Name__c.contains('\'')){
            system.debug('GGGGGGGGGGGG'+ven.Order_Cloud_Vendor_Name__c);
            system.debug('HHHHHHHHHHHHHH'+ven.Order_Cloud_Vendor_Name__c.indexof('\''));
            string subs = ven.name.substring(ven.Order_Cloud_Vendor_Name__c.indexof('\'')+1, ven.Order_Cloud_Vendor_Name__c.length());
            system.debug('JJJJJJJJJJJJJJJJ'+subs);
            string str1 = ven.Order_Cloud_Vendor_Name__c.remove(subs);     
            str1 = str1.remove('\'');                    
            str = str1;
       }
       else{
            str= ven.Order_Cloud_Vendor_Name__c;
       }
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      List<String> sendTo = new List<String>();
      String[] ccAddress = new String[] {'orders@adoptaclassroom.org'};
      String body = '<img align=\"right\" src= \"https://adoptaclassroom--c.na8.content.force.com/servlet/servlet.ImageServer?id=015C000000397TI&oid=00D80000000dWvO&lastMod=1463163550000\"> </img><br> </br> <br> </br> <p> Hi, <br> </br> <br> </br> Please process the order below. If item prices do not match, please contact accountspayable@adoptaclassroom.org before processing.</p>';
      body += '<table Width=\"100%\" border=\"1\" cellspacing=\"5px\" cellpadding=\"5px\">';
      
       set<Id> oppset = new set<Id>();
      for (OpportunityLineItem oline :opptylineitemlist){
         
          string str1;
          if(oline.vendor_name__c.contains('\'')){
            system.debug('GGGGGGGGGGGG'+oline.vendor_name__c);
            system.debug('HHHHHHHHHHHHHH'+oline.vendor_name__c.indexof('\''));
            string subs = oline.vendor_name__c.substring(oline.vendor_name__c.indexof('\'')+1, oline.vendor_name__c.length());
            system.debug('JJJJJJJJJJJJJJJJ'+subs);
            string str2 = oline.vendor_name__c.remove(subs);     
            str1 = str2.remove('\'');                    
           }
           else{
                str1= oline.vendor_name__c;
           }       
          if(str1.contains(str)){
              if(!oppset.contains(oline.OpportunityId)){
                  oppset.add(oline.OpportunityId);
                  body += '<tr> <th>OrderId </th> <td>'+oline.VendorOrderId__c +'</td> <th>Order Date </th><td>'+oline.Opportunity.DateOrderSubmitted__c+'</td> </tr>';
                  //body += '<tr><th>Order Subtotal </th><td>'+oline.Opportunity.Subtotal__c+'</td><th>Order Shipping</th><td>'+oline.Opportunity.ShippingCost__c+'</td> </tr>';
                  //body += '<tr><th>Order Processing Fee </th><td>'+oline.Opportunity.TaxCost__c+'</td><th>Order Total</th><td>'+oline.Opportunity.Total_Amount__c+'</td></tr>';
                  if(oline.Opportunity.Teacher__r != Null){
                      body += '<tr><th>Delivery Address</th><td>'+oline.Opportunity.Teacher__r.Name+'<br></br>'+oline.Opportunity.Teacher__r.School__r.Name+'<br></br>';
                      if(oline.Opportunity.Teacher__r.mailingCity != Null){
                          body += oline.Opportunity.Teacher__r.mailingStreet.replace('null','')+'<br></br>'+oline.Opportunity.Teacher__r.mailingCity+','+oline.Opportunity.Teacher__r.mailingStateCode+' '+oline.Opportunity.Teacher__r.mailingPostalCode;
                      }
                      body+= '<br></br><b>Email:</b>'+oline.Opportunity.Teacher__r.email+'</td><th>Billing Address</th><td>'+og.Name+'<br></br>'+og.street+'<br></br>'+og.city+','+og.statecode+' '+og.postalcode+'<br></br> accountspayable@adoptaclassroom.org </td></tr> </table> <br></br><br></br>';
                  }
                  body += '<table border="1" width="100%"><caption><b>Item Details</b></caption><tr><th>Item Id </th><th>Item Name</th><th>Item Quantity</th><th>Item Price</th></tr>';
              }
             body += '<tr align=\"center\"><td>'+oline.ProductCode +'</td><td>' + oline.Product2.Name + '</td><td>' + oline.Quantity + '</td><td>' + oline.UnitPrice + '</td></tr>';
          }    
      }  
      body = body+ '</table> <br> </br> Thanks, <br> </br> AdoptAClassroom.org '; 
      system.debug('TTTTTTTTTTTTTTTTTTTTTTTTTT'+body);  
      sendTo.add(ven.Email__c);
      mail.setToAddresses(sendTo);
      mail.setCcAddresses(ccAddress);
      mail.setOrgWideEmailAddressId(oea.id);
      mail.setSubject('Order Details');         
      mail.setHtmlBody(body);
      mails.add(mail);
    }
    Messaging.sendEmail(mails);
    }
  }
  
}