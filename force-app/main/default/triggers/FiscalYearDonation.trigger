trigger FiscalYearDonation on Contact (before update, before insert) {
 try {
    Integer startingMonthOfTheYear = 7;
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            for (Contact newContact : trigger.new) {
                newContact.Total_Donation__c = 0;
                newContact.Total_Donation_Modified_Date__c = Date.Today();
            }
        }
        if (Trigger.isUpdate){
           
            Integer CurrentYear = Date.today().year();
            Integer lastModifiedYear;           
            Integer currentMonth = Date.today().month();        
            Integer lastModifiedMonth;
            
            Boolean sameYear = false;
            for (Contact newContact : trigger.new) {               
                if(newContact.Total_Donation_Modified_Date__c ==null){
                    newContact.Total_Donation_Modified_Date__c=Date.today();
                }             
                lastModifiedMonth = newContact.Total_Donation_Modified_Date__c.month();
                lastModifiedYear = newContact.Total_Donation_Modified_Date__c.year();               
                               
                if ((CurrentYear - lastModifiedYear) == 1) {
                    if (currentMonth >= 7){
                        sameYear = false;
                    } else {
                        sameYear = true;
                    }                   
                } else if ((CurrentYear - lastModifiedYear) == 0) {
                    if (currentMonth >= 7){
                        if (lastModifiedMonth >= 7){
                            sameYear = true;
                        } else {
                            sameYear = false;
                        }
                    } else {
                        sameYear = true;
                    }
                }
                if ((CurrentYear - lastModifiedYear) >= 2) {
                    sameYear = false;
                }
                if (trigger.oldMap.get(newContact.Id).Total_Funds_Raised__c != newContact.Total_Funds_Raised__c) {
                       if (!sameYear) {
                            newContact.Total_Donation__c = 0;
                        }
                       newContact.Total_Donation__c = (newContact.Total_Donation__c == null)?0:newContact.Total_Donation__c;
                       if (trigger.oldMap.get(newContact.Id).Total_Funds_Raised__c == null) {
                           newContact.Total_Donation__c += trigger.newMap.get(newContact.Id).Total_Funds_Raised__c;
                       } else {
                           newContact.Total_Donation__c += trigger.newMap.get(newContact.Id).Total_Funds_Raised__c - trigger.oldMap.get(newContact.Id).Total_Funds_Raised__c;
                       }  
                       system.debug('*****************'+ newContact.Total_Donation__c);
                                           
                       newContact.Total_Donation_Modified_Date__c = Date.Today();
                }
                sameYear = false;                           
            }
        }
    }
    } catch(exception e) {
  }    
}