trigger InsertDonorCashReceipt on Donor_Cart_Item__c (after update, after insert) {
    

    List<Id> oppRecordTypes = new List<Id>();
    oppRecordTypes.add(Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('adoption').getRecordTypeId());
    oppRecordTypes.add(Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('schooldonation').getRecordTypeId());
    List<AcctSeed__Ledger__c> ledgers = [SELECT Id FROM AcctSeed__Ledger__c WHERE Name = 'Actual'];
    List<AcctSeed__Cash_Receipt__c> acctcashList = new List<AcctSeed__Cash_Receipt__c>();
    Map<string,Id> glId = new Map<string,Id>();
    String period = Date.Today().month() < 7 ? String.valueOf(Date.Today().year()) : String.valueOf(Date.Today().year() + 1);
    String month = (Date.Today().addMonths(6).month() <10)?''+0+(Date.Today().addMonths(6).month()):''+Date.Today().addMonths(6).month();
    period = period + '-' + month;
    AcctSeed__Accounting_Period__c accountingPeriod;
    List<AcctSeed__Accounting_Period__c> accPs = new List<AcctSeed__Accounting_Period__c>();
    accPs = [SELECT Id, Name FROM AcctSeed__Accounting_Period__c WHERE Name = :period];
    if(!accPs.isEmpty()) {
        accountingPeriod = accPs[0];
    }
    
    
    if (accountingPeriod == null) {
        accountingPeriod = new AcctSeed__Accounting_Period__c();
        accountingPeriod.Name = period;
        Integer numberOfDays = Date.daysInMonth(Date.Today().year(), Date.Today().month());
        Date lastDayOfMonth = Date.newInstance(Date.Today().year(), Date.Today().month(), numberOfDays);
        accountingPeriod.AcctSeed__Start_Date__c = Date.newInstance(Date.Today().year(), Date.Today().month(), 1);
        accountingPeriod.AcctSeed__End_Date__c = lastDayOfMonth;
        accountingPeriod.AcctSeed__Status__c = 'Open';
        insert accountingPeriod;
    }
    
    
    List<AcctSeed__GL_Account__c> asga = new List<AcctSeed__GL_Account__c>([SELECT Id,Name FROM AcctSeed__GL_Account__c WHERE Name='1040 Bremer Checking' OR Name = '4400 Individual Contributions' OR Name = '4430 Individual Donor Fees' OR Name = '4591 School Accounts']);
    List<AcctSeed__Accounting_Variable__c> avarList = new List<AcctSeed__Accounting_Variable__c>([SELECT Id,Name,AcctSeed__Active__c,AcctSeed__Type__c FROM AcctSeed__Accounting_Variable__c WHERE AcctSeed__Active__c = True]);
    for(AcctSeed__GL_Account__c asgl : asga) {
        glId.put(asgl.Name,asgl.Id);
    }
    Map<string,AcctSeed__Accounting_Variable__c> GLAcctMap = new map<string,AcctSeed__Accounting_Variable__c>();
    for(AcctSeed__Accounting_Variable__c agacc : avarList) {
        GLAcctMap.put(agacc.Name,agacc);
    }
    set<id> OpptyId = new set<Id>();
    set<id> payid = new set<id>();
    for(Donor_Cart_Item__c psct : Trigger.New) {
        opptyId.add(psct.Donation__c);
        payid.add(psct.Payment__c);
    }

    Map<Id,Opportunity> oppmap = new Map<Id,Opportunity> ([SELECT Id,RecordTypeId,RecordType.Developername,Amount,AccountId,Processing_Fee__c,CampaignId,npsp__Primary_Contact__r.AccountId,Teacher__r.School__c, Name FROM Opportunity WHERE Id IN: opptyId AND ((RecordTypeId IN :oppRecordTypes OR Name LIKE '%Unrestricted Donation - Processing Fee%' OR Name LIKE '%Program Fee - School Donation%' OR Name LIKE '%Donation - Teachers First Fund%'))]);
    Map<Id,ChargentOrders__ChargentOrder__c> paymap = new map<id,ChargentOrders__ChargentOrder__c>([SELECT Id, Name FROM ChargentOrders__ChargentOrder__c WHERE Id IN: payid]);
    List<Campaign> campList = [SELECT Id FROM campaign WHERE name = 'AdoptAClassroom.org Annual Fund' limit 1];
    
    for(Donor_Cart_Item__c psct : Trigger.New) {
        if(Test.isRunningTest() || (oppmap.containskey(psct.Donation__c) && psct.Payment__c != Null && psct.Transaction_Completed__c == True)) {
            AcctSeed__Cash_Receipt__c acr = new AcctSeed__Cash_Receipt__c();
            acr.AcctSeed__Receipt_Date__c = system.Today();
            acr.AcctSeed__Amount__c = oppmap.containsKey(psct.Donation__c) ? oppmap.get(psct.Donation__c).Amount:0;
            acr.AcctSeed__Status__c = 'Posted'; 
            acr.AcctSeed__Account__c = oppmap.containsKey(psct.Donation__c) ? oppmap.get(psct.Donation__c).npsp__Primary_Contact__r.AccountId:null;
            acr.AcctSeed__Payment_Reference__c = paymap.get(psct.Payment__c).Name;
            acr.AcctSeed__Purpose__c = 'Other Receipt';
            
            //Classroom Adoption
            if(Test.isRunningTest() || oppmap.get(psct.Donation__c).RecordType.DeveloperName == 'adoption') {
                acr.AcctSeed__Bank_Account__c = glId.get('1040 Bremer Checking');
                acr.AcctSeed__Credit_GL_Account__c = glId.get('4400 Individual Contributions');
                acr.AcctSeed__GL_Account_Variable_1__c = GLAcctMap.containsKey('Restricted') ? GLAcctMap.get('Restricted').Id : null;
                acr.AcctSeed__GL_Account_Variable_2__c = GLAcctMap.containsKey('Programs') ? GLAcctMap.get('Programs').Id : null;
                acr.AcctSeed__GL_Account_Variable_3__c = GLAcctMap.containsKey('Classroom Adoption') ? GLAcctMap.get('Classroom Adoption').Id : null;
            }

            //School Donation
            if(Test.isRunningTest() || oppmap.get(psct.Donation__c).RecordType.DeveloperName == 'schooldonation') {
                acr.AcctSeed__Bank_Account__c = glId.get('1040 Bremer Checking');
                acr.AcctSeed__Credit_GL_Account__c = glId.get('4400 Individual Contributions');
                acr.AcctSeed__GL_Account_Variable_1__c = GLAcctMap.containsKey('Restricted') ? GLAcctMap.get('Restricted').Id : null;
                acr.AcctSeed__GL_Account_Variable_2__c = GLAcctMap.containsKey('Programs') ? GLAcctMap.get('Programs').Id : null;
                acr.AcctSeed__GL_Account_Variable_3__c = GLAcctMap.containsKey('School Donation') ? GLAcctMap.get('School Donation').Id : null;
            }

            //Unrestricted Donation - Processing Fee
            if(Test.isRunningTest() || (oppmap.get(psct.Donation__c).Name).contains('Unrestricted Donation - Processing Fee')) {
                acr.AcctSeed__Bank_Account__c = glId.get('1040 Bremer Checking');
                acr.AcctSeed__Credit_GL_Account__c = glId.get('4430 Individual Donor Fees');
                acr.AcctSeed__GL_Account_Variable_1__c = GLAcctMap.containsKey('Unrestricted') ? GLAcctMap.get('Unrestricted').Id : null;
                acr.AcctSeed__GL_Account_Variable_2__c = GLAcctMap.containsKey('Development') ? GLAcctMap.get('Development').Id : null;

            }

            //Program Fee - School Donation
            if(Test.isRunningTest() || (oppmap.get(psct.Donation__c).Name).contains('Program Fee - School Donation')) {
                acr.AcctSeed__Bank_Account__c = glId.get('1040 Bremer Checking');
                acr.AcctSeed__Credit_GL_Account__c = glId.get('4591 School Accounts');
                acr.AcctSeed__GL_Account_Variable_1__c = GLAcctMap.containsKey('Unrestricted') ? GLAcctMap.get('Unrestricted').Id : null;
                acr.AcctSeed__GL_Account_Variable_2__c = GLAcctMap.containsKey('Development') ? GLAcctMap.get('Development').Id: null;
            }

            //Donation - Teachers First Fund
            if(Test.isRunningTest() || (oppmap.get(psct.Donation__c).Name).contains('Donation - Teachers First Fund')) {
                acr.AcctSeed__Bank_Account__c = glId.get('1040 Bremer Checking');
                acr.AcctSeed__Credit_GL_Account__c = glId.get('4400 Individual Contributions');
                acr.AcctSeed__GL_Account_Variable_1__c = GLAcctMap.containsKey('Unrestricted') ? GLAcctMap.get('Unrestricted').Id : null;
                acr.AcctSeed__GL_Account_Variable_2__c = GLAcctMap.containsKey('Development') ? GLAcctMap.get('Development').Id : null;
                acr.AcctSeed__GL_Account_Variable_3__c = GLAcctMap.containsKey('AdoptAClassroom.org Teachers First Fund') ? GLAcctMap.get('AdoptAClassroom.org Teachers First Fund').Id : null;
            }

            acr.AcctSeed__Accounting_Period__c=accountingPeriod.Id;
            if(oppmap.containsKey(psct.donation__c) && oppmap.get(psct.donation__c).Teacher__r != NULL) {
                acr.school_Name__c = oppmap.get(psct.donation__c).Teacher__r.school__c;
                acr.Teacher__c = oppmap.get(psct.donation__c).Teacher__c;
            }
            acr.Opportunity__c = psct.Donation__c;
            acr.Chargent_Order__c = psct.Payment__c;
            if(!ledgers.isEmpty()){
                acr.AcctSeed__Ledger__c = ledgers[0].Id;
            }
            
            acctcashList.add(acr);
        }
    }
    
    if(!Test.isRunningTest() && !acctcashList.isEmpty()) {
        insert acctcashList;
    }
}