trigger ContentDocumentLinkTrigger on ContentDocumentLink (before insert, after insert) {
    
    String lhwDocumentPrefix = Schema.getGlobalDescribe().get('Announcement__c').getDescribe().getKeyPrefix();
    String classroomPrefix = Schema.getGlobalDescribe().get('donation_split__Designation__c').getDescribe().getKeyPrefix();
    String teacherPrefix = Schema.getGlobalDescribe().get('Contact').getDescribe().getKeyPrefix();
    
    if (Trigger.isBefore && Trigger.isInsert) {
        for (ContentDocumentLink cdl : Trigger.New) {
            if ((cdl.LinkedEntityId != null && String.valueOf(cdl.LinkedEntityId).startsWithIgnoreCase(lhwDocumentPrefix)) || (cdl.LinkedEntityId != null && String.valueOf(cdl.LinkedEntityId).startsWithIgnoreCase(classroomPrefix)) || (cdl.LinkedEntityId != null && String.valueOf(cdl.LinkedEntityId).startsWithIgnoreCase(teacherPrefix))) {
                cdl.Visibility = 'AllUsers';
            }
        }
    }

    
    if (Trigger.isAfter && Trigger.isInsert) {
        Map<Id,Id> ContentDocumentIdParentId = new Map<Id,Id>();
        for (ContentDocumentLink cdl : Trigger.New) {
            if (String.valueOf(cdl.LinkedEntityId).startsWithIgnoreCase(lhwDocumentPrefix) || String.valueOf(cdl.LinkedEntityId).startsWithIgnoreCase(teacherPrefix)) {
                ContentDocumentIdParentId.put(cdl.ContentDocumentId, cdl.LinkedEntityId);
            }
        }

        List<ContentDistribution> cdistList = new List<ContentDistribution>();
        for(ContentDocument cd : [SELECT LatestPublishedVersionId, ParentId, Title, FileExtension FROM ContentDocument WHERE ID IN :ContentDocumentIdParentId.keySet() ]) {
            if(String.valueOf(ContentDocumentIdParentId.get(cd.Id)).startsWithIgnoreCase(lhwDocumentPrefix) || ((String.valueOf(ContentDocumentIdParentId.get(cd.Id)).startsWithIgnoreCase(teacherPrefix) && cd.Title.containsIgnoreCase('banner')))) {
                cdistList.add(new ContentDistribution ( ContentVersionId = cd.LatestPublishedVersionId,
                                                    Name = cd.Title + '.' + cd.FileExtension,
                                                    RelatedRecordId = ContentDocumentIdParentId.get(cd.Id),
                                                    PreferencesAllowOriginalDownload = true,
                                                    PreferencesAllowPDFDownload = true,
                                                    PreferencesAllowViewInBrowser = true,
                                                    PreferencesLinkLatestVersion = true,
                                                    PreferencesPasswordRequired = false,
                                                    PreferencesNotifyOnVisit = false,
                                                    PreferencesNotifyRndtnComplete = false));
            }
        }
        insert cdistList;
    }
}