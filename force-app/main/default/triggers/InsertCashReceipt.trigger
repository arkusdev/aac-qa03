/***************************************************************************
        @ Author Name    : Brighthat Team
        @ Description    : Create a Cash Receipt for Donors Payment
        @ Date of Create : 05.22.2017
        @ Version        : 39.0   
***************************************************************************/

trigger InsertCashReceipt on pymt__Shopping_Cart_Item__c (after update) {

set<String> recordstring = new set<string>();
recordstring.add('adoption');
recordstring.add('Donation');
recordstring.add('Program_Fund');
List<AcctSeed__Cash_Receipt__c> acctcashlist = new list<AcctSeed__Cash_Receipt__c>();
Map<Id,RecordType> recordmap = new Map<Id,RecordType>([select Id, name from RecordType where Developername IN: recordstring and sobjectType='Opportunity']);
Map<string,Id> glId = new Map<string,Id>();
list<AcctSeed__GL_Account__c> asga = new list<AcctSeed__GL_Account__c>([select Id,Name from AcctSeed__GL_Account__c where Name='1040 Bremer Checking - Restricted' OR Name = '4400 Individual Contributions']);
list<AcctSeed__Accounting_Variable__c> avarlist = new list<AcctSeed__Accounting_Variable__c>([select Id,Name,AcctSeed__Active__c,AcctSeed__Type__c from AcctSeed__Accounting_Variable__c where AcctSeed__Active__c = True]);
for(AcctSeed__GL_Account__c asgl : asga)
{
    glId.put(asgl.Name,asgl.Id);
}
Map<string,AcctSeed__Accounting_Variable__c> GLAcctMap = new map<string,AcctSeed__Accounting_Variable__c>();
for(AcctSeed__Accounting_Variable__c agacc : avarlist)
{
    GLAcctMap.put(agacc.Name,agacc);
}
set<id> OpptyId = new set<Id>();
set<id> payid = new set<id>();
for(pymt__Shopping_cart_Item__c psct : Trigger.New)
{
    opptyId.add(psct.Donation__c);
    payid.add(psct.pymt__Payment__c);
}
Map<Id,Opportunity> oppmap = new Map<Id,Opportunity> ([select Id,RecordTypeId,Amount,AccountId,Processing_Fee__c,CampaignId,npsp__Primary_Contact__r.AccountId,Teacher__r.School__c from Opportunity Where Id IN: opptyId and RecordTypeId IN:recordmap.keyset()]);
Map<Id,pymt__PaymentX__c> paymap = new map<id,pymt__PaymentX__c>([select Id,pymt__Transaction_Id__c from pymt__PaymentX__c where Id IN: payid]);
Campaign camp = [select Id from campaign where name = 'AdoptAClassroom.org Annual Fund' limit 1];
system.debug('HHHHHHHHHHHHHHHHHHHH'+oppmap);
system.debug('HHHHHHHHHHHHHHHHHHHH'+oppmap.size());
for(pymt__Shopping_cart_Item__c psct : Trigger.New)
{
    system.debug('FFFFFFFFFFFFFFFFFFFF'+psct.Donation__c);
    system.debug('FFFFFFFFFFFFFFFFFFFF'+oppmap.containskey(psct.donation__c));
    system.debug('FFFFFFFFFFFFFFFFFFFF'+psct.pymt__Payment__c);
    system.debug('FFFFFFFFFFFFFFFFFFFF'+psct.pymt__Payment_Completed__c);
    if(oppmap.containskey(psct.donation__c) && psct.pymt__Payment__c != Null && psct.pymt__Payment_Completed__c == True)
    {
        AcctSeed__Cash_Receipt__c acr = new AcctSeed__Cash_Receipt__c();
        acr.AcctSeed__Receipt_Date__c = system.Today();
        acr.AcctSeed__Amount__c = oppmap.get(psct.donation__c).Amount;
        acr.AcctSeed__Status__c = 'Posted'; 
        acr.AcctSeed__Account__c = oppmap.get(psct.donation__c).npsp__Primary_Contact__r.AccountId;
        acr.AcctSeed__Payment_Reference__c = paymap.get(psct.pymt__Payment__c).pymt__Transaction_Id__c;
        //acr.AcctSeed__Purpose__c = 'Customer Receipt';
        acr.AcctSeed__Purpose__c = 'Other Receipt';
        system.debug('FFFFFFFFFFFFFFFFFFFF'+oppmap.get(psct.donation__c).Processing_Fee__c);
        if(oppmap.get(psct.donation__c).CampaignId == camp.Id || oppmap.get(psct.donation__c).Processing_Fee__c == true)
        {
            acr.AcctSeed__Bank_Account__c = glId.get('1040 Bremer Checking - Restricted');
            acr.AcctSeed__Credit_GL_Account__c = glId.get('4400 Individual Contributions');
            acr.AcctSeed__GL_Account_Variable_1__c = GLAcctMap.get('Unrestricted').Id;
            acr.AcctSeed__GL_Account_Variable_2__c = GLAcctMap.get('Development').Id;
          //  acr.AcctSeed__GL_Account_Variable_3__c = GLAcctMap.get('Development').Id;
      
        }
        else
        {
            acr.AcctSeed__Bank_Account__c = glId.get('1040 Bremer Checking - Restricted');
            acr.AcctSeed__Credit_GL_Account__c = glId.get('4400 Individual Contributions');
            acr.AcctSeed__GL_Account_Variable_1__c = GLAcctMap.get('Restricted').Id;
            acr.AcctSeed__GL_Account_Variable_2__c = GLAcctMap.get('Programs').Id;
            acr.AcctSeed__GL_Account_Variable_3__c = GLAcctMap.get('Classroom Adoption').Id;
       
        }
        acr.school_Name__c = oppmap.get(psct.donation__c).Teacher__r.school__c;
        system.debug('***************'+acr.AcctSeed__Status__c );
        acctcashlist.add(acr);
    }
}
if(acctcashlist.size() > 0)
{
    try
    {
        insert acctcashlist;
        
    }
    catch(Exception e)
    {
        system.debug('FFFFFFFFFFFFFFF'+e);
    }

}
}