trigger AnnouncementTrigger on Announcement__c (before insert, after insert, after update) {
    
    if(Trigger.isBefore && Trigger.isInsert){
        AnnouncementTriggerHandler.populateClassroom();
    }
    
    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate )) {
        AnnouncementTriggerHandler.onlyOnePin(Trigger.newMap);
    }
}