trigger ClassroomTrigger on donation_split__Designation__c (before insert, before update, after insert, after update) {
    

    if(Trigger.isBefore && Trigger.isInsert) {
        Map<Id, Id> mapTeacherSchool = new Map<Id, Id>();
        for(donation_split__Designation__c classroom : (List<donation_split__Designation__c>)Trigger.new) {
            if(classroom.Teacher__c != null) {
                mapTeacherSchool.put(classroom.Teacher__c, null);
            }
        }
        if(!mapTeacherSchool.isEmpty()) {
            for(Contact teacher : [SELECT School__c FROM Contact WHERE ID IN :mapTeacherSchool.keySet()]) {
                mapTeacherSchool.put(teacher.Id, teacher.School__c);
            }
        }
        for(donation_split__Designation__c classroom : (List<donation_split__Designation__c>)Trigger.new) {
            classroom.School__c =  mapTeacherSchool.containsKey(classroom.Teacher__c) ? mapTeacherSchool.get(classroom.Teacher__c) : null;
        }
    }
    if(Trigger.isBefore && (Trigger.isInsert ||Trigger.isUpdate)){
        ClassroomTriggerHandler.UpdateRobots(Trigger.new);
    }
    if(Trigger.isAfter){
        if(Trigger.isUpdate) {
            List<donation_split__Designation__c> scope = new List<donation_split__Designation__c>();
            for(donation_split__Designation__c classroom : Trigger.new) {
                if(Trigger.oldMap.get(classroom.Id).donation_split__Active__c != Trigger.newMap.get( classroom.Id ).donation_split__Active__c && Trigger.newMap.get( classroom.Id ).donation_split__Active__c == true) {
                    scope.add(classroom);
                }
            }
            ClassroomTriggerHandler.migrateAnnouncements(Trigger.new);
        } else if(Trigger.isInsert) {
            List<donation_split__Designation__c> scope = new List<donation_split__Designation__c>();
            for(donation_split__Designation__c classroom : Trigger.new) {
                if(Trigger.newMap.get( classroom.Id ).donation_split__Active__c == true) {
                    scope.add(classroom);
                }
            }
            ClassroomTriggerHandler.migrateAnnouncements(Trigger.new);
        }
    }

    /**
     * @description Send interested donor emails once the classroom is active.
     */
    ClassroomTriggerHandler.sendActiveEmails();
}