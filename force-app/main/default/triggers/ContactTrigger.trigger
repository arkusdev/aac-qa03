trigger ContactTrigger on Contact (after update,before update) {
    if (Trigger.isAfter && Trigger.isUpdate ) {
        ContactManagement.afterUpdate(Trigger.new, Trigger.oldMap);
    }

    if(Trigger.isBefore && Trigger.isUpdate) {
        for(Contact con : Trigger.new) {
            if(con.Funds_Available__c != Trigger.oldMap.get(con.Id).Funds_Available__c || ExpFundsFlag__c.getOrgDefaults().Expired_Fund_Contact_Batch__c) {
                con.ToSyncOrderCloud__c = true;
            }
        }
        ContactManagement.beforeUpdate(Trigger.new, Trigger.oldMap);
    }
}