({
    addToCart : function(component, event, helper) {
        
        if (!sessionStorage) {
            return;
        }
        let data = component.get('v.data');
        let newCartItems = [];
        
        if(sessionStorage.getItem('newCartItems')) {
            newCartItems = JSON.parse(sessionStorage.getItem('newCartItems'));
        }
        
        let alreadyInSession = false;
        newCartItems.forEach(element => {
            console.log(element);
            if(element.classroom.classroomId == data.Classroom.Id) {
                alreadyInSession = true;
            }
        });
        
        if(!alreadyInSession) {
            newCartItems.push({    classroom : {
                classroomId : data.Classroom.Id,
                teacherName : data.Classroom.teacherName__c,
                schoolName : data.Classroom.SchoolName__c,
                recordTypeName : data.Classroom.RecordType.Name
            },
            amount : 50.00,
            type : 'generalFund'});
        }
        sessionStorage.setItem('newCartItems', JSON.stringify(newCartItems));
    },
    goToCartPage : function(component, event, helper) {
        var cartUrl = component.get("v.shoppingCartUrl");
	    window.location.href = "/donors/s" + cartUrl;
    }
})