({
    afterRender : function( component, event, helper ) {
        this.superAfterRender();
        
        window.onscroll = function() {stickyWallFloatManu(component, event, helper)};

        function stickyWallFloatManu(component, event, helper) {
            let showingFooter = window.pageYOffset > document.body.clientHeight - window.innerHeight - 200;
            let newTop = document.body.clientHeight - window.innerHeight - window.pageYOffset - 88;
            let wallFloatMenu = component.find('wallFloatMenu');
            if(showingFooter) {
                $A.util.addClass(wallFloatMenu, 'changeMe');
                wallFloatMenu.getElements()[0].style.setProperty("top", newTop + "px");
            } else {
                $A.util.removeClass(wallFloatMenu, 'changeMe');
                wallFloatMenu.getElements()[0].style.setProperty("top", "88px");
            }
        }
    }
})