({
    share : function(component, event, helper){
		var modalBody;
		let data = component.get('v.data');
		$A.createComponent("c:SocialSharingModal", {objectId: component.get('v.data.Classroom.Id'), objectType : (component.get('v.data.Classroom.RecordType.Name') == 'Classroom Registration' ? 'classroom':'school')},
		function(content, status) {
			if (status === "SUCCESS") {
				modalBody = content;
				component.find('overlayLib').showCustomModal({
					body: modalBody,
					showCloseButton: false,
					cssClass: "mymodal slds-modal_small",
				})
			}
		});
	},
	goToDonate : function(component, event, helper) {
		helper.addToCart(component, event, helper);
		helper.goToCartPage(component, event, helper);
	},
	toogleOpen : function(component, event, helper) {
		$A.util.toggleClass(event.currentTarget, 'expanded');
	},
	loadMore : function(component, event, helper) {
		component.set('v.numDonations', component.get('v.numDonations') + 5);
	},
	loadLess : function(component, event, helper) {
		component.set('v.numDonations', component.get('v.numDonations') - 5);
	},
	goToNotify : function(component, event, helper) {
		var modalBody;
		let data = component.get('v.data');
		$A.createComponent("c:NotifyDonor", {	'Classroom' : data.Classroom.Id,
												'Teacher' : data.Teacher.ContactId},
		function(content, status) {
			if (status === "SUCCESS") {
				modalBody = content;
				component.find('overlayLib').showCustomModal({
					body: modalBody,
					showCloseButton: false,
					cssClass: "mymodal cNotifyDonor",
				})
			}
		});
	}
})