({ 
	wrapperInit: function(component, event, helper) {
        helper.sendRequest(component);
	},

  handleCartSessionResponse: function(component, event, helper) {
    var cc = component.getConcreteComponent();
    var cItems = event.getParam("cartItems");
//alert("handleCartSessionResponse");
    //console.log("Donor type");
   //console.log('In Session handle Update'+event.getParam("helpAmount")+event.getParam("fee")+event.getParam("subTotal")+event.getParam("grandTotal"));
    
    component.set("v.cartItems", cItems);
    component.set("v.coverFee", event.getParam("coverFee"));
    component.set("v.subTotal", event.getParam("subTotal"));
    component.set("v.fee", event.getParam("fee"));
    component.set("v.expFees", event.getParam("expFees"));
    component.set("v.helpAmount", event.getParam("helpAmount"));
    component.set("v.frequency", event.getParam("frequency"));   // Rajesh Code     
    component.set("v.grandTotal", event.getParam("grandTotal"));  
    component.set("v.grandmonthlyTotal", event.getParam("grandmonthlyTotal"));    
    component.set("v.total", event.getParam("total"));
    component.set("v.displaySection", event.getParam("displaySection"));
    component.set("v.payment", event.getParam("payment"));
    component.set("v.donorContact", event.getParam("donorContact"));
    component.set("v.donorAccount", event.getParam("donorAccount"));
    component.set("v.donorType", event.getParam("donorType"));
    component.set("v.endDate", event.getParam("endDate"));
    //helper.countTotals(component);
    //helper.countTotalsUpdate(component); // Changed By Brighthat
    if (cc.postResponseMethod) {
      cc.postResponseMethod();
    }
  }
})