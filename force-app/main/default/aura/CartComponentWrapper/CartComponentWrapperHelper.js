({
  sendRequest: function(component) {
    var e = $A.get("e.c:CartSessionRequest");
    e.fire();
  },
  countTotals: function(component) {
    var subTotal = 0;
    var fee = 0;
    var total = 0;
    var monthlySubTotal = 0;
    var monthlyTotal = 0;
    var monthlyFee = 0;
    var helpAmount = 0;
    var expFees = 0;
    var grandTotal = 0; 
    var grandmonthlyTotal = 0;   
    var items = component.get("v.cartItems");
    var coverFee = component.get("v.coverFee");

    if (!items || items.length == 0) {
      this.setTotals(component, subTotal, fee, total, monthlySubTotal, helpAmount, grandTotal, grandmonthlyTotal, expFees,
        monthlyTotal);
      return;
    }
    
    for (var i = 0; i < items.length; i++) {
      subTotal += items[i].amount;
      if(items[i].expenseFees)
        expFees += items[i].expenseFees;  
      if (items[i].frequency == "Monthly") {
        monthlySubTotal += items[i].amount;
      }
    }
     helpAmount = component.get("v.helpAmount");   
     fee = component.get("v.fee");
    //component.get("v.helpAmount");
  //  console.log(helpAmount + fee + subTotal);  
    //component.get("v.fee");
  //  console.log(fee);  
    //  if(helpAmount ==0){
          helpAmount = (subTotal * 0.10);
     // }
   //   if(fee ==0){
    	fee = (subTotal * 0.15);     
    //  }
    //fee = (subTotal * 0.029) + 0.30;
    //monthlyFee = (monthlySubTotal * 0.029) + 0.30;

  /*  if(monthlyFee ==0){
    	monthlyFee = (monthlySubTotal * 0.15); 
        monthlySubTotal += expFees;
      }  */  
      subTotal += expFees;
//  console.log('sub total before'+subTotal);
    if (coverFee) {
      total = subTotal + fee;
      monthlyTotal = monthlySubTotal + monthlyFee;
      grandTotal =  subTotal + fee + helpAmount; 
      grandmonthlyTotal = monthlySubTotal + monthlyFee;   
    } else {
      total = subTotal;
      monthlyTotal = monthlySubTotal;
      grandTotal =  subTotal + fee + helpAmount; 
      grandmonthlyTotal = monthlySubTotal + monthlyFee;    
    }
   //   console.log('help amt'+helpAmount);
   //   console.log('fee'+ fee );
   //   console.log('sub total after'+subTotal);
      
      //helpAmount = subTotal*0.10;
      //fee = subTotal*0.15;
      
      
      
    //  console.log('its in here from normal');
    this.setTotals(component, subTotal, fee, total, monthlySubTotal, helpAmount, grandTotal, grandmonthlyTotal, expFees,
      monthlyTotal, monthlyFee);
  },countTotalsUpdate: function(component) {
    var subTotal = 0;
    var fee = 0;
    var total = 0;
    var monthlySubTotal = 0;
    var monthlyTotal = 0;
    var monthlyFee = 0;
    var helpAmount = 0; 
    var expFees = 0;  
    var grandTotal = 0; 
    var grandmonthlyTotal = 0;   
    var items = component.get("v.cartItems");
    var coverFee = component.get("v.coverFee");

    if (!items || items.length == 0) {
      this.setTotals(component, subTotal, fee, total, monthlySubTotal, helpAmount, grandTotal, grandmonthlyTotal, expFees,
        monthlyTotal);
      return;
    }
      
    for (var i = 0; i < items.length; i++) {
      subTotal += items[i].amount;
      if(items[i].expenseFees)
        expFees += items[i].expenseFees; 
      if (items[i].frequency == "Monthly") {
        monthlySubTotal += items[i].amount;
      }
    }
     helpAmount = component.get("v.helpAmount");   
     fee = component.get("v.fee");
      
      
    //component.get("v.helpAmount");
  //  console.log(helpAmount + fee + subTotal);  
    //component.get("v.fee");
    //console.log(fee);  
    //  if(helpAmount ==0){
          helpAmount = (subTotal * 0.10);
     // }
   //   if(fee ==0){
    	fee = (subTotal * 0.15);       
    //  }
    //fee = (subTotal * 0.029) + 0.30;
    //monthlyFee = (monthlySubTotal * 0.029) + 0.30;

    if(monthlyFee ==0){
    	monthlyFee = (monthlySubTotal * 0.15);
        monthlySubTotal += expFees;
      }    
    subTotal += expFees;  
//console.log('sub total before'+subTotal);
    if (coverFee) {
      total = subTotal + fee;
      monthlyTotal = monthlySubTotal + monthlyFee;
      grandTotal =  subTotal + fee + helpAmount; 
      grandmonthlyTotal = monthlySubTotal + monthlyFee;   
    } else {
      total = subTotal;
      monthlyTotal = monthlySubTotal;
      grandTotal =  subTotal + fee + helpAmount; 
      grandmonthlyTotal = monthlySubTotal + monthlyFee;    
    }
     // console.log('help amt'+helpAmount);
     // console.log('fee'+ fee );
     // console.log('sub total after'+subTotal);
      
      //helpAmount = subTotal*0.10;
      //fee = subTotal*0.15;
      
      
      
      //console.log('its in here from update');
    this.setTotals(component, subTotal, fee, total, monthlySubTotal, helpAmount, grandTotal, grandmonthlyTotal, expFees,
      monthlyTotal, monthlyFee);
  },

  sendCartUpdate: function(component) {
      //this.countTotals(component);// Added by Brighthat
      
    var e = $A.get("e.c:CartSessionUpdate");
    console.log('in send cart update 2'+component.get("v.fee")+'--'+component.get("v.helpAmount")+'--'+component.get("v.grandTotal")+'--'+component.get("v.subTotal"));
    console.log('endDate ' + component.get("v.endDate"));  
    e.setParams({
      cartItems: component.get("v.cartItems"),
      coverFee: component.get("v.coverFee"),
      subTotal: component.get("v.subTotal"),
      fee: component.get("v.fee"),
      expFees: component.get("v.expFees"),  
      helpAmount: component.get("v.helpAmount"),   
      grandTotal: component.get("v.grandTotal"),  
      grandmonthlyTotal: component.get("v.grandmonthlyTotal"),    
      total: component.get("v.total"),
      monthlyTotal: component.get("v.monthlyTotal"),
      monthlySubTotal: component.get("v.monthlySubTotal"),
      monthlyFee: component.get("v.monthlyFee"),
      payment: component.get("v.payment"),
      donorContact: component.get("v.donorContact"),
      donorAccount: component.get("v.donorAccount"),
      donorType: component.get("v.donorType"),
      endDate: component.get("v.endDate")
    });

    e.fire();
  },

  setTotals: function(component, subTotal, fee, total, monthlySubTotal, helpAmount, grandTotal, grandmonthlyTotal, expFees,
      monthlyTotal, monthlyFee) {
    component.set("v.subTotal", subTotal);
    component.set("v.fee", fee);
    component.set("v.expFees", expFees);  
    component.set("v.helpAmount", helpAmount); 
    component.set("v.grandTotal", grandTotal); 
    component.set("v.grandmonthlyTotal", grandmonthlyTotal);   
    component.set("v.total", total);
    component.set("v.monthlySubTotal", monthlySubTotal);
    component.set("v.monthlyTotal", monthlyTotal);
    component.set("v.monthlyFee", monthlyFee);
  }
})