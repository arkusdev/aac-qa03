({
  doInit: function(component, event, helper) {
    helper.initializeObjects(component);
  },
  handleFormUpdate: function(component, event, helper) {
    var eventRequiredFields = event.getParam("requiredFields");
    component.set("v.requiredFields", eventRequiredFields);
    helper.checkValidation(component);
  },
  save: function(component, event, helper) {
    helper.saveSchool(component);
  },
  doneRendering: function(component, event, helper) {
    component.set("v.showUserProfileLoading", false);
  }
})