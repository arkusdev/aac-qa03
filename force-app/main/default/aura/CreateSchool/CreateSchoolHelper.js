({
  initializeObjects: function(component) {
    // Set School record type
    var schoolRecordName = component.get("v.recordTypeName");
    var action = component.get("c.getRecordType");
    action.setParams({
      devName: schoolRecordName
    });

    action.setCallback(this, function(a) {
        var schoolRecordType = a.getReturnValue();
        var school = component.get("v.currentSchool");
        school.RecordTypeId = schoolRecordType;
        component.set("v.currentSchool", school);
    });

    $A.enqueueAction(action);

  },
  clearForm: function(component, fieldSetName) {
    var clearEvent = $A.get("e.c:ClearFieldSetForm");
    clearEvent.setParams({
      fieldSetName: fieldSetName
    });
    clearEvent.fire();
  },
  checkValidation: function(component) {
    var c = component.get("v.currentSchool");
    var requiredFields = component.get("v.requiredFields");
    var valid = true;

    for (var i = 0; i < requiredFields.length; i++) {
      if (!c[requiredFields[i]]) {
        valid = false;
      }
    }

    component.set("v.valid", valid);
  },
  saveSchool: function(component) {
    var s = component.get("v.currentSchool");
    var action = component.get("c.updateSchool");

    component.set("v.saveSuccessful", false);
    component.set("v.saveError", false);
    component.set("v.showUserProfileLoading", true);

    action.setParams({
      s: s
    });

    action.setCallback(this, function(a) {
      var schoolUpdate = a.getReturnValue();
      var state = a.getState();
      var errorMessages = [];

      if (state === "SUCCESS") {
        component.set("v.saveSuccessful", true);
        component.set("v.showUserProfileLoading", false);

        setTimeout(function(){ component.set("v.saveSuccessful", false); }, 5000);

        var e = component.getEvent("schoolCreated");
        e.setParams({"schoolObject" : schoolUpdate });
        e.fire();

      } else if (state === "ERROR") {
        component.set("v.saveError", true);
        var errors = a.getError();
        component.set("v.showUserProfileLoading", false);

        for (i in errors) {
          var currentError = errors[i];

          for (currentField in currentError.fieldErrors) {
            for (cfe in currentError.fieldErrors[currentField]) {
              errorMessages.push(
                currentError.fieldErrors[currentField][cfe].message);
            }
          }
        }

        component.set("v.errorMessages", errorMessages);
      }
    });

    $A.enqueueAction(action);
  }
})