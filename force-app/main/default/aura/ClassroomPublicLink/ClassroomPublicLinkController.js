({
	doInit : function(component, event, helper) {
    	helper.getClassroom(component);
	},
	goToClassroomLink : function(component, event, helper) {
		let classroomLink = component.get('v.classroomLink');
    	window.open(classroomLink, '_blank');
	}
})