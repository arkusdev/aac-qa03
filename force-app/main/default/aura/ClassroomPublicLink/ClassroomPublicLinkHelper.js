({
	getClassroom: function(component) {
    var action = component.get("c.getClassroom");
    action.setParams({
      userId: component.get("v.userId")
    });
    action.setCallback(this, function(a) {
      var classroom = a.getReturnValue();
      if (classroom && classroom.Id) {
        component.set('v.classroom', classroom);

        component.set("v.classroomLink",
          component.get("v.donorCommunityUrl") + classroom.Id);
      }
    });
    $A.enqueueAction(action);
	}
})