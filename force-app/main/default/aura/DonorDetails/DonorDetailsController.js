({
  doInit : function(component, event, helper) {
    helper.checkValidation(component, event, helper);
    console.log('formValid',component.get('v.formValid'));
  },
  onHonorCheck: function(component, event, helper) {
    var checkCmp = component.find("honorCheck");
    component.set("v.inHonorChecked", checkCmp.get("v.value"));
    helper.checkValidation(component, event, helper);
  },
  onDonorTypeChange: function(component, event, helper) {
    helper.donorTypeCheck(component, event);
    helper.sendCartUpdate(component);
    helper.checkValidation(component, event, helper);
  },
  onPostResponse: function(component, event, helper) {
    helper.donorTypeCheck(component);
  },
  doCheckValidation: function(component, event, helper) {
    helper.checkValidation(component, event, helper);
  },
  editGift: function(component, event, helper) {
    var e = $A.get("e.c:CartSectionNavigation");
    
    e.setParams({
      "sectionName": "cart"
    });

    e.fire();
  },
  checkValidEmail: function(component, event, helper) {
    var thisField = event.getSource();
    var fieldValue = thisField.get("v.value");
    var valid = true;

    if (!fieldValue) {
      thisField.set("v.errors", null);
    } else if (!helper.checkValidEmail(fieldValue)) {
      thisField.set("v.errors", [{message:"Email address entered is not in a valid email address format."}]);
      valid = false;
    } else {
      thisField.set("v.errors", null);
    }
    
    component.set("v.formValid", valid);
  },
  doUpdateError: function(component, event, helper) {
    var thisField = event.getSource();
    var fieldValue = thisField.get("v.value");
    var fieldId = thisField.getLocalId();

    // The email is invalid, show error
    if(fieldId.indexOf('email') != -1 && !helper.checkValidEmail(fieldValue)){
      thisField.set("v.errors", [{message:"Email address entered is not in a valid email address format."}]);
    } else if(!fieldValue) {
      thisField.set("v.errors", [{message:"This field is required."}]);
    } else {
      thisField.set("v.errors", null);
    }
  },
  saveDonor: function(component, event, helper) {
    
    var source = event.getSource();
    source.set('v.label','Please wait...');
    var continueBtn = component.find('continueBtn');
    $A.util.addClass(continueBtn, 'not-enabled');
    
    var donor = {};
    var donorType = component.find("donorType").get("v.value");

    var isIndividual = donorType == 'Individual' || donorType == 'Teacher';

    if (!isIndividual) {
      helper.insertAccount(component, donorType);
    } else {
      helper.insertContact(component, donorType, null);
    }
  },
  handleSectionNavigation: function(component, event, helper) {
    component.set("v.displaySection", event.getParam("sectionName"));
    helper.sendRequest(component);
    helper.checkVisibility(component);
  }
})