({
	checkVisibility: function(component) {
    var displaySection = component.get("v.displaySection");

    var showSection = displaySection == "details";

    component.set("v.showSection", showSection);
  },
  checkValidEmail: function(fieldValue) {
    return /^.+@.+\..+$/.test(fieldValue);
  },
  donorTypeCheck: function(component, event) {
    var donorTypeString = component.get("v.donorType");
    component.set("v.donorTypeString", donorTypeString);

    component.set("v.showBusiness", false);
    component.set("v.showIndividual", false);

    if (donorTypeString == 'Individual') {
      component.set("v.showIndividual", true);
    } else if (donorTypeString == 'Business' || donorTypeString == 'Foundation' || donorTypeString == 'Non-Profit') {
      component.set("v.showBusiness", true);
    }
    
    this.checkValidation(component, event, this);
  },
  checkValidation: function(component, event, helper) {

    var requiredFieldIDs = ["email", "fname", "lname", "donorType"];
    var isValid = true;

    var inHonorChecked = component.get("v.inHonorChecked");
    var showIndividual = component.get("v.showIndividual");
    var showBusiness = component.get("v.showBusiness");
    var donorAccount = component.get("v.donorAccount");
    var donorContact = component.get("v.donorContact");

    if (inHonorChecked) {
      requiredFieldIDs.push('honorName');
    }
    
    if (showBusiness && !donorAccount.Name) {
      valid = false;
    }

    for (var i = 0; i < requiredFieldIDs.length; i++) {
      var fieldId = requiredFieldIDs[i];
      var field = component.find(fieldId);

      if (field) {
        var fieldValue = field.get("v.value");
        if (!fieldValue) {
          // There is no value in this field
          isValid = false;
        }
        if(fieldId.indexOf("email") != -1 && !helper.checkValidEmail(fieldValue)){
          // Check that the field is an email address
          isValid = false;
        }
      }
    }

    //check required fields
    let emailValue = component.get('v.donorContact.Email');
    let fnameValue = component.get('v.donorContact.FirstName');
    let lnameValue = component.get('v.donorContact.LastName');
    let donorType = component.get('v.donorType');
    if(emailValue == '' || fnameValue == '' || lnameValue == '' || donorType == '') {
      isValid = false;
    }

    //check email
    if(!helper.checkValidEmail(emailValue)) {
      isValid = false;
    }

    //check donorType variations
    let requireddonorTypeValue = component.get('v.donorAccount.Name');
    console.log('requireddonorTypeValue', requireddonorTypeValue);
    console.log('donorType', donorType);
    if((donorType == 'Business' || donorType == 'Foundation' || donorType == 'Non-Profit') && (!requireddonorTypeValue || requireddonorTypeValue == '')) {
      isValid = false;
    }
    

    if (isValid) {
      component.set("v.formValid", true);
    } else {
      component.set("v.formValid", false);
    }
  },

  insertAccount: function(component, donorType) {
    
    var donor = component.get("v.donorAccount");
    
    if (!donor) {
      donor = {};
    }

    donor.sObjectType = 'Account';
    donor.Email__c = component.find('email').get('v.value');

    if (donorType == 'Non-Profit') {
      donorType = 'Organization';
    }
	//alert("kaka");
      //alert(component.get("v.compB"));
    //  var objChild = component.find('compB');
       // alert("Method Called from Child " + objChild.get('v.mypicklist'));
       //return false;
    // Upsert business donor (account) information to the controller
    
    var action = component.get("c.upsertAccount");

    action.setParams({
      a: donor,
      donorType: donorType
    });

    action.setCallback(this, function(a) {
      var thisDonor = a.getReturnValue();
      var state = a.getState();

      if (state === "SUCCESS") {
        component.set("v.donorAccount", thisDonor);
        console.log('thisDonor', thisDonor);
        this.insertContact(component, donorType, thisDonor);
      } else if (state === "ERROR") {
        //console.log("insert account error");
        console.log(a.getError());
      }
    });

    $A.enqueueAction(action);
  },

  savePaymentAndOpportunities: function(component, account, contact) {
    
    var action = component.get("c.savePaymentData");
//alert("sheel");
      //alert(component.get("v.compB"));
      var objChild = component.find('compB');
        //alert("Method Called from Child Two " + objChild.get('v.mypicklist'));
     // var mypicklistvalue=objChild.get('v.mypicklist');
     var mypicklistvalue;
      if(objChild){
         mypicklistvalue = objChild.get('v.mypicklist'); 
      }
     // alert("kaka"+mypicklistvalue);
    var iho = component.find("honorName");
    var ihoName;
    var ihoe = component.find("emailHonor");
    var ihoemail;
    var pomocode = component.find("promocode");
    var pcode;  
	//console.log('RRRRRRRRRR'+component.get("v.payment.Id"));
    if (iho) {
      ihoName = iho.get("v.value");
    }
      if(ihoe){
       ihoemail = ihoe.get("v.value");   
      }
          if(pomocode){
           pcode = pomocode.get("v.value");   
          }
     console.log('fire method controller: ' + component.get("v.endDate")); 
    action.setParams({
      donorComment: component.get("v.donorComment"),
      c: contact,
      a: account,
      payment: component.get("v.payment"),
      cartItemString: JSON.stringify(component.get("v.cartItems")),
      inHonorOfName: ihoName,
      inHonorOfEmail:ihoemail,
      paymentPromoCode:pcode,  
      subTotal: component.get("v.subTotal"),
      grandTotal: component.get("v.grandTotal"),
      total: component.get("v.total"),
      monthlySubTotal: component.get("v.monthlySubTotal"),
      monthlyTotal: component.get("v.monthlyTotal"),
      fee: component.get("v.fee"),
      expFees: component.get("v.expFees"),
      helpAmount: component.get("v.helpAmount"), 
      monthlyFee: component.get("v.monthlyFee"),
      isProcessingFeeIncluded: component.get("v.coverFee"),
      myPicklist:mypicklistvalue,
      endDate: component.get("v.endDate")
    });
      

    action.setCallback(this, function(a) {
      var payment = a.getReturnValue();
      var state = a.getState();
	  //console.log('WWWWWWWWWWWWWW'+state);
      if (state === "SUCCESS") {
        component.set("v.payment", payment);
        this.sendCartUpdate(component);
        var e = $A.get("e.c:CartSectionComplete");

        e.setParams({
          sectionName: "details",
          nextSectionName: "payment"
        })

        e.fire();
      } else if (state === "ERROR") {
        console.log(a.getError());
      }
        else {
        console.log("Failed with state: " + state);
      }
      
    });

    $A.enqueueAction(action);
  },

  insertContact: function(component, donorType, account) {
    var donor = component.get("v.donorContact");
    
    if (!donor) {
      donor = {};
    }

   // var updatesChecked = component.find("emailUpdatesCheck").get("v.value");
    
   // donor.HasOptedOutOfEmail = !updatesChecked;

    donor.sobjectType = 'Contact';

    if(donorType == 'Individual'){
      donor.DonorDisplayName__c = component.find('donorDisplayName').get('v.value');
    }

    if (account) {
      donor.AccountId = account.Id;
    }

    // Upsert person donor (contact) information to the controller
    var action = component.get("c.upsertContact");
    action.setParams({
      c: donor
    });

    action.setCallback(this, function(a) {
      var thisDonor = a.getReturnValue();
      var state = a.getState();

      if (state === "SUCCESS") {
        var payment = component.get("v.payment");
        payment.Payment_Contact__c = thisDonor.Id; // pymt__Contact__c
        component.set("v.donorContact", thisDonor);
        component.set("v.payment", payment);
        console.log('account', account);
        this.savePaymentAndOpportunities(component, account, thisDonor);
      } else if (state === "ERROR") {
        //console.log("Insert Contact Error");
        console.log(a.getError());
      }
      
    });

    $A.enqueueAction(action);
  }
})