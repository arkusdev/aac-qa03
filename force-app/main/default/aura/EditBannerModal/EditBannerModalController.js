({
	doInit: function(component, event, helper) {
        helper.init(component,event);
   	},
    onDragOver: function(component, event) {
        event.preventDefault();
    },
    onDrop: function(component, event, helper) {
		event.stopPropagation();
        event.preventDefault();
        event.dataTransfer.dropEffect = 'copy';
        var files = event.dataTransfer.files;
        if (files.length>1) {
            return alert("You can only upload one profile picture");
        }
        component.set('v.file', files[0]);
        helper.readFile(component, helper, files[0], false);
	},
	doCloseModal: function(component, event, helper) {
		helper.closeModal(component);
	},
	notifyFileSelected: function(component, event, helper) {
        var file = component.find("file").getElement().files[0];
        component.set('v.file', file);
        helper.readFile(component, helper, file, false);
	},
    addHover: function(component, event, helper) {
        var button = component.find("upload-button");
        $A.util.addClass(button, 'button-hover');
	},
    removeHover: function(component, event, helper) {
        var button = component.find("upload-button");
        $A.util.removeClass(button, 'button-hover');
	}

})