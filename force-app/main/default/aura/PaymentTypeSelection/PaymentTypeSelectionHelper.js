({
	checkVisibility: function(component) {
    var displaySection = component.get("v.displaySection");

    var showSection = displaySection == "payment";

    if (showSection) {
      this.sendRequest(component);
    }

    component.set("v.showSection", showSection);
  },

  jqueryStuff: function(component) {
    $('.shoppingCart').affix({
      offset: {
        top: 560,
        bottom: 355
      }
    });
  },

  resetActiveClasses: function(component) {
    var creditCardLink = component.find("creditCardLink");
    var checkLink = component.find("checkLink");

    $A.util.removeClass(creditCardLink, 'active');
    $A.util.removeClass(checkLink, 'active');
  },

  addActiveClass: function(component, auraId) {
    var l = component.find(auraId);
    $A.util.addClass(l, "active");
  },

  updatePaymentAndForward: function(component, type, isCreditCard, isCheck) {
    var action = component.get("c.updatePaymentAndReturnLink");
    var payment = component.get("v.payment");

    payment.ChargentOrders__Payment_Method__c = type; //pymt__Payment_Type__c

    action.setParams({
      payment: payment
    });

    action.setCallback(this, function(a) {
      var state = a.getState();

      if (state === "SUCCESS") {

        component.set("v.creditCardSelected", isCreditCard);
        component.set("v.checkSelected", isCheck);
		//console.log('TTTTTTTTTTTT'+component.get("v.finishUrl"));
        //console.log('UUUUUUUUUUUUU'+component.get("v.cancelUrl"));
        component.set("v.forwardUrl", ".." + a.getReturnValue() + "&finish_url=" +
        component.get("v.finishUrl") + "&cancel_url=" +
        component.get("v.cancelUrl"));
          
      } else if (state === "ERROR") {
        //console.log(a.getError());
      }
    });

    $A.enqueueAction(action);
  }
})