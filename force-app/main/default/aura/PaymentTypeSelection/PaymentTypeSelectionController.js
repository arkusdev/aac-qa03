({

  handleSectionNavigation: function(component, event, helper) {
    component.set("v.displaySection", event.getParam("sectionName"));
    helper.checkVisibility(component);
  },

  selectCreditCard: function(component, event, helper) {
    helper.resetActiveClasses(component);
    helper.addActiveClass(component, "creditCardLink");
    helper.updatePaymentAndForward(component, "Credit card", true, false);
    helper.jqueryStuff(component);
  },

  selectCheck: function(component, event, helper) {
    helper.resetActiveClasses(component);
    helper.addActiveClass(component, "checkLink");
    helper.updatePaymentAndForward(component, "Check or Money Order",
      false, true);
  },
  runPostResponse: function(component, event, helper) {
    var payment = component.get("v.payment");

    if (payment.Id) {
      helper.resetActiveClasses(component);
      helper.addActiveClass(component, "creditCardLink");
      helper.updatePaymentAndForward(component, "Credit card", true, false);
    }
  },
})