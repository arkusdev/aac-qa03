({
  showQuestionModal: function(component, componentId, helper) {
    helper.showPopupHelper(component, 'modaldialog', 'slds-fade-in-');
    helper.showPopupHelper(component, 'backdrop', 'slds-backdrop--');
  },
  hideQuestionModal: function(component, event, helper) {
    helper.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
    helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop--');
  },

  onInit: function(component, event, helper) {
    var action = component.get("c.getFAQs");
    action.setCallback(this, function(a) {
      var faqs = a.getReturnValue();
      component.set('v.faqItems', faqs);
    });
    $A.enqueueAction(action);
  }
})