({
    copyTextHelper : function(component,event,text) {
        var hiddenInput = document.createElement("input");
        hiddenInput.setAttribute("value", text);
        document.body.appendChild(hiddenInput);
        hiddenInput.select();
        document.execCommand("copy");
        document.body.removeChild(hiddenInput);
        this.showToast('Copied!', 'Link Succesfully Copied!', 'success');
    },

    setHeadMeta :function(component, event){
        var meta = document.createElement("meta");
        meta.setAttribute("property", "og:image");
        meta.setAttribute("content", "https://www.adoptaclassroom.org/wp-content/uploads/2019/08/AAC-Default-FBTWLI-1.jpg");
        document.getElementsByTagName('head')[0].appendChild(meta);
    },

    showToast : function(title, message, type){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },

    getClassInformation : function(component, event){
        let getInfo = component.get('c.getClassroom');
        getInfo.setParams({
            objectId : component.get('v.objectId')
        });
        getInfo.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                let classroom = response.getReturnValue();
                component.set('v.classroom', classroom);
                component.set('v.tweetUrl', 'https://twitter.com/intent/tweet?text=These students need classroom materials to make it through the school year. You can help ensure these students have the resources they need to succeed in school. Visit this fundraising page to learn more and donate:%0D' + classroom.Public_Classroom_Landing_Page_URL__c.match(/href="([^"]*)/)[1]);
            } else {
                console.log(response.getError());
            }
        });
        $A.enqueueAction(getInfo);
    },

    getClassroomEmail : function(component){
        let ClassroomEmail1 = $A.get("$Label.c.ClassroomEmail1").replace('{0}', component.get('v.classroom.Teacher__r.Name'));
        let ClassroomEmail2 = $A.get("$Label.c.ClassroomEmail2").replace('{1}', component.get('v.classroom.Public_Classroom_Landing_Page_URL__c').match(/href="([^"]*)/)[1]);
        return ClassroomEmail1.concat('\n\n').concat(ClassroomEmail2);
    },

    getSchoolEmail : function(component){
        let schoolEmail1 = $A.get("$Label.c.SchoolEmail1").replace('{0}', component.get('v.classroom.School__r.Name'));
        let schoolEmail2 = $A.get("$Label.c.SchoolEmail2").replace('{1}', component.get('v.classroom.Public_Classroom_Landing_Page_URL__c').match(/href="([^"]*)/)[1]);
        return schoolEmail1.concat('\n\n').concat(schoolEmail2);
    },
})