({
    init : function(component, event, helper){
        let shareUrl = component.get('v.shareUrl');
        if(shareUrl == '') {
            component.set('v.shareUrl', window.location.origin + (window.location.pathname.includes('/teachers/s/') ? '/donors/s/designation/' + component.get('v.objectId'): window.location.pathname));
        }
        helper.getClassInformation(component, event);
    },

    copyLink : function(component, event, helper) {
        helper.copyTextHelper(component, event, component.get('v.shareUrl'))
    },

    mailToLink : function(component, event, helper){
        let emailBody = component.get('v.objectType') === 'classroom' ? helper.getClassroomEmail(component) : helper.getSchoolEmail(component);
        var mailToLink = "mailto:?body=" + encodeURIComponent(emailBody);
        window.open(mailToLink,'_blank');
    },

    handleCancel : function(component, event, helper) {
        component.find("overlayLib").notifyClose();
    }
})