({
  handleClick: function(component, event, helper) {

    var linkType = component.get("v.linkType");
    var buttonURL = component.get("v.url");

    if(linkType == "PopUp"){
      window.open(buttonURL);
    } else if(linkType == "CreateRecord"){
      var createRecordEvent = $A.get("e.force:createRecord");
      console.log(createRecordEvent);
      var recordType = component.get("v.recordType");
      createRecordEvent.setParams({
          "entityApiName": recordType
      });
      createRecordEvent.fire();
    } else {
      var type = "School";
      var navigateEvent = $A.get("e.force:navigateToURL");
      navigateEvent.setParams({
          "url": buttonURL,
          "recordTypeId": type
      });
      navigateEvent.fire();
    }
  }
})