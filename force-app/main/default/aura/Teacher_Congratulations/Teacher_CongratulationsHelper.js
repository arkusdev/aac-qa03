({
    onInit : function(component, event, helper) {
		var action= component.get('c.getClassroomId');
        action.setCallback(this, function(response){
            var state= response.getState();
            if(state === 'SUCCESS'){
                let responseValue= response.getReturnValue();
                if(responseValue != ''){
                    //component.set('v.classroomId', responseValue)
                    component.set('v.classroomViewPageUrl', '/donors/s/designation/'+responseValue)
                    //component.set('v.classroomEditPageUrl', '../'+responseValue)
                }
            }
        });
        $A.enqueueAction(action);
    }
})