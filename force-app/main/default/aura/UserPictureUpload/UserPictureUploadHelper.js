({
    loadProfileURL : function(component, event, helper) {
        let action = component.get("c.getProfilePicture");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state == "SUCCESS") {
                let result = response.getReturnValue();
                component.set('v.userImage', result);
            } else if (state == "ERROR") {
                var errors = action.getError();
                if (errors[0] && errors[0].message) {                         
                    console.log(errors[0].message);             
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    editPhoto : function(component) {
    	    var modalBody;
            $A.createComponent("c:EditPhotoModal", {},
                               function(content, status) {
                                   if (status === "SUCCESS") {
                                       modalBody = content;
                                       component.find('overlayLib').showCustomModal({
                                           header: "Update Photo",
                                           body: modalBody,
                                           showCloseButton: true,
                                           cssClass: "mymodal slds-modal_small",
                                           closeCallback: function() {
                                            //$A.get('e.force:refreshView').fire();
                                           }
                                       })
                                   }
                               });
	}
})