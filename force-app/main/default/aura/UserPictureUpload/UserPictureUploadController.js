({
    doInit : function(component, event, helper) {
        helper.loadProfileURL(component, event, helper);
    },
    handleEditPhoto : function(component, event, helper) {
        helper.editPhoto(component);
    },
    refreshImage : function(component, event, helper) {
        
        window.setTimeout(
            $A.getCallback(function() {
                helper.loadProfileURL(component, event, helper);
            }), 1000
        );
    }
})