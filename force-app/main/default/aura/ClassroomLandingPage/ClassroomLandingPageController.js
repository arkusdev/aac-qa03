({
	doInit : function(component, event, helper) {

		// Google Tag Manager - Real GTM-T57V5B | Test GTM-5H2FXD
		(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-T57V5B');
		
		window.addEventListener('scroll', function() {stickyWallFloatManu(component, event, helper)});

		let actionData = component.get("c.getData");
		actionData.setParams({recordId: component.get("v.recordId")});
		actionData.setCallback(this, function(result) {
			let resultData = result.getReturnValue();
			resultData.Donations.forEach(function(element) {
				element['CommentLenght'] = element.Donor_comment__c ? element.Donor_comment__c.length : 0;
			});
			if(!resultData.Teacher.Contact.Donors_This_Year__c) {
				resultData.Teacher.Contact.Donors_This_Year__c = 0;
			}
			component.set('v.data', resultData);
			component.set('v.loading', false);
		});
		$A.enqueueAction(actionData);
	},
	seeAllDescription : function(component, event, helper) {
		var cmpTarget = component.find('description');
		$A.util.addClass(cmpTarget, 'expanded');

		var seeAllDescription = component.find('seeAllDescription');
		$A.util.addClass(seeAllDescription, 'hide');

		var readLessDescription = component.find('readLessDescription');
		$A.util.removeClass(readLessDescription, 'hide');
	},
	readLessDescription : function(component, event, helper) {
		var cmpTarget = component.find('description');
		$A.util.removeClass(cmpTarget, 'expanded');

		var seeAllDescription = component.find('seeAllDescription');
		$A.util.removeClass(seeAllDescription, 'hide');

		var readLessDescription = component.find('readLessDescription');
		$A.util.addClass(readLessDescription, 'hide');
	},
	toogleOpen : function(component, event, helper) {
		$A.util.toggleClass(event.currentTarget, 'expanded');
	},
	loadMore : function(component, event, helper) {
		component.set('v.numDonations', component.get('v.numDonations') + 5);
	},
	loadLess : function(component, event, helper) {
		component.set('v.numDonations', component.get('v.numDonations') - 5);
	}
})