({
	doInit: function(component, event, helper) {
    helper.getClassroomDetails(component);
	},

  removeCartItem: function(component, event, helper) {
    var e = component.getEvent("removeCartItem");

    e.setParams({
      "index": component.get("v.itemIndex")
    });

    e.fire();
  },

  updateCartItem: function(component, event, helper) {
    var ci = component.get("v.cartItem");

    if (!ci) {
      return;
    }
    if ((!ci.amount || ci.amount < 10) && ci.type != "generalFund") {
      component.set("v.showMinimumError", true);
    } else {
      component.set("v.showMinimumError", false);
    }

    var e = component.getEvent("updateCartItem");
    e.fire();
  }
  /*
  updateExpenseFees: function(component, event, helper) {
    var classroom = component.get("v.classroom");
    var cartItem = component.get("v.cartItem");
    cartItem.expenseFees = classroom.RecordType.DeveloperName == 'School_Registration' ? 
                           (cartItem.amount * 10 / 100) : 0;
    component.set("v.cartItem", cartItem);  
  } */ 
})