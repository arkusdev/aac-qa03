({
getClassroomDetails: function(component) {

    classroomId = component.get("v.cartItem").classroomId;
    if(classroomId) {
      var action = component.get("c.getClassroomById");
      action.setParams({
        classroomId: classroomId
      });
  
      action.setCallback(this, function(a) {
        var state = a.getState();
  
        if (state === "SUCCESS") {
          component.set("v.classroom", a.getReturnValue());
          var classroom = component.get("v.classroom");
          var RecordType = component.get("v.classroom.RecordType.DeveloperName");  
          var cartItem = component.get("v.cartItem");
          component.set("v.cartItem", cartItem);
          
          // GTM DataLayer Push
          // window.dataLayer = window.dataLayer || [];
          // dataLayer.push({
          //   'transactionProducts': [{
          //     'name': classroom.GTM_Name__c,
          //     'category': classroom.GTM_Category__c
          //   }]
          // });
          // END - GTM DataLayer Push
        }
      });
  
      $A.enqueueAction(action);
    }
	}
})