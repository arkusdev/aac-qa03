({
    checkAllFields: function(component, event, helper){
        component.set("v.disableSave", true);
        let myInputs = component.find('form').find({instancesOf : "lightning:input"});
        let myTextAreas = component.find('form').find({instancesOf : "lightning:textarea"});
        let myCombobox = component.find('form').find({instancesOf : "lightning:combobox"});
        let myDualListbox = component.find('form').find({instancesOf : "lightning:dualListbox"});

        let validFields = [];

        if(myInputs.length > 0) {
            validFields = validFields.concat(myInputs);
        }
        if(myTextAreas.length > 0) {
            validFields = validFields.concat(myTextAreas);
        }
        if(myCombobox.length > 0) {
            validFields = validFields.concat(myCombobox);
        }
        if(myDualListbox.length > 0) {
            validFields = validFields.concat(myDualListbox);
        }

        var imgSrc = document.querySelector('.profile-picture').src;

        if(imgSrc != undefined && (imgSrc.includes('Step-3-Icon-1') || imgSrc.includes('/005/'))){
            component.set("v.disableSave", true);
            return;
        }
        
        var allValid = validFields.reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);

        return allValid;
    }
})