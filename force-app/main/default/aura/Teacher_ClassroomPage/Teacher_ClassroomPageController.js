({
    doInit: function (component, event, helper) {
        var action = component.get("c.getData");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log(result);
                result.classroom.Hide_My_Page_from_Search_Results_Online__c = !result.classroom.Hide_My_Page_from_Search_Results_Online__c;
                component.set("v.classroom", result.classroom);
                component.set("v.optionsTeachingArea", result.teachingAreaOptions);
                component.set("v.optionsGrade", result.gradesOptions);
                let existStr = result.classroom.Grades__c;
                let arryVals = existStr.split(';');
                component.find('Grades').set('v.value', arryVals);
                

            } else if (state === "ERROR") {
                var errors = action.getError();
                
                if (errors[0] && errors[0].message) {                         
                    console.log(errors[0].message);
                    toastEvent.setParams({
                        "title": "Error",
                        "type": "error",
                        "message": errors[0].message
                    });
                    toastEvent.fire();
                }
            }
        });

        $A.enqueueAction(action);
    },
    doneRendering: function (component, event, helper) {
        if (!component.get("v.isDoneRendering")) {
            try {
                window.setTimeout(
                    function () {
                        var imgSrc = document.querySelector(".profile-picture").src;

                        if (imgSrc != undefined && imgSrc.includes("/005/")) {
                            component.set("v.disableSave", true);
                            document.querySelector(".profile-picture").src =
                                $A.get("$Resource.Teacher_Community") +
                                "/Assets/Step-3/Step-3-Icon-1.png";
                        }

                        component.set("v.isDoneRendering", true);
                    },
                    500
                );
            } catch (error) {
                console.error(error);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: "Unexpected Error",
                    type: "error",
                    message: error
                });
                toastEvent.fire();
            }
        }
    },
    saveClassroom: function (component, event, helper) {
        let record = component.get("v.classroom");
        let myInputs = component.find('form').find({instancesOf : "lightning:input"});
        let myCombobox = component.find('form').find({instancesOf : "lightning:combobox"});
        let myDualListbox = component.find('form').find({instancesOf : "lightning:dualListbox"});

        let validFields = [];
        if(myInputs.length > 0) {
            validFields = validFields.concat(myInputs);
        }
        if(myCombobox.length > 0) {
            validFields = validFields.concat(myCombobox);
        }
        if(myDualListbox.length > 0) {
            validFields = validFields.concat(myDualListbox);
        }

        var allValid = validFields.reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);

        let makeMyPagePublic = component.get('v.classroom.Hide_My_Page_from_Search_Results_Online__c');
        let currentProfileImage = component.get('v.userImage');
        let validProfileImage = ((makeMyPagePublic == false) || (makeMyPagePublic == true && currentProfileImage != '/teachers/profilephoto/005/M'));
        let validDescription = record.Description__c != undefined && ((record.Description__c).length >= 200 && (record.Description__c).length <= 2000);
        let validHowWillYouSpendYourFunds = record.How_will_you_spend_your_funds__c != undefined && ((record.How_will_you_spend_your_funds__c).length >= 50 || (record.How_will_you_spend_your_funds__c).length <= 2000);
        let validWhyWeNeedYourHelp = record.Why_We_Need_Your_Help__c != undefined && ((record.Why_We_Need_Your_Help__c).length >= 200 || (record.Why_We_Need_Your_Help__c).length <= 2000);
        
        if(!validProfileImage) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Error",
                type: "error",
                message: "Please upload a profile photo to create a public page."
            });
            toastEvent.fire();
        }
        
        if(!validDescription) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Error",
                type: "error",
                message: "Please check the character length of 'About My Classroom'"
            });
            toastEvent.fire();
        }

        if(!validHowWillYouSpendYourFunds) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Error",
                type: "error",
                message: "Please check the character length of 'What We Need'"
            });
            toastEvent.fire();
        }

        if(!validWhyWeNeedYourHelp) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Error",
                type: "error",
                message: "Please check the character length of 'Why We Need Your Help'"
            });
            toastEvent.fire();
        }

        if (allValid && validDescription && validHowWillYouSpendYourFunds && validWhyWeNeedYourHelp && validProfileImage) {
            var action = component.get("c.saveRecord");
            

            record.Hide_My_Page_from_Search_Results_Online__c = !record.Hide_My_Page_from_Search_Results_Online__c;

            action.setParams({
                classToSave: record
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    if (result != null) {
                        record.Id = result;
                        record.Hide_My_Page_from_Search_Results_Online__c = !record.Hide_My_Page_from_Search_Results_Online__c;
                        component.set("v.classroom", record);
                    }
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: "Success",
                        type: "success",
                        message: "The Classroom has been updated successfully."
                    });
                    toastEvent.fire();
                    var appEvent = $A.get("e.c:Teacher_SchoolSelectedEvt");
                    appEvent.fire();
                } else if (state === "ERROR") {
                    var appEvent = $A.get("e.c:Teacher_DisableNextEvt");
                    appEvent.fire();
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log(
                                "Error trying to save classroom. Message: " + errors[0].message
                            );

                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                title: "Unexpected Error",
                                type: "error",
                                message: errors[0].message
                            });
                            toastEvent.fire();
                        }
                    } else {
                        console.log("Unknown error");
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title: "Error",
                            type: "error",
                            message: "Unexpected Error"
                        });
                        toastEvent.fire();
                    }
                }
            });

            $A.getCallback(function () {
                $A.enqueueAction(action);
            })();
        }
    },
    onlyNumbers : function(component, event, helper){
        var value = event.getSource().get("v.value");
        var newValue = value.replace(/\D/g,'');
        event.getSource().set("v.value", newValue);
    },
    handleGradeUpdate : function(component, event, helper){
        let arryOfSelVals = component.find('Grades').get('v.value');
        let strOfVals = arryOfSelVals.join(';');
        component.set('v.classroom.Grades__c', strOfVals);
    }
});