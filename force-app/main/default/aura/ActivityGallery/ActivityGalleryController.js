({  doInit : function(component, event, helper) {
        let activityGallery = component.get("v.activity");
        if (sessionStorage.getItem(activityGallery)) {
            var numPhotos = parseInt(sessionStorage.getItem(activityGallery));
            component.set("v.numPhotos", numPhotos);
        } else {
            sessionStorage.setItem(activityGallery, component.get("v.numPhotos"));
        }

        let videos = document.querySelectorAll("#video");
        if(videos != null) {
            videos.forEach(function(video){
                video.pause();
            });
        }
    },
    loadMore : function(component, event, helper) {
        let activityGallery = component.get("v.items[0].RelatedRecordId");
        component.set('v.numPhotos', component.get('v.numPhotos') + 6);
        sessionStorage.setItem(activityGallery, component.get("v.numPhotos"));
    },
    loadLess : function(component, event, helper) {
        let activityGallery = component.get("v.items[0].RelatedRecordId");
        component.set('v.numPhotos', component.get('v.numPhotos') - 6);
        sessionStorage.setItem(activityGallery, component.get("v.numPhotos"));
    },
    deletePhoto : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var photoid = selectedItem.dataset.photoid;

        var action = component.get("c.delPhoto");
        action.setParams({
            "fileId" : photoid
          });
        action.setCallback(this, function(result) {
            var appEvent = $A.get("e.c:refreshActivities");
            appEvent.fire();
        });
        $A.enqueueAction(action);
    },
    openItem : function(component, event, helper) {
        var cmpTarget = document.getElementById('open-'+event.currentTarget.dataset.modalid);
        cmpTarget.classList.add("show");
    },
    closeItem : function(component, event, helper) {
        var cmpTarget = document.getElementById('open-'+event.currentTarget.dataset.modalid);
        cmpTarget.classList.remove("show");
        let videos = document.querySelectorAll("#video");
        if(videos != null) {
            videos.forEach(function(video){
                video.pause();
            });
        }
    }
})