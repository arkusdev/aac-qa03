({
    validateStep : function(component, event, helper) {
        let allValid = component.find('stepField').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            if(inputCmp.get('v.value') && typeof inputCmp.get('v.value') == 'string' && inputCmp.get('v.value').includes(" ")) {
                var value = '';
                value = inputCmp.get('v.value');
                inputCmp.set('v.value', value.trim());
            }
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        return allValid;
    },
    phoneNumberFormat : function(component, event, helper) {
        let valuePhone = event.getSource().get("v.value");

        let value = valuePhone.replace(/\D/g,'');
  
        if(value.length >= 3 && value.length < 6) 
            value = value.slice(0,3) + "-" + value.slice(3);
        else if(value.length >= 6) 
            value = value.slice(0,3) + "-" + value.slice(3,6) + "-" + value.slice(6);
        
        event.getSource().set("v.value", value);
    }
})