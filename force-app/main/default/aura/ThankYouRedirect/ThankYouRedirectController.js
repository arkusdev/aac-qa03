({
	doInit: function(component, event, helper) {
    if (helper.inIframe(component) === true) {
        window.top.location.href = window.location.href;
    }
	}
})