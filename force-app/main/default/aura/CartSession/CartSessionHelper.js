({
	fireSessionResponse: function(component) {
    var e = $A.get("e.c:CartSessionResponse");
    
    e.setParams({
      "cartItems": component.get("v.cartItems"),
      "coverFee": component.get("v.coverFee"),
      "subTotal": component.get("v.subTotal"),
      "fee": component.get("v.fee"),
      "expFees": component.get("v.expFees"), 
      "helpAmount": component.get("v.helpAmount"),
      "grandTotal": component.get("v.grandTotal"),  
      "grandmonthlyTotal": component.get("v.grandmonthlyTotal"), 
      "monthlyTotal": component.get("v.monthlyTotal"), 
      "monthlySubTotal": component.get("v.monthlySubTotal"), 
      "monthlyFee": component.get("v.monthlyFee"),           
      "total": component.get("v.total"),
      "currentSection": component.get("v.currentSection"),
      "displaySection": component.get("v.displaySection"),
      "payment": component.get("v.payment"),
      "latestClassroomId": component.get("v.latestClassroomId"),
      "donorContact": component.get("v.donorContact"),
      "donorAccount": component.get("v.donorAccount"),
      "donorType": component.get("v.donorType"),
      "endDate": component.get("v.endDate")
    });
	console.log('**In Set Method'+component.get("v.expFees"));
    e.fire();
	},

  addCartItem: function(component, event) {
    var items = component.get("v.cartItems");
    var newClassroomId = event.getParam("classroomId");

    if (this.classroomAlreadyInCart(component, newClassroomId)) {
      return;
    }

    if (!items) {
      items = [];
    }

    var i = {};

    i.classroomId = newClassroomId;
    i.amount = event.getParam("amount");
    i.frequency = "One time";

    items.unshift(i);

    component.set("v.cartItems", items);
    component.set("v.latestClassroomId", i.classroomId);
    this.saveToSession(component);
  },

  classroomAlreadyInCart: function(component, classroomId) {
    var items = component.get("v.cartItems");

    if (!items) {
      return false;
    }

    for (var i = 0; i < items.length; i++) {
      if (items[i].classroomId == classroomId) {
        return true;
      }
    }

    return false;
  },

  initGeneralFundItem: function(component) {
    var ci = component.get("v.cartItems");

    var i = {};

    i.type = "generalFund";
    i.amount = 0;
    i.frequency = "One time";

    ci.push(i);

    component.set("v.cartItems", ci);
    this.saveToSession(component);
  },

  updateCart: function(component, event) {
    var newItems = event.getParam("cartItems");
      var totAmount = 0;
      for(var i=0;i<component.get("v.cartItems").length;i++)
      {
          totAmount = totAmount + component.get("v.cartItems")[i].amount;
          console.log(totAmount);
      }
      
    component.set("v.cartItems", newItems);
    component.set("v.coverFee", event.getParam("coverFee"));
    component.set("v.subTotal", event.getParam("subTotal"));
    component.set("v.fee", event.getParam("fee")); 
    component.set("v.expFees", event.getParam("expFees")); 
    component.set("v.helpAmount", event.getParam("helpAmount"));
    component.set("v.grandTotal", event.getParam("grandTotal"));
    component.set("v.grandmonthlyTotal", event.getParam("grandmonthlyTotal"));     
    component.set("v.total", event.getParam("total"));
    component.set("v.monthlyTotal", event.getParam("monthlyTotal"));
    component.set("v.monthlySubTotal", event.getParam("monthlySubTotal"));
    component.set("v.monthlyFee", event.getParam("monthlyFee"));
    component.set("v.payment", event.getParam("payment"));
    component.set("v.donorContact", event.getParam("donorContact"));
    component.set("v.donorAccount", event.getParam("donorAccount"));
    component.set("v.donorType", event.getParam("donorType"));
    component.set("v.endDate", event.getParam("endDate"));

    this.saveToSession(component);      
  },

  saveToSession: function(component) {
    if (!sessionStorage) {
      return;
    }

    this.loadItemToSession(component, "cartItems");
    this.loadItemToSession(component, "coverFee");
    this.loadItemToSession(component, "subTotal");
    this.loadItemToSession(component, "fee");
    this.loadItemToSession(component, "expFees");  
    this.loadItemToSession(component, "helpAmount");
    this.loadItemToSession(component, "grandTotal");
    this.loadItemToSession(component, "grandmonthlyTotal");  
    this.loadItemToSession(component, "total");
    this.loadItemToSession(component, "monthlyTotal");
    this.loadItemToSession(component, "monthlySubTotal");
    this.loadItemToSession(component, "monthlyFee");
    this.loadItemToSession(component, "payment");
    this.loadItemToSession(component, "donorContact");
    this.loadItemToSession(component, "donorAccount");
    this.loadItemToSession(component, "latestClassroomId");
    this.loadItemToSession(component, "donorType");
    this.loadItemToSession(component, "endDate");
  },

  loadFromSession: function(component) {
    if (!sessionStorage) {
      return;
    }

    this.getItemFromSession(component, "cartItems");
    this.getItemFromSession(component, "coverFee");
    this.getItemFromSession(component, "subTotal");
    this.getItemFromSession(component, "fee");
    this.getItemFromSession(component, "expFees");
    this.getItemFromSession(component, "helpAmount");
    this.getItemFromSession(component, "grandTotal");
    this.getItemFromSession(component, "grandmonthlyTotal");
    this.getItemFromSession(component, "total");
    this.getItemFromSession(component, "monthlyTotal");
    this.getItemFromSession(component, "monthlySubTotal");
    this.getItemFromSession(component, "monthlyFee");
    this.getItemFromSession(component, "payment");
    this.getItemFromSession(component, "donorContact");
    this.getItemFromSession(component, "donorAccount");
    this.getItemFromSession(component, "latestClassroomId");
    this.getItemFromSession(component, "donorType");
    this.getItemFromSession(component, "endDate");
  },

  clearSession: function(component) {
    if (!sessionStorage) {
      return;
    }

    sessionStorage.clear();
  },

  loadItemToSession: function(component, itemName) {
    sessionStorage.setItem(itemName, JSON.stringify(component.get(
      "v." + itemName)));
  },

  getItemFromSession: function(component, itemName) {
    var item = sessionStorage.getItem(itemName);

    if (!item || item === "undefined") {
      return;
    }

    component.set("v." + itemName, JSON.parse(item));
  }
})