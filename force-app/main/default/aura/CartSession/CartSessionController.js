({
  doInit: function(component, event, helper) {
    helper.loadFromSession(component);

    var ci = component.get("v.cartItems");
    if (!ci) {
      ci = [];
      component.set("v.cartItems", ci);
    }

    if (ci.length == 0) {
      helper.initGeneralFundItem(component);
    }
  },

	handleCartSessionRequest: function(component, event, helper) {
    helper.fireSessionResponse(component);
	},

  handleAddToCart: function(component, event, helper) {
      console.log("In handler add to cart");
    helper.addCartItem(component, event);
  },

  handleCartUpdate: function(component, event, helper) {
    console.log("In handler update");
    helper.updateCart(component, event);
  },

  handleSectionNavigation: function(component, event, helper) {
      console.log("In handler navigatio");
    component.set("v.displaySection", event.getParam("sectionName"));
  },

  handleSectionComplete: function(component, event, helper) {
      console.log("In handler section complete");
    component.set("v.currentSection", event.getParam("nextSectionName"));
  },

  handleSessionClear: function(component, event, helper) {
      console.log("In handler section clear");
    helper.clearSession(component);
  }
})