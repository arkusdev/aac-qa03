({
  // Load Contact
  onInit: function(component) {
    var action = component.get("c.getContactDataForUser");
    action.setParams({
      userId: component.get("v.recordId"),
    });
    action.setCallback(this, function(a) {
      var contact = a.getReturnValue();
      if (contact && contact.Id) {
        component.set('v.contact', contact);
      }
    });
    $A.enqueueAction(action);
  }

})