({
    handlePreviousClick : function(component, event, helper) {
        event.preventDefault();
        var step = Number(component.get('v.step'));
        if(step > 0) {
            step--;
            component.set('v.step', step.toString());
        }
    },
    handleNextClick : function(component, event, helper) {
        event.preventDefault();
        var step = Number(component.get('v.step'));

        if(step == 1) {
            component.set('v.enableNext', false);
        }

        if(step < 3) {
            step++;
            component.set('v.step', step.toString());
        }
    },
    handlerTeacher_SchoolSelectedEvt : function(component, event, helper) {
        component.set('v.enableNext', true);
    },
    handlerTeacher_DisableNextEvt : function(component, event, helper) {
        component.set('v.enableNext', false);
    }
})