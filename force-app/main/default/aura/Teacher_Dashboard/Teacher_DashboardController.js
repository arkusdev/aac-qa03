({    
    doInit : function(component, event, helper) {
		helper.onInit(component, event, helper);
    },

    share : function(component, event, helper){
		let sharelink = component.get('v.data.Public_Classroom_Landing_Page_URL__c');
		let strUrl = sharelink.substring( sharelink.lastIndexOf('href="') + 6,  sharelink.lastIndexOf('" '));
		var modalBody;
		$A.createComponent("c:SocialSharingModal", {objectId: component.get('v.data.Id'), objectType : (component.get('v.data.RecordType.Name') == 'Classroom Registration' ? 'classroom':'school'), shareUrl : strUrl},
		function(content, status) {
			if (status === "SUCCESS") {
				modalBody = content;
				component.find('overlayLib').showCustomModal({
					body: modalBody,
					showCloseButton: false,
					cssClass: "mymodal slds-modal_small",
				})
			}
		});
	},

    handleClickTransactionHistory : function(component, event, helper) {
        var modalBody;
        $A.createComponent("c:Teacher_Transaction_History", { 'teacherId': component.get('v.data.Teacher__r.Id')},
		function(content, status) {
			if (status === "SUCCESS") {
				modalBody = content;
				component.find('overlayLib').showCustomModal({
					body: modalBody,
					showCloseButton: false,
					cssClass: "mymodal slds-modal_small",
				})
			}
		});
    }
})