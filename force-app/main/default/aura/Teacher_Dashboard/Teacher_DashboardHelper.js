({
    onInit : function(component, event, helper) {
		var action= component.get('c.getClassroomData');
        action.setCallback(this, function(response){
            var state= response.getState();
            if(state === 'SUCCESS'){
                let responseValue= response.getReturnValue();
                component.set('v.data', responseValue);
            }
            else{
                var errors= response.getError();
                console.log(JSON.stringify(errors));
            }
        });
        $A.enqueueAction(action);
    }
})