({
    doInit : function(component, event, helper) {
        var action = component.get("c.getData");
        action.setCallback(this, function(result) {

            if(result.getState() === 'SUCCESS'){
                let data = result.getReturnValue();
                component.set('v.data', data);
            } else {
                console.log(result.getError());
            }
        });
        $A.enqueueAction(action);
    }
})