({
	getShopUrl: function(component) {
	  var action = component.get("c.generateShopUrl");
	  action.setParams({
		userId: component.get("v.userId")
	  });
	  
	  action.setCallback(this, function(response) {
		var state = response.getState();
		component.set("v.processing", false);
  
		if (state === "SUCCESS" && response.getReturnValue() != '') {
		  window.location.href = response.getReturnValue();
		} else{
			var errors = action.getError();
                    
			if (errors[0] && errors[0].message) {                         
				console.log(errors[0].message);
				var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
					"title": "Error",
					"type": "error",
					"mode": "sticky",
					"message": 'Error: ' + errors[0].message + '. Please contact info@adoptaclassroom.org for support.'
				});
				toastEvent.fire();
			}
		}
	  });
  
	  $A.enqueueAction(action);
	},
	processingChange: function(component) {

	  var processing = component.get("v.processing");
  
	  if (processing) {
		component.set("v.buttonLabel", component.get("v.loadingText"));
	  } else {
		component.set("v.buttonLabel", component.get("v.buttonText"));
	  }
	}
  })