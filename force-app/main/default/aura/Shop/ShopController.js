({
	goToShop: function(component, event, helper) {
	  var processing = component.get("v.processing");
  
	  if (processing) {
		return;
	  }
	  component.set("v.processing", true);
	  helper.getShopUrl(component);
	},
	doInit: function(component, event, helper) {
		
		let action = component.get('c.hasActiveClassroom');
		action.setParams({
			userId: component.get("v.userId")
		});
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                component.set('v.hasClassroom', response.getReturnValue());
            } else {
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);

	  	helper.processingChange(component);
	},
	processingChange: function(component, event, helper) {
	  helper.processingChange(component);
	}
  })