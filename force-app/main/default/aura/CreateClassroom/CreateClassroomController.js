({
  doInit: function(component, event, helper) {
    helper.initializeObjects(component);
  },
  showSchoolModal: function(component, componentId, helper) {
    helper.showPopupHelper(component, 'modaldialog', 'slds-fade-in-');
    helper.showPopupHelper(component, 'backdrop', 'slds-backdrop--');
  },
  hideSchoolModal: function(component, event, helper) {
    helper.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
    helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop--');
  },
  hideMessageModal: function(component, event, helper) {
    helper.hidePopupHelper(component, 'schoolCreatedMessage', 'slds-fade-in-');
    helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop--');
  },
  handleFormUpdate: function(component, event, helper) {
    helper.checkValidation(component, event.getParam("requiredFields"),
      event.getParam("fieldSetName"));
  },
  save: function(component, event, helper) {
    helper.saveClassroom(component);
  },
  doneRendering: function(component, event, helper) {
    component.set("v.showUserProfileLoading", false);
  },
  handleSchoolCreated: function(component, event, helper) {
    var c = component.get("v.currentClassroom");
    var newSchool = event.getParam("schoolObject");

    if(newSchool){
      c.School__c = newSchool.Id;
      var de = $A.get("e.c:FieldSetDataChanged");
      de.setParams({
        fieldName: "School__c",
        selectedSObject: newSchool
      });
      de.fire();
    }

    // Hide form and show message
    helper.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
    helper.showPopupHelper(component, 'schoolCreatedMessage', 'slds-fade-in-');
  }
})