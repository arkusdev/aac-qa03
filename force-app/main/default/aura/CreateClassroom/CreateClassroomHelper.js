({
  initializeObjects: function(component) {

    var userId = component.get("v.userId");

    var classroom = {};
    classroom.sobjectType = 'donation_split__Designation__c';

    var getContact = component.get("c.getContactFromUser");
    getContact.setParams({
      classRoom: classroom,
      userId: userId
    });

    classroom.Fundraising_Goal__c = 740;

    getContact.setCallback(this, function(a) {
      var state = a.getState();
      var errorMessages = [];

      if (state === "SUCCESS") {
        var classroomUpdate = a.getReturnValue();
        classroom.Name = classroomUpdate.Name
        classroom.Teacher__c = classroomUpdate.Teacher__c
      }
      component.set("v.currentClassroom", classroom);
    });

    $A.enqueueAction(getContact);
  },
  showPopupHelper: function(component, componentId, className) {
    var modal = component.find(componentId);
    $A.util.removeClass(modal, className + 'hide');
    $A.util.addClass(modal, className + 'open');
  },
  hidePopupHelper: function(component, componentId, className) {
    var modal = component.find(componentId);
    $A.util.addClass(modal, className + 'hide');
    $A.util.removeClass(modal, className + 'open');
    component.set("v.body", "");
  },
  clearForm: function(component, fieldSetName) {
    var clearEvent = $A.get("e.c:ClearFieldSetForm");
    clearEvent.setParams({
      fieldSetName: fieldSetName
    });
    clearEvent.fire();
  },
  checkValidation: function(component, requiredFields, fieldSetName) {
    var rfMap = component.get("v.requiredFields");
    var c = component.get("v.currentClassroom");
    var valid = true;

    if (!rfMap) {
      rfMap = {};
    }

    rfMap[fieldSetName] = requiredFields;

    valid = this.checkFieldValidation(component,
      rfMap[component.get("v.fieldSetName")], c);

    component.set("v.valid", valid);
    component.set("v.requiredFields", rfMap);
  },
  checkFieldValidation: function(component, rf, c) {
    var valid = true;

    if (!rf) {
      return false;
    }

    for (var i = 0; i < rf.length; i++) {
      var f = rf[i];
      if (!c[f]) {
        valid = false;
      }
    }

    return valid;
  },
  saveClassroom: function(component) {
    var c = component.get("v.currentClassroom");
    var action = component.get("c.updateClassroom");

    component.set("v.saveSuccessful", false);
    component.set("v.saveError", false);
    component.set("v.showUserProfileLoading", true);

    action.setParams({
      c: c
    });

    action.setCallback(this, function(a) {
      var state = a.getState();
      var errorMessages = [];

      if (state === "SUCCESS") {
        var classroomUpdate = a.getReturnValue();
        component.set("v.saveSuccessful", true);
        component.set("v.showUserProfileLoading", false);

        setTimeout(function() {
          component.set("v.saveSuccessful", false);
        }, 5000);

        this.redirectToClassroom(classroomUpdate.Id);

      } else if (state === "ERROR") {
        component.set("v.saveError", true);
        var errors = a.getError();
        component.set("v.showUserProfileLoading", false);

        for (i in errors) {
          var currentError = errors[i];

          for (currentField in currentError.fieldErrors) {
            for (cfe in currentError.fieldErrors[currentField]) {
              errorMessages.push(
                currentError.fieldErrors[currentField][cfe].message);
            }
          }
        }

        component.set("v.errorMessages", errorMessages);
      }
    });

    $A.enqueueAction(action);
  },
  redirectToClassroom: function(classId) {
    if (classId) {
      var navEvt = $A.get("e.force:navigateToSObject");
      navEvt.setParams({
        "recordId": classId
      });
      navEvt.fire();
    }
  }
})