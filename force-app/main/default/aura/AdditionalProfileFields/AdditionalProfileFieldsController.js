({
    doInit: function(component, event, helper) {
    	//console.debug('----- doInit');
        component.set("v.auraProcessing", true);
        //helper.getFields(component, event);
        var type = component.get("v.type");
        console.debug('---- type');
        console.debug(type);
        var fsName = component.get("v.fsName");
        console.debug('---- fsName');
        console.debug(fsName);

        var action = component.get("c.getCurrentContact"); // method in the apex class
        action.setCallback(this, function(a) {
            console.debug('---- currentContact');
            console.debug(a.getReturnValue());
            component.set("v.auraProcessing", false);
            component.set("v.currentContact", a.getReturnValue()); // variable in the component
    
        });
        $A.enqueueAction(action);


    },

    doGetTypeNames: function(component, event, helper) {
        console.debug('----- getTypeNames');
        helper.getTypeNames(component, event);
    },

    doSelectType: function(component, event, helper) {
        console.debug('----- doSelectType');
        var type = event.target.getAttribute("name");
        helper.selectType(component, type);
    },

    doSelectFieldSet: function(component, event, helper) {
    	console.debug('----- doSelectFieldSet');
        var fsName = event.target.getAttribute("name");
        helper.selectFieldSet(component, fsName);
    },

    doUpdateAdditionalFields: function(component, event, helper) {
    	console.debug('----- doUpdateAdditionalFields');
        component.set("v.auraProcessing", true);
        

        var requiredError = false;
        var requiredTds = document.getElementsByClassName("required");
        for (var i = 0; i < requiredTds.length; i++) {
            var required = requiredTds[i];
            var children = required.childNodes;
            for(var i2 = 0; i2 < children.length; i2++){
                var child = children[i2];

                if((child.tagName == "INPUT" || child.tagName == "SELECT") && !child.value){
                    child.style.backgroundColor = '#FBD4D4';
                    requiredError = true;
                }
            }
        }
        if(requiredError){
            component.set("v.errorMessage", "Please fill out all required information");
            component.set("v.saveError", "true");
            component.set("v.auraProcessing", false);
            return false;
        } else {
            helper.updateAdditionalFields(component, event);
        }
    }
})