({

    /*
     *  Map the Schema.FieldSetMember to the desired component config, including specific attribute values
     *  Source: https://www.salesforce.com/us/developer/docs/apexcode/index_Left.htm#CSHID=apex_class_Schema_FieldSetMember.htm|StartTopic=Content%2Fapex_class_Schema_FieldSetMember.htm|SkinName=webhelp
     *
     *  Change the componentDef and attributes as needed for other components
     */
    configMap: {
        "anytype": { inputType: "ui:inputText", componentDef: "markup://ui:inputText" },
        "base64": { inputType: "ui:inputText", componentDef: "markup://ui:inputText" },
        "boolean": { inputType: "ui:inputCheckbox", componentDef: "markup://ui:inputCheckbox" },
        "combobox": { inputType: "ui:inputText", componentDef: "markup://ui:inputText" },
        "currency": { inputType: "ui:inputText", componentDef: "markup://ui:inputText" },
        "datacategorygroupreference": { inputType: "ui:inputText", componentDef: "markup://ui:inputText" },
        "date": { inputType: "ui:inputDate", componentDef: "markup://ui:inputDate" },
        "datetime": { inputType: "ui:inputDateTime", componentDef: "markup://ui:inputDateTime" },
        "double": { inputType: "ui:inputNumber", componentDef: "markup://ui:inputNumber", attributes: { values: { format: "0.00"} } },
        "email": { inputType: "ui:inputEmail", componentDef: "markup://ui:inputEmail" },
        "encryptedstring": { inputType: "ui:inputText", componentDef: "markup://ui:inputText" },
        "id": { inputType: "ui:inputText", componentDef: "markup://ui:inputText" },
        "integer": { inputType: "ui:inputNumber", componentDef: "markup://ui:inputNumber", attributes: { values: { format: "0"} } },
        "multipicklist": { inputType: "ui:inputText", componentDef: "markup://ui:inputText" },
        "percent": { inputType: "ui:inputNumber", componentDef: "markup://ui:inputNumber", attributes: { values: { format: "0"} } },
        "phone": { inputType: "ui:inputPhone", componentDef: "markup://ui:inputPhone" },
        "picklist": { inputType: "ui:inputText", componentDef: "markup://ui:inputText" },
        "reference": { inputType: "ui:inputText", componentDef: "markup://ui:inputText" },
        "string": { inputType: "ui:inputText", componentDef: "markup://ui:inputText" },
        "textarea": { inputType: "ui:inputText", componentDef: "markup://ui:inputText" },
        "time": { inputType: "ui:inputDateTime", componentDef: "markup://ui:inputDateTime", attributes: { values: { format: "h:mm a"} } },
        "url": { inputType: "ui:inputText", componentDef: "markup://ui:inputText" }
    },

    // Adds the component via newComponentAsync and sets the value handler
    addComponent: function(component, facet, config, field) {
    	//console.debug('----- addComponent:');
    	console.debug('----- config');
    	console.debug(config);
    	console.debug(config.inputType);
    	console.debug('----- passed field:');
    	console.debug(field);
    	/*createComponentController.js*/

        $A.createComponent(
            config.inputType,
            {
                "aura:id": field.fieldPath,	
                "label": field.label,
                "value": field.fieldValue,
                "required": field.required	
                //"press": cmp.getReference("c.handlePress")
            },
            function(newField){
            	console.debug('----- field callback');
            	console.debug(newField);
                //Add the new field to the body array
                if (component.isValid()) {
                	console.debug('----- component is valid');
                    var body = component.get("v.form");
                    console.debug('----- body');
                    console.debug(body);
                    body.push(newField);
                    component.set("v.form", body);
                }
            }
        );
    },

    // Create a form given the set of fields
    createForm: function(component, fields) {
    	console.debug('----- createForm');
    	console.debug('---- fields: ');
    	console.debug(fields);
        var field = null;
        var cmp = null;
        var def = null;
        var config = null;
        var self = this;

        // Clear any existing components in the form facet
        component.set("v.form", []);
        console.debug('--- set form');
        var facet = component.get("v.form");
        console.debug('--- set facet');
        var values = [];
        console.debug('--- set values');
        console.debug('about to go through fields');
        console.debug(fields.length);
        for (var i = 0; i < fields.length; i++) {
            field = fields[i];
            console.debug('----- field and type:');
            console.debug(field);
            console.debug(field.type);
            // Copy the config, note that this type of copy may not work on all browsers!
            config = JSON.parse(JSON.stringify(this.configMap[field.type.toLowerCase()]));
            // Add attributes if needed
            config.attributes = config.attributes || {};
            // Add attributes.values if needed
            config.attributes.values = config.attributes.values || {};

            // Set the required and label attributes
            config.attributes.values.required = field.required;
            config.attributes.values.label = field.label;

            // Add the value for each field as a name/value            
            values.push({name: field.fieldPath, value: undefined, id: field.fieldPath});

            // Add the component to the facet and configure it
            self.addComponent(component, facet, config, field);
        }
        component.set("v.values", values);
        console.debug('--- values');
        console.debug(values);
		console.debug('----- createForm end');
        component.set("v.auraProcessing", false);
    },

    getTypeNames: function(component, event) {
        var action = component.get("c.getTypeNames");
        action.setParams({})
        action.setCallback(this, function(a) {
            var types = a.getReturnValue();
            component.set("v.types", types);
        });
        $A.enqueueAction(action);        
    },

    selectType: function(component, type) {
    	console.debug('----- selectType');
        component.set("v.type", type);
        // this.getFieldSetNames(component, type);
    },

    getFieldSetNames: function(component, typeName) {
    	console.debug('----- getFieldSetNames');
        var action = component.get("c.getFieldSetNames");
        action.setParams({typeName: typeName});
        action.setCallback(this, function(a) {
            var fsNames = a.getReturnValue();
            component.set("v.fsNames", fsNames);
        });
        $A.enqueueAction(action);        
    },

    selectFieldSet: function(component, fsName) {
        console.debug('----- selectFieldSet');
        component.set("v.fsName", fsName);
        this.getFields(component);
    },

    getFields: function(component) {
    	console.debug('----- getfields');
        var action = component.get("c.getFields");
        var self = this;
        var typeName = component.get("v.type");
        var fsName = component.get("v.fsName");
        action.setParams({typeName: typeName, fsName: fsName});
        action.setCallback(this, function(a) {
            var fields = a.getReturnValue();
            component.set("v.fields", fields);
            console.debug('----- fields');
            console.debug(fields);
            self.createForm(component, fields);
        });
        $A.enqueueAction(action);        
    },

    updateAdditionalFields: function(component, event) {
    	console.debug('----- updateAdditionalFields');
        var form = component.get("v.form");
        console.debug('----- form for saving');
        console.debug(form);
        console.debug(form[0]);
        var formFieldMap = {};
  
        var action = component.get("c.updateFields");
 
        action.setParams({
          "currentContact" : component.get("v.currentContact")
        });
        action.setCallback(this, function(a) {
            console.debug('----- updated fields');
            component.set("v.auraProcessing", false);
        });
        $A.enqueueAction(action);  
    }
})