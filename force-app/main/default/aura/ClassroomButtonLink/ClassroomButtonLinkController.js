({
  onInit: function(component) {
    var action = component.get("c.getClassroom");
    action.setParams({
      userId: component.get("v.userId")
    });
    action.setCallback(this, function(a) {
      var classroom = a.getReturnValue();
      if (classroom && classroom.Id) {
        component.set('v.classroom', classroom);
      }
    });
    $A.enqueueAction(action);
  },
  goToClassroom: function(component, event, helper) {
    var navEvt = $A.get("e.force:navigateToSObject");
    navEvt.setParams({
      "recordId": component.get("v.classroom.Id")
    });
    navEvt.fire();
  },
  createClassroom: function(component, event, helper) {
    var createClassURL = component.get("v.createClassroomURL");
    var navEvt = $A.get("e.force:navigateToURL");
    navEvt.setParams({
      "url": createClassURL
    });
    navEvt.fire();
  }
})