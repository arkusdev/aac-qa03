({
    doInit: function(component, event, helper) {
        var action = component.get("c.getTributeType");
        var inputTribute = component.find("InputTributeType");
        var opts=[];
        action.setCallback(this, function(a) {
            opts.push({
                class: "optionClass",
                label: "Choose One...",
                value: ""
            });
            for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputTribute.set("v.options", opts);
             
        });
        $A.enqueueAction(action); 
    },
    onPicklistChange: function(component, event, helper) {
        //get the value of select option
        var selectedTributeType = component.find("InputTributeType");
        //alert(selectedTributeType.get("v.value"));
        var a=selectedTributeType.get("v.value");
        component.set("v.mypicklist",a);
       // alert(a);
         var vx = cmp.get("v.method");
        $A.enqueueAction(vx);
    },    
})