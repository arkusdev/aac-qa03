({
    doInit : function(component, event, helper) {
        
        let date_ob = new Date();

        // adjust 0 before single digit date
        let date = ("0" + date_ob.getDate()).slice(-2);

        // current month
        let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

        // current year
        let year = date_ob.getFullYear();

        //get cart
        let cart = component.get('v.cart');

        let name = (cart.cartItems[0].classroom.recordTypeName = 'Classroom Registration') ? cart.donor.firstName + ' ' + cart.donor.lastName : cart.cartItems[0].classroom.schoolName;
        let category = (cart.cartItems[0].classroom.recordTypeName = 'Classroom Registration') ? 'Teacher' : ((cart.cartItems[0].classroom.recordTypeName = 'School Registration')?'School':null);
        let transaction = year + "-" + month + "-" + date + cart.donor.lastName + cart.cartItems[0].classroom.classroomId;
        //let urlGTM = '/donors/pushGTM?transactionId=%27'+transaction+'%27&amp;transactionTotal='+cart.total+'&amp;sku=%27'+cart.cartItems[0].classroom+'%27&amp;name=%27'+name+'%27&amp;category=%27'+category+'%27&amp;price='+price;
        
        //component.set('v.urlGTMCode', urlGTM);
        //component.set('v.fireGTMCode', true);

        // GTM DataLayer Push
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            'transactionId' : transaction,
            'transactionTotal' : cart.total,
            'transactionProducts': [{
            'sku' : cart.cartItems[0].classroom,
            'name': name,
            'category': category,
            'price' : cart.total,
            'quantity' : 1
            }]
        });

        // Google Tag Manager - Real GTM-T57V5B | Test GTM-5H2FXD
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-T57V5B');
    }
})