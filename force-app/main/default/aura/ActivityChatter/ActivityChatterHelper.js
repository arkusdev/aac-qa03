({
    doInit : function(component, event, helper) {
        var url = new URL(window.location.href);
        var recordId = url.searchParams.get("id");
        var editMode = url.searchParams.get("action");
        component.set('v.showSpinner', true);
        component.set('v.chatterId', recordId);

        var action = component.get("c.getRecord");
        action.setParams({
            "recordId" : recordId
          });
        action.setCallback(this, function(result) {
            let record = result.getReturnValue();
            record.Comments.forEach((comment) => {
                if (comment.CreatedBy.Name == 'AAC Donor Community Site Guest User') {
                    comment.CreatedBy.Name = 'Guest User';
                }
            });
            component.set("v.showPost", record.Teacher.Id == $A.get( "$SObjectType.CurrentUser.Id" ));
            component.set("v.activityData", record);
            component.set("v.editMode", editMode == 'edit');
            component.set('v.showSpinner',false);
        });
        $A.enqueueAction(action);
    }
})