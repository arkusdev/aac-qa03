({
    doInit : function(component, event, helper) {
        helper.doInit(component, event, helper);
    },
    handleUpdate : function(component, event, helper) {
        component.set('v.showSpinner',true);
        let record = component.get('v.activityData.Announcement');
        
        let action = component.get("c.saveRecord");
            action.setParams({
                'record' : JSON.stringify(record)
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state == "SUCCESS") {
                //success
            } else if (state == "ERROR") {
                var errors = action.getError();
                if (errors[0] && errors[0].message) {                         
                    console.log(errors[0].message);             
                }
            }
            component.set('v.showSpinner',false);
        });
        
        $A.enqueueAction(action);
    },
    handleUploadFinished : function(component, event, helper) {
        // Get the list of uploaded files
        var uploadedFiles = event.getParam("files");
        let fileList = [];
        uploadedFiles.forEach(function(file) {
            fileList.push({
                Name : file.name
              });
        });
        component.set('v.filesUploadedRecently', fileList);
    },
    handleClickPost : function(component, event, helper) {
        var url = new URL(window.location.href);
        if(component.find('comment').get('v.validity').valid) {
            var recordId = url.searchParams.get("id");
            var action = component.get("c.setComment");
            action.setParams({
                "recordId" : recordId,
                "comment" : component.get('v.comment')
            });
            action.setCallback(this, function(result) {
                helper.doInit(component, event, helper);
            });
            $A.enqueueAction(action);
        }
        
    },
    onChangeStatus : function(component, event, helper) {
        helper.doInit(component, event, helper);
    },
    toggleComment : function(component, event, helper) {
        var action = component.get("c.toggleAComment");
        action.setParams({
            "recordId" : event.getSource().get("v.name")
        });
        action.setCallback(this, function(result) {
            if (component.isValid() && state == "SUCCESS") {
                //success
                
            } else if (state == "ERROR") {
                var errors = action.getError();
                if (errors[0] && errors[0].message) {                         
                    console.log(errors[0].message);             
                }
            }
        });
        $A.enqueueAction(action);
    }
})