({
    init : function(component, event, helper){
        var endDate = new Date();
        endDate.setDate(endDate.getDate() + 31);
        var validationEndDate = $A.localizationService.formatDate(endDate, "YYYY-MM-DD");
        
        component.set('v.endDate', validationEndDate);
        helper.calculateCostTotal(component);
    },

    oneTimeChange : function(component, event, helper){
        component.set('v.cart.oneTimeDonation', true); 
        component.set('v.cart.monthlyDonation', false);
    },

    monthlyChange : function(component, event, helper){
        component.set('v.cart.oneTimeDonation', false); 
        component.set('v.cart.monthlyDonation', true);
    },

    recalculateCostAndTotal : function(component, event, helper){
        if(!event.getSource().get("v.value")){
            event.getSource().set("v.value", 0.00);
        } else {
            event.getSource().set("v.value", event.getSource().get("v.value").replace(/^0+/, ''));
        }
        helper.calculateCostTotal(component);
    },

    recalculateTotal : function(component, event, helper){
        if(!event.getSource().get("v.value")){
            event.getSource().set("v.value", 0.00);
        }
        let finalAmount = 0.00;
        let cart = component.get('v.cart');
        cart.cartItems.forEach(element => {
            finalAmount += parseFloat(element.amount);
        });
        finalAmount += parseFloat(cart.fee);
        finalAmount += parseFloat(cart.helpAmount);
        component.set('v.cart.total', finalAmount);
    },

    validateStep : function(component, event, helper){
        let allValid = component.find('stepField').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            if(inputCmp.get('v.value') && typeof inputCmp.get('v.value') == 'string' && inputCmp.get('v.value').includes(" ")) {
                var value = '';
                value = inputCmp.get('v.value');
                inputCmp.set('v.value', value.trim());
            }
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        return allValid;
    },
})