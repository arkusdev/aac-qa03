({
    calculateCostTotal : function(component) {
        let totalAmount = 0.00;
        let cart = component.get('v.cart');
        cart.cartItems.forEach(element => {
            totalAmount += parseFloat(element.amount);
        });
        component.set('v.cart.fee', (totalAmount * 0.15).toFixed(2));
        component.set('v.cart.helpAmount', (totalAmount * 0.10).toFixed(2));
        component.set('v.cart.total', (totalAmount + (totalAmount * 0.15) + (totalAmount * 0.1)).toFixed(2));
    }
})