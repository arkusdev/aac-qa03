({
	handleCartSessionResponse: function(component, event, helper) {
       
    var cItems = event.getParam("cartItems");
    component.set("v.cartItems", cItems);
    component.set("v.coverFee", event.getParam("coverFee"));
    component.set("v.subTotal", event.getParam("subTotal"));
    component.set("v.fee", event.getParam("fee"));
    component.set("v.expFees", event.getParam("expFees"));   
    component.set("v.helpAmount", event.getParam("helpAmount"));
    component.set("v.grandTotal", event.getParam("grandTotal")); 
    component.set("v.grandmonthlyTotal", event.getParam("grandmonthlyTotal")); 
    component.set("v.monthlySubTotal", event.getParam("monthlySubTotal")); 
    component.set("v.monthlyTotal", event.getParam("monthlyTotal")); 
    component.set("v.monthlyFee", event.getParam("monthlyFee")); 
    component.set("v.total", event.getParam("total"));
    component.set("v.displaySection", event.getParam("displaySection"));
    component.set("v.endDate", event.getParam("endDate"));
   // helper.countTotals(component);
  //  var a =event.getParam("fee");
   // alert("orderMethodFee"+a);
	},

  doInit: function(component, event, helper) {
      // alert("order2"+event.getParam("fee"));
      //helper.countTotals(component);
    helper.getNextChargeDate(component);
    var e = $A.get("e.c:CartSessionRequest");
    e.fire();
  },

  handleSectionNavigation: function(component, event, helper) {
    var e = $A.get("e.c:CartSessionRequest");
    e.fire();
  }
})