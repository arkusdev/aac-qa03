({
  getNextChargeDate: function(component) {
    var action = component.get("c.getNextMonthDate");
    action.setCallback(this, function(a) {
      var state = a.getState();

      if (state === "SUCCESS") {
        component.set("v.nextChargeDate", a.getReturnValue());
      }
    });

    $A.enqueueAction(action);
  },

  countTotals: function(component) {
      alert("ordercounttotal");
    var subTotal = 0;
    var fee = 0;
    var expFees = 0; 
    var helpAmount = 0;
    var grandTotal = 0; 
    var grandmonthlyTotal = 0;    
    var total = 0;
    var monthlySubTotal = 0;
    var monthlyFee = 0;  
    var monthlyTotal = 0;
    var items = component.get("v.cartItems");
    var coverFee = component.get("v.coverFee");
    //console.log(items.length);

    if (!items || items.length == 0) {
      //console.log("Empty List");
      this.setTotals(component, subTotal, fee, total, monthlySubTotal, helpAmount, grandTotal, grandmonthlyTotal, expFees,
        monthlyTotal);
      return;
    }

    for (var i = 0; i < items.length; i++) {
      subTotal += items[i].amount;
        
      //if(items[i].expenseFees)
        //expFees += items[i].expenseFees;    

      if (items[i].frequency == "Monthly") {
        monthlySubTotal += items[i].amount;
      }
    }
	helpAmount = (subTotal * 0.10);
    //fee = (subTotal * 0.029) + 0.30;
    fee = (subTotal * 0.15);
    monthlyFee = (monthlySubTotal * 0.15);  
	subTotal += expFees;
      
    if (coverFee) {
      total = subTotal + fee;
      monthlyTotal = monthlySubTotal + monthlyFee;
      grandTotal =  subTotal + fee + helpAmount; 
      grandmonthlyTotal = monthlySubTotal + monthlyFee; 
    } else {
      total = subTotal;
      monthlyTotal = monthlySubTotal + monthlyFee;
      grandTotal =  subTotal + fee + helpAmount; 
      grandmonthlyTotal = monthlySubTotal + monthlyFee;  
    }

    this.setTotals(component, subTotal, fee, total, monthlySubTotal, helpAmount, grandTotal, grandmonthlyTotal, monthlyFee, expFees,
      monthlyTotal);
  },

  setTotals: function(component, subTotal, fee, total, monthlySubTotal, helpAmount, grandTotal, grandmonthlyTotal, monthlyFee, expFees,
      monthlyTotal) {
    component.set("v.subTotal", subTotal);
    component.set("v.fee", fee);
    component.set("v.expFees", expFees);
    component.set("v.helpAmount", helpAmount);
    component.set("v.grandTotal", grandTotal);
    component.set("v.grandmonthlyTotal", grandmonthlyTotal);    
    component.set("v.total", total);
    component.set("v.monthlySubTotal", monthlySubTotal);
    component.set("v.monthlyTotal", monthlyTotal);
    component.set("v.monthlyFee", monthlyFee);  
  }
})