({
    doInit : function(component, event, helper) {
        var action= component.get('c.getTransactions');
        action.setParams({ 'teacherId' : component.get('v.teacherId') });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(state === 'SUCCESS'){
                let responseValue= response.getReturnValue();
                if (responseValue.length > 0 ) {
                    component.set('v.transactionsExisting', true);
                    component.set('v.data', responseValue);
                }else{
                    component.set('v.noData', 'No transaction history available.');
                }
            }
            else{
                var errors= response.getError();
                console.log(JSON.stringify(errors));
            }
        });
        $A.enqueueAction(action);
    },
    handleCancel : function(component, event, helper) {
		component.find("overlayLib").notifyClose();
	}
})