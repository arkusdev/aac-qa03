({
    doInit: function (component, event, helper) {
        try {
            helper.getCurrentSchool(component, event, helper);
        } catch (e) {
            console.error("Unexpected Error ocurred. ", e);
        }
    },
    getResults: function (component, event, helper) {
        helper.getResults(component, event, helper);
    },
    nextPage: function (component, event, helper) {
        try {
            component.set('v.scrollEnd', false);
            var lastPage = component.get("v.lastPage") + 1;
            var tmpList = component.get("v.results");
            var end =
                lastPage * 25 <= tmpList.length ? lastPage * 25 : tmpList.length;
            var displaySeeMore = tmpList.length > end;
            component.set("v.enableSeeMore", displaySeeMore);
            component.set("v.lastPage", lastPage);
            component.set("v.end", end);
        } catch (e) {
            console.error("Unexpected Error ocurred. ", e);
        }
    },
    selectAccount: function (component, event, helper) {
        try {
            var idSelected = event.getSource().get("v.name");
            var currentValue = component.get("v.accountSelected");
            var classId = component.get("v.classroom");

            var tmpList = component.get("v.results");
            if (idSelected != undefined && idSelected != null) {
                component.set("v.accountSelected", idSelected);
                if (currentValue != idSelected) {
                    var pos = tmpList.findIndex(element => element.id == idSelected);

                    if (pos > -1) {
                        var tmpOption = {
                            id: tmpList[pos].id,
                            name: tmpList[pos].name,
                            addressOne: tmpList[pos].addressOne,
                            addressTwo: tmpList[pos].addressTwo
                        };
                        component.set("v.accountSelected", idSelected);
                        component.set("v.account", tmpOption);

                        var action = component.get("c.saveSchool");
                        action.setParams({
                            accId: idSelected,
                            classId: classId
                        });

                        action.setCallback(this, function (response) {
                            var state = response.getState();
                            if (state === "SUCCESS") {
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    title: "Success!",
                                    type: "success",
                                    message: "School has been added successfully."
                                });
                                toastEvent.fire();

                                var appEvent = $A.get("e.c:Teacher_SchoolSelectedEvt");
                                appEvent.fire();
                            } else if (state === "ERROR") {
                                var errors = response.getError();
                                if (errors) {
                                    if (errors[0] && errors[0].message) {
                                        console.log(
                                            "Error trying to get schools. Message: " +
                                            errors[0].message
                                        );
                                    }
                                } else {
                                    console.log("Unknown error");
                                }
                            }
                        });

                        $A.getCallback(function () {
                            $A.enqueueAction(action);
                        })();
                    }
                }
            } else {
                component.set("v.accountSelected", "");
            }
        } catch (e) {
            console.error("Unexpected Error ocurred. ", e);
        }
    },
    onChange: function (component, event, helper) {
        try {
            var state = event.getSource().get("v.value");
            component.set("v.schoolState", state);
        } catch (e) {
            console.error("Unexpected Error ocurred. ", e);
        }
    },
    handleClickNew: function (component, event, helper) {
        var classId = component.get('v.classroom');
        var recordType = component.get("v.recordType");
        try {
            var modalBody;
            $A.createComponent("c:Teacher_CreateSchool", {
                classId: classId,
                recordType: recordType
            }, function (
                content,
                status
            ) {
                if (status === "SUCCESS") {
                    modalBody = content;
                    component.find("overlayLib").showCustomModal({
                        body: modalBody,
                        showCloseButton: true,
                        cssClass: "mymodal slds-modal_medium",
                        closeCallback: function () {
                            component.set("v.loaded", false);
                            helper.getCurrentSchool(component, event, helper);
                            component.set("v.loaded", true);
                        }
                    });
                }
            });
        } catch (e) {
            console.error("Unexpected Error ocurred. ", e);
        }
    },
    scroll: function (component, event, helper) {
        
        if(event.target.scrollTop > (event.target.scrollHeight - event.target.clientHeight) * 0.9 ) {
            component.set('v.scrollEnd', true);
        } else {
            component.set('v.scrollEnd', false);
        }

    }
});