({
    getCurrentSchool: function(component, event, helper){
        var action = component.get("c.getClassroom");

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if (result != null) {
                    component.set("v.recordType", result.schoolRT);
                    component.set("v.classroom", result.id);
                    if (result.schoolId != null && result.schoolId != undefined) {
                        var tmpOption = {
                            id: result.schoolId,
                            name: result.schoolName,
                            addressOne: result.addressOne,
                            addressTwo: result.addressTwo
                        };
                        component.set("v.accountSelected", result.schoolId);
                        component.set("v.account", tmpOption);
                        var tmpList = [];
                        tmpList = [tmpOption, ...tmpList];
                        component.set("v.results", tmpList);
                        component.set("v.showResults", true);
                        component.set("v.hasResults", true);

                        component.set("v.loaded", true);
                        var appEvent = $A.get("e.c:Teacher_SchoolSelectedEvt");
                        appEvent.fire();
                    }
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.error(
                            "Error trying to get schools. Message: " + errors[0].message
                        );
                    }
                } else {
                    console.error("Unknown error");
                }
            }
        });

        $A.getCallback(function () {
            $A.enqueueAction(action);
        })();
    }, 
    getResults: function (component, event, helper) {
        var accountSelected = component.get("v.accountSelected");
        var accountObj = component.get("v.account");
        try {
            var name = component.get("v.schoolName");
            var state = component.get("v.schoolState");
            var city = component.get("v.schoolCity");
            var zip = component.get("v.schoolZip");

            if (name != "" || state != "" || city != "" || zip != "") {
                component.set("v.loaded", false);

                var a = component.get("c.getClassroom");

                component.set("v.showResults", true);
                var parameters = {
                    name: component.get("v.schoolName"),
                    state: component.get("v.schoolState"),
                    city: component.get("v.schoolCity"),
                    zip: component.get("v.schoolZip")
                };

                var action = component.get("c.searchSchool");

                action.setParams({
                    parametersToSearch: JSON.stringify(parameters)
                });

                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var result = response.getReturnValue();
                        var tmpList = [];
                        if (result != null) {
                            component.set("v.hasResults", true);
                            for (var counter in result) {
                                if (accountSelected != result[counter].schoolId) {
                                    var tmpOption = {
                                        id: result[counter].schoolId,
                                        name: result[counter].schoolName,
                                        addressOne: result[counter].addressOne,
                                        addressTwo: result[counter].addressTwo
                                    };
                                    tmpList = [...tmpList, tmpOption];
                                }
                            }
                        }
                        if (
                            accountObj &&
                            accountSelected &&
                            Object.keys(accountObj).length !== 0
                        ) {
                            tmpList = [accountObj, ...tmpList];
                        } else {
                            component.set("v.accountSelected", "");
                        }
                        var displaySeeMore = tmpList.length > 25;
                        component.set("v.results", tmpList);
                        component.set("v.lastPage", 1);
                        var end = 25 <= tmpList.length ? 25 : tmpList.length;
                        component.set("v.end", end);
                        component.set("v.enableSeeMore", displaySeeMore);
                        if (component.get("v.results").length == 0) {
                            component.set("v.hasResults", false);
                        }
                    } else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.error(
                                    "Error trying to get schools. Message: " + errors[0].message
                                );
                            }
                        } else {
                            console.error("Unknown error");
                        }
                    }
                    component.set("v.loaded", true);
                });

                $A.getCallback(function () {
                    $A.enqueueAction(action);
                })();
            } else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: "Error",
                    type: "error",
                    message: "Enter a search parameter."
                });
                toastEvent.fire();
            }
        } catch (e) {
            console.error("Unexpected Error ocurred. ", e);
        }
    }
})