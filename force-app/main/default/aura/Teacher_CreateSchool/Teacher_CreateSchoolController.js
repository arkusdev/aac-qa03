({
    handleSuccess : function (component, event, helper) {
        var updatedRecord = JSON.parse(JSON.stringify(event.getParams()));
        
        component.find("notifLib").showToast({
            variant: "success",
            title: "School Created",
            message: "Record ID: " + updatedRecord.response.id
        });
        try {
            var classId = component.get("v.classId");
            var action = component.get("c.saveSchool");
            action.setParams({
                accId: updatedRecord.response.id,
                classId: classId
            });

            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: "Success!",
                        type: "success",
                        message: "School has been added successfully."
                    });
                    toastEvent.fire();
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log(
                                "Error trying to create schools. Message: " + errors[0].message
                            );
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });

            $A.getCallback(function () {
                $A.enqueueAction(action);
            })();
        } catch (e) {
            console.error("Unexpected Error ocurred. ", e);
        }

        component.find("overlayLib").notifyClose();
    },
    handleCancel : function (component, event, helper) {
        try {
            component.find("overlayLib").notifyClose();
        } catch (e) {
            console.error("Unexpected Error ocurred. ", e);
        }
    },
    handleError : function (component, event, helper) {
        try {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Error!",
                type: "error",
                message: "Unexpected Error ocurred. Please verify all data and try again."
            });
            toastEvent.fire();
            component.find("overlayLib").notifyClose();
        } catch (e) {
            console.error("Unexpected Error ocurred. ", e);
        }
    },
    handleSubmit : function (component, event, helper) {
        event.preventDefault();       // stop the form from submitting
        const fields = event.getParam('fields');
        fields.RecordTypeId = component.get('v.recordType');
        fields.Account_Status__c = 'Pending Verification'; // modify a field
        component.find('accForm').submit(fields);
    }
});