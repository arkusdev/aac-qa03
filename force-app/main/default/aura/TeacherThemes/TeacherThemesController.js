({
  // Load classroom
  onInit: function(component) {
    var action = component.get("c.getClassroom");
    action.setParams({
      userId: component.get("v.userId"),
    });
    action.setCallback(this, function(a) {
      var classroom = a.getReturnValue();
      if (classroom && classroom.Id) {
        component.set('v.classroom', classroom);
      }

      var cssPath = component.get('v.cssPath');
      var color = classroom.CommunityColorTheme__c || "Red";
      cssPath += "/" + color + ".css";

      $A.createComponent(
       "ltng:require",
       {styles: cssPath},
       function(newItem, status, statusMessagesList){
           if (component.isValid()) {
               component.set("v.styleComponent", newItem);
           }
       });

    });
    $A.enqueueAction(action);
  }
})