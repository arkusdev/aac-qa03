({
    loadBannerURL : function(component, event, helper) {
        let action = component.get("c.getBanner");
        action.setParams({
            "classroomId" : component.get('v.recordId')
          });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state == "SUCCESS") {
                let result = response.getReturnValue();
                component.set('v.bannerImage', result);
            } else if (state == "ERROR") {
                var errors = action.getError();
                if (errors[0] && errors[0].message) {                         
                    console.log(errors[0].message);             
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    editPhoto : function(component, event, helper) {
    	    var modalBody;
            $A.createComponent("c:EditBannerModal", {pictureSrc : component.get('v.bannerImage')},
                               function(content, status) {
                                   if (status === "SUCCESS") {
                                       modalBody = content;
                                       component.find('overlayLib').showCustomModal({
                                           header: "Update Banner",
                                           body: modalBody,
                                           showCloseButton: true,
                                           cssClass: "mymodal slds-modal_large cEditBannerModal",
                                           closeCallback: function() {}
                                       })
                                   }
                               });
    }
})