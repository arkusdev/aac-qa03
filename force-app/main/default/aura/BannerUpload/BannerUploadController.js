({
    doInit : function(component, event, helper) {
        helper.loadBannerURL(component, event, helper);
    },
    handleEditPhoto : function(component, event, helper) {
        helper.editPhoto(component, event, helper);
    },
    refreshImage : function(component, event, helper) {
        helper.loadBannerURL(component, event, helper);
    }
})