({
    doInit : function(component, event, helper) {
        
        var action = component.get("c.fetchUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log(storeResponse);
                let notify = {  'FirstName' : '',
                        'FirstLast' : '',
                        'MarketingEmail' : true,
                        'Email' : '',
                        'Classroom' : component.get('v.Classroom'),
                        'Teacher' : component.get('v.Teacher')};
                
                if(!storeResponse.LastName.includes('Guest')) {
                    notify.FirstName = storeResponse.FirstName;
                    notify.LastName = storeResponse.LastName;
                    notify.Email = storeResponse.Email;
                }

                component.set('v.notify', notify);
            }
        });
        $A.enqueueAction(action);
        
    },
    handleCancel : function(component, event, helper) {
        component.find("overlayLib").notifyClose();
    },
    onNotify : function(component, event, helper) {
        
        let notifyStr = JSON.stringify(component.get('v.notify'));
        var toastEvent = $A.get("e.force:showToast");
        var action = component.get("c.saveNotify");
        action.setParams({
            notifyStr : notifyStr
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (component.isValid() && state == "SUCCESS") {   
				                
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "Your notification was sent."
                });
                toastEvent.fire();
                component.find("overlayLib").notifyClose();
            } else if (state == "ERROR") {
                var errors = action.getError();                    
                if (errors[0] && errors[0].message) {                         
                    console.log(errors[0].message);
                    toastEvent.setParams({
                        "title": "Error",
                        "type": "error",
                        "message": errors[0].message
                    });
                    toastEvent.fire();
                    
                }
            }
            
        });
        
        $A.enqueueAction(action);
    }
})