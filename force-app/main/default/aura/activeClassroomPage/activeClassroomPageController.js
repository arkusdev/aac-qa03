({
    doInit : function(component, event, helper) {

        let action = component.get('c.active');
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
            } else {
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
    }
})