({
	getClassroomData: function(component) {
    
    var action = component.get("c.getClassroom");
    action.setParams({
      classroomId: component.get("v.classroomId")
    });

    action.setCallback(this, function(a) {
      var classroom = a.getReturnValue();
      if (classroom && classroom.Id) {
        component.set('v.classroom', classroom);
      }
    });

    $A.enqueueAction(action);
	},

  addClassroomToCart: function(component) {
    console.log("add to cart");
    var e = $A.get("e.c:AddToCart");
    
    e.setParams({
      "classroomId": component.get("v.classroomId"),
      "amount": component.get("v.amount")
    });

    e.fire();
  },

  goToCartPage: function(component) {
    var cartUrl = component.get("v.checkoutPageUrl");
    window.location.href = "/donors/s" + cartUrl;
  }
})