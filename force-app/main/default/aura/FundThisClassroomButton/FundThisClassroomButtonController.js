({
  doInit: function(component, event, helper) {
    helper.getClassroomData(component);
  },
  goToFundThisClassroom: function(component, event, helper) {
    helper.addClassroomToCart(component);
    helper.goToCartPage(component);
  },
	share : function(component, event, helper){
		var modalBody;
        $A.createComponent("c:SocialSharingModal", {objectId: component.get('v.classroomId'), objectType : 'classroom'},
		function(content, status) {
			if (status === "SUCCESS") {
				modalBody = content;
				component.find('overlayLib').showCustomModal({
					body: modalBody,
					showCloseButton: false,
					cssClass: "mymodal slds-modal_small",
				})
			}
		});
	},
})