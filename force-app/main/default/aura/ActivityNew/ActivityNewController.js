({
    doInit : function(component, event, helper) {
        let record = component.get('v.record');
        record.Classroom__c = component.get('v.classroomId');
        record.CreatedById = component.get('v.teacherId');
        component.set('v.record',record);
        component.set('v.showSpinner',false);
    },
    handleCancel : function(component, event, helper) {
		component.find("overlayLib").notifyClose();
	},
	handleSave : function(component, event, helper) {
        component.set('v.showSpinner',true);
        let record = component.get('v.record');
        
        let action = component.get("c.saveRecord");
            action.setParams({
                'record' : JSON.stringify(record)
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state == "SUCCESS") {
                //success
                let recId = response.getReturnValue();
                window.open('/donors/s/activity-chatter?id=' + recId + '&action=edit' ,'_top');
            } else if (state == "ERROR") {
                var errors = action.getError();
                if (errors[0] && errors[0].message) {                         
                    console.log(errors[0].message);             
                }
            }
            component.set('v.showSpinner',false);
        });
        
        $A.enqueueAction(action);   
	}
})