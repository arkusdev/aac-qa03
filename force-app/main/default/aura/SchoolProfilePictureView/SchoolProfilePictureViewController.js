({
    // Load current profile picture
    onInit: function(component) {
        var action = component.get("c.getProfilePictureNew");
        action.setParams({
            recordId: component.get("v.recordId"),
            pictureType: component.get("v.pictureType")
        });
        action.setCallback(this, function(a) {
            var pictureType = component.get("v.pictureType");
            var attachment = a.getReturnValue();
            if (attachment != null) {
                component.set('v.pictureSrc',attachment);
            } else if(pictureType == 'profile') {
                 var hide = component.get("v.hideIfEmpty");
                if(hide == 'yes'){
                    component.set("v.showPicture", false);
                } else {
                    var noPicMessage = component.get('v.noPicMessage');
                    component.set('v.message', noPicMessage);
                }
            }
        });
        $A.enqueueAction(action);
    }

})