({
	init : function(component) {
        var action = component.get( "c.getUser" );
        action.setParams({
            userId: $A.get("$SObjectType.CurrentUser.Id") 
        });

        action.setCallback(this, function(response) {
            var state, user;
            state = response.getState();
            if (state === 'SUCCESS') {
                user = response.getReturnValue();
                
               	component.set("v.pictureSrc", user.MediumPhotoUrl);
                var cmpTarget = component.find('profile-image');
                $A.util.addClass(cmpTarget, 'round-image');
                
            } else {
                console.log(response.getError()[0]);
            }
        });
        
        $A.enqueueAction(action);
    },
    closeModal : function(component) {
        if (sessionStorage.getItem("is_reloadedPicture")){
            sessionStorage.removeItem("is_reloadedPicture");
        } else {
            sessionStorage.setItem("is_reloadedPicture", true);
        }
        component.find("overlayLib").notifyClose();
        //window.location.reload(false);
	},
	readFile: function(component, helper, file, upload) {
        if (!file) return;
        if (!file.type.match(/(image.*)/)) {
  			return alert('Image file not supported');
		}
        
        var currentPhotoTitle = component.find("current-photo-title").getElement();
        currentPhotoTitle.style.display = "none";
        
        var reader = new FileReader();
        reader.onloadend = function() {
            var dataURL = reader.result;
            component.set("v.pictureSrc", dataURL);
            var croppieArray = document.getElementsByClassName("cr-image");
            if (croppieArray === undefined || croppieArray.length == 0){
            	helper.enableCroppie(component,helper, file, dataURL);
            }else{
                var croppieOriginalArray = document.getElementsByClassName("cr-image");
                croppieArray[0].src=dataURL;
            }
        };
        reader.readAsDataURL(file);
	},
    upload: function(component, file, base64Data, helper) {
    	component.set('v.message', 'Uploading photo...');
    	var toastEvent = $A.get("e.force:showToast");
        var action = component.get("c.saveAttachment"); 
        action.setParams({
            base64Data: base64Data, 
            contentType: file.type
        });
	    action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
            	toastEvent.setParams({
			        "title": "Success!",
			        "type": "success",
			        "message": "Your profile picture was updated! You can see the changes after you finish creating your Classroom Page."
			    });
                toastEvent.fire();
                if (sessionStorage.getItem("is_reloadedPicture")){
                    sessionStorage.removeItem("is_reloadedPicture");
                } else {
                    sessionStorage.setItem("is_reloadedPicture", true);
                }
                component.find("overlayLib").notifyClose();
                var e = $A.get("e.c:imageUpdated");
                e.fire();
            } else if (state == "ERROR") {
            	var errors = action.getError();                
                if (errors[0] && errors[0].message) {                         
                    console.log(errors[0].message);
                    toastEvent.setParams({
				        "title": "Error",
				        "type": "error",
				        "message": "An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues."
				    });
				    toastEvent.fire();
                    component.set('v.message', 'Please select another photo');
                }
            }
        });        
        $A.enqueueAction(action);
    },
    enableCroppie : function(component,helper, file, dataURL){
        component.set('v.message','Drag and resize to adjust thumbnail');
        var el = component.find("profile-image").getElement();
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
         var resize = new Croppie(el, {
            enableExif: false,
            viewport: {
                width: 100,
                height: 100,
                type: 'circle'
            },
            boundary: {
                width: 200,
                height: 200
            }
        });
        }else{
            var resize = new Croppie(el, {
                enableExif: false,
                viewport: {
                    width: 200,
                    height: 200,
                    type: 'circle'
                },
                boundary: {
                    width: 300,
                    height: 300
                }
            });
        }
        //resize.bind({url: dataURL});
        
        var originalPhoto = document.getElementsByClassName("croppie-container")[0].firstChild;
        originalPhoto.style.display = "none";
        
        //upload cropped image to salesforce
        component.find("save-button").getElement().addEventListener("click", function(){
            resize.options.viewport.type = 'square';
        	resize.result('blob').then(function(blob){
                var reader = new FileReader();
                 reader.readAsDataURL(blob); 
                 reader.onloadend = function() {
                     var base64data = reader.result;
                     helper.upload(component, file, base64data.match(/,(.*)$/)[1], helper);
                 }
            });
      	});
    }
})