({
    doInit : function(component, event, helper) {
        let classroomId = window.location.href.split('/')[6];
        if(component.get('v.recordId')=="" && classroomId.includes("a1m")) {
            component.set('v.recordId',classroomId);
        }

        if(component.get('v.ownerId') == $A.get( "$SObjectType.CurrentUser.Id" )) {
            component.set('v.showNew', true);
        }
        helper.loadActivityData(component, event, helper);

        if((window.location.href).includes('Updates')) {
            component.set('v.showNew', true);
            self.location = "#Updates";
        }
    },
    openChatter : function(component, event, helper) {
        var activityId = event.currentTarget.dataset.activity;
        window.open('/donors/s/activity-chatter?id='+ activityId,'_top');
    },
    editChatter : function(component, event, helper) {
        var activityId = event.currentTarget.dataset.activity;
        window.open('/donors/s/activity-chatter?id='+ activityId+'&action=edit','_top');
    },
    handleClickNew : function(component, event, helper) {
        var modalBody;
        $A.createComponent("c:ActivityNew", { 'classroomId' : component.get('v.recordId'), 'teacherId' : component.get('v.ownerId')},
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   modalBody = content;
                                   component.find('overlayLib').showCustomModal({
                                       body: modalBody, 
                                       showCloseButton: false,
                                       cssClass: "mymodal slds-modal_medium",
                                       closeCallback: function() {
                                            helper.loadActivityData(component, event, helper);
                                       }
                                   })
                               }                               
                           });
    },
    handleClickHelp : function(component, event, helper) {
        window.open(component.get("v.helpURL"),'_top');
    },
    loadMore : function(component, event, helper) {
		component.set('v.numActivities', component.get('v.numActivities') + 5);
	},
	loadLess : function(component, event, helper) {
		component.set('v.numActivities', component.get('v.numActivities') - 5);
    },
    refreshActivities : function(component, event, helper) {
		helper.loadActivityData(component, event, helper);
    }
})