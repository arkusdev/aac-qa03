({
    loadActivityData : function(component, event, helper) {
        var action = component.get("c.getData");
        action.setParams({
            "classroomId" : component.get("v.recordId")
          });
        action.setCallback(this, function(result) {
            let data = result.getReturnValue();
            let activities = [];
            
            if(data) {
                data.forEach(function(element){
                    element.activity.friendlyDate = moment($A.localizationService.formatDate(element.activity.CreatedDate, "YYYY-MM-DDTHH:mm:ss.SSS"), "YYYY-MM-DDTHH:mm:ss.SSS").fromNow();
                    if (element.activity.CreatedBy.Name == 'AAC Donor Community Site Guest User') {
                        element.activity.CreatedBy.Name = 'Guest User';
                    }

                    activities.push(element);
                    
                    if(element.attachmentURLs) {
                        element.attachmentURLs.forEach(function(file) {
                            let isVideo = false;
                            if((file.Name).includes('.')) {
                                let extension = ((file.Name).split('.')[1]).toLowerCase();
                                if(extension == 'm4v' || extension == 'avi' || extension == 'mpg' || extension == 'mp4') {
                                    isVideo = true;
                                }
                            }
                            file['video'] = isVideo;
                        });
                    }
                    
                });
                activities.sort(function(x, y) {
                    return (x.activity.Pin_at_Top__c === y.activity.Pin_at_Top__c)? 0 : x.activity.Pin_at_Top__c? -1 : 1;
                });
            }
            
            component.set("v.helpURL", data[0].helpLink);
            component.set("v.activities", activities);
        });
        $A.enqueueAction(action);
    }
})