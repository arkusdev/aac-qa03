({
    doInit : function(component, event, helper) {
        
        var url = new URL(window.location.href);
        var orderId = url.searchParams.get("oid");
        if(orderId) {
            let action = component.get('c.getGTMData');
            action.setParams({
                recordId: orderId
            });
            action.setCallback(this, function(response){
                if(response.getState() === 'SUCCESS'){

                    // GTM DataLayer Push
                    window.dataLayer = window.dataLayer || [];
                    let gtmData = response.getReturnValue();
                    dataLayer.push({
                        'transactionId' : gtmData.transactionId,
                        'transactionTotal' : gtmData.transactionTotal,
                        'transactionProducts': [{
                        'sku' : gtmData.sku,
                        'name': gtmData.name,
                        'category': gtmData.category,
                        'price' : gtmData.price,
                        'quantity' : gtmData.quantity
                        }]
                    });

                    // Google Tag Manager - Real GTM-T57V5B | Test GTM-5H2FXD
                    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                    })(window,document,'script','dataLayer','GTM-T57V5B');
                }
            });
            $A.enqueueAction(action);
        }
    }
})