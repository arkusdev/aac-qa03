({
  getCurrentContact: function(component) {
    var userId = component.get("v.userId");
    var action = component.get("c.getContactForUser");
    var currentFieldSet = component.get("v.fieldSetName");
    var fieldSetNames = [];

    fieldSetNames.push(currentFieldSet);

    action.setParams({
      userId: userId,
      fieldSetNames: fieldSetNames
    });

    action.setCallback(this, function(a) {
      var state = a.getState();

      if (state === "SUCCESS") {
        component.set("v.currentContact", a.getReturnValue());
      }
    });

    $A.enqueueAction(action);
  },
  clearForm: function(component, fieldSetName) {
    var clearEvent = $A.get("e.c:ClearFieldSetForm");
    clearEvent.setParams({
      fieldSetName: fieldSetName
    });
    clearEvent.fire();
  },
  checkValidation: function(component, requiredFields, fieldSetName) {
    var rfMap = component.get("v.requiredFields");
    var c = component.get("v.currentContact");
    var valid = true;

    if (!rfMap) {
      rfMap = {};
    }

    rfMap[fieldSetName] = requiredFields;

    valid = this.checkFieldValidation(component,
          rfMap[component.get("v.fieldSetName")], c);


    component.set("v.valid", valid);
    component.set("v.requiredFields", rfMap);
  },
  checkFieldValidation: function(component, rf, c) {
    var valid = true;

    if(!rf) {
      return false;
    }

    for (var i = 0; i < rf.length; i++) {
      var f = rf[i];
      if (!c[f]) {
        valid = false;
      }
    }

    return valid;
  },
  saveContact: function(component) {
    var c = component.get("v.currentContact");
    var action = component.get("c.updateContact");

    component.set("v.saveSuccessful", false);
    component.set("v.saveError", false);
    component.set("v.showUserProfileLoading", true);

    action.setParams({
      c: c
    });

    action.setCallback(this, function(a) {
      var state = a.getState();
      var errorMessages = [];

      if (state === "SUCCESS") {
        component.set("v.saveSuccessful", true);
        component.set("v.showUserProfileLoading", false);

        setTimeout(function(){ component.set("v.saveSuccessful", false); }, 5000);

        var e = component.getEvent("profileSaved");
        e.fire();

        // Redirect to homepage after save
        this.redirectToHome(component);

      } else if (state === "ERROR") {
        component.set("v.saveError", true);
        var errors = a.getError();
        component.set("v.showUserProfileLoading", false);

        for (i in errors) {
          var currentError = errors[i];

          for (currentField in currentError.fieldErrors) {
            for (cfe in currentError.fieldErrors[currentField]) {
              errorMessages.push(
                currentError.fieldErrors[currentField][cfe].message);
            }
          }
        }

        component.set("v.errorMessages", errorMessages);
      }
    });

    $A.enqueueAction(action);
  },
  redirectToHome: function(component){
    var goTo = component.get("v.redirectURL");
    var urlEvent = $A.get("e.force:navigateToURL");

    urlEvent.setParams({
      "url": goTo
    });
    urlEvent.fire();
  }
})