({
	initializeObjects: function(component) {
    var contact = {};
    contact.sobjectType = 'Contact';

    component.set("v.currentContact", contact);
	},
  checkForDuplicateUsername: function(component) {
    var action = component.get("c.getIsUsernameTaken");
     console.log('Cntrl Inside checkForDuplicate');
    action.setParams({
      un: component.get("v.currentContact").Email
    });

    action.setCallback(this, function(a) {
      var state = a.getState();

      if (state === "SUCCESS") {
        var isTaken = a.getReturnValue();
          console.log(isTaken+'retuen value from controller');
        component.set("v.showDuplicateUserError", isTaken);

        if (!isTaken) {
          this.save(component);
        }
      }
    });

    $A.enqueueAction(action);
  },
  save: function(component) {
    var action = component.get("c.createUser");

    component.set("v.savingInProgress", true);

    action.setParams({
      contact: component.get("v.currentContact")
    });

    action.setCallback(this, function(a) {
      var state = a.getState();

      console.log('state'+state);

      if (state === "SUCCESS") {
        window.location.href = component.get("v.confirmationUrl");  
        console.log(a.getReturnValue());
        component.set("v.savingInProgress", false);
        console.log('confirmation URL'+v.confirmationUrl);        
          
      } else if (state === "ERROR") {
          console.log('Error in Save Process');
        component.set("v.savingInProgress", false);
        component.set("v.saveError", true);
        var errorMessages = [];
        var errors = a.getError();
          console.log(a.getError());

        for (i in errors) {
          var currentError = errors[i];

          for (currentField in currentError.fieldErrors) {
            for (cfe in currentError.fieldErrors[currentField]) {
              errorMessages.push(
                currentError.fieldErrors[currentField][cfe].message);
            }
          }
        }

        component.set("v.errorMessages", errorMessages);
      }
    });

    $A.enqueueAction(action);
  },
  checkValidation: function(component) {
    var c = component.get("v.currentContact");
    var requiredFields = component.get("v.requiredFields");
    var valid = true;

    if (!requiredFields) {
      return;
    }

    for (var i = 0; i < requiredFields.length; i++) {
      if (!c[requiredFields[i]]) {
        valid = false;
      }
    }


    component.set("v.valid", valid);
  }
})