({
	doInit: function(component, event, helper) {
    helper.initializeObjects(component);
	},
  save: function(component, event, helper) {
    helper.checkForDuplicateUsername(component);
  },
  handleFormUpdate: function(component, event, helper) {
    var eventRequiredFields = event.getParam("requiredFields");
    
    component.set("v.requiredFields", eventRequiredFields);

    helper.checkValidation(component);
  },
  handleTermChange: function(component, event, helper) {
    helper.checkValidation(component);
  }
})