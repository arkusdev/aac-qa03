<aura:application >
	<c:RegistrationComponent fieldSetName="NewUserRegistrationFields" confirmationUrl="../../apex/CommunitiesSelfRegConfirm" privacyPolicyUrl="http://www.adoptaclassroom.org/privacy-policy/" termsOfUseUrl="http://www.adoptaclassroom.org/terms-of-use/"/> 
     <c:FieldSetForm fieldSetName="NewUserRegistrationFields"
                        currentObject="{!Contact}"
                        registrationStyle="false" />
    <c:CreateClassroom userId="005W0000001z8WRIAY" fieldSetName="New_Classroom_Fields" redirectURL="/" />
    <c:ThankYouRedirect />
    <c:PaymentTypeSelection />
</aura:application>