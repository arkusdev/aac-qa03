({
  // Load current profile picture
  onInit: function(component) {
    var pictureType = component.get("v.pictureType");
    component.set("v.message", "Drag " + pictureType + " picture here");

    var action = component.get("c.getProfilePicture");
    action.setParams({
      recordId: component.get("v.recordId"),
      pictureType: pictureType
    });
    action.setCallback(this, function(a) {
      var attachment = a.getReturnValue();
      var subDomain = "/" + window.location.pathname.split("/")[1];
      if (attachment && attachment.Id) {
        component.set('v.pictureSrc', subDomain + '/servlet/servlet.FileDownload?file=' +
          attachment.Id + '&' + new Date().getTime());
      }
    });
    $A.enqueueAction(action);
  },

  onDragOver: function(component, event, helper) {
    helper.dragHover(component, true);
    event.preventDefault();
  },

  onDragLeave: function(component, event, helper) {
    helper.dragHover(component, false);
    event.preventDefault();
  },

  onDrop: function(component, event, helper) {
    helper.dragHover(component, false);
    event.stopPropagation();
    event.preventDefault();
    event.dataTransfer.dropEffect = 'copy';
    var files = event.dataTransfer.files;
    if (files.length > 1) {
      return alert("You can only upload one profile picture");
    }
    helper.readFile(component, helper, files[0]);
  }

})