({
  readFile: function(component, helper, file) {
    if (!file) return;
    if (!file.type.match(/(image.*)/)) {
      return alert('Image file not supported');
    }
    var reader = new FileReader();
    reader.onload = function() {
      var dataURL = reader.result;
      component.set("v.pictureSrc", dataURL);
      helper.upload(component, file, dataURL.match(/,(.*)$/)[1]);
    };
    reader.readAsDataURL(file);
  },

  upload: function(component, file, base64Data) {

    var action = component.get("c.saveAttachment");

    action.setParams({
      recordId: component.get("v.recordId"),
      pictureType: component.get("v.pictureType"),
      base64Data: base64Data,
      contentType: file.type
    });

    action.setCallback(this, function(a) {
      var message = (a == null) ? "Upload failed" : "Image uploaded";
      component.set("v.message", "Image uploaded");
    });
    component.set("v.message", "Uploading...");

    //$A.run(function() {
    window.setTimeout(
    $A.getCallback(function() {
        $A.enqueueAction(action);
    }), 10);

  },

  dragHover: function(component, isOver) {
    var imageDiv = component.getElements()[0];
    if (isOver)
      imageDiv.classList.add('drag-over');
    else
      imageDiv.classList.remove('drag-over');

  }

})