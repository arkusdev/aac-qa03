({
  doInit: function(component, event, helper) {
    console.log('Current user Id',$A.get("$SObjectType.CurrentUser.Id"));
    var e = $A.get("e.c:CartSessionRequest");
    e.fire();
  },
  showSpinner : function(component,event,helper){			// Rajesh code start from here for spinner
      // display spinner when aura:waiting (server waiting)
        component.set("v.toggleSpinner", true);  
      },
  hideSpinner : function(component,event,helper){
   // hide when aura:downwaiting
        component.set("v.toggleSpinner", false);
    },															// Rajesh code end to here for spinner		  
  goToCart: function(component, event, helper) {
    helper.fireNavigation(component, "cart");
	},
  goToDetails: function(component, event, helper) {
    helper.fireNavigation(component, "details");
  },
  goToPaymentSelection: function(component, event, helper) {
    helper.fireNavigation(component, "payment");
  },
  handleCartSessionResponse: function(component, event, helper) {
    component.set("v.currentSection", event.getParam("currentSection"));
    component.set("v.latestClassroomId", event.getParam("latestClassroomId"));
    helper.setBackToClassroomLink(component);
    helper.checkVisibility(component);
  },
  handleSectionComplete: function(component, event, helper) {
    var nextSection = event.getParam("nextSectionName");
    component.set("v.currentSection", nextSection);
    helper.checkVisibility(component);
    helper.fireNavigation(component, nextSection);
    helper.setActiveLink(component, nextSection);
  },
  handleSectionNavigation: function(component, event, helper) {
    helper.setActiveLink(component, event.getParam("sectionName"));
  },
  goBackToClassroom: function(component, event, helper) {
    var e = $A.get("e.c:CartSessionClear");
    e.fire();
    
    window.location.href = "designation/" +
      component.get("v.latestClassroomId");
  }
})