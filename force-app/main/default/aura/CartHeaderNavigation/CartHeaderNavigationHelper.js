({
	fireNavigation: function(component, sectionName) {
    var e = $A.get("e.c:CartSectionNavigation");
    
    e.setParams({
      "sectionName": sectionName
    });

    e.fire();
	},

  checkVisibility: function(component) {
    var currentSection = component.get("v.currentSection");

    if (currentSection != "cart") {
      component.set("v.showDetailsLink", true);
      component.set("v.showPaymentLink", true);
    }
  },
  resetActiveClasses: function(component) {
    var cartLink = component.find("cart");
    var detailsLink = component.find("details");
    var paymentLink = component.find("payment");

    $A.util.removeClass(cartLink, 'active');
    $A.util.removeClass(detailsLink, 'active');
    $A.util.removeClass(paymentLink, 'active');
  },
  setActiveLink: function(component, sectionName) {
    this.resetActiveClasses(component);

    var currentLink = component.find(sectionName);

    if (!currentLink) {
      return;
    }

    $A.util.addClass(currentLink, 'active');
  },
  setBackToClassroomLink: function(component) {
    //console.log("setting link");
    var latestClassroomId = component.get("v.latestClassroomId");

    console.log(latestClassroomId);

    if (latestClassroomId) {
      component.set("v.backToClassroomLink",
        "designation/" + latestClassroomId);
    } else {
      component.set("v.backToClassroomLink", null);
    }

    console.log(component.get("v.backToClassroomLink"));
  }
})