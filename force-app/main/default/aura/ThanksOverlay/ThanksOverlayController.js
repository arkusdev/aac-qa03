({ 

    init : function(component, event, helper) {
        var modalBody;
        var modalFooter;
        var showModal = "false";
        
        var displayModal = component.get('c.findDonations');
        displayModal.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                showModal = JSON.stringify(response.getReturnValue());
                console.log("****1*" + showModal);
                if(showModal == "true"){
                    $A.createComponents([
                        	["c:ThanksOverlay2",{}]
                        ],
                        function(components, status){
                            if (status === "SUCCESS") {
                                modalBody = components[0];
                                component.find('overlayLib').showCustomModal({
                                    header: "Thank Your Donor",
                                    body: modalBody,
                                    showCloseButton: false,
                                    cssClass: "my-modal,my-custom-class,my-other-class",
                                    closeCallback: function() {
                                                            
                                    }
                                });
                            }
                        }
                	);
                }
            }
        });
        $A.enqueueAction(displayModal);
    }
})