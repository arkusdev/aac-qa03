({
	myAction : function(component, event, helper) {
        		
	},
    
    doInit: function(cmp) {
        var action_1 = cmp.get("c.getBaseUrlSettings");
        action_1.setCallback(this, function(response){
            if(response !== null && response.getState() === 'SUCCESS'){
                cmp.set("v.vfHost", response.getReturnValue());
            }
        });
        
        $A.enqueueAction(action_1);        
               
        window.addEventListener("message", function(event) {
            var vfOrigin = cmp.get("v.vfHost");  
            if (event.origin !== vfOrigin) {
                return;
            }
            cmp.set("v.isOpen",event.data);
        }, false);
        
        var action = cmp.get("c.findDonations");		
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.isOpen",response.getReturnValue());
            }
        });
	 	$A.enqueueAction(action);
        
        var action_2 = cmp.get("c.fetchUser");
        action_2.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
               // debugger;
                var storeResponse = response.getReturnValue();
              
                cmp.set("v.userInfo", storeResponse);
            }
        });
        $A.enqueueAction(action_2);
    },

    closeModel: function(component, event, helper) {
      component.set("v.isOpen", false);
   },
})