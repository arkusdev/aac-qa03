({
	getFullRecord: function(component) {
    var action = component.get("c.queryRecord");

    action.setParams({
      recordId: component.get("v.recordId")
    });

    action.setCallback(this, function(a) {
      var state = a.getState();

      if (state === "SUCCESS") {
        var record = JSON.parse(a.getReturnValue());

        var url = "https://www.google.com/maps/embed/v1/place";

        url += "?key=" + component.get("v.googleAPIKey");

        var address = record[component.get("v.streetField")] + "," +
          record[component.get("v.cityField")] + "," +
            record[component.get("v.stateField")] + 
              " " + record[component.get("v.zipField")];

        url += "&q=" + encodeURI(address);

        component.set("v.googleMapsUrl", url);
      }

    });

    $A.enqueueAction(action);
	}
})