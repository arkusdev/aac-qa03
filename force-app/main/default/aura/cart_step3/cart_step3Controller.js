({
    validateStep : function(component, event, helper){
        let allValid = component.find('stepField').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            if(inputCmp.get('v.value') && typeof inputCmp.get('v.value') == 'string' && inputCmp.get('v.value').includes(" ")) {
                var value = '';
                value = inputCmp.get('v.value');
                inputCmp.set('v.value', value.trim());
            }
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        return allValid;
    },
    onlyNumbers : function(component, event, helper){
        var value = event.getSource().get("v.value");
        var newValue = value.replace(/\D/g,'');
        event.getSource().set("v.value", newValue);
    }
})