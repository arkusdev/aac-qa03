({
  checkout: function(component, event, helper) {
    var e = $A.get("e.c:CartSectionComplete");
	//conosole.log("**here");
    e.setParams({
      sectionName: "cart",
      nextSectionName: "details"
    });
    
    e.fire();  
  },

  nullify: function(component, event, helper) {
    event.preventDefault();
    return false;
  },

  update: function(component, event, helper) {
    //helper.countTotals(component);
    helper.countTotalsUpdate(component); // Added by Brighthat
    helper.sendCartUpdate(component);  
  },

  doInit: function(component, event, helper) {
    helper.getNextChargeDate(component);
    helper.countTotalsUpdate(component);  
    helper.getsubtotal(component);
    helper.sendCartUpdate(component);    
  },
  
  subtotal: function(component, event, helper) {
      helper.getsubtotal(component);
      helper.sendCartUpdate(component);
  },
  grandtotal: function(component, event, helper) {
    helper.getsubtotal(component);
    helper.sendCartUpdate(component);  
  },


  handleItemRemoval: function(component, event, helper) {
    var indexToRemove = event.getParam("index");
    var items = component.get("v.cartItems");

    items.splice(indexToRemove, 1);

    helper.countTotals(component);
    helper.sendCartUpdate(component);
    helper.checkValidation(component);

    component.set("v.cartItems", items);

    event.stopPropagation();
  },

  handleCartItemUpdate: function(component, event, helper) {
   //   console.log("In shopping handle update");
    helper.handleCartItemUpdate(component);
    event.stopPropagation();
  },

  updateFrequency: function(component, event, helper) {
    var newValue = event.getParam("value");
    if (newValue == 'Monthly') {
      
      var today = new Date();
      var endDate = new Date();
      endDate.setDate(today.getDate()+30);
      var dd = String(endDate.getDate()).padStart(2, '0');
      var mm = String(endDate.getMonth() + 1).padStart(2, '0'); 
      var yyyy = endDate.getFullYear();

      endDate = yyyy + '-' + mm + '-' + dd;

      component.set("v.minDate",endDate);

      component.set("v.displayedSection",true);
    } else {
      component.set("v.displayedSection",false);
    }
    var items = component.get('v.cartItems');
    items.forEach(function(item) {
      item.frequency = newValue;
    })
    helper.handleCartItemUpdateBis(component);
  },

  changeCoverFee: function(component, event, helper) {
    //  console.log("In shopping cover fee");
    helper.countTotals(component);
    helper.sendCartUpdate(component);
  },

  handleSectionNavigation: function(component, event, helper) {
    component.set("v.displaySection", event.getParam("sectionName"));
    helper.checkVisibility(component);
  },

  runPostResponse: function(component, event, helper) {
    //  console.log("In shopping run post");
    helper.checkVisibility(component);
    helper.checkValidation(component);
  },

  onEndDateChange: function(component, event, helper) {
    helper.sendCartUpdate(component);
  }
})