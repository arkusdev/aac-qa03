({
  
  getNextChargeDate: function(component) {
    var action = component.get("c.getNextMonthDate");

    action.setCallback(this, function(a) {
      var state = a.getState();

      if (state === "SUCCESS") {
        component.set("v.nextChargeDate", a.getReturnValue());
      }
    });

    $A.enqueueAction(action);
  },
  getgrandtotal: function(component) {
	var fee = component.get("v.fee");
    console.log(fee);
    var helpAmount = component.get("v.helpAmount");
    console.log(helpAmount);    
    var items = component.get("v.cartItems");    
    console.log(items[0].amount);
	component.set("v.grandTotal",helpAmount+items[0].amount+fee);            
  },
    
   getsubtotal: function(component) {
    var helpAmount = component.get("v.helpAmount");
    console.log(helpAmount);    
    var items = component.get("v.cartItems");    
    console.log(items[0].amount);
	var fee = component.get("v.fee");
    var itemamount = 0;    
   for (var i = 0; i < items.length; i++) {
      var product = items[i];
      itemamount = itemamount+ product.amount;
      //console.log('Expense Fees : ' + product.expenseFees);
      //if(product.expenseFees)
        //itemamount = itemamount + product.expenseFees;  
   }    
    component.set("v.total",fee+itemamount); 
    console.log('Updating here'+fee+component.get("v.helpAmount")+component.get("v.fee"));
    component.set("v.grandTotal",helpAmount+itemamount+fee);                
  },
    
  checkVisibility: function(component) {
    var displaySection = component.get("v.displaySection");

    var showSection = displaySection == "cart";

    component.set("v.showSection", showSection);
  },
   
  checkValidation: function(component) {
    var items = component.get("v.cartItems");
    var valid = true;

    if (!items || items.length == 0) {
      component.set("v.valid", false);
      return;
    }

    for (var i = 0; i < items.length; i++) {
      var item = items[i];

      if ((!item.amount || item.amount < 10) && item.type != "generalFund") {
        valid = false;
      }
    }

    component.set("v.valid", valid);
  },
    

  handleCartItemUpdate: function(component) {
    //   console.log("In shopping handle update");
     this.countTotals(component);
     this.sendCartUpdate(component);
     this.checkValidation(component);
   },

   handleCartItemUpdateBis: function(component) {
     this.sendCartUpdate(component);
     this.checkValidation(component);
   }
})