({
    getCart : function(component) {
        let cart = component.get('c.cartInit');
        cart.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                let apexcart = response.getReturnValue();
                this.setSessionCartItems(component, apexcart);
                this.populateStateOptions(component);
                component.set('v.loaded', true);
            } else {
                console.log(response.getError());
            }
        });
        $A.enqueueAction(cart);
    },

    setSessionCartItems : function(component, cart){
        if(sessionStorage && sessionStorage.getItem('newCartItems')){
            cart.cartItems = JSON.parse(sessionStorage.getItem('newCartItems'));
        }
        component.set('v.cart', cart);
    },

    submitCart : function(component){
        component.set('v.loaded', false);
        let action = component.get('c.cartSubmit');
        action.setParams({
            cart: JSON.stringify(component.get('v.cart'))
        });
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                //sessionStorage.clear();
                //window.location.href = "/donors/s/cart-thank-you?oid=" + response.getReturnValue();
                
                // Payment Request
                console.log('order id', response.getReturnValue());
                var returnOrderId = response.getReturnValue();
                var actionTwo = component.get('c.paymentRequest');
                actionTwo.setParams({ corderId : returnOrderId, cart :  JSON.stringify(component.get('v.cart'))});
                actionTwo.setCallback(this, function(res){
                    var stateTwo = res.getState();
                    if(stateTwo === 'SUCCESS') {
                        sessionStorage.clear();
                        window.location.href = "/donors/s/cart-thank-you?oid=" + returnOrderId;
                    } else {
                        component.set('v.loaded', true);
                        var errorsTwo = actionTwo.getError();
                         
                         if (errorsTwo[0] && errorsTwo[0].message) {                         
                             console.log(errorsTwo[0].message);
                             var toastEvent = $A.get("e.force:showToast");
                             toastEvent.setParams({
                                 "title": "Error",
                                 "type": "error",
                                 "message": errorsTwo[0].message
                             });
                             toastEvent.fire();
                        }
                    }
                });
                $A.enqueueAction(actionTwo);

                // Payment Request - END
            } else {
                component.set('v.loaded', true);
                var errors = action.getError();
                 
                 if (errors[0] && errors[0].message) {                         
                     console.log(errors[0].message);
                     var toastEvent = $A.get("e.force:showToast");
                     toastEvent.setParams({
                         "title": "Error",
                         "type": "error",
                         "message": errors[0].message
                     });
                     toastEvent.fire();
                 }
            }
        });
        $A.enqueueAction(action);
    },

    populateStateOptions : function(component) {
        let listStates = [
            {label:'Alaska', value:'Alaska'},
            {label:'Arizona', value:'Arizona'},
            {label:'Arkansas', value:'Arkansas'},
            {label:'Alabama', value:'Alabama'},
            {label:'California', value:'California'},
            {label:'Colorado', value:'Colorado'},
            {label:'Connecticut', value:'Connecticut'},
            {label:'Delaware', value:'Delaware'},
            {label:'District of Columbia', value:'District of Columbia'},
            {label:'Florida', value:'Florida'},
            {label:'Georgia', value:'Georgia'},
            {label:'Guam', value:'Guam'},
            {label:'Hawaii', value:'Hawaii'},
            {label:'Idaho', value:'Idaho'},
            {label:'Illinois', value:'Illinois'},
            {label:'Indiana', value:'Indiana'},
            {label:'Iowa', value:'Iowa'},
            {label:'Kansas', value:'Kansas'},
            {label:'Kentucky', value:'Kentucky'},
            {label:'Louisiana', value:'Louisiana'},
            {label:'Maine', value:'Maine'},
            {label:'Maryland', value:'Maryland'},
            {label:'Massachusetts', value:'Massachusetts'},
            {label:'Michigan', value:'Michigan'},
            {label:'Minnesota', value:'Minnesota'},
            {label:'Mississippi', value:'Mississippi'},
            {label:'Missouri', value:'Missouri'},
            {label:'Montana', value:'Montana'},
            {label:'Nebraska', value:'Nebraska'},
            {label:'Nevada', value:'Nevada'},
            {label:'New Hampshire', value:'New Hampshire'},
            {label:'New Jersey', value:'New Jersey'},
            {label:'New Mexico', value:'New Mexico'},
            {label:'New York', value:'New York'},
            {label:'North Carolina', value:'North Carolina'},
            {label:'North Dakota', value:'North Dakota'},
            {label:'Ohio', value:'Ohio'},
            {label:'Oklahoma', value:'Oklahoma'},
            {label:'Oregon', value:'Oregon'},
            {label:'Pennsylvania', value:'Pennsylvania'},
            {label:'Puerto Rico', value:'Puerto Rico'},
            {label:'Rhode Island', value:'Rhode Island'},
            {label:'South Carolina', value:'South Carolina'},
            {label:'South Dakota', value:'South Dakota'},
            {label:'Tennessee', value:'Tennessee'},
            {label:'Texas', value:'Texas'},
            {label:'Utah', value:'Utah'},
            {label:'Vermont', value:'Vermont'},
            {label:'Virgin Islands', value:'Virgin Islands'},
            {label:'Virginia', value:'Virginia'},
            {label:'Washington', value:'Washington'},
            {label:'West Virginia', value:'West Virginia'},
            {label:'Wisconsin', value:'Wisconsin'},
            {label:'Wyoming', value:'Wyoming'},
        ];
        component.set('v.stateOptions', listStates);
    },
    pushDataLayer : function(component) {
        component.set('v.pushGTM', true);
    }
})