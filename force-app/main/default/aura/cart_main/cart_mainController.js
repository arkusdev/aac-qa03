({

    init : function(component, event, helper){
        helper.getCart(component);
    },
    moveStep : function(component, event, helper){
        
        if(component.find(`cartStep${component.get('v.cart.step')}`).isValidStep()){
            if(parseInt(component.get('v.cart.step')) < 3){
                component.set('v.cart.step', (parseInt(component.get('v.cart.step')) + 1).toString());
            } else {
                helper.submitCart(component);
            }
        }

        if(component.get('v.cart.step')==3){
            helper.pushDataLayer(component);
        }
    },
    navigateTo : function(component, event, helper) {
        let cartStep = component.get('v.cart.step');
        var step = event.currentTarget.dataset.step;
        if(cartStep == '1' && step == 3){
            //validate step 1 and step 2
            if(component.find('cartStep1').isValidStep()) {
                component.set("v.cart.step", '2');
                window.setTimeout(
                    $A.getCallback(function() {
                        if(component.find('cartStep2').isValidStep()) {
                            component.set("v.cart.step", '3');
                        }
                    }), 50
                );
            }
        } else if(component.find(`cartStep${cartStep}`).isValidStep() || step < cartStep){
            component.set("v.cart.step", step);
        }
        if(component.get('v.cart.step')==3){
            helper.pushDataLayer(component);
        }
    }
})