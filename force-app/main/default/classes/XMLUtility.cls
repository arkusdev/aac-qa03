global class XMLUtility{
    public static Map<String,InvoiceFormatWrapper> consolidateInvoiceWrappersList(Map<String,List<InvoiceFormatWrapper>> purchaseOrderWithInvoiceWrapperMap1){
       
       Map<String,InvoiceFormatWrapper> consolidatedInvoiceMap=new Map<String,InvoiceFormatWrapper>();
       for(String i:purchaseOrderWithInvoiceWrapperMap1.keyset()){
           InvoiceFormatWrapper finalWrap=new InvoiceFormatWrapper();
           finalWrap.lineItemMap =new Map<String,lineItem>();
           finalWrap.orderNumber=i;
           finalWrap.errors='';           
               
           for(InvoiceFormatWrapper invList:purchaseOrderWithInvoiceWrapperMap1.get(i)){
               if(invList.orderDate != null){
                    finalWrap.orderDate=invList.orderDate;
               }      
               else{
                    finalWrap.orderDate=null;
               }
               
               if(invList.vendorNameFromFile != null){//New
                   finalWrap.vendorNameFromFile=invList.vendorNameFromFile;
               }     
                         
               if(invList.SubTotalAmount != null)
                    finalWrap.SubTotalAmount= (finalWrap.SubTotalAmount != null) ? (invList.SubTotalAmount + finalWrap.SubTotalAmount) : invList.SubTotalAmount;
               if(invList.taxAmount != null)
                    finalWrap.taxAmount= (finalWrap.taxAmount != null) ? (invList.taxAmount + finalWrap.taxAmount) : invList.taxAmount;
               if(invList.shippingAmount != null)
                    finalWrap.shippingAmount= (finalWrap.shippingAmount != null) ? (invList.shippingAmount + finalWrap.shippingAmount) : invList.shippingAmount;
               if(invList.discountAmt != null)
                    finalWrap.discountAmt= (finalWrap.discountAmt != null) ? (invList.discountAmt + finalWrap.discountAmt) : invList.discountAmt;
               if(invList.totalAmount != null)
                    finalWrap.totalAmount= (finalWrap.totalAmount != null) ? (invList.totalAmount + finalWrap.totalAmount) : invList.totalAmount;
                    
               if(invList.orderLineItemList != null && !invList.orderLineItemList.isEmpty()){
                   for(lineItem li:invList.orderLineItemList){
                       if(!finalWrap.lineItemMap.containsKey(li.itemNumber)){
                           finalWrap.lineItemMap.put(li.itemNumber,new lineItem());
                           finalWrap.lineItemMap.get(li.itemNumber).itemNumber =li.itemNumber;
                       }if(li.itemQuantity != null)
                           finalWrap.lineItemMap.get(li.itemNumber).itemQuantity = (finalWrap.lineItemMap.get(li.itemNumber).itemQuantity != null) ? (finalWrap.lineItemMap.get(li.itemNumber).itemQuantity + li.itemQuantity) : li.itemQuantity;
                       if(li.itemUnitPrice != null)
                           finalWrap.lineItemMap.get(li.itemNumber).itemUnitPrice =li.itemUnitPrice;
                       if(li.itemTotal != null)
                           finalWrap.lineItemMap.get(li.itemNumber).itemTotal=(finalWrap.lineItemMap.get(li.itemNumber).itemTotal != null) ? (finalWrap.lineItemMap.get(li.itemNumber).itemTotal + li.itemTotal) : li.itemTotal;
                                            
                   }                   
               }
           }           
           consolidatedInvoiceMap.put(i,finalWrap);
       }
       return consolidatedInvoiceMap;
   }
   
   global class InvoiceFormatWrapper{
       global Id attachmentId;
       global Id vendorFileId;
       global Id vendorLookupId;
       global String vendorId;
       global String vendorName;
       global String vendorNameFromFile;
       global String invoiceNumber;
       global Date invoiceDate;
       global String orderNumber;
       global string purChaseOrderNumber;
       global String purChaseOrderName;
       global Date orderDate;
       global Integer lineItemCount;
       global List<lineItem> orderLineItemList;
       global Map<string,lineItem> lineItemMap;
       global Decimal SubTotalAmount;   
       global Decimal taxAmount;
       global Decimal shippingAmount;
       global Decimal discountAmt;
       global Decimal totalAmount;   
       global string errors;  
       
       global  InvoiceFormatWrapper(){
           this.orderLineItemList=new List<lineItem>();
       } 
       
       global  InvoiceFormatWrapper(Id attachmentID,String Errors){
           this.attachmentId=attachmentID;
           this.Errors=Errors;
           this.orderLineItemList=new List<lineItem>();
       }  
   }
   
   global class lineItem{
       global string itemNumber;
       global Integer itemQuantity;
       global Decimal itemUnitPrice;
       global Decimal itemTotal;  
       global Id productId;
       global Id purchaseOrderLineId;      
   }
   
}