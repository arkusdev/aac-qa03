public with sharing class DonorDetailsController {

  @AuraEnabled
  public static pymt__PaymentX__c savePaymentData(Contact c, Account a, pymt__PaymentX__c payment, String cartItemString, String inHonorOfName, Decimal subTotal, Decimal total, Decimal monthlySubTotal, Decimal monthlyTotal, Decimal fee, Decimal monthlyFee) {
    return DonorUtils.savePaymentData(c, a, payment, cartItemString, inHonorOfName, subTotal, total, monthlySubTotal, monthlyTotal, fee, monthlyFee);
  }

  @AuraEnabled
  public static Contact upsertContact(Contact c) {
    return DonorUtils.upsertContact(c);
  }

  @AuraEnabled
  public static Account upsertAccount(Account a, String donorType) {
    return DonorUtils.upsertAccount(a, donorType);
  }

  @AuraEnabled
  public static List<FAQ__c> getFAQs() {
    return DonorUtils.getFAQs();
  }

  private static Map<Id, donation_split__Designation__c> getClassroomToCampaignIdMap(Set<Id> classroomIds) {
    return DonorUtils.getClassroomToCampaignIdMap(classroomIds);
  }
  
  @AuraEnabled
  public static boolean cookiesSet() {
    return DonorUtils.cookiesSet();
  }
}