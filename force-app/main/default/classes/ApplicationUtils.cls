public with sharing class ApplicationUtils {

  private static final String SELECT_ONE_OPTION = '-- Select One --';

  public static Map<String, List<CustomSelectOption>>
      getSelectOptionsForFieldAndObject(
        Schema.DescribeSObjectResult objectDescribe, List<String> fieldNames) {
    return getSelectOptionsForFieldAndObject(
      objectDescribe, fieldNames, true);
  }

  public static Map<String, List<CustomSelectOption>>
      getSelectOptionsForFieldAndObject(
        Schema.DescribeSObjectResult objectDescribe, List<String> fieldNames,
          Boolean addEmptyOption) {
    if (objectDescribe == null || fieldNames == null || fieldNames.isEmpty()) {
      return null;
    }

    Schema.DescribeFieldResult fd;
    Schema.SObjectField field;
    List<CustomSelectOption> options;
    Map<String, Schema.SObjectField> fieldMap = objectDescribe.fields.getMap();
    Map<String, List<CustomSelectOption>> optionMap =
      new Map<String, List<CustomSelectOption>>();

    for (String fn : fieldNames) {
      field = fieldMap.get(fn);

      if (field == null) {
        continue;
      }

      fd = field.getDescribe();
      options = new List<CustomSelectOption>();

      if (addEmptyOption) {
        options.add(new CustomSelectOption('', SELECT_ONE_OPTION, false));
      }

      for (Schema.PicklistEntry ple : fd.getPicklistValues()) {
        options.add(new CustomSelectOption(ple.getValue(), ple.getLabel(),
          ple.isDefaultValue()));
      }

      optionMap.put(fn, options);
    }

    return optionMap;
  }

  public static Account getAccountFromUserId(Id userId) {
    try {
      return [select Id,
                     ContactId,
                     Contact.AccountId,
                     Contact.Account.Id,
                     Contact.Account.Name
              from User
              where Id = :userId].Contact.Account;
    } catch (Exception e) {
      return null;
    }
  }
}