public with sharing class SalesTaxManagement {
    
   // public 
    
    public static void afterTrigger( List<Teacher_Sales_Tax__c> nList, Map<Id, Teacher_Sales_Tax__c> oldMap)
    {
        List<Contact> contactList = new List<Contact>();
        Map<String, Teacher_sales_tax__c> taxmap = new Map<string,Teacher_sales_tax__c>();
        set<string> countrycode = new set<string>();
        set<string> statecode = new set<string>();
        for(Teacher_sales_tax__c t : nlist)
        {
            countrycode.add(t.country__c);
            statecode.add(t.States__c);
            string key = t.country__c+t.states__c;
            taxmap.put(key,t);
        }
        contactList = [select Id,Name,School__c,FirstName,LastName,Phone,School__r.Name,mailingCity,mailingStateCode,mailingState,mailingCountry,mailingPostalCode,mailingCountryCode,mailingStreet,Order_Cloud_Spending_Account_Id__c,Mailing_Address_Id__c, Billing_Address_Id__c from contact where mailingState IN: stateCode and mailingCountry IN: countrycode and Mailing_Address_Id__c != Null];
        for(contact con: contactList)
        {    
            string key = con.mailingCountry+con.mailingState;
            if(taxmap.containskey(key))
            {
                System.enqueueJob(new ShippingAddressUpdateQueueable(con,taxmap));
            }
        }
    }
    public static void beforeDelete( List<Teacher_Sales_Tax__c> nList, Map<Id, Teacher_Sales_Tax__c> oldMap)
    {
        List<Contact> contactList = new List<Contact>();
        Map<String, Teacher_sales_tax__c> taxmap = new Map<string,Teacher_sales_tax__c>();
        set<string> countrycode = new set<string>();
        set<string> statecode = new set<string>();
        for(Teacher_sales_tax__c t : oldMap.values())
        {
            countrycode.add(t.country__c);
            statecode.add(t.States__c);
            string key = t.country__c+t.states__c;
            
        }
        contactList = [select Id,Name,School__c,FirstName,LastName,Phone,School__r.Name,mailingCity,mailingStateCode,mailingState,mailingCountry,mailingPostalCode,mailingCountryCode,mailingStreet,Order_Cloud_Spending_Account_Id__c,Mailing_Address_Id__c, Billing_Address_Id__c from contact where mailingState IN: stateCode and mailingCountry IN: countrycode and Mailing_Address_Id__c != Null];
        for(contact con: contactList)
        {  
            System.enqueueJob(new ShippingAddressUpdateQueueable(con,taxmap));           
        }
    }
}