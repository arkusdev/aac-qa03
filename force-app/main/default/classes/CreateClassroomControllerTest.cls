@isTest
private class CreateClassroomControllerTest {

  @testSetup static void initTestData() {

    user tu = TestUtils.createSystemAdministrator();
    Insert tu;
    Account a = TestUtils.createAccount();
    Contact c = TestUtils.createContact(a.id);
    system.runas(tu)
    {
    
    insert a;    
    insert c;
    TestUtils.createTestUser(c.id);
    
    Folder f = 
      [select Id
       from Folder
       where DeveloperName = 'Object_Describes'];

    Self_Registration_Settings__c srSettings =
      new Self_Registration_Settings__c(
        Profile_Picture_Folder__c = f.Id,
        Default_Owner_Id__c = UserInfo.getUserId(),
        Default_Record_Type_Name__c= 'HH_Account',
        Portal_Account_Onwer_Id__c = UserInfo.getUserId());
    insert srSettings;

    donation_split__Designation__c classroom =
      new donation_split__Designation__c(
        Name='Test Classroom',
        How_will_you_spend_your_funds__c = 'This is my really long "how will I spend my funds" response, I really hope that it is 100 characters long.',
        Description__c = 'MORE Test Description area about my class. Bacon ipsum dolor amet chicken beef ribs shoulder, porchetta cow ground round beef doner tri-tip andouille sausage pork sirloin cupim short loin. Meatloaf ham ribeye beef ribs pancetta cow biltong salami filet mignon drumstick cupim doner pork loin. Meatloaf boudin bacon salami ball tip alcatra. Spare ribs pork short ribs flank. Pig salami fatback, bacon ground round ribeye pancetta chuck porchetta. Bacon ipsum dolor amet chicken beef ribs shoulder, porchetta cow ground round beef doner tri-tip andouille sausage pork sirloin cupim short loin. Meatloaf ham ribeye beef ribs pancetta cow biltong salami filet mignon drumstick cupim doner pork loin. Meatloaf boudin bacon salami ball tip alcatra. Spare ribs pork short ribs flank. Pig salami fatback, bacon ground round ribeye pancetta chuck porchetta.',
        Teacher__c = c.Id);
    insert classroom;
    }
  }

    @isTest static void testProfilePicture() {

    User u = [select Id from User order by CreatedDate desc limit 1];

    ProfilePictureController.getProfilePicture(u.Id, 'classroom');
    ProfilePictureController.saveAttachment(u.Id, 'classroom', 'asdf', 'text/plain');
    ProfilePictureController.getClassroom(u.Id);

    }

    @isTest static void testCreateClassroom(){

    Account school = [select Id from Account limit 1];
    User u = [select Id from User order by CreatedDate desc limit 1];
    donation_split__Designation__c classroom = [select Id from donation_split__Designation__c limit 1];

    CreateClassroomController.updateClassroom(classroom);
    CreateClassroomController.updateSchool(school);
    CreateClassroomController.getContactFromUser(u.id, classroom);
    CreateClassroomController.getRecordType('School');

    }


   @isTest static void testGetProfilePictureNew() {
     User u = [select Id from User order by CreatedDate desc limit 1];
     Test.startTest();
      String result = ProfilePictureController.getProfilePictureNew(u.id, 'jpg');
     Test.stopTest();

     system.assertNotEquals(null, result);
   }

    @isTest static void testGetContactDataForUserWithUser() {
     User u = [select Id from User order by CreatedDate desc limit 1];
     Test.startTest();
      Contact result = ProfilePictureController.getContactDataForUser(u.id);
     Test.stopTest();

     system.assertNotEquals(null, result);
   }

   @isTest static void testGetClassroomFromUserWithoutUser() {
     donation_split__Designation__c dsd = [select Id from donation_split__Designation__c order by CreatedDate desc limit 1];
     Test.startTest();
      donation_split__Designation__c result = ProfilePictureController.getClassroomFromUser(dsd.Id);
     Test.stopTest();

     system.assertNotEquals(null, result);
   }

   
}