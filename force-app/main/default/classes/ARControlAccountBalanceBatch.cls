/**
 * Created by christopherjohnson on 4/5/18.
 */

public with sharing class ARControlAccountBalanceBatch  implements Database.Batchable<sObject>, Database.Stateful {
    private List<AcctSeed__Accounting_Period__c> periods;
    private AcctSeed__Accounting_Period__c startPeriod;
    private Id arGLAccountId;
    private Id transReportId;
    private Id arReportId;

    private final String query = 'SELECT Id, Name, AcctSeed__Account__c, AcctSeed__Billing_Line__r.AcctSeed__Billing__r.AcctSeed__Type__c, AcctSeed__Employee__c, AcctSeed__Accounting_Period__c, AcctSeed__GL_Account__c, AcctSeed__Amount__c, AcctSeed__Billing_Line__c, AcctSeed__Billing_Line__r.AcctSeed__Total__c ' +
            'FROM AcctSeed__Transaction__c WHERE AcctSeed__GL_Account__c = :arGLAccountId AND AcctSeed__Accounting_Period__c IN :periods ORDER BY AcctSeed__Accounting_Period__c ASC';


    private Map<String, Control_Account_Balance__c> accountBalanceMap;

    public ARControlAccountBalanceBatch(Id startPeriodId) {
        this.startPeriod = [Select Id, Name, AcctSeed__Status__c From AcctSeed__Accounting_Period__c Where Id = :startPeriodId];
        periods = [Select Id, Name, AcctSeed__Status__c From AcctSeed__Accounting_Period__c Where Name <= :startPeriod.Name];
        transReportId = getReportId('ARAP_Transaction_Report');
        arReportId =  getReportId('ARAP_AR_Aging_Report');
        arGLAccountId = [Select Id, AcctSeed__AR_Control_GL_Account__c FROM AcctSeed__Accounting_Settings__c ORDER BY CreatedDate DESC LIMIT 1].AcctSeed__AR_Control_GL_Account__c;
        accountBalanceMap = new Map<String, Control_Account_Balance__c>();

    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {


        for (AcctSeed__Transaction__c tran : (AcctSeed__Transaction__c[]) scope) {
            String key = getKey(tran);

            if (accountBalanceMap.containsKey(key)) {
                accountBalanceMap.get(key).Balance__c += tran.AcctSeed__Amount__c;
            }
            else {
                Control_Account_Balance__c cab = new Control_Account_Balance__c();
                cab.Customer_Vendor__c = tran.AcctSeed__Account__c != NULL ? key : NULL;
                cab.Balance__c = tran.AcctSeed__Amount__c;
                cab.Accounting_Period__c = startPeriod.Id;
                cab.GL_Account__c = arGLAccountId;
                cab.Aging_Amount__c = 0;
                cab.Type__c = 'Accounts Receivable';
                cab.Transaction_Report_Id__c = transReportId;
                cab.Aging_Report_Id__c = arReportId;
                accountBalanceMap.put(key, cab);
            }

        }
    }

    private String getKey(AcctSeed__Transaction__c tran) {

        String key;
        /*
        if ((tran.AcctSeed__Billing_Line__r.AcctSeed__Billing__r.AcctSeed__Type__c == 'Invoice' && tran.AcctSeed__Amount__c < 0 && tran.AcctSeed__Billing_Line__r.AcctSeed__Total__c > 0)
                || (tran.AcctSeed__Billing_Line__r.AcctSeed__Billing__r.AcctSeed__Type__c == 'Credit Memo' && tran.AcctSeed__Amount__c > 0 && tran.AcctSeed__Billing_Line__r.AcctSeed__Total__c < 0)) {

            return key;
        }
        */

        return tran.AcctSeed__Account__c;
    }

    private Id getReportId(String devName) {
        Id reportId;

        try {
            reportId = [Select Id From Report Where DeveloperName = :devName].Id;
        }
        catch (Exception ex) {

        }

        return reportId;
    }

    public void finish(Database.BatchableContext BC) {

        for (AcctSeed__Billing_Aging_History__c bh : [Select Id, AcctSeed__Amount__c, AcctSeed__Billing__r.AcctSeed__Customer__c From AcctSeed__Billing_Aging_History__c Where AcctSeed__Accounting_Period__r.Name = :startPeriod.Name]) {
            String key = bh.AcctSeed__Billing__r.AcctSeed__Customer__c;

            if (accountBalanceMap.containsKey(key)) {
                accountBalanceMap.get(key).Aging_Amount__c += bh.AcctSeed__Amount__c;
            }
            else {
                Control_Account_Balance__c cab = new Control_Account_Balance__c();
                cab.Customer_Vendor__c = bh.AcctSeed__Billing__r.AcctSeed__Customer__c != NULL ? key : NULL;
                cab.Balance__c = 0;
                cab.Aging_Amount__c = bh.AcctSeed__Amount__c;
                cab.Accounting_Period__c = startPeriod.Id;
                cab.GL_Account__c = arGLAccountId;
                cab.Type__c = 'Accounts Receivable';
                accountBalanceMap.put(key,cab);
            }
        }
        normalizeCurrency();
        insert accountBalanceMap.values();
    }


    private void normalizeCurrency() {
        for (Control_Account_Balance__c cab : accountBalanceMap.values()) {
            cab.Balance__c = cab.Balance__c.setScale(2,System.RoundingMode.HALF_UP);
            cab.Aging_Amount__c = cab.Aging_Amount__c.setScale(2,System.RoundingMode.HALF_UP);
        }
    }
}