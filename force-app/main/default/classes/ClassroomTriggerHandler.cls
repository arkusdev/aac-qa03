public without sharing class ClassroomTriggerHandler {
    public static void UpdateRobots(List<donation_split__Designation__c> scope) {
        for(donation_split__Designation__c classroom : scope){
            if(classroom.Hide_My_Page_from_Search_Results_Online__c) {
                classroom.Robots_text__c = 'noindex';
            } else {
                classroom.Robots_text__c = 'all';
            }
        }
    }

    public static void migrateAnnouncements(List<donation_split__Designation__c> scope) {
        
        Map<Id, Id> mapTeacherClassroomIds = new Map<Id, Id>();
        for(donation_split__Designation__c classroom : scope) {
            if(classroom.Teacher__c != null && classroom.Id != null) {
                mapTeacherClassroomIds.put(classroom.Teacher__c, classroom.Id);
            }
        }

        //Get all Announcementes with the same teacher/school contact and update the classroom to appear in the fundraising page
        List<Announcement__c> announcements = [SELECT Teacher_School__c, Classroom__c FROM Announcement__c WHERE Teacher_School__c IN :mapTeacherClassroomIds.keySet() AND Classroom__c = NULL AND Teacher_School__c != NULL];
        if(!announcements.isEmpty()) {
            for(Announcement__c ann : announcements){
                if(mapTeacherClassroomIds.containsKey(ann.Teacher_School__c)){
                    ann.Classroom__c = mapTeacherClassroomIds.get(ann.Teacher_School__c);
                }
            }
            update announcements;
        }
    }

    public static void sendActiveEmails() {
        
        if( Trigger.isAfter && Trigger.isUpdate ){
            Set<Id> scope = new Set<Id>();

            for(donation_split__Designation__c classroom : ((List<donation_split__Designation__c>)Trigger.new)) {
                if( (((Map<Id,donation_split__Designation__c>)Trigger.oldMap).get(classroom.Id).Inactive_Classroom_Page__c == true) && classroom.Inactive_Classroom_Page__c == false) {
                    scope.add(classroom.Id);
                }
            }

            List<Notify_Donor__c> notifyDonors = [SELECT Id, Classroom__c, Classroom__r.Teacher__c, Email__c, Notified__c FROM Notify_Donor__c WHERE Classroom__c IN :scope];
            if(!notifyDonors.isEmpty()) {
                OrgWideEmailAddress owa = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName='AdoptAClassroom.org' LIMIT 1];
                List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
                
                EmailTemplate templateIdEmail = [SELECT Id FROM EmailTemplate WHERE name = 'Notify re-active Email' LIMIT 1];
                
                for(Notify_Donor__c donor : notifyDonors) {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(donor.Classroom__r.Teacher__c);
                    mail.setTreatTargetObjectAsRecipient(false);
                    List<String> sendTo = new List<String>();
                    sendTo.add(donor.Email__c);
                    mail.setToAddresses(sendTo);
                    mail.setSaveAsActivity(false);
                    mail.setOrgWideEmailAddressId(owa.Id);
                    mail.setTemplateID(templateIdEmail.Id);
                    mail.setWhatId(donor.Id);
                    allmsg.add(mail);

                    donor.Notified__c = true;
                }

                update notifyDonors;
                if(!Test.isRunningTest()) {
                    Messaging.sendEmail(allmsg);
                }
            }

        }
    }
}