@isTest
private class ContentDocumentLinkTriggerSchoolTest
{
	public static account accountData;
    public static contact contactData ;
	public static donation_split__Designation__c classroom ;
	
    @isTest
    static void itShould()
    {
		accountData= TestUtils.createAccount();
        accountData.Email__c ='test@gmail.com';
        insert accountData;
		
		contactData = TestUtils.createContact(accountData.Id);
        contactData.npe01__WorkEmail__c='test@gmail.com';    
        contactData.email='test@gmail.com';
        insert contactData;
		
		classroom = TestUtils.createClassroom(contactData.Id, accountData.Id);
        classroom.Description__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		classroom.How_will_you_spend_your_funds__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		classroom.Why_We_Need_Your_Help__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		insert classroom;
		
        ContentVersion content=new ContentVersion(); 
            content.Title='banner'; 
            content.PathOnClient='/' + content.Title + '.pdf'; 
            Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
            content.VersionData=bodyBlob; 
            //content.LinkedEntityId=sub.id;
            content.origin = 'H';
        insert content;
		
		ContentDocumentLink contentlink=new ContentDocumentLink();
            contentlink.LinkedEntityId=contactData.Id;
            contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: content.id].contentdocumentid;
            contentlink.ShareType = 'V';
            test.starttest();
        insert contentlink;
		
		test.stoptest();

    }
}