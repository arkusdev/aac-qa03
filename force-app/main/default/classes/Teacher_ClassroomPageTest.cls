@isTest
public with sharing class Teacher_ClassroomPageTest {
    private static final Id SCHOOL_RECORD_TYPE = [SELECT Id FROM RecordType WHERE Name = 'School' LIMIT 1].Id;

    @TestSetup
    static void createData(){
        Account acc = new Account(
            Name = 'Test',
            RecordTypeId = SCHOOL_RECORD_TYPE,
            ShippingCity = 'Test City',
            ShippingCountry = 'US',
            ShippingState = 'Alabama',
            ShippingPostalCode = '12345',
            ShippingStreet = '123 Test address'
        );

        insert acc;

        
        Contact newContact = new Contact(
            LastName = 'Testing Contact'
            );
            
            insert newContact;
        String profileId = [SELECT ID FROM Profile WHERE Name = 'AAC - Teacher Community Plus Login User'].Id;
        User testUser = new User(
            FirstName = 'User Test',
            LastName  = 'User Test',
            Email     = 'UserTest@UserTest.com',
            Username  = 'UserTest@UserTest.com',
            Alias     = 'fatty',
            ProfileId = profileId,
            UserPreferencesShowProfilePicToGuestUsers = true,
            ContactId = newContact.Id,
            TimeZoneSidKey    = 'America/Denver',
            LocaleSidKey      = 'en_US',
            EmailEncodingKey  = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        
        insert testUser;

    }

    @IsTest
    static void getDataTest(){
        User usTest = getUserTest();
        Teacher_ClassroomPage.ClassRoomWrapper results;
        
        Test.startTest();
        System.runAs(usTest) {
            results = Teacher_ClassroomPage.getData();
        }
        Test.stopTest();

        System.assertNotEquals(null, results, 'The data does not be null.');
    }
    @IsTest
    static void saveRecordTest(){
        User usTest = getUserTest();
        donation_split__Designation__c classroom = new donation_split__Designation__c();
        classroom.Hide_My_Page_from_Search_Results_Online__c = false;
        classroom.Number_of_Students__c = null;
        classroom.Name = null;
        classroom.Teaching_Area__c = null;
        classroom.Fundraising_Goal__c = null;
        classroom.Description__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eget magna nec sapien eleifend aliquam vel sit amet libero. Aenean convallis feugiat ante, et ullamcorper ante tristique at. Duis lobortis, erat in posuere maximus, mauris orci pellentesque magna, nec auctor massa neque sed dolor.' + 
        'Pellentesque tincidunt purus lectus. Suspendisse at elementum ipsum. Vivamus malesuada auctor nulla nec pretium. Nullam et mollis odio, finibus ultrices magna.' +
        ' Sed porta finibus vulputate. Quisque consectetur dui eu nulla sodales sollicitudin. Praesent porta nibh vitae interdum bibendum. Fusce libero metus, consectetur finibus eros eu, rhoncus tristique nisl.' +
        ' Nam in dolor vel arcu gravida mattis nec faucibus urna. Donec dapibus luctus pretium. Nam placerat tortor nisl, sit amet euismod neque imperdiet ut. Integer vehicula nisl at volutpat condimentum. In consectetur velit eu mauris dapibus, sed hendrerit lorem ornare. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt pellentesque nulla, eget congue eros.' +
        'Ut posuere nibh magna. Mauris et suscipit nisi, a dapibus nibh. Vestibulum malesuada eros mauris, ac mattis lacus blandit et. Maecenas sodales ex nec imperdiet ultrices. Sed leo erat, rutrum non finibus sed, auctor id nulla. Nullam in rutrum lorem. Praesent iaculis tellus a dapibus dictum. ' +
        'Aenean volutpat a felis quis scelerisque. Morbi viverra venenatis tincidunt. Aenean ut finibus magna. Integer tincidunt eu odio sed tristique. Proin et tincidunt ante. Etiam in lorem non turpis commodo mollis et a tortor. Mauris sit amet velit sed dolor egestas dignissim. Phasellus dignissim velit et elit consectetur, sed faucibus nulla aliquam. Curabitur convallis orci eu pulvinar.';
        classroom.How_will_you_spend_your_funds__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eget magna nec sapien eleifend aliquam vel sit amet libero. Aenean convallis feugiat ante, et ullamcorper ante tristique at. Duis lobortis, erat in posuere maximus, mauris orci pellentesque magna, nec auctor massa neque sed dolor.' + 
        ' Nam in dolor vel arcu gravida mattis nec faucibus urna. Donec dapibus luctus pretium. Nam placerat tortor nisl, sit amet euismod neque imperdiet ut. Integer vehicula nisl at volutpat condimentum. In consectetur velit eu mauris dapibus, sed hendrerit lorem ornare. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt pellentesque nulla, eget congue eros.' +
        'Ut posuere nibh magna. Mauris et suscipit nisi, a dapibus nibh. Vestibulum malesuada eros mauris, ac mattis lacus blandit et. Maecenas sodales ex nec imperdiet ultrices. Sed leo erat, rutrum non finibus sed, auctor id nulla. Nullam in rutrum lorem. Praesent iaculis tellus a dapibus dictum. ' +
        'Aenean volutpat a felis quis scelerisque. Morbi viverra venenatis tincidunt. Aenean ut finibus magna. Integer tincidunt eu odio sed tristique. Proin et tincidunt ante. Etiam in lorem non turpis commodo mollis et a tortor. Mauris sit amet velit sed dolor egestas dignissim. Phasellus dignissim velit et elit consectetur, sed faucibus nulla aliquam. Curabitur convallis orci eu pulvinar.';
        classroom.Why_We_Need_Your_Help__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eget magna nec sapien eleifend aliquam vel sit amet libero. Aenean convallis feugiat ante, et ullamcorper ante tristique at. Duis lobortis, erat in posuere maximus, mauris orci pellentesque magna, nec auctor massa neque sed dolor.' + 
        'Pellentesque tincidunt purus lectus. Suspendisse at elementum ipsum. Vivamus malesuada auctor nulla nec pretium. Nullam et mollis odio, finibus ultrices magna.' +
        ' Sed porta finibus vulputate. Quisque consectetur dui eu nulla sodales sollicitudin. Praesent porta nibh vitae interdum bibendum. Fusce libero metus, consectetur finibus eros eu, rhoncus tristique nisl.' +
        ' Nam in dolor vel arcu gravida mattis nec faucibus urna. Donec dapibus luctus pretium. Nam placerat tortor nisl, sit amet euismod neque imperdiet ut. Integer vehicula nisl at volutpat condimentum. In consectetur velit eu mauris dapibus, sed hendrerit lorem ornare. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt pellentesque nulla, eget congue eros.' +
        'Ut posuere nibh magna. Mauris et suscipit nisi, a dapibus nibh. Vestibulum malesuada eros mauris, ac mattis lacus blandit et. Maecenas sodales ex nec imperdiet ultrices. Sed leo erat, rutrum non finibus sed, auctor id nulla. Nullam in rutrum lorem. Praesent iaculis tellus a dapibus dictum. ' +
        'Aenean volutpat a felis quis scelerisque. Morbi viverra venenatis tincidunt. Aenean ut finibus magna. Integer tincidunt eu odio sed tristique. Proin et tincidunt ante. Etiam in lorem non turpis commodo mollis et a tortor. Mauris sit amet velit sed dolor egestas dignissim. Phasellus dignissim velit et elit consectetur, sed faucibus nulla aliquam. Curabitur convallis orci eu pulvinar.';
        classroom.Teacher__c = [SELECT Id FROM Contact].Id;
        classroom.donation_split__Active__c = true;


        Test.startTest();
        System.runAs(usTest) {
            Teacher_ClassroomPage.saveRecord(classroom);
        }
        Test.stopTest();

        List<donation_split__Designation__c> result = [SELECT Id, Number_of_Students__c FROM donation_split__Designation__c LIMIT 1];

        System.assertEquals(1, result.size(), 'It should create 1 classroom.');
    }

  

    private static User getUserTest(){
        return [SELECT Id,ContactId FROM User WHERE username = 'UserTest@UserTest.com' LIMIT 1];

    }
}