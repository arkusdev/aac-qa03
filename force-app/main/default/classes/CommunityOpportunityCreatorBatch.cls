global class CommunityOpportunityCreatorBatch implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'select Id,'
                        + ' (select Id from Community_Opportunities__r)'
                    + ' from Opportunity'
                    + ' where RecordType.DeveloperName IN (\'adjustment\', \'Disbursement\', \'schooldonation\', \'adoption\', \'Orders\')';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Community_Opportunity__c> commOppList = new List<Community_Opportunity__c>();
        for (Opportunity opp : (List<Opportunity>) scope) {
            if (opp.Community_Opportunities__r == null || opp.Community_Opportunities__r.isEmpty()) {
                Community_Opportunity__c commOpp = new Community_Opportunity__c();
                commOpp.Opportunity__c = opp.Id;
                commOppList.add(commOpp);
            }
        }
        
        if (!commOppList.isEmpty())
            insert commOppList;
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
}