public with sharing class CDEController {
    
    public Id recordId { get; set; }
    public List<wrapResult> orders {
        get {
            if(recordId!= null) {
                ChargentOrders__ChargentOrder__c c = [select ChargentOrders__Payment_Frequency__c,(SELECT Teacher_s_School__c, Teacher__r.Name, Amount_W_Feed__c FROM Opportunities__r WHERE RecordType.name!='Unrestricted Donation') FROM ChargentOrders__ChargentOrder__c WHERE Id = :recordId ];
                if(orders==null) {
                    orders = new List<wrapResult>();
                }
                for(Opportunity opp : c.Opportunities__r) {
                    orders.add(new wrapResult(opp.Teacher__r.Name, opp.Teacher_s_School__c, opp.Amount_W_Feed__c, c.ChargentOrders__Payment_Frequency__c));
                }
            }
            return orders;
        }
        set;
    }
    
    public CDEController() {}

    public class wrapResult {
        
        public String TeacherName{get;set;}
        public String TeacherSchool{get;set;}
        public String TeacherAmount_W_Feed{get;set;}
        public String OrderPayment_Frequency{get;set;}

        public wrapResult(String pTeacherName, String pTeacherSchool, Decimal pTeacherAmount_W_Feed, String pOrderPayment_Frequency) {
            TeacherName = pTeacherName;
            TeacherSchool = pTeacherSchool;
            TeacherAmount_W_Feed = decimalToStringCurrency(pTeacherAmount_W_Feed);
            OrderPayment_Frequency = pOrderPayment_Frequency;
        }
    }

    public static String decimalToStringCurrency (Decimal scope) {
        String result = '0.00';
        if(scope != NULL) {
            List<String> args = new String[]{'0','number','#,###.00'};
            result = String.format(scope.format(), args);
            if(result.contains('.')) {
                if(result.split('\\.')[1].length()==1) {
                    result += '0';
                }
            } else {
                result += '.00';
            }
        }
        return result;
    }
}