@RestResource(urlMapping='/orderwebhook/*')
global without sharing class OrderWebhookWs {
    
  @HttpPost
  global static void doPost() {
    RestRequest req = RestContext.request;
    JSONParser parser;
    OrderSubmissionRequest request;
    Order_Cloud_Sync__c orderSync;

    System.debug(req.requestBody.toString());

    parser = JSON.createParser(req.requestBody.toString());

    request = (OrderSubmissionRequest) parser.readValueAs(
      OrderSubmissionRequest.class);
    if(request.Response != Null)
     {
    orderSync = new Order_Cloud_Sync__c(
     
      Order_Id__c = request.Response.Body.ID,
      Request_Body__c = JSON.serialize(request.Response.Body));

    insert orderSync;
    }
  }

  public class OrderSubmissionRequest {
    public OrderSubmissionResponse Response { get; set; }
  }

  public class OrderSubmissionResponse {
    public OrderCloudOrder Body { get; set; }
  }
}