@isTest(seeAllData = true)
public with sharing class TestUtils {

  // Continuous integration

  private static Integer uniqueNumber = 0;
  private static Profile standardProfile;
  private static Profile sysAdminProfile;
  private static Profile communityProfile;

  private static Set<Schema.DisplayType> stringDisplayTypeSet = new Set<Schema.DisplayType>{Schema.DisplayType.anytype,
        Schema.DisplayType.Combobox,
        Schema.DisplayType.MultiPicklist,
        Schema.DisplayType.Picklist,
        Schema.DisplayType.String,
        Schema.DisplayType.TextArea,
        Schema.DisplayType.URL};
private static Set<Schema.DisplayType> decimalDisplayTypeSet = new Set<Schema.DisplayType>{Schema.DisplayType.Currency,
        Schema.DisplayType.Double,
        Schema.DisplayType.Percent};
private static Set<Schema.DisplayType> numberDisplayTypeSet = new Set<Schema.DisplayType>{Schema.DisplayType.Integer};
private static Set<Schema.DisplayType> emailDisplayTypeSet = new Set<Schema.DisplayType>{Schema.DisplayType.Email};
private static Set<Schema.DisplayType> phoneDisplayTypeSet = new Set<Schema.DisplayType>{Schema.DisplayType.Phone};
private static Set<Schema.DisplayType> dateDisplayTypeSet = new Set<Schema.DisplayType>{Schema.DisplayType.Date};
private static Set<Schema.DisplayType> dateTimeDisplayTypeSet = new Set<Schema.DisplayType>{Schema.DisplayType.DateTime};
private static Set<Schema.DisplayType> booleanDisplayTypeSet = new Set<Schema.DisplayType>{Schema.DisplayType.Boolean};
private static Set<Schema.DisplayType> referenceDisplayTypeSet = new Set<Schema.DisplayType>{Schema.DisplayType.Reference};

  public static Id getRecordType(String sObjName, String recName) {
    List<SObject> soList = [SELECT ID FROM RecordType where Name = :recName and SObjectType = :sObjName];
    return soList[0].Id;
  }

  public static Id getRecordTypeIdByDeveloperName(String objectName,
      String rtName) {
    return [select Id
            from RecordType
            where DeveloperName = :rtName
            and SObjectType = :objectName].Id;
  }

  public static AcctSeed__Project__c createProject() {
    return new AcctSeed__Project__c(
      Name = 'Project' + getUnique());
  }

  public static npe03__Recurring_Donations_Settings__c
      createRecurringDonationSettings(Id recordTypeId) {
    return new npe03__Recurring_Donations_Settings__c(
      npe03__Record_Type__c = recordTypeId,
      npe03__Opportunity_Forecast_Months__c = 12);
  }

  public static donation_split__Designation__c createClassroom(Id teacherId, Id recipientId) {
    return new donation_split__Designation__c(
      Teacher__c = teacherId,
      Recipient__c = recipientId,
      How_will_you_spend_your_funds__c = 'This is my really long "how will I spend my funds" response, I really hope that it is 100 characters long.',
      Description__c = 'MORE Test Description area about my class. Bacon ipsum dolor amet chicken beef ribs shoulder, porchetta cow ground round beef doner tri-tip andouille sausage pork sirloin cupim short loin. Meatloaf ham ribeye beef ribs pancetta cow biltong salami filet mignon drumstick cupim doner pork loin. Meatloaf boudin bacon salami ball tip alcatra. Spare ribs pork short ribs flank. Pig salami fatback, bacon ground round ribeye pancetta chuck porchetta. Bacon ipsum dolor amet chicken beef ribs shoulder, porchetta cow ground round beef doner tri-tip andouille sausage pork sirloin cupim short loin. Meatloaf ham ribeye beef ribs pancetta cow biltong salami filet mignon drumstick cupim doner pork loin. Meatloaf boudin bacon salami ball tip alcatra. Spare ribs pork short ribs flank. Pig salami fatback, bacon ground round ribeye pancetta chuck porchetta.',
      Name = 'Test' + getUnique());
  }

  public static Campaign createCampaign(Id classroomId) {
    return new Campaign(
      Name = 'Test' + getUnique(),
      Classroom__c = classroomId);
  }

  public static Account createAccount() {
    return new Account(
      Name = 'Test' + getUnique(),
      Account_Status__c = 'Active');
  }

  public static OrderCloudLineItem createOrderCloudOrderLineItem() {
    OrderCloudLineItem li = new OrderCloudLineItem();
    OrderCloudLineItem.xp lixp = new OrderCloudLineItem.xp();
    li.product = new OrderCloudProduct();
    li.ID = 'test';
    li.ProductID = 'test';
    li.Quantity = 5;
    li.UnitPrice = 10;
    
    lixp.Description = 'test description';
    lixp.PunchoutName = 'Testname';
    lixp.SupplierPartID = 'testId';
    lixp.SupplierPartAuxiliaryID = 'test1';
    lixp.vendorOrderId = new list<string>();
    lixp.vendorOrderId.add('test');
    li.xp = lixp;
   // li.Product = new li.OrderCloudProduct();
    li.Product.Id = 'Test1';
    li.Product.Name = 'Test1';
    li.product.Description = 'test description';
    OrderCloudProduct.Xp opxp = new OrderCloudProduct.xp();
    opxp.VendorName = 'test vendor';
    opxp.SecondaryItemNo = '1233';
    li.product.xp = opxp;
    return li;
  }
  public static OrderCloudSpendingAccount createOrderCloudSpendingAccount()
  {
      OrderCloudSpendingAccount osa = new OrderCloudSpendingAccount();
      osa.Id = 'orderid';
      osa.Name = 'OrderName';
      osa.Balance = 10.12;
      osa.AllowAsPaymentMethod = true;
      osa.startdate = date.today();
      osa.enddate = date.today();
      return osa;
  }
  public static OrderCloudOrder createOrderCloudOrder(Id userId) {
    OrderCloudOrder oco = new OrderCloudOrder();

    oco.ID = 'orderid';
    oco.DateCreated = Datetime.now();
    oco.DateSubmitted = Datetime.now();
    oco.Total = 100;
    oco.TaxCost = 0;
    oco.Subtotal = 100;
    oco.FromUserID = userId;
    oco.PromotionDiscount = 0;

    return oco;
  }

  public static Order_Cloud_Sync__c createOrderCloudSync(Id userId) {
    return new Order_Cloud_Sync__c(
      Order_Id__c = 'test',
      Request_Body__c = JSON.serialize(createOrderCloudOrder(userId)));
  }

  public static Contact createContact(Id acctId) {
    return new Contact(
      FirstName = 'TestFirst' + getUnique(),
      LastName = 'TestLast' + getUnique(),
      AccountId = acctId);
  }

  public static Shop_Settings__c createShopSettings() {
    return new Shop_Settings__c(
      Hash_Key__c  = 'test key',
      Pricebook_Id__c = Test.getStandardPricebookId());
  }

  public static Opportunity createOpportunity(Id accountId) {

    return new Opportunity(
      Name = 'Test' + getUnique(),
      AccountId = accountId,
      CloseDate = Date.today(),
      //RecordTypeId = rt.id,
      StageName = 'Pending');
  }

  public static pymt__PaymentX__c createPayment() {
    return new pymt__PaymentX__c(
      Name = 'Test' + getUnique()
      );
  }

  public static Objects_To_Describe__c createObjectToDescribe(
      String objectName) {
    return new Objects_To_Describe__c(
      Name = objectName);
  }

  public static Page_Layout_Settings__c createPageLayoutSettings() {
    return new Page_Layout_Settings__c(
      Instance_Url__c = 'https://www.salesforce.com');
  }

   public static Application_Display_Field_Logic__c
      createApplicationDisplayLogic(
        String objectName, String controllingField, String dependedField,
          String values) {
    return new Application_Display_Field_Logic__c(
      ControllingField_Object__c = objectName,
      ControllingField_API__c = controllingField,
      DependentField_API__c = dependedField,
      ControllingField_Value__c = values,
      Controlling_Field_Match_Type__c = 'Equals',
      Controlling_Field_Type__c = 'Text/Picklist');
  }

  @future
  public static void createTestUser(Id contactId) {
    User u = createCommunityUser();
    u.ContactId = contactId;
    insert u;
  }

  public static User createSystemAdministrator() {
    if (sysAdminProfile == null) {
      querySystemAdminProfile();
    }
    UserRole ur = [Select Id,developername From UserRole where developername = 'Administrator' limit 1];
    return new User(
      userRoleId = ur.Id,
      FirstName = 'TestStandard',
      LastName = 'TestUser',
      Email = 'test@test.com',
      Username = 'test@test' + String.valueOf(Math.random() * 100) + '.com',
      Alias = 'su',
      CommunityNickname = 'su' + String.valueOf(Math.random() * 100),
      ProfileId = sysAdminProfile.Id,
      LocaleSidKey = 'en_US',
      LanguageLocaleKey = 'en_US',
      EmailEncodingKey = 'ISO-8859-1',
      TimeZoneSidKey = 'America/Los_Angeles');
  }

  public static User createStandardUser() {
    if (standardProfile == null) {
      queryStandardProfile();
    }
     UserRole ur = [Select PortalType, PortalAccountId From UserRole where PortalType =:'CustomerPortal' limit 1];
    return new User(
     UserRoleId = ur.Id,
      FirstName = 'TestStandard',
      LastName = 'TestUser',
      Email = 'test@test.com',
      Username = 'test@test' + String.valueOf(Math.random() * 100) + '.com',
      Alias = 'su',
      CommunityNickname = 'su' + String.valueOf(Math.random() * 100),
      ProfileId = standardProfile.Id,
      LocaleSidKey = 'en_US',
      LanguageLocaleKey = 'en_US',
      EmailEncodingKey = 'ISO-8859-1',
      TimeZoneSidKey = 'America/Los_Angeles');
  }

  public static User createCommunityUser() {
    if (communityProfile == null) {
      queryCommunityProfile();
    }

    return new User(
      FirstName = 'TestStandard',
      LastName = 'TestUser',
      Email = 'test@test.com',
      Username = 'test@test' + String.valueOf(Math.random() * 100) + '.com',
      Alias = 'su',
      CommunityNickname = 'su' + String.valueOf(Math.random() * 100),
      ProfileId = communityProfile.Id,
      LocaleSidKey = 'en_US',
      LanguageLocaleKey = 'en_US',
      EmailEncodingKey = 'ISO-8859-1',
      TimeZoneSidKey = 'America/Los_Angeles');
  }

  private static void querySystemAdminProfile() {
    List<Profile> profiles;
    Set<String> profileNames =
      new Set<String>{'System Administrator'};

    profiles = [select Id, Name
                    from Profile
                    where Name in :profileNames
                    order by Name];

    sysAdminProfile = profiles[0];
  }

  private static void queryStandardProfile() {
    List<Profile> profiles;
    Set<String> profileNames =
      new Set<String>{'Standard User'};

    profiles = [select Id, Name
                    from Profile
                    where Name in :profileNames
                    order by Name];

    standardProfile = profiles[0];
  }
  
  @future
    public static void insertUser() {
        queryStandardProfile();
        UserRole r = [SELECT Id FROM UserRole WHERE Name='Administrator'];
        User futureUser = new User(firstname = 'Future', lastname = 'User',
            alias = 'future', defaultgroupnotificationfrequency = 'N',
            digestfrequency = 'N', email = 'test@test.org',
            emailencodingkey = 'UTF-8', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = standardProfile.Id, 
            timezonesidkey = 'America/Los_Angeles',
            username = 'futureuser@test.org',
            userpermissionsmarketinguser = false,
            userpermissionsofflineuser = false, userroleid = r.Id);
        insert(futureUser);
    }

  private static void queryCommunityProfile() {
    List<Profile> profiles;
    Set<String> profileNames =
      new Set<String>{'AAC - School Community Plus Login User'};

    profiles = [select Id, Name
                    from Profile
                    where Name in :profileNames
                    order by Name];

    communityProfile = profiles[0];
  }

  public static Id getPermissionSetIdByName(String pName) {
    return [select Id
            from PermissionSet
            where Name = :pName].Id;
  }

  public static String getUnique() {
    return String.valueOf(uniqueNumber++) + '_' + System.now().millisecond();
  }

  public static Object getTestDataForType(Schema.DisplayType fieldType) {

    Object fieldTestData;

    // Data isn't complicated or random yet. Will update as the need arises.
    if (stringDisplayTypeSet.contains(fieldType)) {
      fieldTestData = 'TestString';
    } else if (decimalDisplayTypeSet.contains(fieldType)) {
      fieldTestData = 12.34;
    } else if (numberDisplayTypeSet.contains(fieldType)) {
      fieldTestData = 1234;
    } else if (emailDisplayTypeSet.contains(fieldType)) {
      fieldTestData = 'test@modacto.com';
    } else if (phoneDisplayTypeSet.contains(fieldType)) {
      fieldTestData = '15555555555';
    } else if (dateDisplayTypeSet.contains(fieldType)) {
      fieldTestData = Date.today();
    } else if (dateTimeDisplayTypeSet.contains(fieldType)) {
      fieldTestData = DateTime.now();
    } else if (booleanDisplayTypeSet.contains(fieldType)) {
      fieldTestData = true;
    } else if (referenceDisplayTypeSet.contains(fieldType)) {
      fieldTestData = null;
    } else {
      fieldTestData = 'test';
    }

  return fieldTestData;
  }
}