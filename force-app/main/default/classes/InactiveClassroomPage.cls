/**
 * @description Automated process to deactivate classrooms
 */
public class InactiveClassroomPage implements Database.Batchable<sObject>, schedulable {
    
    String schoolRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('School Admin').getRecordTypeId();
    String teacherRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Teacher').getRecordTypeId();

    /**
     * @description Batch Start Process
     * @param bc Batch context
     * @return List of school and teacher users with more than 6 months without login
     */
    public List<User> start(Database.BatchableContext bc) {
        
        if(Test.isRunningTest()){
            return [SELECT ID, ContactId, Contact.RecordTypeId, LastLoginDate FROM User WHERE ContactId != NULL AND (Contact.RecordTypeId = :schoolRT OR Contact.RecordTypeId = :teacherRT)];
        } else {
            return [SELECT ID, ContactId, Contact.RecordTypeId, LastLoginDate FROM User WHERE ContactId != NULL AND (Contact.RecordTypeId = :schoolRT OR Contact.RecordTypeId = :teacherRT) AND (NOT (LastLoginDate = LAST_N_MONTHS:5 OR LastLoginDate = THIS_MONTH)) AND LastLoginDate > 2020-11-01T00:00:00Z AND ContactId IN (SELECT Teacher__c FROM donation_split__Designation__c WHERE donation_split__Active__c = TRUE) ];
        }
        
    }

    /**
     * @description Batch Excecute Process
     * @param bc Batch context
     * @param scope List of school and teacher users with more than 6 months without login
     */
    public void execute(Database.BatchableContext bc, List<User> scope) {
        
        Map<Id, DateTime> lastLoginMap = new Map<Id, DateTime>();
        for(User user : scope){
            lastLoginMap.put(user.ContactId, user.LastLoginDate);
        }
        Date twoYearsAgo = system.today().addYears(-2);

        List<donation_split__Designation__c> classrooms = new List<donation_split__Designation__c>();
        for(donation_split__Designation__c classroom : [SELECT Hide_My_Page_from_Search_Results_Online__c, Inactive_Classroom_Page__c, Teacher__c FROM donation_split__Designation__c WHERE Teacher__c IN :lastLoginMap.keySet() AND donation_split__Active__c = true]) {
            
            classroom.Inactive_Classroom_Page__c = true;
            
            if(lastLoginMap.get(classroom.Teacher__c) < twoYearsAgo) {
                classroom.Hide_My_Page_from_Search_Results_Online__c = true;
            }
            classrooms.add(classroom);
        }

        if(!classrooms.isEmpty()) {
            update classrooms;
        }
    }

    /**
     * @description Batch Finish Process
     * @param bc Batch context
     */
    public void finish(Database.BatchableContext bc) {
        //Finish Context
    }

    /**
     * @description Batch Finish Process
     * @param sc Batch context
     */
    public void execute(SchedulableContext sc) {
        database.executebatch(new InactiveClassroomPage());
    }
}