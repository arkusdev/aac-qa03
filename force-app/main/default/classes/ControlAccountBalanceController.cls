public class ControlAccountBalanceController {
    public Control_Account_Balance__c record {get; set;}
    
    public ControlAccountBalanceController() {
        record = new Control_Account_Balance__c();
    }
    
    public void executeJob() {

        if (record.Type__c == 'Accounts Payable') {
            delete [SELECT Id From Control_Account_Balance__c WHERE Accounting_Period__c = :record.Accounting_Period__c AND Type__c = 'Accounts Payable'];
            Database.executeBatch(new APControlAccountBalanceBatch(record.Accounting_Period__c), 2000);
        }
        else  {
            delete [SELECT Id From Control_Account_Balance__c WHERE Accounting_Period__c = :record.Accounting_Period__c AND Type__c = 'Accounts Receivable'];
            Database.executeBatch(new ARControlAccountBalanceBatch(record.Accounting_Period__c), 2000);
        }
    }
}