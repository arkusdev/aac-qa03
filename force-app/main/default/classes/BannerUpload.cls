public without sharing class BannerUpload {
    
    @AuraEnabled
	public static String getBanner() {
        User usr = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];
		
		StaticResource staticResource = [SELECT Name, SystemModStamp FROM StaticResource WHERE Name = 'donor_community' LIMIT 1];
        String bannerURL = URL.getCurrentRequestUrl().toExternalForm().split('sfsites')[0] + 'resource/' + String.valueOf(((DateTime)staticResource.get('SystemModStamp')).getTime()) + '/' + staticResource.get('Name') + '/default_banner.jpg';
        List<ContentDistribution> cdistribution = [SELECT Name, RelatedRecordId, ContentDownloadUrl, DistributionPublicUrl FROM ContentDistribution WHERE RelatedRecordId = :usr.ContactId AND Name LIKE '%banner%'];
        if(!cdistribution.isEmpty()) {
            bannerURL = cdistribution[0].ContentDownloadUrl;
        }
        
        return bannerURL;
    }

    @AuraEnabled
	public static void setBanner(String base64Data, String fileName) {
        User usr = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];

        List<ContentDocumentLink> cdls =[SELECT Id,ContentDocumentId,LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :usr.ContactId AND ContentDocument.Title LIKE '%banner%'];
		if(base64Data != '') {
			ContentVersion banner = new ContentVersion();
			banner.PathOnClient = 'banner.' + fileName.split('\\.')[1];
			banner.Title = 'banner.' + fileName.split('\\.')[1];
			banner.VersionData = EncodingUtil.base64Decode(base64Data);
			
			if(!cdls.isEmpty()) {
				banner.ContentDocumentId = cdls[0].ContentDocumentId;
			}
			if(!Test.isRunningTest()) {
				insert banner;
			}
			
			
			if(cdls.isEmpty()) {
				ContentDocumentLink cdl = new ContentDocumentLink();
				cdl.LinkedEntityId = usr.ContactId;
				cdl.ShareType = 'I';
				
				if(!Test.isRunningTest()) {
					cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: banner.Id].ContentDocumentId;
					insert cdl;
				}
			}
		}
    }
}