public with sharing class ChargentDonorDetailsController {
    
  @AuraEnabled
  public static ChargentOrders__ChargentOrder__c savePaymentData(String donorComment, Contact c, Account a, ChargentOrders__ChargentOrder__c payment, String cartItemString, String inHonorOfName, String inHonorOfEmail, String paymentPromoCode,  Decimal subTotal, Decimal total, Decimal monthlySubTotal, Decimal grandTotal, Decimal expFees, Decimal monthlyTotal, Decimal fee, Decimal helpAmount, Decimal monthlyFee,Boolean isProcessingFeeIncluded, String myPicklist, Date endDate) {
    return DonorUtils.savePaymentData(donorComment,c, a, payment, cartItemString, inHonorOfName, inHonorOfEmail, paymentPromoCode, subTotal, total, monthlySubTotal, grandTotal, expFees, monthlyTotal, fee, helpAmount, monthlyFee, isProcessingFeeIncluded, myPicklist, endDate);
  }
    
    @AuraEnabled
    public static Contact upsertContact(Contact c) {
        return DonorUtils.upsertContact(c);
    }
    
    @AuraEnabled
    public static Account upsertAccount(Account a, String donorType) {
        return DonorUtils.upsertAccount(a, donorType);
    }
    
    @AuraEnabled
    public static List<FAQ__c> getFAQs() {
        return DonorUtils.getFAQs();
    }
    
    private static Map<Id, donation_split__Designation__c> getClassroomToCampaignIdMap(Set<Id> classroomIds) {
        return DonorUtils.getClassroomToCampaignIdMap(classroomIds);
    } 
    
    @AuraEnabled
    public static List<String> getTributeType() {
        return DonorUtils.getTributeType();
    }

    public static String formatDate(Date input) {
        return DonorUtils.formatDate(input);
    }
}