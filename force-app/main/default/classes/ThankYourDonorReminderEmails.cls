/**
 * @description Automated process to notify teachers that haven’t logged in for some time that their page will go inactive
 */
public class ThankYourDonorReminderEmails implements Database.Batchable<sObject>, schedulable {

    /**
     * @description Batch Start Process
     * @param bc Batch context
     * @return Opportunities that didnt thank donor in the last 30, 60 or 90 days and are from a certain record type
     */
    public List<Opportunity> start(Database.BatchableContext bc) {
        return ThankYourDonorReminderEmailsHandler.getOpportunities();
    }

    /**
     * @description Batch Excecute Process
     * @param bc Batch context
     * @param scope List of opportunities that didnt thank donor in the last 30, 60 or 90 days and are from a certain record type
     */
    public void execute(Database.BatchableContext bc, List<Opportunity> scope) {
        ThankYourDonorReminderEmailsHandler.sendEmail(scope);
    }

    /**
     * @description Batch Finish Process
     * @param bc Batch context
     */
    public void finish(Database.BatchableContext bc) {}

    /**
     * @description Batch Finish Process
     * @param sc Batch context
     */
    public void execute(SchedulableContext sc) {
        database.executebatch(new ThankYourDonorReminderEmails());
    }
}