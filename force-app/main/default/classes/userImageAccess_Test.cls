@isTest 
public class userImageAccess_Test {
    static testMethod void insertNewUser() {
    
       Account portalAccount = new Account(name = 'testAccount');
       insert portalAccount;
    
       Contact portalContact = new contact(LastName = 'testContact', AccountId = portalAccount.Id);
       insert portalContact;
           
       User portalUser = new User();
       
       portalUser.FirstName = 'David';
       portalUser.LastName  = 'Liu';
       portalUser.Email     = 'dvdkliu+aac@gmail.com';
       portalUser.Username  = 'aac-dreamer@gmail.com';
       portalUser.Alias     = 'fatty';
       portalUser.ProfileId = '00eC00000012HXVIA2';
       portalUser.UserPreferencesShowProfilePicToGuestUsers = true;
       portalUser.ContactId = portalContact.Id;
       portalUser.TimeZoneSidKey    = 'America/Denver';
       portalUser.LocaleSidKey      = 'en_US';
       portalUser.EmailEncodingKey  = 'UTF-8';
       portalUser.LanguageLocaleKey = 'en_US';
       
       insert portalUser;
     
    }
}