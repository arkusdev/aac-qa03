public with sharing class Teacher_Header {
    
    @AuraEnabled
    public static data getData(){
        try {
            String classroomId = '';
            String schoolId = '';
            User usr = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];
            List<donation_split__Designation__c> classrooms = [SELECT Id, School__c FROM donation_split__Designation__c WHERE Teacher__c = :usr.ContactId];
            if(!classrooms.isEmpty()) {
                classroomId = classrooms[0].Id;
                schoolId = classrooms[0].School__c != null ? classrooms[0].School__c : null;
            }

            Teacher_Header.data data = new Teacher_Header.data(classroomId, schoolId);

            return data;

        } catch (Exception ex) {
           throw new AuraHandledException(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') ? ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')[1].split(':')[0] : ex.getMessage());
        }
    }

    public class data { 
        @AuraEnabled public String classroomId { get; set; }
        @AuraEnabled public String schoolId { get; set; }

        public data (String pClassroomId, String pSchoolId) {
            classroomId = pClassroomId;
            schoolId = pSchoolId;
        }
    }
}