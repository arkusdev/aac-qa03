@IsTest
public class CartControllerTest {
    
    @testSetup static void initTestData() {

        AcctSeed__GL_Account__c accGL = new AcctSeed__GL_Account__c(Name='1040 Bremer Checking - Restricted', AcctSeed__Sub_Type_1__c='Assets');
        AcctSeed__GL_Account__c accGL2 = new AcctSeed__GL_Account__c(Name='4400 Individual Contributions', AcctSeed__Sub_Type_1__c='Assets');
        AcctSeed__GL_Account__c accGL3 = new AcctSeed__GL_Account__c( Name = '1050-Unapplied Cash', AcctSeed__Active__c = true, AcctSeed__Type__c = 'Balance Sheet', AcctSeed__Sub_Type_1__c = 'Assets', AcctSeed__Sub_Type_2__c = 'Cash' );
        AcctSeed__GL_Account__c accGL4 = new AcctSeed__GL_Account__c(Name='4599 Teacher Restricted Released', AcctSeed__Sub_Type_1__c='Assets', AcctSeed__Bank__c = true, AcctSeed__Active__c = true,AcctSeed__Type__c = 'Revenue');
        
        insert accGL;
        insert accGL2;
        insert accGL3;
        insert accGL4;
        
        Account account = new Account();
        account.Name = 'Test Account';
        account.Account_Status__c = 'Active';
		insert account;

        Account school =  new Account();
        school.Name = 'Test School';
        school.Account_Status__c = 'Active';
		school.ShippingStreet = '123 Main Street';
		school.ShippingCity = 'Minneapolis';
		school.ShippingStateCode = 'MN';
		school.ShippingCountryCode = 'US';
		school.ShippingPostalCode = '12345';
		insert school;
		
        Contact contact = new Contact();
        contact.FirstName = 'Test First';
        contact.LastName = 'Test Last';
        contact.AccountId = account.Id;
		contact.School__c = school.Id;
		insert contact;

		//insert portal user
		User user1 = new User(
			Username = 'testamc@test.acm',
			ContactId = contact.Id,
			ProfileId = [SELECT Id FROM Profile WHERE Name = 'AAC - Teacher Community Plus Login User' Limit 1].Id,
			Alias = 'test123',
			Email = 'test12345@test.com',
			EmailEncodingKey = 'UTF-8',
			LastName = 'Qtest',
			CommunityNickname = 'test12345',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'en_US',
			LanguageLocaleKey = 'en_US'
		);
		insert user1;
	
        donation_split__Designation__c classroom = new donation_split__Designation__c();
        classroom.Teacher__c = contact.Id;
        classroom.Recipient__c = school.Id;
        classroom.School__c = school.Id;
        classroom.How_will_you_spend_your_funds__c = 'This is my really long "how will I spend my funds" response, I really hope that it is 100 characters long.';
        classroom.Description__c = 'MORE Test Description area about my class. Bacon ipsum dolor amet chicken beef ribs shoulder, porchetta cow ground round beef doner tri-tip andouille sausage pork sirloin cupim short loin. Meatloaf ham ribeye beef ribs pancetta cow biltong salami filet mignon drumstick cupim doner pork loin. Meatloaf boudin bacon salami ball tip alcatra. Spare ribs pork short ribs flank. Pig salami fatback, bacon ground round ribeye pancetta chuck porchetta. Bacon ipsum dolor amet chicken beef ribs shoulder, porchetta cow ground round beef doner tri-tip andouille sausage pork sirloin cupim short loin. Meatloaf ham ribeye beef ribs pancetta cow biltong salami filet mignon drumstick cupim doner pork loin. Meatloaf boudin bacon salami ball tip alcatra. Spare ribs pork short ribs flank. Pig salami fatback, bacon ground round ribeye pancetta chuck porchetta.';
        classroom.Name = 'Test Classroom';
		Insert classroom;
        
    }

    @IsTest
    public static void TestCartInit() {
        
        Test.startTest();
        CartController.Cart cart = CartController.cartInit();
        Test.stopTest();

        System.assertNotEquals(NULL, cart, 'It should load the Cart objact structure');
    }

    @IsTest
    public static void TestCartSubmit() {

        donation_split__Designation__c classroomData = [SELECT TeacherName__c, SchoolName__c FROM donation_split__Designation__c LIMIT 1];
        
        CartController.Classroom classroom = new CartController.Classroom();
        classroom.classroomId = classroomData.Id;
        classroom.teacherName = classroomData.TeacherName__c;
        classroom.schoolName = classroomData.SchoolName__c;
        classroom.recordTypeName = 'Classroom Registration';

        List<CartController.CartItem> cartItems = new List<CartController.CartItem>();
        CartController.CartItem cartItem = new CartController.CartItem();
        cartItem.classroom = classroom;
        cartItem.amount = 100.00;
        cartItem.type = 'generalFund';
        cartItems.add(cartItem);

        CartController.Donor donor = new CartController.Donor();
        donor.firstName = 'Test Donor';
        donor.lastName = 'Test Donor Last';
        donor.email = 'test@unittest.com';
        donor.phone = '';
        donor.suscribed = true;
        donor.annualReport = true;
        donor.anonymous = false;
        donor.donorType = 'Business';
        donor.displayName = 'Test Display Donor';

        CartController.Payment payment = new CartController.Payment();
        payment.addressLine1 = 'Test address line 1';
        payment.addressLine2 = 'Test address line 2';
        payment.city = 'New York';
        payment.state = 'New York';
        payment.zip = '10018';
        payment.cardName = 'Test Donor';
        payment.cardNumber = '4111111111111111';
        payment.cardCVV = '111';
        payment.cardExpMM = '12';
        payment.cardExpYYYY = '2050';

        CartController.Cart cart = new CartController.Cart();
        cart.step = '3';
        cart.cartItems = cartItems;
        cart.fee = 15.00;
        cart.helpAmount = 10.00;
        cart.total = 125.00;
        cart.oneTimeDonation = true;
        cart.monthlyDonation = false;
        cart.endDonationDate = null;
        cart.comment = 'This is a comment';
        cart.donor = donor;
        cart.payment = payment;
        
        Test.startTest();
        String orderId = CartController.cartSubmit(JSON.serialize(cart));
        Test.stopTest();


        CartController.paymentRequest(orderId, JSON.serialize(cart));
        CartController.gTMData gtmData = CartController.getGTMData(orderId);
        
    }
}