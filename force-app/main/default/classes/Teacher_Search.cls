/**
 * @description Teacher_Search controller for aura component Teacher_Search
 */
global with sharing class Teacher_Search {
    private static final Id SCHOOL_RECORD_TYPE = [SELECT Id FROM RecordType WHERE Name = 'School' LIMIT 1].Id;

    /**
     * @description getRecordType
     * @return the school recordtype
     */
    @AuraEnabled
    global static string getRecordType(){
        return (string) SCHOOL_RECORD_TYPE;
    }

    /**
     * @description searchSchool
     * @param parametersToSearch the parameters to filter the search
     * @return all the results found
     */
    @AuraEnabled
    global static List<Data> searchSchool(String parametersToSearch){
        List<Data> schoolToReturn = new List<Data>();
        try{
            ParametersWrapper pWrapper = (ParametersWrapper)JSON.deserialize(parametersToSearch, ParametersWrapper.class);


            String sqlQuery = 'SELECT Id, Name, ShippingAddress, ShippingCity, ShippingState, ShippingPostalCode, ShippingStreet  FROM Account WHERE ';
            Boolean addAnd = false;

            if (String.isNotEmpty(pWrapper.name)){
                sqlQuery += ' Name LIKE \'%' + pWrapper.name + '%\'';
                addAnd = true;
            }

            if (String.isNotEmpty(pWrapper.state)){
                sqlQuery += addAnd ? ' AND ShippingState LIKE \'%' + pWrapper.state + '%\'' : ' ShippingState  LIKE \'%' + pWrapper.state + '%\'';
                addAnd = true;
            }

            if (String.isNotEmpty(pWrapper.city)){
                sqlQuery += addAnd ? ' AND ShippingCity LIKE \'%' + pWrapper.city + '%\'' : ' ShippingCity LIKE \'%' + pWrapper.city + '%\'';
                addAnd = true;
            }

            if (String.isNotEmpty(pWrapper.zip)){
                sqlQuery += addAnd ? ' AND ShippingPostalCode LIKE  \'' + pWrapper.zip + '%\'' : ' ShippingPostalCode LIKE  \'%' + pWrapper.zip + '%\'';
            }

            sqlQuery += ' AND RecordTypeId = \'' + SCHOOL_RECORD_TYPE + '\' AND Account_Status__c = \'Active\' ORDER BY Name ASC LIMIT 5000';

            List<Account> results = Database.query(sqlQuery);

            for(Account acc: results){
                Data classRoom = new Data();
                classRoom.name = '';
                classRoom.schoolName = acc.Name;
                classRoom.id = '';
                classRoom.schoolId = acc.Id;
                if(acc.ShippingAddress != null){
                    classRoom.addressOne = acc.ShippingStreet;
                    classRoom.addressTwo = acc.ShippingCity + ', ' + acc.ShippingState + ' ' + acc.ShippingPostalCode;
                }
                schoolToReturn.add(classRoom);
            }
        
        } catch(Exception e){
            System.debug(LoggingLevel.ERROR, e.getMessage() + '. ' + e.getStackTraceString());
            throw new AuraException(e.getMessage() + '. ' + e.getStackTraceString());
        }

        return schoolToReturn;
    }

    /**
     * @description saveSchool
     * @param accId the account id
     * @param classId the classroom id
     */
    @AuraEnabled
    global static void saveSchool(String accId, String classId){
        try{
            List<donation_split__Designation__c> classrooms = [SELECT Id, School__c, Name FROM donation_split__Designation__c WHERE Id = :classId LIMIT 1];
            if(!classrooms.isEmpty()){
                classrooms[0].School__c = accId;
                update classrooms;
            }
            User usr = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];
            if(usr.ContactId != null) {
                List<Contact> teachers = [SELECT School__c FROM Contact WHERE ID = :usr.ContactId];
                if(!teachers.isEmpty()) {
                    teachers[0].School__c = accId;
                    update teachers;
                }
            }
        }catch(Exception e){
            System.debug(LoggingLevel.ERROR, e.getMessage() + '. ' + e.getStackTraceString());
            throw new AuraException(e.getMessage() + '. ' + e.getStackTraceString());
        }
    }

    /**
     * @description getData
     * @return wrapper class with the needed information
     */
    @AuraEnabled
    global static Data getClassroom(){
        
        try {
            Data data = new Data();
            data.schoolRT = SCHOOL_RECORD_TYPE;
            User usr = [SELECT ContactId, Contact.School__c, Contact.School__r.Name, Contact.Name, Contact.School__r.ShippingCity, Contact.School__r.ShippingState, Contact.School__r.ShippingPostalCode, Contact.School__r.ShippingStreet FROM User WHERE Id = :UserInfo.getUserId()];
            List<donation_split__Designation__c> classrooms = [SELECT Id, School__c, School__r.Name, Name, School__r.ShippingCity, School__r.ShippingState, School__r.ShippingPostalCode, School__r.ShippingStreet FROM donation_split__Designation__c WHERE Teacher__c = :usr.ContactId LIMIT 1];
            if(!classrooms.isEmpty()){
                data.name = classrooms[0].Name;
                data.schoolName = classrooms[0].School__r.Name;
                data.id = classrooms[0].Id;
                data.schoolId = classrooms[0].School__c;
                if(classrooms[0].School__c != null){
                    data.addressOne = classrooms[0].School__r.ShippingStreet;
                    data.addressTwo = classrooms[0].School__r.ShippingCity + ', ' + classrooms[0].School__r.ShippingState + ' ' + classrooms[0].School__r.ShippingPostalCode;
                }
            } else if(usr.Contact.School__c != null) {
                data.schoolName = usr.Contact.School__r.Name;
                data.schoolId = usr.Contact.School__c;
                data.addressOne = usr.Contact.School__r.ShippingStreet;
                data.addressTwo = usr.Contact.School__r.ShippingCity + ', ' + usr.Contact.School__r.ShippingState + ' ' + usr.Contact.School__r.ShippingPostalCode;
            }

            return data;

        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') ? ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')[1].split(':')[0] : 'Error: ' + ex.getMessage());
        }

    }

    /**
     * @description ParametersWrapper
     */
    global class ParametersWrapper {
        /**
         * @description name
         */
        @AuraEnabled
        public String name{get;set;}

        /**
         * @description state
         */
        @AuraEnabled
        public String state{get;set;}

        /**
         * @description city
         */
        @AuraEnabled
        public String city{get;set;}

        /**
         * @description zip
         */
        @AuraEnabled
        public String zip{get;set;}
    }

    /**
     * @description Data
     */
    global class Data {
        /**
         * @description name
         */
        @AuraEnabled
        public String name{get;set;}

        /**
         * @description id
         */
        @AuraEnabled
        public String id{get;set;}

        /**
         * @description schoolName
         */
        @AuraEnabled
        public String schoolName{get;set;}

        /**
         * @description schoolId
         */
        @AuraEnabled
        public String schoolId{get;set;}

        /**
         * @description addressOne
         */
        @AuraEnabled
        public String addressOne{get;set;}

        /**
         * @description addressTwo
         */
        @AuraEnabled
        public String addressTwo{get;set;}

        /**
         * @description School RecordType
         */
        @AuraEnabled
        public String schoolRT{get;set;}
    }
}