@isTest
private class ShopUtilsTest {

  static testMethod void testCreateAddressAssignment() {
    Account a = TestUtils.createAccount();
    insert a;

    Account school = TestUtils.createAccount();
    school.ShippingStreet = '123 Main Street';
    school.ShippingCity = 'Minneapolis';
    school.ShippingStateCode = 'MN';
    school.ShippingCountryCode = 'US';
    school.ShippingPostalCode = '12345';
    insert school;

    Contact c = TestUtils.createContact(a.Id);
    c.School__c = school.Id;
    c.School__r = school;
    insert c;
        
    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());

    ShopUtils.createAddressAssignment(null, null, null);

    ShopAddress address = new ShopAddress(ShopAddress.AddressType.Shipping, c);

    ShopUtils.createAddressAssignment('token', UserInfo.getUserId(), address);

    System.assertEquals(null, ShopUtils.createAddress(null, null));

    String addressId = ShopUtils.createAddress('token', address);

    System.assertNotEquals(null, addressId);

    Test.stopTest();
  }

 static testMethod void testUpdateShippingAddress() {
    Account a = TestUtils.createAccount();
    insert a;

    Account school = TestUtils.createAccount();
    school.ShippingStreet = '123 Main Street';
    school.ShippingCity = 'Minneapolis';
    school.ShippingStateCode = 'MN';
    school.ShippingCountryCode = 'US';
    school.ShippingPostalCode = '12345';
    insert school;

    Contact c = TestUtils.createContact(a.Id);
    c.School__c = school.Id;
    c.School__r = school;
    insert c;
        
    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());

    ShopUtils.UpdateShippingAddress(null, null);

    ShopAddress address = new ShopAddress(ShopAddress.AddressType.Shipping, c);

    ShopUtils.UpdateShippingAddress('token', address);

    Test.stopTest();
  }
  
  static testMethod void testUpdateShippingAddresserr() {  
        
    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());

    ShopUtils.UpdateShippingAddress(null, null);
    Test.stopTest();
  
  }

  static testMethod void testGetCurrentUser() {
    Test.startTest();

    System.assertEquals(null, ShopUtils.getCurrentUser(null));

    User u = ShopUtils.getCurrentUser(UserInfo.getUserId());

    System.assertEquals(UserInfo.getUserId(), u.Id);
 
    Test.stopTest();
  }

  static testMethod void testHandleLineItem() {
    Shop_Settings__c settings = TestUtils.createShopSettings();
    insert settings;

    Account a = TestUtils.createAccount();
    insert a;

    AcctSeed__GL_Account__c asgl = new  AcctSeed__GL_Account__c();
     asgl.Name = '4599 Teacher Restricted Released';
     asgl.AcctSeed__Active__c = true;
     asgl.AcctSeed__Type__c = 'Revenue';
     Insert asgl;
     
    Opportunity o = TestUtils.createOpportunity(a.Id);
    o.Order_Cloud_Order_Id__c = 'test';
    insert o;

    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());

    ShopUtils.handleLineItem(null, null);

    ShopUtils.handleLineItem('test',
      JSON.serialize(TestUtils.createOrderCloudOrderLineItem()));

    Test.stopTest();
  }
  
  
static testMethod void testupdateSpendingAccount() {
    Test.startTest();
     OrderCloudSpendingAccount osac = TestUtils.createOrderCloudSpendingAccount();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());

    //System.assertEquals(null, ShopUtils.getLineItemsForOrder(null, null));

   ShopUtils.updateSpendingAccount('token', osac);

    Test.stopTest();  
  
  }
  
  static testMethod void testupdateSpendingAccountError() {
    Test.startTest();
    OrderCloudSpendingAccount osac = TestUtils.createOrderCloudSpendingAccount();
    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock(true));   

    try {
      ShopUtils.updateSpendingAccount(null, osac);
    } catch (Exception e) {

    } 

    Test.stopTest();
  }

static testMethod void testcreateSpendingAccountAssignment() {
    Test.startTest();
     OrderCloudSpendingAccount osac = TestUtils.createOrderCloudSpendingAccount();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());

    //System.assertEquals(null, ShopUtils.getLineItemsForOrder(null, null));

   ShopUtils.createSpendingAccountAssignment('token',userinfo.getUserId(), osac.Id);

    Test.stopTest();  
  
  }
  
  static testMethod void testcreateSpendingAccountAssignmentError() {
    Test.startTest();
     OrderCloudSpendingAccount osac = TestUtils.createOrderCloudSpendingAccount();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock(true));   

    try {
      ShopUtils.createSpendingAccountAssignment(null,userinfo.getUserId(), osac.Id);
    } catch (Exception e) {

    } 

    Test.stopTest();
  }

static testMethod void testcreateSpendingAccount() {
    Test.startTest();
     OrderCloudSpendingAccount osac = TestUtils.createOrderCloudSpendingAccount();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());

    //System.assertEquals(null, ShopUtils.getLineItemsForOrder(null, null));

   ShopUtils.createSpendingAccount('token',osac);

    Test.stopTest();  
  
  }
  
  static testMethod void testcreateSpendingAccountError() {
    Test.startTest();
     OrderCloudSpendingAccount osac = TestUtils.createOrderCloudSpendingAccount();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock(true));   

    try {
      ShopUtils.createSpendingAccount(null,osac);
    } catch (Exception e) {

    } 

    Test.stopTest();
  }

  static testMethod void testCreateOpportunityFromQueue() {
    User tuser = TestUtils.createSystemAdministrator();
    insert tuser;
    
    Account a = TestUtils.createAccount();
    Contact c = TestUtils.createContact(a.id);
    User testUser = TestUtils.createCommunityUser();
    
     
     
    system.runas(tuser)
    {
    list<AcctSeed__Accounting_Variable__c> acvlist = new list<AcctSeed__Accounting_Variable__c>();
    
    AcctSeed__Accounting_Variable__c  acv1 = new AcctSeed__Accounting_Variable__c();
    acv1.AcctSeed__Type__c = 'GL Account Variable 1';
    acv1.AcctSeed__Active__c = true;
    acv1.Name = 'Unrestricted';
    
    AcctSeed__Accounting_Variable__c  acv2 = new AcctSeed__Accounting_Variable__c();
    acv2.AcctSeed__Type__c = 'GL Account Variable 2';
    acv2.AcctSeed__Active__c = true;
    acv2.Name = 'Programs';
    
    AcctSeed__Accounting_Variable__c  acv3 = new AcctSeed__Accounting_Variable__c();
    acv3.AcctSeed__Type__c = 'GL Account Variable 3';
    acv3.AcctSeed__Active__c = true;
    acv3.Name = 'Classroom Adoption';
    
    AcctSeed__Accounting_Variable__c  acv4 = new AcctSeed__Accounting_Variable__c();
    acv4.AcctSeed__Type__c = 'GL Account Variable 1';
    acv4.AcctSeed__Active__c = true;
    acv4.Name = 'Restricted';
    
    acvlist.add(acv1);
    acvlist.add(acv2);
    acvlist.add(acv3);
    acvlist.add(acv4);
    
    insert acvlist;
    
    AcctSeed__GL_Account__c asgl = new  AcctSeed__GL_Account__c();
     asgl.Name = '4599 Teacher Restricted Released';
     asgl.AcctSeed__Active__c = true;
     asgl.AcctSeed__Type__c = 'Revenue';
     Insert asgl;
    
    insert a;   
    insert c;
    
    testUser.ContactId = c.Id;
    insert testUser;
    
     
    Order_Cloud_Sync__c sync = TestUtils.createOrderCloudSync(testUser.Id);
    insert sync;   
    
    RecordType rt = [select Id,developerName from Recordtype where developerName='adoption' and sobjectType='Opportunity' limit 1];
    
    Opportunity o = TestUtils.createOpportunity(a.Id);
    o.Order_Cloud_Order_Id__c = 'test';
    o.RecordTypeId = rt.Id;
    insert o;

    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());

    ShopUtils.createOpportunityFromQueue(null);

    ShopUtils.createOpportunityFromQueue(sync);

    Test.stopTest();
    }
    
  }

  static testMethod void testProcessSyncQueue() {
    User tuser = TestUtils.createSystemAdministrator();
    insert tuser;
    system.runas(tuser)
    {
    Account a = TestUtils.createAccount();
    insert a;

    Contact c = TestUtils.createContact(a.id);
    insert c;    

    User testUser = TestUtils.createCommunityUser();
    testUser.ContactId = c.Id;
   
    insert testUser;

    Order_Cloud_Sync__c sync = TestUtils.createOrderCloudSync(testUser.Id);
    insert sync;
    
    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());

    ShopUtils.processSyncQueue();

    Test.stopTest();
    }
  }

  static testMethod void testGetLineItemsForOrderError() {
    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock(true));

    System.assertEquals(null, ShopUtils.getLineItemsForOrder(null, null));

    List<OrderCloudLineItem> lineItems;

    try {
      lineItems = ShopUtils.getLineItemsForOrder(
        'token', 'orderid');
    } catch (Exception e) {

    }

    System.assertEquals(null, lineItems);

    Test.stopTest();
  }

  static testMethod void testGetLineItemsForOrder() {
    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());

    System.assertEquals(null, ShopUtils.getLineItemsForOrder(null, null));

    List<OrderCloudLineItem> lineItems = ShopUtils.getLineItemsForOrder(
      'token', 'orderid');

    System.assertEquals(1, lineItems.size());

    Test.stopTest();
  }

  static testMethod void testGetProductError() {
    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock(true));

    System.assertEquals(null, ShopUtils.getProduct(null, null));

    OrderCloudProduct p;

    try {
      p = ShopUtils.getProduct('token', 'productid');
    } catch (Exception e) {

    }

    System.assertEquals(null, p);

    Test.stopTest();
  }

  static testMethod void testGetProduct() {
    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());

    System.assertEquals(null, ShopUtils.getProduct(null, null));

    OrderCloudProduct p = ShopUtils.getProduct('token', 'productid');

    System.assertNotEquals(null, p);

    Test.stopTest();
  }

    static testMethod void testGetAdminAuthToken() {
    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());

    String at = ShopUtils.getAdminAuthToken();

    System.assertNotEquals(null, at);

    Test.stopTest();
  }

  static testmethod void testGetAdminAuthTokenError() {
    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock(true));

    String at;

    try {
      at = ShopUtils.getAdminAuthToken();
    } catch (Exception e) {

    }

    System.assertEquals(null, at);

    Test.stopTest();
  }

  static testMethod void testProvisionUser() {
  
     Shop_Settings__c ssc = TestUtils.createShopSettings();
     Insert ssc;
     
  
    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());

    System.assertEquals(null, ShopUtils.provisionUser(null, null));

    String password = ShopUtils.provisionUser('test token',
      UserInfo.getUserId());

    System.assertNotEquals(null, password);

    Test.stopTest();
  }

  static testMethod void testProvisionUserError() {
    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock(true));

    String password;

    try {
      password = ShopUtils.provisionUser('test token',
        UserInfo.getUserId());
    } catch (Exception e) {

    }

    System.assertEquals(null, password);

    Test.stopTest();
  }

  static testMethod void addUserToGroup() {
    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());

    ShopUtils.addUserToGroup(null, null);

    ShopUtils.addUserToGroup('token', UserInfo.getUserId());

    System.assert(true);

    Test.stopTest();
  }

  static testMethod void addUserToGroupError() {
    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock(true));

    try {
      ShopUtils.addUserToGroup('token', UserInfo.getUserId());
      System.assert(false);
    } catch (Exception e) {

    }

    Test.stopTest();
  }

  static testMethod void testGetImpersonationAccessToken() {
    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());

    System.assertEquals(null,
      ShopUtils.getImpersonationAccessToken(null, null));

    String at = ShopUtils.getImpersonationAccessToken('token',
      UserInfo.getUserId());

    System.assertNotEquals(null, at);

    Test.stopTest();
  }

  static testMethod void testGetImpersonationAccessTokenError() {
    Test.startTest();

    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock(true));

    String at;

    try {
      at = ShopUtils.getImpersonationAccessToken('token',
        UserInfo.getUserId());
    } catch (Exception e) {

    }

    System.assertEquals(null, at);

    Test.stopTest();
  }
   static testMethod void testcreateProduct() {
    Test.startTest();
    OrderCloudLineItem oli = TestUtils.createOrderCloudOrderLineItem();
    Shop_Settings__c ssc = TestUtils.createShopSettings();
    Insert ssc;

    try {
      Id at = ShopUtils.createProduct(oli);
    } catch (Exception e) {

    }

    Test.stopTest();
  }
}