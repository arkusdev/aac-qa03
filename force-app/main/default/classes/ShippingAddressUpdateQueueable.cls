public with sharing class ShippingAddressUpdateQueueable implements Queueable,
    Database.AllowsCallouts {

  private static final String MAILING_ADDRESS_NAME = 'Mailing Address';
  private static final String ADDRESS_NEW_LINE = '\r\n';
  private Contact teacher;
  private Map<string,Teacher_sales_tax__c> taxmap = new map<string,Teacher_sales_tax__c>();

  public ShippingAddressUpdateQueueable(
      Contact teacher,Map<string,Teacher_sales_tax__c> taxmap) {
    this.teacher = teacher;
    if(taxmap.size() > 0)
    {
        this.taxmap.putAll(taxmap);
    }
  }
   private List<String> getSplitStreet(String street) {
    return street.split(ADDRESS_NEW_LINE);
  }
  public void execute(QueueableContext SC) {
    ShopAddress mailingAddress = new ShopAddress();
    List<String> splitStreet;
   //  mailingAddress.xp =  new ShopAddress.dTaxcost();
    string key =  teacher.mailingCountry+teacher.mailingState;
    mailingAddress.FirstName = teacher.FirstName;
    mailingAddress.LastName = teacher.LastName;
    
    if (String.isBlank(teacher.mailingStreet)) {
      return;
    }
    else
    {
    splitStreet = getSplitStreet(teacher.mailingStreet);

    mailingAddress.Street1 = splitStreet[0];

    if (splitStreet.size() > 1) {
      mailingAddress.Street2 = splitStreet[1];
    }
    }
    mailingAddress.City = teacher.mailingCity;
    mailingAddress.State = teacher.mailingState;
    mailingAddress.Zip = teacher.mailingPostalCode;
    mailingAddress.Country = teacher.mailingCountryCode;
    mailingAddress.AddressName = MAILING_ADDRESS_NAME;
    mailingAddress.Id  = teacher.Mailing_Address_Id__c;
    if(taxmap.size() > 0)
    {
        mailingAddress.xp.TaxCost = taxmap.get(key).Tax_Percentage__c /100;
    }
    else
    {
        mailingAddress.xp.TaxCost = 0;
    }
    ShopUtils.UpdateShippingAddress(ShopUtils.getAdminAuthToken(), mailingAddress);
  }
}