public with sharing class PaymentTypeSelectionController {

  @AuraEnabled
  public static String updatePaymentAndReturnLink(ChargentOrders__ChargentOrder__c payment) {
    return DonorUtils.updatePaymentAndReturnLink(payment);
  }
}