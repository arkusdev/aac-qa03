public class FieldSetMember {

    public FieldSetMember(Schema.FieldSetMember f, Object fieldValue) {
        this.DBRequired = f.DBRequired;
        this.fieldPath = f.fieldPath;
        this.label = f.label;
        this.required = f.required;
        this.type = '' + f.getType();
        this.fieldValue = fieldValue;
    }

    public FieldSetMember(Boolean DBRequired) {
        this.DBRequired = DBRequired;
    }

    @AuraEnabled
    public Boolean DBRequired { get;set; }

    @AuraEnabled
    public String fieldPath { get;set; }

    @AuraEnabled
    public String label { get;set; }

    @AuraEnabled
    public Boolean required { get;set; }

    @AuraEnabled
    public String type { get; set; }

    @AuraEnabled    
    public Object fieldValue { get; set; }
}