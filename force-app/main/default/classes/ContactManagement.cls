/**
 * @description Handler class used by ContactTrigger to proces contact related to donors. Unit Test: ContactManagementTest
 */
public with sharing class ContactManagement {
  
	public static Boolean queueProcess = false;

    /**
     * @description Method excuted after update contacts
     */
	public static void afterUpdate(List<Contact> nList, Map<Id, Contact> oldMap) {
    	if (nList == null || nList.isEmpty() || oldMap == null || oldMap.isEmpty()) {
      		return;
    	}
		ExpFundsFlag__c efg = ExpFundsFlag__c.getOrgDefaults();
		if((!efg.Expired_Fund_Batch__c) || (Test.isRunningTest())) {
			syncBalanceToOrderCloud(nList, oldMap);
		}
  	}
    
    /**
     * @description Syncs balances with OrderCloud
     */
  	private static void syncBalanceToOrderCloud( List<Contact> nList, Map<Id, Contact> oldMap) {
    	ExpFundsFlag__c efg = ExpFundsFlag__c.getOrgDefaults();
    
    	List<Contact> conts = new List<Contact>();
    	for (Contact c : nList) {
			if((c.Funds_Available__c != oldmap.get(c.id).Funds_Available__c) || (efg.Expired_Fund_Contact_Batch__c) || Test.isRunningTest()){
				conts.add(c);
			}
    	}
		if(!ContactManagement.queueProcess) {
			ContactManagement.queueProcess = true;
			System.enqueueJob(new SpendingAccountUpdateQueueable(conts));
		}
	}

    /**
     * @description Method excuted before update contacts
     */
    public static void beforeUpdate(List<Contact> nList, Map<Id, Contact> oldMap) {
        
        List<ExpFundsOppRTs__mdt> rTList = [ SELECT MasterLabel, DeveloperName FROM ExpFundsOppRTs__mdt];
        List<String> rTDevNames = new List<String>();
        for(ExpFundsOppRTs__mdt rt : rTList){
            if(rt.DeveloperName != 'adjustment') {
                rTDevNames.add(rt.DeveloperName);
            }
        }
        
        List<Opportunity> last18monthOpps = [SELECT Name, Amount, Teacher__c, Anonymous__c, npsp__Primary_Contact__c, Hide_Transaction_on_Profile__c FROM Opportunity WHERE (RecordType.DeveloperName IN: rTDevNames) AND (StageName = 'Sale Closed / Paid') AND (Teacher__c IN: nList) AND (CloseDate = LAST_N_DAYS:548) AND (NOT Name LIKE '%Teacher Credit Card%')];
        
        for(Contact c : nList) {
            Set<Id> donorNumbers = new Set<Id>();
            c.Funds_Raised_Last_18_Months__c = 0;
            c.Donors_Last_18_Months__c = 0;
            for(Opportunity o : last18monthOpps){
                c.Funds_Raised_Last_18_Months__c += (o.Teacher__c == c.Id) ? o.Amount : 0;
                if(o.Anonymous__c==false && o.Teacher__c == c.Id && o.Hide_Transaction_on_Profile__c == FALSE) {
                    donorNumbers.add(o.npsp__Primary_Contact__c);
                }
                c.Funds_Raised_Last_18_Months__c = c.Funds_Raised_Last_18_Months__c < 0 ? 0 : c.Funds_Raised_Last_18_Months__c;
            }
            c.Donors_Last_18_Months__c = donorNumbers.size();
        }

        Integer currentMonth = System.today().month();
        Integer currentYear = System.today().year();
        Date initalDate = currentMonth >= 7 ? date.newinstance(currentYear, 7, 1) : date.newinstance((currentYear - 1), 7, 1);
        
        List<Opportunity> currentYearOpps  = [SELECT Name, Amount, Teacher__c, Anonymous__c, npsp__Primary_Contact__c, Hide_Transaction_on_Profile__c FROM Opportunity WHERE (RecordType.DeveloperName IN: rTDevNames) AND (StageName = 'Sale Closed / Paid') AND (Teacher__c IN: nList) AND (NOT Name LIKE '%Teacher Credit Card%') AND (CloseDate >= :initalDate AND CloseDate <= TODAY)];
        
        if (nList == null || nList.isEmpty() || oldMap == null || oldMap.isEmpty()) {
          return;
        }
        for(Contact con : nList) {
            Set<Id> donorNumbersThisYear = new Set<Id>();
            con.Donors_This_Year__c = 0;
            Decimal initailTotalDonation = con.Total_Donation__c;
            con.Total_Donation__c = 0;
            for(Opportunity opp : currentYearOpps) {
                if(opp.Anonymous__c==false && opp.Teacher__c == con.Id && opp.Hide_Transaction_on_Profile__c == FALSE) {
                    donorNumbersThisYear.add(opp.npsp__Primary_Contact__c);
                }

                con.Total_Donation__c += (opp.Teacher__c == con.Id) ? opp.Amount : 0;
                con.Total_Donation__c = con.Total_Donation__c < 0 ? 0 : con.Total_Donation__c;
            }
            con.Donors_This_Year__c = donorNumbersThisYear.size();

            if(con.Total_Donation_Modified_Date__c == null || initailTotalDonation != con.Total_Donation__c){
                con.Total_Donation_Modified_Date__c = Date.today();
            }
        }
        
        ExpFundsFlag__c efg = ExpFundsFlag__c.getOrgDefaults();
        if((!efg.Expired_Fund_Batch__c) || (Test.isRunningTest())){
            for(Contact c:nList){
                if(oldMap.get(c.Id).Funds_Available__c != c.Funds_Available__c){
                    c.Previous_Balance__c=oldMap.get(c.Id).Funds_Available__c;
                }
            }
        }

        List<Contact> conts = new List<Contact>();
        for (Contact c : nList) {
            if(c.school__c != Null && c.Order_Cloud_Spending_Account_Id__c != Null && oldmap.get(c.Id).school__c != c.school__c) {
                conts.add(c);
            }   
        }


        Map<Id,Id> userId = new Map<Id,Id>();
        for(User use : [SELECT Id, ContactId FROM User WHERE ContactId IN: oldMap.keyset()]) {
            userId.put(use.ContactId,use.Id);
        }
        
        if(!ContactManagement.queueProcess) {
			ContactManagement.queueProcess = true;
			System.enqueueJob(new AddressUpdateQueueable(conts, userId));
		}
    }

    /**
     * @description Validation to update Spending Accounts in OrderCloud
     * @return Returns true to update
     * @param nc New Contact
     * @param oc Old Contact
     */
  	public static Boolean updateSpendingAccount( Contact nc, Contact oc) {
    	return String.isNotBlank(nc.Order_Cloud_Spending_Account_Id__c) && (oc == null || nc.Funds_Available__c != oc.Funds_Available__c);
  	}
}