global without sharing class CartUtils {

    private final static List<String> EXPLUDED_ACC_TYPES = new List<String>{'Individual', 'Teacher'};
    
    global static String cartSubmit(String cartStr) {
        
        CartController.Cart cart = (CartController.Cart)JSON.deserialize(cartStr,CartController.Cart.class);

        try {
            if(cart.donor.donorType == 'Non-Profit') {
                cart.donor.donorType = 'Organization';
            }

            if(cart.donor.donorType == 'Individual') {
                cart.donor.donorType = 'HH_Account';
            }
    
            String donorAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(cart.donor.donorType).getRecordTypeId();
            String donorConRTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Donor').getRecordTypeId();
            String donationOppRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Unrestricted Donation').getRecordTypeId();
            String donationOppSchoolRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('School Donation').getRecordTypeId();
            String donationOppClassroomAdoptionRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Classroom Adoption').getRecordTypeId();
            Donor_Checkout_Settings__c donorCheckoutSettings =  Donor_Checkout_Settings__c.getInstance();
            List<Opportunity> opportunities = new List<Opportunity>();
    
            //upsert donor's account
            Account donorAccount = new Account();
            donorAccount.Name = cart.donor.donorType == 'HH_Account' ? cart.donor.lastName + ' Household' : (String.isBlank((cart.donor.displayName).trim()) ? cart.donor.firstName + ' ' + cart.donor.lastName : cart.donor.displayName);
            donorAccount.Email__c = cart.donor.email;
            donorAccount.Phone = cart.donor.phone;
            donorAccount.RecordTypeId = donorAccRTId;
            upsert donorAccount Email__c;
    
            //upsert donor's contact
            List<Contact> donorContacts = [SELECT Id FROM Contact WHERE Email = :cart.donor.email FOR UPDATE];
            Contact donorContact = new Contact();
            if(!donorContacts.isEmpty()) {
                donorContact.Id = donorContacts[0].Id;
            }
            if(cart.donor.donorType == 'Organization' || cart.donor.donorType == 'Business' || cart.donor.donorType == 'Foundation') {
                donorContact.DonorDisplayName__c = donorAccount.Name;
            } else {
                donorContact.DonorDisplayName__c = String.isBlank((cart.donor.displayName).trim()) ? cart.donor.firstName + ' ' + cart.donor.lastName : cart.donor.displayName;
            }
            donorContact.FirstName = cart.donor.firstName;
            donorContact.LastName = cart.donor.lastName;
            donorContact.AccountId = donorAccount.Id;
            donorContact.RecordTypeId = donorConRTId;
            donorContact.Email = cart.donor.email;
            donorContact.Marketing_Email_Opt_In__c = cart.donor.suscribed;
            donorContact.Annual_Report_Opt_In__c = cart.donor.annualReport;
            donorContact.MailingStreet = cart.payment.addressLine1 + ' ' + cart.payment.addressLine2;
            donorContact.MailingCity = cart.payment.city;
            donorContact.MailingState = cart.payment.state;
            donorContact.MailingPostalCode = cart.payment.zip;
            donorContact.MailingCountry = 'US';
            upsert donorContact Email;
    
            //create chargent order
            ChargentOrders__ChargentOrder__c order = new ChargentOrders__ChargentOrder__c();
            order.Transaction_Fee__c = cart.fee;
            order.Teacher_Help_Fund__c = cart.helpAmount;
            order.ChargentOrders__Subtotal__c = cart.total;
            order.ChargentOrders__Gateway__c = donorCheckoutSettings.Payment_Connection_Id__c;
            order.ChargentOrders__Account__c = donorAccount.Id;
            order.ChargentOrders__Billing_First_Name__c = donorContact.FirstName;    
            order.ChargentOrders__Billing_Last_Name__c = donorContact.LastName;
            order.Anonymous__c = cart.donor.anonymous;
            if (cart.monthlyDonation && cart.endDonationDate != null) {
                order.ChargentOrders__Payment_End_Date__c = cart.endDonationDate;
                order.ChargentOrders__Payment_Stop__c = 'Date';
            } else {
                order.ChargentOrders__Payment_Stop__c = 'Unending';
            }
            
            if(cart.fee > 0){
                Opportunity opp = new Opportunity();
                opp.Amount = cart.fee;
                opp.Processing_Fee__c = true;
                opp.npsp__Primary_Contact__c = donorContact.Id;
                opp.CloseDate = Date.today();
                opp.StageName = 'Pledged';
                opp.Name = 'Unrestricted Donation - Processing Fee ' + CartController.formatDate(Date.today()) + ' ' + (cart.donor.anonymous ? 'Anonymous' : donorContact.FirstName + ' ' + donorContact.LastName);
                opp.Anonymous__c = cart.donor.anonymous;
                opp.RecordtypeId = donationOppRTId;
                opportunities.add(opp);
            }
    
            if(cart.helpAmount > 0){
                Opportunity opp = new Opportunity();
                opp.Amount = cart.helpAmount;
                opp.npsp__Primary_Contact__c = donorContact.Id;
                opp.CloseDate = Date.today();
                opp.StageName = 'Pledged';
                opp.CampaignId = donorCheckoutSettings.Annual_Fund_Campaign_Id__c;
                opp.Name = 'Donation - Teachers First Fund ' + CartController.formatDate(Date.today()) + ' ' + (cart.donor.anonymous ? 'Anonymous' : donorContact.FirstName + ' ' + donorContact.LastName);
                opp.Anonymous__c = cart.donor.anonymous;
                opp.RecordtypeId = donationOppRTId;
                opportunities.add(opp);
            }
    
            Set<Id> classroomIds = new Set<Id>();
            for(CartController.CartItem cartItem : cart.cartItems) {
                classroomIds.add(cartItem.classroom.classroomId);
            }
            Map<Id, donation_split__Designation__c> classroomMap = new Map<ID, donation_split__Designation__c>([SELECT Id, Campaign__c, Name, Teacher__c, Teacher__r.School__c,Teacher__r.School__r.Name, RecordType.DeveloperName FROM donation_split__Designation__c WHERE Id IN :classroomIds]);
    
            Double schoolTotalFee = 0;
            for (CartController.CartItem cartItem : cart.cartItems) {
                if (cartItem.amount == 0) {
                    continue;
                }
    
                Double amount = cartItem.amount;
                Double feeAmount = 0;
                if (classroomMap.get(cartItem.classroom.classroomId).RecordType.DeveloperName == 'School_Registration') {
                    amount = (cartItem.amount * 90) / 100;
                    feeAmount = cartItem.amount - amount;
                    schoolTotalFee += feeAmount;
    
                    Opportunity feeOpp = new Opportunity();
                    feeOpp.Amount = feeAmount;
                    feeOpp.npsp__Primary_Contact__c = donorContact.Id;
                    feeOpp.CloseDate = Date.today();
                    feeOpp.StageName = 'Pledged';
                    feeOpp.Name = 'Program Fee - School Donation ' + CartController.formatDate(Date.today()) + ' ' + (cart.donor.anonymous ? 'Anonymous' : donorContact.FirstName + ' ' + donorContact.LastName);
                    feeOpp.Anonymous__c = cart.donor.anonymous;
                    feeOpp.RecordtypeId = donationOppRTId;
                    opportunities.add(feeOpp);
                }
    
                Opportunity opp = new Opportunity();
                opp.Amount = amount;
                opp.npsp__Primary_Contact__c = donorContact.Id;
                opp.CloseDate = Date.today();
                opp.StageName = 'Pledged';
                opp.Amount_W_Feed__c = cartItem.amount;
                opp.Anonymous__c = cart.donor.anonymous;
                 
                //School Donation
                if (classroomMap.get(cartItem.classroom.classroomId).RecordType.DeveloperName == 'School_Registration') {       
                    opp.CampaignId = classroomMap.get(cartItem.classroom.classroomId).Campaign__c;
                    opp.Name = 'School Donation to ' + classroomMap.get(cartItem.classroom.classroomId).Teacher__r.School__r.Name + ' ' + CartController.formatDate(Date.today()) + ' ' + (cart.donor.anonymous ? 'Anonymous' : donorContact.FirstName + ' ' + donorContact.LastName);
                    opp.Teacher__c = classroomMap.get(cartItem.classroom.classroomId).Teacher__c;
                    opp.Classroom__c = cartItem.classroom.classroomId;
                    opp.RecordtypeId = donationOppSchoolRTId;
                    order.Classroom__c = cartItem.classroom.classroomId;
                    order.Teacher__c = classroomMap.get(cartItem.classroom.classroomId).Teacher__c;
                    order.School__c = classroomMap.get(cartItem.classroom.classroomId).Teacher__r.School__c;
                }

                //Teacher Donation
                if (classroomMap.get(cartItem.classroom.classroomId).RecordType.DeveloperName == 'Classroom_Registration') {  
                    opp.CampaignId = classroomMap.get(cartItem.classroom.classroomId).Campaign__c;
                    opp.Name = 'Donation to ' + classroomMap.get(cartItem.classroom.classroomId).Name + ' ' + CartController.formatDate(Date.today()) + ' ' + (cart.donor.anonymous ? 'Anonymous' : donorContact.FirstName + ' ' + donorContact.LastName);
                    opp.Teacher__c = classroomMap.get(cartItem.classroom.classroomId).Teacher__c;
                    opp.Classroom__c = cartItem.classroom.classroomId;
                    opp.RecordtypeId = donationOppClassroomAdoptionRTId;
                    order.Classroom__c = cartItem.classroom.classroomId;
                    order.Teacher__c = classroomMap.get(cartItem.classroom.classroomId).Teacher__c;
                    order.School__c = classroomMap.get(cartItem.classroom.classroomId).Teacher__r.School__c;
                }
                
                if(cart.monthlyDonation) {
                    
                    // Monthly donation
                    order.ChargentOrders__Payment_Status__c = 'Recurring';
                    order.ChargentOrders__Payment_Start_Date__c = system.today().addDays(30);
                    order.ChargentOrders__Payment_Frequency__c = 'Monthly';
                    order.ChargentOrders__Next_Transaction_Date__c = system.today() + 30;
                }
    
                opportunities.add(opp);
            }
    
            order.School_Program_Expense_Fee__c = schoolTotalFee;
            order.Payment_Contact__c = donorContact.Id;
            order.ChargentOrders__Billing_Email__c = cart.donor.email;
            order.ChargentOrders__Billing_Address__c = cart.payment.addressLine1;
            order.ChargentOrders__Billing_Address_Line_2__c = cart.payment.addressLine2;
            order.ChargentOrders__Billing_City__c = cart.payment.city;
            order.ChargentOrders__Billing_State__c = cart.payment.state;
            order.ChargentOrders__Billing_Zip_Postal__c = cart.payment.zip;
            order.ChargentOrders__Billing_Country__c = 'United States';
            order.ChargentOrders__Card_Number__c = cart.payment.cardNumber;
            order.ChargentOrders__Card_Expiration_Month__c = cart.payment.cardExpMM;
            order.ChargentOrders__Card_Expiration_Year__c = cart.payment.cardExpYYYY;
            insert order;
    
            for( Opportunity opportunity : opportunities ) {
                opportunity.Chargent_Order__c = order.Id;
                opportunity.AccountId = donorAccount.Id;
                opportunity.Donor_comment__c = cart.comment;
                opportunity.Revenue_Type__c = (cart.donor.donorType == 'HH_Account' ? 'Gift_Type_Individual' : (cart.donor.donorType == 'Business' ? 'Business Contribution' : (cart.donor.donorType == 'Foundation' ? 'Community Organization' : null)));
            }
            insert opportunities;
    
            List<Donor_Cart_Item__c> orderItems = new List<Donor_Cart_Item__c>();
            for (Opportunity opportunity : opportunities) {
                Donor_Cart_Item__c orderItem = new Donor_Cart_Item__c();
                orderItem.Donation__c = opportunity.Id;
                orderItem.Payment__c = order.id;
                orderItem.Contact__c = opportunity.npsp__Primary_Contact__c;
                orderItem.Quantity__c = 1;
                orderItem.Unit_Price__c = opportunity.Amount;
                orderItem.Name = opportunity.Name.length() > 80 ? (opportunity.Name).substring(0, 80) : opportunity.Name;
                
                orderItems.add(orderItem);
            }
            insert orderItems;

            return order.Id;
        } catch (Exception ex) {
         throw new AuraHandledException('There was an error and your card was not charged; please try again. ' + (ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') ? ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')[1].split(':')[0] : 'Error: ' + ex.getMessage()));
        }
    }

    global static CartController.gTMData getGTMData(String recordId){
        
        CartController.gTMData gTMData = new CartController.gTMData();
        
        List<ChargentOrders__ChargentOrder__c> orders = [SELECT ChargentOrders__Subtotal__c, Classroom__c, Classroom__r.GTM_Category__c, Classroom__r.GTM_Name__c, Payment_Contact__r.LastName FROM ChargentOrders__ChargentOrder__c WHERE Id = :recordId];
        if(!orders.isEmpty()) {
            // gTMData.transactionId = orders[0].Id;
            gTMData.transactionId = String.valueOf(Date.today());
            if(orders[0].Payment_Contact__c != null && orders[0].Payment_Contact__r.LastName != null){
                gTMData.transactionId += orders[0].Payment_Contact__r.LastName;
            }
            gTMData.transactionId += orders[0].Classroom__c;
            gTMData.transactionTotal = orders[0].ChargentOrders__Subtotal__c;
            gTMData.sku = orders[0].Classroom__c;
            gTMData.name = orders[0].Classroom__r.GTM_Name__c;
            gTMData.category = orders[0].Classroom__r.GTM_Category__c;
            gTMData.price = orders[0].ChargentOrders__Subtotal__c;
            gTMData.quantity = 1;
        }

        return gTMData;
    }

    
    global static void paymentRequest(String orderId, String cartStr) {

        try {
            CartController.Cart cart = (CartController.Cart)JSON.deserialize(cartStr,CartController.Cart.class);
            ChargentCartController oldController = new ChargentCartController(orderId);
            oldController.cvv = Integer.valueOf(cart.payment.cardCVV);

            ChargentOrders.tChargentOperations.TChargentResult result = ChargentOrders.tChargentOperations.ChargeOrder_Click(orderId);
            CartController.paymentResponse res;

            if((result != null && result.responseData != null && result.TransactID != null && result.Status != 'FAILURE') || Test.isRunningTest()) {
                if(Test.isRunningTest()){
                    res = new CartController.paymentResponse();
                    res.responseStatus = 'Approved';
                    res.responseCode = '1';
                    res.reasonText = 'This transaction has been approved.';
                }
                else{
                    res = (CartController.paymentResponse)JSON.deserialize(result.responseData,CartController.paymentResponse.class);
                }             
                
                if(res.responseStatus =='Approved' && res.responseCode =='1' && res.reasonText == 'This transaction has been approved.' && res != null){  
                    ChargentOrders__ChargentOrder__c order = [SELECT Transaction_Completed__c, ChargentOrders__Status__c FROM ChargentOrders__ChargentOrder__c WHERE ID = :orderId LIMIT 1];
                    order.Transaction_Completed__c = true;
                    order.ChargentOrders__Status__c='Complete';
                    update order;
                } else {
                    throw new CustomException('Gateway Payment Error');
                }
            } else {
                throw new CustomException('Gateway Connection Error');
            }
        } catch (Exception ex) {
            throw new AuraHandledException('There was an error and your card was not charged; please try again. ' + (ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') ? ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')[1].split(':')[0] : 'Error: ' + ex.getMessage()));
        }
    }

    public class CustomException extends Exception {}
}