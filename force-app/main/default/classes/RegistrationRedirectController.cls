public class RegistrationRedirectController {
  public RegistrationRedirectController() {
    System.debug('Test');
  }

  @AuraEnabled
    public static User getCurrentUser() {
      User toReturn = [SELECT Id, 
                    ContactId
             FROM User 
             WHERE Id = :UserInfo.getUserId() 
             LIMIT 1];
        return toReturn;
  }

  @AuraEnabled
  public static Contact getCurrentContact() {
      User tempUser = getCurrentUser();
      System.debug('----- temp current user');
      System.debug(tempUser);
      try{
        Contact toReturn = [SELECT Id, 
                    RegistrationComplete__c
             FROM Contact 
             WHERE Id = :tempUser.ContactId 
             LIMIT 1];
        return toReturn;
      }catch(Exception e){
        return null;
      }
  }

}