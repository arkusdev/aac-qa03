/**
 * Created by christopherjohnson on 4/3/18.
 */

@IsTest
public class ControlAccountBalanceControllerTest {

    @IsTest
    public static void testControlAccountBalanceControllerAR() {
        TestDataSuite.initialize();

        ControlAccountBalanceController cabc = new ControlAccountBalanceController();
        cabc.record.Type__c = 'Accounts Receivable';
        cabc.record.Accounting_Period__c = TestDataSuite.acctPeriods[0].Id;
        System.assertNotEquals(NULL,cabc.record);
        cabc.executeJob();
    }

    @IsTest
    public static void testControlAccountBalanceControllerAP() {
        TestDataSuite.initialize();

        ControlAccountBalanceController cabc = new ControlAccountBalanceController();
        cabc.record.Type__c = 'Accounts Payable';
        cabc.record.Accounting_Period__c = TestDataSuite.acctPeriods[0].Id;
        System.assertNotEquals(NULL,cabc.record);
        cabc.executeJob();
    }
}