@isTest
private class ContactMyDonorsControllerTest {

    
    @isTest public static void testContactMyDonors() {

        
        UserRole userRole_1 = [SELECT Id FROM UserRole LIMIT 1];
        User admin = [SELECT Id, Username, UserRoleId FROM User WHERE Profile.Name = 'System Administrator' AND UserRoleId !=''  AND IsActive = true LIMIT 1];
        User user1;
        System.runAs(admin) {
            //account   
            Account acc = new Account(  
                Name = 'Unit test'
            );
            insert acc;

            //contact
            Contact contact = new Contact();
            contact.FirstName = 'Test';
            contact.LastName = 'Unit Test';
            contact.Salutation = 'Mr.';
            contact.Birthdate = date.today();
            contact.Phone = '1234567';
            contact.MobilePhone = '1234567';
            contact.Fax = '1234567';
            contact.Title = 'Robot';
            contact.Email = 'me@unittest.com';
            contact.AccountId = acc.Id;
            insert contact;

            //contact
            Contact contact2 = new Contact();
            contact2.FirstName = 'Test2';
            contact2.LastName = 'Unit Test2';
            contact2.Salutation = 'Mr.';
            contact2.Birthdate = date.today();
            contact2.Phone = '12345678';
            contact2.MobilePhone = '12345678';
            contact2.Fax = '12345678';
            contact2.Title = 'Robot';
            contact2.Email = 'me2@unittest.com';
            contact2.AccountId = acc.Id;
            insert contact2;

            //insert portal user
            user1 = new User(
                Username = 'testamc@test.acm',
                ContactId = contact.Id,
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'AAC - Teacher Community Plus Login User' Limit 1].Id,
                Alias = 'test123',
                Email = 'test12345@test.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'Qtest',
                CommunityNickname = 'test12345',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'
            );
            insert user1;

            AcctSeed__GL_Account__c asgl = new  AcctSeed__GL_Account__c();
            asgl.Name = '4599 Teacher Restricted Released';
            asgl.AcctSeed__Active__c = true;
            asgl.AcctSeed__Type__c = 'Revenue';
            Insert asgl;

            Map<String, RecordType> recordTypeMap = ContactMyDonorsController.getRecordTypeMap(new Set<String>{ContactMyDonorsController.RECORD_TYPE_DONATION, ContactMyDonorsController.RECORD_TYPE_SCHOOL, ContactMyDonorsController.RECORD_TYPE_DISBURSMENT}, 'Opportunity');
            Opportunity opp = new Opportunity();
            opp.Name = 'Test';
            opp.teacher__c = contact.Id;
            opp.Account = acc;
            opp.recordTypeId = recordTypeMap.get(ContactMyDonorsController.RECORD_TYPE_DONATION).Id;
            opp.StageName = 'Sale Closed / Paid';
            opp.CloseDate = date.today()+1;
            opp.Amount = 55;

            insert opp;
        }
        Test.StartTest();
            System.runAs(user1){
                ContactMyDonorsController cmd = new ContactMyDonorsController();
                Contact c = [SELECT Id FROM Contact WHERE FirstName ='Test2' LIMIT 1];
                ContactMyDonorsController.emailWrapper ew = new ContactMyDonorsController.emailWrapper('this is a test email', 'Mr Test', c.Id);
                ContactMyDonorsController.sendEmail(JSON.serialize(ew));
            }
            Integer invocations = Limits.getEmailInvocations(); 
        Test.stopTest();

        System.assertEquals(1, invocations, 'An email has not been sent');
    }
    
}