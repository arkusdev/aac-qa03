public without sharing class ActivityUtils {
    public static List<ActivityController.data> getData(String classroomId) {

        //Get Help Link
        String helpLink = '';
        List<DonorsCommunitySetting__mdt> donorsCommunitySettings = [SELECT Help_Link__c FROM DonorsCommunitySetting__mdt LIMIT 1];
        if(!donorsCommunitySettings.isEmpty()){
            helpLink = donorsCommunitySettings[0].Help_Link__c;
        }
        
        //Get activities
        Map<ID, Announcement__c> mapActivity = new Map<ID, Announcement__c>([SELECT Name, Comment__c, CreatedBy.Name, CreatedBy.SmallPhotoUrl, Flagged__c, Flagged_Reason__c, LastModifiedBy.Name, Pin_at_Top__c, Classroom__c, Classroom__r.Name, Classroom__r.OwnerId, CreatedDate, Auto_Post__c, (SELECT Id, Name FROM Attachments) FROM Announcement__c WHERE Classroom__c = :classroomId AND Flagged__c=false ORDER BY CreatedDate DESC]);

        //Get default bot picture
        StaticResource staticResource = [SELECT Name, SystemModStamp FROM StaticResource WHERE Name = 'donor_community' LIMIT 1];
        String botPictureURL = URL.getCurrentRequestUrl().toExternalForm().split('sfsites')[0] + 'resource/' + String.valueOf(((DateTime)staticResource.get('SystemModStamp')).getTime()) + '/' + staticResource.get('Name') + '/default_bot.jpg';

        //Get post numbers
        Map<Id, Integer> mapAnnouncementPosts = new Map<Id, Integer>();
        if(!mapActivity.isEmpty()) {
            for(Announcement_Comment__c comment : [SELECT Announcement__c FROM Announcement_Comment__c WHERE Announcement__c IN :mapActivity.keyset() AND Approved__c = true]) {
                if(mapAnnouncementPosts.containsKey(comment.Announcement__c)) { mapAnnouncementPosts.put(comment.Announcement__c, mapAnnouncementPosts.get(comment.Announcement__c) + 1); } else { mapAnnouncementPosts.put(comment.Announcement__c, 1); }
            }
        }

        //Get ContentDistributions
        Map<Id, List<ContentDistribution>> mapAnnFIleURLs = new Map<Id, List<ContentDistribution>>();
        for(ContentDistribution cd : [SELECT Id, RelatedRecordId, ContentDownloadUrl, DistributionPublicUrl, ContentDocumentId, Name FROM ContentDistribution WHERE RelatedRecordId IN :mapActivity.keyset() ORDER BY CreatedDate DESC]) {
                List<ContentDistribution> FIleURLs = mapAnnFIleURLs.get(cd.RelatedRecordId);
                if(FIleURLs == NULL) { FIleURLs = new List<ContentDistribution>(); }
                FIleURLs.add(cd);
                mapAnnFIleURLs.put(cd.RelatedRecordId, FIleURLs);
        }
        

        //Create retrun data
        List<ActivityController.data> processedData = new List<ActivityController.Data>();
        for(Announcement__c activity : mapActivity.values()) {
            processedData.add(new ActivityController.data(activity, mapAnnouncementPosts.containsKey(activity.Id)?mapAnnouncementPosts.get(activity.Id):0, mapAnnFIleURLs.get(activity.Id), botPictureURL, helpLink));
        }

        return processedData;
    }
    public static String saveRecord(String record) {

        try{
    		Announcement__c activity = (Announcement__c) System.JSON.deserialize(record, Announcement__c.class);
            upsert activity;
            return activity.Id;
        }
        catch(Exception ex){
            throw new AuraHandledException(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') ? ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')[1].split(':')[0] : 'Error: ' + ex.getMessage());
        }
    }
    public static void setComment(String recordId, String comment) {
        try{
    		Announcement_Comment__c newComment = new Announcement_Comment__c();
            newComment.Approved__c = true;
            newComment.Announcement__c = recordId;
            newComment.Comment__c = comment;
            insert newComment;
        }
        catch(Exception ex){
            throw new AuraHandledException(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') ? ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')[1].split(':')[0] : 'Error: ' + ex.getMessage());
        }
    }
    public static void toggleAComment(String recordId) {
        try{
    		List<Announcement_Comment__c> comments = [SELECT Approved__c FROM Announcement_Comment__c WHERE Id = :recordId];
            if(!comments.isEmpty()) {
                comments[0].Approved__c = !comments[0].Approved__c;
                update comments;
            }
        }
        catch(Exception ex){ throw new AuraHandledException(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') ? ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')[1].split(':')[0] : 'Error: ' + ex.getMessage());}
    }
    public static ActivityController.activityData getRecord(String recordId) {
        try {
            //Check current user
            Boolean isGuest = true;
            String userType = UserInfo.getUserType();
            if (userType != 'Guest') {
                isGuest = false;
            }
            
            Announcement__c activity = new Announcement__c();
            User user = new User();
            List<Announcement_Comment__c> comments = new List<Announcement_Comment__c>();
            List<Announcement__c> activities = [SELECT Pin_at_Top__c, Comment__c, Flagged__c, Flagged_Reason__c, Classroom__c, Classroom__r.Teacher__r.OwnerId, CreatedById, Classroom__r.Teacher__c, Auto_Post__c FROM Announcement__c WHERE ID = :recordId];
            if(!activities.isEmpty()) {
                activity = activities[0];
                List<User> teacherUser = [SELECT Id FROM User WHERE ContactId = :activity.Classroom__r.Teacher__c LIMIT 1];
                if(!teacherUser.isEmpty()) {
                    user = teacherUser[0];
                }
                comments = [SELECT Approved__c, Comment__c, CreatedDate, CreatedBy.Name, CreatedBy.SmallPhotoUrl FROM Announcement_Comment__c WHERE Announcement__c = :activity.Id ORDER BY CreatedDate DESC];
            }
            ActivityController.activityData activityData = new ActivityController.activityData(activity, user, comments, isGuest);
            return activityData;
        }
        catch(Exception ex){ throw new AuraHandledException(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') ? ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')[1].split(':')[0] : 'Error: ' + ex.getMessage());}
    }
    public static void delPhoto(String fileId){
        List<ContentDocument> files = new List<ContentDocument>();
        files = [SELECT Id FROM ContentDocument WHERE Id = :fileId];
        if(!files.isEmpty()) { delete files; }
    }
    
}