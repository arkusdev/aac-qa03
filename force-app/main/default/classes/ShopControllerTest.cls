@isTest
private class ShopControllerTest {

  	private static User testUser;
  
  	static testMethod void testclassroom() {
		
		Account a = TestUtils.createAccount();
		a.ShippingStreet = 'Test';
		a.ShippingCity = 'Test';
		a.ShippingStateCode = 'MN';
		a.ShippingPostalCode = '11111';
		a.ShippingCountryCode = 'US';
		a.BillingStreet = 'Test';
		a.BillingCity = 'Test';
		a.BillingStateCode = 'MN';
		a.BillingPostalCode = '11111';
		a.BillingCountryCode = 'US';
		insert a;

		Contact c = TestUtils.createContact(a.Id);
		c.Funds_Available__c = 5000;
		c.School__c = a.Id;
		insert c;

		donation_split__Designation__c dsd = testutils.createClassroom(c.Id,a.Id);
		insert dsd;

		Test.startTest();
		ShopController.getClassroomForContact(c.Id);
		Test.stopTest();
	}
  
	static testMethod void testShopControllerWithHash() {
		initTestData();
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());
		System.assertEquals(null, ShopController.generateShopUrl(null));
		String url = null;
		System.runAs(testUser) {
			url = ShopController.generateShopUrl(testuser.Id);
			User u = [SELECT Id, Shop_Password__c, Shop_User_Created__c FROM User WHERE Id = :testuser.Id];
		}
		Test.stopTest();
		System.assertNotEquals(null, url);
	}
    
	static testMethod void testShopController() {
		initTestData();
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());
		System.assertEquals(null, ShopController.generateShopUrl(null));
		String url = null;
		System.runAs(testUser) {
			url = ShopController.generateShopUrl(testuser.Id);
			User u = [SELECT Id, Shop_Password__c, Shop_User_Created__c FROM User WHERE Id = :testuser.Id];
		}

		Test.stopTest();
		System.assertNotEquals(null, url);
	}

	private static void initTestData() {
		User tuser = TestUtils.createSystemAdministrator();
		insert tuser;
		system.runas(tuser) {
			Shop_Settings__c settings = TestUtils.createShopSettings();
			insert settings;
			
			Account a = TestUtils.createAccount();
			a.ShippingStreet = 'Test';
			a.ShippingCity = 'Test';
			a.ShippingStateCode = 'MN';
			a.ShippingPostalCode = '11111';
			a.ShippingCountryCode = 'US';
			a.BillingStreet = 'Test';
			a.BillingCity = 'Test';
			a.BillingStateCode = 'MN';
			a.BillingPostalCode = '11111';
			a.BillingCountryCode = 'US';
			a.Account_Status__c = 'Active';
			insert a;

			Contact c = TestUtils.createContact(a.Id);
			c.Funds_Available__c = 5000;
			c.School__c = a.Id;
			insert c;

			donation_split__Designation__c dsd = TestUtils.createClassroom(c.Id,a.Id);
			dsd.School__c = a.Id;
			insert dsd;

			testUser = TestUtils.createCommunityUser();
			testUser.ContactId = c.Id;
			insert testUser;
		}

	}
  
	private static void initTestData1() {
  		donation_split__Designation__c doc = new donation_split__Designation__c();
		doc.Name = 'Test Name';
		insert doc;
		ShoppingCartController.getClassroomById(doc.id);
	}
	
	public static testMethod void testGenerateMac() {
		Long timestamp = Datetime.now().getTime();
		Test.startTest();
		String result = ShopController.generateMac(timestamp);
		Test.stopTest();

		System.assertNotEquals(null, result);
	}
}