public with sharing class FundThisClassroomController {
  
  @AuraEnabled
  public static donation_split__Designation__c getClassroom(Id classroomId) {
    if (String.isBlank(classroomId)) {
      return null;
    }

    try {
      return [select Id, 
                     ClassroomLandingPageURL__c
              from donation_split__Designation__c
              where Id = :classroomId];
    } catch (Exception e) {
      return null;
    }
  }
}