public without sharing class AnnouncementTriggerHandler {
    public static void onlyOnePin(Map<ID, Announcement__c> announcements) {
        List<Id> classrooms = new List<Id>();
        for(Announcement__c announcement : announcements.values()) {
            if(announcement.Pin_at_Top__c) {
                classrooms.add(announcement.Classroom__c);
            }
        }
        if(!classrooms.isEmpty()) {
            List<Announcement__c> cleanList = [SELECT Pin_at_Top__c FROM Announcement__c WHERE Classroom__c IN :classrooms AND Pin_at_Top__c = TRUE AND ID NOT IN :announcements.keyset()];
            if(!cleanList.isEmpty()) {
                for(Announcement__c announcement : cleanList) {
                    announcement.Pin_at_Top__c = false;
                }
                update cleanList;
            }
        }
    }

    public static void populateClassroom() {
        List<Id> teachers = new List<Id>();
        Map<Id, Id> mapTeacherClassroom = new Map<Id, Id>();
        for(Announcement__c announcement : (List<Announcement__c>)Trigger.new) {
            if(announcement.Teacher_School__c != null && announcement.Classroom__c == null) {
                teachers.add(announcement.Teacher_School__c);
            }
        }

        if(!teachers.isEmpty()) {
            for(donation_split__Designation__c classroom : [SELECT Teacher__c FROM donation_split__Designation__c WHERE Teacher__c IN :teachers]) {
                if(classroom.Teacher__c != null) {
                    mapTeacherClassroom.put(classroom.Teacher__c, classroom.Id);
                }
                
            }
        }

        for(Announcement__c announcement : (List<Announcement__c>)Trigger.new) {
            if(mapTeacherClassroom.containsKey(announcement.Teacher_School__c)) {
                announcement.Classroom__c = mapTeacherClassroom.get(announcement.Teacher_School__c);
            }
        }
    }
}