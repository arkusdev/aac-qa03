@isTest
public class UserPictureUploadTest {
    
    @TestSetup
    static void makeData(){
        Account a = TestUtils.createAccount();
		insert a;

		Account school = TestUtils.createAccount();
		school.ShippingStreet = '123 Main Street';
		school.ShippingCity = 'Minneapolis';
		school.ShippingStateCode = 'MN';
		school.ShippingCountryCode = 'US';
		school.ShippingPostalCode = '12345';
		insert school;
		
		Contact c = TestUtils.createContact(a.Id);
		c.School__c = school.Id;
		c.School__r = school;
		insert c;

		//insert portal user
		User user1 = new User(
			Username = 'testamc@test.acm',
			ContactId = c.Id,
			ProfileId = [SELECT Id FROM Profile WHERE Name = 'AAC - Teacher Community Plus Login User' Limit 1].Id,
			Alias = 'test123',
			Email = 'test12345@test.com',
			EmailEncodingKey = 'UTF-8',
			LastName = 'Qtest',
			CommunityNickname = 'test12345',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'en_US',
			LanguageLocaleKey = 'en_US'
		);
		insert user1;
	
		donation_split__Designation__c dsd = TestUtils.createClassroom(c.Id,school.Id);
		Insert dsd;
    }

    @isTest
    public static void testGetProfilePicture() {
        
        Test.startTest();
        String result = UserPictureUpload.getProfilePicture();
        Test.stopTest();

        system.assertNotEquals(null, result, 'It should return the profile picture url.');
    }

    @isTest
    public static void testGetUser() {
        
        String userId = UserInfo.getUserId();

        Test.startTest();
        User result = UserPictureUpload.getUser(userId);
        Test.stopTest();

        system.assertNotEquals(null, result, 'It should return the current user.');
    }

    @isTest
    public static void testSaveAttachment() {
        
        String base64Test = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQYV2M4c+bMfwAIMANkq3cY2wAAAABJRU5ErkJggg==';
        String fileNameTest = 'test.jpg';

        Test.startTest();
        UserPictureUpload.saveAttachment(base64Test, fileNameTest);
        Test.stopTest();

        User result = [SELECT SmallPhotoUrl FROM User WHERE ID = :UserInfo.getUserId() LIMIT 1];

        system.assertNotEquals(null, result.SmallPhotoUrl, 'It should return the current user Profile Picture.');
    }
}