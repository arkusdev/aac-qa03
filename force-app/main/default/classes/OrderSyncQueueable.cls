public with sharing class OrderSyncQueueable implements Queueable,
    Database.AllowsCallouts {

  private Order_Cloud_Sync__c cloudSync;

  public OrderSyncQueueable(Order_Cloud_Sync__c cloudSync) {
    this.cloudSync = cloudSync;
  }
	
  public void execute(QueueableContext SC) {
    ShopUtils.createOpportunityFromQueue(this.cloudSync);
  }
}