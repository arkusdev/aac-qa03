public class ChargentCartController {

    private ChargentOrders__ChargentOrder__c Ord;
    public ChargentOrders__ChargentOrder__c c {
        get {
            if(c == null) {
                return DonorUtils.getChargentOrder(chargentId);
            } else {
                return c;
            }
        }
        set;
    }
    public Id chargentId{
        get {
            if(chargentId == null) {
                return ApexPages.currentPage().getParameters().get('id') == null ? ApexPages.currentPage().getParameters().get('cid') : ApexPages.currentPage().getParameters().get('id');
            } else {
                return chargentId;
            }
        }
        set;
    }
    public integer cvv{get;set;}
    public string cardname{get;set;}
    public List<wrapResult> orders {
        get {
            if(orders==null) {
                orders = new List<wrapResult>();
                for(Opportunity opp : c.Opportunities__r) {
                    orders.add(new wrapResult(opp.Classroom__r.GTM_Category__c, opp.Classroom__r.GTM_Name__c, opp.Classroom__c, opp.Teacher__r.Name, opp.Teacher_s_School__c, opp.RecordType.DeveloperName, opp.Amount_W_Feed__c, c.ChargentOrders__Payment_Frequency__c, opp.Amount));
                }
                return orders;
            } else {
                return orders;
            }
            
        }
        set;
    }

    public ChargentCartController(ApexPages.StandardController stdController) {
        this.Ord = (ChargentOrders__ChargentOrder__c)stdController.getRecord();
    }

    public ChargentCartController(String pChargentId) {
        chargentId = pChargentId;
        c = DonorUtils.getChargentOrder(chargentId);
    }

    public chargentCartController() {
        c = DonorUtils.getChargentOrder(chargentId);
        System.debug(chargentId);
        System.debug(c);
    }        
    
    public PageReference update_and_charge() {
        Update c;         
        return null; 
    }

    public class paymentResponse{
        public string responseStatus;
        public string responseCode;  
        public string reasonText;        
    }
    
    public PageReference charge() {
        return DonorUtils.charge(chargentId, c);
    }

    public String c1 { get; set; }
    
    public PageReference OnlinePdf() {
    PageReference print = New PageReference('/apex/' + 'PayOnline_pdf');
    return print;   
    }
    
    public PageReference PayByCheckpdf() {
    PageReference offlinepdf = New PageReference('/apex/'+'PayByCheckpdf?id='+ Ord.Id);
    offlinepdf.setredirect(true);
    return offlinepdf;   
    }

    public class wrapResult {
        public String GTM_Category{get;set;}
        public String GTM_Name{get;set;}
        public String ClassroomId{get;set;}
        public String TeacherName{get;set;}
        public String TeacherSchool{get;set;}
        public String OrderRecordType{get;set;}
        public String TeacherAmount_W_Feed{get;set;}
        public String OrderPayment_Frequency{get;set;}
        public String Amount{get;set;}

        public wrapResult(String pGTM_Category, String pGTM_Name, String pClassroomId, String pTeacherName, String pTeacherSchool, String pOrderRecordType, Decimal pTeacherAmount_W_Feed, String pOrderPayment_Frequency, Decimal pAmount) {
            GTM_Category = pGTM_Category;
            GTM_Name = pGTM_Name;
            ClassroomId = pClassroomId;
            TeacherName = pTeacherName;
            TeacherSchool = pTeacherSchool;
            OrderRecordType = pOrderRecordType;
            TeacherAmount_W_Feed = decimalToStringCurrency(pTeacherAmount_W_Feed);
            OrderPayment_Frequency = pOrderPayment_Frequency;
            Amount = decimalToStringCurrency(pAmount);
        }
    }

    public static String decimalToStringCurrency (Decimal scope) {
        String result = '0.00';
        if(scope != NULL) {
            List<String> args = new String[]{'0','number','#,###.00'};
            result = String.format(scope.format(), args);
            if(result.contains('.')) {
                if(result.split('\\.')[1].length()==1) {
                    result += '0';
                }
            } else {
                result += '.00';
            }
        }
        return result;
    }
}