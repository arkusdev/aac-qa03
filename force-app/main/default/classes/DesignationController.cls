public class DesignationController {
    @AuraEnabled
    public static List<donation_split__Designation__c> getDesignation() {
        return [Select Name From donation_split__Designation__c];
    }
}