global class ExpiringFundsNotificationBatch implements Database.Batchable<sObject>, Database.Stateful{

    global List<String> RTDevNames = new List<String>();

    global Database.QueryLocator start(Database.BatchableContext BC){
        ExpFundsFlag__c efg = ExpFundsFlag__c.getOrgDefaults();
        efg.Expired_Fund_Batch__c = true;
        update efg;
        
        List<ExpFundsOppRTs__mdt> RTList = [ SELECT MasterLabel, DeveloperName FROM ExpFundsOppRTs__mdt ];
        String RTConditions = '(';
        for(ExpFundsOppRTs__mdt rt : RTList){
            RTConditions += '(RecordType.DeveloperName = \'' + rt.DeveloperName + '\') OR ';
            RTDevNames.add(rt.DeveloperName);
        }
        RTConditions = RTConditions.substring(0, RTConditions.length()-4) + ') AND ';
        
        return Database.getQueryLocator('SELECT Id, Amount, AccountId, CloseDate, Teacher__c, Teacher_Email__c, Teacher__r.Name, Teacher_Funds_Available__c, Expiration_Date__c, FundsExpNot_1__c, FundsExpNot_2__c, FundsExpNot_3__c, FundsExpNot__c, ExpDateToday__c FROM Opportunity WHERE ' + RTConditions + ' (FundsExpNot__c = true OR ExpDateToday__c = true) AND Teacher__c <> null AND Teacher_Email__c <> null AND Teacher_Email__c <> \'\' AND Teacher_Funds_Available__c > 0 AND Do_not_process__c = FALSE ORDER BY Teacher__c, Expiration_Date__c, CloseDate, Amount');
    }

    global void execute(Database.BatchableContext BC, List<Opportunity> scope){
        
        Date earliestCloseDate = System.today();
        List<String> allTeachers = new List<String>();
        for(Opportunity o : scope){
            allTeachers.add(o.Teacher__c);
            earliestCloseDate = earliestCloseDate > o.CloseDate ? o.CloseDate : earliestCloseDate;
        }
        
        List<Opportunity> oppAfter = [ SELECT Id, Amount, Teacher__c, CloseDate FROM Opportunity WHERE (NOT(Name LIKE '%Expired Funds%')) AND (RecordType.DeveloperName IN: RTDevNames) AND (CloseDate >: earliestCloseDate) AND (Teacher__c IN: allTeachers) ];
        
        String log = '';
        
        try{
            ExpFundsSett__c efs = [SELECT Expired_Fund_Account__c, Expired_Fund_Record_Type__c, Expired_Fund_Stage__c, Expired_Fund_Fund__c, Org_Wide_Email_Address_ID__c, FundsExpNot_1_Templ__c, FundsExpNot_2_Templ__c, FundsExpNot_3_Templ__c FROM ExpFundsSett__c LIMIT 1];
            ID adjustmentRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(efs.Expired_Fund_Record_Type__c).getRecordTypeId();
            Account accAA = [SELECT Id FROM Account WHERE Name =: efs.Expired_Fund_Account__c LIMIT 1];
            List<Expired_Fund_Contact_Update__c> efcuList = new List<Expired_Fund_Contact_Update__c>();
            
            List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
            List<ID> cIDList = new List<ID>();
            Map<ID, Decimal> deductionsMap = new Map<ID, Decimal>();
            
            List<Opportunity> deductionList = new List<Opportunity>();
            List<Opportunity> amountUpdateList = new List<Opportunity>();
            
            Opportunity lastAdj;
            
            for(Opportunity opp : scope){
                log += 'Process: ' + opp.Id + '\n';
                //List<Opportunity> oppAfter = [ SELECT Id, Amount, Teacher__c, CloseDate FROM Opportunity WHERE RecordType.DeveloperName IN: RTDevNames AND CloseDate >: opp.CloseDate AND Teacher__c =: opp.Teacher__c ];
                
                Decimal d = opp.Teacher_Funds_Available__c;
                for(Opportunity o : oppAfter){
                    if((o.Teacher__c == opp.Teacher__c) && (o.CloseDate > opp.CloseDate)){
                        d -= o.Amount;
                    }
                }
    
                if(d > 0){
                    d = (d > opp.Amount) ? opp.Amount : (opp.Teacher_Funds_Available__c - d < 0) ? opp.Teacher_Funds_Available__c : d;
                        
                    cIDList.add(opp.Teacher__c);
                    
                    if(opp.FundsExpNot__c){
                        if(d > 10){
                            opp.Funds_Expiration_Amount__c = d;
                            amountUpdateList.add(opp);
                            log += 'Process Notification: ' + opp.Id + ' || ' + d + '\n';
                            
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            mail.setOrgWideEmailAddressId(efs.Org_Wide_Email_Address_ID__c);
                            mail.setTargetObjectId(opp.Teacher__c);
                            mail.setWhatId(opp.Id);
                            String templID = opp.FundsExpNot_1__c ? efs.FundsExpNot_1_Templ__c : opp.FundsExpNot_2__c ? efs.FundsExpNot_2_Templ__c : efs.FundsExpNot_3_Templ__c;
                            mail.setTemplateID(templID);
                            if(Test.isRunningTest())
                                mail.setHTMLBody('test');
                            mail.setToAddresses(new List<String>{opp.Teacher_Email__c});
                            mail.setSaveAsActivity(true);
                            emailList.add(mail);
                        }
                    }
                    if(opp.ExpDateToday__c){
                        if((lastAdj != null) && (lastAdj.Teacher__c != opp.Teacher__c)){
                            deductionList.add(lastAdj);
                        }
                        
                        lastAdj = new Opportunity(Name = 'AdoptAClassroom.org Expired Funds - ' + System.now().format('YYYY-MM-dd'), 
                        RecordTypeId = adjustmentRTId, 
                        AccountId = accAA.Id, 
                        Amount = -d,
                        Teacher__c = opp.Teacher__c, 
                        StageName = efs.Expired_Fund_Stage__c, 
                        CloseDate = System.today().addDays(-1),
                        Expiration_Date__c = System.today().addDays(-1),
                        IsThanksReceived__c = true,
                        Fund__c = efs.Expired_Fund_Fund__c,
                        Probability = 100,
                        Expired_Opportunity__c = opp.Id,
                        Do_not_process__c = true);
                        
                        log += 'Process Expiration: ' + opp.Id + ' || ' + lastAdj + '\n';
                        
                        deductionsMap.put(opp.Teacher__c, d);
                        
                        Expired_Fund_Contact_Update__c newExpired = new Expired_Fund_Contact_Update__c();
                        newExpired.Contact__c = opp.Teacher__c;
                        newExpired.Deduction__c = d;
                        newExpired.Opportunity__c = opp.Id;
                        efcuList.add(newExpired);

                    }
                }
            }
            
            if(lastAdj != null){
                deductionList.add(lastAdj);
            }
            
            if(amountUpdateList.size() > 0){
                update amountUpdateList;
            }
            
            if(deductionList.size() > 0){
                insert deductionList;
            }
            
            if(efcuList.size() > 0){
                insert efcuList;
            }
            
            if(cIDList.size() > 0){
                
                List<Contact> cList = [SELECT Id, Last_Expiring_Fund_Notification_Date__c FROM Contact WHERE Id IN: cIDList];
                
                for(Contact c : cList){
                    if(deductionsMap.get(c.Id) != null){
                        log += 'Contact Deduction: ' + c.Id + ' || deduct to funds available ' + deductionsMap.get(c.Id) + '\n';
                    }else{
                        c.Last_Expiring_Fund_Notification_Date__c = DateTime.now();
                        log += 'Contact Notification: ' + c.Id + ' || ' + c.Last_Expiring_Fund_Notification_Date__c + '\n';
                    }
                }
                
                update cList;
            }
            
            Messaging.SendEmailResult[] results = new List<Messaging.SendEmailResult>();
            Messaging.SendEmailError[] errors = new List<Messaging.SendEmailError>();
            
            try{
                if(emailList.size() > 0){
                    results = Messaging.sendEmail(emailList);
                }
            }catch(Exception e){
                log += 'EXCEPTION 1: ' + e.getStackTraceString() + '\n';
                log += 'SEND EMAIL LIMITS: ' + Limits.getEmailInvocations() + '/' + Limits.getLimitEmailInvocations() + '\n';
                for(Messaging.SendEmailResult currentResult : results){
                    errors = currentResult.getErrors();
                    if(errors != null){
                        for(Messaging.SendEmailError currentError : errors){
                            log += '(' + currentError.getStatusCode() + ') ' + currentError.getMessage() + '\n';
                        }
                    }
                }
            }
        }catch(Exception e){
            log += 'EXCEPTION 2: ' + e.getStackTraceString();
        }
        insert new Expired_Fund_Log__c(Log__c = log);
    }

    global void finish(Database.BatchableContext BC){
        ExpFundsFlag__c efg = ExpFundsFlag__c.getOrgDefaults();
        efg.Expired_Fund_Batch__c = false;
        update efg;
        
        ExpiredFundContactUpdateBatch b = new ExpiredFundContactUpdateBatch(); 
        database.executebatch(b, 1);
    }
}