global class Contact18MonthFundUpdateBatch implements Database.Batchable<sObject>, Database.Stateful{
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id FROM Contact WHERE RecordType.DeveloperName = \'Teacher\' OR RecordType.DeveloperName = \'School_Admin\'');
    }

    public void execute(Database.BatchableContext BC, List<Contact> scope){
        try{
            update scope;
        }catch(Exception e){
            insert new Expired_Fund_Log__c(Log__c = 'EXCEPTION ExpiredFundContactUpdateBatch: ' + e.getStackTraceString());
        }
    }

    public void finish(Database.BatchableContext BC){
    }
}