public class ThanksYourDonorsController {
    public string inputMessage {get; set;}
    private static final String RECORD_TYPE_ORDER = 'Orders';
    private static final String RECORD_TYPE_DONATION = 'adoption';
    private static final String RECORD_TYPE_SCHOOL = 'schooldonation';
    public static final String RECORD_TYPE_DISBURSMENT = 'Disbursement';
    private static final String RECORD_TYPE_DONATION_UNRESTRICTED = 'Donation'; 
    private static final String RECORD_TYPE_NAME_FIELD = 'recordTypeName';
    public static final String STAGE_PAID = 'Sale Closed / Paid';
    public donationClass donationOppWrapper {get; set;}
    public Opportunity donationOpp {get; set;}
    public string errorMessage {get; set;}
    public Boolean isDonorsExists {get; set;}
    public static date createdDateStartDate=Date.newInstance(2017,07,01);
    
    public class donationClass{
        public string name {get; set;}
        public DateTime createdDate {get; set;}
        public Decimal amount {get; set;}
        public string email;
        public ID oppId {get; set;}
        public boolean isAnonymus {get; set;}
        
        public donationClass(){
            this.isAnonymus=false;
        }
    }
    
    private static Map<String, RecordType> recordTypeMap = new Map<String, RecordType>();
    
    public pageReference sendMessage(){
        errorMessage='';        
        
        if(inputMessage == null || inputMessage ==''){
            errorMessage ='Please enter the message.';
            return null;
        }
        else if(inputMessage.length() < 100){
            errorMessage ='Minimum length is 100 characters - please add text about how donation will be used and why it is important to your students.';
            return null;
        }
        
        inputMessage+='<br/><br/>Thanks,<br/>'+teacherContact[0].Name+'<br/>';
        if(teacherContact[0].School__r.name != null){
            inputMessage+=teacherContact[0].School__r.name;
        } 
        List<orgwideEmailAddress> orgEmailAddressesList=[select id,Address,DisplayName from orgwideEmailAddress where DisplayName='noreply@AdoptAClassroom.org'];
        List<Messaging.SingleEmailMessage> mess = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage smail = new Messaging.SingleEmailMessage();
        String[] ccAddress = new String[] {'donorsupport@adoptaclassroom.org'};             
        smail.setToAddresses(new List<string>{donationOppWrapper.email});
        smail.setCcAddresses(ccAddress);
        smail.setSaveAsActivity(false); 
        smail.setUseSignature(false);
        smail.setHTMLBody(inputMessage);
        smail.setSubject('Thank You Donor!');
        if(orgEmailAddressesList != null && !orgEmailAddressesList.isEmpty()){
            smail.setOrgWideEmailAddressId(orgEmailAddressesList[0].Id);  
        }
        mess.add(smail); 
        
        if(mess.size() > 0)
        {
          try{
              Messaging.sendEmail(mess);
              
              recordTypeMap = getRecordTypeMap(new Set<String>{RECORD_TYPE_ORDER, RECORD_TYPE_DONATION, RECORD_TYPE_SCHOOL, RECORD_TYPE_DISBURSMENT, RECORD_TYPE_DONATION_UNRESTRICTED, RECORD_TYPE_NAME_FIELD}, 'Opportunity');
              opportunity oppToUpdate=new Opportunity(id=donationOppWrapper.oppId,IsThanksReceived__c=true);
              update oppToUpdate;
              
              findDonor();
              if(donationOppWrapper != null && donationOppWrapper.oppId != null){
                  isDonorsExists=true;
              }
              else{
                  isDonorsExists=false;
              }
          }
          Catch(Exception e){
              isDonorsExists=false;
          }
        }
        return null;
    }
    
    public List<Contact> teacherContact {get; set;}
    
    public ThanksYourDonorsController(){
        errorMessage ='';
        isDonorsExists =false;
        recordTypeMap = getRecordTypeMap(new Set<String>{RECORD_TYPE_ORDER, RECORD_TYPE_DONATION, RECORD_TYPE_SCHOOL, RECORD_TYPE_DISBURSMENT, RECORD_TYPE_DONATION_UNRESTRICTED, RECORD_TYPE_NAME_FIELD}, 'Opportunity');
        donationOpp=new opportunity();
        User communityRunningUser=[select id,contactid,email from user where id =:Userinfo.getUserId() limit 1];
        teacherContact=[select id,name,recordTypeId,School__c,School__r.name from contact where Id =:communityRunningUser.contactId];
        findDonor();
        if(donationOppWrapper != null && donationOppWrapper.oppId != null){
            isDonorsExists=true;
        }
    }
    
    public void findDonor(){
        donationOppWrapper=new donationClass();
        
        if(teacherContact != null && !teacherContact.isEmpty()){
            List<opportunity> donations=[select id,IsThanksReceived__c,Anonymous__c,pymt__Total_Amount__c,createdDate,npsp__Primary_Contact__c,npsp__Primary_Contact__r.name,npsp__Primary_Contact__r.npe01__HomeEmail__c,npsp__Primary_Contact__r.npe01__WorkEmail__c,npsp__Primary_Contact__r.email, npsp__Primary_Contact__r.DonorDisplayName__c from opportunity where teacher__c =:teacherContact[0].id and (recordTypeId =:recordTypeMap.get(RECORD_TYPE_DONATION).Id OR recordTypeId =:recordTypeMap.get(RECORD_TYPE_SCHOOL).Id OR recordTypeId =:recordTypeMap.get(RECORD_TYPE_DISBURSMENT).Id OR recordTypeId =:recordTypeMap.get(RECORD_TYPE_DONATION_UNRESTRICTED).Id) and StageName=:STAGE_PAID and (Createddate >=:createdDateStartDate) and (NOT name like 'Teacher Credit Card%') AND IsThanksReceived__c=false order by createdDate DESC];
            if(donations !=null && donations.size()>0){
                donationOppWrapper.name= !String.isBlank(donations[0].npsp__Primary_Contact__r.DonorDisplayName__c) ? donations[0].npsp__Primary_Contact__r.DonorDisplayName__c : donations[0].npsp__Primary_Contact__r.name;
                donationOppWrapper.createdDate=donations[0].createdDate;
                donationOppWrapper.amount =donations[0].pymt__Total_Amount__c;
                donationOppWrapper.email=donations[0].npsp__Primary_Contact__r.npe01__WorkEmail__c;
                donationOppWrapper.oppId=donations[0].Id;     
                donationOppWrapper.isAnonymus =donations[0].Anonymous__c; 
                inputMessage='';        
            }
        }
    }
        
    private static Map<String, RecordType> getRecordTypeMap(
        Set<String> developerNames, String objectName) {
        Map<String, RecordType> rtMap = new Map<String, RecordType>();
    
        for (RecordType rt :
            [select Id,
                    DeveloperName
             from RecordType
             where DeveloperName in :developerNames
             and SObjectType = :objectName]) {
          rtMap.put(rt.DeveloperName, rt);
        }

        return rtMap;
        
  }
  
  @AuraEnabled
  public static Boolean findDonations() {
        recordTypeMap = getRecordTypeMap(new Set<String>{RECORD_TYPE_ORDER, RECORD_TYPE_DONATION, RECORD_TYPE_SCHOOL, RECORD_TYPE_DISBURSMENT, RECORD_TYPE_DONATION_UNRESTRICTED, RECORD_TYPE_NAME_FIELD}, 'Opportunity');
        User RunningUser=[select id, contactid, email, profile.id from user where id =:Userinfo.getUserId() limit 1];
        List<Contact> teacher=[select id,name,recordTypeId from contact where Id =:RunningUser.contactId];
        
       // system.debug('*******RunningUser'+RunningUser);
        
        if(teacher != null && !teacher.isEmpty()){
            List<opportunity> donations=[select id,IsThanksReceived__c,Anonymous__c,pymt__Total_Amount__c,createdDate,npsp__Primary_Contact__c,npsp__Primary_Contact__r.name,npsp__Primary_Contact__r.npe01__HomeEmail__c,npsp__Primary_Contact__r.npe01__WorkEmail__c,npsp__Primary_Contact__r.email, npsp__Primary_Contact__r.DonorDisplayName__c from opportunity where teacher__c =:teacher[0].id and (recordTypeId =:recordTypeMap.get(RECORD_TYPE_DONATION).Id OR recordTypeId =:recordTypeMap.get(RECORD_TYPE_SCHOOL).Id OR recordTypeId =:recordTypeMap.get(RECORD_TYPE_DISBURSMENT).Id OR recordTypeId =:recordTypeMap.get(RECORD_TYPE_DONATION_UNRESTRICTED).Id) and StageName=:STAGE_PAID and (Createddate >=:createdDateStartDate) and (NOT name like 'Teacher Credit Card%') AND IsThanksReceived__c=false order by createdDate DESC];
            if(donations !=null && donations.size()>0){
                 return true;
            }
        }
        return false;
  }
  
  @AuraEnabled
  public static String getBaseUrlSettings(){
        return Base_Settings__c.getInstance().Visual_force_base_url__c;
  }
   
  @AuraEnabled 
  public static user fetchUser(){
      User comUser = [select id, contactid, email, ProfileId FROM User Where id =: userInfo.getUserId()];
      system.debug('*******UserProfile'+comUser);
        return comUser;            
  }
}