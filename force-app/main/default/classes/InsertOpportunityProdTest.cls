@isTest(seeAllData = true)
private class InsertOpportunityProdTest {

  static testMethod void testInsertOpportunityProd() {
      
       OrderCloudLineItem ocli = TestUtils.createOrderCloudOrderLineItem();
       list<ordercloudLineItem> oclilist = new list<ordercloudlineitem>();
       oclilist.add(ocli);
       OrderCloudLineItem ocline = testUtils.createOrderCloudOrderLineItem();
       ocline.ProductId = 'AACPunchoutProduct';
       
      User tuser = TestUtils.createSystemAdministrator();
    insert tuser;
    
    Account a = TestUtils.createAccount();
    Contact c = TestUtils.createContact(a.id);
    c.npe01__HomeEmail__c='test@gmail.com';
    
    Contact c1 = TestUtils.createContact(a.id);
    c1.npe01__HomeEmail__c='testnew@gmail.com';
    
    User testUser = TestUtils.createCommunityUser();
    system.runas(tuser)
    {
     Shop_Settings__c shopSettings = Shop_Settings__c.getInstance();
     if(shopSettings == Null)
     {
         shopSettings = TestUtils.createShopSettings();
         shopSettings.Pricebook_Id__c = Test.getStandardPricebookId();
         insert shopSettings;
     }    
    system.debug('VVVVVVVVVVVVVVVVV'+shopSettings.pricebook_Id__c);
    
    insert a;   
    insert new List<Contact>{c,c1};
    
    AcctSeed__GL_Account__c asgl = new  AcctSeed__GL_Account__c();
    asgl = [select Id,Name from AcctSeed__GL_Account__c where Name = '4599 Teacher Restricted Released' limit 1];
    if(asgl == Null)
    {
     asgl.Name = '4599 Teacher Restricted Released';
     asgl.AcctSeed__Active__c = true;
     asgl.AcctSeed__Type__c = 'Revenue';
     Insert asgl;
    }
    testUser.ContactId = c.Id;
    insert testUser;
    
   // pricebook2 pb = [Select Id,name,isstandard  from Pricebook2 Where IsStandard=true limit 1];
    
     
    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());
     
    Order_Cloud_Sync__c sync = TestUtils.createOrderCloudSync(testUser.Id);
    sync.Order_id__c = 'testId';
    insert sync;   
    
    Map<String,RecordType> recordTypeMap = OpportunityManagement.getRecordTypeMap(new Set<String>{OpportunityManagement.RECORD_TYPE_ORDER,OpportunityManagement.RECORD_TYPE_DONATION_UNRESTRICTED}, 'Opportunity');
    
    Opportunity o = TestUtils.createOpportunity(a.Id);
    o.Order_Cloud_Order_Id__c = 'testId';
    o.recordtypeId=recordTypeMap.get(OpportunityManagement.RECORD_TYPE_ORDER).Id;
    o.Teacher__c =c.Id;
    o.amount=100;
    o.OrderCloudLineItemCount__c=0;
    o.StageName = OpportunityManagement.STAGE_PAID;
    
    Opportunity donation = TestUtils.createOpportunity(a.Id);
    donation.recordtypeId=recordTypeMap.get(OpportunityManagement.RECORD_TYPE_DONATION_UNRESTRICTED).Id;
    donation.Teacher__c =c.Id;
    donation.npsp__Primary_Contact__c=c1.id;
    donation.StageName = OpportunityManagement.STAGE_PAID;
    donation.Amount=100;
    
    Opportunity donation1 = TestUtils.createOpportunity(a.Id);
    donation1.recordtypeId=recordTypeMap.get(OpportunityManagement.RECORD_TYPE_DONATION_UNRESTRICTED).Id;
    donation1.Teacher__c =c.Id;
    donation1.StageName = OpportunityManagement.STAGE_PAID;
    donation1.npsp__Primary_Contact__c=c1.id;
    donation1.Amount=50;
    
    insert new List<Opportunity>{o,donation,donation1};
    
    Test.startTest();
    InsertOpportunityProd iop = new InsertOpportunityProd(sync,oclilist);
    iop.pbid = shopSettings.Pricebook_Id__c;
    system.debug('VVVVVVVVVVVVVVVVV'+iop.pbid);
    database.executebatch(iop);
    test.stopTest();
    }
  }
}