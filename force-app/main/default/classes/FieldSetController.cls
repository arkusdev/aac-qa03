public class FieldSetController {

  @AuraEnabled
    public static User getCurrentUser() {
      User toReturn = [SELECT Id, 
                    ContactId
             FROM User 
             WHERE Id = :UserInfo.getUserId() 
             LIMIT 1];
        return toReturn;
  }

  @AuraEnabled
  public static Contact getCurrentContact() {
      User tempUser = getCurrentUser();
      System.debug('----- temp current user');
      System.debug(tempUser);
      list<Contact> toReturn = [SELECT Id, 
                    LastName, 
                    MobilePhone, 
                    Email, 
                    FirstName, 
                    Gender__c, 
                    Birthdate
             FROM Contact 
             WHERE Id = :tempUser.ContactId 
             LIMIT 1];
        System.debug('----- current contact');
        if(toReturn.size() > 0)
        {
        System.debug(toReturn[0]);
        return toReturn[0];
        }
        else
        {
            return Null;
        }
  }
  
    @AuraEnabled
    public static List<String> getTypeNames() {
        Map<String, Schema.SObjectType> types = Schema.getGlobalDescribe();
        List<String> typeNames = new List<String>();
        String typeName = null;
        List<String> fsNames;
        for (String name : types.keySet()) {
            if (hasFieldSets(name)) {
                typeNames.add(name);        
            }
        }
        return typeNames;
    }

    @AuraEnabled
    public static Boolean hasFieldSets(String typeName) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
        return !fsMap.isEmpty();
    }

    @AuraEnabled
    public static List<String> getFieldSetNames(String typeName) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
        List<String> fsNames = new List<String>();
        for (String name : fsMap.keySet()) {
            fsNames.add(name);
        }
        return fsNames;
    }

    @AuraEnabled
    public static List<FieldSetMember> getFields(String typeName, String fsName) {
      System.debug('----- getFields');
      System.debug(typeName);
      System.debug(fsName);
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        System.debug(targetType);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        System.debug(describe);
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
        System.debug(fsMap);
        Schema.FieldSet fs = fsMap.get(fsName);
        System.debug(fs);
        List<Schema.FieldSetMember> fieldSet = fs.getFields();
        System.debug(fieldSet);
        System.debug('----- getting contact');
        Contact currentContact = getCurrentContact();
        if(currentContact != Null)
        {
        System.debug(currentContact);
        List<FieldSetMember> fset = new List<FieldSetMember>();
        for (Schema.FieldSetMember f: fieldSet) {
          System.debug(f);
          System.debug(f.fieldPath);
          System.debug(currentContact.get(f.fieldPath));
            fset.add(new FieldSetMember(f, currentContact.get(f.fieldPath)));
        }
        return fset;
        }
        else
        {
            return Null;
        }
    }


  public static List<Schema.FieldSetMember> getCommunitiesUserProfileAdditionalFields() {
  return SObjectType.Contact.FieldSets.CommunitiesUserProfileAdditionalFields.getFields();
  }

  @AuraEnabled
  public static void updateFields(Contact currentContact) {
    
    System.debug('----- updateFields');
    System.debug(currentContact);

    update currentContact;

  }

}