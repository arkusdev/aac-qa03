public with sharing class ShopUtils {

	private static final String CLIENT_ID_PARAM = 'client_id';
	private static final String ALPHA_REGEX = '[^a-zA-Z0-9]';
	private static final String GRANT_TYPE_PARAM = 'grant_type';
	private static final String USERNAME_PARAM = 'username';
	private static final String PASSWORD_PARAM = 'password';
	private static final String SCOPE_PARAM = 'scope';
	private static final String GRANT_TYPE_PASSWORD = 'password';
	private static final String SCOPE_TYPE_FULL = 'FullAccess';
	private static final String OPPORTUNITY_ORDER_RECORD_TYPE = 'Orders';
	private static final String METHOD_POST = 'POST';
	private static final String METHOD_PUT = 'PUT';
	private static final String METHOD_GET = 'GET';
	private static final String ORDER_NAME_TEMPLATE = '{0} Order ({1})';
	private static final String OPPORTUNITY_STAGE = 'Sale Closed / Paid';
	private static final String CONTENT_TYPE = 'text/html; charset=UTF-8';
	private static final String CONTENT_TYPE_JSON = 'application/json;charset=UTF-8';
	private static final String CHARSET = 'UTF-8';
	private static final String CONTENT_TYPE_HEADER = 'Content-Type';
	private static final String PARAM_TEMPLATE = '{0}={1}';
	private static final String SEPARATOR_TEMPLATE = '&{0}';
	private static final String GRANT_TYPE_CREDENTIALS = 'client_credentials';
	private static final String CLIENT_SECRET_PARAM = 'client_secret';
	private static final String USER_PROVISION_PATH = '{0}/buyers/{1}/users';
	private static final String CREATE_ADDRESS_PATH = '{0}/buyers/{1}/addresses';
	private static final String ADDRESS_GROUP_PATH = '{0}/buyers/{1}/addresses/assignments';
	private static final String USER_GROUP_PATH = '{0}/buyers/{1}/usergroups/assignments';
	private static final String IMPERSONATION_PATH = '{0}/buyers/{1}/users/{2}/accesstoken';
	private static final String LINE_ITEMS_PATH = '{0}/orders/incoming/{2}/lineitems?pageSize=100';
	private static final String CREATE_SPENDING_ACCOUNT_PATH = '{0}/buyers/{1}/spendingaccounts';
	private static final String SPENDING_ACCOUNT_ASSIGNMENT_PATH = '{0}/buyers/{1}/spendingaccounts/assignments';
	private static final String UPDATE_SPENDING_ACCOUNT_PATH = '{0}/buyers/{1}/spendingaccounts/{2}';
	private static final string UPDATE_SHIPPING_ADDRESS_PATH = '{0}/buyers/{1}/addresses/{2}';
	private static final String GET_PRODUCT_PATH = '{0}/products/{1}';
	private static final String OAUTH_PATH = '{0}/oauth/token';
	private static final Integer STATUS_CODE_SUCCESS = 200;
	private static final Integer STATUS_CODE_CREATED = 201;
	private static final Integer STATUS_CODE_NO_CONTENT = 204;
	private static final Integer PASSWORD_LENGTH = 10;
	private static final Integer AES_KEY_LENGTH = 192;
	private static final String AUTHORIZATION_HEADER = 'Authorization';
	private static final String BEARER_TEMPLATE = 'Bearer {0}';
	private static final String ID_TEMPLATE = '{0}{1}';
	private static final Integer ID_LENGTH = 10;
	private static final String PROVISIONING_FAILED_MESSAGE = 'OrderCloud User Provisioning Failed';
	private static final String GROUP_MEMBERSHIP_FAILED_MESSAGE = 'Failed to add user to group';
	private static final String IMPERSONATION_FAILED_MESSAGE = 'OrderCloud Impersonation Failed';
	private static final String ADMIN_AUTH_ERROR_MESSAGE = 'OrderCloud Admin authentication failed';
	private static final String ADDRESS_CREATE_ERROR_MESSAGE = 'OrderCloud Failed to Create Address';
	private static final String ADDRESS_ASSIGNMENT_ERROR_MESSAGE = 'OrderCloud Failed to Assign Address';
	private static final String LINE_ITEMS_ERROR_MESSAGE = 'OrderCloud Error retrieving line items';
	private static final String GET_PRODUCT_ERROR_MESSAGE = 'OrderCloud Error getting product';
	private static final String CREATE_SPENDING_ACCOUNT_ERROR = 'OrderCloud Error creating spending account';
	private static final String SPENDING_ACCOUNT_ASSIGNMENT_ERROR = 'OrderCloud Error assigning spending account';
	private static final String UPDATE_SPENDING_ACCOUNT_ERROR = 'OrderCloud Error updating spending account';
	private static final string UPDATE_SHIPPING_ADDRESS_ERROR = 'OrderCloud Error updating Shipping Address';
	private static final string ERROR_FETCHING_PAYMENTS = 'Error in fetching payment details for the orders';
	private static final Set<Integer> SUCCESS_CODES = new Set<Integer> { STATUS_CODE_CREATED, STATUS_CODE_SUCCESS, STATUS_CODE_NO_CONTENT };
	private static final List<String> IMPERSONATE_CLAIMS = new List<String> { 'FullAccess' };
	private static Shop_Settings__c shopSettings;
	private static User currentUser;
	private static final String PAYMENTS_PATH = '{0}/orders/incoming/{1}/payments'; //Order Cloud Payment details end point, Kumaresan M
	
	public static paymentDetails getOrderPayments(string orderId) {
		String at = getAdminAuthToken();
		JSONParser parser;
		Shop_Settings__c settings = getSettings();
		HttpRequest r = new HttpRequest();
		HttpResponse response;
		paymentDetails paymentRecord;

		r.setHeader(AUTHORIZATION_HEADER, String.format(BEARER_TEMPLATE,
		new List<String> { at }));
		r.setHeader(CONTENT_TYPE_HEADER, CONTENT_TYPE_JSON);
		r.setMethod(METHOD_GET);
		r.setEndpoint(String.format(PAYMENTS_PATH, new List<String> { settings.API_Endpoint__c, orderId })); 
		response = makeCallout(r);
		
		if (response == null || !isValidCode(response.getStatusCode())) {
			paymentRecord=new paymentDetails();
			paymentRecord.Error=ERROR_FETCHING_PAYMENTS;
			paymentRecord.orderId=orderId;
			return paymentRecord;
		}
		
		parser = JSON.createParser(response.getBody());
		paymentRecord= (paymentDetails)parser.readValueAs(paymentDetails.class);
		paymentRecord.orderId=orderId;
		
		if(Test.isRunningTest()) {
			paymentRecord.orderId='testId';
		}
		
		return paymentRecord;
	}
	
	public class paymentDetails {
		public string orderId;
		public string Error;    
		public List<paymentItem> Items;
	}
	
	public class paymentItem {
		public String ID;
		public string Type;
		public String DateCreated;
		public String CreditCardID;
		public String SpendingAccountID;
		public String Description;
		public String Amount;
		public Boolean Accepted;
		public string xp;
		public List<transactionDetail> Transactions;
	}
	
	public class transactionDetail {
		public String ID;
		public string Type;
		public String DateExecuted;
		public String Amount;
		public Boolean Succeeded;
		public String ResultCode;
		public String ResultMessage;
		public transac xp;
	}

	public class transac {
		public string TransID;
	}

	public static Boolean checkOrderCloudUser(Contact scope) {
		Boolean status = false;
		if(scope.Order_Cloud_Spending_Account_Id__c != '') {
			List<donation_split__Designation__c> classrooms = [SELECT Id,School__c FROM donation_split__Designation__c WHERE Teacher__c= :scope.Id LIMIT 1];
			if(!classrooms.isEmpty()) {   
				List<Account> affiliations = [SELECT Account_Status__c FROM Account WHERE Id =: classrooms[0].School__c];
				
				if(!affiliations.isEmpty() && affiliations[0].Account_Status__c =='Active') {
					status = true;
				} else {
					status = false;
				}  
			}
		}

		return status;
	}

	public static void updateSpendingAccount(String accessToken, OrderCloudSpendingAccount spendingAccount) {
		
		if (String.isBlank(accessToken) || spendingAccount == null) {
			return;
		}

		Shop_Settings__c settings = getSettings();
		HttpRequest r = new HttpRequest();
		HttpResponse response;
		OrderCloudProduct ocp;

		r.setHeader(AUTHORIZATION_HEADER, String.format(BEARER_TEMPLATE, new List<String> { accessToken }));
		r.setHeader(CONTENT_TYPE_HEADER, CONTENT_TYPE_JSON);
		r.setMethod(METHOD_PUT);
		r.setEndpoint(String.format(UPDATE_SPENDING_ACCOUNT_PATH, new List<String> { settings.API_Endpoint__c, settings.Buyer_Id__c, spendingAccount.ID }));
		r.setBody(JSON.serialize(spendingAccount));

		response = makeCallout(r);

		if (response == null || !isValidCode(response.getStatusCode())) {
			throw new ShopException(UPDATE_SPENDING_ACCOUNT_ERROR, ShopException.Type.UpdateSpendingAccount);
		}
	}
	
	public static void UpdateShippingAddress(string accessToken, ShopAddress shopAddress) {
		
		if (String.isBlank(accessToken) || shopAddress == null) {
			return;
		}
		Shop_Settings__c settings = getSettings();
		HttpRequest r = new HttpRequest();
		HttpResponse response;
		r.setHeader(AUTHORIZATION_HEADER, String.format(BEARER_TEMPLATE, new List<String> { accessToken }));
		r.setHeader(CONTENT_TYPE_HEADER, CONTENT_TYPE_JSON);
		r.setMethod(METHOD_PUT);
		r.setEndpoint(String.format(UPDATE_SHIPPING_ADDRESS_PATH, new List<String> { settings.API_Endpoint__c, settings.Buyer_Id__c, shopAddress.ID }));
		r.setBody(JSON.serialize(shopAddress));
		response = makeCallout(r);
		if (response == null || !isValidCode(response.getStatusCode())) {
			throw new ShopException(UPDATE_SHIPPING_ADDRESS_ERROR, ShopException.Type.UpdateShippingAddress);
		}
	}
	
	public static void createSpendingAccountAssignment(String accessToken, String userId, String spendingAccountId) {
		if (String.isBlank(accessToken) || String.isBlank(userId) || String.isBlank(spendingAccountId)) {
			return;
		}

		Shop_Settings__c settings = getSettings();
		HttpRequest r = new HttpRequest();
		HttpResponse response;
		OrderCloudProduct ocp;
		SpendingAccountAssignmentRequest request = new SpendingAccountAssignmentRequest(userId, spendingAccountId);

		r.setHeader(AUTHORIZATION_HEADER, String.format(BEARER_TEMPLATE, new List<String> { accessToken }));
		r.setHeader(CONTENT_TYPE_HEADER, CONTENT_TYPE_JSON);
		r.setMethod(METHOD_POST);
		r.setEndpoint(String.format(SPENDING_ACCOUNT_ASSIGNMENT_PATH, new List<String> { settings.API_Endpoint__c, settings.Buyer_Id__c }));
		r.setBody(JSON.serialize(request));

		response = makeCallout(r);

		if (response == null || !isValidCode(response.getStatusCode())) {
			String errorCode = '';
			if((response.getBody()).contains('ErrorCode')) {
				if((response.getBody()).containsIgnoreCase('ErrorCode":"')) {
					errorCode = (response.getBody()).split('ErrorCode":"')[1];
					errorCode = errorCode.split('"')[0];
				}
			}

			String errorMessage = '';
			if((response.getBody()).contains('Message')) {
				if((response.getBody()).containsIgnoreCase('Message":"')) {
					errorMessage = (response.getBody()).split('Message":"')[1];
					errorMessage = errorMessage.split('"')[0];
				}
			}

			String errorData = '';
			if((response.getBody()).contains('Data')) {
				if((response.getBody()).containsIgnoreCase('Data":')) {
					errorData = (response.getBody()).split('Data":')[1];
					errorData = errorData.split('}}]')[0];
					errorData = errorData.replace('{', '');
				}
			}

			throw new ShopException(SPENDING_ACCOUNT_ASSIGNMENT_ERROR + (errorMessage!='' ? (': ' + errorMessage + (errorData != '' ? ' (Data: ' + errorData + ')' : '') ) : ''),
			ShopException.Type.SpendingAccountAssignment);
		}
	}

	public static String createSpendingAccount(String accessToken, OrderCloudSpendingAccount spendingAccount) {
		
		if (String.isBlank(accessToken) || spendingAccount == null) {
			return null;
		}

		JSONParser parser;
		Shop_Settings__c settings = getSettings();
		HttpRequest r = new HttpRequest();
		HttpResponse response;
		OrderCloudProduct ocp;

		spendingAccount.ID = generateId();

		r.setHeader(AUTHORIZATION_HEADER, String.format(BEARER_TEMPLATE, new List<String> { accessToken }));
		r.setHeader(CONTENT_TYPE_HEADER, CONTENT_TYPE_JSON);
		r.setMethod(METHOD_POST);
		r.setEndpoint(String.format(CREATE_SPENDING_ACCOUNT_PATH, new List<String> { settings.API_Endpoint__c, settings.Buyer_Id__c }));
		r.setBody(JSON.serialize(spendingAccount));

		response = makeCallout(r);

		if (response == null || !isValidCode(response.getStatusCode())) {
			String errorCode = '';
			if((response.getBody()).contains('ErrorCode')) {
				if((response.getBody()).containsIgnoreCase('ErrorCode":"')) {
					errorCode = (response.getBody()).split('ErrorCode":"')[1];
					errorCode = errorCode.split('"')[0];
				}
			}

			String errorMessage = '';
			if((response.getBody()).contains('Message')) {
				if((response.getBody()).containsIgnoreCase('Message":"')) {
					errorMessage = (response.getBody()).split('Message":"')[1];
					errorMessage = errorMessage.split('"')[0];
				}
			}

			String errorData = '';
			if((response.getBody()).contains('Data')) {
				if((response.getBody()).containsIgnoreCase('Data":')) {
					errorData = (response.getBody()).split('Data":')[1];
					errorData = errorData.split('}}]')[0];
					errorData = errorData.replace('{', '');
				}
			}
			
			throw new ShopException(CREATE_SPENDING_ACCOUNT_ERROR + (errorMessage!='' ? (': ' + errorMessage + (errorData != '' ? ' (Data: ' + errorData + ')' : '') ) : ''),
			ShopException.Type.CreateSpendingAccount);
		}

		return spendingAccount.ID;
	}

	public static OrderCloudProduct getProduct(String accessToken, String productId) {
		if (String.isBlank(accessToken) || String.isBlank(productId)) {
			return null;
		}

		JSONParser parser;
		Shop_Settings__c settings = getSettings();
		HttpRequest r = new HttpRequest();
		HttpResponse response;
		OrderCloudProduct ocp;

		r.setHeader(AUTHORIZATION_HEADER, String.format(BEARER_TEMPLATE, new List<String> { accessToken }));
		r.setHeader(CONTENT_TYPE_HEADER, CONTENT_TYPE_JSON);
		r.setMethod(METHOD_GET);
		r.setEndpoint(String.format(GET_PRODUCT_PATH, new List<String> { settings.API_Endpoint__c, productId }));

		response = makeCallout(r);

		if (response == null || !isValidCode(response.getStatusCode())) {
			throw new ShopException(GET_PRODUCT_ERROR_MESSAGE,
			ShopException.Type.GetProduct);
		}

		parser = JSON.createParser(response.getBody());

		ocp = (OrderCloudProduct) parser.readValueAs(OrderCloudProduct.class);
		return ocp;
	}

	public static List<OrderCloudLineItem> getLineItemsForOrder( String accessToken, String orderId) {
		
		if (String.isBlank(accesstoken) || String.isBlank(orderId)) {
			return null;
		}

		JSONParser parser;
		Shop_Settings__c settings = getSettings();
		HttpRequest r = new HttpRequest();
		HttpResponse response;
		LineItemResponse liResponse;

		r.setHeader(AUTHORIZATION_HEADER, String.format(BEARER_TEMPLATE, new List<String> { accessToken }));
		r.setHeader(CONTENT_TYPE_HEADER, CONTENT_TYPE_JSON);
		r.setMethod(METHOD_GET);
		r.setEndpoint(String.format(LINE_ITEMS_PATH, new List<String> { settings.API_Endpoint__c, settings.Buyer_Id__c, orderId })); 
		
		response = makeCallout(r);
		if (response == null || !isValidCode(response.getStatusCode())) {
			throw new ShopException(LINE_ITEMS_ERROR_MESSAGE,
			ShopException.Type.LineItems);
		}

		parser = JSON.createParser(response.getBody());
		liResponse = (LineItemResponse) parser.readValueAs(LineItemResponse.class);
		
		return liResponse.Items;
	}

	public static void processSyncQueue() {
		List<Order_Cloud_Sync__c> syncQueue = [SELECT Id, Order_Id__c, Request_Body__c, Sync_In_Progress__c, Sync_Completed__c FROM Order_Cloud_Sync__c WHERE Sync_Completed__c != true];

		for (Order_Cloud_Sync__c ocs : syncQueue) {
			ocs.Sync_In_Progress__c = true;
		}

		update syncQueue;

		for (Order_Cloud_Sync__c ocs : syncQueue) {
			if (!Test.isRunningTest()) {
				System.enqueueJob(new OrderSyncQueueable(ocs));
			}
		}
	}

	@future(callout=true)
	public static void handleLineItem(String orderId, String lineItemString) {
		
		if (String.isBlank(lineItemString)) {
			return;
		}

		Id pricebookEntryId;
		OpportunityLineItem oli;
		JSONParser parser = JSON.createParser(lineItemString);
		OrderCloudLineItem lineItem = (OrderCloudLineItem) parser.readValueAs(
		OrderCloudLineItem.class);
		Opportunity opportunity = getOpportunityByCloudId(orderId);
		Product2 p;
		if(lineItem.ProductID == 'AACPunchoutProduct') {
			p = getProductByOrderCloudId(lineitem.xp.SupplierPartID,lineitem.xp.PunchoutName);
		} else {
			p = getProductByOrderCloudId(lineItem.ProductID,lineItem.product.xp.VendorName);
		}
		if (p == null) {
			Shop_Settings__c settings = getSettings();
			Product2 newProduct = new Product2();
			if(lineItem.productID == 'AACPunchoutProduct') {        
				newProduct.Name = lineitem.xp.Description;
				newProduct.Order_Cloud_Product_Id__c = lineitem.xp.SupplierPartID;
				newProduct.Description = lineitem.xp.Description;
				newProduct.Vendor_Name__c = lineitem.xp.PunchoutName;
				newProduct.ProductCode = lineitem.xp.SupplierPartAuxiliaryID;
				newProduct.PunchoutProduct__c = True;  
				newProduct.IsActive = true;                  
			} else {
				newProduct.Name = lineItem.product.Name;
				newProduct.Order_Cloud_Product_Id__c = lineItem.product.ID;
				newProduct.Description = lineItem.product.Description;
				newProduct.Vendor_Name__c = lineItem.product.xp.VendorName;
				newProduct.ProductCode = lineItem.product.xp.SecondaryItemNo;
				newProduct.PunchoutProduct__c = false;
				newProduct.IsActive = true;
			}
			insert newProduct;
		} else {
			pricebookEntryId = p.PricebookEntries[0].Id;
			oli = new OpportunityLineItem(
			PricebookEntryId = pricebookEntryId,
			OpportunityId = opportunity.Id,
			Quantity = lineItem.Quantity,
			UnitPrice = lineItem.UnitPrice,
			VendorOrderId__c = lineItem.xp.vendorOrderId.get(0));
			insert oli;
		}
	}

	public static void createOpportunityFromQueue(Order_Cloud_Sync__c orderSync) {
		
		if (orderSync == null) {
			return;
		}

		JSONParser parser = JSON.createParser(orderSync.Request_Body__c);
		OrderCloudOrder cloudOrder = (OrderCloudOrder) parser.readValueAs( OrderCloudOrder.class);
		Opportunity opportunity = generateOpportunityFromCloudOrder(cloudOrder);
		User u = getUserFromId(cloudOrder.FromUserID);
		RecordType rt = getRecordTypeByName(OPPORTUNITY_ORDER_RECORD_TYPE);
		Contact c;
		String at = getAdminAuthToken();
		List<OrderCloudLineItem> lineItems = getLineItemsForOrder(at,
		orderSync.Order_Id__c);

		if (u == null) {
			return;
		}

		c = (Contact) QueryUtils.queryObject(u.ContactId);

		opportunity.AccountId = c.AccountId;
		opportunity.School__c = c.npsp__Primary_Affiliation__c;
		opportunity.Name = String.format(ORDER_NAME_TEMPLATE,
		new List<String> { c.Name, Datetime.now().format() });
		opportunity.RecordTypeId = rt.Id;
		opportunity.Teacher__c = c.Id;

		upsert opportunity Order_Cloud_Order_Id__c;
		if (!Test.isRunningTest()) {
			InsertOpportunityProd iop = new InsertOpportunityProd(orderSync,lineItems);
			database.executebatch(iop);  
		}
		orderSync.Sync_Completed__c = true;
		update orderSync;
	}

	public static User getCurrentUser(Id userId) {
		
		if (String.isBlank(userId)) {
			return null;
		}

		if (currentUser != null) {
			return currentUser;
		}

		currentUser = [SELECT Id, Name, ContactId, Shop_User_Created__c FROM User WHERE Id = :userId];

		return getCurrentUser(userId);
	}

	public static void createAddressAssignment(String accesstoken, Id userId, ShopAddress address) {
		
		if (String.isBlank(accesstoken) || String.isBlank(userId) || address == null || !address.isComplete()) {
			return;
		}

		Shop_Settings__c settings = getSettings();
		HttpRequest r = new HttpRequest();
		HttpResponse response;
		AddressAssignmentRequest request = new AddressAssignmentRequest(userId,
		address.ID, address.getType());
		r.setHeader(AUTHORIZATION_HEADER, String.format(BEARER_TEMPLATE, new List<String> { accessToken }));
		r.setHeader(CONTENT_TYPE_HEADER, CONTENT_TYPE_JSON);
		r.setMethod(METHOD_POST);
		r.setEndpoint(String.format(ADDRESS_GROUP_PATH, new List<String> { settings.API_Endpoint__c, settings.Buyer_Id__c }));
		r.setBody(JSON.serialize(request));
		
		response = makeCallout(r);

		
		
		if (response == null || !isValidCode(response.getStatusCode())) {
			String errorCode = '';
			if((response.getBody()).contains('ErrorCode')) {
				if((response.getBody()).containsIgnoreCase('ErrorCode":"')) {
					errorCode = (response.getBody()).split('ErrorCode":"')[1];
					errorCode = errorCode.split('"')[0];
				}
			}

			String errorMessage = '';
			if((response.getBody()).contains('Message')) {
				if((response.getBody()).containsIgnoreCase('Message":"')) {
					errorMessage = (response.getBody()).split('Message":"')[1];
					errorMessage = errorMessage.split('"')[0];
				}
			}

			String errorData = '';
			if((response.getBody()).contains('Data')) {
				if((response.getBody()).containsIgnoreCase('Data":')) {
					errorData = (response.getBody()).split('Data":')[1];
					errorData = errorData.split('}}]')[0];
					errorData = errorData.replace('{', '');
				}
			}
			throw new ShopException(ADDRESS_ASSIGNMENT_ERROR_MESSAGE + (errorMessage!='' ? (': ' + errorMessage + (errorData != '' ? ' (Data: ' + errorData + ')' : '') ) : ''), ShopException.Type.AddressAssignment);
		}
	}

	public static String createAddress(String accesstoken, ShopAddress address) {
		if (String.isBlank(accesstoken) || address == null || !address.isComplete()) {
			return null;
		}

		Shop_Settings__c settings = getSettings();
		HttpRequest r = new HttpRequest();
		HttpResponse response;
		Long currentTimeStamp = Datetime.now().getTime();
		String addressId = generateId();

		address.ID = addressId;
		
		r.setHeader(AUTHORIZATION_HEADER, String.format(BEARER_TEMPLATE, new List<String> { accessToken }));
		r.setHeader(CONTENT_TYPE_HEADER, CONTENT_TYPE_JSON);
		r.setMethod(METHOD_POST);
		r.setEndpoint(String.format(CREATE_ADDRESS_PATH, new List<String> { settings.API_Endpoint__c, settings.Buyer_Id__c }));
		r.setBody(JSON.serialize(address));

		response = makeCallout(r);

		

		if (response == null || !isValidCode(response.getStatusCode())) {
			String errorCode = '';
			if((response.getBody()).contains('ErrorCode')) {
				if((response.getBody()).containsIgnoreCase('ErrorCode":"')) {
					errorCode = (response.getBody()).split('ErrorCode":"')[1];
					errorCode = errorCode.split('"')[0];
				}
			}

			String errorMessage = '';
			if((response.getBody()).contains('Message')) {
				if((response.getBody()).containsIgnoreCase('Message":"')) {
					errorMessage = (response.getBody()).split('Message":"')[1];
					errorMessage = errorMessage.split('"')[0];
				}
			}

			String errorData = '';
			if((response.getBody()).contains('Data')) {
				if((response.getBody()).containsIgnoreCase('Data":')) {
					errorData = (response.getBody()).split('Data":')[1];
					errorData = errorData.split('}}]')[0];
					errorData = errorData.replace('{', '');
				}
			}
			throw new ShopException(ADDRESS_CREATE_ERROR_MESSAGE + (errorMessage!='' ? (': ' + errorMessage + (errorData != '' ? ' (Data: ' + errorData + ')' : '') ) : ''), ShopException.Type.Address);
		}
		return addressId;
	}

	public static String getImpersonationAccessToken(String accessToken, Id userId) {
			
		if (String.isBlank(accessToken) || String.isBlank(userId)) {
			return null;
		}

		Shop_Settings__c settings = getSettings();
		HttpRequest r = new HttpRequest();
		AccessTokenRequest request = new AccessTokenRequest(
		settings.Buyer_Client_Id__c);
		TokenResponse tResponse;
		HttpResponse response;
		JSONParser parser;
		r.setHeader(AUTHORIZATION_HEADER, String.format(BEARER_TEMPLATE, new List<String> { accessToken }));
		r.setHeader(CONTENT_TYPE_HEADER, CONTENT_TYPE_JSON);
		r.setMethod(METHOD_POST);
		r.setEndpoint(String.format(IMPERSONATION_PATH, new List<String> { settings.API_Endpoint__c, settings.Buyer_Id__c, userId }));
		r.setBody(JSON.serialize(request));
		response = makeCallout(r);
		if (response == null || !isValidCode(response.getStatusCode())) {
			String errorCode = '';
			if((response.getBody()).contains('ErrorCode')) {
				if((response.getBody()).containsIgnoreCase('ErrorCode":"')) {
					errorCode = (response.getBody()).split('ErrorCode":"')[1];
					errorCode = errorCode.split('"')[0];
				}
			}

			String errorMessage = '';
			if((response.getBody()).contains('Message')) {
				if((response.getBody()).containsIgnoreCase('Message":"')) {
					errorMessage = (response.getBody()).split('Message":"')[1];
					errorMessage = errorMessage.split('"')[0];
				}
			}

			String errorData = '';
			if((response.getBody()).contains('Data')) {
				if((response.getBody()).containsIgnoreCase('Data":')) {
					errorData = (response.getBody()).split('Data":')[1];
					errorData = errorData.split('}}]')[0];
					errorData = errorData.replace('{', '');
				}
			}

			throw new ShopException(IMPERSONATION_FAILED_MESSAGE + (errorMessage!='' ? (': ' + errorMessage + (errorData != '' ? ' (Data: ' + errorData + ')' : '') ) : ''),
			ShopException.Type.Impersonation);
		}

		parser = JSON.createParser(response.getBody());

		tResponse = (TokenResponse) parser.readValueAs(TokenResponse.class);

		return tResponse.access_token;
	}

	public static void addUserToGroup(String accessToken, Id userId) {
		if (String.isBlank(accessToken) || String.isBlank(userId)) {
			return;
		}

		Shop_Settings__c settings = getSettings();
		HttpResponse response;
		HttpRequest r = new HttpRequest();
		UserGroupRequest request = new UserGroupRequest(userId, settings.Buyer_Group_Id__c);

		r.setHeader(AUTHORIZATION_HEADER, String.format(BEARER_TEMPLATE, new List<String> { accessToken }));
		r.setHeader(CONTENT_TYPE_HEADER, CONTENT_TYPE_JSON);
		r.setMethod(METHOD_POST);
		r.setEndpoint(String.format(USER_GROUP_PATH, new List<String> { settings.API_Endpoint__c, settings.Buyer_Id__c }));
		r.setBody(JSON.serialize(request));

		response = makeCallout(r);

		if (response == null || !isValidCode(response.getStatusCode())) {
			throw new ShopException(GROUP_MEMBERSHIP_FAILED_MESSAGE,
			ShopException.Type.UserGroup);
		}
	}

	public static String provisionUser(String accessToken, Id userId) {
		
		if (String.isBlank(userId) || String.isBlank(accessToken)) {
			return null;
		}

		User u = getUserDetails(userId);
		Shop_Settings__c settings = getSettings();
		HttpRequest r = new HttpRequest();
		String password = generatePassword();
		HttpResponse response;
		UserProvisionRequest request = new UserProvisionRequest(u, password, true);
		String endPoint = String.format(USER_PROVISION_PATH, new List<String> {
		settings.API_Endpoint__c, settings.Buyer_Id__c });

		r.setMethod(METHOD_POST);
		r.setEndpoint(endPoint);
		r.setHeader(AUTHORIZATION_HEADER, String.format(BEARER_TEMPLATE, new List<String> { accessToken }));
		r.setBody(JSON.serialize(request));
		r.setHeader(CONTENT_TYPE_HEADER, CONTENT_TYPE_JSON);
		
		response = makeCallout(r);
		
		String errorCode = '';
		if((response.getBody()).contains('ErrorCode')) {
			if((response.getBody()).containsIgnoreCase('ErrorCode":"')) {
				errorCode = (response.getBody()).split('ErrorCode":"')[1];
				errorCode = errorCode.split('"')[0];
			}
		}

		String errorMessage = '';
		if((response.getBody()).contains('Message')) {
			if((response.getBody()).containsIgnoreCase('Message":"')) {
				errorMessage = (response.getBody()).split('Message":"')[1];
				errorMessage = errorMessage.split('"')[0];
			}
		}

		String errorData = '';
		if((response.getBody()).contains('Data')) {
			if((response.getBody()).containsIgnoreCase('Data":')) {
				errorData = (response.getBody()).split('Data":')[1];
				errorData = errorData.split('}}]')[0];
				errorData = errorData.replace('{', '');
			}
		}
		

		if (response == null || !isValidCode(response.getStatusCode())) {
			throw new ShopException(PROVISIONING_FAILED_MESSAGE + (errorMessage!='' ? (': ' + errorMessage + (errorData != '' ? ' (Data: ' + errorData + ')' : '') ) : ''), ShopException.Type.Provisioning);
		}

		return password;
	}

	public static String getAdminAuthToken() {
		
		HttpResponse response;
		TokenResponse tResponse;
		JSONParser parser;
		HttpRequest request = new HttpRequest();
		Shop_Settings__c settings = getSettings();
		Map<String, String> params = new Map<String, String>();

		params.put(CLIENT_ID_PARAM, settings.Client_Id__c);
		params.put(GRANT_TYPE_PARAM, GRANT_TYPE_CREDENTIALS);
		params.put(CLIENT_SECRET_PARAM, settings.Client_Secret__c);
		params.put(SCOPE_PARAM, SCOPE_TYPE_FULL);
		request.setEndpoint(String.format(OAUTH_PATH, new List<String> {
		settings.OAuth_Endpoint__c }));
		request.setMethod(METHOD_POST);
		request.setBody(generateQueryStringFromParams(params));
		response = makeCallout(request);


		if (response == null || !isValidCode(response.getStatusCode())) {
			String errorCode = '';
			if((response.getBody()).contains('ErrorCode')) {
				if((response.getBody()).containsIgnoreCase('ErrorCode":"')) {
					errorCode = (response.getBody()).split('ErrorCode":"')[1];
					errorCode = errorCode.split('"')[0];
				}
			}

			String errorMessage = '';
			if((response.getBody()).contains('Message')) {
				if((response.getBody()).containsIgnoreCase('Message":"')) {
					errorMessage = (response.getBody()).split('Message":"')[1];
					errorMessage = errorMessage.split('"')[0];
				}
			}

			String errorData = '';
			if((response.getBody()).contains('Data')) {
				if((response.getBody()).containsIgnoreCase('Data":')) {
					errorData = (response.getBody()).split('Data":')[1];
					errorData = errorData.split('}}]')[0];
					errorData = errorData.replace('{', '');
				}
			}
			throw new ShopException(ADMIN_AUTH_ERROR_MESSAGE + (errorMessage!='' ? (': ' + errorMessage + (errorData != '' ? ' (Data: ' + errorData + ')' : '') ) : ''), ShopException.Type.Admin);
		}

		parser = JSON.createParser(response.getBody());

		tResponse = (TokenResponse) parser.readValueAs(TokenResponse.class);

		return tResponse.access_token;
	}

	public static Shop_Settings__c getSettings() {
		if (shopSettings != null) {
			return shopSettings;
		}

		shopSettings = Shop_Settings__c.getInstance();
		return getSettings();
	}

	private static User getUserFromId(Id userId) {
		return (User) QueryUtils.queryObject(userId);
	}

	private static RecordType getRecordTypeByName(String devName) {
		return [SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName = :devName];
	}

	public static Id createProduct(OrderCloudLineItem lineItem) {
		
		Shop_Settings__c settings = getSettings();
		Product2 newProduct = new Product2();
		newProduct.Name = lineItem.product.Name;
		newProduct.Order_Cloud_Product_Id__c = lineItem.product.ID;
		newProduct.Description = lineItem.product.Description;
		newProduct.Vendor_Name__c = lineItem.product.xp.VendorName;
		newProduct.ProductCode = lineItem.product.xp.SecondaryItemNo;
		newProduct.PunchoutProduct__c = false;
		insert newProduct;

		PricebookEntry pbe = new PricebookEntry();
		pbe.Product2Id = newProduct.Id;
		pbe.Pricebook2Id = settings.Pricebook_Id__c;
		pbe.UnitPrice = 0;
		pbe.IsActive = true;
		insert pbe;

		return pbe.Id;  
	}

	private static Opportunity generateOpportunityFromCloudOrder( OrderCloudOrder oco) {
		return new Opportunity(
		Order_Cloud_Order_Id__c = oco.ID,
		Amount = oco.Total,
		Subtotal__c = oco.Subtotal,
		ShippingCost__c = oco.ShippingCost,
		DateOrderCreated__c = oco.DateCreated,
		DateOrderSubmitted__c = oco.DateSubmitted,
		OrderCloudLineItemCount__c = oco.LineItemCount,
		StageName = OPPORTUNITY_STAGE,
		CloseDate = Date.today(),
		TaxCost__c = oco.TaxCost,
		PromotionDiscount__c = oco.PromotionDiscount);
	}

	public static Opportunity getOpportunityByCloudId(String cloudId) {
		return [SELECT Id, Name FROM Opportunity WHERE Order_Cloud_Order_Id__c = :cloudId];
	}

	private static HttpResponse makeCallout(HttpRequest request) {
		Http http = new Http();

		try {
			return http.send(request);
		} catch (Exception e) {
			System.debug(e.getMessage());
			return null;
		}
	}

	private static User getUserDetails(Id userId) {
		return [SELECT Id, Username, Email, FirstName, LastName, Phone FROM User WHERE Id = :userId];
	}

	private static String generateQueryStringFromParams( Map<String, String> params) {
		
		String queryString = '';
		String currentParam;
		Integer counter = 0;

		for (String p : params.keySet()) {
			currentParam = String.format(PARAM_TEMPLATE, new List<String> { p, params.get(p) });
			queryString += (counter == 0) ? currentParam : String.format( SEPARATOR_TEMPLATE, new List<String> { currentParam });
			counter++;
		}

		return queryString;
	}

	private static String generateId() {
		return String.format(ID_TEMPLATE, new List<String> { generateAlphaNumericString(ID_LENGTH), String.valueOf(Datetime.now().getTime()) });
	}

	public static String generatePassword() {
		return oCValidPassword('');
	}

	public static String oCValidPassword(String pwd) {
		if(!oCPasswordValidator(pwd)) {
			pwd = generateRandomString(PASSWORD_LENGTH);
			pwd = oCValidPassword(pwd);
		}

		return pwd;
	}

	public static Boolean oCPasswordValidator(String pwd) {
		Boolean isValid = true;
		
		//Check Empty
		if(String.isBlank(pwd)) {
			isValid = false;
		//Check lenght
		} else if(pwd.length() < 10 || pwd.length() > 100) {
			isValid = false;
		//Check at least one lowercase character, at least one uppercase character, at least one numeric value, at least one special character
		} else if(!Pattern.matches('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[-+_!@#$%^&*.,?]).+$', pwd)) {
			isValid = false;
		}

		return isValid;
	}

	private static String generateAlphaNumericString(Integer length) {
		String generatedString = generateRandomString(length);
		Pattern nonAlphanumeric = Pattern.compile(ALPHA_REGEX);
		Matcher matcher = nonAlphanumeric.matcher(generatedString);

		return matcher.replaceAll('');
	}

	private static String generateRandomString(Integer length) {
		
		Blob blobKey = Crypto.generateAesKey(AES_KEY_LENGTH);
		String key = EncodingUtil.base64encode(blobKey);
		return key.substring(0, length);
	}

	private static Boolean isValidCode(Integer c) {
		
		return SUCCESS_CODES.contains(c);
	}
	
	public static Product2 getProductByOrderCloudId(String orderCloudId,String vendorName) {
		
		Shop_Settings__c settings = getSettings();

		try {
			return [SELECT Id, Name, Order_Cloud_Product_Id__c, (SELECT Id, Pricebook2Id FROM PricebookEntries WHERE Pricebook2Id = :settings.Pricebook_Id__c) FROM Product2 WHERE Order_Cloud_Product_Id__c = :orderCloudId AND Vendor_Name__c=:vendorName];
		} catch (Exception e) {
			return null;
		}
	}

	public class AccessTokenRequest {
		
		public String ClientID { get; set; }
		public List<String> roles { get; set; }
		public AccessTokenRequest(String clientId) {
			this.ClientID = clientId;
			this.roles = IMPERSONATE_CLAIMS;
		}
	}

	public class LineItemResponse {
		
		public List<OrderCloudLineItem> Items { get; set; }
	}

	public class AddressAssignmentRequest {
		
		public Id UserID { get; set; }
		public String AddressID { get; set; }
		public Boolean IsShipping { get; set; }
		public Boolean IsBilling { get; set; }

		public AddressAssignmentRequest(Id userId, String addressId, ShopAddress.AddressType addressType) {
			
			this.UserID = userId;
			this.AddressID = addressId;
			this.IsShipping = false;
			this.IsBilling = false;

			if (addressType == ShopAddress.AddressType.Shipping) {
				this.IsShipping = true;
			} else {
				this.IsBilling = true;
			}
		}
	}

	public class SpendingAccountAssignmentRequest {
		
		public String SpendingAccountID { get; set; }
		public String UserID { get; set; }

		public SpendingAccountAssignmentRequest(String userId, String spendingAccountId) {
			this.SpendingAccountID = spendingAccountId;
			this.UserID = userId;
		}
	}

	public class UserGroupRequest {
		
		public Id UserID { get; set; }
		public String UserGroupID { get; set; }

		public UserGroupRequest(Id userId, String groupId) {
			this.UserID = userId;
			this.UserGroupID = groupId;
		}
	}

	public class UserProvisionRequest {
		
		public String ID { get; set; }
		public String Username { get; set; }
		public String Password { get; set; }
		public String FirstName { get; set; }
		public String LastName { get; set; }
		public String Phone { get; set; }
		public String Email { get; set; }
		public Boolean Active { get; set; }
		public Datetime TermsAccepted { get; set; }
		public String xp { get; set; }

		public UserProvisionRequest(User u, String password, Boolean active) {
			this.Active = active;
			this.Username = u.Username;
			this.Password = password;
			this.FirstName = u.FirstName;
			this.LastName = u.LastName;
			this.Phone = u.Phone;
			this.Email = u.Email;
			this.ID = u.Id;
		}
	}

	public class TokenResponse {
		
		public String access_token { get; set; }
	}

	public class CustomException extends Exception {}
}