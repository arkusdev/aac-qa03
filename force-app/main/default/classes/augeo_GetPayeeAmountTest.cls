@isTest
public class augeo_GetPayeeAmountTest
{
    public static testMethod void getpayeeamt()
    {
        AcctSeed__Cash_Disbursement_Batch__c cashDisBatch = new AcctSeed__Cash_Disbursement_Batch__c(AcctSeed__Starting_Check_Number__c = 1, Name = 'Test Batch');
        insert cashdisBatch; 

 // 1 Cash GL-Account
        AcctSeed__GL_Account__c glAccount = new AcctSeed__GL_Account__c(Name = '1000-Cash', AcctSeed__Active__c = true, AcctSeed__Type__c = 'Balance Sheet', AcctSeed__Bank__c = true, AcctSeed__Sub_Type_1__c = 'Assets', AcctSeed__Sub_Type_2__c = 'Cash');
        insert glAccount;
        
        Account acct = new Account(
                 Name = 'Test Account');
         insert acct;
 
 // insert 1 Accounting Period         
          AcctSeed__Accounting_Period__c acctPeriod = new AcctSeed__Accounting_Period__c(
                 Name = '1991-10',
                 AcctSeed__Start_Date__c = Date.newinstance(1991, 10, 1),
                 AcctSeed__End_Date__c = Date.newinstance(1991, 10, 31),
                 AcctSeed__Status__c = 'Open');
         insert acctPeriod;
         
 // 1 Cash GL-Account         
          AcctSeed__GL_Account__c cashGL = new AcctSeed__GL_Account__c(
                 Name = '1000-Cash',
                 AcctSeed__Active__c = true,
                 AcctSeed__Type__c = 'Balance Sheet',
                 AcctSeed__Bank__c = true,
                 AcctSeed__Sub_Type_1__c = 'Assets',
                 AcctSeed__Sub_Type_2__c = 'Cash');
         insert CashGL;
 
  // insert 4 billing formats
         AcctSeed__Billing_Format__c[] billingFormats = new List<AcctSeed__Billing_Format__c>();
         billingFormats.add(
                 new AcctSeed__Billing_Format__c(
                         Name = 'Default Billing Product',
                         AcctSeed__Visualforce_PDF_Page__c = 'BillingProductPDF',
                         AcctSeed__Default_Email_Template__c = [Select Id, DeveloperName From EmailTemplate limit 1].DeveloperName,
                         AcctSeed__Type__c = 'Billing',
                         AcctSeed__Sort_Field__c = 'Name'
                 )
         );
 
         billingFormats.add(
                 new AcctSeed__Billing_Format__c(
                         Name = 'Default Billing Service',
                         AcctSeed__Visualforce_PDF_Page__c = 'BillingServicePDF',
                         AcctSeed__Default_Email_Template__c = [Select Id, DeveloperName From EmailTemplate limit 1].DeveloperName,
                         AcctSeed__Type__c = 'Billing',
                         AcctSeed__Sort_Field__c = 'Name'
                 )
         );
 
         billingFormats.add(
                 new AcctSeed__Billing_Format__c (
                         Name = 'Billing Outstanding Statement',
                         AcctSeed__Visualforce_PDF_Page__c = 'BillingOutstandingStatementPDF',
                         AcctSeed__Default_Email_Template__c = [Select Id, DeveloperName From EmailTemplate limit 1].DeveloperName,
                         AcctSeed__ReplyTo_Email__c = 'test3463464364646@gmail.com',
                         AcctSeed__Type__c = 'Outstanding Statement'
                 )
         );
 
         billingFormats.add(
                 new AcctSeed__Billing_Format__c (
                         Name = 'Billing Activity Statement',
                         AcctSeed__Visualforce_PDF_Page__c = 'BillingActivityStatementPDF',
                         AcctSeed__Default_Email_Template__c = [Select Id, DeveloperName From EmailTemplate limit 1].DeveloperName,
                         AcctSeed__ReplyTo_Email__c = 'test3463464364646@gmail.com',
                         AcctSeed__Type__c = 'Activity Statement'
                 )
         );
 
         billingFormats.add(
                 new AcctSeed__Billing_Format__c(
                         Name = 'Default Purchase Order',
                         AcctSeed__Visualforce_PDF_Page__c = 'BillingServicePDF',
                         AcctSeed__Default_Email_Template__c = [Select Id, DeveloperName From EmailTemplate limit 1].DeveloperName,
                         AcctSeed__Type__c = 'Purchase Order',
                         AcctSeed__Sort_Field__c = 'Name'
                 )
         );
 
         billingFormats.add(
                 new AcctSeed__Billing_Format__c(
                         Name = 'Default Packing Slip',
                         AcctSeed__Visualforce_PDF_Page__c = 'BillingServicePDF',
                         AcctSeed__Default_Email_Template__c = [Select Id, DeveloperName From EmailTemplate limit 1].DeveloperName,
                         AcctSeed__Type__c = 'Packing Slip',
                         AcctSeed__Sort_Field__c = 'Name'
                 )
         );
 
         insert billingFormats;
 
 // insert 1 Ledger records         
          AcctSeed__Ledger__c ledger = new AcctSeed__Ledger__c(
                 Name = 'Actual',
                 AcctSeed__Type__c = 'Transactional',
                 AcctSeed__Default_Bank_Account__c = CashGL.id,
                 AcctSeed__Default_Billing_Format__c = billingFormats[0].Id,
                 AcctSeed__Billing_Outstanding_Statement_Format__c = billingFormats[2].Id,
                 AcctSeed__Billing_Activity_Statement_Format__c = billingFormats[3].Id,
                 AcctSeed__Default_Purchase_Order_Format__c = billingFormats[4].Id,
                 AcctSeed__Default_Packing_Slip_Format__c = billingFormats[5].Id);
         insert ledger;

        AcctSeed__Cash_Disbursement__c acd = new AcctSeed__Cash_Disbursement__c();
        // Add all required fields
        acd.AcctSeed__Vendor__c = acct.id;
        acd.AcctSeed__Accounting_Period__c = acctPeriod.id; 
        acd.AcctSeed__Ledger__c = ledger.id;
        acd.AcctSeed__Cash_Disbursement_Batch__c = cashDisBatch.Id;
        acd.AcctSeed__Status__c='Posted';
        acd.AcctSeed__Payment_Status__c = 'Paid';
        acd.AcctSeed__Type__c = 'Check';
        acd.AcctSeed__Disbursement_Date__c = system.today();
        acd.AcctSeed__Amount__c = 1.0;
        acd.AcctSeed__Bank_Account__c =glAccount.Id;
        acd.AcctSeed__Source__c = 'Manual';
        insert acd;

        augeo_GetPayeeAmount testCont = new augeo_GetPayeeAmount ();
        List<AcctSeed__Cash_Disbursement__c> lstcdpayeeamt = testCont.getpayeeamt();
       System.assert(lstcdpayeeamt!= null);
        //System.assertEquals(1, lstcdpayeeamt.size());
        //System.assertEquals(cashDisBatch.Id , lstcdpayeeamt[0].AcctSeed__Cash_Disbursement_Batch__c  );
    }
}