@isTest private class ClassroomTriggerHandlerTest {
    
    @isTest static void TestTriggerRunWithActiveClassroom() {
        
        Contact con = new Contact();
        con.LastName = 'Test';
        insert con;
        
        Announcement__c ann = new Announcement__c();
        ann.Teacher_School__c = con.Id;
        ann.Comment__c = 'Test';
        insert ann;
        
        donation_split__Designation__c classroom = new donation_split__Designation__c();
        classroom.Description__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		classroom.How_will_you_spend_your_funds__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		classroom.Why_We_Need_Your_Help__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		classroom.Teacher__c = con.Id;
        classroom.donation_split__Active__c = true;
                                            
        Test.startTest();
        insert classroom;
        Test.stopTest();
    }

    @isTest static void TestTriggerRunWithInactiveClassroom() {
        
        Contact con = new Contact();
        con.LastName = 'Test';
        insert con;
        
        Announcement__c ann = new Announcement__c();
        ann.Teacher_School__c = con.Id;
        ann.Comment__c = 'Test';
        insert ann;
        
        donation_split__Designation__c classroom = new donation_split__Designation__c();
        classroom.Description__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		classroom.How_will_you_spend_your_funds__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		classroom.Why_We_Need_Your_Help__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		classroom.Teacher__c = con.Id;
        classroom.donation_split__Active__c = false;
        insert classroom;

        classroom.donation_split__Active__c = true;

        Test.startTest();
        update classroom;
        Test.stopTest();
    }

    @isTest static void TestTriggerReActiveEmails() {
        
        Contact con = new Contact();
        con.LastName = 'Test';
        insert con;

        
        donation_split__Designation__c classroom = new donation_split__Designation__c();
        classroom.Description__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		classroom.How_will_you_spend_your_funds__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		classroom.Why_We_Need_Your_Help__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		classroom.Teacher__c = con.Id;
        classroom.donation_split__Active__c = false;
        classroom.Inactive_Classroom_Page__c = true;
        insert classroom;

        Notify_Donor__c notify = new Notify_Donor__c();
        notify.Classroom__c = classroom.Id;
        notify.Email__c = 'test@test.com';
        notify.First_Name__c = 'Test';
        notify.Last_Name__c = 'Test';
        insert notify;

        classroom.Inactive_Classroom_Page__c = false;

        Test.startTest();
        update classroom;
        Test.stopTest();
    }
}