@isTest
private class OpportunityManagementTest {

  private static donation_split__Designation__c classroom;
  private static Campaign campaign;
  private static Account account;
  private static Contact contact;
    
  static testMethod void testFundsAvailableTrigger() {
    initTestData();
    list<AcctSeed__Accounting_Variable__c> acvlist = new list<AcctSeed__Accounting_Variable__c>();
    
    AcctSeed__Accounting_Variable__c  acv1 = new AcctSeed__Accounting_Variable__c();
    acv1.AcctSeed__Type__c = 'GL Account Variable 1';
    acv1.AcctSeed__Active__c = true;
    acv1.Name = 'Unrestricted';
    
    AcctSeed__Accounting_Variable__c  acv2 = new AcctSeed__Accounting_Variable__c();
    acv2.AcctSeed__Type__c = 'GL Account Variable 2';
    acv2.AcctSeed__Active__c = true;
    acv2.Name = 'Programs';
    
    AcctSeed__Accounting_Variable__c  acv3 = new AcctSeed__Accounting_Variable__c();
    acv3.AcctSeed__Type__c = 'GL Account Variable 3';
    acv3.AcctSeed__Active__c = true;
    acv3.Name = 'Classroom Adoption';
    
    AcctSeed__Accounting_Variable__c  acv4 = new AcctSeed__Accounting_Variable__c();
    acv4.AcctSeed__Type__c = 'GL Account Variable 1';
    acv4.AcctSeed__Active__c = true;
    acv4.Name = 'Restricted';
    
    acvlist.add(acv1);
    acvlist.add(acv2);
    acvlist.add(acv3);
    acvlist.add(acv4);
    
    insert acvlist;
    
    AcctSeed__GL_Account__c asgl = new  AcctSeed__GL_Account__c();
    asgl.Name = '4599 Teacher Restricted Released';
    asgl.AcctSeed__Active__c = true;
    asgl.AcctSeed__Type__c = 'Revenue';
    Insert asgl;

    Test.startTest();

    Id dontationRt = TestUtils.getRecordTypeIdByDeveloperName(Opportunity.SObjectType.getDescribe().getName(), 'adoption');
    Id orderRt = TestUtils.getRecordTypeIdByDeveloperName(Opportunity.SObjectType.getDescribe().getName(), 'Orders');

    OpportunityManagement.afterInsert(null);
    OpportunityManagement.afterUpdate(new List<Opportunity> { new Opportunity() }, new Map<Id, Opportunity>());

    Opportunity o = TestUtils.createOpportunity(account.Id);
    o.CampaignId = campaign.Id;
    o.Amount = 5000;
    o.RecordTypeId = dontationRt;
    o.Teacher__c = contact.Id;
    o.StageName = OpportunityManagement.STAGE_PAID;
    insert o;

    contact = [select Id, Funds_Available__c, Total_Funds_Raised__c from Contact where Id = :contact.Id];

    o.Amount = 6000;
    update o;

    contact = [select Id, Funds_Available__c, Total_Funds_Raised__c from Contact where Id = :contact.Id];

    o.Name = 'Changing the Name';
    update o;

    contact = [select Id, Funds_Available__c, Total_Funds_Raised__c from Contact where Id = :contact.Id];
 
    Opportunity o2 = TestUtils.createOpportunity(account.Id);
    o2.CampaignId = campaign.Id;
    o2.Amount = 1000;
    o2.RecordTypeId = orderRt;
    o2.Teacher__c = contact.Id;
    o2.StageName = OpportunityManagement.STAGE_PAID;
    insert o2;

    contact = [select Id, Funds_Available__c, Total_Funds_Raised__c from Contact where Id = :contact.Id];
  
    Test.stopTest();
    
    OpportunityManagement.SendDonorEmail(new set<Id>{o.Id, o2.Id});
  }

  private static void initTestData() {
    account = TestUtils.createAccount();
    insert account;

    contact = TestUtils.createContact(account.Id);
    insert contact;

    classroom = TestUtils.createClassroom(contact.Id, account.Id);
    insert classroom;

    campaign = TestUtils.createCampaign(classroom.Id);
    insert campaign;
  }
}