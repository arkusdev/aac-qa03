@IsTest
private class CommunityOpportunityCreatorBatchTest {
    
    static testMethod void executeTest() {
        AcctSeed__GL_Account__c glAccount = new AcctSeed__GL_Account__c();
        glAccount.Name = '4599 Teacher Restricted Released';
        glAccount.AcctSeed__Sub_Type_1__c = 'Liabilities';
        insert glAccount;
        
        Account account = new Account();
        account.Name = 'Test';
        insert account;
        
        RecordType rt = [select Id
                         from RecordType 
                         where SObjectType = 'Opportunity' 
                         and DeveloperName ='adjustment'];
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.CloseDate = Date.today();
        opp.AccountId = account.Id;
        opp.StageName = 'Open';
        opp.RecordTypeId = rt.Id;
        insert opp;
    
        delete [select Id from Community_Opportunity__c where Opportunity__c =: opp.Id];
        
        Test.startTest();
            Database.executeBatch(new CommunityOpportunityCreatorBatch());
        Test.stopTest();
        
        List<Community_Opportunity__c> commOppList = [select Id from Community_Opportunity__c where Opportunity__c =: opp.Id];
        system.assertEquals(1, commOppList.size());
    }

}