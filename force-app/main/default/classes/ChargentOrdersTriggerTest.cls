@isTest
public class ChargentOrdersTriggerTest {
    
    @testSetup static void initTestData() {
        
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Test';
        insert con;
        
        ChargentOrders__ChargentOrder__c order = new ChargentOrders__ChargentOrder__c();
        order.Transaction_Completed__c = true;
        order.ChargentOrders__Billing_Email__c = 'test@test.com';
        order.Payment_Contact__c = con.Id;
        order.ChargentOrders__Billing_City__c = 'New York';
        order.ChargentOrders__Billing_Zip_Postal__c = '10018';
        order.ChargentOrders__Billing_State__c = 'New York';
        order.ChargentOrders__Billing_Address__c = 'Test 123';

        insert order;
    }

    @isTest
    static void TestRunTrigger() {
        
        ChargentOrders__ChargentOrder__c order = [SELECT Id FROM ChargentOrders__ChargentOrder__c LIMIT 1];
        order.ChargentOrders__Status__c = 'Complete';
        Test.startTest();
        update order;
        Test.stopTest();

        ChargentOrders__ChargentOrder__c result = [SELECT ToSendThankYouEmail__c FROM ChargentOrders__ChargentOrder__c LIMIT 1];
        system.assertEquals(true, result.ToSendThankYouEmail__c);
    }
}