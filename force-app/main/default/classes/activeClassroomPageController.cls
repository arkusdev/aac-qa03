/**
 * @description Apex controller used in the aura component activeClassroomPage, unit test activeClassroomPageControllerTest
 */
public with sharing class activeClassroomPageController {

    
    /**
     * @description used in the aura component activeClassroomPage to active the classroom after login
     */
    @AuraEnabled
    public static void active() {
        
        if (Schema.sObjectType.User.fields.ContactId.isAccessible() && Schema.sObjectType.donation_split__Designation__c.fields.Inactive_Classroom_Page__c.isUpdateable()) {
            User usr = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];
            
            if(usr.ContactId != null) {
                List<donation_split__Designation__c> classrooms = [SELECT Inactive_Classroom_Page__c, Teacher__c FROM donation_split__Designation__c WHERE Teacher__c = :usr.ContactId];
                if(!classrooms.isEmpty()) {
                    
                    List<String> teacherIds = new List<String>();
                    for(donation_split__Designation__c classroom : classrooms) {
                        classroom.Inactive_Classroom_Page__c = false;
                        teacherIds.add(classroom.Teacher__c);
                        
                    }
                    update classrooms;

                    List<Contact> teachers = [SELECT InactiveEmail1__c, InactiveEmail2__c, InactiveEmail3__c FROM Contact WHERE ID IN :teacherIds];
                    for(Contact teacher : teachers) {
                        teacher.InactiveEmail1__c = false;
                        teacher.InactiveEmail2__c = false;
                        teacher.InactiveEmail3__c = false;
                    }
                    update teachers;
                }
            }
        }
    }
}