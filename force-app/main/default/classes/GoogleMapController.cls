public with sharing class GoogleMapController {
	
  @AuraEnabled
  public static String queryRecord(Id recordId) {
    if (String.isBlank(recordId)) {
      return null;
    }

    return JSON.serialize(QueryUtils.queryObject(recordId));
  }
}