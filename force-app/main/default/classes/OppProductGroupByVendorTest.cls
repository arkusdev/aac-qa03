@isTest
private class  OppProductGroupByVendorTest {
static testMethod void testOppProductGroupByVendor()
{
     Account school = TestUtils.createAccount();
    school.ShippingStreet = '123 Main Street';
    school.ShippingCity = 'Minneapolis';
    school.ShippingStateCode = 'MN';
    school.ShippingCountryCode = 'US';
    school.ShippingPostalCode = '12345';
    insert school;
    Contact c = TestUtils.createContact(school.Id);
    c.School__c = school.Id;
    c.School__r = school;
    insert c;
    
     list<AcctSeed__Accounting_Variable__c> acvlist = new list<AcctSeed__Accounting_Variable__c>();
    
    AcctSeed__Accounting_Variable__c  acv1 = new AcctSeed__Accounting_Variable__c();
    acv1.AcctSeed__Type__c = 'GL Account Variable 1';
    acv1.AcctSeed__Active__c = true;
    acv1.Name = 'Unrestricted';
    
    AcctSeed__Accounting_Variable__c  acv2 = new AcctSeed__Accounting_Variable__c();
    acv2.AcctSeed__Type__c = 'GL Account Variable 2';
    acv2.AcctSeed__Active__c = true;
    acv2.Name = 'Programs';
    
    AcctSeed__Accounting_Variable__c  acv3 = new AcctSeed__Accounting_Variable__c();
    acv3.AcctSeed__Type__c = 'GL Account Variable 3';
    acv3.AcctSeed__Active__c = true;
    acv3.Name = 'Classroom Adoption';
    
    AcctSeed__Accounting_Variable__c  acv4 = new AcctSeed__Accounting_Variable__c();
    acv4.AcctSeed__Type__c = 'GL Account Variable 1';
    acv4.AcctSeed__Active__c = true;
    acv4.Name = 'Restricted';
    
    acvlist.add(acv1);
    acvlist.add(acv2);
    acvlist.add(acv3);
    acvlist.add(acv4);
    
    insert acvlist;
    
    AcctSeed__GL_Account__c asgl = new  AcctSeed__GL_Account__c();
     asgl.Name = '4599 Teacher Restricted Released';
     asgl.AcctSeed__Active__c = true;
     asgl.AcctSeed__Type__c = 'Revenue';
     Insert asgl;

    Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
    Account acc = new Account(Email__c = 'test@unittest.com', Name='officedepot', Order_Cloud_Vendor_Name__c = 'officedepot', RecordTypeId = rtId);
    insert acc;
   // Pricebook2 standardPb = [select id, name, isActive from Pricebook2 where isStandard=true];
    Opportunity o = TestUtils.createOpportunity(school.Id);
    o.Pricebook2Id = Test.getStandardPricebookId();
    insert o;
    Product2 p = new Product2();
    p.name = 'Test Product';
    p.Vendor_Name__c = 'officedepot';
    p.IsActive = true;
    Insert p;
    PriceBookEntry pbe = new PriceBookEntry();
    pbe.Product2Id = p.Id;
    pbe.Pricebook2Id = Test.getStandardPricebookId();
    pbe.IsActive = true;
    pbe.UnitPrice = 123;
    Insert pbe;
    OpportunityLineItem oli = new OpportunityLineItem();
    oli.VendorOrderId__c = 'test Id';
    oli.PriceBookEntryId = pbe.Id;
    Oli.OpportunityId = o.Id;
    oli.unitPrice= 0;
    oli.Quantity= 10;
    Insert Oli;
    OpportunityLineItem oli1 = new OpportunityLineItem();
    oli1.VendorOrderId__c = 'test Id';
    oli1.PriceBookEntryId = pbe.Id;
    Oli1.OpportunityId = o.Id;
    oli1.unitPrice= 0;
    oli1.Quantity= 10;
    Insert Oli1;
    Test.startTest();
    OppProductGroupByVendor opg = new OppProductGroupByVendor();
    opg.recId = o.Id;
    opg.getOpptylineItems();
    opg.olimap = new Map<string,list<opportunityLineitem>>();
    opg.olimap1 = new Map<string,list<opportunityLineitem>>();
    opg.keyValues = new list<string>();
    Test.stopTest();
}
}