public without sharing class TransactionHistoryController {
    
    public String                           recordType { get; set; }
    public User                             user { get; set; }
    public List<Community_Opportunity__c>   commOppList { get; set; }
      
    public TransactionHistoryController() {
        this.user = [select ContactId 
                     from User 
                     where Id =: UserInfo.getUserId()];
        this.recordType = 'All';
        this.search();
    }
    
    public void search() {
        String query = 'select Amount__c, '
                            + ' Opportunity_Record_Type__c, '
                            + ' Opportunity_Name__c,'
                            + ' Donor_Name__c,'
                            + ' Close_Date__c,'
                            + ' Opportunity__c,'
                            + ' Expiration_Date__c'                            
                        + ' from Community_Opportunity__c';
        query += String.isBlank(this.user.ContactId) ? ' where Opportunity__r.Teacher__c = null' : ' where Opportunity__r.Teacher__c = \'' + this.user.ContactId + '\'';
        query += ' and Stage__c = \'Sale Closed / Paid\' ';
        query += this.recordType == 'All' ? ' and Opportunity__r.RecordType.DeveloperName IN (\'adjustment\',\'Disbursement\',\'schooldonation\',\'adoption\',\'Orders\')' : ' and Opportunity__r.RecordType.DeveloperName = \'' + this.recordType + '\'';
        query += ' ORDER BY Opportunity__r.CreatedDate DESC'
               + ' LIMIT 1000';
        this.commOppList = Database.query(query);

    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('All','All Transactions'));
        for (RecordType rt : [select    Name,
                                        DeveloperName
                              from RecordType 
                              where SObjectType = 'Opportunity' 
                              and DeveloperName IN ('adjustment', 'Disbursement', 'schooldonation', 'adoption', 'Orders')])
            options.add(new SelectOption(rt.DeveloperName, rt.Name));
        return options;
    }
}