public with sharing class AddressUpdateQueueable implements Queueable,
    Database.AllowsCallouts {

  private List<Contact> teachers;
  private string mailingId;
  private string city;
  private string zip;
  private string Country;
  private string State;
  private string ShippingStreet;
  Private Map<Id,Id> userId = new Map<Id,Id>();
  public AddressUpdateQueueable(
      List<Contact> teachers, Map<Id,Id> userId) {
    this.UserId = userId;
    this.teachers = teachers;
  }

    public void execute(QueueableContext SC) {
        for(Contact teacher : teachers) {
            ShopAddress sa = new ShopAddress(ShopAddress.AddressType.Shipping,teacher);
            string name = [select name from Account where Id =: teacher.School__c limit 1].Name;
            if(teacher.Mailing_Address_Id__c != Null) {
                sa.Id = teacher.Mailing_Address_Id__c;       
                sa.CompanyName = name;
                shoputils.UpdateShippingAddress(ShopUtils.getAdminAuthToken(), sa);
            } else {
                teacher.Mailing_Address_Id__c = ShopUtils.createAddress(ShopUtils.getAdminAuthToken(), sa);   
                ShopUtils.createAddressAssignment(ShopUtils.getAdminAuthToken(), userId.get(teacher.id), sa);
            }
            ShopAddress sab = new ShopAddress(ShopAddress.AddressType.Billing,teacher);
            if(teacher.Billing_Address_Id__c != NUll) {
                sab.Id = teacher.Billing_Address_Id__c;
                sab.CompanyName = name;
                shoputils.UpdateShippingAddress(ShopUtils.getAdminAuthToken(), sab);
            } else {
                teacher.Billing_Address_Id__c = ShopUtils.createAddress(ShopUtils.getAdminAuthToken(), sab);   
                ShopUtils.createAddressAssignment(ShopUtils.getAdminAuthToken(), userId.get(teacher.id), sab);
            }
        }
    }
}