public with sharing class UserPictureUpload {
    
    @AuraEnabled
	public static String getProfilePicture() {
        
        String currentId = UserInfo.getUserId();
        User currentUser = [SELECT MediumPhotoUrl FROM User WHERE Id = :currentId];
        
        return currentUser.MediumPhotoUrl;
    }

    @AuraEnabled
    public static User getUser (String userId) {
        return [SELECT Id, MediumPhotoUrl FROM User WHERE Id =:userId];
    }
    
    @AuraEnabled
    public static void saveAttachment(String base64Data, String contentType) {
        try {
            User user = [SELECT ID, ContactID, SmallPhotoUrl, FullPhotoUrl, FederationIdentifier FROM User WHERE ID = :UserInfo.getUserId() LIMIT 1];
                            
            if(!Test.isRunningTest()) {
                ConnectApi.UserProfiles.setPhoto(Network.getNetworkId(), UserInfo.getUserId(), (new ConnectApi.BinaryInput(EncodingUtil.base64Decode(base64Data), contentType, String.valueOf(UserInfo.getUserId()))));
            }
            
        } catch(Exception ex) {
            throw new AuraHandledException('Error: ' + ex.getMessage());
        }
    }
}