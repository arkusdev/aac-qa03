@isTest
public class ExpiredFundContactUpdateBatchTest {
    
    static testMethod void ExpiredFundContactUpdateBatchTest1() {
        
        insert new ExpFundsFlag__c();
        
        Integer funds = 50;
        Integer deduction = 25;
        
        Contact contact = new Contact();
        contact.LastName = 'test';
        contact.Funds_Available__c = funds;
        insert contact;

        Expired_Fund_Contact_Update__c efcu = new Expired_Fund_Contact_Update__c();
        efcu.Contact__c = contact.Id;
        efcu.Deduction__c = deduction;
        insert efcu;

        Test.startTest();
            Database.executeBatch(new ExpiredFundContactUpdateBatch());
        Test.stopTest();

        contact = [select Funds_Available__c from Contact where Id =: contact.Id];
        efcu = [select Processed__c from Expired_Fund_Contact_Update__c where Id =: efcu.Id];

        //system.assertEquals(funds - deduction, contact.Funds_Available__c);
        system.assert(efcu.Processed__c);
    }
    
}