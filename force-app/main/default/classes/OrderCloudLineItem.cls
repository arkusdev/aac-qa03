global with sharing class OrderCloudLineItem {
    global String ID { get; set; }
  global String ProductID { get; set; }
  global Integer Quantity { get; set; }
  global Decimal UnitPrice { get; set; }
  global xp XP{get;set;}
  global OrderCloudProduct Product{get;set;}
 /* global OrderCloudLineItem()
  {
      XP = new xp();  
  }
  */
  global class xp
  {
      global string Description{get;set;}  
      global string PunchoutName{get;set;}
      global string SupplierPartID{get;set;}
      global string SupplierPartAuxiliaryID{get;set;}
      global list<string> vendorOrderId{get;set;}
  }
 
}