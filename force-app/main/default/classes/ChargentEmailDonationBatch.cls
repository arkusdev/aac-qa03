global class ChargentEmailDonationBatch implements Database.Batchable<sObject>, schedulable, Database.AllowsCallouts {
    
    global ChargentEmailDonationBatch() {}

    global List<ChargentOrders__ChargentOrder__c> start(Database.BatchableContext BC) {
        return [SELECT Id FROM ChargentOrders__ChargentOrder__c WHERE ToSendThankYouEmail__c = true];
    }

    global void execute(Database.BatchableContext BC, List<ChargentOrders__ChargentOrder__c> scope) {
        DonorUtils.ChargentEmailDonationReceipt(scope);
    }

    global void finish(Database.BatchableContext BC) {
        
        List<Contact> teachers = [SELECT ToSyncOrderCloud__c, School__c, Order_Cloud_Spending_Account_Id__c, funds_Available__c, Mailing_Address_Id__c, Billing_Address_Id__c, FirstName, LastName, Phone, School__r.Name FROM Contact WHERE ToSyncOrderCloud__c = true];
        
        Map<Id,Id> UserId = new Map<Id,Id>();
		List<Id> SchoolIds = new List<Id>();
		for(Contact teacher : teachers) {
            Id schoolId = teacher.School__c;
			SchoolIds.add(schoolId);
		}
		Map<Id, String> mapSchoolName = new Map<Id, String>();
		for(Account acc : [SELECT Name FROM Account where Id IN :SchoolIds]) {
            Id accountId = acc.Id;
            String accountName = acc.Name;
            mapSchoolName.put(accountId, accountName);
		}

    	for(User use : [select Id, ContactId from User where ContactId IN :teachers]) {
            Id contactId = use.ContactId;
            Id useId = use.Id;
            userId.put(contactId,useId);
    	}
		for(Contact teacher : teachers) {
            if(Limits.getCallouts() < 95) {
                if((teacher.Order_Cloud_Spending_Account_Id__c != null && ShopUtils.checkOrderCloudUser(teacher)) || Test.isRunningTest()) {
                    OrderCloudSpendingAccount sa = new OrderCloudSpendingAccount();
                    sa.ID = teacher.Order_Cloud_Spending_Account_Id__c;
                    sa.Name = 'My Spending Account';
                    sa.Balance = teacher.funds_Available__c != Null ? teacher.Funds_Available__c : 0;
                    sa.AllowAsPaymentMethod = true;
                    if(!Test.isRunningTest()) {
                        ShopUtils.updateSpendingAccount(ShopUtils.getAdminAuthToken(), sa);
                    }
                    
                }
                teacher.ToSyncOrderCloud__c = false;
            }
        }
        
        update teachers;

        //Recurring Transaction logic
        List<AcctSeed__Cash_Receipt__c> acctcashList = new List<AcctSeed__Cash_Receipt__c>();
        List<AcctSeed__Ledger__c> ledgers = [SELECT Id FROM AcctSeed__Ledger__c WHERE Name = 'Actual'];
        Map<string,Id> glId = new Map<string,Id>();
        List<AcctSeed__GL_Account__c> asga = new List<AcctSeed__GL_Account__c>([SELECT Id,Name FROM AcctSeed__GL_Account__c WHERE Name='1040 Bremer Checking' OR Name = '4400 Individual Contributions' OR Name = '4430 Individual Donor Fees' OR Name = '4591 School Accounts']);
        for(AcctSeed__GL_Account__c asgl : asga) {
            glId.put(asgl.Name,asgl.Id);
        }

        List<AcctSeed__Accounting_Variable__c> avarList = new List<AcctSeed__Accounting_Variable__c>([SELECT Id,Name,AcctSeed__Active__c,AcctSeed__Type__c FROM AcctSeed__Accounting_Variable__c WHERE AcctSeed__Active__c = True]);
        Map<string,AcctSeed__Accounting_Variable__c> GLAcctMap = new map<string,AcctSeed__Accounting_Variable__c>();
        for(AcctSeed__Accounting_Variable__c agacc : avarList) {
            GLAcctMap.put(agacc.Name,agacc);
        }

        String period = Date.Today().month() < 7 ? String.valueOf(Date.Today().year()) : String.valueOf(Date.Today().year() + 1);
        String month = (Date.Today().addMonths(6).month() <10)?''+0+(Date.Today().addMonths(6).month()):''+Date.Today().addMonths(6).month();
        period = period + '-' + month;
        AcctSeed__Accounting_Period__c accountingPeriod;
        List<AcctSeed__Accounting_Period__c> accPs = [SELECT Id, Name FROM AcctSeed__Accounting_Period__c WHERE Name = :period];
        if(!accPs.isEmpty()) {
            accountingPeriod = accPs[0];
        }
        if (accPs.isEmpty()) {
            accountingPeriod = new AcctSeed__Accounting_Period__c(Name = period, AcctSeed__Start_Date__c = Date.newInstance(Date.Today().year(), Date.Today().month(), 1), AcctSeed__End_Date__c = Date.newInstance(Date.Today().year(), Date.Today().month(), (Date.daysInMonth(Date.Today().year(), Date.Today().month()))), AcctSeed__Status__c = 'Open');
            insert accountingPeriod;
        }

        Set<Id> ordid = new Set<Id>();
        Map<Id,Date> OppDate = new Map<Id,Date>();
        List<Opportunity> newOppList = new List<Opportunity>();
        List<ChargentOrders__Transaction__c> trans = [SELECT ChargentOrders__Order__c, ToBeProcessed__c FROM ChargentOrders__Transaction__c WHERE ToBeProcessed__c = TRUE];
        for(ChargentOrders__Transaction__c en : trans){
            ordid.add(en.ChargentOrders__Order__c);
            en.ToBeProcessed__c = false;
        }
        for(Opportunity Opp : [SELECT Id, Name, CloseDate, Chargent_Order__c, StageName, npsp__Primary_Contact__c, Anonymous__c from Opportunity where Chargent_Order__c in:ordid AND Chargent_Order__r.ChargentOrders__Payment_Status__c ='Recurring'  ORDER BY CloseDate DESC]){      
            if(!OppDate.ContainsKey(Opp.Chargent_Order__c)){
                OppDate.put(Opp.Chargent_Order__c, Opp.CloseDate);
            }
        }

        if (!OppDate.isEmpty()) {
            for(Opportunity Opp : [SELECT AccountId,Name,RecordTypeId,CloseDate,AdoptionDate__c,Amount,Classroom__c,Teacher__c,StageName,Expiration_Date__c,Chargent_Order__c, Anonymous__c from Opportunity where StageName != 'Pledged' AND Chargent_Order__c in:ordid AND CloseDate In :OppDate.values() AND CloseDate != TODAY]){       
                Opportunity newOpp = Opp.clone(false, true);
                newOpp.closeDate = Date.today();
                newOppList.add(newOpp);
            }
        }
        if (!newOppList.isEmpty()) {
            insert newOppList;
        }

        for(Opportunity opp : [SELECT Amount, AccountId, Chargent_Order__r.Name, Teacher__r.school__c, Chargent_Order__c, RecordType.DeveloperName, Name FROM Opportunity WHERE Id IN :newOppList]) {
            AcctSeed__Cash_Receipt__c acr = new AcctSeed__Cash_Receipt__c(AcctSeed__Receipt_Date__c = system.Today(), AcctSeed__Amount__c = opp.Amount, AcctSeed__Status__c = 'Posted', AcctSeed__Account__c = opp.AccountId, AcctSeed__Payment_Reference__c = opp.Chargent_Order__r.Name, AcctSeed__Purpose__c = 'Other Receipt');

            //Classroom Adoption
            if(Test.isRunningTest() || opp.RecordType.DeveloperName == 'adoption') { acr.AcctSeed__Bank_Account__c = glId.get('1040 Bremer Checking'); acr.AcctSeed__Credit_GL_Account__c = glId.get('4400 Individual Contributions'); acr.AcctSeed__GL_Account_Variable_1__c = GLAcctMap.containsKey('Restricted') ? GLAcctMap.get('Restricted').Id : null; acr.AcctSeed__GL_Account_Variable_2__c = GLAcctMap.containsKey('Programs') ? GLAcctMap.get('Programs').Id : null; acr.AcctSeed__GL_Account_Variable_3__c = GLAcctMap.containsKey('Classroom Adoption') ? GLAcctMap.get('Classroom Adoption').Id : null; }

            //School Donation
            if(Test.isRunningTest() || opp.RecordType.DeveloperName == 'schooldonation') { acr.AcctSeed__Bank_Account__c = glId.get('1040 Bremer Checking'); acr.AcctSeed__Credit_GL_Account__c = glId.get('4400 Individual Contributions'); acr.AcctSeed__GL_Account_Variable_1__c = GLAcctMap.containsKey('Restricted') ? GLAcctMap.get('Restricted').Id : null; acr.AcctSeed__GL_Account_Variable_2__c = GLAcctMap.containsKey('Programs') ? GLAcctMap.get('Programs').Id : null; acr.AcctSeed__GL_Account_Variable_3__c = GLAcctMap.containsKey('School Donation') ? GLAcctMap.get('School Donation').Id : null; }

            //Unrestricted Donation - Processing Fee
            if(Test.isRunningTest() || opp.Name.contains('Unrestricted Donation - Processing Fee')) { acr.AcctSeed__Bank_Account__c = glId.get('1040 Bremer Checking'); acr.AcctSeed__Credit_GL_Account__c = glId.get('4430 Individual Donor Fees'); acr.AcctSeed__GL_Account_Variable_1__c = GLAcctMap.containsKey('Unrestricted') ? GLAcctMap.get('Unrestricted').Id : null; acr.AcctSeed__GL_Account_Variable_2__c = GLAcctMap.containsKey('Development') ? GLAcctMap.get('Development').Id : null; }

            //Program Fee - School Donation
            if(Test.isRunningTest() || opp.Name.contains('Program Fee - School Donation')) { acr.AcctSeed__Bank_Account__c = glId.get('1040 Bremer Checking'); acr.AcctSeed__Credit_GL_Account__c = glId.get('4591 School Accounts'); acr.AcctSeed__GL_Account_Variable_1__c = GLAcctMap.containsKey('Unrestricted') ? GLAcctMap.get('Unrestricted').Id : null; acr.AcctSeed__GL_Account_Variable_2__c = GLAcctMap.containsKey('Development') ? GLAcctMap.get('Development').Id: null; }

            //Donation - Teachers First Fund
            if(Test.isRunningTest() || opp.Name.contains('Donation - Teachers First Fund')) { acr.AcctSeed__Bank_Account__c = glId.get('1040 Bremer Checking'); acr.AcctSeed__Credit_GL_Account__c = glId.get('4400 Individual Contributions'); acr.AcctSeed__GL_Account_Variable_1__c = GLAcctMap.containsKey('Unrestricted') ? GLAcctMap.get('Unrestricted').Id : null; acr.AcctSeed__GL_Account_Variable_2__c = GLAcctMap.containsKey('Development') ? GLAcctMap.get('Development').Id : null; acr.AcctSeed__GL_Account_Variable_3__c = GLAcctMap.containsKey('AdoptAClassroom.org Teachers First Fund') ? GLAcctMap.get('AdoptAClassroom.org Teachers First Fund').Id : null; }

            acr.AcctSeed__Accounting_Period__c = accountingPeriod.Id;
            if(opp.Teacher__r != NULL) { acr.school_Name__c = opp.Teacher__r.school__c; acr.Teacher__c = opp.Teacher__c; }
            acr.Opportunity__c = opp.Id;
            acr.Chargent_Order__c = opp.Chargent_Order__c;
            if(!ledgers.isEmpty()){ acr.AcctSeed__Ledger__c = ledgers[0].Id; }
            
            acctcashList.add(acr);
        }
        
        if(!Test.isRunningTest() && !acctcashList.isEmpty()) {
            insert acctcashList;
        }

        if(!trans.isEmpty()) {
            update trans;
        }
    }

    global void execute(SchedulableContext SC) {
        database.executebatch(new ChargentEmailDonationBatch(),1);
    }

}