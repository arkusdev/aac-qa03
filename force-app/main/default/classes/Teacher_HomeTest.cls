@isTest
private class Teacher_HomeTest {
    @TestSetup
    static void makeData(){

        String schoolRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('School Admin').getRecordTypeId();
        
        Account portalAccount = new Account(name = 'testAccount');
        insert portalAccount;
        
        Contact con = new Contact();
        con.LastName = 'Unit Test';
        con.RecordTypeId = schoolRT;
        con.AccountId = portalAccount.Id;
        insert con;

        String profileId = [SELECT ID FROM Profile WHERE Name = 'AAC - Teacher Community Plus Login User'].Id;
        User portalUser = new User();
       
        portalUser.FirstName = 'Unit Test';
        portalUser.LastName  = 'Unit Test';
        portalUser.Email     = 'UnitTest@UnitTestaac.com';
        portalUser.Username  = 'UnitTest@UnitTestaac.com';
        portalUser.Alias     = 'fatty';
        portalUser.ProfileId = profileId;
        portalUser.UserPreferencesShowProfilePicToGuestUsers = true;
        portalUser.ContactId = con.Id;
        portalUser.TimeZoneSidKey    = 'America/Denver';
        portalUser.LocaleSidKey      = 'en_US';
        portalUser.EmailEncodingKey  = 'UTF-8';
        portalUser.LanguageLocaleKey = 'en_US';
        
        insert portalUser;

        donation_split__Designation__c classroom = new donation_split__Designation__c();
        classroom.Description__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		classroom.How_will_you_spend_your_funds__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		classroom.Why_We_Need_Your_Help__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		classroom.Teacher__c = con.Id;
        classroom.donation_split__Active__c = true;
        insert classroom;
    }

    @isTest static void testGetData() {
        List<User> users = [SELECT Id FROM User WHERE username = 'UnitTest@UnitTestaac.com'];
        
        Boolean hasSchool = true;
        Test.startTest();
        system.runAs(users[0]) {
            Teacher_Home.data data = Teacher_Home.getData();
            hasSchool = data.hasSchool;
        }
        Test.stopTest();

        System.assertEquals(false, hasSchool, 'It should be false');
    }
}