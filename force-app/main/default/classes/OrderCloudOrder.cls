public with sharing class OrderCloudOrder {
	
  public String ID { get; set; }
  public String Type { get; set; }
  public String FromUserID { get; set; }
  public String FromUserFirstName { get; set; }
  public String FromUserLastName { get; set; }
  public String Comments { get; set; }
  public Integer LineItemCount { get; set; }
  public String Status { get; set; }
  public Datetime DateCreated { get; set; }
  public Datetime DateSubmitted { get; set; }
  public Decimal Subtotal { get; set; }
  public Decimal ShippingCost { get; set; }
  public Decimal TaxCost { get; set; }
  public Decimal PromotionDiscount { get; set; }
  public Decimal Total { get; set; }
  public Boolean IsSubmitted { get; set; }
}