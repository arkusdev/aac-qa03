@isTest
public with sharing class ShopCalloutMock implements HttpCalloutMock {

  private static final Integer ERROR_STATUS_CODE = 500;
  private static final Integer STATUS_CODE_SUCCESS = 200;
  private static final Integer STATUS_CODE_CREATED = 201;
  private static final Integer STATUS_CODE_NO_CONTENT = 204;

  private static final String AUTH_PATH = 'oauth';
  private static final String ACCESS_TOKEN = 'accesstoken';
  private static final String USER_GROUPS = 'usergroups';
  private static final String USERS = 'users';
  private static final String PRODUCTS_PATH = 'products';
  private static final String LINEITEMS_PATH = 'lineitems';
  private static final String ADDRESS_GROUP_PATH = 'addresses/assignments';
  private static final String CREATE_ADDRESS_PATH = 'addresses';
  private static final String Payment_URL = 'payments';

  private Boolean error;

  public ShopCalloutMock() {
    this(false);
  }

  public ShopCalloutMock(Boolean error) {
    this.error = error;
  }
  
  public HttpResponse respond(HttpRequest request) {
    HttpResponse response = new HttpResponse();

    if (this.error) {
      response.setStatusCode(ERROR_STATUS_CODE);
      return response;
    }

    if (request.getEndpoint().contains(AUTH_PATH)) {
      response.setStatusCode(STATUS_CODE_SUCCESS);
      response.setBody(generateAuthResponse());
    } else if (request.getEndpoint().contains(ACCESS_TOKEN)) {
      response.setStatusCode(STATUS_CODE_SUCCESS);
      response.setBody(generateAuthResponse());
    } else if (request.getEndpoint().contains(USER_GROUPS)) {
      response.setStatusCode(STATUS_CODE_NO_CONTENT);
    } else if (request.getEndpoint().contains(USERS)) {
      response.setStatusCode(STATUS_CODE_CREATED);
    } else if(request.getEndpoint().contains(PRODUCTS_PATH)) {
      response.setStatusCode(STATUS_CODE_SUCCESS);
      response.setBody(generateProductResponse());
    } else if (request.getEndpoint().contains(LINEITEMS_PATH)) {
      response.setStatusCode(STATUS_CODE_SUCCESS);
      response.setBody(generateLineItemsResponse());
    } else if (request.getEndpoint().contains(ADDRESS_GROUP_PATH)) {
      response.setStatusCode(STATUS_CODE_SUCCESS);
    } else if (request.getEndpoint().contains(CREATE_ADDRESS_PATH)) {
      response.setStatusCode(STATUS_CODE_SUCCESS);
    } else if (request.getEndpoint().contains(Payment_URL)) {
      response.setBody(generatePaymentResponse());
    } else {
      response.setStatusCode(STATUS_CODE_SUCCESS);
    }

    return response;
  }
  
  private String generatePaymentResponse() {
    ShopUtils.paymentDetails payments = new ShopUtils.paymentDetails();

    payments.orderId = 'sampleOrderId';
    payments.Error = 'Test class';
    payments.Items = null;

    return JSON.serialize(payments);
  }
  
  private String generateLineItemsResponse() {
    OrderCloudLineItem li = new OrderCloudLineItem();
    ShopUtils.LineItemResponse r = new ShopUtils.LineItemResponse();
    List<OrderCloudLineItem> lineItems = new List<OrderCloudLineItem>();

    li.ID = 'lineitemid';
    li.ProductID = 'productid';
    li.Quantity = 10;
    li.UnitPrice = 10;

    lineItems.add(li);

    r.Items = lineItems;

    return JSON.serialize(r);
  }

  private String generateProductResponse() {
    OrderCloudProduct p = new OrderCloudProduct();

    p.ID = 'productid';
    p.Name = 'My Product';
    p.Description = 'Very awesome product';

    return JSON.serialize(p);
  }

  private String generateAuthResponse() {
    ShopUtils.TokenResponse tr = new ShopUtils.TokenResponse();
    tr.access_token = 'dummy token';
    return JSON.serialize(tr);
  }
}