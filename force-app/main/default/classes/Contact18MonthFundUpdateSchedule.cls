public with sharing class Contact18MonthFundUpdateSchedule implements Schedulable{

    public void execute(SchedulableContext sc){
        Contact18MonthFundUpdateBatch b = new Contact18MonthFundUpdateBatch(); 
        database.executebatch(b, 20);
    }
    
}