public with sharing class VFC_VendorContacts {
    
    Public List<Vendor_Contacts__c> vendorlst{get;set;}
    Public List<Vendor_Contacts__c> sortedVendors{get;set;}
    Public VFC_VendorContacts(){
        
       vendorLst = vendor_contacts__c.getAll().values();
       
         vendorLst.sort();                          
    }
}