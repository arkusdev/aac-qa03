global with sharing class ObjectDescribeBatchable implements
    Database.Batchable<String>, Database.AllowsCallouts {

  private String sessionId;

  global ObjectDescribeBatchable(String sessionId) {
    this.sessionId = sessionId;
  }
  
  global List<String> start(Database.BatchableContext bc) {
    Map<String, Objects_To_Describe__c> dMap = Objects_To_Describe__c.getAll();

    return new List<String>(dMap.keySet());
  }
  
  global void execute(Database.BatchableContext bc,
      List<String> objectNames) {
    if (objectNames.size() > ObjectDescribeUtils.BATCH_SIZE) {
      return;
    }

    ObjectDescribeUtils.updateDescribeDocument(objectNames[0], this.sessionId);
  }
  
  global void finish(Database.BatchableContext bc) { }
  
}