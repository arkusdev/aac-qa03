public with sharing class OrderCloudProduct {
  public String ID { get; set; }
  public String Name { get; set; }
  public String Description { get; set; }
  public xp XP{get;set;}
  
  public class xp
  {
      public string VendorName{get;set;}   
      public string SecondaryItemNo{get;set;}
  }
}