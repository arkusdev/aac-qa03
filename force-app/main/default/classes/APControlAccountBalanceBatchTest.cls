/**
 * Created by christopherjohnson on 4/3/18.
 */
@IsTest
public class APControlAccountBalanceBatchTest {

    @IsTest
    public static void testAPControlAccountCreate() {
        TestDataSuite.initialize();

        List<AcctSeed__AP_Aging_History__c> apAgingList = new List<AcctSeed__AP_Aging_History__c>();
        List<AcctSeed__Account_Payable__c> apList = new List<AcctSeed__Account_Payable__c>();

        Test.startTest();
        delete TestDataSuite.cashDisbursements;
        for (AcctSeed__Account_Payable__c ap : [Select Id, AcctSeed__Net_Amount__c, AcctSeed__Accounting_Period__c, AcctSeed__Status__c From AcctSeed__Account_Payable__c Where Id In :TestDataSuite.accountPayables]) {
            ap.AcctSeed__Status__c = 'Approved';
            ap.AcctSeed__Accounting_Period__c = TestDataSuite.acctPeriods[0].Id;
            AcctSeed__AP_Aging_History__c aph = new AcctSeed__AP_Aging_History__c();
            aph.AcctSeed__Account_Payable__c = ap.Id;
            aph.AcctSeed__Accounting_Period__c = ap.AcctSeed__Accounting_Period__c;
            aph.AcctSeed__Amount__c = ap.AcctSeed__Net_Amount__c;
            apList.add(ap);
            apAgingList.add(aph);
        }

        insert apAgingList;
        update apList;
        AcctSeed.AccountPayablePostService.postAccountPayables(TestDataSuite.accountPayables);
        System.assertNotEquals(0,[SELECT count() FROM AcctSeed__Transaction__c WHERE AcctSeed__GL_Account__c = :[Select AcctSeed__AP_Control_GL_Account__c From AcctSeed__Accounting_Settings__c Limit 1].AcctSeed__AP_Control_GL_Account__c AND AcctSeed__Accounting_Period__c = :TestDataSuite.acctPeriods[0].Id]);
        Database.executeBatch(new APControlAccountBalanceBatch(TestDataSuite.acctPeriods[0].Id),2000);
        Test.stopTest();

        System.assertNotEquals(0,[SELECT count() FROM Control_Account_Balance__c WHERE Type__c = 'Accounts Payable']);
    }
}