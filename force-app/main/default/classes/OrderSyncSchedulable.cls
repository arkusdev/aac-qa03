public with sharing class OrderSyncSchedulable implements Schedulable {
    public void execute(SchedulableContext sc) {
        OrderSyncSchedulable_Switch__c os = OrderSyncSchedulable_Switch__c.getOrgDefaults();
        if(!os.On__c){
            os.On__c = true;
            update os;
            ShopUtils.processSyncQueue();
            os.On__c = false;
            update os;
        }
    }
}