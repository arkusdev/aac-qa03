public with sharing class ProfilePictureController {

    public static Document[] profilePicture;
    
    //New method in order to get Teacher Image from the respective user record instead from document 
    @AuraEnabled
    public static String getProfilePictureNew(Id recordId, String pictureType) {
        
        recordId = getTeacherContact(recordId);
        if(recordId == null){
          return null;
        }
        
        List<User> teacherUser=[select FullPhotoUrl, ContactId from User where ContactId=:recordId];

        return (teacherUser != null && teacherUser.size() > 0 && teacherUser[0].FullPhotoUrl != null && !(teacherUser[0].FullPhotoUrl).contains('profilephoto/005/F') ? teacherUser[0].FullPhotoUrl : '');

    }
    
    @AuraEnabled
    public static Document getProfilePicture(Id recordId, String pictureType) {

        Id attachmentFolderId = getPictureFolder();
        recordId = getTeacherContact(recordId);
        if(attachmentFolderId == null || recordId == null){
          return null;
        }

        String fileName = pictureType + '_' + recordId;

        profilePicture = [SELECT Id, Name, LastModifiedDate, ContentType FROM Document
            WHERE FolderId=:attachmentFolderId AND ContentType IN ('image/png', 'image/jpeg', 'image/gif')
            AND Name=:fileName ORDER BY LastModifiedDate DESC LIMIT 1];

        if(profilePicture.size() > 0){
          return profilePicture[0];
        } else {
          return null;
        }
    }

    @AuraEnabled
    public static Id saveAttachment(Id recordId, String pictureType, String base64Data, String contentType) {

        Id attachmentFolderId = getPictureFolder();
        recordId = getTeacherContact(recordId);
        if(attachmentFolderId == null || recordId == null){
            return null;
        }

        String fileName = pictureType + '_' + recordId;

        Document picture = new Document();
        picture.FolderId = attachmentFolderId;
        picture.body = EncodingUtil.base64Decode(base64Data);
        picture.name = fileName;
        picture.contentType = contentType;

        profilePicture = [SELECT Id FROM Document WHERE FolderId=:attachmentFolderId
          AND Name=:fileName ORDER BY LastModifiedDate DESC LIMIT 1];

        if(profilePicture.size() > 0){
          // Overwrite existing profile picture
          picture.Id = profilePicture[0].Id;
          update picture;
        } else {
          // Add a new one
          insert picture;
        }

        return picture.Id;
    }

    @AuraEnabled
    public static donation_split__Designation__c getClassroom(Id userId) {
      return getClassroomFromUser(userId);
    }

    private static Id getPictureFolder(){
      Self_Registration_Settings__c settings =
        Self_Registration_Settings__c.getInstance();

      return (Id)settings.Profile_Picture_Folder__c;

    }

    @AuraEnabled
    public static Contact getContactDataForUser(Id userId) {
      if (String.isBlank(userId)) {
        return null;
      }

      User u =
        [select Id,
                ContactId,
                Contact.Id,
                Contact.Total_Funds_Raised__c,
                Contact.Funds_Available__c
         from User
         where Id = :userId];

      return u.Contact;
    }

    @TestVisible private static Id getTeacherContact(Id userOrClassroomId){
      // Return the Teacher Contact Id from a User OR Classroom Id
      try {
        List<User> userList = [SELECT ContactId from user where id=:userOrClassroomId];
        if(userList.size() > 0){
          return userList[0].contactId;
        } else {
          donation_split__Designation__c thisClass =
            [SELECT Teacher__c FROM donation_split__Designation__c WHERE Id = :userOrClassroomId LIMIT 1];
          return thisClass.Teacher__c;
        }
      } catch(Exception e) {
        System.debug(e);
        return null;
      }
    }

    @TestVisible private static donation_split__Designation__c getClassroomFromUser(Id userOrClassroomId) {
      // Return a Classroom object from either a User Id OR a classroom Id
      try {
        List<User> userList = [SELECT ContactId from user where id=:userOrClassroomId];

        if(userList.size() > 0){
          Id contactId = userList[0].contactId;
          return [SELECT Id, Name, Total_Funds_Raised__c, Funds_Available__c, CommunityColorTheme__c
            FROM donation_split__Designation__c WHERE Teacher__c = :contactId LIMIT 1];
        } else {
          return [SELECT Id, Name, Total_Funds_Raised__c, Funds_Available__c, CommunityColorTheme__c
            FROM donation_split__Designation__c WHERE Id = :userOrClassroomId LIMIT 1];
        }

      } catch(Exception e) {
        System.debug(e);
        return null;
      }
    }
}