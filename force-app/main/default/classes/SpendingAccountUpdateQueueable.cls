public with sharing class SpendingAccountUpdateQueueable implements Queueable, Database.AllowsCallouts {

	private static final String SPENDING_ACCOUNT_NAME = 'My Spending Account';
	private List<Contact> teachers;
	private list<contact> nlist = new list<contact>();
	Map<Id,Contact> oldmap = new Map<Id,Contact>();

	public SpendingAccountUpdateQueueable( List<Contact> teachers) {
		this.teachers = teachers;
	}

	public void execute(QueueableContext SC) {

		Map<Id,Id> UserId = new Map<Id,Id>();
		List<Id> SchoolIds = new List<Id>();
		for(Contact teacher : this.teachers) {
			SchoolIds.add(teacher.School__c);
		}
		Map<Id, String> mapSchoolName = new Map<Id, String>();
		for(Account acc : [SELECT Name FROM Account where Id IN :SchoolIds]) {
			mapSchoolName.put(acc.Id, acc.Name);
		}

    	for(User use : [select Id, ContactId from User where ContactId IN: this.teachers]) {
        	userId.put(use.ContactId,use.Id);
    	}
		for(Contact teacher : this.teachers) {
			if(ShopUtils.checkOrderCloudUser(teacher)) {
				OrderCloudSpendingAccount sa = new OrderCloudSpendingAccount();
				sa.ID = teacher.Order_Cloud_Spending_Account_Id__c;
				sa.Name = SPENDING_ACCOUNT_NAME;
				if(teacher.funds_Available__c != Null) {
					sa.Balance = teacher.Funds_Available__c;
				} else {
					sa.Balance = 0;
				}
				sa.AllowAsPaymentMethod = true;
				ShopUtils.updateSpendingAccount(ShopUtils.getAdminAuthToken(), sa);
			}

			//added logic from AddressUpdateQueueable
			ShopAddress sa = new ShopAddress(ShopAddress.AddressType.Shipping,teacher);
            string name = mapSchoolName.containsKey(teacher.School__c) ? mapSchoolName.get(teacher.School__c) : '';
            if(teacher.Mailing_Address_Id__c != Null) {
                sa.Id = teacher.Mailing_Address_Id__c;       
                sa.CompanyName = name;
                shoputils.UpdateShippingAddress(ShopUtils.getAdminAuthToken(), sa);
            } else {
                teacher.Mailing_Address_Id__c = ShopUtils.createAddress(ShopUtils.getAdminAuthToken(), sa);   
                ShopUtils.createAddressAssignment(ShopUtils.getAdminAuthToken(), userId.get(teacher.id), sa);
            }
            
            ShopAddress sab = new ShopAddress(ShopAddress.AddressType.Billing,teacher);
            
            if(teacher.Billing_Address_Id__c != NUll) {
                sab.Id = teacher.Billing_Address_Id__c;
                sab.CompanyName = name;
                shoputils.UpdateShippingAddress(ShopUtils.getAdminAuthToken(), sab);
            } else {
                teacher.Billing_Address_Id__c = ShopUtils.createAddress(ShopUtils.getAdminAuthToken(), sab);   
                ShopUtils.createAddressAssignment(ShopUtils.getAdminAuthToken(), userId.get(teacher.id), sab);
            }
		}
	}
}