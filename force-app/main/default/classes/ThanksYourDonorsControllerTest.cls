@isTest
public class ThanksYourDonorsControllerTest{
    public static account accountData;
    public static contact contactData ;
    public static donation_split__Designation__c classroom ;
    public static user commUser;
    
    static {
        accountData= TestUtils.createAccount();
        insert accountData;
        
        contactData = TestUtils.createContact(accountData.Id);
        contactData.npe01__WorkEmail__c='test@gmail.com';        
        insert contactData ;
        
        classroom = TestUtils.createClassroom(contactData.Id, accountData.Id);
        insert classroom;
        
        List<AcctSeed__Accounting_Variable__c> acvlist = new list<AcctSeed__Accounting_Variable__c>();
    
        AcctSeed__Accounting_Variable__c  acv1 = new AcctSeed__Accounting_Variable__c();
        acv1.AcctSeed__Type__c = 'GL Account Variable 1';
        acv1.AcctSeed__Active__c = true;
        acv1.Name = 'Unrestricted';
        
        AcctSeed__Accounting_Variable__c  acv2 = new AcctSeed__Accounting_Variable__c();
        acv2.AcctSeed__Type__c = 'GL Account Variable 2';
        acv2.AcctSeed__Active__c = true;
        acv2.Name = 'Programs';
        
        AcctSeed__Accounting_Variable__c  acv3 = new AcctSeed__Accounting_Variable__c();
        acv3.AcctSeed__Type__c = 'GL Account Variable 3';
        acv3.AcctSeed__Active__c = true;
        acv3.Name = 'Classroom Adoption';
        
        AcctSeed__Accounting_Variable__c  acv4 = new AcctSeed__Accounting_Variable__c();
        acv4.AcctSeed__Type__c = 'GL Account Variable 1';
        acv4.AcctSeed__Active__c = true;
        acv4.Name = 'Restricted';
        
        acvlist.add(acv1);
        acvlist.add(acv2);
        acvlist.add(acv3);
        acvlist.add(acv4);
        
        insert acvlist;
        
        AcctSeed__GL_Account__c asgl = new  AcctSeed__GL_Account__c();
        asgl.Name = '4599 Teacher Restricted Released';
        asgl.AcctSeed__Active__c = true;
        asgl.AcctSeed__Type__c = 'Revenue';
        Insert asgl;
        
        commUser=TestUtils.createCommunityUser();
        commUser.contactId=contactData .Id;
        insert commUser;
    }
    
    public Testmethod static void testThanksYouPost(){
        system.Runas(commUser){
            ThanksYourDonorsController th=new ThanksYourDonorsController();  
        }  
    }
    
    public Testmethod static void testThanksYouPostWithDonation(){
        Id dontationRt = TestUtils.getRecordTypeIdByDeveloperName(
        Opportunity.SObjectType.getDescribe().getName(), 'adoption');
        
        Opportunity o = TestUtils.createOpportunity(accountData.Id);
        o.Amount = 5000;
        o.RecordTypeId = dontationRt;
        o.Teacher__c = contactData.Id;
        o.StageName = OpportunityManagement.STAGE_PAID;
        insert o;
    
        system.Runas(commUser){
            ThanksYourDonorsController th=new ThanksYourDonorsController();  
            th.sendMessage();
            th.inputMessage ='test';
            th.sendMessage();
            th.inputMessage ='testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest';
            th.sendMessage();
            ThanksYourDonorsController.findDonations();
            ThanksYourDonorsController.getBaseUrlSettings();
        }  
    }
}