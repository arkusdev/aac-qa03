public with sharing class Teacher_ClassroomPage {
    
    @AuraEnabled
    public static ClassRoomWrapper getData(){
        
        ClassRoomWrapper data = new ClassRoomWrapper();
        try {
            User usr = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];
            List<donation_split__Designation__c> classrooms = [SELECT Id, Name, Number_of_Students__c, Teaching_Area__c, Why_We_Need_Your_Help__c, Grades__c, Fundraising_Goal__c, Description__c, Hide_My_Page_from_Search_Results_Online__c, How_will_you_spend_your_funds__c FROM donation_split__Designation__c WHERE Teacher__c = :usr.ContactId LIMIT 1];
            if(!classrooms.isEmpty()) {
                data.classroom = classrooms[0];
            } else {
                donation_split__Designation__c classroom = new donation_split__Designation__c();
                classroom.Hide_My_Page_from_Search_Results_Online__c = false;
                classroom.Number_of_Students__c = null;
                classroom.Name = null;
                classroom.Teaching_Area__c = null;
                classroom.Fundraising_Goal__c = null;
                classroom.Description__c = null;
                classroom.How_will_you_spend_your_funds__c = null;
                classroom.Why_We_Need_Your_Help__c = null;
                classroom.Teacher__c = usr.ContactId;
                classroom.donation_split__Active__c = true;
                classroom.Grades__c = null;
                data.classroom = classroom;
            }
            List<options> pickListValuesList= new List<options>();
            Schema.DescribeFieldResult fieldResult = donation_split__Designation__c.Teaching_Area__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple){
                pickListValuesList.add( new options(pickListVal.getValue(), pickListVal.getLabel()));
            }
            data.teachingAreaOptions = pickListValuesList;

            List<options> pickListValuesList2= new List<options>();
            Schema.DescribeFieldResult fieldResult2 = donation_split__Designation__c.Grades__c.getDescribe();
            List<Schema.PicklistEntry> ple2 = fieldResult2.getPicklistValues();
            for( Schema.PicklistEntry pickListVal2 : ple2){
                pickListValuesList2.add( new options(pickListVal2.getValue(), pickListVal2.getLabel()));
            }
            data.gradesOptions = pickListValuesList2;

        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') ? ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')[1].split(':')[0] : 'Error: ' + ex.getMessage());
        }

        return data;
    }

    @AuraEnabled
    public static Id saveRecord(donation_split__Designation__c classToSave){
        try {
            upsert classToSave;
            return classToSave.Id;
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') ? ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')[1].split(':')[0] : 'Error: ' + ex.getMessage());
        }
    }

    /**
     * @description ClassRoomWrapper
     */
    public class ClassRoomWrapper {
        /**
         * @description classroom record
         */
        @AuraEnabled
        public donation_split__Designation__c classroom{get;set;}
        
        /**
         * @description teaching Area Options
         */
        @AuraEnabled
        public List<options> teachingAreaOptions{get;set;}
        
        /**
         * @description hidePage
         */
        @AuraEnabled
        public List<options> gradesOptions{get;set;}

        
    }

    /**
     * @description options
     */
    public class options {
        /**
         * @description label
         */
        @AuraEnabled
        public String label {get;set;}
        /**
         * @description label
         */
        @AuraEnabled
        public String value {get;set;}

        public options(String pLabel, String pValue) {
            label = pLabel;
            value = pValue;
        }
    }
}