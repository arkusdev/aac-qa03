global class ExpiredFundContactUpdateBatch implements Database.Batchable<sObject>, Database.Stateful{
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        ExpFundsFlag__c efg = ExpFundsFlag__c.getOrgDefaults();
        efg.Expired_Fund_Contact_Batch__c = true;
        update efg;
        
        return Database.getQueryLocator('SELECT Contact__c, Deduction__c FROM Expired_Fund_Contact_Update__c WHERE Processed__c = false');
    }

    public void execute(Database.BatchableContext BC, List<Expired_Fund_Contact_Update__c> scope){
        String log = '';
        
        try{
            List<String> contactIds = new List<String>();
            for(Expired_Fund_Contact_Update__c efcu : scope){
                contactIds.add(efcu.Contact__c);
            }
            
            Map<Id, Contact> contactMap = new Map<Id, Contact>([SELECT Funds_Available__c FROM Contact WHERE Id IN: contactIds]);
            
            for(Expired_Fund_Contact_Update__c efcu : scope){
                
                if(contactMap.containsKey(efcu.Contact__c)){
                
                    efcu.Processed__c = true;
                    
                    if(efcu.Deduction__c != null){
                        //contactMap.get(efcu.Contact__c).Funds_Available__c -= efcu.Deduction__c;
                    }
                }
            }
    
            update contactMap.values();
            update scope;
        }catch(Exception e){
            log += 'EXCEPTION ExpiredFundContactUpdateBatch: ' + e.getStackTraceString();
            insert new Expired_Fund_Log__c(Log__c = log);
        }
    }

    public void finish(Database.BatchableContext BC){ 
        ExpFundsFlag__c efg = ExpFundsFlag__c.getOrgDefaults();
        efg.Expired_Fund_Contact_Batch__c = false;
        update efg;
    }
}