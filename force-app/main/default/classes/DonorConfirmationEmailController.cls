public with sharing class DonorConfirmationEmailController {
	public Id pId { get; set; }

  public List<pymt__Shopping_Cart_Item__c> getCartItems() {

    System.debug(this.pId);

    return [select Id,
                   Donation__r.Campaign.Classroom__r.Name,
                   pymt__Payment__r.pymt__Contact__r.Name,
                   pymt__Unit_Price__c,
                   School_Name__c,
                   Teacher_Name__c
            from pymt__Shopping_Cart_Item__c
            where pymt__Payment__c = :this.pId];
  }
}