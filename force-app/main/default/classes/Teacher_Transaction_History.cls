public with sharing class Teacher_Transaction_History {
	
    @AuraEnabled
    public static List<Opportunity> getTransactions(String teacherId) {
        List<Opportunity> donations = [SELECT Donor_Name__c, Amount, Donor_Comment__c, CloseDate
                                               FROM Opportunity
                                               WHERE Teacher__c = :teacherId AND RecordType.DeveloperName IN ('adoption', 'Disbursement', 'schooldonation') AND StageName = 'Sale Closed / Paid' AND CloseDate = LAST_N_DAYS:548 AND (NOT Name  LIKE '%Teacher Credit Card%') AND Hide_Transaction_on_Profile__c = FALSE
                                               ORDER BY CloseDate DESC];
        return donations;
    }
}