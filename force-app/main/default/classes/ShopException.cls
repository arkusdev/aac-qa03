public class ShopException extends Exception {

  public enum Type { Admin, Provisioning, UserGroup, Impersonation, Address,
    AddressAssignment, LineItems, GetProduct, CreateSpendingAccount,
      SpendingAccountAssignment, UpdateSpendingAccount, UpdateShippingAddress}

  public Type type { get; set; }

  public ShopException(String message, Type type) {
    setMessage(message);
    this.type = type;
  }

}