public with sharing class ExpiringFundsNotificationSchedule implements Schedulable{

    public void execute(SchedulableContext sc){
        ExpiringFundsNotificationBatch b = new ExpiringFundsNotificationBatch(); 
        database.executebatch(b, 10);
    }
    
}