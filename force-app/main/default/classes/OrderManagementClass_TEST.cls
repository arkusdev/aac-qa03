@isTest
private class OrderManagementClass_TEST {
  @TestSetup
  private static void setupData() {
    Account a = new Account(Name = 'Test Organization', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Organization').getRecordTypeId());
    insert a;
      
    Contact c = new Contact(RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Donor').getRecordTypeId(),
                            LastName = 'Test Contact', AccountId = a.Id);  
    insert c;
      
    AcctSeed__GL_Account__c glAccount = new AcctSeed__GL_Account__c(Name ='4599 Teacher Restricted Released', AcctSeed__Sub_Type_1__c = 'Assets');
    insert glAccount;
      
    ChargentOrders__ChargentOrder__c co = new ChargentOrders__ChargentOrder__c(ChargentOrders__Account__c = a.Id, Payment_Contact__c = c.Id,
                                                                               ChargentOrders__Status__c = 'Complete', ChargentOrders__Subtotal__c = 100,
                                                                               ChargentOrders__Tax__c = 15, ChargentOrders__Shipping__c = 10);
    insert co;  
      
    Opportunity opp = new Opportunity(AccountId = a.Id, StageName = 'Sale Closed / Paid', CloseDate = system.today(),
                                      Name = 'Test Donation - Test Contact', npsp__Primary_Contact__c = c.Id, Amount = 100,
                                      RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Ecommerce_Credit_Card').getRecordTypeId(),
                                      Chargent_Order__c = co.Id);
    insert opp; 
    Shop_Settings__c ssc = TestUtils.createShopSettings();
    ssc.API_Endpoint__c = 'https://api.ordercloud.io/v1';  
    insert ssc;  
  }  
    
  @isTest
  private static void validateOrderPayments() {
    Opportunity opp = [SELECT Id, Chargent_Order__c FROM Opportunity WHERE Name = 'Test Donation - Test Contact' LIMIT 1];  
    update new Opportunity(Id = opp.Id, Order_Cloud_Order_Id__c = 'testId');  
    Test.setMock(HttpCalloutMock.class, new OrderManagementCalloutMock());
    Test.startTest();  
    OrderManagementClass.processOrderPayments(new map<Id, string>{opp.Id => 'testId'}); 
    Test.stopTest();
    Opportunity o = [SELECT Id, Amount FROM Opportunity WHERE Name LIKE 'Teacher Credit Card%' LIMIT 1];  
    system.assertEquals(o.Amount, 115);  
    Donor_emails__c de = new Donor_emails__c(Name = opp.Id, Donor_Email_Id__c = 'test@test.com');
    insert de;  
    OrderManagementClass.deleteDonorEmailRecordsFuture(new list<string>{System.JSON.serialize(de)});       
  }
}