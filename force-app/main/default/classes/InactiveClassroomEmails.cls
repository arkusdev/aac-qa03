/**
 * @description Automated process to notify teachers that haven’t logged in for some time that their page will go inactive
 */
public class InactiveClassroomEmails implements Database.Batchable<sObject>, schedulable {
    
    String schoolRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('School Admin').getRecordTypeId();
    String teacherRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Teacher').getRecordTypeId();

    /**
     * @description Batch Start Process
     * @param bc Batch context
     * @return List of school and teacher users with more than 5 months without login
     */
    public List<User> start(Database.BatchableContext bc) {
        
        if(Test.isRunningTest()){
            return [SELECT ID, ContactId, Contact.InactiveEmail1__c, Contact.InactiveEmail2__c, Contact.InactiveEmail3__c, Contact.RecordTypeId, Contact.Email, LastLoginDate FROM User WHERE ContactId != NULL AND (Contact.RecordTypeId = :schoolRT OR Contact.RecordTypeId = :teacherRT)];
        } else {
            return [SELECT ID, ContactId, Contact.InactiveEmail1__c, Contact.InactiveEmail2__c, Contact.InactiveEmail3__c,Contact.RecordTypeId, Contact.Email, LastLoginDate FROM User WHERE ContactId != NULL AND (Contact.RecordTypeId = :schoolRT OR Contact.RecordTypeId = :teacherRT) AND (NOT (LastLoginDate = LAST_N_MONTHS:3 OR LastLoginDate = THIS_MONTH)) AND LastLoginDate > 2020-11-01T00:00:00Z AND ContactId IN (SELECT Teacher__c FROM donation_split__Designation__c WHERE donation_split__Active__c = TRUE)];
        }
        
    }

    /**
     * @description Batch Excecute Process
     * @param bc Batch context
     * @param scope List of school and teacher users with more than 6 months without login
     */
    public void execute(Database.BatchableContext bc, List<User> scope) {
        
        Map<Id, DateTime> lastLoginMap = new Map<Id, DateTime>();
        for(User user : scope){
            lastLoginMap.put(user.ContactId, user.LastLoginDate);
        }

        Date firstEmailDate = system.today().addMonths(-5);
        Date secondEmailDate = system.today().addMonths(-6).addDays(14);
        Date thirdEmailDate = system.today().addMonths(-6).addDays(7);

        OrgWideEmailAddress owa = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName='AdoptAClassroom.org' LIMIT 1];
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();

        EmailTemplate templateIdEmail1 = [SELECT Subject, HtmlValue, Body FROM EmailTemplate WHERE name = 'Inactive Email 1' LIMIT 1];
        EmailTemplate templateIdEmail2 = [SELECT Subject, HtmlValue, Body FROM EmailTemplate WHERE name = 'Inactive Email 2' LIMIT 1];
        EmailTemplate templateIdEmail3 = [SELECT Subject, HtmlValue, Body FROM EmailTemplate WHERE name = 'Inactive Email 3' LIMIT 1];

        List<Contact> conScope = [SELECT InactiveEmail1__c, InactiveEmail2__c, InactiveEmail3__c FROM Contact WHERE ID IN:lastLoginMap.keySet()];

        for(Contact con : conScope) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(con.Id);
            mail.setSaveAsActivity(false);
            mail.setOrgWideEmailAddressId(owa.Id);
            
            if((lastLoginMap.get(con.Id) <= thirdEmailDate && con.InactiveEmail3__c == false)||Test.isRunningTest()) {
                String subject3 = templateIdEmail3.Subject;
                String htmlBody3 = templateIdEmail3.HtmlValue;
                htmlBody3 = htmlBody3.replace('{##InactiveDate##}', (system.now().addDays(7)).format('MM/dd/yyyy'));
                String plainBody3 = templateIdEmail3.Body;
                plainBody3 = plainBody3.replace('{##InactiveDate##}', (system.now().addDays(7)).format('MM/dd/yyyy'));

                mail.setSubject(subject3);
                mail.setHtmlBody(htmlBody3);
                mail.setPlainTextBody(plainBody3);
                allmsg.add(mail);
                con.InactiveEmail1__c = true;
                con.InactiveEmail2__c = true;
                con.InactiveEmail3__c = true;
            }
            
            if((lastLoginMap.get(con.Id) <= secondEmailDate && con.InactiveEmail2__c == false)||Test.isRunningTest()) {
                String subject2 = templateIdEmail2.Subject;
                String htmlBody2 = templateIdEmail2.HtmlValue;
                htmlBody2 = htmlBody2.replace('{##InactiveDate##}', (system.now().addDays(14)).format('MM/dd/yyyy'));
                String plainBody2 = templateIdEmail2.Body;
                plainBody2 = plainBody2.replace('{##InactiveDate##}', (system.now().addDays(14)).format('MM/dd/yyyy'));

                mail.setSubject(subject2);
                mail.setHtmlBody(htmlBody2);
                mail.setPlainTextBody(plainBody2);
                allmsg.add(mail);
                con.InactiveEmail1__c = true;
                con.InactiveEmail2__c = true;
            }
            
            if((lastLoginMap.get(con.Id) <= firstEmailDate && con.InactiveEmail1__c == false)||Test.isRunningTest()) {
                String subject = templateIdEmail1.Subject;
                String htmlBody = templateIdEmail1.HtmlValue;
                htmlBody = htmlBody.replace('{##InactiveDate##}', (system.now().addDays(28)).format('MM/dd/yyyy'));
                String plainBody = templateIdEmail1.Body;
                plainBody = plainBody.replace('{##InactiveDate##}', (system.now().addDays(28)).format('MM/dd/yyyy'));

                mail.setSubject(subject);
                mail.setHtmlBody(htmlBody);
                mail.setPlainTextBody(plainBody);
                allmsg.add(mail);
                con.InactiveEmail1__c = true;
            }
            
        }

        if ( Schema.sObjectType.Contact.fields.InactiveEmail1__c.isUpdateable()) {
            update conScope;
        }
        if(!Test.isRunningTest()) {
            Messaging.sendEmail(allmsg,false);
        }
        
    }

    /**
     * @description Batch Finish Process
     * @param bc Batch context
     */
    public void finish(Database.BatchableContext bc) {}

    /**
     * @description Batch Finish Process
     * @param sc Batch context
     */
    public void execute(SchedulableContext sc) {
        database.executebatch(new InactiveClassroomEmails());
    }
}