@isTest
public class ExpiringFundsNotificationBatchTest{

    static testmethod void ExpiringFundsNotificationBatchTest1(){
        System.debug('*** test 1 begin');
        insert new AcctSeed__GL_Account__c(Name ='4599 Teacher Restricted Released',
                                            AcctSeed__Type__c = 'Revenue', AcctSeed__Sub_Type_1__c = 'Service Revenue');
        
        insert new Account(Name = 'Adopts classroom fund');
        
        insert new ExpFundsSett__c(/*FundsExpNot_1_Templ__c = '00X8A000000MHBn', FundsExpNot_2_Templ__c = '00X8A000000MHBn', 
                                    FundsExpNot_3_Templ__c = '00X8A000000MHBn', */
                                    Expired_Fund_Account__c = 'Adopts classroom fund', Expired_Fund_Record_Type__c = 'Administrative Adjustment', 
                                    Expired_Fund_Stage__c = 'Sale Closed / Paid', Expired_Fund_Fund__c = 'Classroom Adoption');
        
        insert new ExpFundsFlag__c();
        
        Id conRTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Teacher').getRecordTypeId();
        Contact teacher = new Contact(RecordTypeId = conRTId, LastName = 'Test', Email = 'test@test.com', Funds_Available__c = 500);
        insert teacher;
        
        Id oppRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Classroom Adoption').getRecordTypeId();
        Opportunity opp1 = new Opportunity(RecordTypeId = oppRTId, Name = 'Test opp', StageName='Pledged', 
                                            CloseDate = Date.Today().addDays(45), Teacher__c = teacher.Id, 
                                            Expiration_Date__c = Date.Today().addDays(45), Amount = 50);
        insert opp1;
        
        Opportunity opp2 = new Opportunity(RecordTypeId = oppRTId, Name = 'Test opp', StageName='Pledged', 
                                            CloseDate = Date.Today().addDays(46), Teacher__c = teacher.Id, 
                                            Expiration_Date__c = Date.Today().addDays(46), Amount = 50);
        insert opp2;
        
        List<Opportunity> oppList = [SELECT Id, Amount, AccountId, CloseDate, Teacher__c, Teacher_Email__c, Teacher__r.Name, Teacher_Funds_Available__c, Expiration_Date__c, FundsExpNot_1__c, FundsExpNot_2__c, FundsExpNot_3__c, FundsExpNot__c, ExpDateToday__c FROM Opportunity WHERE FundsExpNot__c = true];
        
        ExpiringFundsNotificationBatch efnbatch = new ExpiringFundsNotificationBatch();
        efnbatch.start(null);
        efnbatch.execute(null, oppList);
        efnbatch.finish(null);
        /*
        Contact t = [SELECT Id, Last_Expiring_Fund_Notification_Date__c, Funds_Available__c FROM Contact WHERE Id =: teacher.Id];
        DateTime dt = t.Last_Expiring_Fund_Notification_Date__c;
        System.assert(date.newinstance(dt.year(), dt.month(), dt.day()) == Date.today());
        */
        Opportunity o = [SELECT Funds_Expiration_Amount__c FROM Opportunity WHERE Id =: opp1.Id];
        System.assert(o.Funds_Expiration_Amount__c == 50);
        
        ExpiringFundsNotificationSchedule b = new ExpiringFundsNotificationSchedule();
        b.execute(null);
        System.debug('*** test 1 end');
    }
    
    static testmethod void ExpiringFundsNotificationBatchTest2(){
        
        System.debug('*** test 2 begin');
        insert new AcctSeed__GL_Account__c(Name ='4599 Teacher Restricted Released',
                                            AcctSeed__Type__c = 'Revenue', AcctSeed__Sub_Type_1__c = 'Service Revenue');
        
        insert new Account(Name = 'Adopts classroom fund');

        insert new ExpFundsSett__c(/*FundsExpNot_1_Templ__c = '00X8A000000MHBn', FundsExpNot_2_Templ__c = '00X8A000000MHBn', 
                                    FundsExpNot_3_Templ__c = '00X8A000000MHBn',*/
                                    Expired_Fund_Account__c = 'Adopts classroom fund', Expired_Fund_Record_Type__c = 'Administrative Adjustment', 
                                    Expired_Fund_Stage__c = 'Sale Closed / Paid', Expired_Fund_Fund__c = 'Classroom Adoption');
        
        insert new ExpFundsFlag__c();

        Id conRTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Teacher').getRecordTypeId();
        Contact teacher = new Contact(RecordTypeId = conRTId, LastName = 'Test', Email = 'test@test.com', Funds_Available__c = 500);
        insert teacher;
        
        Id oppRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Classroom Adoption').getRecordTypeId();
        Opportunity opp1 = new Opportunity(RecordTypeId = oppRTId, Name = 'Test opp', StageName='Pledged', 
                                            CloseDate = Date.Today(), Teacher__c = teacher.Id, 
                                            Expiration_Date__c = Date.Today().addDays(-1), Amount = 50);
        insert opp1;
        
        Opportunity opp2 = new Opportunity(RecordTypeId = oppRTId, Name = 'Test opp', StageName='Pledged', 
                                            CloseDate = Date.Today().addDays(46), Teacher__c = teacher.Id, 
                                            Expiration_Date__c = Date.Today().addDays(46), Amount = 50);
        insert opp2;
        
        List<Opportunity> oppList = [SELECT Id, Amount, AccountId, CloseDate, Teacher__c, Teacher_Email__c, Teacher__r.Name, Teacher_Funds_Available__c, Expiration_Date__c, FundsExpNot_1__c, FundsExpNot_2__c, FundsExpNot_3__c, FundsExpNot__c, ExpDateToday__c FROM Opportunity WHERE (FundsExpNot__c = true OR ExpDateToday__c = true)];
        
        ExpiringFundsNotificationBatch efnbatch = new ExpiringFundsNotificationBatch();
        efnbatch.start(null);
        efnbatch.execute(null, oppList);
        efnbatch.finish(null);
        
        Opportunity o = [SELECT Name, Amount FROM Opportunity WHERE Teacher__c =: teacher.Id AND RecordType.DeveloperName = 'adjustment'];
        System.assert(o.Name.contains(System.now().format('YYYY-MM-dd')));
        System.assert(o.Amount == -50);
        
        System.debug('*** test 2 end');
    }

}