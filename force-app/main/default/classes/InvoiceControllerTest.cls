@isTest
private class InvoiceControllerTest {

	@isTest static void testGetCart() {

    pymt__PaymentX__c p = new pymt__PaymentX__c(name='test payment');

    ApexPages.StandardController sc = new ApexPages.StandardController(p);
    InvoiceController ic = new InvoiceController(sc);

    ic.getShoppingCart();

    System.assertEquals(ic.cartItems.size(), 0);
	}

}