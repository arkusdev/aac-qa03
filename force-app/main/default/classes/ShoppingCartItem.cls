public with sharing class ShoppingCartItem {
  public Decimal amount { get; set; }
  public String type { get; set; }
  public Id classroomId { get; set; }
  public String frequency { get; set; }
}