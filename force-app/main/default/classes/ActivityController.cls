public with sharing class ActivityController {
    @AuraEnabled
    public static List<data> getData(String classroomId) {
        return ActivityUtils.getData(classroomId);
    }

    @AuraEnabled
    public static String saveRecord(String record) {
        string recordId = ActivityUtils.saveRecord(record);
        return recordId;
    }

    @AuraEnabled
    public static activityData getRecord(String recordId) {
        return ActivityUtils.getRecord(recordId);
    }

    @AuraEnabled
    public static void setComment(String recordId, String comment){
        ActivityUtils.setComment(recordId, comment);
    }

    @AuraEnabled
    public static void toggleAComment(String recordId){
        ActivityUtils.toggleAComment(recordId);
    }

    @AuraEnabled
    public static void delPhoto(String fileId){
        ActivityUtils.delPhoto(fileId);
    }

    public class data {
        
        @AuraEnabled public Announcement__c activity { get; set; }
        @AuraEnabled public Integer posts { get; set; }
        @AuraEnabled public List<ContentDistribution> attachmentURLs { get; set; }
        @AuraEnabled public String botPictureURL { get; set; }
        @AuraEnabled public String helpLink { get; set; }
        
        public data(Announcement__c pActivity, Integer pPosts, List<ContentDistribution> pAttachmentURLs, String pBotPictureURL, String pHelpLink) {
            activity = pActivity;
            posts = pPosts;
            attachmentURLs = pAttachmentURLs;
            botPictureURL = pBotPictureURL;
            helpLink = pHelpLink;
        }
    }

    public class activityData {
        
		@AuraEnabled public Announcement__c Announcement { get; set; }
        @AuraEnabled public User Teacher { get; set; }
        @AuraEnabled public List<Announcement_Comment__c> Comments { get; set; }
        @AuraEnabled public Boolean isGuest { get; set; }

        public activityData(Announcement__c pAnnouncement, User pTeacher, List<Announcement_Comment__c> pComments, Boolean pIsGuest) {
			Announcement = pAnnouncement;
            Teacher = pTeacher;
            Comments = pComments;
            isGuest = pIsGuest;
        }
    }
}