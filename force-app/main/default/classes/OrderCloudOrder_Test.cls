@isTest
public class OrderCloudOrder_Test {
    static testMethod void OrderCloudOrderTest() {
        
        OrderCloudOrder oco = new OrderCloudOrder();
        oco.ID ='Test';
        oco.Type = 'Test';
        oco.FromUserID = 'TestFromUserID';
        oco.FromUserFirstName = 'TestFromUserFirstName';
        oco.FromUserLastName = 'TestFromUserLastName';
        oco.Comments = 'TestComments';
        oco.LineItemCount = 1;
        oco.Status = 'TestStatus';
        oco.DateCreated = system.today();
        oco.DateSubmitted = system.today();
        oco.Subtotal = 1000;
        oco.ShippingCost = 100;
        oco.TaxCost = 20;
        oco.PromotionDiscount = 10;
        oco.Total = 1140;
        oco.IsSubmitted = true;
    }
}