@IsTest
private class TransactionHistoryControllerTest {
    
    static testMethod void TransactionHistoryControllerTest() {
        AcctSeed__GL_Account__c glAccount = new AcctSeed__GL_Account__c();
        glAccount.Name = '4599 Teacher Restricted Released';
        glAccount.AcctSeed__Sub_Type_1__c = 'Liabilities';
        insert glAccount;
        
        Account account = new Account();
        account.Name = 'Test';
        insert account;
        
        RecordType rt = [select Id
                         from RecordType 
                         where SObjectType = 'Opportunity' 
                         and DeveloperName ='adjustment'];
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.CloseDate = Date.today();
        opp.AccountId = account.Id;
        opp.StageName = 'Sale Closed / Paid';
        opp.RecordTypeId = rt.Id;
        
        insert opp;
        
        Test.startTest();
            TransactionHistoryController controller = new TransactionHistoryController();
        Test.stopTest();
        
        system.assertEquals(1, controller.commOppList.size());
        system.assertEquals(opp.Id, controller.commOppList[0].Opportunity__c);
    }
    
    static testMethod void getItemsTest() {
        TransactionHistoryController controller = new TransactionHistoryController();
        
        Integer rtCount = [select count()
                           from RecordType 
                           where SObjectType = 'Opportunity' 
                           and DeveloperName IN ('adjustment', 'Disbursement', 'schooldonation', 'adoption', 'Orders')];
        
        Test.startTest();
            List<SelectOption> optionList = controller.getItems();
        Test.stopTest();
        
        system.assertEquals(rtCount + 1, optionList.size());
    }
}