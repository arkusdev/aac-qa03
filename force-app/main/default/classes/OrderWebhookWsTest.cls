@isTest(SeeAllData=true)

private class OrderWebhookWsTest {
     static testMethod void testOrderWebhookWs() {
         
         string JSONMsg = '{"Type":"Standard","Total":58.55,"TaxCost":0.0,"Subtotal":58.55,"Status":"Open","ShippingCost":0.0,"PromotionDiscount":0.0,"LineItemCount":2,"IsSubmitted":true,"ID":"Sn04lsmaQ0CYqaL7UO_1UQ","FromUserLastName":"qa","FromUserID":"005W0000001z0fuIAA","FromUserFirstName":"rajesh","DateSubmitted":"2017-02-24T19:46:50.797Z","DateCreated":"2017-02-22T05:44:27.550Z","Comments":null}';
         
         RestRequest re = new RestRequest(); 
         RestResponse rp = new RestResponse();
         re.requestURI =  'https://stage-adoptaclassroom.cs52.force.com/teachers/services/apexrest/orderwebhook';
         re.httpMethod = 'POST';
         re.requestBody = Blob.valueof(JSONMsg);
         re.params.put('Id','testId');
         re.params.put('Type','standard');
        
         RestContext.request = re;
         RestContext.response = rp;
         
        Test.startTest();
        orderWebhookWs.OrderSubmissionRequest  ws = new orderWebhookWs.OrderSubmissionRequest(); 
        OrdercloudOrder oco = TestUtils.createOrderCloudOrder(userinfo.getUserId());
        // = orderwebhookws();
        ws.Response = new orderWebhookWs.OrderSubmissionResponse();
        ws.response.Body = oco;
         OrderWebhookWs.doPost();
        Test.stopTest();
     }
}