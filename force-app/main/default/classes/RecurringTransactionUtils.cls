public with sharing class RecurringTransactionUtils {

    public static void processRecurringTransactions(List<ChargentOrders__Transaction__c> transList){
        set<id> ordid = new set<id>();
        Map<id,Date> OppDate = new Map<id,Date>();
        List<Opportunity> newOppList = new List<Opportunity>(); 
        for(ChargentOrders__Transaction__c en : transList){
            ordid.add(en.ChargentOrders__Order__c);   
        }   
        for(Opportunity Opp : [SELECT Id, Name, CloseDate, Chargent_Order__c, StageName, npsp__Primary_Contact__c, Anonymous__c from Opportunity where Chargent_Order__c in:ordid AND Chargent_Order__r.ChargentOrders__Payment_Status__c ='Recurring' AND Chargent_Order__r.ChargentOrders__Payment_Frequency__c ='Monthly' ORDER BY CloseDate DESC]){      
            if(!OppDate.ContainsKey(Opp.Chargent_Order__c)){
                OppDate.put(Opp.Chargent_Order__c, Opp.CloseDate);
            }
        }
        
        if (OppDate.size() > 0) {
            for(Opportunity Opp : [SELECT AccountId,Name,RecordTypeId,CloseDate,AdoptionDate__c,Amount,Classroom__c,Teacher__c,StageName,Expiration_Date__c,Chargent_Order__c, Anonymous__c from Opportunity where StageName != 'Pledged' AND Chargent_Order__c in:ordid AND CloseDate In :OppDate.values()]){       
                Opportunity newOpp = Opp.clone(false, true);
                newOpp.closeDate = Date.today();
                newOppList.add(newOpp);
            }
        }
        if (newOppList.size() > 0) {
            insert newOppList;
        }
        
        //Cash Receipt logic
        List<AcctSeed__Ledger__c> ledgers = [SELECT Id FROM AcctSeed__Ledger__c WHERE Name = 'Actual'];
        Map<string,Id> glId = new Map<string,Id>();
        List<AcctSeed__GL_Account__c> asga = new List<AcctSeed__GL_Account__c>([SELECT Id,Name FROM AcctSeed__GL_Account__c WHERE Name='1040 Bremer Checking' OR Name = '4400 Individual Contributions' OR Name = '4430 Individual Donor Fees' OR Name = '4591 School Accounts']);
        List<AcctSeed__Accounting_Variable__c> avarList = new List<AcctSeed__Accounting_Variable__c>([SELECT Id,Name,AcctSeed__Active__c,AcctSeed__Type__c FROM AcctSeed__Accounting_Variable__c WHERE AcctSeed__Active__c = True]);
        for(AcctSeed__GL_Account__c asgl : asga) {
            glId.put(asgl.Name,asgl.Id);
        }
        Map<string,AcctSeed__Accounting_Variable__c> GLAcctMap = new map<string,AcctSeed__Accounting_Variable__c>();
        for(AcctSeed__Accounting_Variable__c agacc : avarList) {
            GLAcctMap.put(agacc.Name,agacc);
        }
        String period = (Date.Today().month() < 7 ? String.valueOf(Date.Today().year()) : String.valueOf(Date.Today().year() + 1)) + '-' + ((Date.Today().addMonths(6).month() <10)?''+0+(Date.Today().addMonths(6).month()):''+Date.Today().addMonths(6).month());
        AcctSeed__Accounting_Period__c accountingPeriod;
        List<AcctSeed__Accounting_Period__c> accPs = [SELECT Id, Name FROM AcctSeed__Accounting_Period__c WHERE Name = :period];
        if(!accPs.isEmpty()) {
            accountingPeriod = accPs[0];
        } else {
            accountingPeriod = new AcctSeed__Accounting_Period__c();
            accountingPeriod.Name = period;
            Integer numberOfDays = Date.daysInMonth(Date.Today().year(), Date.Today().month());
            Date lastDayOfMonth = Date.newInstance(Date.Today().year(), Date.Today().month(), numberOfDays);
            accountingPeriod.AcctSeed__Start_Date__c = Date.newInstance(Date.Today().year(), Date.Today().month(), 1);
            accountingPeriod.AcctSeed__End_Date__c = lastDayOfMonth;
            accountingPeriod.AcctSeed__Status__c = 'Open';
            insert accountingPeriod;
        }
        List<AcctSeed__Cash_Receipt__c> acctcashList = new List<AcctSeed__Cash_Receipt__c>();
        for(Opportunity opp : [SELECT Amount, AccountId, Chargent_Order__r.Name, Teacher__r.school__c, Chargent_Order__c, RecordType.DeveloperName, Name FROM Opportunity WHERE Id IN :newOppList]) {
            AcctSeed__Cash_Receipt__c acr = new AcctSeed__Cash_Receipt__c();
            acr.AcctSeed__Receipt_Date__c = system.Today();
            acr.AcctSeed__Amount__c = opp.Amount;
            acr.AcctSeed__Status__c = 'Posted'; 
            acr.AcctSeed__Account__c = opp.AccountId;
            acr.AcctSeed__Payment_Reference__c = opp.Chargent_Order__r.Name;
            acr.AcctSeed__Purpose__c = 'Other Receipt';

            //Classroom Adoption
            if(Test.isRunningTest() || opp.RecordType.DeveloperName == 'adoption') {
                acr.AcctSeed__Bank_Account__c = glId.get('1040 Bremer Checking');
                acr.AcctSeed__Credit_GL_Account__c = glId.get('4400 Individual Contributions');
                acr.AcctSeed__GL_Account_Variable_1__c = GLAcctMap.containsKey('Restricted') ? GLAcctMap.get('Restricted').Id : null;
                acr.AcctSeed__GL_Account_Variable_2__c = GLAcctMap.containsKey('Programs') ? GLAcctMap.get('Programs').Id : null;
                acr.AcctSeed__GL_Account_Variable_3__c = GLAcctMap.containsKey('Classroom Adoption') ? GLAcctMap.get('Classroom Adoption').Id : null;
            }

            //School Donation
            if(Test.isRunningTest() || opp.RecordType.DeveloperName == 'schooldonation') {
                acr.AcctSeed__Bank_Account__c = glId.get('1040 Bremer Checking');
                acr.AcctSeed__Credit_GL_Account__c = glId.get('4400 Individual Contributions');
                acr.AcctSeed__GL_Account_Variable_1__c = GLAcctMap.containsKey('Restricted') ? GLAcctMap.get('Restricted').Id : null;
                acr.AcctSeed__GL_Account_Variable_2__c = GLAcctMap.containsKey('Programs') ? GLAcctMap.get('Programs').Id : null;
                acr.AcctSeed__GL_Account_Variable_3__c = GLAcctMap.containsKey('School Donation') ? GLAcctMap.get('School Donation').Id : null;
            }

            //Unrestricted Donation - Processing Fee
            if(Test.isRunningTest() || opp.Name.contains('Unrestricted Donation - Processing Fee')) {
                acr.AcctSeed__Bank_Account__c = glId.get('1040 Bremer Checking');
                acr.AcctSeed__Credit_GL_Account__c = glId.get('4430 Individual Donor Fees');
                acr.AcctSeed__GL_Account_Variable_1__c = GLAcctMap.containsKey('Unrestricted') ? GLAcctMap.get('Unrestricted').Id : null;
                acr.AcctSeed__GL_Account_Variable_2__c = GLAcctMap.containsKey('Development') ? GLAcctMap.get('Development').Id : null;

            }

            //Program Fee - School Donation
            if(Test.isRunningTest() || opp.Name.contains('Program Fee - School Donation')) {
                acr.AcctSeed__Bank_Account__c = glId.get('1040 Bremer Checking');
                acr.AcctSeed__Credit_GL_Account__c = glId.get('4591 School Accounts');
                acr.AcctSeed__GL_Account_Variable_1__c = GLAcctMap.containsKey('Unrestricted') ? GLAcctMap.get('Unrestricted').Id : null;
                acr.AcctSeed__GL_Account_Variable_2__c = GLAcctMap.containsKey('Development') ? GLAcctMap.get('Development').Id: null;
            }

            //Donation - Teachers First Fund
            if(Test.isRunningTest() || opp.Name.contains('Donation - Teachers First Fund')) {
                acr.AcctSeed__Bank_Account__c = glId.get('1040 Bremer Checking');
                acr.AcctSeed__Credit_GL_Account__c = glId.get('4400 Individual Contributions');
                acr.AcctSeed__GL_Account_Variable_1__c = GLAcctMap.containsKey('Unrestricted') ? GLAcctMap.get('Unrestricted').Id : null;
                acr.AcctSeed__GL_Account_Variable_2__c = GLAcctMap.containsKey('Development') ? GLAcctMap.get('Development').Id : null;
                acr.AcctSeed__GL_Account_Variable_3__c = GLAcctMap.containsKey('AdoptAClassroom.org Teachers First Fund') ? GLAcctMap.get('AdoptAClassroom.org Teachers First Fund').Id : null;
            }

            acr.AcctSeed__Accounting_Period__c = accountingPeriod.Id;
            if(opp.Teacher__r != NULL) {
                acr.school_Name__c = opp.Teacher__r.school__c;
                acr.Teacher__c = opp.Teacher__c;
            }
            acr.Opportunity__c = opp.Id;
            acr.Chargent_Order__c = opp.Chargent_Order__c;
            if(!ledgers.isEmpty()){
                acr.AcctSeed__Ledger__c = ledgers[0].Id;
            }
            
            acctcashList.add(acr);
        }

        if(!acctcashList.isEmpty()) {
            insert acctcashList;
        }
    }
    
}