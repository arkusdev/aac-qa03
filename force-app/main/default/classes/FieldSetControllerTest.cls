@isTest
private class FieldSetControllerTest {
  private static Contact contact1;
  private static User user1;

  static testMethod void getCurrentContact_test(){
    Account account1 = TestUtils.createAccount();
    
    

    contact1 = TestUtils.createContact(account1.Id);
    
    User thisUser = TestUtils.createSystemAdministrator();
    
    insert thisUser;
    system.runAs(thisUser){
    insert account1;
    insert contact1;
   
     user u = FieldSetController.getCurrentUser();
    system.debug('VVVVVVVVVVVVVVVVV'+u);
     Contact testContact;
    
   
      testContact = FieldSetController.getCurrentContact();
    }
/*
      user1 = TestUtils.createCommunityUser();
      user1.ContactId = contact1.Id;
      insert user1;
      
    
 //   system.assertEquals(contact1.Id, testContact.Id);
    }
    
    system.runAs(user1){
    Contact testContact;
     user u = FieldSetController.getCurrentUser();
    system.debug('VVVVVVVVVVVVVVVVV'+u);
    system.debug('JJJJJJJJJJJJJJJJJJJ'+user1.contactId);
   
      testContact = FieldSetController.getCurrentContact();
    } */
    
  }

  static testMethod void getTypeNames_test(){
    List<String> typeNames = FieldSetController.getTypeNames();
    system.assert(!typeNames.isEmpty());
  }

  static testMethod void getFieldSetNames_getFields_test(){
    init();
  //  system.runAs(user1){
      List<String> fieldSetNames = FieldSetController.getFieldSetNames('Contact');
    //  system.assert(!fieldSetNames.isEmpty());

      List<FieldSetMember> fields = FieldSetController.getFields('Contact', 'User_Profile_Fields');
    //  system.assert(!fields.isEmpty());
   // }
  }

  static testMethod void getCommunitiesUserProfileAdditionalFields_test(){
    List<Schema.FieldSetMember> additionalFields = FieldSetController.getCommunitiesUserProfileAdditionalFields();
   // system.assert(!additionalFields.isEmpty());
  }

  static testMethod void updateFields_test(){
     Account account1 = TestUtils.createAccount();
    

    contact1 = TestUtils.createContact(account1.Id);
    
    User thisUser = TestUtils.createSystemAdministrator();
    insert thisUser;
    system.runAs(thisUser){
    insert account1;
    insert contact1;

      user1 = TestUtils.createCommunityUser();
      user1.ContactId = contact1.Id;
      insert user1;
      contact1.Email = 'new@email.com';
    FieldSetController.updateFields(contact1);
    system.assertEquals('new@email.com', [SELECT Email FROM Contact WHERE Id = :contact1.Id].Email);
    }

    
  }

  private static void init(){
    Account account1 = TestUtils.createAccount();
    

    contact1 = TestUtils.createContact(account1.Id);
    
    User thisUser = TestUtils.createSystemAdministrator();
    insert thisUser;
    system.runAs(thisUser){
    insert account1;
    insert contact1;

      user1 = TestUtils.createCommunityUser();
      user1.ContactId = contact1.Id;
      insert user1;
    }
  }
}