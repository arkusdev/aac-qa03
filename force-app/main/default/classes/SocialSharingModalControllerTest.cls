@isTest
public with sharing class SocialSharingModalControllerTest {
    @TestSetup
    static void makeData(){
        donation_split__Designation__c classroom = new donation_split__Designation__c();
        classroom.How_will_you_spend_your_funds__c = 'This is a test, minium 100 characters.This is a test, minium 100 characters.This is a test, minium 100 characters.';
        classroom.Description__c = 'This is a test, minium 100 characters.This is a test, minium 100 characters.This is a test, minium 100 characters. This is a test, minium 100 characters.This is a test, minium 100 characters.This is a test, minium 100 characters. This is a test, minium 100 characters.';
        insert classroom;
    }

    @IsTest
    static void successMethodTest(){
        donation_split__Designation__c classroom = [SELECT Id FROM donation_split__Designation__c classroom LIMIT 1];
        donation_split__Designation__c testClass = SocialSharingModalController.getClassroom(classroom.Id);
        System.assertEquals(classroom.Id, testClass.Id);
    }

    @IsTest
    static void failMethodTest(){
        String exc = '';
        try {
            SocialSharingModalController.getClassroom('invalid');
        } catch (QueryException qe) {
            System.debug(qe);
            exc = qe.getMessage();
        }
        System.assertEquals('List has no rows for assignment to SObject', exc);
    }
}