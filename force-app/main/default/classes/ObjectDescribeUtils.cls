public with sharing class ObjectDescribeUtils {
  
  public static final Integer BATCH_SIZE = 1;

  private static final String METHOD_GET = 'GET';
  private static final String LAYOUT_ENDPOINT_TEMPLATE =
    '{1}/services/data/v28.0/sobjects/{0}/describe';
  private static final String AUTHORIZATION_HEADER = 'Authorization';
  private static final String OAUTH_TEMPLATE = 'Bearer {0}';
  private static final String DOCUMENT_NAME_TEMPLATE = 'ObjectDescribe_{0}';
  private static final String TIMESTAMP_TEMPLATE = '_{0}';
  private static final String FOLDER_NAME = 'Object_Describes';
  private static final String TYPE_JSON = 'json';
  private static final String UNDERSCORE_C = '__c';
  private static final String DOUBLE_UNDERSCORE = '__';
  private static final String DC_REPLACE = 'duc';
  private static final String CUSTOM_C = '_custom';
  private static final String JOB_NAME = 'ObjectDescribeJob';
  private static final String LIKE_TEMPLATE = '%{0}%';
  private static final Integer DELAY = 10;
  private static final Integer STATUS_CODE_OK = 200;

  public static SObjectDescribe getDescribeInfoFromDocument(String objectName) {
    if (String.isBlank(objectName)) {
      return null;
    }

    JSONParser parser;
    String documentName = generateDocumentName(objectName);
    Document doc = getExistingDocument(documentName);

    if (doc == null) {
      return null;
    }

    parser = JSON.createParser(doc.Body.toString());

    return (SObjectDescribe) parser.readValueAs(SObjectDescribe.class);
  }

  public static void updateDescribeDocument(String objectName,
      String sessionId) {
    if (String.isBlank(objectName) || String.isBlank(sessionId)) {
      return;
    }

    JSONParser parser;
    SObjectDescribe od;
    String documentName = generateDocumentName(objectName);
    String bodyString;
    Document doc = getExistingDocument(documentName);

    if (doc == null) {
      doc = initializeNewDocument(documentName);
    }

    if (doc == null) {
      return;
    }

    bodyString = getDescribeInforForObject(objectName, sessionId);

    if (String.isBlank(bodyString)) {
      return;
    }

    parser = JSON.createParser(bodyString);
    od = (SObjectDescribe) parser.readValueAs(SObjectDescribe.class);

    doc.Body = Blob.valueOf(JSON.serialize(od));
    upsert doc;
  }

  private static String generateDocumentName(String objectName) {
    String n = objectName.replace(UNDERSCORE_C, CUSTOM_C);

    n = n.replace(DOUBLE_UNDERSCORE, DC_REPLACE);

    return String.format(DOCUMENT_NAME_TEMPLATE, new List<String> { n });
  }

  private static Document initializeNewDocument(String documentName) {
    Id folderId = getFolderId();

    if (folderId == null) {
      return null;
    }

    String timeStampString = String.valueOf(Datetime.now().getTime());
    String dn = documentName + String.format(TIMESTAMP_TEMPLATE,
      new List<String> { timeStampString });

    return new Document(
      Name = dn,
      DeveloperName = dn,
      FolderId = folderId,
      Type = TYPE_JSON);
  }

  private static Id getFolderId() {
    try {
      return [select Id
              from Folder
              where DeveloperName = :FOLDER_NAME].Id;
    } catch (Exception e) {
      return null;
    }
  }

  private static Document getExistingDocument(String documentName) {
    String likeTerm = String.format(LIKE_TEMPLATE,
      new List<String> { documentName });

    try {
      return [select Id,
                     Name,
                     DeveloperName,
                     Body
              from Document
              where DeveloperName like :likeTerm
              order by CreatedDate desc
              limit 1];
    } catch (Exception e) {
      return null;
    }
  }

  private static String getDescribeInforForObject(
      String objectName, String sessionId) {
    String endPoint;
    HttpRequest request = new HttpRequest();
    HttpResponse response;
    Http http = new Http();
    Page_Layout_Settings__c settings = Page_Layout_Settings__c.getInstance();

    if (settings == null || String.isBlank(settings.Instance_Url__c)) {
      return null;
    }

    endPoint = String.format(LAYOUT_ENDPOINT_TEMPLATE, new List<String> {
      objectName, settings.Instance_Url__c });

    request.setEndpoint(endPoint);
    request.setMethod(METHOD_GET);

    request.setHeader(AUTHORIZATION_HEADER, String.format(OAUTH_TEMPLATE,
      new List<String> { sessionId }));

    response = http.send(request);

    if (response.getStatusCode() != STATUS_CODE_OK) {
      return null;
    }

    return response.getBody();
  }
}