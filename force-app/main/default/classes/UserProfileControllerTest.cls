@isTest
private class UserProfileControllerTest {

  static testMethod void testUserProfileController() {
       User tuser = TestUtils.createSystemAdministrator();
    insert tuser;
    system.runas(tuser)
    {
    Account a = TestUtils.createAccount();
    insert a;

    Contact c = TestUtils.createContact(a.Id);
    insert c;

    User testUser = TestUtils.createCommunityUser();
    testUser.ContactId = c.Id;
    insert testUser;

    Test.startTest();

    System.assertEquals(null, UserProfileController.getContactForUser(
      null, null));
    Contact testContact = UserProfileController.getContactForUser(
      UserInfo.getUserId(), new List<String> { 'test' });
    System.assertEquals(null, testContact);

    testContact = UserProfileController.getContactForUser(testUser.Id,
      new List<String> { 'User_Profile_Fields' });

    System.assertEquals(c.Id, testContact.id);

    testContact.LastName = 'NewLastName';

    UserProfileController.updateContact(null);
    UserProfileController.updateContact(testContact);

    Test.stopTest();
    }
  }

}