@isTest
private class ApplicationUtilsTest {
	static testMethod void testApplicationUtils() {
    Test.startTest();

    System.assertEquals(null,
      ApplicationUtils.getSelectOptionsForFieldAndObject(null, null, false));


    System.assertNotEquals(null,
      ApplicationUtils.getSelectOptionsForFieldAndObject(
        Account.SObjectType.getDescribe(),
          new List<String> { 'AccountSource' }));

    Test.stopTest();
  }
}