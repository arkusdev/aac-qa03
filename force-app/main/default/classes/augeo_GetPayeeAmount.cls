public class augeo_GetPayeeAmount {

 
 public Id CDBatch_Id {get;set;}
    public List<AcctSeed__Cash_Disbursement__c> getpayeeamt()
    {
        List<AcctSeed__Cash_Disbursement__c> cdpayeeamt;
        cdpayeeamt = [SELECT AcctSeed__Payee__c, AcctSeed__Amount__c FROM AcctSeed__Cash_Disbursement__c WHERE AcctSeed__Cash_Disbursement_Batch__c =: CDBatch_Id];
        return cdpayeeamt;
    }
}