@IsTest
public class ActivityControllerTest {

    @testSetup static void initTestData() {
        Account accountData= TestUtils.createAccount();
        insert accountData;
        
        Contact contactData = TestUtils.createContact(accountData.Id);
        contactData.npe01__WorkEmail__c='test@gmail.com';        
        insert contactData ;
        
        donation_split__Designation__c classroom = TestUtils.createClassroom(contactData.Id, accountData.Id);
        insert classroom;
        
        List<AcctSeed__Accounting_Variable__c> acvlist = new list<AcctSeed__Accounting_Variable__c>();
    
        AcctSeed__Accounting_Variable__c  acv1 = new AcctSeed__Accounting_Variable__c();
        acv1.AcctSeed__Type__c = 'GL Account Variable 1';
        acv1.AcctSeed__Active__c = true;
        acv1.Name = 'Unrestricted';
        
        AcctSeed__Accounting_Variable__c  acv2 = new AcctSeed__Accounting_Variable__c();
        acv2.AcctSeed__Type__c = 'GL Account Variable 2';
        acv2.AcctSeed__Active__c = true;
        acv2.Name = 'Programs';
        
        AcctSeed__Accounting_Variable__c  acv3 = new AcctSeed__Accounting_Variable__c();
        acv3.AcctSeed__Type__c = 'GL Account Variable 3';
        acv3.AcctSeed__Active__c = true;
        acv3.Name = 'Classroom Adoption';
        
        AcctSeed__Accounting_Variable__c  acv4 = new AcctSeed__Accounting_Variable__c();
        acv4.AcctSeed__Type__c = 'GL Account Variable 1';
        acv4.AcctSeed__Active__c = true;
        acv4.Name = 'Restricted';
        
        acvlist.add(acv1);
        acvlist.add(acv2);
        acvlist.add(acv3);
        acvlist.add(acv4);
        
        insert acvlist;
        
        AcctSeed__GL_Account__c asgl = new  AcctSeed__GL_Account__c();
        asgl.Name = '4599 Teacher Restricted Released';
        asgl.AcctSeed__Active__c = true;
        asgl.AcctSeed__Type__c = 'Revenue';
        Insert asgl;
        
        User commUser=TestUtils.createCommunityUser();
        commUser.contactId=contactData.Id;
        insert commUser;

        Announcement__c activity = new Announcement__c();
        activity.Comment__c = 'Test comment';
        activity.Pin_at_Top__c = true;
        activity.Classroom__c = classroom.Id;
        activity.Flagged__c = false;
        insert activity;

        Announcement__c activity2 = new Announcement__c();
        activity2.Comment__c = 'Test comment2';
        activity2.Pin_at_Top__c = true;
        activity2.Classroom__c = classroom.Id;
        activity2.Flagged__c = false;
        insert activity2;

        Announcement_Comment__c comment = new Announcement_Comment__c();
        comment.Comment__c = 'Test comment';
        comment.Announcement__c = activity.Id;
        comment.Approved__c = true;
        insert comment;

        ContentVersion content=new ContentVersion(); 
            content.Title='Header_Picture1'; 
            content.PathOnClient='/Header_Picture1.jpg'; 
            Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
            content.VersionData=bodyBlob;
            content.origin = 'H';
        insert content;
		
		ContentDocumentLink contentlink=new ContentDocumentLink();
            contentlink.LinkedEntityId=activity.id;
            contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: content.id].contentdocumentid;
            contentlink.ShareType = 'V';
            test.starttest();
        insert contentlink;
    }
    
    @IsTest
    public static void testGetData() {

        String classroomId = [SELECT Id FROM donation_split__Designation__c].Id;

        Test.startTest();
        List<ActivityController.data> result = ActivityController.getData(classroomId);
        Test.stopTest();
    }

    @IsTest
    public static void testSaveRecord() {
        
        String recordStr = JSON.serialize([SELECT ID FROM Announcement__c LIMIT 1][0]);
        
        Test.startTest();
        ActivityController.saveRecord(recordStr);
        Test.stopTest();
    }

    @IsTest
    public static void testGetRecord() {

        String recordId = [SELECT ID FROM Announcement__c LIMIT 1][0].Id;

        Test.startTest();
        ActivityController.activityData activityData = ActivityController.getRecord(recordId);
        Test.stopTest();
    }

    @IsTest
    public static void testSetComment() {

        Announcement__c activity = [SELECT ID FROM Announcement__c LIMIT 1];

        Test.startTest();
        ActivityController.setComment(activity.Id, 'Test');
        Test.stopTest();
    }

    @IsTest
    public static void testToggleAComment() {

        Announcement_Comment__c comment = [SELECT ID FROM Announcement_Comment__c LIMIT 1];

        Test.startTest();
        ActivityController.toggleAComment(comment.Id);
        Test.stopTest();
    }

    @IsTest
    public static void testDelPhoto() {
        
        List<ContentDocument> cd = [SELECT ID FROM ContentDocument LIMIT 1];
        
        Test.startTest();
        ActivityController.delPhoto(cd[0].Id);
        Test.stopTest();
    }
}