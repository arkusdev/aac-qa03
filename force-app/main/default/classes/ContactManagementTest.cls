@isTest
private class ContactManagementTest {

	static testMethod void testContactManagement() {
		Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());
		Account a = TestUtils.createAccount();
		a.Account_Status__c = 'Active';
		insert a;

		Account school = TestUtils.createAccount();
		school.ShippingStreet = '123 Main Street';
		school.ShippingCity = 'Minneapolis';
		school.ShippingStateCode = 'MN';
		school.ShippingCountryCode = 'US';
		school.ShippingPostalCode = '12345';
		insert school;

		Contact c = TestUtils.createContact(a.Id);
		c.School__c = school.Id;
		c.School__r = school;
		c.Order_Cloud_Spending_Account_Id__c = 'testId';
		c.Billing_Address_Id__c = 'test';
		c.Mailing_Address_Id__c = 'test';
		insert c;

		c.Funds_Available__c = 1000.00;
		update c;
		
		donation_split__Designation__c classroom = new donation_split__Designation__c();
		classroom.Teacher__c = c.Id;
		classroom.School__c = school.Id;
		classroom.How_will_you_spend_your_funds__c = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean m';
		classroom.Description__c = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium.';
		insert classroom;
		
		Test.startTest();
			ContactManagement.updateSpendingAccount(c,c);
			System.enqueueJob(new SpendingAccountUpdateQueueable(new List<Contact>{c}));
		Test.stopTest();
	}
}