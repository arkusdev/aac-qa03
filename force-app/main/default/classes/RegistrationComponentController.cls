public without sharing class RegistrationComponentController {

  @AuraEnabled
  public static Id createUser(Contact contact) {
    if (contact == null || String.isBlank(contact.Email)) {
      return null;
    }

    User user = new User();
    Self_Registration_Settings__c settings =
      Self_Registration_Settings__c.getInstance();
    Database.DMLOptions dmo = new Database.DMLOptions();

    String recordTypeName = settings.Default_Record_Type_Name__c;
    Id householdRecordId = null;

    try {
      householdRecordId = [select Id from RecordType
        where DeveloperName = :recordTypeName limit 1].id;
    } catch(Exception e){
      System.debug(e);
    }

    Account a = new Account(
      Name = contact.FirstName + ' ' + contact.LastName,
      OwnerId = settings.Portal_Account_Onwer_Id__c,
      RecordTypeId = householdRecordId
    );

    dmo.EmailHeader.triggerUserEmail = true;

    insert a;

    contact.AccountId = a.Id;
    insert contact;

    user.Email = contact.Email;
    user.FirstName = contact.FirstName;
    user.LastName = contact.LastName;
    user.Username = user.Email;
    user.CommunityNickname = user.Email;
    user.setOptions(dmo);
    /* Priya Added */
    system.debug('RRRRRRRRRRRRRRRRR'+contact);
    system.debug('RRRRRRRRRRRRRRRRR'+a);
    system.debug('RRRRRRRRRRRRRRRRR'+user);
    return Site.createExternalUser(user, a.Id);
  }

  @AuraEnabled
  public static Boolean getIsUsernameTaken(String un) {
    User u;

    try {
      u = [select Id
           from User
           where Username = :un];

      return true;
    } catch (Exception e) {
      return false;
    }
  }
}