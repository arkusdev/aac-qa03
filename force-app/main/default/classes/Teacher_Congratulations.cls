public class Teacher_Congratulations {
	
    @AuraEnabled
    public static String getClassroomId(){
        String classroomPageUrl= '';
        User usr = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];
        List<donation_split__Designation__c> classrooms = [SELECT Id, Public_Classroom_Landing_Page_URL__c,
                                                           Classroom_Link__c
                                                           FROM donation_split__Designation__c 
                                                           WHERE Teacher__c = :usr.ContactId Limit 1];
        if( classrooms.size() > 0){
            classroomPageUrl= classrooms[0].Id;
        }
        return classroomPageUrl;
    }
}