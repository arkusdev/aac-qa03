public with sharing class InvoiceController {

  public pymt__PaymentX__c payment {get; set;}
  public Date nextChargeDate {get; set;}
  public List<pymt__Shopping_Cart_Item__c> cartItems {get; set;}

	public InvoiceController(ApexPages.StandardController stdController) {
    this.payment = (pymt__PaymentX__c)stdController.getRecord();
    getShoppingCart();
    nextChargeDate = ShoppingCartController.getNextMonthDate();
	}

  public void getShoppingCart(){
    cartItems = [select Id,
        Name,
        pymt__Payment__c,
        pymt__Contact__r.School__r.Name,
        pymt__Contact__r.Name,
        Donation__c,
        pymt__Total__c
      from pymt__Shopping_Cart_Item__c
      where pymt__Payment__c = :payment.Id];
  }

}