/**
 * @description Handler of the ContentDocumentLinkTrigger Trigger process
 */
public without sharing class ContentDocumentLinkTriggerSchoolHandler {
    
    /**
     * @description Get updates related classroom with the PDF file is uploaded
     */
    public static void checkPDFFile() {
        if(Trigger.isAfter && Trigger.isInsert) {
            List<Id> cdIds = new List<Id>();
            for(ContentDocumentLink cdl : (List<ContentDocumentLink>)Trigger.new) {
                cdIds.add(cdl.ContentDocumentId);
            }
            List<Id> cdPDFIds = new List<Id>();
            for(ContentDocument cd : [SELECT Id FROM ContentDocument WHERE Id IN :cdIds AND FileType='PDF' ]){
                cdPDFIds.add(cd.Id);
            }
            if(!cdPDFIds.isEmpty()) {
                Set<Id> parentIds = new Set<Id>();
                for ( ContentDocumentLink cdl : [SELECT LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId IN :cdPDFIds] ) {
                    parentIds.add( cdl.LinkedEntityId );
                }
        
                List<donation_split__Designation__c> classrooms = [ SELECT School_Pdf_File__c FROM donation_split__Designation__c WHERE Id IN :parentIds AND RecordType.DeveloperName = 'School_Registration' WITH SECURITY_ENFORCED];
                if(!classrooms.isEmpty() && Schema.sObjectType.donation_split__Designation__c.isUpdateable()) {
                    for(donation_split__Designation__c classroom : classrooms) {
                        classroom.School_Pdf_File__c = true;
                    }
                    
                    update classrooms;
                }
            }
        }
    }
}