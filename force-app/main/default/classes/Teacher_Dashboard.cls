global class Teacher_Dashboard {
	
    @AuraEnabled
    global static donation_split__Designation__c getClassroomData(){
        User usr = [SELECT Id, ContactId, Contact.Name FROM User WHERE Id = :UserInfo.getUserId()];

        donation_split__Designation__c classroom = [SELECT  Id, 
                                                    		Teacher__r.Id,
                                                    		Use_Donations_this_Year_Instead__c, 
                                                            Fundraising_Goal__c, 
                                                            Teacher__r.Total_Donation__c, 
                                                            Teacher__r.Funds_Raised_Last_18_Months__c,
                                                            teacherName__c, 
                                                            Teacher__r.Donors_This_Year__c, 
                                                            Teacher__r.Donors_Last_18_Months__c,
                                                            FundsAvailable__c,
                                                            RecordType.Name,
                                                            Public_Classroom_Landing_Page_URL__c 
                                                            FROM donation_split__Designation__c 
                                                            WHERE Teacher__c = :usr.ContactId];                                       
        return classroom;
    }
}