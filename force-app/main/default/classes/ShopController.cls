public with sharing class ShopController {

	private static final String SHOP_URL_TEMPLATE = '{0}/autoLogin/{1}/{4}/{2}{3}';
	private static final String SHA_ALGORITHM = 'hmacSHA256';
	private static final String SPENDING_ACCOUNT_NAME = 'My Spending Account';
	private static final String SCHOOL_FIELD_TEMPLATE = 'School__r.{0}';
	private static final String SLASH = '/';
  
  
	public static boolean ActiveAccount(Id userId) {
		if (String.isBlank(userId) || userId.getSobjectType() != User.SObjectType) {
			return false;
		} 
		boolean status = false;
		User u = ShopUtils.getCurrentUser(userId);
		if(u != Null){
			List<donation_split__Designation__c> c=[SELECT Id,School__c FROM donation_split__Designation__c WHERE Teacher__c=: u.contactId limit 1];
			if(c != Null && c.size()>0) {   
				string schoolstatus = [SELECT Account_Status__c FROM Account WHERE Id =: c[0].School__c].Account_Status__c;
				if(schoolstatus =='Active') {
					status = true;
				} else {
					status = false;
				}  
			}
		}
		return status;
	}

	@AuraEnabled
	public static Boolean hasActiveClassroom(String userId){
		try {
			Boolean hasClassroom = false;
			//Classroom Verification
			List<User> currentUser = [SELECT ContactId FROM User WHERE Id = :userId];
			if(!currentUser.isEmpty()) {
				List<donation_split__Designation__c> classrooms = [SELECT donation_split__Active__c FROM donation_split__Designation__c WHERE Teacher__c = :currentUser[0].ContactId];
				if(!classrooms.isEmpty()) {
					if(classrooms[0].donation_split__Active__c) {
						hasClassroom = true;
					}
				}
			}
			return hasClassroom;
		} catch (Exception ex) {
			throw new AuraHandledException(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') ? ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')[1].split(':')[0] : ex.getMessage());
		}
	}
		
	@AuraEnabled
	public static String generateShopUrl(Id userId) {
		try {
			if (String.isBlank(userId) || userId.getSobjectType() != User.SObjectType ) {
				return null;
			}

			//Classroom Verification
			List<User> currentUser = [SELECT ContactId FROM User WHERE Id = :userId];
			if(!currentUser.isEmpty()) {
				List<donation_split__Designation__c> classrooms = [SELECT donation_split__Active__c FROM donation_split__Designation__c WHERE Teacher__c = :currentUser[0].ContactId];
				if(!classrooms.isEmpty()) {
					if(!classrooms[0].donation_split__Active__c) {
						throw new CustomException('Please create your profile page before shopping. If your school is not already in our database, please allow up to three days for your school to be approved');
					}
				} else {
					throw new CustomException('Please create your profile page before shopping. If your school is not already in our database, please allow up to three days for your school to be approved');
				}
			}

			if(!ActiveAccount(userId)) {
				if(!Test.isRunningTest()) {
					return '';
				} else {
					throw new CustomException('You need an Active Account in order to proceed');
				}
			}

			Boolean userCreated = getUserCreated(userId);
			String adminAccessToken;
			String accessToken;
			String newPassword;
			User toUpdate;
			Long timestamp;
			Shop_Settings__c settings = ShopUtils.getSettings();
			if(settings == null) {
				throw new CustomException('Shop Settings are empty.');
			}
			User u = ShopUtils.getCurrentUser(userId);
			Contact c;

			adminAccessToken = ShopUtils.getAdminAuthToken();

			if (!userCreated) {
				c = (Contact) QueryUtils.queryObject(u.ContactId, getAdditionalQueryFields());
				newPassword = generateUser(adminAccessToken, userId, c);
			}

			accessToken = ShopUtils.getImpersonationAccessToken(
			adminAccessToken, userId);

			if (!userCreated) {
				toUpdate = new User( Id = userId, Shop_User_Created__c = true, Shop_Password__c = newPassword);
				update toUpdate;
				if(c != Null) {
					update c;
				}
			}

			timestamp = Datetime.now().getTime();
			return String.format(SHOP_URL_TEMPLATE, new List<String> { settings.Shop_URL__c, accessToken, String.valueOf(timestamp), generateMac(timestamp), settings.Default_Catalog_Id__c });
		} catch (Exception ex) {
			throw new AuraHandledException(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') ? ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')[1].split(':')[0] : ex.getMessage());
		}
	}

	private static String generateUser(String at, Id userId, Contact c) {
		Boolean groupSuccess;
		String spendingAccountId;
		OrderCloudSpendingAccount spendingAccount = new OrderCloudSpendingAccount();
		ShopAddress mailingAddress, otherAddress;
		User u = ShopUtils.getCurrentUser(userId);
		String pw = ShopUtils.provisionUser(at, userId);

		ShopUtils.addUserToGroup(at, userId);

		if (c == null) {
			return pw;
		}

		mailingAddress = new ShopAddress(ShopAddress.AddressType.Shipping, c);
		otherAddress = new ShopAddress(ShopAddress.AddressType.Billing, c);
		c.Mailing_Address_Id__c = ShopUtils.createAddress(at, mailingAddress);
		c.Billing_Address_Id__c = ShopUtils.createAddress(at, otherAddress);
		ShopUtils.createAddressAssignment(at, userId, mailingAddress);
		ShopUtils.createAddressAssignment(at, userId, otherAddress);
		if(mailingAddress.City != Null) {
			c.mailingCity = mailingAddress.City;
		}
		if(mailingAddress.zip != Null) {
			c.mailingPostalCode = mailingAddress.zip;
		}
		if(mailingAddress.Country != Null) {
			c.mailingCountryCode = mailingAddress.Country;
		}
		if( mailingAddress.State != Null) {
			c.mailingStateCode = mailingAddress.State;
		}
		if(mailingAddress.Street1 != Null || mailingAddress.street2 != Null) {
			String addressStr = mailingAddress.Street1 != Null ? mailingAddress.Street1 : '';
			addressStr += mailingAddress.Street2 != Null ? ' ' + mailingAddress.Street2 : '';
			c.mailingStreet = addressStr;
		}
		spendingAccount.Name = SPENDING_ACCOUNT_NAME;
		if(c.Funds_Available__c == Null) {
			spendingAccount.Balance = 0.0;
		} else {
			spendingAccount.Balance = c.Funds_Available__c;
		}
			
		spendingAccount.AllowAsPaymentMethod = true;
		spendingAccountId = ShopUtils.createSpendingAccount(at, spendingAccount);
		c.Order_Cloud_Spending_Account_Id__c = spendingAccountId;
		ShopUtils.createSpendingAccountAssignment(at, userId, spendingAccountId);

		return pw;
	}

	public static donation_split__Designation__c getClassroomForContact(
		Id contactId) {
		try {
		return [SELECT Id, Funds_Available__c, Order_Cloud_Spending_Account_Id__c
				FROM donation_split__Designation__c
				WHERE Teacher__c = :contactId and Teacher__c != null];
		} catch (Exception e) {
		return null;
		}
	}

	private static Set<String> getAdditionalQueryFields() {
		List<String> accountFields = QueryUtils.getFieldListByObjectType(
		Account.SObjectType);
		Set<String> returnFields = new Set<String>();

		for (String f : accountFields) {
			returnFields.add(String.format(SCHOOL_FIELD_TEMPLATE,
			new List<String> { f }));
		}

		return returnFields;
	}

	private static Boolean getUserCreated(Id userId) {
		User u = ShopUtils.getCurrentUser(userId);
		return u.Shop_User_Created__c;
	}
	
	public static String generateMac(Long timestamp) {
		Shop_Settings__c settings = ShopUtils.getSettings();

		if (!settings.Enable_Encryption__c || String.isBlank(settings.Hash_Key__c)) {
			return '';
		}
		
		Blob input = Blob.valueOf(String.valueOf(timestamp));
		Blob key = Blob.valueOf(settings.Hash_Key__c);
		Blob mac = Crypto.generateMac(SHA_ALGORITHM, input, key);
		
		return SLASH + EncodingUtil.convertToHex(mac);
	}

	public class CustomException extends Exception {}
}