public class APControlAccountBalanceBatch implements Database.Batchable<sObject>, Database.Stateful {
    private List<AcctSeed__Accounting_Period__c> periods;
    private AcctSeed__Accounting_Period__c startPeriod;
    private Id apGLAccountId;
    private Id transReportId;
    private Id apReportId;
    private String openingBalancePeriod;

    private final String query = 'SELECT Id, Name, AcctSeed__Account__c, AcctSeed__Account_Payable_Line__r.AcctSeed__Account_Payable__r.AcctSeed__Type__c, AcctSeed__Employee__c, AcctSeed__Accounting_Period__c, AcctSeed__GL_Account__c, AcctSeed__Amount__c, AcctSeed__Account_Payable_Line__c, AcctSeed__Account_Payable_Line__r.AcctSeed__Account_Payable__r.AcctSeed__Contact__c, AcctSeed__Account_Payable_Line__r.AcctSeed__Amount__c ' +
            'FROM AcctSeed__Transaction__c WHERE AcctSeed__GL_Account__c = :apGLAccountId AND AcctSeed__Accounting_Period__c IN :periods ORDER BY AcctSeed__Accounting_Period__c ASC';

    private Map<String,Control_Account_Balance__c> accountBalanceMap;

    public APControlAccountBalanceBatch(Id startPeriodId) {
        this.startPeriod = [Select Id, Name, AcctSeed__Status__c From AcctSeed__Accounting_Period__c Where Id = :startPeriodId];
        periods = [Select Id, Name, AcctSeed__Status__c From AcctSeed__Accounting_Period__c Where Name <= :startPeriod.Name];
        transReportId = getReportId('ARAP_Transaction_Report');
        apReportId = getReportId('ARAP_AP_Aging_Report');
        openingBalancePeriod = getFirstAccountingPeriod();

        apGLAccountId = [Select Id, AcctSeed__AP_Control_GL_Account__c FROM AcctSeed__Accounting_Settings__c ORDER BY CreatedDate DESC LIMIT 1].AcctSeed__AP_Control_GL_Account__c;
        accountBalanceMap = new Map<String,Control_Account_Balance__c>();
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope){

        for (AcctSeed__Transaction__c tran : (AcctSeed__Transaction__c[]) scope) {
            String key = getKey(tran);

            if (accountBalanceMap.containsKey(key)) {
                accountBalanceMap.get(key).Balance__c += (tran.AcctSeed__Amount__c);
            }
            else {
                Control_Account_Balance__c cab = new Control_Account_Balance__c();
                cab.Customer_Vendor__c = tran.AcctSeed__Account__c != NULL ? key : NULL;
                cab.Employee__c = tran.AcctSeed__Employee__c != NULL ? key : NULL;
                cab.Contact__c = tran.AcctSeed__Account_Payable_Line__r.AcctSeed__Account_Payable__r.AcctSeed__Contact__c != NULL ? key : NULL;
                cab.Balance__c = tran.AcctSeed__Amount__c;
                cab.Accounting_Period__c = startPeriod.Id;
                cab.GL_Account__c = apGLAccountId;
                cab.Aging_Amount__c = 0;
                cab.Type__c = 'Accounts Payable';
                cab.Transaction_Report_Id__c = transReportId;
                cab.Aging_Report_Id__c = apReportId;
                accountBalanceMap.put(key,cab);
            }
        }
    }

    private String getKey(AcctSeed__Transaction__c tran) {
        String key;
        /*
        if (tran.AcctSeed__Account_Payable_Line__r.AcctSeed__Account_Payable__r.AcctSeed__Type__c == 'Invoice' && tran.AcctSeed__Amount__c > 0 && tran.AcctSeed__Account_Payable_Line__r.AcctSeed__Amount__c > 0)
            || (tran.AcctSeed__Account_Payable_Line__r.AcctSeed__Account_Payable__r.AcctSeed__Type__c == 'Credit Memo' && tran.AcctSeed__Amount__c < 0 && tran.AcctSeed__Account_Payable_Line__r.AcctSeed__Amount__c < 0)) {
            return key;
        }
        */
        if (tran.AcctSeed__Account__c != NULL) {
            key = tran.AcctSeed__Account__c;
        }
        else if (tran.AcctSeed__Employee__c != NULL) {
            key = tran.AcctSeed__Employee__c;
        }
        else if (tran.AcctSeed__Account_Payable_Line__r.AcctSeed__Account_Payable__r.AcctSeed__Contact__c != NULL) {
            key = tran.AcctSeed__Account_Payable_Line__r.AcctSeed__Account_Payable__r.AcctSeed__Contact__c;
        }
        return key;
    }

    private Id getReportId(String devName) {
        Id reportId;

        try {
           reportId = [Select Id From Report Where DeveloperName = :devName].Id;
        }
        catch (Exception ex) {

        }

        return reportId;
    }

    public void finish(Database.BatchableContext BC) {

        for (AcctSeed__AP_Aging_History__c aph : [Select Id, AcctSeed__Amount__c, AcctSeed__Account_Payable__r.AcctSeed__Vendor__c, AcctSeed__Account_Payable__r.AcctSeed__Contact__c, AcctSeed__Account_Payable__r.AcctSeed__Employee__c From AcctSeed__AP_Aging_History__c Where AcctSeed__Accounting_Period__r.Name = :startPeriod.Name]) {
            String key = aph.AcctSeed__Account_Payable__r.AcctSeed__Vendor__c;

            if (accountBalanceMap.containsKey(key)) {
                accountBalanceMap.get(key).Aging_Amount__c += aph.AcctSeed__Amount__c;
            }
            else {
                Control_Account_Balance__c cab = new Control_Account_Balance__c();
                cab.Customer_Vendor__c = aph.AcctSeed__Account_Payable__r.AcctSeed__Vendor__c != NULL ? key : NULL;
                cab.Employee__c = aph.AcctSeed__Account_Payable__r.AcctSeed__Employee__c != NULL ? key : NULL;
                cab.Contact__c = aph.AcctSeed__Account_Payable__r.AcctSeed__Contact__c != NULL ? key : NULL;
                cab.Balance__c = 0;
                cab.Aging_Amount__c = aph.AcctSeed__Amount__c;
                cab.Accounting_Period__c = startPeriod.Id;
                cab.GL_Account__c = apGLAccountId;
                cab.Type__c = 'Accounts Payable';
                accountBalanceMap.put(key,cab);
            }
        }
        normalizeCurrency();
        insert accountBalanceMap.values();
    }

    private void normalizeCurrency() {
        for (Control_Account_Balance__c cab : accountBalanceMap.values()) {
            if (cab.Customer_Vendor__c != NULL || cab.Contact__c != NULL || cab.Employee__c != NULL) {
                cab.Balance__c = cab.Balance__c.setScale(2,System.RoundingMode.HALF_UP);
            }
            cab.Aging_Amount__c = cab.Aging_Amount__c.setScale(2,System.RoundingMode.HALF_UP);
        }
    }

    private String getFirstAccountingPeriod() {
        return [SELECT Id, Name FROM AcctSeed__Accounting_Period__c ORDER BY Name ASC LIMIT 1].Id;
    }
}