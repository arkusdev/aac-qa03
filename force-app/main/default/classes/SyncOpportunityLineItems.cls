global class SyncOpportunityLineItems implements Database.Batchable<Sobject>, Database.AllowsCallouts{
    
    global string accessTokenToBeUsed;
    List<AcctSeed__GL_Account__c> glAccount;
    Map<String, AcctSeed__Accounting_Variable__c> accountingvariableMap;
    List<String> accountingVariableNameList;
    global Id pbid;
    global Map<String,Opportunities_to_process__c> opportunitiesToProcess=new Map<String,Opportunities_to_process__c>();
    global set<String> missingOpps=new Set<String>();
    
    global Database.QueryLocator Start(Database.BatchableContext BC){
        Set<String> opportunitiesSet=new Set<String>();
        String oppQueryStr='select id, Order_Cloud_Order_Id__c,OrderCloudLineItemCount__c,OpportunityProductCount__c,RecordTypeId,StageName from opportunity where Order_Cloud_Order_Id__c != null AND StageName=\'Sale Closed / Paid\'';
        
        /*if(opportunitiesToProcess.keyset() != null && opportunitiesToProcess.keyset().size() >0){
            opportunitiesSet=opportunitiesToProcess.keySet();
            oppQueryStr+=' and Id IN:opportunitiesSet';
        }*/
        
        if(missingOpps != null && missingOpps.size() >0){
            opportunitiesSet=missingOpps;
            oppQueryStr+=' and Id IN:opportunitiesSet';
        }
        
        system.debug('*****************'+oppQueryStr+'-'+missingOpps);
        
        return Database.getQueryLocator(oppQueryStr);
    }
    
    
    global SyncOpportunityLineItems(){
        glAccount = [SELECT Id, Name FROM AcctSeed__GL_Account__c WHERE Name ='6000 In Network purchases'];
        accountingvariableMap = new Map<String, AcctSeed__Accounting_Variable__c>();
        accountingVariableNameList = new List<String>{'Unrestricted', 'Programs', 'Classroom Adoption'};
        opportunitiesToProcess=Opportunities_to_process__c.getAll();
        
        for (AcctSeed__Accounting_Variable__c accountingVariable : [SELECT Id, Name FROM AcctSeed__Accounting_Variable__c WHERE Name IN :accountingVariableNameList]) {
        accountingvariableMap.put(accountingVariable.Name, accountingVariable);
        }
        
        List<Opportunity_line_items__c> missingOppIds=[select id,line_Items__c from Opportunity_line_items__c where name='missing orders' limit 1];
        if(missingOppIds != null && missingOppIds.size() >0 && missingOppIds[0].line_Items__c != ''){
            for(String s:missingOppIds[0].line_Items__c.split('\n')){
                missingOpps.add(s.normalizeSpace());
            }        
        }
        system.debug('*****************'+missingOpps);
    }
    
    global void Execute(Database.BatchableContext BC,List<Opportunity> allOpportunities){
        List<Opportunity> OppToSync=new List<Opportunity>();
        Set<Id> OppIds=new Set<Id>();
        Map<String,Map<String,Id>> oppWithLineItemsExisting=new Map<String,Map<String,Id>>();        
        Map<String,List<OrderCloudLineItem>> orderIdWithLineItems=new Map<String,List<OrderCloudLineItem>>();
        Set<String> lineItemsExternalId=new Set<String>();
        List<OrderCloudLineItem> lineItemsTobeProcessed=new List<OrderCloudLineItem>();
        Shop_Settings__c settings = ShopUtils.getSettings();
        pbid = settings.Pricebook_Id__c;
        
        Map<String,Product2> productWithPriceBookEntries =new Map<String,Product2>();
        list<Pricebook2> pb = new list<Pricebook2>([select Id from Pricebook2 where id =: pbid limit 1]);
        Map<string,product2> newProductsToInsert=new Map<string,product2>();
        Map<Id,product2> oldprodlist = new Map<Id,product2>();
        Map<String,pricebookEntry> pricebookEntryMap= new Map<String,Pricebookentry>();
        Map<String,String> lineItemWithOpps=new Map<String,String>();
        Map<String,Id> oppOrderIDWithOppId=new Map<String,Id>();
        
        
        for(Opportunity o:allOpportunities){
            if(o.OrderCloudLineItemCount__c != o.OpportunityProductCount__c){
                OppToSync.add(o);
                OppIds.add(o.Id);
                oppOrderIDWithOppId.put(o.Order_Cloud_Order_Id__c,o.Id);
            }
        }
        
        system.debug('***********************'+OppIds);
        system.debug('***********************'+oppOrderIDWithOppId);
        if(OppIds.size() >0){
            for(opportunityLineItem l:[select id,PricebookEntryId ,PricebookEntry.Product2Id,PricebookEntry.Product2.Order_Cloud_Product_Id__c, OpportunityId, Opportunity.Order_Cloud_Order_Id__c, Quantity ,UnitPrice ,VendorOrderId__c  from OpportunityLineItem where OpportunityId IN:OppIds]){
                if(!oppWithLineItemsExisting.containsKey(l.Opportunity.Order_Cloud_Order_Id__c)){
                    oppWithLineItemsExisting.put(l.Opportunity.Order_Cloud_Order_Id__c,new Map<String,Id>());
                }
                oppWithLineItemsExisting.get(l.Opportunity.Order_Cloud_Order_Id__c).put(l.PricebookEntry.Product2.Order_Cloud_Product_Id__c,l.Id);
            }
        }
        
        system.debug('***********************'+oppWithLineItemsExisting);
        
        if(accessTokenToBeUsed == null){
            accessTokenToBeUsed=ShopUtils.getAdminAuthToken();
        }
        
        system.debug('***********************'+accessTokenToBeUsed);
        
        if(accessTokenToBeUsed != null && OppToSync.size() >0){
            for(Opportunity o:OppToSync){
                orderIdWithLineItems.put(o.Order_Cloud_Order_Id__c,ShopUtils.getLineItemsForOrder(accessTokenToBeUsed,o.Order_Cloud_Order_Id__c));            
            }
            
            system.debug('***********************'+orderIdWithLineItems);
            
            if(orderIdWithLineItems.keyset() != null && orderIdWithLineItems.keyset().size() >0){
                for(String oId:orderIdWithLineItems.keyset()){
                    if(orderIdWithLineItems.get(oId) != null){
                        system.debug('***********************'+orderIdWithLineItems.get(oId));
                        for(OrderCloudLineItem li:orderIdWithLineItems.get(oId)){
                            system.debug('***********************'+li);
                            if(li.ProductID == 'AACPunchoutProduct' && li.xp != null && li.xp.SupplierPartID != null && (!oppWithLineItemsExisting.containsKey(oId) || (oppWithLineItemsExisting.get(oId) != null && !oppWithLineItemsExisting.get(oId).containsKey(li.xp.SupplierPartID))) ){
                                lineItemsExternalId.add(li.xp.SupplierPartID);
                                lineItemsTobeProcessed.add(li);
                                lineItemWithOpps.put(li.xp.SupplierPartID,oppOrderIDWithOppId.get(oId));
                                system.debug('***********************');
                            }
                            else if(li.product.ID != null && (!oppWithLineItemsExisting.containsKey(oId) || (oppWithLineItemsExisting.get(oId) != null && !oppWithLineItemsExisting.get(oId).containsKey(li.product.ID)))){
                                lineItemsExternalId.add(li.product.ID);
                                lineItemsTobeProcessed.add(li);
                                lineItemWithOpps.put(li.product.ID,oppOrderIDWithOppId.get(oId));
                                system.debug('***********************');
                            }
                        }
                    }
                }
                
                system.debug('***********************'+lineItemsExternalId);
                if(lineItemsExternalId.size() >0){
                    for(product2 p:[select Id,Name, Order_Cloud_Product_Id__c, (select Id,Pricebook2Id from PricebookEntries where Pricebook2Id = :settings.Pricebook_Id__c) from Product2 where Order_Cloud_Product_Id__c IN: lineItemsExternalId]){
                        productWithPriceBookEntries.put(p.Order_Cloud_Product_Id__c,p);
                    }
                }
                
                system.debug('***********************'+productWithPriceBookEntries);
                
                if(!lineItemsTobeProcessed.isEmpty()){
                    system.debug('***********************'+lineItemsTobeProcessed);
                    for(OrderCloudLineItem lineItem1:lineItemsTobeProcessed){
                        Product2 p;                        
                        if(lineItem1.ProductID == 'AACPunchoutProduct' && lineitem1.xp != null && lineitem1.xp.SupplierPartID != null && productWithPriceBookEntries.containsKey(lineitem1.xp.SupplierPartID)){
                            p = productWithPriceBookEntries.get(lineitem1.xp.SupplierPartID);
                        }
                        else if(productWithPriceBookEntries.containsKey(lineItem1.Product.ID)){
                            p = productWithPriceBookEntries.get(lineItem1.Product.ID);
                        }
                        
                        system.debug('***********************'+p);
                        system.debug('***********************'+lineItem1);
                        
                        if (p == null && (lineitem1.xp == null || (lineitem1.xp != null && lineitem1.xp.SupplierPartID != null && !newProductsToInsert.containsKey(lineitem1.xp.SupplierPartID))) && (lineItem1.Product.ID != null && !newProductsToInsert.containsKey(lineItem1.Product.ID))){                            
                            system.debug('***********************');
                            Product2 newProduct = new Product2();
                            if(lineitem1.productID == 'AACPunchoutProduct' && lineitem1.xp != null && lineitem1.xp.SupplierPartID != null){        
                                newProduct.Name = lineitem1.xp.Description;
                                newProduct.Order_Cloud_Product_Id__c = lineitem1.xp.SupplierPartID;
                                newProduct.Description = lineitem1.xp.Description;
                                newProduct.Vendor_Name__c = lineitem1.xp.PunchoutName;
                                newProduct.ProductCode = lineitem1.xp.SupplierPartAuxiliaryID;
                                newProduct.PunchoutProduct__c = True;  
                                newProduct.IsActive = true;   
                                system.debug('***********************');                                           
                            }
                            else{
                                newProduct.Name = lineitem1.product.Name;
                                newProduct.Order_Cloud_Product_Id__c = lineitem1.product.ID;
                                newProduct.Description = lineitem1.product.Description;
                                newProduct.Vendor_Name__c = lineitem1.product.xp.VendorName;
                                newProduct.ProductCode = lineitem1.product.xp.SecondaryItemNo;
                                newProduct.PunchoutProduct__c = false;
                                newProduct.IsActive = true;
                            }
                            newProduct.AcctSeed__Expense_GL_Account__c = glAccount[0].Id;
                            newProduct.AcctSeed__GL_Account_Variable_1__c = accountingvariableMap.get('Unrestricted').Id;
                            newProduct.AcctSeed__GL_Account_Variable_2__c = accountingvariableMap.get('Programs').Id;
                            newProduct.AcctSeed__GL_Account_Variable_3__c = accountingvariableMap.get('Classroom Adoption').Id;
                            newProduct.AcctSeed__Inventory_Type__c = 'Raw Material';
                            newProductsToInsert.put(newProduct.Order_Cloud_Product_Id__c,newProduct);
                            system.debug('***********************'+newProduct);                            
                         }
                         else if(p != null){
                             system.debug('***********************'+p);
                             oldprodlist.put(p.Id,p);
                         }    
                    }         
                    
                    system.debug('***********************'+newProductsToInsert);
                    
                    if(newProductsToInsert.keyset() != null && newProductsToInsert.keyset().size() > 0){
                        list<product2> plist = new list<product2>();
                        plist.addall(newProductsToInsert.values());
                        database.Insert(plist,false);  
                        
                        system.debug('***********************'+plist);
                        
                        for(Product2 pd : plist){
                            if(pd.Id != null){
                                oldprodlist.put(pd.Id,pd);
                            }
                        }
                        
                        system.debug('***********************'+oldprodlist);
                        
                        list<PricebookEntry> pbelist = new list<PricebookEntry>();
                        if(plist.size() > 0){       
                            for(product2 p : plist){
                                pricebookEntry pbe = new pricebookEntry();
                                pbe.Product2Id = p.Id;
                                pbe.Pricebook2Id = pb[0].Id;
                                pbe.UnitPrice = 0;
                                pbe.IsActive = true;
                                pbelist.add(pbe);
                            }
                        }
                        if(pbelist.size() > 0){
                            database.Insert(pbelist,false);      
                        }      
                        system.debug('***********************'+pbelist);
                    }
                    
                    if(oldprodlist.size() >0){
                        //oldpbelist = [select Id,Product2Id,pricebook2Id,unitprice,IsActive,product2.Order_Cloud_Product_Id__c,product2.Name from PricebookEntry where Product2Id IN: oldprodlist.keyset()];
                        for(pricebookentry pbookentry:[select Id,Product2Id,pricebook2Id,unitprice,IsActive,product2.Order_Cloud_Product_Id__c,product2.Name from PricebookEntry where Product2Id IN: oldprodlist.keyset()]){
                            pricebookEntryMap.put(pbookentry.product2.Order_Cloud_Product_Id__c,pbookentry);
                        }
                    }
                    
                    set<OpportunityLineitem> olist = new set<OpportunityLineitem>();
   
                    for(ordercloudLineItem lineitem1: lineItemsTobeProcessed){
                        system.debug('***********************'+lineitem1);
                        if(lineitem1.productID == 'AACPunchoutProduct' && lineitem1.xp != null && lineitem1.xp.SupplierPartID != null && lineitem1.xp.vendorOrderId != null && pricebookEntryMap.containsKey(lineitem1.xp.SupplierPartID) && lineItemWithOpps.containsKey(lineitem1.xp.SupplierPartID)){ 
                            OpportunityLineItem oli = new OpportunityLineItem(
                            PricebookEntryId = pricebookEntryMap.get(lineitem1.xp.SupplierPartID).Id,
                            OpportunityId = lineItemWithOpps.containsKey(lineitem1.xp.SupplierPartID) ? lineItemWithOpps.get(lineitem1.xp.SupplierPartID) : null,
                            Quantity = lineitem1.Quantity,
                            //UnitPrice = lineitem1.UnitPrice,
                            UnitPrice = (lineitem1.UnitPrice != null)?lineitem1.UnitPrice:0, 
                            VendorOrderId__c = lineitem1.xp.vendorOrderId.get(0));
                            olist.add(oli);
                            system.debug('***********************');
                        }
                        else if(lineitem1.product.ID != null && pricebookEntryMap.containsKey(lineitem1.product.ID) && lineitem1.xp != null && lineitem1.xp.vendorOrderId != null && lineItemWithOpps.containsKey(lineitem1.product.ID)){
                            OpportunityLineItem oli = new OpportunityLineItem(
                            PricebookEntryId = pricebookEntryMap.get(lineitem1.product.ID).Id,
                            OpportunityId = lineItemWithOpps.containsKey(lineitem1.product.ID) ? lineItemWithOpps.get(lineitem1.product.ID) : null,
                            Quantity = lineitem1.Quantity,
                            //UnitPrice = lineitem1.UnitPrice,
                            UnitPrice = (lineitem1.UnitPrice != null)?lineitem1.UnitPrice:0,    
                            VendorOrderId__c = lineitem1.xp.vendorOrderId.get(0));
                            olist.add(oli);
                            system.debug('***********************');
                        }                     
                    }
                    
                    if(olist.size() > 0){
                        system.debug('***********************'+olist);
                        //Savepoint sp = Database.setSavepoint();                        
                            list<Opportunitylineitem> olilist = new list<OpportunityLineitem>();
                            olilist.addall(olist);
                            //Database.saveResult[] results=database.Insert(olilist,false);                            
                            //system.debug('**************'+results);
                            insert olilist;
                        //Database.rollback(sp);
                    }
                }
            }
        }
        
    }
    
    global void finish(Database.BatchableContext BC){
    
    }
    
    global void testDummyMethod(){
           string newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';    
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';     
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';
           newStr='test';                      
    }
}