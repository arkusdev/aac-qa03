global without sharing class FindAClassroom2 {
    
    public Map<String, Map<String, Boolean>> filterMaps {get; set;}
    public Map<String, String> labelAPINameMap {get; set;}
    
    public void readFieldSet(String fieldSetName, String fieldNamefilter, String ObjectName){
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        for(Schema.FieldSetMember f : fieldSetObj.getFields()){
            filterMaps.put(f.getSObjectField().getDescribe().getLabel(), populateMap(f.getSObjectField().getDescribe().getName(), ObjectName));
            labelAPINameMap.put(f.getSObjectField().getDescribe().getLabel(), fieldNamefilter);
        }
    }
    
    public Map<String, Boolean> populateMap(String fieldName, String ObjectName){
        Map<String, Boolean> mapWithValues = new Map<String, Boolean>();
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.SObjectField fieldObj = DescribeSObjectResultObj.Fields.getMap().get(fieldName);
        Schema.DescribeFieldResult fieldResult = fieldObj.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for(Schema.PicklistEntry p : ple){
            mapWithValues.put(p.getLabel(), true);
        }
        
        return mapWithValues;
    }

    public List<Schema.PicklistEntry> getStates(){
        List<String> states = new List<String>();
        Schema.DescribeFieldResult fieldResult = Account.BillingStateCode.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        return ple;
    }
    
    public FindAClassroom2(){
        filterMaps = new Map<String, Map<String, Boolean>>();
        labelAPINameMap = new Map<String, String>();
        readFieldSet('FindAClassroomFilters', 'Teaching_Area__c = ', 'donation_split__Designation__c');
        readFieldSet('FindAClassroomFilters', 'School__r.Level__c = ', 'Account');
    }
    
    @RemoteAction
    global static List<Donation> doSearch(String teacherName, 
                                        String schoolName, 
                                        String schoolCity, 
                                        String schoolState,
                                        String schoolZipCode,
                                        String additionalFilters, 
                                        Boolean defaultSuggestion) {
        
        String query = 'SELECT Name,'
                            + 'donation_split__Active__c,'
                            + 'Teacher__c,'
                            + 'Teacher__r.Name,'
                            + 'School_Name__c,'
                            + 'School_City__c,'
                            + 'School_State__c,'
                            + 'School_Zip__c,'
                            + 'Description__c,'
                            + 'Fundraising_Goal__c,'
                            + 'SchoolLevel__c,'
                            + 'Teaching_Area__c,'
                            + 'RecordType.DeveloperName,'
                            + 'How_will_you_spend_your_funds__c,'
                            + 'Public_Classroom_Landing_Page_URL__c, '
                            + 'Inactive_Classroom_Page__c '
                        + 'FROM donation_split__Designation__c '
                        + 'WHERE Teacher__r.Name != null '
                        + 'AND ((RecordType.DeveloperName = \'School_Registration\' AND School_Status__c=\'Active\') '
                        + 'OR RecordType.DeveloperName = \'Classroom_Registration\') '
                        + 'AND Hide_My_Page_from_Search_Results_Online__c = false '
                        + 'AND donation_split__Active__c = true';

        if(!defaultSuggestion){
            List<String> filterList = new List<String>();
            
            List<ClassroomSearchSynonym__mdt> syns = [ SELECT Label, Term__c, Location__c FROM ClassroomSearchSynonym__mdt ];
            
            if (!String.isBlank(teacherName)){
                filterList.add(' ((Teacher__r.Name = \'' + teacherName + '\' ) OR (Teacher__r.Name LIKE \'%' + teacherName + '%\')) ');
            }
            
            if (!String.isBlank(schoolName)){
                List<String> filterListSchoolName = new List<String>();
                filterListSchoolName.add(' School_Name__c = \'' + schoolName + '\' ');
                filterListSchoolName.add(' School_Name__c = \'' + schoolName.replaceAll('[^a-zA-Z0-9 ]', '') + '\' ');
                filterListSchoolName.add(' School_Name__c LIKE \'%' + schoolName + '%\' ');
                String type = '';
                String found = '';
                for(ClassroomSearchSynonym__mdt sy : syns){
                    if((!sy.Location__c) && (schoolName.containsIgnoreCase(sy.Label))){
                        type = sy.Term__c;
                        found = sy.Label;
                        break;
                    }
                }
                
                for(ClassroomSearchSynonym__mdt sy : syns){
                    if((sy.Term__c == type) && !(sy.Label.equalsIgnoreCase(found))){
                        filterListSchoolName.add(' School_Name__c = \'' + schoolName.tolowerCase().replace(found.tolowerCase(), sy.Label) + '\' ');
                    }
                }
                filterList.add(' ( ' + String.join(filterListSchoolName, 'OR') + ' ) ');
                
            }
            if (!String.isBlank(schoolCity)){
                List<String> filterListSchoolCity = new List<String>();
                filterListSchoolCity.add(' School_City__c = \'' + schoolCity + '\' ');
                filterListSchoolCity.add(' School_City__c = \'' + schoolCity.replaceAll('[^a-zA-Z0-9 ]', '') + '\' ');
                String type = '';
                String found = '';
                for(ClassroomSearchSynonym__mdt sy : syns){
                    if((sy.Location__c) && (schoolCity.containsIgnoreCase(sy.Label))){
                        type = sy.Term__c;
                        found = sy.Label;
                        break;
                    }
                }
                
                for(ClassroomSearchSynonym__mdt sy : syns){
                    if((sy.Term__c == type) && !(sy.Label.equalsIgnoreCase(found))){
                        filterListSchoolCity.add(' School_City__c = \'' + schoolCity.tolowerCase().replace(found.tolowerCase(), sy.Label) + '\' ');
                    }
                }
                filterList.add(' ( ' + String.join(filterListSchoolCity, 'OR') + ' ) ');
            }
            
            if (!String.isBlank(schoolState)){
                filterList.add(' School_State__c = \'' + schoolState + '\' ');
            }
            
            if (!String.isBlank(schoolZipCode)){
                filterList.add(' (School_Zip__c = \'' + schoolZipCode + '\' )');
            }
            
            if (!filterList.isEmpty()) {
                String filterString = String.join(filterList, 'AND');
                query += ' AND (' + filterString + ' ) ';
            }
            
            if(additionalFilters != ''){
                query += additionalFilters.replaceFirst(' OR', ' AND ( ') + ' ) ';
            }
        }else{
            String stateCondition = '';
            if (!String.isBlank(schoolState)){
                stateCondition = query.contains(' WHERE ') ? ' AND ' : ' WHERE ';
                stateCondition += ' School_State__c = \'' + schoolState + '\' ';
            }
            query += stateCondition + ' ORDER BY LastModifiedDate DESC LIMIT 50';
        }
        
        String query2 = '';
        
        if(!String.isBlank(schoolZipCode)){
            query2 = query.replace('(School_Zip__c = \'' + schoolZipCode + '\' ) ', ' ((School_Zip__c <> \'' + schoolZipCode + '\' ) AND (School_Zip__c LIKE \'' + schoolZipCode.substring(0, 3) + '%\' ))');
        }

        List<Donation> donationList = new List<Donation>();
        List<Id> contactIds = new List<Id>();
        
        List<donation_split__Designation__c> queryResults = (List<donation_split__Designation__c>)Database.query(query);
        
        if((query != query2) && (query2 != '')){
            queryResults.addAll((List<donation_split__Designation__c>)Database.query(query2));
        }
        
        for (donation_split__Designation__c designation : queryResults) {
            
            Donation donation = new Donation();
            designation.Description__c = (designation.Description__c).replaceAll('<[^>]+>',' ');
            donation.record = designation;
            
            donationList.add(donation);
            
            contactIds.add(designation.Teacher__c);
        }

        Map<Id, String> contactPhotoMap = new Map<Id, String>();
        for (User user : [select ContactId, SmallPhotoUrl from User where ContactId IN: contactIds]) 
            contactPhotoMap.put(user.ContactId, user.SmallPhotoUrl);

        for (Donation donation : donationList)
            donation.photo = contactPhotoMap.get(donation.record.Teacher__c);

        donationList.sort();

        return donationList;
    }

    global class Donation implements Comparable {
        public donation_split__Designation__c   record  { get; set; }
        public String                           photo   { get; set; }

        public Integer compareTo(Object objToCompare) {
            Donation emp = (Donation)objToCompare;
            if (record.Inactive_Classroom_Page__c == emp.record.Inactive_Classroom_Page__c){
                return 0;
            }
            else if (record.Inactive_Classroom_Page__c == false && emp.record.Inactive_Classroom_Page__c == true){
                return -1;
            }
            else{
                return 1;        
            }
        }
    }
}