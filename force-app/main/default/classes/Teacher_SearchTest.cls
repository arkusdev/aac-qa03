@isTest
public with sharing class Teacher_SearchTest {
    private static final Id SCHOOL_RECORD_TYPE = [SELECT Id FROM RecordType WHERE Name = 'School' LIMIT 1].Id;

    @TestSetup
    static void createData(){
        Account acc = new Account(
            Name = 'Test',
            RecordTypeId = SCHOOL_RECORD_TYPE,
            ShippingCity = 'Test City',
            ShippingCountry = 'US',
            ShippingState = 'Alabama',
            ShippingPostalCode = '12345',
            ShippingStreet = '123 Test address',
            Account_Status__c = 'Active'
        );

        insert acc;

        
        Contact newContact = new Contact(
            LastName = 'Testing Contact',
            School__c = acc.Id
            );
            
            insert newContact;
        String profileId = [SELECT ID FROM Profile WHERE Name = 'AAC - Teacher Community Plus Login User'].Id;
        User testUser = new User(
            FirstName = 'User Test',
            LastName  = 'User Test',
            Email     = 'UserTest@UserTest.com',
            Username  = 'UserTest@UserTest.com',
            Alias     = 'fatty',
            ProfileId = profileId,
            UserPreferencesShowProfilePicToGuestUsers = true,
            ContactId = newContact.Id,
            TimeZoneSidKey    = 'America/Denver',
            LocaleSidKey      = 'en_US',
            EmailEncodingKey  = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        
        insert testUser;
        donation_split__Designation__c classRoom = new donation_split__Designation__c(
            Name = 'Test Class',
            School__c = acc.Id,
            Teacher__c = newContact.Id,
            Description__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.',
            How_will_you_spend_your_funds__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.',
            Why_We_Need_Your_Help__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.'
		
        );

        insert classRoom;
    }

    @IsTest
    static void searchSchoolTest(){
        User usTest = getUserTest();
        Teacher_Search.ParametersWrapper parameter = new Teacher_Search.ParametersWrapper();
        parameter.city = 'Test City';
        parameter.name = 'Test';
        parameter.state = 'Alabama';
        parameter.zip = '12345';

        String param = JSON.serialize(parameter);
        List<Teacher_Search.Data> results;
        Test.startTest();
        System.runAs(usTest) {
            results = Teacher_Search.searchSchool(param);
        }
        Test.stopTest();

        System.assertEquals(1, results.size(), 'Only one school should have been returned.');
        System.assertEquals(parameter.name, results[0].schoolName, 'The data does not match.');
    }
    @IsTest
    static void getClassroomTest(){
        User usTest = getUserTest();
        Account acc = [SELECT Id, Name FROM Account LIMIT 1];

        donation_split__Designation__c classRoom = new donation_split__Designation__c(
            Name = 'Test Class',
            School__c = acc.Id,
            Teacher__c = usTest.ContactId
        );
        classRoom.Description__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		classRoom.How_will_you_spend_your_funds__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		classRoom.Why_We_Need_Your_Help__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		

        insert classRoom;
        Teacher_Search.Data results;
        Test.startTest();
        System.runAs(usTest) {
            results = Teacher_Search.getClassroom();
        }
        Test.stopTest();

        System.assertEquals(acc.Name, results.schoolName, 'The data does not match.');
    }

    @IsTest
    static void saveSchoolTest(){
        User usTest = getUserTest();
        donation_split__Designation__c classRoom = [SELECT Id FROM donation_split__Designation__c LIMIT 1];
        Account acc = new Account(
            Name = 'Test 2',
            RecordTypeId = SCHOOL_RECORD_TYPE,
            ShippingCity = 'Test City',
            ShippingCountry = 'US',
            ShippingState = 'Alabama',
            ShippingPostalCode = '12345',
            ShippingStreet = '123 Test address'
        );

        insert acc;
        donation_split__Designation__c result;

        Test.startTest();
        System.runAs(usTest) {
            Teacher_Search.saveSchool(acc.Id, classRoom.Id);
            result = [SELECT Id,School__c FROM donation_split__Designation__c LIMIT 1];
        }
        Test.stopTest();

        System.assertEquals(acc.Id, result.School__c, 'The data does not match.');
    }

    @IsTest
    static void getRecordTypeTest(){
        User usTest = getUserTest();

        String result;

        Test.startTest();
        System.runAs(usTest) {
            result = Teacher_Search.getRecordType();
        }
        Test.stopTest();

        System.assertEquals(SCHOOL_RECORD_TYPE, result, 'The record type id does not match.');
    }

    private static User getUserTest(){
        return [SELECT Id,ContactId FROM User WHERE username = 'UserTest@UserTest.com' LIMIT 1];

    }
}