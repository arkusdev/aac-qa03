public with sharing class CreateClassroomController {

  @AuraEnabled
  public static donation_split__Designation__c updateClassroom(donation_split__Designation__c c) {
    if (c == null) {
      return null;
    }
    upsert c;
    return c;
  }

  @AuraEnabled
  public static Account updateSchool(Account s) {
    System.debug(s);
    if (s == null) {
      return null;
    }
    upsert s;
    return s;
  }

  @AuraEnabled
  public static donation_split__Designation__c getContactFromUser(Id userId,
    donation_split__Designation__c classRoom) {

    if (String.isBlank(userId)) {
      return null;
    }

    User u = [select Id, FirstName, LastName, ContactId
              from User
              where Id = :userId];

    classRoom.Name = u.FirstName + ' ' + u.LastName;
    classRoom.Teacher__c = u.ContactId;

    return classRoom;
  }

  @AuraEnabled
  public static Id getRecordType(String devName) {
    try {
      Id schoolType = [select Id, DeveloperName from RecordType
        where DeveloperName = :devName limit 1].id;
      return schoolType;
    } catch(Exception e){
      //System.debug(e);
      return null;
    }
  }
  
}