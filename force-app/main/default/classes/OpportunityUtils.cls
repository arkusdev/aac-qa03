public class OpportunityUtils {
	
    public static void CreateCommunityOpportunity() {
        Map<Id, RecordType> rtMap = new Map<Id, RecordType>([select Id
                                                              from RecordType 
                                                              where SObjectType = 'Opportunity' 
                                                              and DeveloperName IN ('adjustment', 'Disbursement', 'schooldonation', 'adoption', 'Orders')]);
        
        List<Community_Opportunity__c> commOppList = new List<Community_Opportunity__c>();
        for (Opportunity opp : (List<Opportunity>) trigger.new) {
            if (rtMap.containsKey(opp.RecordTypeId)) {
                Community_Opportunity__c commOpp = new Community_Opportunity__c();
                commOpp.Opportunity__c = opp.Id;
                commOppList.add(commOpp);
            }
        }
        if (!commOppList.isEmpty())
	        insert commOppList;
    }
    
}