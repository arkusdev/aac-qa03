@IsTest
private class FindAClassroom2Test {
    
    static testMethod void FindAClassroom2TestCR() {
        
        Contact contact     = new Contact();
        contact.LastName    = 'Test';
        insert contact;

        Account account             = new Account();
        account.Name                = 'TEST';
        account.BillingStreet       = '123 st';
        account.BillingState        = 'Texas';
        account.BillingCountry      = 'US';
        account.BillingPostalCode   = '1234';
        account.BillingCity         = 'Houston';
        account.Account_Status__c   = 'Active';
        insert account;

        donation_split__Designation__c donation             = new donation_split__Designation__c();
        donation.How_will_you_spend_your_funds__c           = 'TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST'
                                                            + '-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST';
        donation.Description__c                             = 'TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST'
                                                            + '-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST'
                                                            + '-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST'
                                                            + '-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST'
                                                            + '-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST';
        donation.Teacher__c                                 = contact.Id;
        donation.School__c                                  = account.Id;
        donation.Grades__c                                  = 'Pre-K;Kindergarten';
        donation.Teaching_Area__c                           = 'Art';
        donation.Hide_My_Page_from_Search_Results_Online__c = false;
        donation.RecordTypeId                               = [select Id 
                                                                from RecordType 
                                                                where SObjectType = 'donation_split__Designation__c' 
                                                                and DeveloperName = 'Classroom_Registration'].Id;
        insert donation;

        Test.setFixedSearchResults(new List<Id> {donation.Id});

        Test.startTest();
            FindAClassroom2 f = new FindAClassroom2();
            f.getStates();
            List<FindAClassroom2.Donation> donationList = FindAClassroom2.doSearch('Test','hs','st','California','11111','AND ((Name LIKE \'Test\')', false);
        Test.stopTest();

        system.assertEquals(0, donationList.size());
        //system.assertEquals(donation.Id, donationList[0].record.Id);
    }

    static testMethod void FindAClassroom2TestSR() {
        
        Contact contact     = new Contact();
        contact.LastName    = 'Test';
        insert contact;

        Account account             = new Account();
        account.Name                = 'TEST';
        account.BillingStreet       = '123 st';
        account.BillingState        = 'Texas';
        account.BillingCountry      = 'US';
        account.BillingPostalCode   = '1234';
        account.BillingCity         = 'Houston';
        account.Account_Status__c   = 'Active';
        insert account;

        donation_split__Designation__c donation             = new donation_split__Designation__c();
        donation.How_will_you_spend_your_funds__c           = 'TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST'
                                                            + '-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST';
        donation.Description__c                             = 'TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST'
                                                            + '-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST'
                                                            + '-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST'
                                                            + '-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST'
                                                            + '-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST-TEST';
        donation.Teacher__c                                 = contact.Id;
        donation.School__c                                  = account.Id;
        donation.Grades__c                                  = 'Pre-K;Kindergarten';
        donation.Teaching_Area__c                           = 'Art';
        donation.Hide_My_Page_from_Search_Results_Online__c = false;
        donation.School_Status__c                           = 'Active';
        donation.donation_split__Active__c                  = true;
        donation.RecordTypeId                               = [select Id 
                                                                from RecordType 
                                                                where SObjectType = 'donation_split__Designation__c' 
                                                                and DeveloperName = 'School_Registration'].Id;
        insert donation;

        Test.setFixedSearchResults(new List<Id> {donation.Id});

        Test.startTest();
            List<FindAClassroom2.Donation> donationList = FindAClassroom2.doSearch('Test','','','','','', false);
        Test.stopTest();

        system.assertEquals(1, donationList.size());
        system.assertEquals(donation.Id, donationList[0].record.Id);
    }
    
}