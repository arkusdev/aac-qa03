public with sharing class SocialSharingModalController {
    @AuraEnabled
    public static donation_split__Designation__c getClassroom(String objectId){
        try {
            donation_split__Designation__c classroom = [SELECT Teacher__r.Name, School__r.Name, Public_Classroom_Landing_Page_URL__c FROM donation_split__Designation__c WHERE Id = :objectId];
            return classroom;
        } catch (QueryException qe) {
            System.debug(qe);
            throw new QueryException(qe.getMessage());
        }
    }
}