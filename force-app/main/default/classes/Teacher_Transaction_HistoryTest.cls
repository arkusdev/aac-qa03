@isTest
private class Teacher_Transaction_HistoryTest {
    @TestSetup
    static void makeData(){

        String schoolRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('School Admin').getRecordTypeId();
        String accountRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household').getRecordTypeId();
      
        Account testAccount = new Account(name = 'testAccount', RecordTypeId= accountRT);
        insert testAccount;
                
        
        AcctSeed__GL_Account__c newAcctSeed = new AcctSeed__GL_Account__c(
            Name = '4599 Teacher Restricted Released',
            AcctSeed__Type__c = 'Revenue'
        );
        insert newAcctSeed;
        
        Contact con = new Contact();
        con.LastName = 'Unit Test';
        con.RecordTypeId = schoolRT;
        con.AccountId = testAccount.Id;
        insert con;

        String profileId = [SELECT ID FROM Profile WHERE Name = 'AAC - Teacher Community Plus Login User'].Id;
        User portalUser = new User();
       
        portalUser.FirstName = 'Unit Test';
        portalUser.LastName  = 'Unit Test';
        portalUser.Email     = 'UnitTest@UnitTestaac.com';
        portalUser.Username  = 'UnitTest@UnitTestaac.com';
        portalUser.Alias     = 'UT';
        portalUser.ProfileId = profileId;
        portalUser.UserPreferencesShowProfilePicToGuestUsers = true;
        portalUser.ContactId = con.Id;
        portalUser.TimeZoneSidKey    = 'America/Denver';
        portalUser.LocaleSidKey      = 'en_US';
        portalUser.EmailEncodingKey  = 'UTF-8';
        portalUser.LanguageLocaleKey = 'en_US';
        
        insert portalUser;

        String transactionRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('School Donation').getRecordTypeId();

        date myDate = date.newInstance(2021, 4, 4);
        List<Opportunity> opportunities= new List<Opportunity>();
        
        for (Integer i = 0; i < 4; i++) {
            Opportunity opp= new Opportunity();
            opp.Name= 'Test Trasaction';
            opp.RecordTypeId= transactionRT;
            opp.StageName= 'Sale Closed / Paid';
            opp.Amount= 10;
            opp.AccountId= testAccount.Id;
            opp.Donor_comment__c= 'Test donation';
            opp.Teacher__c= con.Id;
            opp.CloseDate= myDate;
            opp.Hide_Transaction_on_Profile__c= False;
            opportunities.add(opp);
        }
        insert opportunities;
    }

    @isTest static void testGetTransactions() {
        List<User> users = [SELECT Id FROM User WHERE username = 'UnitTest@UnitTestaac.com'];

        List<Contact> con = [SELECT Id FROM Contact WHERE lastname = 'Unit Test'];
        
        List<Opportunity> opp= new List<Opportunity>();

        Test.startTest();
        system.runAs(users[0]) {
            opp = Teacher_Transaction_History.getTransactions(con[0].Id);
        }
        Test.stopTest();

        List<Opportunity> result = [SELECT Id, Donor_Name__c, Amount, Donor_Comment__c, CloseDate FROM Opportunity WHERE Teacher__c =: con[0].Id AND RecordType.DeveloperName IN ('adoption', 'Disbursement', 'schooldonation') AND StageName = 'Sale Closed / Paid' AND CloseDate = LAST_N_DAYS:548 AND (NOT Name  LIKE '%Teacher Credit Card%') AND Hide_Transaction_on_Profile__c = FALSE ORDER BY CloseDate DESC];

        System.assertEquals(result, opp, 'It should be the same');
    }
}