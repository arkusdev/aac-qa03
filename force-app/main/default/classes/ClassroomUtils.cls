/**
 * @description Utility class of ClassroomLandingPageController, Unit Test: ClassroomLandingPageControllerTest
 */
public without sharing class ClassroomUtils {
    
    public static ClassroomLandingPageController.Data getData(Id recordId) {
        donation_split__Designation__c classroom = [SELECT Name, Inactive_Classroom_Page__c, Teacher__c, Description__c, TotalFundsRaised__c, Video_URL_ID__c, User_Id__c, School_Name_for_Fundraising_Page__c,
													School_Video_URL__c, Number_of_Students__c, CommunityColorTheme__c, School_Name__c, School_State__c, RecordType.Name,
													School_City__c, School_Street__c, School_Zip__c, Teaching_Area__c, SchoolLevel__c, HighestNeedsClassroom__c, 
													How_will_you_spend_your_funds__c, FirstYearTeaching__c, Fundraising_Goal__c, Total_Funds_Raised__c, SchoolName__c, Title_1__c, Use_Donations_this_Year_Instead__c, teacherName__c, Why_We_Need_Your_Help__c,
                                                    School__r.Account_Status__c
													FROM donation_split__Designation__c
													WHERE Id =:recordId][0];
		
		User teacher = [SELECT ContactId, Contact.Email, Contact.Name, Contact.FirstName, Contact.LastName, Contact.Funds_Raised_Last_18_Months__c, Contact.Donors_Last_18_Months__c, Contact.Total_Donation__c, Contact.Donors_This_Year__c, BannerPhotoUrl, FullPhotoUrl, MediumPhotoUrl, SmallPhotoUrl
						FROM User 
                        WHERE ContactId = :classroom.Teacher__c ][0];
        
        List<Opportunity> donations = [SELECT Donor_Name__c, Amount, Donor_Comment__c, CloseDate
                                        FROM Opportunity
                                        WHERE Teacher__c = :teacher.Contact.Id AND RecordType.DeveloperName IN ('adoption', 'Disbursement', 'schooldonation') AND StageName = 'Sale Closed / Paid' AND CloseDate = LAST_N_DAYS:548 AND (NOT Name  LIKE '%Teacher Credit Card%') AND Hide_Transaction_on_Profile__c = FALSE
                                        ORDER BY CloseDate DESC];


        StaticResource staticResource = [SELECT Name, SystemModStamp FROM StaticResource WHERE Name = 'donor_community' LIMIT 1];
        String bannerURL = URL.getCurrentRequestUrl().toExternalForm().split('sfsites')[0] + 'resource/' + String.valueOf(((DateTime)staticResource.get('SystemModStamp')).getTime()) + '/' + staticResource.get('Name') + '/default_banner.jpg';
        List<ContentDistribution> cdistribution = [SELECT Name, RelatedRecordId, ContentDownloadUrl, DistributionPublicUrl FROM ContentDistribution WHERE RelatedRecordId = :classroom.Teacher__c AND Name LIKE '%banner%'];
        if(!cdistribution.isEmpty()) {
            bannerURL = cdistribution[0].ContentDownloadUrl;
        }

        ClassroomLandingPageController.data data = new ClassroomLandingPageController.data(classroom, teacher, donations, bannerURL);

		return data;
    }

    public static void saveNotify(ClassroomLandingPageController.notify notify) {
		
        List<Notify_Donor__c> notifications = [SELECT Id FROM Notify_Donor__c WHERE Email__c = :notify.Email AND Classroom__c = :notify.Classroom];
        
        if(notifications.isEmpty()) {
            Notify_Donor__c notifyDonor = new Notify_Donor__c();
            notifyDonor.First_Name__c = notify.FirstName;
            notifyDonor.Last_Name__c = notify.LastName;
            notifyDonor.Email__c = notify.Email;
            notifyDonor.Classroom__c = notify.Classroom;
            notifyDonor.Marketing_Email__c = notify.MarketingEmail;

            insert notifyDonor;

            
            OrgWideEmailAddress owa = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName='AdoptAClassroom.org' LIMIT 1];
            List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
            
            EmailTemplate template = [SELECT Id FROM EmailTemplate WHERE name = 'Notify Inactive Email' LIMIT 1];

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(notify.Teacher);
            mail.setSaveAsActivity(false);
            mail.setOrgWideEmailAddressId(owa.Id);
            mail.setTemplateID(template.Id);
            allmsg.add(mail);
            if(!Test.isRunningTest()) {
                Messaging.sendEmail(allmsg);
            }
        } else {
            throw new CustomException('You have already notified it.');
        }
	}

    public class CustomException extends Exception {}
}