public with sharing class OrderCloudSpendingAccount {
  public String ID { get; set; }
  public String Name { get; set; }
  public Decimal Balance { get; set; }
  public Boolean AllowAsPaymentMethod { get; set; }
  public Date StartDate { get; set; }
  public Date EndDate { get; set; }
}