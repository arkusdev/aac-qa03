@isTest
private class QueryUtilsTest {

  private static Map<Id, Account> accounts;

  static testMethod void testQueryUtils() {
    initTestData();

    Test.startTest();

    System.assertEquals(null, QueryUtils.queryObject(null));
    System.assertEquals(null, QueryUtils.queryObjects(null));
    System.assertEquals(null, QueryUtils.queryObjects(new Set<Id>()));
    System.assertEquals(null, QueryUtils.queryObjectFromFieldSets(null, null));
    System.assertEquals(null, QueryUtils.queryObjectsFromFieldSets(null, null));

    Account a = accounts.values()[0];

    System.assertEquals(null, QueryUtils.queryObjectFromFieldSets(
      a.Id, new List<String>()));
    System.assertEquals(null, QueryUtils.queryObjectsFromFieldSets(
      accounts.keySet(), new List<String>()));

    Account testAccount = (Account) QueryUtils.queryObject(a.Id);

    System.assertEquals(a.Id, testAccount.Id);

    List<Account> testAccountList = QueryUtils.queryObjects(accounts.keySet());

    System.assertEquals(accounts.size(), testAccountList.size());

    for (Account acc : testAccountList) {
      System.assertEquals(true, accounts.containsKey(acc.Id));
    }

    testAccount = (Account) QueryUtils.queryObjectFromFieldSets(a.Id,
      new List<String> { 'Unit_Test', 'Random' + Datetime.now().getTime() });

    System.assertEquals(a.Id, testAccount.Id);

    System.assertEquals(null, QueryUtils.getFieldListByObjectType(null));
    List<String> fields = QueryUtils.getFieldListByObjectType(
      Account.SObjectType);

    System.assertNotEquals(true, fields.isEmpty());

    Test.stopTest();
  }

  private static void initTestData() {
    List<Account> aList = new List<Account>();

    for (Integer i = 0; i < 10; i++) {
      aList.add(TestUtils.createAccount());
    }

    insert aList;

    accounts = new Map<Id, Account>(aList);

    List<Contact> contacts = new List<Contact>();

    for (Account a : aList) {
      for (Integer i = 0; i < 10; i++) {
        contacts.add(TestUtils.createContact(a.Id));
      }
    }

    insert contacts;
  }
}