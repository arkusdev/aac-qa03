public without sharing class DonorUtils {
    
    //Unit tests: ShoppingCartController_Test, DonorDetailsControllerTest, ChargentDonorDetailsControllerTest

    private static final String TYPE_CREDIT_CARD = 'Credit Card';
    private static final String TYPE_CHECK = 'Check or Money Order';
    private static final String ID_PARAM = 'id';
    private static final String PID_PARAM = 'cid';
    private static final String DONOR = 'Donor';
    private static final String ITEM_TYPE_GENERAL_FUND = 'generalFund';
    private static final String DONATION_NAME = 'Donation to {0}';
    private static final String SCHOOL_DONATION_NAME = 'School Donation to {0}';
    private static final String OPPORTUNITY_STAGE_PLEDGED = 'Pledged';
    private static final String FREQUENCY_ONE_TIME = 'One time';
    private static final String FREQUENCY_MONTHLY = 'Monthly';
    private static final String NAME_ANNUAL_FUND = 'Annual Fund';
    private static final String AUTHORIZE_NET = 'Authorize.Net';
    private static final String USD = 'USD';
    private static final String MULTIPLY_BY = 'Multiply By';
    private static final String PENDING_AUTHORIZATION = 'Pending Authorization';
    private static final String ITEM_TYPE_ANNUAL_FUND = 'Annual Fund';
    private static final string CLASSROOM_ADOPTION_RECORDTYPE=[SELECT Id FROM recordtype WHERE developername='adoption'].Id;
    private static final string SCHOOL_DONATION_RECORDTYPE=[SELECT Id FROM recordtype WHERE developername='schooldonation'].Id;
    private static final string UNRESTRICTED_RECORDTYPE=[SELECT Id FROM recordtype WHERE developername='Donation'].Id;
    private static final String UNRESTRCITED_DONATION_NAME = 'Unrestricted Donation - Processing Fee';
    private static final String UNRESTRCITED_HELP_DONATION_NAME = 'Donation - Teachers First Fund';
    private static final String UNRESTRCITED_PROGRAM_FEE_NAME = 'Program Fee - School Donation';

    public static donation_split__Designation__c getClassroomById(Id classroomId) {
        
        donation_split__Designation__c classroom = new donation_split__Designation__c();
        List<donation_split__Designation__c> result = [SELECT Id, Name, School__c, School__r.Name, SchoolName__c, Teacher__c, Teacher__r.Name, TeacherName__c, Teacher__r.FirstName, Teacher__r.LastName,  RecordType.DeveloperName, GTM_Category__c, GTM_Name__c FROM donation_split__Designation__c WHERE Id = :classroomId];
        if(!result.isEmpty()) {
            classroom = result[0];
        }

        return classroom;
    }

    public static Date getNextMonthDate() {
        return Date.today().addMonths(1);
    }

    public static pymt__PaymentX__c savePaymentData(Contact c, Account a, pymt__PaymentX__c payment, String cartItemString, String inHonorOfName, Decimal subTotal, Decimal total, Decimal monthlySubTotal, Decimal monthlyTotal, Decimal fee, Decimal monthlyFee) {
        if (c == null) {
            return null;
        }

        if (String.isNotBlank(payment.Id)) {
            payment = payment.clone(false, true);
        }

        JSONParser parser;
        Opportunity opp;
        donation_split__Designation__c cr;
        pymt__Shopping_Cart_Item__c currentItem;
        npe03__Recurring_Donation__c rd;
        Donor_Checkout_Settings__c settings = Donor_Checkout_Settings__c.getInstance();
        Set<Id> classroomIds = new Set<Id>();
        List<Opportunity> opportunities = new List<Opportunity>();
        List<pymt__Shopping_Cart_Item__c> paymentItems = new List<pymt__Shopping_Cart_Item__c>();
        List<npe03__Recurring_Donation__c> recurringDonations = new List<npe03__Recurring_Donation__c>();
        Map<Id, donation_split__Designation__c> campaignIdMap;
        parser = JSON.createParser(cartItemString);
        List<ShoppingCartItem> cartItems = (List<ShoppingCartItem>) parser.readValueAs(List<ShoppingCartItem>.class);

        payment.pymt__Amount__c = total;
        payment.pymt__Transaction_Fee__c = fee;
        payment.Subtotal__c = subTotal;
        payment.pymt__Payment_Processor__c = AUTHORIZE_NET;
        payment.pymt__Currency_ISO_Code__c = USD;
        payment.pymt__Processor_Connection__c = settings.Payment_Connection_Id__c;
        insert payment;

        for (ShoppingCartItem sci : cartItems) {
            if (String.isNotBlank(sci.classroomId)) {
                classroomIds.add(sci.classroomId);
            }
        }

        campaignIdMap = getClassroomToCampaignIdMap(classroomIds);

        for (ShoppingCartItem sci : cartItems) {
            if (sci.amount == null || sci.amount == 0) {
                continue;
            }

            if (sci.frequency == FREQUENCY_ONE_TIME) {
                opp = new Opportunity( Amount = sci.amount, npsp__Primary_Contact__c = c.Id, CloseDate = Date.today(), StageName = OPPORTUNITY_STAGE_PLEDGED);

                if (sci.type == ITEM_TYPE_GENERAL_FUND) {
                    opp.Name = String.format(DONATION_NAME, new List<String> { NAME_ANNUAL_FUND });
                    opp.CampaignId = settings.Annual_Fund_Campaign_Id__c;
                } else if (String.isNotBlank(sci.classroomId)) {
                    cr = campaignIdMap.get(sci.classroomId);

                    if (cr != null) {
                        opp.CampaignId = cr.Campaign__c;
                        opp.Name = String.format(DONATION_NAME, new List<String> { cr.Name });
                        opp.Teacher__c = cr.Teacher__c;
                    }
                }

                opportunities.add(opp);
            } else if (sci.frequency == FREQUENCY_MONTHLY) {
                rd = new npe03__Recurring_Donation__c( npe03__Installment_Period__c = FREQUENCY_MONTHLY,npe03__Installments__c = 12,npe03__Schedule_Type__c = MULTIPLY_BY,npe03__Amount__c = sci.amount,npe03__Contact__c = c.Id,npe03__Date_Established__c = Date.today(),npe03__Open_Ended_Status__c = PENDING_AUTHORIZATION);

                if (sci.type == ITEM_TYPE_GENERAL_FUND) {
                    rd.Name = String.format(DONATION_NAME, new List<String> { NAME_ANNUAL_FUND });
                    rd.npe03__Recurring_Donation_Campaign__c = settings.Annual_Fund_Campaign_Id__c;
                } else if (String.isNotBlank(sci.classroomId)) {
                    cr = campaignIdMap.get(sci.classroomId);

                    if (cr != null) {
                        rd.npe03__Recurring_Donation_Campaign__c = cr.Campaign__c;
                        rd.Name = String.format(DONATION_NAME, new List<String> { cr.Name });
                    }
                }
                recurringDonations.add(rd);
            }
        }

        insert recurringDonations;
        insert opportunities;

        for (Opportunity o : opportunities) {
            currentItem = new pymt__Shopping_Cart_Item__c(Donation__c = o.Id,pymt__Contact__c = o.npsp__Primary_Contact__c,pymt__Payment__c = payment.Id,pymt__Quantity__c = 1,pymt__Unit_Price__c = o.Amount,Name = o.Name);
            paymentItems.add(currentItem);
        }

        insert paymentItems;
        return payment;
    }

    public static Map<Id, donation_split__Designation__c> getClassroomToCampaignIdMap(Set<Id> classroomIds) {
        Map<Id, donation_split__Designation__c> idMap = new Map<Id, donation_split__Designation__c>();
        for (donation_split__Designation__c cr : [SELECT Id, Campaign__c, Name, Teacher__c, Teacher__r.School__c,Teacher__r.School__r.Name, RecordType.DeveloperName FROM donation_split__Designation__c WHERE Id in :classroomIds]) {
            idMap.put(cr.Id, cr);
        }
        return idMap;
    }

    public static Contact upsertContact(Contact c) {
        if (c == null) {
            return null;
        }

        try {
            Id donorRecordType = [select Id, DeveloperName from RecordType
            where DeveloperName = :DONOR
            and SObjectType = :Contact.SObjectType.getDescribe().getName()].Id;
            c.RecordTypeId = donorRecordType;
        } catch(Exception e){
            System.debug(e);
        }

        try {
            Contact existingContact = [SELECT name, email FROM Contact WHERE email=:c.Email LIMIT 1];
            c.id = existingContact.id;
        } catch(Exception e){
            System.debug(e);
        }

        upsert c;
        return c;
    }

    public static Account upsertAccount(Account a, String donorType) {
        if (a == null) {
            return null;
        }

        try {
            Id donorRecordType = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :donorType AND SObjectType = :Account.SObjectType.getDescribe().getName()].Id;
            a.RecordTypeId = donorRecordType;
        } catch(Exception e){
            System.debug(e);
        }

        try {
            Account existingAccount = [SELECT Id,name, Email__c FROM Account WHERE Email__c=:a.Email__c AND name=:a.Name LIMIT 1];
            a.id = existingAccount.id;
        } catch(Exception e){
            System.debug(e);
        }

        upsert a;
        return a;
    }

    public static List<FAQ__c> getFAQs() {
        List<FAQ__c> faqs = [SELECT Active__c, Question__c, Answer__c FROM FAQ__c WHERE Active__c = true];
        return faqs;
    }

    public static boolean cookiesSet() {
        Pagereference pr = new Pagereference(Apexpages.currentPage().getUrl());
        Cookie cook = new Cookie('domain', ApexPages.currentPage().getHeaders().get('Host'), null, -1, false);
        Cookie cook1 = new Cookie('debug_logs','debug_logs', null, -1, false);
        pr.setCookies(new Cookie[] {cook,cook1});
        return true;
    }

    public static ChargentOrders__ChargentOrder__c savePaymentData(String donorComment, Contact c, Account a, ChargentOrders__ChargentOrder__c payment, String cartItemString, String inHonorOfName, String inHonorOfEmail, String paymentPromoCode,  Decimal subTotal, Decimal total, Decimal monthlySubTotal, Decimal grandTotal, Decimal expFees, Decimal monthlyTotal, Decimal fee, Decimal helpAmount, Decimal monthlyFee,Boolean isProcessingFeeIncluded, String myPicklist, Date endDate) {
        if (c == null) {
            return null;
        }
    
        if (String.isNotBlank(payment.Id)) {
            payment = payment.clone(false, true);
        }
    
        JSONParser parser;
        Opportunity opp;
        donation_split__Designation__c cr;
        Donor_Cart_Item__c currentItem;
        npe03__Recurring_Donation__c rd;
        Donor_Checkout_Settings__c settings =  Donor_Checkout_Settings__c.getInstance();
        Set<Id> classroomIds = new Set<Id>();
        List<Opportunity> opportunities = new List<Opportunity>();
        List<Donor_Cart_Item__c> paymentItems = new List<Donor_Cart_Item__c>(); 
        List<npe03__Recurring_Donation__c> recurringDonations =  new List<npe03__Recurring_Donation__c>();
        Map<Id, donation_split__Designation__c> campaignIdMap;
        Id teacherId;
        Id schoolId;
        
        parser = JSON.createParser(cartItemString);
        List<ShoppingCartItem> cartItems = (List<ShoppingCartItem>)  parser.readValueAs(List<ShoppingCartItem>.class); 
        payment.Transaction_Fee__c = (fee == null) ? 0 : fee;
        payment.Teacher_Help_Fund__c = (helpAmount == null) ? 0 :helpAmount;
        payment.In_Honor_Of_Name__c = inHonorOfName;
        payment.ChargentOrders__Subtotal__c = grandTotal;
        payment.InHonorOfEmail__c = inHonorOfEmail;
        payment.Promo_Code__c = paymentPromoCode;
        payment.ChargentOrders__Gateway__c = settings.Payment_Connection_Id__c;
        payment.ChargentOrders__Account__c = c.AccountId;
        payment.ChargentOrders__Billing_First_Name__c = c.FirstName;    
        payment.ChargentOrders__Billing_Last_Name__c = c.LastName;
        payment.Tribute_Type__c= myPicklist;
        
        if (endDate != null) {
            payment.ChargentOrders__Payment_End_Date__c = endDate;
            payment.ChargentOrders__Payment_Stop__c = 'Date';
        } else {
            payment.ChargentOrders__Payment_Stop__c = 'Unending';
        }
               
        for (ShoppingCartItem sci : cartItems) {
            if (String.isNotBlank(sci.classroomId)) {
                classroomIds.add(sci.classroomId);
            }
        }
        
        campaignIdMap = getClassroomToCampaignIdMap(classroomIds);
        
        if(payment.Transaction_Fee__c != 0.00 && payment.Transaction_Fee__c != null){
            opp = new Opportunity(Amount = payment.Transaction_Fee__c,Processing_Fee__c=true,npsp__Primary_Contact__c = c.Id,CloseDate = Date.today(),StageName = OPPORTUNITY_STAGE_PLEDGED);
            opp.Name =UNRESTRCITED_DONATION_NAME + ' ' + formatDate(Date.today()) + ' ' + (payment.Anonymous__c ? 'Anonymous' : c.FirstName + ' ' + c.LastName);
            opp.RecordtypeId=UNRESTRICTED_RECORDTYPE;
            opportunities.add(opp);
        }
        
        if(payment.Teacher_Help_Fund__c != 0.00 && payment.Teacher_Help_Fund__c != null){
            opp = new Opportunity(Amount = payment.Teacher_Help_Fund__c,npsp__Primary_Contact__c = c.Id,CloseDate = Date.today(),StageName = OPPORTUNITY_STAGE_PLEDGED);
            opp.CampaignId = settings.Annual_Fund_Campaign_Id__c;
            opp.Name =UNRESTRCITED_HELP_DONATION_NAME + ' ' + formatDate(Date.today()) + ' ' + (payment.Anonymous__c ? 'Anonymous' : c.FirstName + ' ' + c.LastName);
            opp.RecordtypeId=UNRESTRICTED_RECORDTYPE;
            opportunities.add(opp);
        }
        
        Double schoolTotalFee = 0;
    
        for (ShoppingCartItem sci : cartItems) {
            if (sci.amount == null || sci.amount == 0) {
                continue;
            }
            
            if (sci.frequency == FREQUENCY_ONE_TIME) {
                Double amount = sci.amount;
                Double feeAmount = 0;
                
                if(String.isNotBlank(sci.classroomId)) {
                    cr = campaignIdMap.get(sci.classroomId);
                }
                if (cr != null && cr.RecordType.DeveloperName =='School_Registration'){
                amount = (sci.amount * 90) / 100;
                feeAmount = sci.amount - amount;
                schoolTotalFee += feeAmount;
    
                Opportunity feeOpp = new Opportunity(Amount = feeAmount, npsp__Primary_Contact__c = c.Id,CloseDate = Date.today(),StageName = OPPORTUNITY_STAGE_PLEDGED);
                feeOpp.Name =UNRESTRCITED_PROGRAM_FEE_NAME + ' ' + formatDate(Date.today()) + ' ' + (payment.Anonymous__c ? 'Anonymous' : c.FirstName + ' ' + c.LastName);
                feeOpp.RecordtypeId=UNRESTRICTED_RECORDTYPE;
                opportunities.add(feeOpp);
                }
                
                opp = new Opportunity(
                    Amount = amount,
                    npsp__Primary_Contact__c = c.Id,
                    CloseDate = Date.today(),
                    StageName = OPPORTUNITY_STAGE_PLEDGED,
                    Amount_W_Feed__c = sci.amount);
                
                if (sci.type == ITEM_TYPE_GENERAL_FUND) {
                    opp.Name = String.format(DONATION_NAME, new List<String> { NAME_ANNUAL_FUND }) + ' ' + formatDate(Date.today()) + ' ' + (payment.Anonymous__c ? 'Anonymous' : c.FirstName + ' ' + c.LastName);
                    opp.CampaignId = settings.Annual_Fund_Campaign_Id__c;
                    opp.RecordtypeId=UNRESTRICTED_RECORDTYPE;
                    payment.Annual_Fee__c = amount;
                } else if (String.isNotBlank(sci.classroomId)) {
                    cr = campaignIdMap.get(sci.classroomId);
                    
                    if (cr != null && cr.RecordType.DeveloperName =='School_Registration') {       
                        opp.CampaignId = cr.Campaign__c;
                        opp.Name = String.format(SCHOOL_DONATION_NAME,new List<String> {cr.Teacher__r.School__r.Name}) + ' ' + formatDate(Date.today()) + ' ' + (payment.Anonymous__c ? 'Anonymous' : c.FirstName + ' ' + c.LastName);
                        opp.Teacher__c = cr.Teacher__c;
                        opp.Classroom__c = cr.Id;
                        payment.Classroom__c = cr.Id;
                        teacherId=cr.Teacher__c;
                        schoolId=cr.Teacher__r.School__c;
                        opp.RecordtypeId=SCHOOL_DONATION_RECORDTYPE;
                    }
                    
                    if (cr != null && cr.RecordType.DeveloperName =='Classroom_Registration') {  
                        opp.CampaignId = cr.Campaign__c;
                        opp.Name = String.format(DONATION_NAME,new List<String> {cr.Name}) + ' ' + formatDate(Date.today()) + ' ' + (payment.Anonymous__c ? 'Anonymous' : c.FirstName + ' ' + c.LastName);
                        opp.Teacher__c = cr.Teacher__c;
                        opp.Classroom__c = cr.Id;
                        payment.Classroom__c = cr.Id;
                        teacherId=cr.Teacher__c;
                        schoolId=cr.Teacher__r.School__c;
                        opp.RecordtypeId=CLASSROOM_ADOPTION_RECORDTYPE;
                    }
                }
                
                opportunities.add(opp);
            } else if (sci.frequency == FREQUENCY_MONTHLY) {
                Double amount = sci.amount;
                Double feeAmount = 0;
                
                if(String.isNotBlank(sci.classroomId)) {
                    cr = campaignIdMap.get(sci.classroomId);
                }
                if (cr != null && cr.RecordType.DeveloperName =='School_Registration'){
                amount = (sci.amount * 90) / 100;
                feeAmount = sci.amount - amount;
                schoolTotalFee += feeAmount;
    
                Opportunity feeOpp = new Opportunity(Amount = feeAmount, npsp__Primary_Contact__c = c.Id,CloseDate = Date.today(),StageName = OPPORTUNITY_STAGE_PLEDGED);
                feeOpp.Name =UNRESTRCITED_PROGRAM_FEE_NAME + ' ' + formatDate(Date.today()) + ' ' + (payment.Anonymous__c ? 'Anonymous' : c.FirstName + ' ' + c.LastName);
                feeOpp.RecordtypeId=UNRESTRICTED_RECORDTYPE;
                opportunities.add(feeOpp);
                }
                opp = new Opportunity(
                    Amount = amount,
                    npsp__Primary_Contact__c = c.Id,
                    CloseDate = Date.today(),
                    StageName = OPPORTUNITY_STAGE_PLEDGED,
                    Amount_W_Feed__c = sci.amount);
                
                if (sci.type == ITEM_TYPE_GENERAL_FUND) {
                    opp.Name = String.format(DONATION_NAME, new List<String> { NAME_ANNUAL_FUND }) + ' ' + formatDate(Date.today()) + ' ' + (payment.Anonymous__c ? 'Anonymous' : c.FirstName + ' ' + c.LastName);
                    opp.CampaignId = settings.Annual_Fund_Campaign_Id__c;
                    opp.RecordtypeId=UNRESTRICTED_RECORDTYPE;
                    payment.Annual_Fee__c = amount;
                } else if (String.isNotBlank(sci.classroomId)) {
                    cr = campaignIdMap.get(sci.classroomId);
                    
                    if (cr != null && cr.RecordType.DeveloperName =='School_Registration') {       
                        opp.CampaignId = cr.Campaign__c;
                        opp.Name = String.format(SCHOOL_DONATION_NAME,new List<String>{cr.Teacher__r.School__r.Name}) + ' - ' + formatDate(Date.today()) + ' ' + (payment.Anonymous__c ? 'Anonymous' : c.FirstName + ' ' + c.LastName);
                        opp.Teacher__c = cr.Teacher__c;
                        opp.Classroom__c = cr.Id;
                        teacherId=cr.Teacher__c;
                        schoolId=cr.Teacher__r.School__c;
                        opp.RecordtypeId=SCHOOL_DONATION_RECORDTYPE;
                    }
                    
                    if (cr != null && cr.RecordType.DeveloperName =='Classroom_Registration') {  
                        opp.CampaignId = cr.Campaign__c;
                        opp.Name = String.format(DONATION_NAME,new List<String> { cr.Name}) + ' ' + formatDate(Date.today()) + ' ' + (payment.Anonymous__c ? 'Anonymous' : c.FirstName + ' ' + c.LastName);
                        opp.Teacher__c = cr.Teacher__c;
                        opp.Classroom__c = cr.Id;
                        teacherId=cr.Teacher__c;
                        schoolId=cr.Teacher__r.School__c;
                        opp.RecordtypeId=CLASSROOM_ADOPTION_RECORDTYPE;
                    }
                }
                
                opportunities.add(opp);
                
                Payment.ChargentOrders__Payment_Status__c = 'Recurring';
                payment.ChargentOrders__Payment_Start_Date__c = system.today().addDays(30);
                payment.ChargentOrders__Payment_Frequency__c = FREQUENCY_MONTHLY;
                payment.ChargentOrders__Next_Transaction_Date__c = system.today() + 30;
            }
        }
        
        payment.School_Program_Expense_Fee__c = schoolTotalFee;
        payment.Teacher__c=teacherId;
        payment.School__c=schoolId;
        
        insert payment;
        for(Opportunity o:opportunities){
            
            if(o.Teacher__c!=null){
                o.Chargent_Order__c=payment.id;   
                o.Promo_Code__c = payment.Promo_Code__c;
            }else{
                o.Chargent_Order__c=payment.id;
            }
            if(a != NULL) {
                o.AccountId = a.Id;
            }
            o.Donor_comment__c = donorComment;
            
        }
        
        insert opportunities;
        
        for (Opportunity o : opportunities) {
            currentItem = new Donor_Cart_Item__c(
                Donation__c = o.Id,
                Payment__c = payment.id,
                Contact__c = o.npsp__Primary_Contact__c,
                Quantity__c = 1,
                Unit_Price__c = o.Amount,
                Name = o.Name);
            
            paymentItems.add(currentItem);
        }
        
        insert paymentItems;
        
        return payment;
    }

    public static String formatDate(Date input) {
        return DateTime.newInstance(
            input.year(), input.month(), input.day()
        ).format('MM/dd/yyyy');
    }

    public static List<String> getTributeType() {
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = ChargentOrders__ChargentOrder__c.Tribute_Type__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            options.add(p.getLabel());
        }
        return options;
    }

    public static ChargentOrders__ChargentOrder__c getChargentOrder(String chargentId) {
        ChargentOrders__ChargentOrder__c result = new ChargentOrders__ChargentOrder__c();
        List<ChargentOrders__ChargentOrder__c> orders = [SELECT Id,(SELECT Id, Teacher__c,Teacher_s_School__c,Teacher__r.Name,amount,Amount_W_Feed__c,RecordType.DeveloperName, Classroom__r.GTM_Category__c, Classroom__r.GTM_Name__c FROM Opportunities__r where RecordType.name!='Unrestricted Donation'),ChargentOrders__Billing_First_Name__c,Payment_Contact__r.Donor_email_address__c, Payment_Contact__r.FirstName,Teacher_Help_Fund__c,Payment_Contact__r.LastName,Payment_Contact__r.Email,ChargentOrders__Last_Transaction__c,ChargentOrders__Subtotal__c,ChargentOrders__Billing_Last_Name__c,ChargentOrders__Billing_Address__c,ChargentOrders__Billing_Address_Line_2__c,ChargentOrders__Billing_City__c,ChargentOrders__Billing_State__c, Transaction_Date__c,ChargentOrders__Billing_Zip_Postal__c,ChargentOrders__Billing_Email__c,ChargentOrders__Billing_Country__c, ChargentOrders__Card_Number__c,ChargentOrders__Card_Expiration_Month__c, Annual_Fee__c,Transaction_Fee__c,Teacher__r.Name,School__r.Name,Donation_Amount__c,ChargentOrders__Payment_Frequency__c,ChargentOrders__Card_Last_4__c , Payment_Subtotal__c, ChargentOrders__Total__c, Donation_Reference__c, School_Program_Expense_Fee__c, ChargentOrders__Card_Expiration_Year__c, ChargentOrders__Next_Transaction_Date__c, ChargentOrders__Payment_End_Date__c FROM ChargentOrders__ChargentOrder__c WHERE Id =:chargentId];
        if(!orders.isEmpty()) {
            result = orders[0];
        }

        return result;
    }

    public static List<String> ChargentEmailDonationReceipt(List<ChargentOrders__ChargentOrder__c> scope) {
        List<String> result = new List<String>();
        OrgWideEmailAddress owa = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName = 'AdoptAClassroom.org'];
        EmailTemplate templateId = [SELECT Id FROM EmailTemplate WHERE name = 'Chargent Donation Email Receipt'];
        List<ChargentOrders__ChargentOrder__c> orders = [SELECT ToSendThankYouEmail__c,ChargentOrders__Billing_Email__c, Payment_Contact__c,Payment_Contact__r.MailingCity,Payment_Contact__r.MailingCountry,Payment_Contact__r.MailingPostalCode,Payment_Contact__r.MailingState,Payment_Contact__r.MailingStreet, ChargentOrders__Billing_City__c, ChargentOrders__Billing_Zip_Postal__c, ChargentOrders__Billing_State__c, ChargentOrders__Billing_Address__c FROM ChargentOrders__ChargentOrder__c WHERE Id IN :scope ];
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
        for(ChargentOrders__ChargentOrder__c order : orders) {
            
            //Create Email
            if(order.ChargentOrders__Billing_Email__c!='') {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                List<String> toAddresses = new List<String> {order.ChargentOrders__Billing_Email__c};
                mail.setTemplateID(templateId.Id);
                mail.setSaveAsActivity(false);
                mail.setOrgWideEmailAddressId(owa.Id);
                mail.setToAddresses(toAddresses);
                mail.setTargetObjectId(order.Payment_Contact__c);
                mail.setWhatId(order.Id);
                mail.setTreatTargetObjectAsRecipient(false);
                allmsg.add(mail);
                order.ToSendThankYouEmail__c = false;
            }

            //Update Contact Information
            if(order.Payment_Contact__c != NULL) {
                order.Payment_Contact__r.MailingCity = order.ChargentOrders__Billing_City__c;
                order.Payment_Contact__r.MailingCountry = 'US';
                order.Payment_Contact__r.MailingPostalCode = order.ChargentOrders__Billing_Zip_Postal__c;
                order.Payment_Contact__r.MailingState = order.ChargentOrders__Billing_State__c == 'Outside US' ? '' : order.ChargentOrders__Billing_State__c;
                order.Payment_Contact__r.MailingStreet = order.ChargentOrders__Billing_Address__c;
            }
        }

        if(!orders.isEmpty()) {
            update orders;
            List<Donor_Cart_Item__c> cartItems = [SELECT Id,Payment__c,Transaction_Completed__c from Donor_Cart_Item__c where Payment__c IN :orders ];
            if(!cartItems.isEmpty()){
                for(Donor_Cart_Item__c ci : cartItems){
                    ci.Transaction_Completed__c=true;
                }
                update cartItems;
            }
        }

        if(!allmsg.isEmpty() && !Test.isRunningTest()) {
            Messaging.SendEmailResult[] results = Messaging.sendEmail(allmsg);
            if (results[0].success) {
                System.debug('The email was sent successfully.');
            } else {
                System.debug('The email failed to send: ' +  results[0].errors[0].message);
            }
        }
        
        return result;
    }

    public static String updatePaymentAndReturnLink(ChargentOrders__ChargentOrder__c payment) {

        return '';
    }

    public static PageReference charge(String chargentId, ChargentOrders__ChargentOrder__c c) {
        
        ChargentOrders.tChargentOperations.TChargentResult result = ChargentOrders.tChargentOperations.ChargeOrder_Click(chargentId);
        ChargentCartController.paymentResponse res;

        return null;
    }
}