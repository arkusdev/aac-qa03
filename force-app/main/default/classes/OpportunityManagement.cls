public with sharing class OpportunityManagement {

  public static final String RECORD_TYPE_DISBURSMENT = 'Disbursement';
  public static final String RECORD_TYPE_ADJUSTMENT = 'adjustment';
  public static final String RECORD_TYPE_ORDER = 'Orders';
  private static final String RECORD_TYPE_DONATION = 'adoption';
  private static final String RECORD_TYPE_SCHOOL = 'schooldonation';
  public static final String RECORD_TYPE_DONATION_UNRESTRICTED = 'Donation'; 
  private static final String AMOUNT_FIELD = 'amount';
  private static final String TEACHER_FIELD = 'teacherId';
  private static final String RECORD_TYPE_NAME_FIELD = 'recordTypeName';
  public  static Map<String, RecordType> recordTypeMap = new Map<String, RecordType>();  
  public static final String STAGE_PAID = 'Sale Closed / Paid';

  private static final Set<String> VALID_TYPES = new Set<String> {
    RECORD_TYPE_DONATION, RECORD_TYPE_SCHOOL, RECORD_TYPE_ORDER, RECORD_TYPE_DISBURSMENT, RECORD_TYPE_ADJUSTMENT};
    
  public static void afterInsert(List<Opportunity> nList) {
    if (nList == null || nList.isEmpty()) {
      return;
    }
    recordTypeMap = getRecordTypeMap(new Set<String>{RECORD_TYPE_ORDER, RECORD_TYPE_DONATION, RECORD_TYPE_SCHOOL, RECORD_TYPE_DONATION_UNRESTRICTED, RECORD_TYPE_NAME_FIELD, RECORD_TYPE_DISBURSMENT, RECORD_TYPE_ADJUSTMENT}, 'Opportunity');
    rollupAmountsToContact(nList, null);
    UpdateRefund(nList,null);
  }

  public static void afterUpdate(List<Opportunity> nList,
      Map<Id, Opportunity> oldMap) {
    if (nList == null || nList.isEmpty() || oldMap == null ||
        oldMap.isEmpty()) {
      return;
    }
    recordTypeMap = getRecordTypeMap(new Set<String>{RECORD_TYPE_ORDER, RECORD_TYPE_DONATION, RECORD_TYPE_SCHOOL, RECORD_TYPE_DONATION_UNRESTRICTED, RECORD_TYPE_NAME_FIELD, RECORD_TYPE_DISBURSMENT, RECORD_TYPE_ADJUSTMENT}, 'Opportunity');
    rollupAmountsToContact(nList, oldMap);
    UpdateRefund(nList, oldMap);
    //SendDonorEmail(nList,oldmap);
  }

  private static void rollupAmountsToContact(List<Opportunity> nList,
      Map<Id, Opportunity> oldMap) {
    Opportunity oldOpportunity;
    Map<String, RecordType> rtMap = recordTypeMap;
    Set<Id> recordTypeIds = new Set<Id>();
    Set<Id> contactIds = new Set<Id>();
    List<Contact> toUpdate = new List<Contact>();

    for (RecordType rt : rtMap.values()) {
        recordTypeIds.add(rt.Id);
        system.debug('RecordtypeId-line47-->' +recordTypeIds);
    }

    for (Opportunity o : nList) {
      oldOpportunity = (oldMap == null) ? null : oldMap.get(o.Id);
        system.debug('oldOpportunity-Line53-->' +oldOpportunity);
      if (processOpportunity(o, oldOpportunity, recordTypeIds)) {
        contactIds.add(o.Teacher__c);
          system.debug('ContactIds-line56-->' +contactIds);

        if (oldOpportunity != null) {
          contactIds.add(oldOpportunity.Teacher__c);
            system.debug('ContactIds-line60-->' +contactIds);
        }
      }
    }

    if (contactIds.isEmpty()) {
      return;
    }
    Map<Id,contact> conlist = new Map<Id,contact>([select Id,Funds_Available__c from contact where Id IN: contactIds]);
          system.debug('Map conList-line69-->' +conlist);
    set<Id> conids = new set<Id>();
    list<contact> conlistupdate = new list<contact>();
    map<Id,contact> conmap = new map<Id,Contact>();
    for(Opportunity opp : nlist)
    {
        system.debug('Opp List-Line74-->' +conlist.keyset());
        system.debug('Opp Id-Line76-->' +opp.Id);
        system.debug('Opp List-Line75-->' +opp.Teacher__c);
        if(conlist.containskey(opp.teacher__c))
      
        {
           
            system.debug('Opp amount-Line-82-->' +opp.Total_Amount__c+ '--' +opp.Id);
            system.debug('conList Fund-Line-83-->' +conlist.get(opp.teacher__c).Funds_Available__c);
            system.debug('opp reocrdtype-Line-84-->' +opp.RecordTypeId);
            if(conlist.get(opp.teacher__c).Funds_Available__c > opp.Total_Amount__c && opp.RecordTypeId == rtMap.get('Orders').Id)
        
            {
                contact c = new contact();
                c= conlist.get(opp.teacher__c);
                system.debug('contact get tech-Line82-->'+c);
                if(opp.Total_Amount__c != Null)
                {
                    c.Funds_Available__c -= opp.Total_Amount__c;     
                }
                else if(opp.Subtotal__c != Null)
                {
                     c.Funds_Available__c -= (opp.Subtotal__c + opp.ShippingCost__c + opp.TaxCost__c + opp.PromotionDiscount__c);
                }
                else
                {
                     c.Funds_Available__c -= opp.Amount;
                }
              //  c.Id = conlist.get(opp.teacher__c).Id;
                conlistupdate.add(c);
                conmap.put(c.id,c);
            }
            else
            {
                conids.add(opp.teacher__c);
            }
        }
    }
     list<contact> cupdate = new list<contact>();
    if(conids.size() > 0)    
    { 
        system.debug('conids if-->');
        system.debug('conids size-Line110-->' +conids);
        toUpdate = generateClassroomUpdateList(contactIds, recordTypeIds);
        system.debug('toupdate-Line108-->'+toUpdate);
    for(Opportunity op: nList)
    {
         system.debug('op records-Line 122-->'+op);
        system.debug('to update size-Line 123-->'+toUpdate.size());
        for(Contact c: toUpdate)
        {
             system.debug('c records-Line 125-->'+c);
            contact con = new contact();
            con = c;
            system.debug('con record-Line 116-->' +con);
            system.debug('conid-Line-129-->' +con.Id+'--'+op.Teacher__c+ '-' +op.RecordTypeId);
            if(con.Id == op.Teacher__c && (op.RecordTypeId == rtMap.get('adoption').Id || op.RecordTypeId == rtMap.get('schooldonation').Id || op.RecordTypeId == rtMap.get('Disbursement').Id || op.RecordTypeId == rtMap.get('adjustment').Id) && !op.Name.startsWith('Teacher Credit Card'))
            
            {
                con.Funds_Available__c += op.Amount;
                system.debug('con record-Line 118-->' +con);
                
            }
            if(con.Id == op.Teacher__c && op.RecordTypeId == rtMap.get('Orders').Id && oldmap == Null)
            {
                con.funds_Available__c -= op.Total_Amount__c;
                if(con.funds_Available__c < 0)
                {
                    con.funds_Available__c = 0;
                }
            }
            cupdate.add(con);
            conmap.put(con.Id,con);
        }
      }
    } 
       if(conmap.size() > 0)
     {
         list<contact> conlist1 = new list<contact>();
         for(Contact c : conmap.values())
         {
             system.debug('conmap values-Line 139-->' +c);
             if(C.id != Null)
             {
                 system.debug('conmap id not null-Line 142' +c.Id);
                 conlist1.add(c);
             }
         }
         update conlist1;
     }
      }

  private static List<Contact>
      generateClassroomUpdateList(Set<Id> contactIds, Set<Id> recordTypeIds) {
    system.debug('******'+contactIds+contactIds);
    system.debug('******'+recordTypeIds+recordTypeIds);
    String rtName;
    Id teacherId;
    Decimal amount;
    Contact c;
    Map<Id, Contact> contactMap = new Map<Id, Contact>();
    Map<string,AggregateResult> agmap = new Map<string,AggregateResult>();
       
    for (AggregateResult ar :
        [select sum(Total_Amount__c) orderamount, sum(Amount) adoptionAmount,
                Teacher__c teacherId,
                RecordType.DeveloperName recordTypeName
         from Opportunity
         where Teacher__c = :contactIds
         and RecordTypeId in :recordTypeIds
         and Teacher__c !=  null
         and StageName = :STAGE_PAID
         and (Not Name Like 'Teacher Credit Card%')
         group by Teacher__c, RecordType.DeveloperName]) {
             system.debug('AggregateResult-Line 171-->' +ar);
           agmap.put( (String) ar.get(RECORD_TYPE_NAME_FIELD), ar);
             
      //  }
          system.debug('agmap Line192-->' +agmap);
        AggregateResult agrorder = agmap.get('orders');
        AggregateResult agradoption = agmap.get('adoption');
        AggregateResult agrschooldonation = agmap.get('schooldonation');
        AggregateResult agrDisbursment = agmap.get('Disbursement');
        AggregateResult agradjustment = agmap.get('adjustment');
        System.debug('agrDisbursment -- 165--'+agrDisbursment);
        System.debug('agrorder---'+agrorder);
        System.debug('agradoption ---'+agradoption );
        System.debug('agradjustment -- 172--'+agradjustment);
         
          if(agmap.size() > 0)
        {
            //for loop to print multiple teacher ids
        if(agrorder != Null)
        {
            teacherId = (ID) agmap.get('orders').get(TEACHER_FIELD);
        }
        else
        {
            if(agmap.get('adoption') != Null)
            {
                teacherId = (ID) agmap.get('adoption').get(TEACHER_FIELD);
            }
            else if(agmap.get('schooldonation') != Null)
            {
                teacherId = (ID) agmap.get('schooldonation').get(TEACHER_FIELD);
            }
            else if(agmap.get('Disbursement') != Null)
            {
                teacherId = (ID) agmap.get('Disbursement').get(TEACHER_FIELD);
            }
            else if(agmap.get('adjustment') != Null)
            {
                teacherId = (ID) agmap.get('adjustment').get(TEACHER_FIELD);
            }
        }
        System.debug('teacherId ---'+teacherId );
        }
        list<contact> con = new list<contact>([select Id,Funds_Available__c,Total_Funds_Raised__c from contact where Id=: teacherId]);
        System.debug('con---'+con);
        System.debug('contactMap---'+contactMap);
        c = contactMap.get(teacherId);
        System.debug('c---'+c);

      if (c == null) {
         
        c = new Contact(
          Id = teacherId,          
          Funds_Available__c = 0,
          Total_Funds_Raised__c = 0);
          
      }
      if(agradoption != Null)
      {
          c.Funds_Available__c =  (Decimal) agradoption.get('adoptionAmount');
          c.Total_Funds_Raised__c =  (Decimal) agradoption.get('adoptionAmount');
          system.debug('Only Teacher Donation'+c.Funds_Available__c+'-'+c.Total_Funds_Raised__c);
      }
      
      if (agrschooldonation != NULL) {
          c.Funds_Available__c =  (Decimal) agrschooldonation.get('adoptionAmount');
          c.Total_Funds_Raised__c =  (Decimal) agrschooldonation.get('adoptionAmount');  
          system.debug('Only School Donation'+c.Funds_Available__c+'-'+c.Total_Funds_Raised__c);   
      }
      
      if(agrschooldonation != Null && agradoption == Null && agrDisbursment != Null && agradjustment != Null || agrschooldonation != Null && agradoption == Null && agrDisbursment == Null && agradjustment == Null){
          c.Funds_Available__c =  (Decimal) agrschooldonation.get('adoptionAmount');
          c.Total_Funds_Raised__c =  (Decimal) agrschooldonation.get('adoptionAmount');
          system.debug('Teacher and School Donation - School Donation'+c.Funds_Available__c+'-'+c.Total_Funds_Raised__c);       
      }
      
      if(agrschooldonation == Null && agradoption != Null && agrDisbursment != Null && agradjustment != Null || agrschooldonation == Null && agradoption != Null && agrDisbursment == Null && agradjustment == Null){
          c.Funds_Available__c =  (Decimal) agradoption.get('adoptionAmount');
          c.Total_Funds_Raised__c =  (Decimal) agradoption.get('adoptionAmount');
          system.debug('School and Teacher Donation - Adoption'+c.Funds_Available__c+'-'+c.Total_Funds_Raised__c);
      }
      
      if(agradoption != Null && agradjustment != NULL || agradjustment != NULL && agrschooldonation != Null || agrDisbursment != Null && agradjustment != NULL){
          c.Funds_Available__c +=  (Decimal) agradjustment.get('adoptionAmount');
          c.Total_Funds_Raised__c +=  (Decimal) agradjustment.get('adoptionAmount');
          system.debug('AdjustmentEvent'+c.Funds_Available__c+'-'+c.Total_Funds_Raised__c);
      }
      
      if(agrDisbursment != NULL || agradjustment == NULL && agrDisbursment != Null){
          c.Funds_Available__c +=  (Decimal) agrDisbursment.get('adoptionAmount');
          c.Total_Funds_Raised__c +=  (Decimal) agrDisbursment.get('adoptionAmount');
          system.debug('DisbursmentEvent'+c.Funds_Available__c+'-'+c.Total_Funds_Raised__c);
      }

      if(agrorder != Null && (agradoption != NUll || agrschooldonation != NUll || agrDisbursment !=NULL || agradjustment !=NULL))
      { 
          c.Funds_Available__c = (Decimal) agradoption.get('adoptionAmount') - ((agrorder.get('orderamount') != null)?(Decimal)agrorder.get('orderamount'):((agrDisbursment.get('adoptionAmount') != null)?(Decimal)agrDisbursment.get('adoptionAmount'):((Decimal)agrschooldonation.get('adoptionAmount') != null)?(Decimal)agrschooldonation.get('adoptionAmount'):(Decimal)agradjustment.get('adoptionAmount')));
          system.debug('************'+c.Funds_Available__c+'-'+c.Total_Funds_Raised__c);
      }
      
      if(agrorder == Null)//|| agrDisbursment == Null) && agradoption == Null)  
      {
          c.Funds_Available__c = 0;
      }
      if(c.Funds_Available__c  < 0)
      {
          c.Funds_Available__c  = 0;
      }
        /*  for(Contact cont : con){
              if(cont.funds_Available__c > 0){
                  c.Funds_Available__c += cont.funds_Available__c;
              }
              system.debug('************'+c.Funds_Available__c+'-'+c.Total_Funds_Raised__c);
          }*/
      if(con.size() > 0)
      {
          if(con[0].funds_Available__c > 0)
          {
              c.Funds_Available__c += con[0].funds_Available__c;
          }
          system.debug('************'+c.Funds_Available__c+'-'+c.Total_Funds_Raised__c);
      }
       contactMap.put(teacherId, c);
   //Ending for loop
      }
          system.debug('************contactMap'+contactMap);
    return contactMap.values();
  }

  private static Boolean processOpportunity(Opportunity no, Opportunity oo,
      Set<Id> recordTypeIds) {
    return opportunityMatches(no, recordTypeIds) && opportunityChanged(no, oo);
  }

  private static Boolean opportunityChanged(Opportunity no, Opportunity oo) {
  
    Map<String, RecordType> rtMap = recordTypeMap;
      
    if (oo == null) {
      return true;
    }
    else if((no.Teacher__c != oo.Teacher__c || no.Total_Amount__c != oo.Total_Amount__c || no.RecordTypeId != oo.RecordTypeId || no.StageName != oo.StageName) && no.RecordTypeId == rtMap.get('Orders').Id && no.StageName == STAGE_PAID) {
      //return ((no.Teacher__c != oo.Teacher__c || no.Total_Amount__c != oo.Total_Amount__c || no.RecordTypeId != oo.RecordTypeId || no.StageName != oo.StageName) && no.RecordTypeId == rtMap.get('Orders').Id && no.StageName == STAGE_PAID);
      return true;
    }
    else if((no.Teacher__c != oo.Teacher__c || no.amount!= oo.amount|| no.RecordTypeId != oo.RecordTypeId || no.StageName != oo.StageName) && (no.RecordTypeId == rtMap.get('adoption').Id || no.RecordTypeId == rtMap.get('Disbursement').Id || no.RecordTypeId == rtMap.get('schooldonation').Id) && no.StageName == STAGE_PAID) {
      //return ((no.Teacher__c != oo.Teacher__c || no.amount!= oo.amount|| no.RecordTypeId != oo.RecordTypeId || no.StageName != oo.StageName) && (no.RecordTypeId == rtMap.get('adoption').Id  || no.RecordTypeId == rtMap.get('Disbursement').Id)&& no.StageName == STAGE_PAID);
      return true;
    }
    else{
      return false;
    }
  }
  private static Boolean opportunityMatches(Opportunity o,
      Set<Id> recordTypeIds) {
    return recordTypeIds.contains(o.RecordTypeId) && o.StageName == STAGE_PAID;
  }

  public static Map<String, RecordType> getRecordTypeMap(
      Set<String> developerNames, String objectName) {
    Map<String, RecordType> rtMap = new Map<String, RecordType>();

    for (RecordType rt :
        [select Id,
                DeveloperName
         from RecordType
         where DeveloperName in :developerNames
         and SObjectType = :objectName]) {
      rtMap.put(rt.DeveloperName, rt);
    }

    return rtMap;
  }
  public static void UpdateRefund(List<Opportunity> nList, Map<Id, Opportunity> oldMap)
  {
      RecordType rt = [select Id,DeveloperName from RecordType where DeveloperName =: 'Refund'  and SObjectType = :'Opportunity'];
      set<Id> contactId = new set<Id>();
      if(nList.size() > 0)
      {
          for(Opportunity op : nlist)
          {
              if(oldmap != Null && oldmap.get(op.Id) != Null)
              {
                  if(oldmap.get(op.Id).Amount != op.Amount)
                  {
                      if(op.RecordTypeId == rt.Id)
                      {
                         contactId.add(op.teacher__c);
                      }
                  }
              }
              else
              {
                  if(op.RecordTypeId == rt.Id)
                  {
                     contactId.add(op.teacher__c);
                  }
              }
          }
      }
      Map<Id,contact> conlist = new map<Id,contact>([select Id,funds_Available__c from Contact where Id IN: contactId]);
      list<contact> conupdate = new list<contact>();
      for(Opportunity o : nlist)
      {
          if(conlist.containskey(o.teacher__c))
          {
              contact c = new contact();
              c.Id = o.Teacher__c;
              c.funds_Available__c = conlist.get(o.teacher__c).funds_Available__c + o.Amount;
              conupdate.add(c);
          }
      }
    if(conupdate.size() > 0)
      {
          update conupdate;
      }
  }
  
  public static void SendDonorEmail(Set<Id> IdSet)
  {
      if(IdSet == null || IdSet.size() ==0){
          return;
      }
      List<Opportunity> nList=[select id,name,Teacher__c,RecordTypeId,OrderCloudLineItemCount__c,OpportunityProductcount__c from opportunity where id IN:IdSet];
      //This method has been modified to handle bulk orders along with sending to stage of paid donors alone.
      set<Id> contactId = new set<Id>();
      Map<String, RecordType> recordMap = new Map<String, RecordType>();
      recordTypeMap = getRecordTypeMap(new Set<String>{RECORD_TYPE_ORDER, RECORD_TYPE_DONATION, RECORD_TYPE_SCHOOL, RECORD_TYPE_DISBURSMENT, RECORD_TYPE_DONATION_UNRESTRICTED, RECORD_TYPE_NAME_FIELD}, 'Opportunity');
      recordMap = recordTypeMap;
      map<Id,Opportunity> contactmap = new map<Id,Opportunity>();
      for(Opportunity op : nlist)
      {
          if(!op.name.containsIgnorecase('teacher credit card') && op.Teacher__c != Null && op.RecordTypeId == recordmap.get(RECORD_TYPE_ORDER).Id)
          {
              contactId.add(op.Teacher__c);
              contactmap.put(op.Teacher__c, op);
          }
      }
      
      if(contactId.size() ==0){
          return ;
      }
      Map<Id,contact> contactFundHistory=new Map<Id,Contact>([select id,Funds_Available__c,previous_balance__c from contact where Id IN:contactId]);
      
      List<Opportunity> opptylist = new list<Opportunity>(); 
      List<OpportunityContactRole> ocrlist = new list<OpportunityContactRole>();
      Map<Id,Set<String>> classRoomDonationsEmailMap = new Map<Id,Set<String>>();  
      //Added new record type called "Unrestricted donations" along with "Class room apdaption"
      opptylist = [select Id,name,Amount,teacher__c,npsp__Primary_Contact__r.npe01__HomeEmail__c,npsp__Primary_Contact__r.npe01__WorkEmail__c,npsp__Primary_Contact__r.email from Opportunity where Teacher__c IN:contactId and (RecordtypeId =: recordmap.get(RECORD_TYPE_DONATION_UNRESTRICTED).Id OR RecordtypeId =: recordmap.get(RECORD_TYPE_DONATION).Id OR RecordtypeId =: recordmap.get(RECORD_TYPE_SCHOOL).Id OR RecordtypeId =: recordmap.get(RECORD_TYPE_DISBURSMENT).Id) and StageName=:STAGE_PAID and (NOT name like 'Teacher Credit Card%') and Amount != null Order by Teacher__c, CreatedDate ASC]; //and Teacher__c IN:contactId  //Added Stage filter to only query paid donations
      Map<Id,List<Opportunity>> donationsMapOrdered=new Map<Id,List<Opportunity>>();
      
      for(Opportunity opty : opptylist)
      {
          if(!donationsMapOrdered.ContainsKey(opty.teacher__c)){
              donationsMapOrdered.put(opty.teacher__c,new List<Opportunity>());
          }    
          donationsMapOrdered.get(opty.teacher__c).add(opty);
      }
      if(donationsMapOrdered != null && donationsMapOrdered.keyset() != null && donationsMapOrdered.keyset().size() >0){
          for(Id teacherId:donationsMapOrdered.keyset()){
              if(donationsMapOrdered.get(teacherId) != null && contactFundHistory.ContainsKey(teacherId) && contactFundHistory.get(teacherId) != null){
                  Decimal previousBalance=(contactFundHistory.get(teacherId).previous_balance__c != null) ? contactFundHistory.get(teacherId).previous_balance__c : 0.00;
                  Decimal newBalance=(contactFundHistory.get(teacherId).Funds_Available__c != null) ? contactFundHistory.get(teacherId).Funds_Available__c : 0.00;
                  Decimal currentSum=0.00;
                  if(previousBalance == newBalance){
                      //continue;
                  }
                  Decimal totalDonation=0.00;
                  for(opportunity opp:donationsMapOrdered.get(teacherId)){
                      totalDonation+=opp.Amount;
                  }
                  
                  Decimal StartRange= (totalDonation-previousBalance);
                  Decimal EndRange= (totalDonation-newBalance);
                  
                  Map<Id,List<OpportunityRangeClass>> teacherDonars=new Map<Id,List<OpportunityRangeClass>>();
                  
                  for(opportunity opp1:donationsMapOrdered.get(teacherId)){
                      Decimal preBal=currentSum;
                      currentSum+=opp1.Amount;  
                      if((preBal < StartRange && currentSum > EndRange) || (preBal >= StartRange && preBal <= EndRange) || (currentSum >= StartRange && currentSum <= EndRange)){
                          if(!teacherDonars.containsKey(teacherId)){
                              teacherDonars.put(teacherId,new List<OpportunityRangeClass>());
                          }
                          OpportunityRangeClass o=new OpportunityRangeClass();
                          o.startDigit=preBal;
                          o.endDigit=currentSum;
                          o.opp=opp1;
                          
                          teacherDonars.get(teacherId).add(o);
                      }                
                  }
                  
                  if(teacherDonars.containsKey(teacherId) && teacherDonars.get(teacherId) != null){
                      if(teacherDonars.get(teacherId).size() > 1){
                          Boolean removeBeginning=false;
                          Boolean removeLast=false;
                          List<OpportunityRangeClass> oppListDonations=teacherDonars.get(teacherId);
                          if(oppListDonations[0].endDigit == oppListDonations[1].startDigit && StartRange == oppListDonations[0].endDigit){
                              removeBeginning=true;
                          }
                          if(oppListDonations.size() > 2 && oppListDonations[(oppListDonations.size()-2)].endDigit == oppListDonations[(oppListDonations.size()-1)].startDigit && oppListDonations[(oppListDonations.size()-1)].startDigit == EndRange){
                              removeLast=true;
                          }
                          if(removeLast){
                              oppListDonations.remove((oppListDonations.size()-1));
                          }
                          if(removeBeginning){
                              oppListDonations.remove(0);
                          }
                      }
                      
                      if(!classRoomDonationsEmailMap.containsKey(teacherId)){
                          classRoomDonationsEmailMap.put(teacherId,new Set<string>());
                      }                        
                          
                      for(OpportunityRangeClass ol:teacherDonars.get(teacherId)){
                          if(ol.opp.npsp__Primary_Contact__r.npe01__HomeEmail__c!= Null){
                              classRoomDonationsEmailMap.get(teacherId).add(ol.opp.npsp__Primary_Contact__r.npe01__HomeEmail__c);
                          }
                          if(ol.opp.npsp__Primary_Contact__r.npe01__WorkEmail__c != Null){
                              classRoomDonationsEmailMap.get(teacherId).add(ol.opp.npsp__Primary_Contact__r.npe01__WorkEmail__c);
                          }                    
                          if(ol.opp.npsp__Primary_Contact__r.email != Null){
                              classRoomDonationsEmailMap.get(teacherId).add(ol.opp.npsp__Primary_Contact__r.email);
                          } 
                      }
                  }              
              }              
              
          }
      }
      
      EmailTemplate  et = [Select e.Id,e.Name,e.subject,e.BrandTemplateId From EmailTemplate e where e.DeveloperName='AdoptAClassroom_org_Donor_Impact_Report' Limit 1];      
      List<orgwideEmailAddress> orgEmailAddressesList=[select id,Address,DisplayName from orgwideEmailAddress where DisplayName='AdoptAClassroom.org'];
      List<Messaging.SingleEmailMessage> mess = new List<Messaging.SingleEmailMessage>();
      List<Donor_emails__c> donorsEmailList=new List<Donor_emails__c>();
            
      for(Opportunity op : nlist)
      {          
          if(op.OrderCloudLineItemCount__c != NUll && op.OpportunityProductcount__c != Null)
          {
              if((op.OrderCloudLineItemCount__c == op.OpportunityProductCount__c) && classRoomDonationsEmailMap.containsKey(op.Teacher__c) && classRoomDonationsEmailMap.get(op.Teacher__c) != null && !classRoomDonationsEmailMap.get(op.Teacher__c).isEmpty())
              {
                    for(string v:classRoomDonationsEmailMap.get(op.Teacher__c)){
                        Donor_emails__c donorEmail=new Donor_emails__c();
                        donorEmail.name=op.id;
                        donorEmail.Donor_Email_Id__c=v;
                        donorsEmailList.add(donorEmail);
                    }
              }
          }
     }
     if(donorsEmailList.size()>0){
         insert donorsEmailList;
         List<string> donorEmailsSerialList=new List<String>();
         
         for(donor_emails__c d:donorsEmailList){
             donorEmailsSerialList.add(System.JSON.serialize(d));
         }    
         
         OrderManagementClass.deleteDonorEmailRecordsFuture(donorEmailsSerialList);     
     }
  }

    public static void UpdateSchoolDetails() {
        RecordType rt = [select Id
                        from RecordType
                        where SObjectType = 'Opportunity' 
                        and DeveloperName = 'Orders'];

        List<Opportunity> oppList = new List<Opportunity>();
        List<Id> contactIds = new List<Id>();

        for (Opportunity opp : (List<Opportunity>) trigger.new) {
            
            if (opp.RecordTypeId == rt.Id) {
                oppList.add(opp);
                
                if (!String.isBlank(opp.Teacher__c))
                    contactIds.add(opp.Teacher__c);
            }
        }
        Map<Id, Contact> contactMap = new Map<Id, Contact>([select  School__c,
                                                                    School__r.Name, 
                                                                    School__r.ShippingStreet, 
                                                                    School__r.ShippingCity, 
                                                                    School__r.ShippingCountry, 
                                                                    School__r.ShippingState, 
                                                                    School__r.ShippingPostalCode,
                                                                    School__r.SW_Title_1__c,
                                                                    School__r.Title_1__c 
                                                            from Contact 
                                                            where Id IN: contactIds]);
        for (Opportunity opportunity : oppList) {
            
            if (String.isBlank(opportunity.Teacher__c) || !contactMap.containskey(opportunity.Teacher__c) || String.isBlank(contactMap.get(opportunity.Teacher__c).School__c) || String.isBlank(contactMap.get(opportunity.Teacher__c).School__r.Name)) {
                opportunity.Order_School__c             = null;
                opportunity.Order_School_Street__c      = null;
                opportunity.Order_School_City__c        = null;
                opportunity.Order_School_Country__c     = null;
                opportunity.Order_School_State__c       = null;
                opportunity.Order_School_Zip__c         = null;
                opportunity.Order_School_SW_Title_1__c  = false;
                opportunity.Order_School_Title_1__c     = false;
            } else {
                opportunity.Order_School__c             = contactMap.get(opportunity.Teacher__c).School__r.Name;
                opportunity.Order_School_Street__c      = contactMap.get(opportunity.Teacher__c).School__r.ShippingStreet;
                opportunity.Order_School_City__c        = contactMap.get(opportunity.Teacher__c).School__r.ShippingCity;
                opportunity.Order_School_Country__c     = contactMap.get(opportunity.Teacher__c).School__r.ShippingCountry;
                opportunity.Order_School_State__c       = contactMap.get(opportunity.Teacher__c).School__r.ShippingState;
                opportunity.Order_School_Zip__c         = contactMap.get(opportunity.Teacher__c).School__r.ShippingPostalCode;
                opportunity.Order_School_SW_Title_1__c  = contactMap.get(opportunity.Teacher__c).School__r.SW_Title_1__c;
                opportunity.Order_School_Title_1__c     = contactMap.get(opportunity.Teacher__c).School__r.Title_1__c;
            }
        }
    }
  
  public class OpportunityRangeClass{
      public decimal startDigit;
      public decimal endDigit;
      public opportunity opp;
  }
 
}