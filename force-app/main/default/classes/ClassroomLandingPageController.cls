public class ClassroomLandingPageController {

	@AuraEnabled
	public static data getData(Id recordId) {
		return ClassroomUtils.getData(recordId);
	}

    @AuraEnabled
	public static void saveNotify(String notifyStr) {
        try {
            notify notify = (notify)System.JSON.deserialize(notifyStr, ClassroomLandingPageController.notify.class);
            ClassroomUtils.saveNotify(notify);
        } catch(Exception ex){
            throw new AuraHandledException(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') ? ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')[1].split(':')[0] : 'Error: ' + ex.getMessage());
        }
	}

    @AuraEnabled 
    public static user fetchUser(){
        User oUser = [select Email,FirstName,LastName FROM User Where id =: userInfo.getUserId()];
        return oUser;
    }

	public class data {
        
		@AuraEnabled public donation_split__Designation__c Classroom { get; set; }
        @AuraEnabled public User Teacher { get; set; }
        @AuraEnabled public List<Opportunity> Donations { get; set; }
        @AuraEnabled public String bannerURL { get; set; }

        public data(donation_split__Designation__c pClassroom, User pTeacher, List<Opportunity> pDonations, String pbannerURL) {
			Classroom = pClassroom;
            Teacher = pTeacher;
            Donations = pDonations;
            bannerURL = pbannerURL;
        }
    }

    public class notify {
        
		@AuraEnabled public String FirstName { get; set; }
        @AuraEnabled public String LastName { get; set; }
        @AuraEnabled public String Email { get; set; }
        @AuraEnabled public Boolean MarketingEmail { get; set; }
        @AuraEnabled public String Classroom { get; set; }
        @AuraEnabled public String Teacher { get; set; }

        public notify(String pFirstName,String pLastName, String pEmail, Boolean pMarketingEmail, String pClassroom, String pTeacher) {
			FirstName = pFirstName;
            LastName = pLastName;
            Email = pEmail;
            MarketingEmail = pMarketingEmail;
            Classroom = pClassroom;
            Teacher = pTeacher;
        }
    }
}