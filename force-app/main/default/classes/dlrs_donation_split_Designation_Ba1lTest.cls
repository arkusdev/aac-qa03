/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_donation_split_Designation_Ba1lTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_donation_split_Designatioa1lTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new donation_split__Designation_Budget__c());
    }
}