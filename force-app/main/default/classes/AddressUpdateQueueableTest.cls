@istest(seeAllData = true)
private class AddressUpdateQueueableTest {

     static testMethod void testAddressUpdateQueueable() {
     
    Account school = TestUtils.createAccount();
     Contact c = TestUtils.createContact(school.Id);
    
    school.ShippingStreet = '123 Main Street';
    school.ShippingCity = 'Minneapolis';
    school.ShippingStateCode = 'MN';
    school.ShippingCountryCode = 'US';
    school.ShippingPostalCode = '12345';
    insert school;

   
    c.School__c = school.Id;
    c.School__r = school;
    insert c;     

    
    Test.startTest();

    

    ShopAddress address = new ShopAddress(ShopAddress.AddressType.Shipping, c);
    
    Map<Id,Id> userId = new Map<Id,Id>();
    userId.put(c.Id,userinfo.getUserId());
    
     AddressUpdateQueueable auq = new AddressUpdateQueueable(new List<Contact>{c},userId);
    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());
     System.enqueueJob(auq);


    Test.stopTest(); 
     
    
     }
     static testMethod void AddressUpdateQueueabletestwithId() {
     
    Account school = TestUtils.createAccount();
    Contact c = TestUtils.createContact(school.Id);
    
    school.ShippingStreet = '123 Main Street';
    school.ShippingCity = 'Minneapolis';
    school.ShippingStateCode = 'MN';
    school.ShippingCountryCode = 'US';
    school.ShippingPostalCode = '12345';
    insert school;
   
    c.School__c = school.Id;
    c.School__r = school;
    c.Billing_Address_Id__c = 'TestId';
    c.Mailing_Address_Id__c = 'testId';
    insert c; 
     
    Test.startTest();    

    ShopAddress address = new ShopAddress(ShopAddress.AddressType.Billing, c);
    
    Map<Id,Id> userId = new Map<Id,Id>();
    userId.put(c.Id,userinfo.getUserId());
    
    AddressUpdateQueueable auq = new AddressUpdateQueueable(new List<Contact>{c},userId);
    Test.setMock(HttpCalloutMock.class, new ShopCalloutMock());
    System.enqueueJob(auq);
    
    Test.stopTest(); 
     
    
     }
}