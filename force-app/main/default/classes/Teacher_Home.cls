public with sharing class Teacher_Home {
    
    @AuraEnabled
    public static data getData(){
        try {
            Boolean hasSchool = false;
            User usr = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];
            List<donation_split__Designation__c> classrooms = [SELECT School__c FROM donation_split__Designation__c WHERE Teacher__c = :usr.ContactId];
            if(!classrooms.isEmpty() && classrooms[0].School__c != null) {
                hasSchool = true;
            }

            Teacher_Home.data data = new Teacher_Home.data(hasSchool);

            return data;

        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') ? ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')[1].split(':')[0] : ex.getMessage());
        }
    }

    public class data { 
        @AuraEnabled public Boolean hasSchool { get; set; }

        public data (Boolean pHasSchool) {
            hasSchool = pHasSchool;
        }
    }
}