@isTest
public with sharing class OrderManagementCalloutMock implements HttpCalloutMock{
  public HttpResponse respond(HttpRequest request) {
    HttpResponse response = new HttpResponse();
    response.setStatusCode(200);
    
    if(request.getEndpoint().contains('payments')) {  
      ShopUtils.paymentDetails pd = new ShopUtils.paymentDetails();
      pd.orderId = request.getEndpoint().substringBetween('incoming/', '/payments');
      pd.Error = null;
    
      ShopUtils.paymentItem pi = new ShopUtils.paymentItem();
      pi.ID = 'paymentItemId';
      pi.Type = 'CreditCard';
      pi.DateCreated = string.valueOf(system.today());
      pi.CreditCardID = '4111111111111111';
      pi.SpendingAccountID = 'Test Account';
      pi.Description = 'Test Description';
      pi.Amount = '115';
      pi.Accepted = true;
      pi.xp = '25';  
      
      ShopUtils.transactionDetail td = new ShopUtils.transactionDetail();
      td.ID = 'transactionId';
      td.Type = 'CreditCard';
      td.DateExecuted = string.valueOf(system.today());
      td.Amount = '115';
      td.Succeeded = true;
      td.ResultCode = '200';
      td.ResultMessage = 'Approved';
      td.xp = new ShopUtils.transac();
      td.xp.TransID = '4567890';  
      
      pi.Transactions = new list<ShopUtils.transactionDetail>{td};  
      pd.Items = new list<ShopUtils.paymentItem>{pi}; 
      response.setStatusCode(200);    
      response.setBody(JSON.serialize(pd));
    }      
    else if(request.getEndpoint().contains('oauth')) {  
      ShopUtils.TokenResponse tr = new ShopUtils.TokenResponse();
      tr.access_token = 'dummy token';
      response.setStatusCode(200);  
      response.setBody(JSON.serialize(tr));
    }      
    return response;
  }
}