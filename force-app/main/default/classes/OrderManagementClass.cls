public class OrderManagementClass{
    
    private static Map<String, RecordType> getRecordTypeMap(
        Set<String> developerNames, String objectName) {
        Map<String, RecordType> rtMap = new Map<String, RecordType>();
    
        for(RecordType rt : [select Id, DeveloperName from RecordType where DeveloperName in :developerNames
                             and SObjectType = :objectName]) {
          rtMap.put(rt.DeveloperName, rt);
        }
    
        return rtMap;
    }
  
    @future(callout=true)
    public static void processOrderPayments (Map<Id,String> opportunityOrderMap){
        List<ShopUtils.paymentDetails> payments=new List<ShopUtils.paymentDetails>();
        Map<String, RecordType> recordTypeMap = getRecordTypeMap(new set<String>{'Ecommerce_Credit_Card'},'Opportunity'); 
        Map<String,Opportunity> ordersMap=new Map<String,Opportunity>();        
        List<Opportunity> orderTranscations=new List<Opportunity>();
        Opportunity newCredit;
        Boolean transactionSuccess=false;
        Decimal totalAmount=0.00;
        String transactionId;
        
        Map<string,Id> glId = new Map<string,Id>();
        list<AcctSeed__GL_Account__c> asga = new list<AcctSeed__GL_Account__c>([select Id,Name from AcctSeed__GL_Account__c where Name='1040 Bremer Checking - Restricted' OR Name = '4650 Teacher Credit Card']);
        list<AcctSeed__Accounting_Variable__c> avarlist = new list<AcctSeed__Accounting_Variable__c>([select Id,Name,AcctSeed__Active__c,AcctSeed__Type__c from AcctSeed__Accounting_Variable__c where AcctSeed__Active__c = True]);
        for(AcctSeed__GL_Account__c asgl : asga)
        {
            glId.put(asgl.Name,asgl.Id);
        }
        Map<string,AcctSeed__Accounting_Variable__c> GLAcctMap = new map<string,AcctSeed__Accounting_Variable__c>();
        for(AcctSeed__Accounting_Variable__c agacc : avarlist)
        {
            GLAcctMap.put(agacc.Name,agacc);
        }
    
        if(opportunityOrderMap != null && opportunityOrderMap.keyset() != null){
            for(Id o:opportunityOrderMap.keyset()){
                if(opportunityOrderMap.get(o) != null){
                    payments.add(ShopUtils.getOrderPayments(opportunityOrderMap.get(o)));
                    ordersMap.put(opportunityOrderMap.get(o),null);
                }
            }
            
            if(ordersMap.keyset() != null){
                for(opportunity ord:[select id,Order_Cloud_Order_Id__c,Teacher__c,Teacher__r.name,Teacher__r.school__c,npsp__Primary_Contact__c,Amount,StageName,AccountId,School__c,RecordTypeId from opportunity where Order_Cloud_Order_Id__c IN:ordersMap.keyset()]){
                    ordersMap.put(ord.Order_Cloud_Order_Id__c,ord); 
                }
            }
            
            if(!payments.isEmpty()){
                for(ShopUtils.paymentDetails p:payments){
                    if(p.orderId != null && p.Error == null && p.Items != null && p.Items.size() >0){
                        transactionSuccess=false; 
                        totalAmount=0.00;   
                        transactionId='';                                                     
                        for(ShopUtils.paymentItem pItem:p.Items){
                            if(pItem.Type =='CreditCard' && pItem.CreditCardID != null && pItem.Amount != null && Decimal.Valueof(pItem.Amount) != 0 && pItem.Accepted ==true){
                                totalAmount=+Decimal.Valueof(pItem.Amount);
                                if(pItem.Transactions != null && pItem.Transactions.size() >0){
                                    for(ShopUtils.transactionDetail t:pItem.Transactions){
                                        if(t.Succeeded == true && t.ResultMessage=='Approved' && t.xp != null && t.xp.TransID != null){
                                            transactionSuccess=true;
                                            transactionId=t.xp.TransID;
                                        }
                                        else{
                                            transactionSuccess=false;
                                        }
                                    }
                                }
                                
                            }
                        }    
                        
                        if(transactionSuccess && ordersMap.containsKey(p.orderId)){
                            newCredit=new Opportunity();
                            newCredit.name= 'Teacher Credit Card - '+ordersMap.get(p.orderId).Teacher__r.name;
                            newCredit.npsp__Primary_Contact__c=ordersMap.get(p.orderId).npsp__Primary_Contact__c;
                            newCredit.AccountId=ordersMap.get(p.orderId).AccountId;
                            newCredit.Teacher__c=ordersMap.get(p.orderId).Teacher__c;
                            newCredit.Amount=totalAmount;
                            newCredit.Subtotal__c =totalAmount;
                            newCredit.TaxCost__c =0;
                            newCredit.ShippingCost__c =0;
                            newCredit.PromotionDiscount__c=0;
                            newCredit.StageName='Sale Closed / Paid';
                            newCredit.RecordTypeId=recordTypeMap.get('Ecommerce_Credit_Card').Id; 
                            newCredit.CloseDate = Date.today();
                            orderTranscations.add(newCredit);  
                            
                        }                    
                    }                 
                }
            }
        }
        
        if(!orderTranscations.isEmpty()){
            insert orderTranscations;
        }
    }
    
    public static void deleteDonorEmailRecordsFuture(List<String> serialDonorEmailsString){
        List<donor_emails__c> donorEmailRecords=new List<donor_emails__c>();
        
        if(serialDonorEmailsString != null && serialDonorEmailsString .size() >0){
            for(String s:serialDonorEmailsString){
                donorEmailRecords.add((donor_emails__c)System.JSON.deserialize(s,donor_emails__c.class));
            }
            
            if(!donorEmailRecords.isEmpty()){
                delete donorEmailRecords;
            }
        }    
    }
}