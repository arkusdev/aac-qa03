@isTest
private class DonorDetailsControllerTest {

    @isTest static void addDonor() {
    
    Campaign camp = new Campaign();
    camp.Name = 'AdoptAClassroom.org Annual Fund';
    insert camp;
    
     list<AcctSeed__Accounting_Variable__c> acvlist = new list<AcctSeed__Accounting_Variable__c>();
    
    AcctSeed__Accounting_Variable__c  acv1 = new AcctSeed__Accounting_Variable__c();
    acv1.AcctSeed__Type__c = 'GL Account Variable 1';
    acv1.AcctSeed__Active__c = true;
    acv1.Name = 'Unrestricted';
    
    AcctSeed__Accounting_Variable__c  acv2 = new AcctSeed__Accounting_Variable__c();
    acv2.AcctSeed__Type__c = 'GL Account Variable 2';
    acv2.AcctSeed__Active__c = true;
    acv2.Name = 'Programs';
    
    AcctSeed__Accounting_Variable__c  acv3 = new AcctSeed__Accounting_Variable__c();
    acv3.AcctSeed__Type__c = 'GL Account Variable 3';
    acv3.AcctSeed__Active__c = true;
    acv3.Name = 'Classroom Adoption';
    
    AcctSeed__Accounting_Variable__c  acv4 = new AcctSeed__Accounting_Variable__c();
    acv4.AcctSeed__Type__c = 'GL Account Variable 1';
    acv4.AcctSeed__Active__c = true;
    acv4.Name = 'Restricted';
    
    acvlist.add(acv1);
    acvlist.add(acv2);
    acvlist.add(acv3);
    acvlist.add(acv4);
    
    insert acvlist;
    
    AcctSeed__GL_Account__c asgl = new  AcctSeed__GL_Account__c();
     asgl.Name = '4599 Teacher Restricted Released';
     asgl.AcctSeed__Active__c = true;
     asgl.AcctSeed__Type__c = 'Revenue';
     Insert asgl;
     
    Contact c = null;
    Account a = null;
    pymt__PaymentX__c payment;
    donation_split__Designation__c classRoom;

    // null tests
    DonorDetailsController.upsertContact(c);
    DonorDetailsController.upsertAccount(a, '');

    c = [select Id, FirstName, Lastname, Email from Contact limit 1];
    a = [select Id, Name, Email__c from Account limit 1];

    String emailTest = 'emailtest@test.com';

    c.Email = emailTest;
    DonorDetailsController.upsertContact(c);
    System.assertEquals(c.Email, emailTest);

    a.Email__c = emailTest;
    DonorDetailsController.upsertAccount(a, 'Business');
    System.assertEquals(a.Email__c, emailTest);

    List<FAQ__c> faqList = DonorDetailsController.getFAQs();

    String inHonorOfName = 'Name Honor';
    payment = TestUtils.createPayment();

    classRoom = [select Id from donation_split__Designation__c limit 1];

    String cartItems =
      '[{"classroomId":"' + classRoom.Id + '","frequency":"One time","amount":40},{"classroomId":"' + classRoom.Id + '","frequency":"One time","amount":25}]';

    DonorDetailsController.savePaymentData(c, a, payment, cartItems, inHonorOfName, 20.00, 30.00, 10.00, 10.00, 5.00, 50.00);

    }

  @testSetup static void initTestData() {

    Id recordTypeId = TestUtils.getRecordTypeIdByDeveloperName(
      Opportunity.SObjectType.getDescribe().getName(), 'Donation');
    
    
    npe03__Recurring_Donations_Settings__c settings =
      TestUtils.createRecurringDonationSettings(recordTypeId);
    insert settings;

    Account a = TestUtils.createAccount();
    insert a;

    Contact c = TestUtils.createContact(a.id);
    insert c;

    donation_split__Designation__c classroom = TestUtils.createClassroom(c.Id, a.Id);
    insert classroom;
    }

    @isTest static void TestCookiesSet() {
      Test.setCurrentPageReference(new PageReference('Chargent_Cartpage.page')); 
      //System.currentPageReference().getParameters().put('id', idVariable);
      boolean result = DonorDetailsController.cookiesSet();
    }
}