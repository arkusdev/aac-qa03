public with sharing class UserProfileController {

  @AuraEnabled
  public static Contact getContactForUser(Id userId,
      List<String> fieldSetNames) {
    if (String.isBlank(userId)) {
      return null;
    }

    Id contactId;
    User u = [select Id,
                     ContactId
              from User
              where Id = :userId];

    contactId = u.ContactId;

    if (String.isBlank(contactId)) {
      return null;
    }

    return (Contact) QueryUtils.queryObjectFromFieldSets(
      contactId, fieldSetNames);
  }
  
  @AuraEnabled
  public static void updateContact(Contact c) {
    System.debug(c);
    if (c == null) {
      return;
    }

    update c;
  }
}