@isTest
private class GoogleMapControllerTest {
	
  static testMethod void testGoogleMapController() {
    Account a = TestUtils.createAccount();
    insert a;

    Test.startTest();

    System.assertEquals(null, GoogleMapController.queryRecord(null));

    System.assertNotEquals(null, GoogleMapController.queryRecord(a.Id));

    Test.stopTest();
  }
}