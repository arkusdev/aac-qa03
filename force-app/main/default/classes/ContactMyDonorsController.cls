global without sharing class ContactMyDonorsController {
    //// CONSTANTS ////
    @TestVisible
    private static final String RECORD_TYPE_DONATION = 'adoption';
    @TestVisible
    private static final String RECORD_TYPE_SCHOOL = 'schooldonation';
    @TestVisible
    private static final String RECORD_TYPE_DISBURSMENT = 'Disbursement';


    private static Map<String, RecordType> recordTypeMap = new Map<String, RecordType>();
    public String error { get;set; }
    public List<donorWrapper> dwList {get;set;}

    public ContactMyDonorsController() {
        try{
            recordTypeMap = getRecordTypeMap(new Set<String>{RECORD_TYPE_DONATION, RECORD_TYPE_SCHOOL, RECORD_TYPE_DISBURSMENT}, 'Opportunity');
            User RunningUser=[SELECT Id, contactid, email, profile.id FROM user WHERE Id =:Userinfo.getUserId() LIMIT 1];
            List<Contact> teacher=[SELECT Id, Name, recordTypeId FROM contact WHERE Id =:RunningUser.contactId];

            if(teacher != null && !teacher.isEmpty()){
                List<Opportunity> opportunities =(List<Opportunity>)[SELECT Id, Anonymous__c, createdDate,npsp__Primary_Contact__c, npsp__Primary_Contact__r.Name, npsp__Primary_Contact__r.npe01__Preferred_Email__c, npsp__Primary_Contact__r.npe01__HomeEmail__c, npsp__Primary_Contact__r.npe01__WorkEmail__c, npsp__Primary_Contact__r.email, npsp__Primary_Contact__r.npe01__AlternateEmail__c, npsp__Primary_Contact__r.DonorDisplayName__c, account.Name, npsp__Primary_Contact__r.HasOptedOutOfEmail FROM Opportunity WHERE StageName = 'Sale Closed / Paid' AND teacher__c =:teacher[0].id and (recordTypeId =:recordTypeMap.get(RECORD_TYPE_DONATION).Id OR recordTypeId =:recordTypeMap.get(RECORD_TYPE_SCHOOL).Id OR recordTypeId =:recordTypeMap.get(RECORD_TYPE_DISBURSMENT).Id) ORDER BY createdDate DESC];

                dwList = new List<donorWrapper>();
                for(Opportunity op : opportunities){
                    String donorName = op.npsp__Primary_Contact__r.Name;
                    String affiliationName = op.account.Name;
                    
                    if(op.npsp__Primary_Contact__r.DonorDisplayName__c != null && op.npsp__Primary_Contact__r.DonorDisplayName__c != ''){
                        donorName = op.npsp__Primary_Contact__r.DonorDisplayName__c;
                    }
                    if(op.Anonymous__c){
                        donorName = 'Anonymous';
                        affiliationName = '';
                    }
                    String donorEmail = op.npsp__Primary_Contact__r.email;
                    if((donorEmail == NULL || donorEmail == '') && op.npsp__Primary_Contact__r.npe01__Preferred_Email__c != NULL){
                        switch on op.npsp__Primary_Contact__r.npe01__Preferred_Email__c {
                            when 'Home' {       
                                donorEmail = op.npsp__Primary_Contact__r.npe01__HomeEmail__c;
                            }   
                            when 'Work' {
                                donorEmail = op.npsp__Primary_Contact__r.npe01__WorkEmail__c;
                            }
                            when 'Alternate' {
                                donorEmail = op.npsp__Primary_Contact__r.npe01__AlternateEmail__c;
                            }
                        }
                    }
                    Boolean isShowMessageDisplayed = true;
                    if(op.npsp__Primary_Contact__r.HasOptedOutOfEmail || donorEmail == NULL || donorEmail == ''){
                        isShowMessageDisplayed = false;
                    }
                    donorWrapper opw = new donorWrapper(donorName, affiliationName, op.Id, isShowMessageDisplayed, op.npsp__Primary_Contact__c);
                    dwList.add(opw);
                    }
          }

          error = 'No error';  

          }catch(exception e) {
            error = e.getMessage() + ' ' + e.getLineNumber();            
        }
    }

    @RemoteAction
    global static void sendEmail(String messageObject){
        emailWrapper ew = (emailWrapper)JSON.deserialize(messageObject, emailWrapper.class);
        System.debug(ew);
        User commUser = [SELECT Id, Contact.Name FROM User WHERE Id = :UserInfo.getUSerId()];
        Contact donor = [SELECT Id, Email, npe01__Preferred_Email__c, npe01__HomeEmail__c, npe01__WorkEmail__c, npe01__AlternateEmail__c FROM Contact WHERE Id =: ew.idDonor LIMIT 1];
        String donorEmail = donor.Email;
        if((donorEmail == NULL || donorEmail == '') && donor.npe01__Preferred_Email__c != NULL){
            switch on donor.npe01__Preferred_Email__c {
                when 'Home' {       
                    donorEmail = donor.npe01__HomeEmail__c;
                }   
                when 'Work' {
                    donorEmail = donor.npe01__WorkEmail__c;
                }
                when 'Alternate' {
                    donorEmail = donor.npe01__AlternateEmail__c;
                }
            }
        }

        ew.message+='<br/><br/><br/>This email was sent to you by ' + commUser.Contact.Name + ' using AdoptAClassroom.org. To stop receiving these kinds of messages, contact us at info@adoptaclassroom.org.<br/>';
        List<orgwideEmailAddress> orgEmailAddressesList=[SELECT Id,Address,DisplayName FROM orgwideEmailAddress WHERE DisplayName='AdoptAClassroom.org'];
        List<Messaging.SingleEmailMessage> mess = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage smail = new Messaging.SingleEmailMessage();
        String[] ccAddress = new String[] {'donorsupport@adoptaclassroom.org'};             
        smail.setToAddresses(new List<string>{donorEmail});
        smail.setSaveAsActivity(false); 
        smail.setUseSignature(false);
        smail.setHTMLBody(ew.message);
        smail.setSubject('AdoptAClassroom.org - Message from ' + commUser.Contact.Name);
        if(orgEmailAddressesList != null && !orgEmailAddressesList.isEmpty()){
            smail.setOrgWideEmailAddressId(orgEmailAddressesList[0].Id);  
        }
        mess.add(smail); 
        System.debug(mess);
        if(mess.size() > 0)
        {
            try{
                Messaging.sendEmail(mess);
            }
            Catch(Exception e){
                System.debug('ERROR');
            }
        }

    }
    @TestVisible
    private static Map<String, RecordType> getRecordTypeMap(Set<String> developerNames, String objectName) {
        Map<String, RecordType> rtMap = new Map<String, RecordType>();
        
        for (RecordType rt :
            [SELECT Id,
            DeveloperName
            FROM RecordType
            WHERE DeveloperName IN :developerNames
            AND SObjectType = :objectName]) {

            rtMap.put(rt.DeveloperName, rt);
        }

        return rtMap;

    }

    private class donorWrapper{

        public String  name                     {get;set;}
        public String  affiliationName           {get;set;}
        public String  idOpp                    {get;set;}
        public Boolean isShowMessageDisplayed   {get;set;}
        public String  idDonor                  {get;set;}

        donorWrapper(String name, String affiliationName, String idOpp, Boolean isShowMessageDisplayed, String idDonor){
            this.name = name;
            this.affiliationName = affiliationName;
            this.idOpp = idOpp;
            this.isShowMessageDisplayed = isShowMessageDisplayed;
            this.idDonor = idDonor;
        }
    }
    @TestVisible
    public class emailWrapper{
        public String message   {get;set;}
        public String nameDonor {get;set;}
        public String idDonor   {get;set;}
        @TestVisible
        emailWrapper(String message, String nameDonor, String idDonor){
            this.message = message;
            this.nameDonor = nameDonor;
            this.idDonor = idDonor;
        }
    }
}