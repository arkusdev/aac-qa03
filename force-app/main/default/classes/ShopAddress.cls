public with sharing class ShopAddress {

  private static final String OTHER_ADDRESS_NAME = 'Other Address';
  private static final String MAILING_ADDRESS_NAME = 'Mailing Address';
  private static final String ADDRESS_NEW_LINE = '\r\n';

  public enum AddressType { Shipping, Billing }

  public String ID { get; set; }
  public String FirstName { get; set; }
  public String LastName { get; set; }
  public String Street1 { get; set; }
  public String Street2 { get; set; }
  public String City { get; set; }
  public String State { get; set; }
  public String Zip { get; set; }
  public String Country { get; set; }
  public String Phone { get; set; }
  public String AddressName { get; set; }
  public String CompanyName { get; set; }
  public dtaxcost xp{get;set;}

  private AddressType type;
    public ShopAddress()
    {
         xp = new dTaxcost();
    }
    public ShopAddress(AddressType type, Contact c) {
    system.debug('Inside Shop Address');
    system.debug('DDDDDDDDDDDDDDD'+c);
    system.debug('DDDDDDDDDDDDDDD'+c.school__c);
    if (c == null || c.School__c == null) {
      return;
    }
    
    this.FirstName = c.FirstName;
    this.LastName = c.LastName;
    this.Phone = c.Phone;
    this.type = type;
    this.CompanyName = c.School__r.Name;
    system.debug('&&&&&&&&&&&&&&&&&&&&&&&&'+this);
        if (type == AddressType.Shipping) {
        system.debug('Inside shipping Address if');
        Account a = [select Id,Name,ShippingCity,ShippingStateCode,ShippingState,ShippingCountry,ShippingPostalCode,ShippingCountryCode,ShippingStreet from Account where Id =:c.School__c];
        system.debug('FFFFFFFFFFFFFFFFFFF'+a.ShippingCountry);
        system.debug('FFFFFFFFFFFFFFFFFFF'+a.Shippingstate);
        list<Teacher_Sales_Tax__c> tst = new List<Teacher_Sales_Tax__c>();
        tst = [select Id,Country__c,States__c,Tax_Percentage__c from Teacher_sales_tax__c where Country__c =:a.ShippingCountry and States__c =: a.Shippingstate limit 1 ];
        
      setShippingAddress(a,tst);
    } else if (type == AddressType.Billing) {
        Organization o = [select o.Name,o.PostalCode,o.Street,o.city,o.StateCode,o.countrycode from Organization o limit 1];
      setBillingAddress(o);
    }
    }

  public AddressType getType() {
    return this.type;
  }

  public Boolean isComplete() {
    return String.isNotBlank(this.FirstName) && String.isNotBlank(this.LastName)
      && String.isNotBlank(this.Street1) && String.isNotBlank(this.City)
        && String.isNotBlank(this.State) && String.isNotBlank(this.Country);
  }

  private List<String> getSplitStreet(String street) {
    return street.split(ADDRESS_NEW_LINE);
  }

  private void setBillingAddress(Organization a) {
    List<String> splitStreet;
    this.Street1 = a.Name;
    this.City = a.city;
    this.State = a.StateCode;
    this.Zip = a.PostalCode;
    this.Country = a.countrycode;
    this.AddressName = OTHER_ADDRESS_NAME;
    this.street2 = a.Street;
    
    if (String.isBlank(a.street)) {
      return;
    }

  /*  splitStreet = getSplitStreet(a.BillingStreet);

    this.Street1 = splitStreet[0];

    if (splitStreet.size() > 1) {
      this.Street2 = splitStreet[1];
    } */
  }

  private void setShippingAddress(Account a,list<Teacher_Sales_Tax__c> tst) {
   system.debug('VVVVVVVInside Shipping address functionVVVVVVVVVVV');

    List<String> splitStreet;
    xp = new dTaxcost();
    if(tst.size() <= 0)
    {
        this.xp.TaxCost = 0;
    }
    else
    {
        system.debug('GGGGGGGGGGGGGGGGGGGGGGG'+tst[0].Tax_Percentage__c);
        this.xp.TaxCost = tst[0].Tax_Percentage__c/100;
    }
    if(a.shippingCity != Null)
    {
        this.City = a.ShippingCity;
        system.debug('GGGGGGGGGGGGGGGGGGGGGGGGG'+a.shippingCity);
    }
        if(a.ShippingPostalCode != Null)
    {
        this.zip = a.ShippingPostalCode;
        system.debug('GGGGGGGGGGGGGGGGGGGGGGGGG'+a.ShippingPostalCode);
    }
        if(a.ShippingCountryCode != Null)
    {
        this.Country = a.ShippingCountryCode;
        system.debug('GGGGGGGGGGGGGGGGGGGGGGGGG'+a.ShippingCountryCode);
    }
        if(a.ShippingStateCode != Null)
    {
        this.State = a.ShippingStateCode;
        system.debug('GGGGGGGGGGGGGGGGGGGGGGGGG'+a.ShippingStateCode);
    }
    
    
    /*
    
    
    this.State = a.ShippingStateCode;
    this.Zip = a.ShippingPostalCode; 
    this.Country = a.ShippingCountryCode; */
    this.AddressName = MAILING_ADDRESS_NAME;

    if (String.isBlank(a.ShippingStreet)) {
      return;
    }
    else
    {
    splitStreet = getSplitStreet(a.ShippingStreet);

    this.Street1 = splitStreet[0];

    if (splitStreet.size() > 1) {
      this.Street2 = splitStreet[1];
    }
    }
    system.debug('FFFFFFFFFFFFFFFFF'+this);
  }
  public class dtaxCost
  {
      public decimal Taxcost {get;set;}   
  }
}