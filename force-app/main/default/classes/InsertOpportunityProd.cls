global with sharing class InsertOpportunityProd implements Database.Batchable<Sobject>, Database.Stateful{
global Id pbid;
global list<Opportunity> opptylist = new list<opportunity>();
global Opportunity opty = new Opportunity();
global Product2 prod = new Product2(); 
global list<OrderCloudLineItem> ocli = new list<OrderCloudLineItem>();
global Order_cloud_sync__c ocs = new order_cloud_sync__c();
global opportunity opportunity;

global InsertOpportunityProd(Order_cloud_sync__c ocsc,list<OrderCloudLineItem> lineitemlist){
    ocs = ocsc;
    ocli.addall(lineItemlist);
}
 global Database.QueryLocator start(Database.BatchableContext BC){
      string query = 'select Id,Order_id__c from Order_Cloud_Sync__c where Order_id__c =\'' +ocs.Order_id__c+ '\'';
      system.debug('GGGGGGGGGGGGGGGGGGGGGGGG'+query);
   //  opptylist = [select Id from Opportunity where Id=:opty.Id];
      return Database.getQueryLocator(query); 
 }
 global void execute(Database.BatchableContext BC, List<Order_Cloud_Sync__c> scope){
     system.debug('XXXXXXXXXXXXXXXXXXXXXXX'+scope.size());
     Shop_Settings__c shopSettings = Shop_Settings__c.getInstance();
     pbid = shopSettings.Pricebook_Id__c;
     system.debug('AAAAAAAAAAAAAAAAAAA'+pbid);
     list<Pricebook2> pb = new list<Pricebook2>([select Id from Pricebook2 where id =: pbid limit 1]);
     system.debug('PB SIZE'+pb);
     set<Product2> prodlist = new set<Product2>();
     
     Map<Id,product2> oldprodlist = new Map<Id,product2>();
     list<pricebookEntry> oldpbelist = new list<Pricebookentry>();
     AcctSeed__GL_Account__c glAccount = [SELECT Id, Name FROM AcctSeed__GL_Account__c WHERE Name ='6000 In Network purchases'];
        Map<String, AcctSeed__Accounting_Variable__c> accountingvariableMap = new Map<String, AcctSeed__Accounting_Variable__c>();
        List<String> accountingVariableNameList = new List<String>{'Unrestricted', 'Programs', 'Classroom Adoption'};
        
        for (AcctSeed__Accounting_Variable__c accountingVariable : [SELECT Id, Name FROM AcctSeed__Accounting_Variable__c WHERE Name IN :accountingVariableNameList]) {
        accountingvariableMap.put(accountingVariable.Name, accountingVariable);
        }
     for(ordercloudLineItem oline: ocli){
     string lineitemstring =  JSON.serialize(oline);
     JSONParser parser = JSON.createParser(lineItemString);      
      Id pricebookEntryId;    
    OrderCloudLineItem lineItem1 = (OrderCloudLineItem) parser.readValueAs(OrderCloudLineItem.class);
    opportunity = ShopUtils.getOpportunityByCloudId(scope[0].Order_id__c);
    system.debug('XXXXXXXXXXXXXXXXXXX'+lineitem1.ProductID);
    Product2 p;
    
    if(lineItem1.ProductID == 'AACPunchoutProduct'){
        system.debug('GGGGGGGGGGGGGGGGGGGG'+lineItem1.ProductId);
        p = ShopUtils.getProductByOrderCloudId(lineitem1.xp.SupplierPartID,lineitem1.xp.PunchoutName); //Brighthat - fixing product identification logic to check vendor name along with order cloud id
        system.debug('GGGGGGGGGGGGGGGGGGGG'+p);
    }
    else{
         system.debug('GGGGGGGGGGGGGGGGGGGG'+lineItem1.ProductId);
        p = ShopUtils.getProductByOrderCloudId(lineItem1.ProductID,lineitem1.product.xp.VendorName);  //Brighthat - fixing product identification logic to check vendor name along with order cloud id
        system.debug('GGGGGGGGGGGGGGGGGGGG'+p);
    }
    
    system.debug('GGGGGGGGGGGGGGGGGGGG'+p);
    
    if (p == null){
        system.debug('GGGGGGGGGGGGGGGGGGGG');
        Product2 newProduct = new Product2();
        if(lineitem1.productID == 'AACPunchoutProduct'){        
            newProduct.Name = lineitem1.xp.Description;
            newProduct.Order_Cloud_Product_Id__c = lineitem1.xp.SupplierPartID;
            newProduct.Description = lineitem1.xp.Description;
            newProduct.Vendor_Name__c = lineitem1.xp.PunchoutName;
            newProduct.ProductCode = lineitem1.xp.SupplierPartAuxiliaryID;
            newProduct.PunchoutProduct__c = True;  
            newProduct.IsActive = true; 
            system.debug('GGGGGGGGGGGGGGGGGGGG'+lineitem1);                 
        }
        else{
            newProduct.Name = lineitem1.product.Name;
            newProduct.Order_Cloud_Product_Id__c = lineitem1.product.ID;
            newProduct.Description = lineitem1.product.Description;
            newProduct.Vendor_Name__c = lineitem1.product.xp.VendorName;
            newProduct.ProductCode = lineitem1.product.xp.SecondaryItemNo;
            newProduct.PunchoutProduct__c = false;
            newProduct.IsActive = true;
            system.debug('GGGGGGGGGGGGGGGGGGGG'+lineitem1); 
        }
        newProduct.AcctSeed__Expense_GL_Account__c = glAccount.Id;
        newProduct.AcctSeed__GL_Account_Variable_1__c = accountingvariableMap.get('Unrestricted').Id;
        newProduct.AcctSeed__GL_Account_Variable_2__c = accountingvariableMap.get('Programs').Id;
        newProduct.AcctSeed__GL_Account_Variable_3__c = accountingvariableMap.get('Classroom Adoption').Id;
        newProduct.AcctSeed__Inventory_Type__c = 'Raw Material';
        prodlist.add(newProduct);
        system.debug('GGGGGGGGGGGGGGGGGGGG'+newProduct);
     }
     else{
         oldprodlist.put(p.Id,p);
         system.debug('GGGGGGGGGGGGGGGGGGGG'+p); 
     }
     system.debug('GGGGGGGGGGGGGGGGGGGG'+prodlist);
     system.debug('GGGGGGGGGGGGGGGGGGGG'+oldprodlist); 
   }
   system.debug('GGGGGGGGGGGGGGGGGGGG'+prodlist);
   system.debug('GGGGGGGGGGGGGGGGGGGG'+oldprodlist); 
     
   if(prodlist.size() > 0){
       list<product2> plist = new list<product2>();
       plist.addall(prodlist);
       database.Insert(plist); 
        
       for(Product2 pd : plist){
           oldprodlist.put(pd.Id,pd);
       }  
   }
   system.debug('GGGGGGGGGGGGGGGGGGGG'+oldprodlist); 
     
   list<PricebookEntry> pbelist = new list<PricebookEntry>();
   if(prodlist.size() > 0){       
       for(product2 p : prodlist){
        pricebookEntry pbe = new pricebookEntry();
        pbe.Product2Id = p.Id;
        pbe.Pricebook2Id = pb[0].Id;
        pbe.UnitPrice = 0;
        pbe.IsActive = true;
        pbelist.add(pbe);
       }
   }
   if(pbelist.size() > 0){
       Insert pbelist;      
   }
   
   system.debug('PPPPPPPPPPPPPPPPPP'+pbelist);
   
   if(oldprodlist.size() >0){
        system.debug('QQQQQQQQQQQQQQQQQQ'+oldprodlist.size());
        oldpbelist = [select Id,Product2Id,pricebook2Id,unitprice,IsActive,product2.Order_Cloud_Product_Id__c,product2.Name from PricebookEntry where Product2Id IN: oldprodlist.keyset()];
        system.debug('QQQQQQQQQQQQQQQQQQ'+oldpbelist.size());
    }
    
   system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@'+pbelist.size());
   /*  for(pricebookentry phb : oldpbelist){
       system.debug('TTTTTTTTTTTTTTTTTTTTTT'+phb);
       system.debug('TTTTTTTTTTTTTTTTTTTTTT'+phb.Product2.Order_cloud_product_Id__c);
       system.debug('TTTTTTTTTTTTTTTTTTTTTT'+phb.product2.name);
   } */
   set<OpportunityLineitem> olist = new set<OpportunityLineitem>();
   opportunity = ShopUtils.getOpportunityByCloudId(scope[0].Order_id__c);
    for(pricebookentry pb1 : oldpbelist){  
        system.debug('************'+pb1); 
        for(ordercloudLineItem order: ocli){
            system.debug('oooooooooooooo'+order);
            string lineitemstring =  JSON.serialize(order);
            JSONParser parser = JSON.createParser(lineItemString); 
            OrderCloudLineItem lineItem1 = (OrderCloudLineItem) parser.readValueAs(OrderCloudLineItem.class);
            system.debug('%%%%%%%%%lineitem %%%%%%%%%%%%%%%%%%'+lineitem1.productID);
            system.debug('%%%%%%%%%Product%%%%%%%%%%%%%%%%%%'+pb1.product2.Order_Cloud_Product_Id__c);
             system.debug('---------------------------------------------------------------');
            system.debug('%%%%%%%%%lineitem %%%%%%%%%%%%%%%%%%'+pb1.product2.name);
             system.debug('%%%%%%%%%lineitem %%%%%%%%%%%%%%%%%%'+lineitem1.product.Name);
            if(lineitem1.productID == 'AACPunchoutProduct' && (pb1.product2.Order_Cloud_Product_Id__c == lineitem1.xp.SupplierPartID || pb1.product2.Name == lineitem1.xp.Description)){
                system.debug('oooooooooooooo'+lineitem1);
                OpportunityLineItem oli = new OpportunityLineItem(
                PricebookEntryId = pb1.Id,
                OpportunityId = opportunity.Id,
                Quantity = lineitem1.Quantity,
                //UnitPrice = lineitem1.UnitPrice,
                UnitPrice = (lineitem1.UnitPrice != null)?lineitem1.UnitPrice:0, 
                VendorOrderId__c = lineitem1.xp.vendorOrderId.get(0));
                olist.add(oli);
            }
            else if(pb1.product2.Order_Cloud_Product_Id__c == lineitem1.product.ID){
                system.debug('oooooooooooooo'+lineitem1);
                system.debug('IIIIIIIIIIINNNNNNNNNNSSSSSSSSSSSIIIIIIIIIIIDDDDDDDDEEEEEE'+pb1.product2.Order_Cloud_Product_Id__c );
                system.debug('IIIIIIIIIIINNNNNNNNNNSSSSSSSSSSSIIIIIIIIIIIDDDDDDDDEEEEEE'+pb1.Product2.Name );
                OpportunityLineItem oli = new OpportunityLineItem(
                PricebookEntryId = pb1.Id,
                OpportunityId = opportunity.Id,
                Quantity = lineitem1.Quantity,
                //UnitPrice = lineitem1.UnitPrice,
                UnitPrice = (lineitem1.UnitPrice != null)?lineitem1.UnitPrice:0,              
                VendorOrderId__c = lineitem1.xp.vendorOrderId.get(0));
                olist.add(oli);
            }
            else{
                system.debug('oooooooooooooo');
            }    
            
        }
    }
    system.debug('RRRRRRRRRRRRRRRRRRRR'+olist.size());
    system.debug('TTTTTTTTTTTTTTTTTTTTT'+olist);
    if(olist.size() > 0){
        list<Opportunitylineitem> olilist = new list<OpportunityLineitem>();
        olilist.addall(olist);
        database.Insert(olilist);
    }
 }
     global void finish(Database.BatchableContext BC){
         if(opportunity != null && opportunity.id != null){
             OpportunityManagement.SendDonorEmail(new set<Id>{opportunity.Id});
         }
     }
}