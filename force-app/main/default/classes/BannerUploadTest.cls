@isTest
public class BannerUploadTest {
    
    @TestSetup
    static void makeData(){
        Account a = TestUtils.createAccount();
		insert a;

		Account school = TestUtils.createAccount();
		school.ShippingStreet = '123 Main Street';
		school.ShippingCity = 'Minneapolis';
		school.ShippingStateCode = 'MN';
		school.ShippingCountryCode = 'US';
		school.ShippingPostalCode = '12345';
		insert school;
		
		Contact c = TestUtils.createContact(a.Id);
		c.School__c = school.Id;
		c.School__r = school;
		insert c;

		//insert portal user
		User user1 = new User(
			Username = 'testamc@test.acm',
			ContactId = c.Id,
			ProfileId = [SELECT Id FROM Profile WHERE Name = 'AAC - Teacher Community Plus Login User' Limit 1].Id,
			Alias = 'test123',
			Email = 'test12345@test.com',
			EmailEncodingKey = 'UTF-8',
			FirstName = 'Unit Test',
			LastName = 'Qtest',
			CommunityNickname = 'test12345',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'en_US',
			LanguageLocaleKey = 'en_US'
		);
		insert user1;
	
		donation_split__Designation__c dsd = TestUtils.createClassroom(c.Id,school.Id);
		dsd.Description__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		dsd.How_will_you_spend_your_funds__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		dsd.Why_We_Need_Your_Help__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere, nisl ac iaculis efficitur, massa leo sodales diam, et scelerisque ex erat ac est. Nulla sit amet semper metus, ac elementum elit. Quisque at mattis urna. Duis at nisi arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rutrum nulla tellus, bibendum gravida justo rhoncus ut. Donec tempor lorem eu condimentum scelerisque. Nullam sem est, elementum id interdum sit amet, blandit ut arcu. Cras ut tellus at lacus ornare porttitor. Pellentesque placerat diam quis leo bibendum lacinia.';
		Insert dsd;
    }

    @isTest
    public static void testGetBanner() {
        String result = '';
			Test.startTest();
			result = BannerUpload.getBanner();
			Test.stopTest();

        system.assertNotEquals(null, result, 'It should return the default banner or the attached one.');
    }

    @isTest
    public static void testsetBanner() {
        
        String base64Test = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQYV2M4c+bMfwAIMANkq3cY2wAAAABJRU5ErkJggg==';
        String fileNameTest = 'test.jpg';
		List<User> usr = [SELECT ID FROM User WHERE FirstName = 'Unit Test'];

        system.runAs(usr[0]) {
			Test.startTest();
			BannerUpload.setBanner(base64Test, fileNameTest);
			Test.stopTest();
		}
		
		String teacherId = [SELECT Teacher__c FROM donation_split__Designation__c LIMIT 1].Teacher__c;
        system.assertNotEquals(null, teacherId, 'It should upload the banner and relate ir to the classroom record.');
    }
}