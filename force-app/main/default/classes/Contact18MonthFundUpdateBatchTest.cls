@isTest
public class Contact18MonthFundUpdateBatchTest {
    
    static testMethod void Contact18MonthFundUpdateBatchTest1() {
        Id devRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Teacher').getRecordTypeId();
        
        Contact contact = new Contact(RecordTypeId = devRecordTypeId, LastName = 'test');
        insert contact;
        
        Contact18MonthFundUpdateSchedule m = new Contact18MonthFundUpdateSchedule();
        String sch = '20 30 8 10 2 ?';
        String jobID = system.schedule('Test Job', sch, m);
        
        Contact18MonthFundUpdateBatch b = new Contact18MonthFundUpdateBatch(); 
        database.executebatch(b, 1);
    }
    
}