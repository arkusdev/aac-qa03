@isTest
public class ChargentDonorDetailsControllerTest{
    public static account accountData;
    public static contact contactData ;
    public static contact contactDataTeacher ;
    public static donation_split__Designation__c classroom ;
    public static user commUser;
    
    public Testmethod static void testChargentDonorDetailsController(){
        accountData= TestUtils.createAccount();
        accountData.Email__c ='test@gmail.com';
        ChargentDonorDetailsController.upsertAccount(accountData,'Organization');
        
        contactData = TestUtils.createContact(accountData.Id);
        contactData.npe01__WorkEmail__c='test@gmail.com';    
        contactData.email='test@gmail.com';  
        
        ChargentDonorDetailsController.upsertContact(contactData);  
        
        classroom = TestUtils.createClassroom(contactData.Id, accountData.Id);
        insert classroom;
        
        List<AcctSeed__Accounting_Variable__c> acvlist = new list<AcctSeed__Accounting_Variable__c>();
    
        AcctSeed__Accounting_Variable__c  acv1 = new AcctSeed__Accounting_Variable__c();
        acv1.AcctSeed__Type__c = 'GL Account Variable 1';
        acv1.AcctSeed__Active__c = true;
        acv1.Name = 'Unrestricted';
        
        AcctSeed__Accounting_Variable__c  acv2 = new AcctSeed__Accounting_Variable__c();
        acv2.AcctSeed__Type__c = 'GL Account Variable 2';
        acv2.AcctSeed__Active__c = true;
        acv2.Name = 'Programs';
        
        AcctSeed__Accounting_Variable__c  acv3 = new AcctSeed__Accounting_Variable__c();
        acv3.AcctSeed__Type__c = 'GL Account Variable 3';
        acv3.AcctSeed__Active__c = true;
        acv3.Name = 'Classroom Adoption';
        
        AcctSeed__Accounting_Variable__c  acv4 = new AcctSeed__Accounting_Variable__c();
        acv4.AcctSeed__Type__c = 'GL Account Variable 1';
        acv4.AcctSeed__Active__c = true;
        acv4.Name = 'Restricted';
        
        acvlist.add(acv1);
        acvlist.add(acv2);
        acvlist.add(acv3);
        acvlist.add(acv4);
        
        insert acvlist;
        
        AcctSeed__GL_Account__c asgl = new  AcctSeed__GL_Account__c();
        asgl.Name = '4599 Teacher Restricted Released';
        asgl.AcctSeed__Active__c = true;
        asgl.AcctSeed__Type__c = 'Revenue';
        Insert asgl;
        
        ChargentBase__Gateway__c g=new ChargentBase__Gateway__c();
        g.name='Authorize.Net Developer Account';
        g.recordtypeid=[select id,name from recordtype where developername='Authorizenet'].Id;
        g.ChargentBase__Merchant_ID__c='Test';
        g.ChargentBase__Security_Key__c='test';
        g.ChargentBase__Available_Payment_Methods__c='Credit Card; eCheck';
        insert g;
        
        ChargentOrders__ChargentOrder__c t=new ChargentOrders__ChargentOrder__c();
        t.ChargentOrders__Gateway__c=g.id;
        t.ChargentOrders__Subtotal__c=72;
        t.Transaction_Fee__c=3;
        t.ChargentOrders__Charge_Amount__c = 80; 
        t.Teacher_Help_Fund__c = 10;
        t.School_Program_Expense_Fee__c = 5;
        t.Tribute_Type__c = 'In honor of';
        
        List<ShoppingCartItem> spcartLst=new List<ShoppingCartItem>();
        ShoppingCartItem sc=new ShoppingCartItem();
        sc.classroomId=classroom.id;
        sc.amount =30;
        sc.frequency='One time';
        sc.type ='generalFund';
        spcartLst.add(sc);
        
        ShoppingCartItem sc1=new ShoppingCartItem();
        sc1.classroomId=classroom.id;
        sc1.amount =40;
        sc1.frequency='One time';
        spcartLst.add(sc1);
        
        ChargentDonorDetailsController.savePaymentData('test',contactData,accountData,t,JSON.serialize(spcartLst),null,'test@gmail.com','AACPromoCode',72,72,72,72,3,80,10,5,null,true,'In honor of', Date.today());
    }

    public Testmethod static void testChargentDonorDetailsController2(){
        accountData= TestUtils.createAccount();
        accountData.Email__c ='test@gmail.com';
        ChargentDonorDetailsController.upsertAccount(accountData,'Organization');
        
        contactData = TestUtils.createContact(accountData.Id);
        contactData.npe01__WorkEmail__c='test@gmail.com';    
        contactData.email='test@gmail.com';  
        
        ChargentDonorDetailsController.upsertContact(contactData);  
        
        classroom = TestUtils.createClassroom(contactData.Id, accountData.Id);
        insert classroom;
        
        List<AcctSeed__Accounting_Variable__c> acvlist = new list<AcctSeed__Accounting_Variable__c>();
    
        AcctSeed__Accounting_Variable__c  acv1 = new AcctSeed__Accounting_Variable__c();
        acv1.AcctSeed__Type__c = 'GL Account Variable 1';
        acv1.AcctSeed__Active__c = true;
        acv1.Name = 'Unrestricted';
        
        AcctSeed__Accounting_Variable__c  acv2 = new AcctSeed__Accounting_Variable__c();
        acv2.AcctSeed__Type__c = 'GL Account Variable 2';
        acv2.AcctSeed__Active__c = true;
        acv2.Name = 'Programs';
        
        AcctSeed__Accounting_Variable__c  acv3 = new AcctSeed__Accounting_Variable__c();
        acv3.AcctSeed__Type__c = 'GL Account Variable 3';
        acv3.AcctSeed__Active__c = true;
        acv3.Name = 'Classroom Adoption';
        
        AcctSeed__Accounting_Variable__c  acv4 = new AcctSeed__Accounting_Variable__c();
        acv4.AcctSeed__Type__c = 'GL Account Variable 1';
        acv4.AcctSeed__Active__c = true;
        acv4.Name = 'Restricted';
        
        acvlist.add(acv1);
        acvlist.add(acv2);
        acvlist.add(acv3);
        acvlist.add(acv4);
        
        insert acvlist;
        
        AcctSeed__GL_Account__c asgl = new  AcctSeed__GL_Account__c();
        asgl.Name = '4599 Teacher Restricted Released';
        asgl.AcctSeed__Active__c = true;
        asgl.AcctSeed__Type__c = 'Revenue';
        Insert asgl;
        
        ChargentBase__Gateway__c g=new ChargentBase__Gateway__c();
        g.name='Authorize.Net Developer Account';
        g.recordtypeid=[select id,name from recordtype where developername='Authorizenet'].Id;
        g.ChargentBase__Merchant_ID__c='Test';
        g.ChargentBase__Security_Key__c='test';
        g.ChargentBase__Available_Payment_Methods__c='Credit Card; eCheck';
        insert g;
        
        ChargentOrders__ChargentOrder__c t=new ChargentOrders__ChargentOrder__c();
        t.ChargentOrders__Gateway__c=g.id;
        t.ChargentOrders__Subtotal__c=72;
        t.Transaction_Fee__c=3;
        t.ChargentOrders__Charge_Amount__c = 80; 
        t.Teacher_Help_Fund__c = 10;
        t.School_Program_Expense_Fee__c = 5;
        t.Tribute_Type__c = 'In honor of';
        
        List<ShoppingCartItem> spcartLst=new List<ShoppingCartItem>();
        ShoppingCartItem sc=new ShoppingCartItem();
        sc.classroomId=classroom.id;
        sc.amount =30;
        sc.frequency='Monthly';
        sc.type ='generalFund';
        spcartLst.add(sc);
        
        ShoppingCartItem sc1=new ShoppingCartItem();
        sc1.classroomId=classroom.id;
        sc1.amount =40;
        sc1.frequency='Monthly';
        spcartLst.add(sc1);
        
        ChargentDonorDetailsController.savePaymentData('test',contactData,accountData,t,JSON.serialize(spcartLst),null,'test@gmail.com','AACPromoCode',72,72,72,72,3,80,10,5,null,true,'In honor of', Date.today().addDays(30));
    }

    public Testmethod static void TestGetFAQs() {
        List<FAQ__c> result = ChargentDonorDetailsController.getFAQs();
    }

    public Testmethod static void TestGetClassroomToCampaignIdMap() {
        
    }

    public Testmethod static void TestGetTributeType() {
        List<String> result = ChargentDonorDetailsController.getTributeType();
    }

    public Testmethod static void TestFormatDate() {
        String result = ChargentDonorDetailsController.formatDate(date.today());
    }

    public Testmethod static void testGetChargentOrder(){
        accountData= TestUtils.createAccount();
        accountData.Email__c ='test@gmail.com';
        ChargentDonorDetailsController.upsertAccount(accountData,'Organization');
        
        contactData = TestUtils.createContact(accountData.Id);
        contactData.npe01__WorkEmail__c='test@gmail.com';    
        contactData.email='test@gmail.com';  
        
        ChargentDonorDetailsController.upsertContact(contactData);  
        
        classroom = TestUtils.createClassroom(contactData.Id, accountData.Id);
        insert classroom;
        
        List<AcctSeed__Accounting_Variable__c> acvlist = new list<AcctSeed__Accounting_Variable__c>();
    
        AcctSeed__Accounting_Variable__c  acv1 = new AcctSeed__Accounting_Variable__c();
        acv1.AcctSeed__Type__c = 'GL Account Variable 1';
        acv1.AcctSeed__Active__c = true;
        acv1.Name = 'Unrestricted';
        
        AcctSeed__Accounting_Variable__c  acv2 = new AcctSeed__Accounting_Variable__c();
        acv2.AcctSeed__Type__c = 'GL Account Variable 2';
        acv2.AcctSeed__Active__c = true;
        acv2.Name = 'Programs';
        
        AcctSeed__Accounting_Variable__c  acv3 = new AcctSeed__Accounting_Variable__c();
        acv3.AcctSeed__Type__c = 'GL Account Variable 3';
        acv3.AcctSeed__Active__c = true;
        acv3.Name = 'Classroom Adoption';
        
        AcctSeed__Accounting_Variable__c  acv4 = new AcctSeed__Accounting_Variable__c();
        acv4.AcctSeed__Type__c = 'GL Account Variable 1';
        acv4.AcctSeed__Active__c = true;
        acv4.Name = 'Restricted';
        
        acvlist.add(acv1);
        acvlist.add(acv2);
        acvlist.add(acv3);
        acvlist.add(acv4);
        
        insert acvlist;
        
        AcctSeed__GL_Account__c asgl = new  AcctSeed__GL_Account__c();
        asgl.Name = '4599 Teacher Restricted Released';
        asgl.AcctSeed__Active__c = true;
        asgl.AcctSeed__Type__c = 'Revenue';
        Insert asgl;
        
        ChargentBase__Gateway__c g=new ChargentBase__Gateway__c();
        g.name='Authorize.Net Developer Account';
        g.recordtypeid=[select id,name from recordtype where developername='Authorizenet'].Id;
        g.ChargentBase__Merchant_ID__c='Test';
        g.ChargentBase__Security_Key__c='test';
        g.ChargentBase__Available_Payment_Methods__c='Credit Card; eCheck';
        insert g;
        
        ChargentOrders__ChargentOrder__c t=new ChargentOrders__ChargentOrder__c();
        t.ChargentOrders__Gateway__c=g.id;
        t.ChargentOrders__Subtotal__c=72;
        t.Transaction_Fee__c=3;
        t.ChargentOrders__Charge_Amount__c = 80; 
        t.Teacher_Help_Fund__c = 10;
        t.School_Program_Expense_Fee__c = 5;
        t.Tribute_Type__c = 'In honor of';
        insert t;

        Test.startTest();
        DonorUtils.getChargentOrder(t.Id);
        Test.stopTest();

    }

    public Testmethod static void testChargentEmailDonationReceipt(){
        accountData= TestUtils.createAccount();
        accountData.Email__c ='test@gmail.com';
        ChargentDonorDetailsController.upsertAccount(accountData,'Organization');
        
        contactData = TestUtils.createContact(accountData.Id);
        contactData.npe01__WorkEmail__c='test@gmail.com';    
        contactData.email='test@gmail.com';  
        
        ChargentDonorDetailsController.upsertContact(contactData);  
        
        classroom = TestUtils.createClassroom(contactData.Id, accountData.Id);
        insert classroom;
        
        List<AcctSeed__Accounting_Variable__c> acvlist = new list<AcctSeed__Accounting_Variable__c>();
    
        AcctSeed__Accounting_Variable__c  acv1 = new AcctSeed__Accounting_Variable__c();
        acv1.AcctSeed__Type__c = 'GL Account Variable 1';
        acv1.AcctSeed__Active__c = true;
        acv1.Name = 'Unrestricted';
        
        AcctSeed__Accounting_Variable__c  acv2 = new AcctSeed__Accounting_Variable__c();
        acv2.AcctSeed__Type__c = 'GL Account Variable 2';
        acv2.AcctSeed__Active__c = true;
        acv2.Name = 'Programs';
        
        AcctSeed__Accounting_Variable__c  acv3 = new AcctSeed__Accounting_Variable__c();
        acv3.AcctSeed__Type__c = 'GL Account Variable 3';
        acv3.AcctSeed__Active__c = true;
        acv3.Name = 'Classroom Adoption';
        
        AcctSeed__Accounting_Variable__c  acv4 = new AcctSeed__Accounting_Variable__c();
        acv4.AcctSeed__Type__c = 'GL Account Variable 1';
        acv4.AcctSeed__Active__c = true;
        acv4.Name = 'Restricted';
        
        acvlist.add(acv1);
        acvlist.add(acv2);
        acvlist.add(acv3);
        acvlist.add(acv4);
        
        insert acvlist;
        
        AcctSeed__GL_Account__c asgl = new  AcctSeed__GL_Account__c();
        asgl.Name = '4599 Teacher Restricted Released';
        asgl.AcctSeed__Active__c = true;
        asgl.AcctSeed__Type__c = 'Revenue';
        Insert asgl;
        
        ChargentBase__Gateway__c g=new ChargentBase__Gateway__c();
        g.name='Authorize.Net Developer Account';
        g.recordtypeid=[select id,name from recordtype where developername='Authorizenet'].Id;
        g.ChargentBase__Merchant_ID__c='Test';
        g.ChargentBase__Security_Key__c='test';
        g.ChargentBase__Available_Payment_Methods__c='Credit Card; eCheck';
        insert g;
        
        List<ChargentOrders__ChargentOrder__c> orders = new List<ChargentOrders__ChargentOrder__c>();
        ChargentOrders__ChargentOrder__c t=new ChargentOrders__ChargentOrder__c();
        t.ChargentOrders__Gateway__c=g.id;
        t.ChargentOrders__Subtotal__c=72;
        t.Transaction_Fee__c=3;
        t.ChargentOrders__Charge_Amount__c = 80; 
        t.Teacher_Help_Fund__c = 10;
        t.School_Program_Expense_Fee__c = 5;
        t.Tribute_Type__c = 'In honor of';
        orders.add(t);
        insert orders;

        Test.startTest();
        List<String> result = DonorUtils.ChargentEmailDonationReceipt(orders);
        Test.stopTest();

    }
}