@isTest
private class ObjectDescribeUtilsTest {
	
  static testMethod void testObjectDescribe() {
    initTestData();

    Test.startTest();

    StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
    mock.setStaticResource('AccountDescribeMock');
    mock.setStatusCode(200);
    mock.setHeader('Content-Type', 'application/json');

    Test.setMock(HttpCalloutMock.class, mock);

    System.assertEquals(null,
      ObjectDescribeUtils.getDescribeInfoFromDocument(null));
    ObjectDescribeUtils.updateDescribeDocument(null, null);

    Database.executeBatch(new ObjectDescribeBatchable(UserInfo.getSessionId()),
      ObjectDescribeUtils.BATCH_SIZE);

    Test.stopTest();

    SObjectDescribe sod = ObjectDescribeUtils.getDescribeInfoFromDocument(
      Account.SObjectType.getDescribe().getName());
    System.assertNotEquals(null, sod);

    for (SObjectDescribe.Field f : sod.fields) {
      System.assertNotEquals(null, f.name);
      System.assertNotEquals(null, f.label);

      for (SObjectDescribe.PicklistValue plv : f.picklistValues) {
        System.assertNotEquals(null, plv.value);
        System.assertNotEquals(null, plv.label);
        System.debug(plv.validFor);
      }
    }
  }

  private static void initTestData() {
    Page_Layout_Settings__c settings = TestUtils.createPageLayoutSettings();
    insert settings;

    Objects_To_Describe__c otd = TestUtils.createObjectToDescribe(
      Account.SObjectType.getDescribe().getName());
    insert otd;
  }
}