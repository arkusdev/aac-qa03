@isTest
private class activeClassroomPageControllerTest {
    
    @TestSetup
    static void makeData(){

        String schoolRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('School Admin').getRecordTypeId();
        
        Account portalAccount = new Account(name = 'testAccount');
        insert portalAccount;
        
        Contact con = new Contact();
        con.LastName = 'Unit Test';
        con.RecordTypeId = schoolRT;
        con.AccountId = portalAccount.Id;
        insert con;

        User portalUser = new User();
       
       portalUser.FirstName = 'Unit Test';
       portalUser.LastName  = 'Unit Test';
       portalUser.Email     = 'UnitTest@UnitTestaac.com';
       portalUser.Username  = 'UnitTest@UnitTestaac.com';
       portalUser.Alias     = 'fatty';
       portalUser.ProfileId = '00eC00000012HXVIA2';
       portalUser.UserPreferencesShowProfilePicToGuestUsers = true;
       portalUser.ContactId = con.Id;
       portalUser.TimeZoneSidKey    = 'America/Denver';
       portalUser.LocaleSidKey      = 'en_US';
       portalUser.EmailEncodingKey  = 'UTF-8';
       portalUser.LanguageLocaleKey = 'en_US';
       
       insert portalUser;

        donation_split__Designation__c classroom = new donation_split__Designation__c();
        classroom.How_will_you_spend_your_funds__c = 'This is a test, minium 100 characters.This is a test, minium 100 characters.This is a test, minium 100 characters.';
        classroom.Description__c = 'This is a test, minium 100 characters.This is a test, minium 100 characters.This is a test, minium 100 characters. This is a test, minium 100 characters.This is a test, minium 100 characters.This is a test, minium 100 characters. This is a test, minium 100 characters.';
        classroom.Teacher__c = con.Id;
        classroom.Inactive_Classroom_Page__c = true;
        insert classroom;

    }

    @isTest
    static void testActive() {

        List<User> usr = [SELECT ID FROM User WHERE FirstName = 'Unit Test'];

        test.startTest();
        system.runAs(usr[0]) {
            activeClassroomPageController.active();
        }
        test.stopTest();

        donation_split__Designation__c result = [SELECT Inactive_Classroom_Page__c FROM donation_split__Designation__c];
        System.assertEquals(false, result.Inactive_Classroom_Page__c, 'It should be false');
    }
}