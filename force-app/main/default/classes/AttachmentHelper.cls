public class AttachmentHelper{
    static Map<String,string> vendorWithIdMap=new  Map<string,string>();
    static Map<string,string> fileNameWithVendorMap=new  Map<string,string>();
    static Map<String,Vendor_File__c> vendorNameWithRecordMap=new  Map<String,Vendor_File__c>();
    static String VendorNameStr;
    static String VendorEmailStr;
    static Integer emailIndex;
    static Integer nameIndex;
    static set<Id> vendorIds=new Set<Id>();
    
    public static void processInvoiceAttachmentBefore(List<Attachment> aList){
        for(Attachment a:aList){
            if(a.name != null && a.parentId == null && (a.contentType=='text/plain' || a.contentType=='text/xml' || a.name.EndsWithIgnoreCase('.dat'))){
                if(!vendorWithIdMap.containsKey(a.name)){
                    vendorWithIdMap.put(a.name,null);                    
                }
                if(!vendorNameWithRecordMap.containsKey(a.name)){
                    vendorNameWithRecordMap.put(a.name,new Vendor_File__c(name=a.name,Status__c='Un Processed'));
                }
            }
        }
        
        if(vendorNameWithRecordMap.keyset() != null && vendorNameWithRecordMap.keyset().size() >0){
            Insert vendorNameWithRecordMap.values();
            for(Vendor_File__c v:vendorNameWithRecordMap.values()){
                vendorWithIdMap.put(v.name,v.Id);
                vendorIds.add(v.Id);
            }
        }       
        
        for(Attachment a:aList){      
            if(a.name != null && vendorWithIdMap.containsKey(a.name) && vendorWithIdMap.get(a.name) != null){
                a.parentId=vendorWithIdMap.get(a.name);
                if(a.contentType=='text/plain' && !a.name.endsWith('.txt')){
                    a.name=a.name+'.txt';
                }
                else if(a.contentType=='text/xml' && !a.name.endsWith('.xml')){
                    a.name=a.name+'.xml';
                }              
            }
        }
    }
    
    public static void processInvoiceAttachmentAfter(List<Attachment> aList){
        if(vendorWithIdMap.keyset() != null && vendorWithIdMap.keyset().size() >0){
            AttachmentHelper.updatevendorfilesFuture(new set<string>(vendorWithIdMap.values()));
        }
    }
    
    @Future
    public static void updatevendorfilesFuture(Set<string> fileIds){
        List<Vendor_File__c> fileToUpdate=new List<Vendor_File__c>();
        For(Attachment a:[SELECT Id,BodyLength,ParentId from Attachment where parentId IN:fileIds AND BodyLength=0]){
            fileToUpdate.add(new Vendor_File__c(Id=a.ParentId,Status__c='Failed'));
        }
        
        if(fileToUpdate.size() >0)
            update fileToUpdate;
    }
}