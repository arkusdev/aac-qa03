public class OppProductGroupByVendor {
    public String recID {get; set;}  
    public Map<String,list<OpportunityLineItem>> olimap{get;set;}
    public Map<String,list<OpportunityLineItem>> olimap1{get;set;}
    public list<String> keyValues{get;set;}
    public OppProductGroupByVendor()
    {
        /*   olimap = new Map<string,list<OpportunityLineItem>>();
olimap1 = new Map<string,list<OpportunityLineItem>>();
olimap1 = getOpptylineItems();
keyvalues = new list<string>();
keyvalues.addall(olimap1.keyset()); */
    }
    
    public opportunity getOpportunityDetails(){
        if(recID != null){
            List<opportunity> oppList=[select id,name,Subtotal__c,ShippingCost__c,TaxCost__c,PromotionDiscount__c,Total_Amount__c,Teacher__c,Teacher__r.lastname,Teacher__r.firstname from opportunity where id=:recId];
            if(oppList != null && oppList.size() >0){
                return oppList[0];
            }
        }
        
        return new opportunity();
    }
    Public Map<String,list<OpportunityLineItem>>  getOpptylineItems()
    {    
        List<OpportunityLineItem> olilist = new List<OpportunityLineItem>();
        system.debug('@@@@@@@@@@@@@@@@@@@'+recID);
        list<string> vendorslist = new list<string>();
        set<string> vendorsname = new set<string>();
        Map<String,list<OpportunityLineItem>> olimap =  new map<string,list<OpportunityLineItem>>();
        RecordType rt = [select Id,developerName from RecordType where developername='Vendor' and sobjectType = 'Account' limit 1];  
        olilist = [Select Id, Vendor_name__c,VendorOrderId__c,TotalPrice,ProductCode,Product2.Name,Quantity,UnitPrice,Opportunity.OrderCloudLineItemCount__c,Opportunity.OpportunityProductCount__c,Opportunity.Order_Cloud_Order_Id__c,Opportunity.DateOrderSubmitted__c,Opportunity.Subtotal__c,Opportunity.ShippingCost__c,Opportunity.TaxCost__c,Opportunity.Total_Amount__c,Opportunity.Teacher__r.mailingCity,Opportunity.Teacher__r.mailingPostalCode ,Opportunity.Teacher__r.mailingCountryCode,Opportunity.Teacher__r.mailingStateCode,Opportunity.Teacher__r.mailingStreet,Opportunity.Teacher__r.email,Opportunity.Teacher__r.School__r.Name from OpportunityLineItem where OpportunityId =: recID];  
        for(Opportunitylineitem o : olilist)
        {
            string venstring = o.vendor_name__c+' -  Order # '+o.VendorOrderId__c;
            if(olimap.containsKey(venstring)) 
            {
                List<OpportunityLineItem> oppProdlist = olimap.get(venstring);
                oppProdlist.add(o);
                olimap.put(venstring, oppProdlist);
            } 
            else 
            {
                olimap.put(venstring, new List<OpportunityLineItem> { o });
            }
        }
        return olimap;
    }
    
}