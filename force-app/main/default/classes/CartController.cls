global with sharing class CartController {

    @AuraEnabled
    global static Cart cartInit() {
        Cart cart = new cart();
        return cart;
    }

    @AuraEnabled
    global static String cartSubmit(String cart) {
        return CartUtils.cartSubmit(cart);
    }

    @AuraEnabled
    global static void paymentRequest(String corderId, String cart) {
        CartUtils.paymentRequest(corderId, cart);
    }

    @AuraEnabled
    public static gTMData getGTMData(String recordId){
        return CartUtils.getGTMData(recordId);
    }
    
    public static String formatDate(Date input) {
        return DateTime.newInstance( input.year(), input.month(), input.day() ).format('MM/dd/yyyy');
    }
    
    global class Cart {
        @AuraEnabled global String step;
        @AuraEnabled global List<CartItem> cartItems;
        @AuraEnabled global Decimal fee;
        @AuraEnabled global Decimal helpAmount;
        @AuraEnabled global Decimal total;
        @AuraEnabled global Boolean oneTimeDonation;
        @AuraEnabled global Boolean monthlyDonation;
        @AuraEnabled global Date endDonationDate;
        @AuraEnabled global String comment;
        @AuraEnabled global Donor donor;
        @AuraEnabled global Payment payment;
        
        global Cart() {
            step = '1';
            cartItems = new List<CartItem>();
            fee = 0;
            helpAmount = 0;
            total = 0;
            oneTimeDonation = true;
            monthlyDonation = false;
            endDonationDate = null;
            comment = '';
            donor = new Donor();
            payment = new Payment();
        }
    }

    global class CartItem {
        @AuraEnabled global Classroom classroom;
        @AuraEnabled global Decimal amount;
        @AuraEnabled global String type;
        
        global CartItem() {
            classroom = new Classroom();
            amount = 50.00;
            type = '';
        }
    }

    global class Classroom {
        
        @AuraEnabled global String classroomId;
        @AuraEnabled global String teacherName;
        @AuraEnabled global String schoolName;
        @AuraEnabled global String recordTypeName;
        
        global Classroom() {
            classroomId = '';
            teacherName = '';
            schoolName = '';
            recordTypeName = '';
        }
    }

    global class Donor {
        
        @AuraEnabled global String firstName;
        @AuraEnabled global String lastName;
        @AuraEnabled global String email;
        @AuraEnabled global String phone;
        @AuraEnabled global Boolean suscribed;
        @AuraEnabled global Boolean annualReport;
        @AuraEnabled global Boolean anonymous;
        @AuraEnabled global String donorType;
        @AuraEnabled global String displayName;
        
        global Donor() {
            firstName = '';
            lastName = '';
            email = '';
            phone = '';
            suscribed = true;
            annualReport = true;
            anonymous = false;
            donorType = 'Individual';
            displayName = '';
        }
    }

    global class Payment {
        
        @AuraEnabled global String addressLine1;
        @AuraEnabled global String addressLine2;
        @AuraEnabled global String city;
        @AuraEnabled global String state;
        @AuraEnabled global String zip;
        @AuraEnabled global String cardName;
        @AuraEnabled global String cardNumber;
        @AuraEnabled global String cardCVV;
        @AuraEnabled global String cardExpMM;
        @AuraEnabled global String cardExpYYYY;
        
        global Payment() {
            addressLine1 = '';
            addressLine2 = '';
            city = '';
            state = '';
            zip = '';
            cardName = '';
            cardNumber = '';
            cardCVV = '';
            cardExpMM = '';
            cardExpYYYY = '';
        }
    }

    public class paymentResponse{
        public string responseStatus;
        public string responseCode;  
        public string reasonText;        
    }

    global class gTMData {    
        @AuraEnabled global String transactionId;
        @AuraEnabled global Decimal transactionTotal;
        @AuraEnabled global String sku;
        @AuraEnabled global String name;
        @AuraEnabled global String category;
        @AuraEnabled global Decimal price;
        @AuraEnabled global Integer quantity;
        global gTMData() {
            transactionId = '';
            transactionTotal = 0.00;
            sku = '';
            name = '';
            category = '';
            price = 0.00;
            quantity = 1;
        }
    }
}