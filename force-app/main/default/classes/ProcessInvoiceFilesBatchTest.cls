@isTest
private class ProcessInvoiceFilesBatchTest{
    private static donation_split__Designation__c classroom;
    private static Campaign campaign;
    private static Account account;
    private static Contact contact;
    
    static String XMLInvoiceBody='<?xml version="1.0"?>'+
    +'<!DOCTYPE cXML SYSTEM "http://xml.cXML.org/schemas/cXML/1.2.011/InvoiceDetail.dtd">'+
    +'<cXML version="1.2.011" payloadID="0925201758991.cXML.346726610001.HJKwBpOhLXavPlDX@officedepot.com" timestamp="2017-09-25T05:30:58-05:00" xml:lang="en-US">'+
      +'<Header>'+
        +'<From>'+
          +'<Credential domain="NetworkId">'+
            +'<Identity>AN01000000147</Identity>'+
          +'</Credential>'+
        +'</From>'+
        +'<To>'+
          +'<Credential domain="NetworkID">'+
            +'<Identity>78553589-CXML</Identity>'+
          +'</Credential>'+
        +'</To>'+
        +'<Sender>'+
          +'<Credential domain="NetworkId">'+
            +'<Identity>AN01000000147</Identity>'+
            +'<SharedSecret>Welcome1</SharedSecret>'+
          +'</Credential>'+
          +'<UserAgent>test</UserAgent>'+
        +'</Sender>'+
      +'</Header>'+
      +'<Request deploymentMode="production">'+
        +'<InvoiceDetailRequest>'+
          +'<InvoiceDetailRequestHeader invoiceID="514145" purpose="standard" operation="new" invoiceDate="2017-09-25T12:00:00-05:00">'+
            +'<InvoiceDetailHeaderIndicator/>'+
            +'<InvoiceDetailLineIndicator/>'+
            +'<InvoicePartner>'+
              +'<Contact role="remitTo">'+
                +'<Name xml:lang="en-US">OFFICE DEPOT</Name>'+
                +'<PostalAddress>'+
                  +'<Street>PO BOX 88040</Street>'+
                  +'<City>CHICAGO</City>'+
                  +'<State>IL</State>'+
                  +'<PostalCode>60680-1040</PostalCode>'+
                  +'<Country isoCountryCode="US">United States</Country>'+
                +'</PostalAddress>'+
              +'</Contact>'+
            +'</InvoicePartner>'+
            +'<InvoicePartner>'+
              +'<Contact role="soldTo">'+
                +'<Name xml:lang="en-US">0YCIZ1SYXL1501863549200</Name>'+
                +'<PostalAddress>'+
                  +'<Street>601 TAYLOR STREET</Street>'+
                  +'<City>EAST PEORIA</City>'+
                  +'<State>IL</State>'+
                  +'<PostalCode>61611</PostalCode>'+
                  +'<Country isoCountryCode="US">United States</Country>'+
                +'</PostalAddress>'+
              +'</Contact>'+
            +'</InvoicePartner>'+
            +'<InvoiceDetailShipping>'+
              +'<Contact role="shipTo" addressID=" ">'+
                +'<Name xml:lang="en-US">0YCIZ1SYXL1501863549200</Name>'+
                +'<PostalAddress>'+
                  +'<Street>601 TAYLOR STREET</Street>'+
                  +'<City>EAST PEORIA</City>'+
                  +'<State>IL</State>'+
                  +'<PostalCode>61611</PostalCode>'+
                  +'<Country isoCountryCode="US">United States</Country>'+
                +'</PostalAddress>'+
              +'</Contact>'+
              +'<Contact role="shipFrom">'+
                +'<Name xml:lang="en-US">Office Depot</Name>'+
              +'</Contact>'+
            +'</InvoiceDetailShipping>'+
            +'<Extrinsic name="invoice due date">20170925</Extrinsic>'+
          +'</InvoiceDetailRequestHeader>'+
          +'<InvoiceDetailOrder>'+
            +'<InvoiceDetailOrderInfo>'+
              +'<OrderReference orderID="12342345" orderDate="2017-01-01">'+
                +'<DocumentReference payloadID="2322f875-7ac8-4a53-bade-740f0a1858da"/>'+
              +'</OrderReference>'+
              +'<SupplierOrderInfo orderID="346726610001"/>'+
            +'</InvoiceDetailOrderInfo>'+
            +'<InvoiceDetailItem invoiceLineNumber="1" quantity="1">'+
              +'<UnitOfMeasure>PK</UnitOfMeasure>'+
              +'<UnitPrice>'+
                +'<Money currency="USD">5</Money>'+
              +'</UnitPrice>'+
              +'<InvoiceDetailItemReference lineNumber="1">'+
                +'<ItemID>'+
                  +'<SupplierPartID>12345</SupplierPartID>'+
                +'</ItemID>'+
                +'<Description xml:lang="en-US">GLUE,STIC,26OZ,6/PK,CLR</Description>'+
              +'</InvoiceDetailItemReference>'+
              +'<Extrinsic name="UNSPSC">44121631</Extrinsic>'+
            +'</InvoiceDetailItem>'+
          +'</InvoiceDetailOrder>'+
          +'<InvoiceDetailSummary>'+
            +'<SubtotalAmount>'+
              +'<Money currency="USD">5</Money>'+
            +'</SubtotalAmount>'+
            +'<Tax>'+
              +'<Money currency="USD">0.46</Money>'+
              +'<Description xml:lang="en-US">Tax</Description>'+
              +'<TaxDetail purpose="tax" category="sales">'+
                +'<TaxAmount>'+
                  +'<Money currency="USD">0.46</Money>'+
                +'</TaxAmount>'+
              +'</TaxDetail>'+
            +'</Tax>'+
            +'<GrossAmount>'+
              +'<Money currency="USD">5</Money>'+
            +'</GrossAmount>'+
            +'<NetAmount>'+
              +'<Money currency="USD">5.46</Money>'+
            +'</NetAmount>'+
          +'</InvoiceDetailSummary>'+
        +'</InvoiceDetailRequest>'+
      +'</Request>'+
    +'</cXML>';
    
    static String XMLInvoiceBodyWrong='<?xml version="1.0"?>'+
    +'<!DOCTYPE cXML SYSTEM "http://xml.cXML.org/schemas/cXML/1.2.011/InvoiceDetail.dtd">'+
    +'<cXML version="1.2.011" payloadID="0925201758991.cXML.346726610001.HJKwBpOhLXavPlDX@officedepot.com" timestamp="2017-09-25T05:30:58-05:00" xml:lang="en-US">'+
      +'<Header>'+
        +'<From>'+
          +'<Credential domain="NetworkId">'+
            +'<Identity>AN01000000147</Identity>'+
          +'</Credential>'+
        +'</From>'+
        +'<To>'+
          +'<Credential domain="NetworkID">'+
            +'<Identity>78553589-CXML</Identity>'+
          +'</Credential>'+
        +'</To>'+
        +'<Sender>'+
          +'<Credential domain="NetworkId">'+
            +'<Identity>AN01000000147</Identity>'+
            +'<SharedSecret>Welcome1</SharedSecret>'+
          +'</Credential>'+
          +'<UserAgent>test</UserAgent>'+
        +'</Sender>'+
      +'</Header>'+
      +'<Request deploymentMode="production">'+
        +'<InvoiceDetailRequest>'+
          +'<InvoiceDetailRequestHeader invoiceID="514145" purpose="standard" operation="new" invoiceDate="2017-09-25T12:00:00-05:00">'+
            +'<InvoiceDetailHeaderIndicator/>'+
            +'<InvoiceDetailLineIndicator/>'+
            +'<InvoicePartner>'+
              +'<Contact role="remitTo">'+
                +'<Name xml:lang="en-US">OFFICE DEPOT</Name>'+
                +'<PostalAddress>'+
                  +'<Street>PO BOX 88040</Street>'+
                  +'<City>CHICAGO</City>'+
                  +'<State>IL</State>'+
                  +'<PostalCode>60680-1040</PostalCode>'+
                  +'<Country isoCountryCode="US">United States</Country>'+
                +'</PostalAddress>'+
              +'</Contact>'+
            +'</InvoicePartner>'+
            +'<InvoicePartner>'+
              +'<Contact role="soldTo">'+
                +'<Name xml:lang="en-US">0YCIZ1SYXL1501863549200</Name>'+
                +'<PostalAddress>'+
                  +'<Street>601 TAYLOR STREET</Street>'+
                  +'<City>EAST PEORIA</City>'+
                  +'<State>IL</State>'+
                  +'<PostalCode>61611</PostalCode>'+
                  +'<Country isoCountryCode="US">United States</Country>'+
                +'</PostalAddress>'+
              +'</Contact>'+
            +'</InvoicePartner>'+
            +'<InvoiceDetailShipping>'+
              +'<Contact role="shipTo" addressID=" ">'+
                +'<Name xml:lang="en-US">0YCIZ1SYXL1501863549200</Name>'+
                +'<PostalAddress>'+
                  +'<Street>601 TAYLOR STREET</Street>'+
                  +'<City>EAST PEORIA</City>'+
                  +'<State>IL</State>'+
                  +'<PostalCode>61611</PostalCode>'+
                  +'<Country isoCountryCode="US">United States</Country>'+
                +'</PostalAddress>'+
              +'</Contact>'+
              +'<Contact role="shipFrom">'+
                +'<Name xml:lang="en-US">Office Depot</Name>'+
              +'</Contact>'+
            +'</InvoiceDetailShipping>'+
            +'<Extrinsic name="invoice due date">20170925</Extrinsic>'+
          +'</InvoiceDetailRequestHeader>'+
          +'<InvoiceDetailOrder>'+
            +'<InvoiceDetailOrderInfo>'+
              +'<OrderReference orderID="12342345" orderDate="201d7-01-01">'+
                +'<DocumentReference payloadID="2322f875-7ac8-4a53-bade-740f0a1858da"/>'+
              +'</OrderReference>'+
              +'<SupplierOrderInfo orderID="346726610001"/>'+
            +'</InvoiceDetailOrderInfo>'+
            +'<InvoiceDetailItem invoiceLineNumber="1" quantity="1">'+
              +'<UnitOfMeasure>PK</UnitOfMeasure>'+
              +'<UnitPrice>'+
                +'<Money currency="USD">5</Money>'+
              +'</UnitPrice>'+
              +'<InvoiceDetailItemReference lineNumber="1">'+
                +'<ItemID>'+
                +'</ItemID>'+
                +'<Description xml:lang="en-US">GLUE,STIC,26OZ,6/PK,CLR</Description>'+
              +'</InvoiceDetailItemReference>'+
              +'<Extrinsic name="UNSPSC">44121631</Extrinsic>'+
            +'</InvoiceDetailItem>'+
          +'</InvoiceDetailOrder>'+
          +'<InvoiceDetailSummary>'+
            +'<Tax>'+
              +'<Money currency="USD">0.46</Money>'+
              +'<Description xml:lang="en-US">Tax</Description>'+
              +'<TaxDetail purpose="tax" category="sales">'+                
              +'</TaxDetail>'+
            +'</Tax>'+
            +'<GrossAmount>'+
              +'<Money currency="USD">5</Money>'+
            +'</GrossAmount>'+
            +'<NetAmount>'+
            +'</NetAmount>'+
          +'</InvoiceDetailSummary>'+
        +'</InvoiceDetailRequest>'+
      +'</Request>'+
    +'</cXML>';
    
    
    Static String x12InvoiceBody='ISA*00**00**ZZ*SCHOOLSP*ZZ*ADOPTCLASS*170726*0914*U*00401*000095813*0*P*+~'+
        +'GS*IN*SCHOOLSP*ADOPTCLASS*20170726*0914*95813*X*004010~'+
        +'ST*810*159976~'+
        +'BIG*20170725*208118668179*20170627*pGVeZqzf-UG_aXXFOYNWxA~'+
        +'N1*BT*ADOPT A CLASSROOM~'+
        +'N3*110 N 5TH ST~'+
        +'N4*MINNEAPOLIS*MN*55403-1619~'+
        +'N1*RI*SCHOOL SPECIALTY~'+
        +'N3*32656 COLLECTION CENTER DR~'+
        +'N4*CHICAGO**60693-0326~'+
        +'N1*ST*BEAUMONT MAGNET ELEM SCHOOL~'+
        +'N3*1211 BEAUMONT AVE~'+
        +'N4*KNOXVILLE*TN*37921-3464~'+
        +'ITD*01*3****2170727*30*****30 NET~'+
        +'DTM*011*20170725~'+
        +'IT1*12*1.0*EA*3.32**IN*12311~'+
        +'PID*F****TABLE - CS ACTIVITY SQUARE 42 - MARKERBOARD TOP - SPECIFY T-MOLD EDGE/UPPER LEG ~'+
        +'TDS*3.72~'+
        +'SAC*C*D240***0.2~'+
        +'SAC*C*H850***0.3~'+
        +'CTT*3~'+
        +'SE*23*159976~'+
        +'GE*1*95813~'+
        +'IEA*1*000095813~';

    public Testmethod Static void TestcXMLInvoiceBatchJob(){
        initTestData();
        Attachment xmlAttachment=new Attachment();
        xmlAttachment.name='Vendor-School Specialty-test@gmail.com';
        xmlAttachment.body=Blob.ValueOf(XMLInvoiceBody);
        xmlAttachment.contentType='text/xml';
        insert xmlAttachment;
        
        Database.ExecuteBatch(new ProcessInvoiceFilesBatch());
    } 
    
    public Testmethod Static void TestcXMLInvoiceBatchJob_2(){
        initTestData();
        list<AcctSeed__Accounting_Variable__c> acvlist = new list<AcctSeed__Accounting_Variable__c>();    
        AcctSeed__Accounting_Variable__c  acv1 = new AcctSeed__Accounting_Variable__c();
        acv1.AcctSeed__Type__c = 'GL Account Variable 1';
        acv1.AcctSeed__Active__c = true;
        acv1.Name = 'Unrestricted';
        
        AcctSeed__Accounting_Variable__c  acv2 = new AcctSeed__Accounting_Variable__c();
        acv2.AcctSeed__Type__c = 'GL Account Variable 2';
        acv2.AcctSeed__Active__c = true;
        acv2.Name = 'Programs';
        
        AcctSeed__Accounting_Variable__c  acv3 = new AcctSeed__Accounting_Variable__c();
        acv3.AcctSeed__Type__c = 'GL Account Variable 3';
        acv3.AcctSeed__Active__c = true;
        acv3.Name = 'Classroom Adoption';
        
        AcctSeed__Accounting_Variable__c  acv4 = new AcctSeed__Accounting_Variable__c();
        acv4.AcctSeed__Type__c = 'GL Account Variable 1';
        acv4.AcctSeed__Active__c = true;
        acv4.Name = 'Restricted';
        
        acvlist.add(acv1);
        acvlist.add(acv2);
        acvlist.add(acv3);
        acvlist.add(acv4);
        
        insert acvlist;
        
        AcctSeed__GL_Account__c asgl = new  AcctSeed__GL_Account__c();
        asgl.Name = '4599 Teacher Restricted Released';
        asgl.AcctSeed__Bank__c = true;
        asgl.AcctSeed__Active__c = true;
        asgl.AcctSeed__Type__c = 'Revenue';
        Insert asgl;
        
        Id orderRt = TestUtils.getRecordTypeIdByDeveloperName(Opportunity.SObjectType.getDescribe().getName(), 'Orders');
            OpportunityManagement.afterInsert(null);
        OpportunityManagement.afterUpdate(
          new List<Opportunity> { new Opportunity() }, new Map<Id, Opportunity>());
    
        Opportunity o2 = TestUtils.createOpportunity(account.Id);
        o2.CampaignId = campaign.Id;
        o2.Amount = 1000;
        o2.RecordTypeId = orderRt;
        o2.Teacher__c = contact.Id;
        o2.StageName = OpportunityManagement.STAGE_PAID;
        //o2.Order_Cloud_Order_Id__c='12342345';
        insert o2;
        
        
        product2 p1=new product2(name='test',productcode='12345',isActive=true);
        insert p1;
        
        pricebook2 p=new pricebook2(name='Standard ProceBook',isActive=true);
        insert p;
        
        ID standardPBID = Test.getStandardPricebookId(); 
        
        PricebookEntry pe=new PricebookEntry(Pricebook2Id=standardPBID,Product2Id=p1.id,IsActive=true,UnitPrice=5);
        insert pe;
        
        PricebookEntry pe1=new PricebookEntry(Pricebook2Id=p.id,Product2Id=p1.id,IsActive=true,UseStandardPrice=false,UnitPrice=5);
        insert pe1;
        
        opportunityLineItem line=new opportunityLineItem(OpportunityId=o2.Id,PricebookEntryId=pe.Id,Product2Id=p1.Id,Quantity=1,TotalPrice=5);
        insert line;
        
        AcctSeed__Billing_Format__c bil=new AcctSeed__Billing_Format__c(name='test');
        bil.AcctSeed__Visualforce_PDF_Page__c='PurchaseOrderPDF';
        bil.AcctSeed__Default_Email_Template__c='Purchase_Order_Email_Template';
        insert bil;
        
        try{
        AcctSeed__Ledger__c Actual = new AcctSeed__Ledger__c();
        Actual.Name='test';
        Actual.AcctSeed__Type__c='Transactional';
        Actual.AcctSeed__Billing_Activity_Statement_Format__c=bil.id;
        Actual.AcctSeed__Billing_Outstanding_Statement_Format__c=bil.id;
        Actual.AcctSeed__Default_Bank_Account__c=asgl.id;
        Actual.AcctSeed__Default_Billing_Format__c=bil.id;
        Actual.AcctSeed__Default_Purchase_Order_Format__c=bil.id;
        Actual.AcctSeed__Default_Packing_Slip_Format__c=bil.id;
        insert Actual;
        
        AcctSeedERP__Purchase_Order__c po=new AcctSeedERP__Purchase_Order__c();
        po.Opportunity_Name__c=o2.Id;
        po.AcctSeedERP__Status__c='Open';
        po.AcctSeedERP__Vendor__c=account.id;
        po.VendorId__c='3213421';
        po.AcctSeedERP__Purchase_Order_Format__c=bil.id;
        po.AcctSeedERP__Order_Date__c=Date.newInstance(2017,01,01);
        po.AcctSeedERP__Ledger__c=Actual.id;
        insert po;
        
        AcctSeedERP__Purchase_Order_Line__c pl=new AcctSeedERP__Purchase_Order_Line__c();
        pl.AcctSeedERP__Purchase_Order__c=po.id;
        pl.AcctSeedERP__Quantity__c=1;
        pl.AcctSeedERP__Product__c=p1.id;
        pl.AcctSeedERP__Unit_Price__c=5;
        insert pl;
        }catch(Exception e){}
        
        Attachment xmlAttachment=new Attachment();
        xmlAttachment.name='Vendor-School Specialty-test@gmail.com';
        xmlAttachment.body=Blob.ValueOf(XMLInvoiceBody);
        xmlAttachment.contentType='text/xml';
        insert xmlAttachment;
        
        Database.ExecuteBatch(new ProcessInvoiceFilesBatch());
    } 
    
    public Testmethod Static void TestX12InvoiceBatchJob(){
        initTestData();
        Attachment xmlAttachment=new Attachment();
        xmlAttachment.name='Kaplan';
        xmlAttachment.body=Blob.ValueOf(x12InvoiceBody);
        xmlAttachment.contentType='text/plain';
        insert xmlAttachment;
        
        Database.ExecuteBatch(new ProcessInvoiceFilesBatch());
    }  
    
   public Testmethod Static void TestXMLInvoiceBatchJob(){
        initTestData();
        Attachment xmlAttachment=new Attachment();
        xmlAttachment.name='Vendor-Kaplan-test@gmail.com';
        xmlAttachment.body=Blob.ValueOf(XMLInvoiceBodyWrong);
        xmlAttachment.contentType='text/xml';
        insert xmlAttachment;
        
        Database.ExecuteBatch(new ProcessInvoiceFilesBatch());
    }  
    
    private static void initTestData() {
    
        
        Vendor_Email_Setup__c vendor=new Vendor_Email_Setup__c(name='test@gmail.com');
        insert vendor;
        
        account = TestUtils.createAccount();
        account.Invoice_vendor_name__c ='OFFICE DEPOT';
        insert account;
    
        contact = TestUtils.createContact(account.Id);
        insert contact;
    
        classroom = TestUtils.createClassroom(contact.Id, account.Id);
        insert classroom;
    
        campaign = TestUtils.createCampaign(classroom.Id);
        insert campaign;
   }
     
    
    public Testmethod Static void TestIncorrectInvoiceBatchJob(){
        initTestData();
        Attachment xmlAttachment=new Attachment();
        xmlAttachment.name='Kaplan';
        xmlAttachment.body=Blob.ValueOf(x12InvoiceBody);
        xmlAttachment.contentType='text/xml';
        insert xmlAttachment;
        
        Database.ExecuteBatch(new ProcessInvoiceFilesBatch());
        ProcessInvoiceFilesBatch.testCoverage();
    }     

}